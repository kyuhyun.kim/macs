<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/include.jsp"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="utf-8">
<meta property="og:title" content="집닥" />
<meta property="og:image" content="<spring:message code="STATIC_URL"/>images/zipdoc.jpg"/>
<meta property="og:description" content="인테리어 비교견적 중개 플랫폼 집닥"/>
<meta name="viewport" content="width=device-width,initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<title>인테리어 비교 견적 플랫폼 집닥</title>
<style type="text/css">
	body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, code, form, fieldset, legend, textarea, p, blockquote, th, td, input, select, textarea, button {
		margin: 0;
		padding: 0;
	}

	.subject {
		#background-color:silver;
		margin:10px;
		font-weight:bold;
		font-size:1.5em;
	}

	.contents {
		#background-color:whitesmoke;
	}
	.contents p {
		margin:10px;
		height:20px;
	}
	img {
		width:100%;
	}
	.pictureSubject {
		margin:10px;
		font-weight:bold;
		font-size:1.3em;
	}
	.description {
		margin:10px;
	}
	.tags {
		margin:10px;
		font-weight:bold;
	}
	a:link      {color:#ffffff; text-decoration:none;}
	a:visited   {color:#ffffff; text-decoration:none;}
	a:active    {color:#ffffff; text-decoration:none;}
	a:hover     {color:#6d6d6d; text-decoration:none;}

	.linkButton {
		positon:relative;width:50%;margin:-1px;text-align:center;vertical-align:middle;font-size:1.5em;background-color:#3a3a3a;border:1px solid #4a4a4a;
	}
	.left{
		float:left;
	}
	.right{
		float: right;
	}
</style>
</head>
<body>
<div class="wrap">
	<div class="logo">
		<a href="http://www.zipdoc.co.kr"><img src='<c:url value="/images/zipdoc.jpg"/>' style="width:100%"/></a>
	</div>
	<div class="subject">
		${interior.subject}
	</div>
	<div class="contents">
		<p>
			<c:choose>
				<c:when test="${interior.category_code1 eq '10'}">주거공간</c:when>
				<c:when test="${interior.category_code1 eq '20'}">상업공간</c:when>
				<c:when test="${interior.category_code1 eq '30'}">부분시공</c:when>
				<c:otherwise>${interior.category_code1}</c:otherwise>
			</c:choose>
			| ${interior.space}평
		</p>
		<p>
			${interior.address}
		</p>
<c:choose>
	<c:when test="${not empty interior.items}">
		<c:forEach items="${interior.items}" var="item" varStatus="roop">
		<div class="pictureArea">
			<div class="picture">
				<img src="${item.thumbnail_url}"/>
			</div>
			<div class="pictureSubject">
				${item.subject}
			</div>
			<div class="description">
				${fn:replace(item.description, newLineChar, "<br/>")}
			</div>
			<div class="tags">
				<c:forEach items="${item.tag_list}" var="tag" varStatus="roop">
					#${fn:replace(tag, "*", "")}
				</c:forEach>
			</div>
		</div>
		</c:forEach>
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>
		<div id="buttonArea">
			<div class="linkButton left"><a href="http://www.zipdoc.co.kr">집닥홈</a></div>
			<div class="linkButton right"><a href="http://www.zipdoc.co.kr/zws/product/getList.do">더보기</a></div>
		</div>
	</div>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/include.jsp"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%--<meta name="viewport" id="viewport" content="width=device-width,initial-scale=1, minimum-scale=1, user-scalable=no">--%>
<meta name="viewport" content="user-scalable=yes, initial-scale=1.0, width=device-width">
<title>인테리어 비교 견적 플랫폼 집닥</title>
<link rel="stylesheet" href="<c:url value='/css/acs_common.css' />">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.js"></script>
<%--<script>

	var defaultWidth = $(window).width();
	var zoomLevel = 1;


	function zoomIn(){
		if(zoomLevel >= 8){
			$("#btn_zoom_in").hide();
			$("#btn_zoom_out").show();
			return;
		}
		zoomLevel++;
		var clientWidth = zoomLevel*100 + defaultWidth;
		$(".contents").css("width", clientWidth + "px");
	}

	function zoomOut(){
		if(zoomLevel <= 1){
			$("#btn_zoom_in").show();
			$("#btn_zoom_out").hide();
			return;
		}
		zoomLevel--;
		if(zoomLevel<=1){
			$(".contents").css("width", "100%");
		} else {
			var clientWidth = zoomLevel*100 + defaultWidth;
			$(".contents").css("width", clientWidth + "px");
		}
	}
</script>--%>
</head>
<body>
<div class="wrap">
	<div class="header_section">
		<div class="thumbnail">
			<img src="${article.thumbnail_url}"/>
			<%--<img src="http://192.168.1.210/static/${article.file_path}${article.thumbnail}"/>--%>
		</div>

		<div class="subject">
			<h3>${article.subject}</h3>
		</div>

		<div class="category">
			${article.category} <c:if test="${not empty article.category }">|</c:if> <fmt:formatDate value="${article.create_date}" pattern="yyyy.MM.dd"/>
		</div>

	<c:if test="${article.start_date != null && article.end_date != null}">
		<div class="event">
			이벤트 기간 : <fmt:formatDate value="${article.start_date}" pattern="yyyy.MM.dd"/> ~ <fmt:formatDate value="${article.end_date}" pattern="yyyy.MM.dd"/>
		</div>
	</c:if>
	</div>
	<div class="body_section">
		<div class="contents">
			<c:set var="staticUrl"><spring:message code="STATIC_URL"/>smarteditor</c:set>
			<c:out value="${article.contents}"  escapeXml="false" />
		</div>
	</div>
</div>
<!-- <a id="btn_zoom_in" class="btn_zoom" href="javascript:zoomIn();">+</a>
<a id="btn_zoom_out" class="btn_zoom" href="javascript:zoomOut();" style="display:none;!important">-</a> -->
	<!-- 동영상 -->
	<script>
	var video_file = $("iframe[src*='//www.youtube.com']");
	video_file.each(function() {
	    $(this).parent().parent().css('max-width', $(this).attr('width') + 'px');
	    $(this).parent().css('padding-bottom', $(this).attr('height')/$(this).attr('width')*100 + '%');
	    $(this).removeAttr("width").removeAttr("height"); });
	</script>
	<!-- //동영상 -->
</body>
</html>
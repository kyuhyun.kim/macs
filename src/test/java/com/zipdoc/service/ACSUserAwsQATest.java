package com.zipdoc.service;

import com.zipdoc.acs.define.AppType;
import com.zipdoc.acs.define.MemberStatus;
import com.zipdoc.acs.define.PictureType;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.PartnerService;
import com.zipdoc.acs.model.PartnerType;
import com.zipdoc.acs.model.SubscriberType;
import com.zipdoc.acs.domain.entity.Account;
import com.zipdoc.acs.model.subs.ZID;
import com.zipdoc.acs.persistence.dao.ArticleDao;
import com.zipdoc.acs.utils.RandomString;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;

public class ACSUserAwsQATest extends AbstractApplicationContextTestForAwsQA { // AbstractApplicationContextTestForReal

    private static final Logger log = LoggerFactory.getLogger(ACSUserAwsQATest.class);

    @Autowired
    public ArticleDao articleDao;

    @Autowired
    public MemberService memberService;

    @Autowired
    public PartnerService partnerService;


    @Test
    public void test_qa_db() {

        log.info("test_qa_db");
        assertNotNull(articleDao);
        Article a = (Article) articleDao.selectEvent(6);

        assertNotNull(a);
        log.info(" getSubject : " + a.getSubject());
    }


    /**
     * 파트너스 로그인
     * @throws Exception
     */
    @Test
    public void test_partners_login() throws Exception {

        //비즈디자인 상용 계정
        String app_version = "1.7.3";
        String os_type = "android";
        String os_version = "7.0";
        String device_id = "352460074324849";
        String device_model = "SM-N920K";
        String mdn = "01071764411";
        String token = "fXV97t2TRdY:APA91bHzDr6q9jPE0_X_OuErtRg9DXeZlthhA4T7";
//        String token = "fXV97t2TRdY:APA91bHzDr6q9jPE0_X_OuErtRg9DXeZlthhA48b21cNzaYH9Zv6FO2JUa2Y_7FOLl2k_xqw8BtoPfCy2cv47oXbZWbTQsrmR9NXEAk6E7LPWd8TQlDGUFY16qHrwetDg7vMS-BY2RT7";

        Account account = new Account();
        account.setAccountType(1);
        account.setAccount("woonoxg@naver.com");
        account.setPassword("1234");
        account.setUsername("");
        

        // 이전 자동 가입 정보 조회
        Member device = memberService.selectDeviceByDeviceId(device_id, AppType.PARTNER.value());

        // 디바이스 ID와 MDN 매핑이 다른 경우
		/*
		if (device != null && !StringUtils.equals(mdn, device.getMobile_no())) {
			int rows = partnerService.deleteDeviceByDeviceId(device_id);
			log.debug("[PARTNER-LOGIN] MDN["+mdn+"]에 등록된 디바이스 정보가 일치하지 않아 DEVICE["+device_id+"] "+rows+"개를 삭제합니다.");
			device = null;
		}
		*/
        // 이전 자동 가입 정보가 없는 경우
        if (device == null) {

            System.out.println("device = null -> create device");

            device = new Member(
                    -1,
                    generationKey(),
                    mdn,
                    app_version,
                    os_type,
                    os_version,
                    device_id,
                    device_model,
                    token);

            System.out.println("device : " + device.getZid() + " "
                    + device.getZid_key());

            int rows = memberService.createDevice(device);
            if (rows == 0) {
                System.out.println("[PARTNER-LOGIN] MDN[" + mdn + "] 디바이스 등록에 실패했습니다.");
            }
        }  else { //이전 기기 정보가 있는 경우

            System.out.println("device : " + device.getZid() + " "
                    + device.getZid_key());

            //Edit by DKLEE(2016/10/11) 토큰값이 다른 경우 갱신 처리 로직 추가함.
            //푸시토큰이 불일치한는 경우 토큰값을 갱신한다.
            if(StringUtils.isNotEmpty(token) && !StringUtils.equals(device.getPush_token(), token)){
                device.setPush_token(token);
                memberService.updateToken(device);
            }
        }

        // 계정명으로 가입자 정보를 조회한다.
        Member accountMember = memberService.selectMemberByAccount(account);

        // 계정명으로 가입자가 검색되지 않을 경우. 파트너 계정은 웹을 통해 미리 등록되어 있어야 한다.
        if (accountMember == null) {
            System.out.println("[PARTNER-LOGIN] 요청 계정[" + account.getAccount() + "]이 없거나 비밀번호가 유효하지 않습니다.");
            return;
        }

        // 계정 사용자 구분 검사. 파트너 계정이 아닌 경우
        SubscriberType subsType = SubscriberType.valueOf(accountMember.getMember_type());
        if (subsType != SubscriberType.CORPORATE) {
            System.out.println("[PARTNER-LOGIN] 요청 계정[" + account.getAccount() + "/"+subsType+"]은 파트너 계정이 아닙니다.");
            return;
        }

        // 2017.12.14. 장한솔
        // 회원탈퇴만 처리하도록 변경
        if (accountMember.getStatus() != null && accountMember.getStatus() == MemberStatus.WITHDRAWAL.getCode()) {
            System.out.println("[PARTNER-LOGIN] 요청 계정[" + account.getAccount() + "/"+accountMember.getStatus()+"]은 탈퇴 상태입니다.");
            return;
        }

        // 파트너 업체 정보 조회
        Partner partner = partnerService.selectPartner(accountMember.getPartner_id());

        System.out.println("partner : " + partner.getPartner_id() + " "
                + partner.getPartner_type());


        // 파트너 사 승인 여부
        if (partner == null || partner.getPartner_type() == PartnerType.UNAPPROVED.value()) {
            System.out.println("[PARTNER-LOGIN] 요청 계정[" + account.getAccount() + "/"+accountMember.getStatus()+"]은 파트너 가입자가 아니거나 미승인 상태입니다.");
            return;
        }

        partner.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);

        // 계정과 디바이스 매핑 정보 등록
        accountMember.setZid(device.getZid());
        if (device.getUser_level() == SubscriberType.AUTO.value()) { // 자동가입 고객으로 되어있는 경우 갱신 처리
            accountMember.setUser_level(subsType.value());
            memberService.updateDeviceUserLevel(accountMember);
        } else {
            accountMember.setUser_level(device.getUser_level());
        }

        // T_MEMBER_DEVICE 정보를 등록 처리한다.
        memberService.treatMemberDevice(accountMember);

        /**
         * 장한솔.
         * 테스트에서만 주석처리 (상용에서는 ip 갱신)
         */
        //마지막 로그인 정보를 갱신 처리한다.
//        accountMember.setLogin_ip(IpUtil.getClientIP(request));
//        memberService.updateMemberLoginInfo(accountMember);

        ZID result = new ZID(device.getZid(), device.getZid_key());
        account.setMember_no(accountMember.getMember_no());
        account.setSubsType(accountMember.getUser_level());
        account.setAccountType(accountMember.getAccount_type());
        account.setUsername(accountMember.getUsername());
        account.setPassword(null); // Hidden

        // 파트너 업체 정보
        account.setPartner(partner);

        result.setAccount(account);

        System.out.println("200 OK " + account.getZid() + " " + account.getAccount());
        return;
    }

    private String generationKey() {
        RandomString randomString = new RandomString();
        return randomString.getString(16, "");
    }

}

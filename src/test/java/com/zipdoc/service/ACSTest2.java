package com.zipdoc.service;

import com.zipdoc.acs.controller.ArticleController;
import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.domain.entity.AppVersion;
import com.zipdoc.acs.domain.entity.Article;
import com.zipdoc.acs.domain.service.AppVersionService;
import com.zipdoc.acs.domain.service.ArticleService;
import com.zipdoc.acs.domain.service.CommentService;
import com.zipdoc.acs.persistence.dao.AppVersionDao;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author akm
 *  AbstractApplicationContextTest 를 상속받은 Test Class 예제
 *  같은 형식으로 계속만들어서 사용하면 됨  ( extends AbstractApplicationContextTest )
 */

public class ACSTest2 extends AbstractApplicationContextTestForReal{
	
	private static final Logger log = LoggerFactory.getLogger(ZipACSTest.class);
	
	/*
	 * controller, dao, service TEST 예제
	 * *Controller, *Service, *Dao 등록, @Autowired 필수
	 */
//	@Autowired
//	private ArticleController articleController;
//
//	@Autowired
//	private AppVersionDao appVersionDao;
	
	@Autowired
	private AppVersionService appVersionService; 
	
	@Autowired
	private ArticleService articleService;
	
//	@Autowired
//	private CommentService commentService;

	/*

	 */
	@Mock
	CommentService commentService;

	@InjectMocks
	private ArticleController articleController;


	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(articleController).build();
	}

	@Test
	public void testCategoryController() throws Exception {

//		verify(mockMvc.perform(get("/article/detail/1/1") ));
//		when(articleService.selectArticle(ArticleType.NOTICE, 8)).thenReturn(10);
//		mockMvc.perform( get("/article/detail/1/1")).andExpect(status().isOk());
//		verify(articleService.selectArticle(ArticleType.NOTICE, 8)).method1();
//		verifyNoMoreInteractions(articleService);

	}


	/*
	 * @Test 어노테이션 표기시 테스트에 포함
	 * @Autowired bean 생성 테스트 ( not null test )
	 */
	@Test
	public void test_AppVersionDao_test1() {
		Article article = articleService.selectArticle(ArticleType.NOTICE, 8);

		log.info("article.getSubject() "  + article.getSubject() );
		log.info("article.getContents() "  + article.getContents() );
		log.info("article.getCno() "  + article.getCno() );

		log.info("test complete!!  " );

	}
	
	/*
	 * 필요한 TEST 계속 추가 가능
	 */
	@Test
	public void test_AppVersionDao_test2() {
		

	}

	/*
	 *  필요한 TEST 계속 추가 가능, articleService.selectArticle 메소드 테스트
	 */
	@Test
	public void test_AppVersionDao_test3() {

	}
}
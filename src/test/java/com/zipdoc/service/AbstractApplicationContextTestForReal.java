package com.zipdoc.service;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author akm
 * acs Junit 테스트용 추상 클래스, 실제 설정 xml 파일 경로 지정,
 * web.xml 파일에 기재된 내용중 root-context.xml,  servlet-context.xml 하위의 설정만 loading 됨
 * <p>
 * context.test 폴더에 별도로 저장된 테스트용 context file 임
 * --> 테스트 속도를 위해 annotation scan 을 제거함
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/resources/root-context.xml",
        "file:src/main/resources/servlet-context.xml"
})
@WebAppConfiguration
public abstract class AbstractApplicationContextTestForReal {

    /**
     * 객체를 json 문자열로 변환
     * @param obj
     * @return
     */
    protected static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            System.out.println(e.toString());
            throw new RuntimeException(e);
        }
    }
}

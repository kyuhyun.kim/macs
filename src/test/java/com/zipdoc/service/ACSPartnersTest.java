package com.zipdoc.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.zipdoc.acs.controller.CommonController;
import com.zipdoc.acs.controller.ContractController;
import com.zipdoc.acs.controller.PartnerController;
import com.zipdoc.acs.controller.subs.AgreementController;
import com.zipdoc.acs.controller.subs.PartnersLoginController;
import com.zipdoc.acs.domain.entity.PartnerAssignGeo;
import com.zipdoc.acs.domain.service.PartnerService;
import com.zipdoc.acs.model.ContractSideCallRequest;
import com.zipdoc.acs.persistence.dao.PartnerDao;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ACSPartnersTest extends AbstractApplicationContextTestForReal {

    private static final Logger log = LoggerFactory.getLogger(ACSPartnersTest.class);

    /**
     * 계정 정보
     * aws.zipdocdev.com(개발서버)
     * hsjang / hsjang (솔솔인테리어)
     */
    private static final String APP_VERSION = "1.6.3";
    private static final String APP_MARKET = "google";
    private static final String MEMBER_NO = "523";
    private static final String ZID = "8535";
    private static final String ZID_KEY = "UeRxEXxaPXb7X2Gp";
    private static final String OS_TYPE = "android";
    private static final String OS_VERSION = "6.0.1";
    private static final String DEVICE_ID = "355504071399135";
    private static final String DEVICE_MODEL = "SM-G930L";
    private static final String MDN = "355504071399135";
    private static final String TOKEN = "fFaRrz2LgFs:APA91bGC0fvBmoOD5jDf7sBNJVY-_VNThqMXp7pohA0acBxgyAd8fEliGbvXcNckOlfDb_N9Gi3FHYa4ZP3AC4i8vakF8ix38coGEC4u6UXEiEirV5ucILfVrN8BIFpyNHSniHpQbeSZ";
    //usually use in body params
    private static final String PARTNER_ID = "74";
    private static final String ESTIMATE_NO = "978";
    private static final String CONTRACT_NO = "385";


//    private static final String APP_VERSION = "1.7.3";
//    private static final String APP_MARKET = "google";
//    private static final String MEMBER_NO = "2906";
//    private static final String ZID = "68327";
//    private static final String ZID_KEY = "hu2e3qjmufeNAHsy";
//    private static final String OS_TYPE = "android";
//    private static final String OS_VERSION = "7.1.1";
//    private static final String DEVICE_ID = "351565090188506";
//    private static final String DEVICE_MODEL = "SM-N950N";
//    private static final String MDN = "01054180034";
//    private static final String TOKEN = "ejJ32jOmFSw:APA91bFv9CjyiCIA-fHRhh9WQhOedt3Xi1O5gINcuM6Pe5l4AwK86DWKjwWNhogFkRSYHjYN64t9Q_cJpfd3pyIbYx1yXWRQH7P5jlgbSqdbTHnKa8tovdNPEVzg2V6jPlJrnAFLZ8-G";

    private HttpHeaders commonHeaders;

    private MockMvc mock;

    @Autowired
    private PartnersLoginController loginController;

    @Autowired
    private CommonController commonController;

    @Autowired
    private PartnerController partnerController;

    @Autowired
    private ContractController contractController;

    @Autowired
    private AgreementController agreementController;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private PartnerDao partnerDao;

    @Test
    public void testGeo(){

        assertNotNull(partnerService);
        List<PartnerAssignGeo> list = partnerService.selectAssignWithdrawGeo(690) ;
        List<PartnerAssignGeo> list2 = partnerDao.selectCSbehindContractList(690);

        assertNotNull(list);
        assertNotNull(list2);


    }

    @Before
    public void setup() {
        this.commonHeaders = new HttpHeaders();
        this.commonHeaders.add("Accept-Encoding", "gzip");
        this.commonHeaders.add("APP-VERSION", APP_VERSION);
        this.commonHeaders.add("MEMBER-NO", MEMBER_NO);
        this.commonHeaders.add("ZID", ZID);
        this.commonHeaders.add("ZID-KEY", ZID_KEY);
        this.commonHeaders.add("APP-MARKET", APP_MARKET);
    }

    @Test
    public void test_init() throws Exception {

        assertNotNull(commonHeaders);

        assertNotNull(loginController);
        assertNotNull(commonController);
        assertNotNull(partnerController);
        assertNotNull(contractController);
        assertNotNull(agreementController);
    }

    /**
     * 파트너스 로그인
     *
     * @throws Exception
     */
    @Test
    public void test_login() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(loginController).build();

        JSAccount json = new JSAccount();
        json.account_type = 1; //zipdoc
        json.account = "soldierjti@naver.com";
        json.password = "zipdoc3@";
        json.name = "";

        System.out.println(asJsonString(json));

        this.mock.perform(
                post("/partner/login")
                        .headers(commonHeaders)
                        .param("os_type", OS_TYPE)
                        .param("os_version", OS_VERSION)
                        .param("device_id", DEVICE_ID)
                        .param("device_model", DEVICE_MODEL)
                        .param("mdn", MDN)
                        .param("token", TOKEN)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(json)))
                .andDo(print())
                .andExpect(status().isOk());

    }

    /**
     * 사용자 인증
     *
     * @throws Exception
     */
    @Test
    public void test_auth() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(loginController).build();

        this.mock.perform(
                get("/partner/authentication")
                        .headers(commonHeaders)
                        .param("os_type", OS_TYPE)
                        .param("os_version", OS_VERSION)
                        .param("device_id", DEVICE_ID)
                        .param("device_model", DEVICE_MODEL)
                        .param("mdn", MDN))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 초기 데이터 조회
     *
     * @throws Exception
     */
    @Test
    public void test_serviceinfo() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(commonController).build();

        this.mock.perform(
                get("/partner/serviceinfo")
                        .header("APP-VERSION", APP_VERSION))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=utf-8"));
    }

    /**
     * 메인에 보여질 업체 정보 조회
     *
     * @throws Exception
     */
    @Test
    public void test_partner() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(partnerController).build();

        this.mock.perform(
                get("/partner/detail")
                        .headers(commonHeaders))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=utf-8"));
    }

    /**
     * 이면계약 의심 통화이력 추가
     *
     * @throws Exception
     */
    @Test
    public void test_side_call_add() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(contractController).build();

        ContractSideCallRequest json = new ContractSideCallRequest();
        json.setEstimate_no(Long.parseLong(ESTIMATE_NO));
        json.setPartner_id(Integer.parseInt(PARTNER_ID));
        json.setPartner_phone("01044445555");
        json.setWriter_phone("01033334444");
        json.setOutgoing_flag(1);

        System.out.println("body : " + asJsonString(json));

        this.mock.perform(
                post("/contract/side/call/add")
                        .headers(commonHeaders)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(json)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 개인정보취급방침 약관 동의
     * @throws Exception
     */
    @Test
    public void test_agree_privacy() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(agreementController).build();

        this.mock.perform(
                post("/agree/privacy")
                        .headers(commonHeaders))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     *
     * json Obj.
     *
     */

    public class JSAccount {
        public int account_type;
        public String account;
        public String password;
        public String name;
    }
}

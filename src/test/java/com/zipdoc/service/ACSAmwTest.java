package com.zipdoc.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.zipdoc.acs.controller.ArticleController;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ACSAmwTest extends AbstractApplicationContextTestForReal {

    private static final Logger log = LoggerFactory.getLogger(ACSAmwTest.class);

    private MockMvc mock;

    @Autowired
    private ArticleController articleController;

    @Before
    public void setup() {

    }

    /**
     * 건축사(AMW) 메인 페이지에서 로드할 집닥 소식
     * 앱과는 다르게 header 없이도 작동
     * @throws Exception
     */
    @Test
    public void test_news_list_amw() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(articleController).build();

        this.mock.perform(
                get("/article/list/news/0/10"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}

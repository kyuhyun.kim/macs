package com.zipdoc.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.zipdoc.acs.controller.GeoLocationController;
import com.zipdoc.acs.controller.PartnerController;
import com.zipdoc.acs.domain.entity.RecommendKeyword;
import com.zipdoc.acs.persistence.dao.CommonDao;
import com.zipdoc.acs.utils.FileUploadUtil;
import com.zipdoc.acs.utils.Messages;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.zipdoc.acs.controller.ArticleController;
import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.domain.entity.AppVersion;
import com.zipdoc.acs.domain.entity.Article;
import com.zipdoc.acs.domain.service.AppVersionService;
import com.zipdoc.acs.domain.service.ArticleService;
import com.zipdoc.acs.domain.service.CommentService;
import com.zipdoc.acs.persistence.dao.AppVersionDao;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * @author akm
 *  AbstractApplicationContextTest 를 상속받은 Test Class 예제
 *  같은 형식으로 계속만들어서 사용하면 됨  ( extends AbstractApplicationContextTest )
 */

public class ACSTest1 extends AbstractApplicationContextTestForReal{
	
	private static final Logger log = LoggerFactory.getLogger(ZipACSTest.class);
	
	/*
	 * controller, dao, service TEST 예제
	 * *Controller, *Service, *Dao 등록, @Autowired 필수
	 */
	@Autowired
	private ArticleController articleController;

	@Autowired
	private PartnerController partnerController;


	@Autowired
	private AppVersionDao appVersionDao;
	
	@Autowired
	private AppVersionService appVersionService; 
	
	@Autowired
	private ArticleService articleService;
	
	@Autowired
	private CommentService commentService;

	@Autowired
	private CommonDao commonDao;

	@Autowired
	private GeoLocationController geoLocationController;

//	@Autowired
//	private FileUploadUtil fileUtil;

	private MockMvc mock;

	private HttpHeaders commonHeaders;

	@Before
	public void setup() {

		this.commonHeaders = new HttpHeaders();
		this.commonHeaders.add("Accept-Encoding", "gzip");
		this.commonHeaders.add("zid-key", "3Iis2lfhGM41irDz");
		this.commonHeaders.add("zid", "6930");
		this.commonHeaders.add("member-no", "1");
		this.commonHeaders.add("APP-VERSION", "3.2.0");
	}

	@Test
	public void test_partner_list() throws Exception{
		this.mock = MockMvcBuilders.standaloneSetup(partnerController).build();

		this.mock.perform(
				post("/partner/list/2/12/0")
						.headers(commonHeaders))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void geo_loc_test() throws Exception{
		this.mock = MockMvcBuilders.standaloneSetup(geoLocationController).build();

		assertNotNull(this.mock );

		this.mock.perform(
				get("/checkLocation")
						.headers(commonHeaders))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=utf-8"));
	}

	@Test
	public void key_test(){

		List<RecommendKeyword> keywords = commonDao.selectRecommendKeywordList();
		assertNotNull(keywords);



	}

	@Test
	public void s3_cli_test(){

		assertTrue( FileUploadUtil.getInstance().isExist( "akmTest", "/lam2.jpg") );
		assertTrue( FileUploadUtil.getInstance().isExist( "akmTest/akm2/akm3", "/lam2.jpg") );

	}

	@Test
	public void fileup_test() throws InterruptedException {

		log.info(" s3 helper test ");

        /*
        akm test code
         */
		String filePath = "/home/akm/Downloads/static/huracan-coupe-facebook-og.jpg";
		String filePathDir = "/home/akm/Downloads/static/";
		String filename_ext = "jpg";


		File file = new File(filePath);
		if (!file.exists()) {
			file.mkdirs();
		}
		String realFileNm = "12341234."+filename_ext;

        /*
        같은 이름의 파일의 경우 덮어 씀, update 의 경우 같은 이름인 경우 같은 로직 사용
        test ok
        */
		FileUploadUtil.getInstance().uploadToS3( file.getParentFile(), "akmTest/akm2/akm3/" + realFileNm );
//		FileUploadUtil.getInstance().uploadToS3( new File(filePath).getParentFile(), "akmTest/akm2/akm3/" );


	}

	/*
	 * @Test 어노테이션 표기시 테스트에 포함 
	 * @Autowired bean 생성 테스트 ( not null test ) 
	 */
	@Test
	public void test_AppVersionDao_test1() {
		
		// 예제 1. Junit 테스트 창에 나타남, 3 > 2 가 true 인지 체크
		assertTrue(3 > 2);
		
		// @Autowired bean 생성 확인, not null 인지 체크, null 이 아닐 경우 TEST 통과 
		assertNotNull(articleController);
		assertNotNull(appVersionDao);

		assertNotNull(appVersionService);
		assertNotNull(articleService);
		assertNotNull(commentService);
		
		// 여기 까지 실행시 테스트 통과 완료 
		log.info("test complete!!  " );
	}
	
	/*
	 * 필요한 TEST 계속 추가 가능
	 */
	@Test
	public void test_AppVersionDao_test2() {
		
		/*
		 *  appVersionService 테스트 , 실제 구현은 AppVersionServiceImpl 에 있음, 
		 *  파라미터는 android 앱에서 "android", "google" 호출 ( AuthenticationController - 에서 사용 ) 
		 *  DB 접근 후 AppVersion appVer 객체 생성
		 */
		AppVersion appVer = appVersionService.selectVersion("android", "google");
		
		// appVer 이 null 인지 체크 
		assertNotNull(appVer);
				
		log.info( " appVer.getApp_market() : " +  appVer.getApp_market() );
		log.info( " appVer.getApp_version() " +  appVer.getApp_version() );
		
		log.info("test complete!!  " );
	}
	
	/*
	 *  필요한 TEST 계속 추가 가능, articleService.selectArticle 메소드 테스트
	 */
	@Test
	public void test_AppVersionDao_test3() {
		
		Article article = articleService.selectArticle(ArticleType.INTERIOR_TIP, 90);
		
		log.info("article.getSubject() "  + article.getSubject() );
		log.info("article.getContents() "  + article.getContents() );
		log.info("article.getCno() "  + article.getCno() );
		
		log.info("test complete!!  " );
	}
}
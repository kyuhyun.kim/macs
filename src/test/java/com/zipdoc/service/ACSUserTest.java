package com.zipdoc.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.zipdoc.acs.controller.*;
import com.zipdoc.acs.controller.subs.AuthenticationController;
import com.zipdoc.acs.controller.subs.LoginController;
import com.zipdoc.acs.controller.subs.RegistrationController;
import com.zipdoc.acs.domain.entity.CrashLog;
import com.zipdoc.acs.model.AccountType;
import com.zipdoc.acs.model.EstimateRequest;
import com.zipdoc.acs.model.SubscriberType;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author hansolzzang
 * app 환경에서 api request
 */
public class ACSUserTest extends AbstractApplicationContextTestForReal {

    private static final Logger log = LoggerFactory.getLogger(ACSUserTest.class);

    /**
     * 계정 정보
     * aws.zipdocdev.com(개발서버)
     * zipdoc79(구글 계정으로 로그인)
     */
    private static final String APP_VERSION = "3.2.0";
    private static final String APP_MARKET = "google";
    private static final String MEMBER_NO = "1";
    private static final String ZID = "8454";
    private static final String ZID_KEY = "CaBplf81joFVEtuw";
    private static final String OS_TYPE = "android";
    private static final String OS_VERSION = "6.0.1";
    private static final String DEVICE_ID = "352461073880401";
    private static final String DEVICE_MODEL = "SM-N920L";
    private static final String MDN = "352461073880401";
    private static final String TOKEN = "c9AvRZp0v_g:APA91bE_3fjkJf8Md-o4QP7njuhN-UpexlazMB8tQ1SWjEBImDTMPGYEVzcRa5Ip_xrYDj-P7UG-og0n6ge78xr7AAQ36W10oQ8MX9m6iK_KkZb4yVaDGk-ue7cX0qTBBz_ujBIBUDDN";

    private HttpHeaders commonHeaders;

    private HttpHeaders webHeaders;

    private MockMvc mock;

    @Autowired
    private CommonController commonController;

    @Autowired
    private HomeScreenController homeScreenController;

    @Autowired
    private AuthenticationController authController;

    @Autowired
    private EstimateController estimateController;

    @Autowired
    private PartnerController partnerController;

    @Autowired
    private LoginController loginController;

    @Autowired
    private ProductController productController;

    @Autowired
    private RegistrationController registrationController;

    @Before
    public void setup() {

        this.commonHeaders = new HttpHeaders();
        this.commonHeaders.add("Accept-Encoding", "gzip");
        this.commonHeaders.add("APP-VERSION", APP_VERSION);
        this.commonHeaders.add("MEMBER-NO", MEMBER_NO);
        this.commonHeaders.add("ZID", ZID);
        this.commonHeaders.add("ZID-KEY", ZID_KEY);
        this.commonHeaders.add("APP-MARKET", APP_MARKET);

        webHeaders = new HttpHeaders();
        commonHeaders.add("zid-key", "3Iis2lfhGM41irDz");
        commonHeaders.add("zid", "6930");
        commonHeaders.add("member-no", "1");
        commonHeaders.add("APP-VERSION", "3.2.0");


    }

    @Test
    public void test_init() throws Exception {

        assertNotNull(commonHeaders);

        assertNotNull(commonController);
        assertNotNull(authController);
        assertNotNull(estimateController);
        assertNotNull(loginController);
        assertNotNull(productController);
        assertNotNull(registrationController);
    }

    @Test
    public void test_acs3_main_keyword() throws Exception  {
        this.mock = MockMvcBuilders.standaloneSetup(homeScreenController).build();

        this.mock.perform(
                get("/main/residential")
                        .header("APP-VERSION", APP_VERSION))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=utf-8"));

//        this.mock.perform(
//                get("/main/commercial")
//                        .header("APP-VERSION", APP_VERSION))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(content().contentType("application/json;charset=utf-8"));
    }

    /**
     * 앱 실행 후 초기 데이터 조회
     *
     * @throws Exception
     */
    @Test
    public void test_serviceinfo() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(commonController).build();

        this.mock.perform(
                get("/serviceinfo")
                        .header("APP-VERSION", APP_VERSION))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 크래시 로그 전송
     *
     * @throws Exception
     */
    @Test
    public void test_crash() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(commonController).build();

        CrashLog json = new CrashLog();
        json.setOs_type(OS_TYPE);
        json.setOs_version(OS_VERSION);
        json.setDevice_model(DEVICE_MODEL);
        json.setCategory("critical error");
        json.setStack_trace("NPE");

        this.mock.perform(
                post("/crash")
                        .headers(commonHeaders)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(json)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 파트너 api 테스트
     *
     * @throws Exception
     */
    @Test
    public void test_partner_list() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(partnerController).build();
        this.mock.perform(
                post("/partner/list/2/12/0").headers(webHeaders)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 사용자 인증
     *
     * @throws Exception
     */
    @Test
    public void test_auth() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(authController).build();

        this.mock.perform(
                get("/authentication")
                        .headers(commonHeaders)
                        .param("os_type", OS_TYPE)
                        .param("os_version", OS_VERSION)
                        .param("device_id", DEVICE_ID)
                        .param("device_model", DEVICE_MODEL)
                        .param("mdn", MDN))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 사용자 인증 (탈퇴회원)
     * @throws Exception
     */
    @Test
    public void test_auth_withdraw() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(authController).build();

        // withdraw / test@123
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept-Encoding", "gzip");
        headers.add("APP-VERSION", APP_VERSION);
        headers.add("MEMBER-NO", "698");
        headers.add("ZID", "8478");
        headers.add("ZID-KEY", "RjcRVKYvjevGjWMT");
        headers.add("APP-MARKET", APP_MARKET);

        this.mock.perform(
                get("/authentication")
                        .headers(headers)
                        .param("os_type", OS_TYPE)
                        .param("os_version", OS_VERSION)
                        .param("device_id", DEVICE_ID)
                        .param("device_model", DEVICE_MODEL)
                        .param("mdn", MDN))
                .andDo(print())
                .andExpect(status().is(401));
    }

    /**
     * 간편견적 신청
     *
     * @throws Exception
     */
    @Test
    public void test_simple_estimate() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(estimateController).build();

        EstimateRequest json = new EstimateRequest();
        json.setWriter("장한솔");
        json.setPhone_no("010-1111-2222");
        json.setComment("요청사항없음");
        json.setEstimate_type(1);
        json.setSubject("서울 아파트 인테리어");

        this.mock.perform(
                post("/estimate").headers(commonHeaders)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(json)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 상세견적 신청
     *
     * @throws Exception
     */
    @Test
    public void test_estimate() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(estimateController).build();

        EstimateRequest json = new EstimateRequest();
        json.setCategory_code1("10");
        json.setCategory_code2("1010");
        json.setAddress("서울");
        json.setAddress("강남구 역삼동 621-2");
        json.setBudget("1000-2000만원");
        json.setSpace("10평 이하");
        json.setWriter("장한솔");
        json.setPhone_no("010-1111-2222");
        json.setComment("요청사항없음");
        json.setEstimate_type(2);
        json.setSubject("서울 주택 인테리어");
        json.setAgreement_marketing(1);

        this.mock.perform(
                post("/estimate")
                        .headers(commonHeaders)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(json)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 로그인
     */

    @Test
    public void test_login() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(loginController).build();

        JSAccount json = new JSAccount();
        json.subs_type = SubscriberType.PERSONAL.value();
        json.account = "N45947570";
        json.password = "zipdoc";
        json.account_type = AccountType.ACCOUNT_NAVER.value();
        json.name = "";

        this.mock.perform(
                post("/login")
                        .headers(commonHeaders)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(json)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 탈퇴한 사용자
     * @throws Exception
     */
    @Test
    public void test_login_withdraw() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(loginController).build();

        JSAccount json = new JSAccount();
        json.subs_type = SubscriberType.PERSONAL.value();
        json.account = "withdraw";
        json.password = "test@123";
        json.account_type = AccountType.ACCOUNT_ZIPDOC.value();
        json.name = "";

        this.mock.perform(
                post("/login")
                        .headers(commonHeaders)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(json)))
                .andDo(print())
                .andExpect(status().is(401));
    }

    /**
     * 단일 시공사례 조회
     * @throws Exception
     */
    @Test
    public void test_product() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(productController).build();

        this.mock.perform(get("/interior/detail/414")
                .headers(commonHeaders))
                .andDo(print())
                .andExpect(status().isOk());

    }

    /**
     * 회원가입
     * @throws Exception
     */
    @Test
    public void test_registration() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(registrationController).build();

        JSAccount json = new JSAccount();
        json.subs_type = 1;
        json.account_type = 1;
        json.account = "hansol234@gmail.com";
        json.name = "hansolsol";
        json.password = "test@123";
        json.agreement_marketing = 1;

        this.mock.perform(
                post("/registration")
                        .headers(commonHeaders)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(json)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    public class JSAccount {
        public int subs_type;
        public int account_type;
        public String account;
        public String password;
        public String name;
        public int agreement_marketing;
    }
}

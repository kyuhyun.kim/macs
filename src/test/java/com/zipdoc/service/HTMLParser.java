package com.zipdoc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLParser {
	
	private static String html = 
"<p align=\"center\" style=\"text-align: center;\"><sub><br></sub></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub><br></sub></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 14pt; color: rgb(57, 57, 57); font-family: 나눔고딕, 'Nanum Gothic', 굴림, Gulim, sans-serif; line-height: 20.4px;\"><sub>안녕하세요!</sub></span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center; color: rgb(57, 57, 57); font-family: 나눔고딕, 'Nanum Gothic', 굴림, Gulim, sans-serif; line-height: 20.4px;\"><span style=\"font-size: 14pt;\"><sub>집닥 협력업체인 '칸 디자인'입니다.</sub></span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub><span style=\"font-size: 11pt; color: rgb(57, 57, 57); font-family: 나눔고딕, 'Nanum Gothic', 굴림, Gulim, sans-serif; line-height: 20.4px;\">서울시 마포구 성산동<span style=\"font-size: 11pt;\">​</span> 시영아파트 24평 아파트 인테리어 소개해드리겠습니다.</span><br></sub></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub><br></sub></p>\r\n"+
"\r\n"+
"<p align=\"center\" style=\"text-align: center;\">\r\n"+
" <sub>\r\n"+
"   <span style=\"font-size: 11pt;\">전체적으로 화이트톤으로 밝은 느낌을 주게 인테리어 하였습니다^.^</span>\r\n"+
" </sub>\r\n"+
"</p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub><span style=\"font-size: 11pt;\"><br></span></sub></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub><span style=\"font-size: 11pt;\"><br></span></sub></p>\r\n"+
"\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub><span style=\"font-size: 11pt;\">\r\n"+
"<img src=\"http://zipdoc.co.kr/data/editor/1512/6d4005865b34ab48c90d1579668f16ee_1450228792_67.jpg\" title=\"6d4005865b34ab48c90d1579668f16ee_1450228792_67.jpg\">&nbsp;<span id=\"husky_bookmark_start_1450325037763\"></span><span id=\"husky_bookmark_end_1450325037763\"></span></span></sub></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub><span style=\"font-size: 11pt;\"><br></span></sub></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub><span style=\"font-size: 11pt;\">파티션형 욕실입니다.</span></sub></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; line-height: 22px;\">화이트 벽타일과 바닥은 검은색 타일로 시공했습니다.</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; line-height: 22px;\">반다리세면대로 설치했구요. 고객님의 요청에 따라 거울 아래에 선반 하나를 추가했습니다.</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 14.6667px; line-height: 22px;\">&nbsp;</span></p>\r\n"+
"\r\n"+
"\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 14.6667px; line-height: 22px;\"><br></span></div><sub><div align=\"center\" style=\"text-align: center;\">\r\n"+
"<img src=\"http://zipdoc.co.kr/data/editor/1512/6d4005865b34ab48c90d1579668f16ee_1450228792_75.jpg\" title=\"6d4005865b34ab48c90d1579668f16ee_1450228792_75.jpg\" style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\">&nbsp;</span></div><div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\"><br></span></div><div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\">상부장은 화이트 색상으로 설치하여 전체적으로 톤 일치를 시켰습니다.</span></div><div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\">젠다이 설치가 없는 욕실의 경우는 꼭 선반을 추가하시는 걸 추천합니다!</span></div></sub><p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"\r\n"+
"\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 14.6667px; line-height: 22px;\"><br></span></div><sub><div align=\"center\" style=\"text-align: center;\">\r\n"+
"<img src=\"http://zipdoc.co.kr/data/editor/1512/6d4005865b34ab48c90d1579668f16ee_1450228792_83.jpg\" title=\"6d4005865b34ab48c90d1579668f16ee_1450228792_83.jpg\" style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\">&nbsp;</span></div><div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\"><br></span></div><div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\">화이트 색상의 'ㅡ'자 주방입니다.</span></div><div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\">회색의 주방타일을 시공하여 전체적으로 밝은 느낌을 줍니다^^</span></div><div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\">하이라이트 전기쿡탑을 설치하여 깔끔한 조리에 도움이 됩니다~</span></div></sub><p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub><span style=\"font-size: 11pt;\"><br></span></sub></p>\r\n"+
"\r\n"+
"\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<sub><div align=\"center\" style=\"text-align: center;\">\r\n"+
"<img src=\"http://zipdoc.co.kr/data/editor/1512/6d4005865b34ab48c90d1579668f16ee_1450228792_9.jpg\" title=\"6d4005865b34ab48c90d1579668f16ee_1450228792_9.jpg\" style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\">&nbsp;</span></div></sub><p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub><span style=\"font-size: 11pt;\">빌트인 후드와 전기쿡탑입니다</span></sub></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; line-height: 24.4444px;\">후드는 벽장과 높이를 맞춰 라인이 살아있네요!</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 14.6667px; line-height: 24.4444px;\">&nbsp;</span></p>\r\n"+
"\r\n"+
"\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 14.6667px; line-height: 22px;\"><br></span></div><sub><div align=\"center\" style=\"text-align: center;\">\r\n"+
"<img src=\"http://zipdoc.co.kr/data/editor/1512/6d4005865b34ab48c90d1579668f16ee_1450228792_98.jpg\" title=\"6d4005865b34ab48c90d1579668f16ee_1450228792_98.jpg\" style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\">&nbsp;</span></div></sub><p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 11pt;\">&nbsp;</span><span style=\"font-size: 11pt; line-height: 1.5;\">&nbsp;벽지와 몰딩, 프레임, 현관중문도 전체적으로 다 화이트입니다.</span></p>\r\n"+
"<p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 12px; line-height: 1.5;\">&nbsp;</span></p>\r\n"+
"\r\n"+
"\r\n"+
"<p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 12px; line-height: 1.5;\">&nbsp;</span></p>\r\n"+
"<div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 14.6667px; line-height: 22px;\"><br></span></div><sub><div align=\"center\" style=\"text-align: center;\">\r\n"+
"<img src=\"http://zipdoc.co.kr/data/editor/1512/6d4005865b34ab48c90d1579668f16ee_1450228793_12.jpg\" title=\"6d4005865b34ab48c90d1579668f16ee_1450228793_12.jpg\" style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\"><span style=\"font-size: 11pt; vertical-align: sub; line-height: 1.5;\">&nbsp;</span></div></sub><p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 11pt;\">현관장입니다.</span></p>\r\n"+
"<p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 11pt;\">화이트 칼라에 멤브레인으로 제작한 현관장이구요.</span></p>\r\n"+
"<p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 11pt;\">엔틱한 손잡이를 설치해 클래식한 느낌을 줍니다^^&nbsp;</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\">&nbsp;</p>\r\n"+
"<div align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 14.6667px; line-height: 22px;\"><br></span></div><sub><div style=\"text-align: center; font-size: 14.6667px; line-height: 22px;\" align=\"center\"><span style=\"font-size: 11pt; line-height: 20.4px; color: rgb(57, 57, 57); font-family: 나눔고딕, 'Nanum Gothic', 굴림, Gulim, sans-serif;\">여기까지 '칸 디자인'의 서울시 마포구 성산동 시영아파트 인테리어 현장이었습니다.</span></div></sub><p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<p style=\"text-align: center; color: rgb(57, 57, 57); font-family: 나눔고딕, 'Nanum Gothic', 굴림, Gulim, sans-serif; line-height: 20.4px;\" align=\"center\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center; color: rgb(57, 57, 57); font-family: 나눔고딕, 'Nanum Gothic', 굴림, Gulim, sans-serif; line-height: 20.4px;\"><span style=\"font-size: 11pt;\">깔끔한 화이트 인테리어가 맘에 드시는 지요~</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center; color: rgb(57, 57, 57); font-family: 나눔고딕, 'Nanum Gothic', 굴림, Gulim, sans-serif; line-height: 20.4px;\"><span style=\"font-size: 11pt; line-height: 20.4px;\">&nbsp;</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center; color: rgb(57, 57, 57); font-family: 나눔고딕, 'Nanum Gothic', 굴림, Gulim, sans-serif; line-height: 20.4px;\"><span style=\"font-size: 11pt;\">집닥 파트너사 '칸 디자인'에 믿고 맡겨 주세요!</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub></sub></p>\r\n"+
"<div style=\"text-align: center; font-size: 14.6667px; line-height: 22px;\" align=\"center\"><span style=\"font-size: 11pt; color: rgb(57, 57, 57); font-family: 나눔고딕, 'Nanum Gothic', 굴림, Gulim, sans-serif; line-height: 20.4px;\">&nbsp;</span><br></div><p style=\"text-align: center; \" align=\"center\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt;\">&nbsp;</span></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub style=\"line-height: 22px; font-size: 14.6667px;\"><br></sub></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><sub style=\"line-height: 22px; font-size: 14.6667px;\"><br></sub></p>\r\n"+
"<p align=\"center\" style=\"text-align: center;\"><span style=\"font-size: 11pt; color: rgb(57, 57, 57); font-family: 나눔고딕, 'Nanum Gothic', 굴림, Gulim, sans-serif; line-height: 20.4px;\">&nbsp;</span></p>";


    public static void main(String[] args) {
    	
//    	String pattern = "<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>";
//    	//String patterm = "<img[^>]*src=[\"']?([^>\"']+)[\"']?[^>]*>";
//    	
//    	Pattern urlPattern = Pattern.compile(pattern);
//    	
//		StringBuffer out = new StringBuffer();
//		
//		Matcher matcher = urlPattern.matcher(html);
//		
//		matcher.r
//		
////		System.out.println(matcher.toString());
//		
//		while (matcher.find()) {
//		
//			for (int i = 0; i < matcher.groupCount(); i++) {
//				System.out.println("["+i+"] ["+matcher.group(i)+"]");
//			}
//			//matcher.
//			//String temp = matcher.group(0);
//			
//			// 목록 추가
//			//System.out.println(": "+temp);
//		}
//		
//		matcher.appendTail(out);
    }
}

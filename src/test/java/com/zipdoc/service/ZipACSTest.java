package com.zipdoc.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.zipdoc.acs.controller.ArticleController;
import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.domain.entity.AppVersion;
import com.zipdoc.acs.domain.entity.Article;
import com.zipdoc.acs.domain.service.AppVersionService;
import com.zipdoc.acs.domain.service.ArticleService;
import com.zipdoc.acs.domain.service.CommentService;
import com.zipdoc.acs.persistence.dao.AppVersionDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author akm
 * JUnit Test 코드 작성 : *Controller, *Service, *Dao
 * context.test 하위 파일들을 참조하여 scan 을 하지 않고 필요한 Bean 만 등록-Autowired 하여 빈 주입하여 사용
 */
public class ZipACSTest extends AbstractApplicationContextTest{

	private static final Logger log = LoggerFactory.getLogger(ZipACSTest.class);

	@Autowired
	private ArticleService articleService;

	/*
	 * @Test 어노테이션 표기시 테스트에 포함
	 * @Autowired bean 생성 테스트 ( not null test )
	 */
	@Test
	public void test_AppVersionDao_test1() {

		// 예제 1. Junit 테스트 창에 나타남, 3 > 2 가 true 인지 체크
		assertTrue(3 > 2);

		// 여기 까지 실행시 테스트 통과 완료
		log.info("test complete!!  " );

        Article article = articleService.selectArticle(ArticleType.INTERIOR_TIP, 90);
        assertNotNull(article);

	}

	/*
	 * 필요한 TEST 계속 추가 가능
	 */
	@Test
	public void test_AppVersionDao_test2() {

		log.info("test complete!!  " );
	}

	/*
	 *  필요한 TEST 계속 추가 가능, articleService.selectArticle 메소드 테스트
	 */
	@Test
	public void test_AppVersionDao_test3() {

		Article article = articleService.selectArticle(ArticleType.NOTICE, 8);

		log.info("article.getSubject() "  + article.getSubject() );
		log.info("article.getContents() "  + article.getContents() );
		log.info("article.getCno() "  + article.getCno() );

		log.info("test complete!!  " );
	}

}

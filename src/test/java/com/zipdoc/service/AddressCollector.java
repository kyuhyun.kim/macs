package com.zipdoc.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zipdoc.acs.utils.Json;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.springframework.http.HttpStatus;


/**
 * Created by ZIPDOC on 2016-05-24.
 */
public class AddressCollector {

    int maxPage = 500;

    private String url1 = "http://map.naver.com/search2/local.nhn?query=%EA%B0%95%EC%9B%90%EB%8F%84+%EA%B0%95%EB%A6%89%EC%8B%9C+%EC%9D%B8%ED%85%8C%EB%A6%AC%EC%96%B4&type=SITE_1&queryRank=1&re=1&siteSort=0&menu=location&searchCoord=127.9455822%3B37.3373821&sm=clk&mpx=01150615%3A37.742405%2C128.9127877%3AZ7%3A0.3014557%2C0.2476024";
    public static final String LINE = "\r\n";

    protected int resultCode;
    protected String resultDescription;
    private String resultBody;

    private Map<String, String> headerAttr = new HashMap<String, String>();

    private String header = "업체명,전화번호,업종,시도,군구,주소,도로명주소,홈페이지" + LINE;
    private FileWriter fileWriter;

    private boolean runningFlag = true;


    private void execute() throws IOException {
        fileWriter = new FileWriter(new File("D://TEMP/address.csv"));

        fileWriter.append(header);

        for(int i= 1; i <= maxPage; i++){
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            collect(i);
            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println("total : " + maxPage + ", current : " + i + ", remain : " + (maxPage - i) + ", pecent : " + ((i/maxPage)*100));
            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

            if(!runningFlag) break;
        }
       fileWriter.close();
    }


    private void collect(int page) {
        HttpClient client = new DefaultHttpClient();

        HttpParams params = client.getParams();
        HttpConnectionParams.setConnectionTimeout(params, 3000);
        HttpConnectionParams.setSoTimeout(params, 20000);

        String invokeUrl = url1 + "&page=" + page;

        try{
            HttpGet httpget = new HttpGet(invokeUrl);
            if( headerAttr.size() != 0 ){
                String[] names = headerAttr.keySet().toArray(new String[0]);
                for(String name : names){
                    httpget.setHeader(name, headerAttr.get(name));
                }
            }

            Header[] headers = httpget.getAllHeaders();
            if( headers != null && headers.length != 0 ){
                for(Header header : headers){
                }
            }

            HttpResponse response = null;
            Map<String, Object> res = null;
            try {
                response = client.execute(httpget);
                res = treatResult(response);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            if(res == null) return;

            Map<String, Object> resultMap = (Map<String, Object>)res.get("result");
            Map<String, Object> siteMap = (Map<String, Object>)resultMap.get("site");
            List<Map<String, Object>> list = (List<Map<String, Object>>)siteMap.get("list");

            if(list == null || list.size()==0) runningFlag = false;

            for(Map map : list) {
                StringBuilder sb = new StringBuilder();
                List<String> category = (List<String>)map.get("category");
                if(category != null && category.size() >= 2 && StringUtils.equals(category.get(0), "전문디자인") && StringUtils.equals(category.get(1), "인테리어디자인")){
                    sb.append((String)map.get("name")).append(",");
                    sb.append((String)map.get("tel")).append(",");

                    sb.append(category.get(0) + ">" + category.get(1)).append(",");

                    String[] address;
                    if(StringUtils.isNotEmpty((String)map.get("roadAddress"))) {
                        address = StringUtils.split((String) map.get("roadAddress"), " ");
                    } else {
                        address = StringUtils.split((String) map.get("address"), " ");
                    }

                    if(address.length > 2){
                        sb.append(address[0]).append(",");
                        sb.append(address[1]).append(",");
                    } else {
                        sb.append(",,");
                    }
                    sb.append((String)map.get("address")).append(",");
                    sb.append((String)map.get("roadAddress")).append(",");
                    sb.append((String)map.get("homePage")).append(LINE);
                    System.out.println(sb.toString());
                    fileWriter.append(sb.toString());
                    fileWriter.flush();
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
            resultCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
            resultDescription = "cannt interwork, "+e.getMessage();
        }
        finally{
            try{
                client.getConnectionManager().shutdown();
            }
            catch(Exception e){}
        }
    }

    private HashMap<String, Object> treatResult( HttpResponse response ) throws Exception {
        if( response != null && response.getStatusLine() != null ){
            StatusLine statusLine = response.getStatusLine();
            resultDescription = statusLine.getReasonPhrase();
            resultCode = statusLine.getStatusCode();
            HttpEntity responseEntity = response.getEntity();
            if (responseEntity != null) {
                StringBuffer sb = new StringBuffer();
                InputStream instream = responseEntity.getContent();
                int l = 0;
                byte[] tmp = new byte[2048];
                while ((l = instream.read(tmp, 0, tmp.length)) != -1) {
                    sb.append(new String(tmp, 0,l));
                }
                resultBody = sb.toString();
            }

            HashMap<String, Object> resultMap = (HashMap) Json.toObjectJson(resultBody, HashMap.class);
            return resultMap;
        }
        else {
            return null;
        }
    }

    public static void main(String [] args) {
        AddressCollector collector = new AddressCollector();
        try {
            collector.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.zipdoc.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.zipdoc.acs.controller.CommonController;
import com.zipdoc.acs.controller.EstimateController;
import com.zipdoc.acs.controller.PartnerController;
import com.zipdoc.acs.controller.subs.LoginController;
import com.zipdoc.acs.model.ListRequest;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ACSCmsTest extends AbstractApplicationContextTestForReal {

    private static final Logger log = LoggerFactory.getLogger(ACSCmsTest.class);

    /**
     * 계정 정보
     * aws.zipdocdev.com(개발서버)
     * hansolzzang / zipdoc@123
     */
    private static final String APP_VERSION = "1.0.0";
    private static final String MEMBER_NO = "519";
    private static final String ZID = "8263";
    private static final String ZID_KEY = "HUwRlnmfxRgOkLQh";
    private static final String OS_TYPE = "android";
    private static final String OS_VERSION = "6.2.0";
    private static final String DEVICE_ID = "353166061903508";
    private static final String DEVICE_MODEL = "SM-G930L";
    private static final String MDN = "353166061903508";
    private static final String TOKEN = "fTM9CCRBxLs:APA91bFoPUNNrM4NCHyk5rLVejeE9XHt20ylrZQd7R4hxD2Vt7G_qtAsxFAW3oTrDfs3Z01iqzrVCT4KpS4I3JQfZ-q73rywzmgH11bApHzAjQ6ItrlRFjPMQRmu7QK6srJI6msB5HhD";

    private HttpHeaders commonHeaders;

    private MockMvc mock;

    @Autowired
    private LoginController loginController;

    @Autowired
    private EstimateController estimateController;

    @Autowired
    private PartnerController partnerController;

    @Autowired
    private CommonController commonController;

    @Before
    public void setup() {
        this.commonHeaders = new HttpHeaders();
        this.commonHeaders.add("Accept-Encoding", "gzip");
        this.commonHeaders.add("APP-VERSION", APP_VERSION);
        this.commonHeaders.add("MEMBER-NO", MEMBER_NO);
        this.commonHeaders.add("ZID", ZID);
        this.commonHeaders.add("ZID-KEY", ZID_KEY);
    }

    @Test
    public void test_init() {
        assertNotNull(commonHeaders);

        assertNotNull(loginController);
        assertNotNull(estimateController);
        assertNotNull(partnerController);
    }

    /**
     * cms 정상 로그인
     *
     * @throws Exception
     */
//    @Test
//    public void test_login_success() throws Exception {
//
//        this.mock = MockMvcBuilders.standaloneSetup(loginController).build();
//
//        JSAccount json = new JSAccount();
//        json.account_type = 1; //zipdoc
//        json.account = "hansolzzang";
//        json.password = "zipdoc@123";
//        json.name = "";
//
//        System.out.println(asJsonString(json));
//
//        this.mock.perform(
//                post("/cms/login")
//                        .headers(commonHeaders)
//                        .param("os_type", OS_TYPE)
//                        .param("os_version", OS_VERSION)
//                        .param("device_id", DEVICE_ID)
//                        .param("device_model", DEVICE_MODEL)
//                        .param("mdn", MDN)
//                        .param("token", TOKEN)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(asJsonString(json)))
//                .andDo(print())
//                .andExpect(status().isOk());
//
//    }

    /**
     * cms 로그인
     * 파트너스의 권한을 가진 계정으로 로그인할 경우 403에러
     *
     * @throws Exception
     */
//    @Test
//    public void test_login_auth() throws Exception {
//
//        this.mock = MockMvcBuilders.standaloneSetup(loginController).build();
//
//        JSAccount json = new JSAccount();
//        json.account_type = 1; //zipdoc
//        json.account = "hsjang"; //파트너스 계정
//        json.password = "hsjang";
//        json.name = "";
//
//        System.out.println(asJsonString(json));
//
//        this.mock.perform(
//                post("/cms/login")
//                        .headers(commonHeaders)
//                        .param("os_type", OS_TYPE)
//                        .param("os_version", OS_VERSION)
//                        .param("device_id", DEVICE_ID)
//                        .param("device_model", DEVICE_MODEL)
//                        .param("mdn", MDN)
//                        .param("token", TOKEN)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(asJsonString(json)))
//                .andDo(print())
//                .andExpect(status().is(403)); //인증에러
//    }

    /**
     * 견적 리스트 조회.
     * 생성일 기준 최신순 20개씩
     *
     * @throws Exception
     */
    @Test
    public void test_estimates() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(estimateController).build();

        this.mock.perform(
                get("/estimates/0/20"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 파트너스 리스트 조회 (검색조건없음)
     * 키워드로 주소, 파트너명 검색 가능
     *
     * @throws Exception
     */
    @Test
    public void test_partners() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(partnerController).build();

        this.mock.perform(
                post("/partners/0/20"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 파트너스 리스트 조회 (검색조건있음)
     * 키워드로 주소, 파트너명 검색 가능
     *
     * @throws Exception
     */
    @Test
    public void test_partners_keyword() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(partnerController).build();

        ListRequest json = new ListRequest();
        json.setKeyword("서울");

        this.mock.perform(
                post("/partners/0/20")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(json)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 파트너스 상세 조회
     * @throws Exception
     */
    @Test
    public void test_partners_detail() throws Exception {

        this.mock = MockMvcBuilders.standaloneSetup(partnerController).build();

        this.mock.perform(
                get("/partners/74"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * cms용 서비스 정보 조회
     * @throws Exception
     */
    @Test
    public void test_serviceinfo() throws Exception {
        this.mock = MockMvcBuilders.standaloneSetup(commonController).build();

        this.mock.perform(
                get("/cms/serviceinfo"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 파트너스 통계(전체)
     * @throws Exception
     */
    @Test
    public void test_partner_stats() throws Exception {
        this.mock = MockMvcBuilders.standaloneSetup(partnerController).build();

        this.mock.perform(
                post("/partners/stats"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 파트너스 통계(서울)
     * @throws Exception
     */
    @Test
    public void test_partner_stats_seoul() throws Exception {
        this.mock = MockMvcBuilders.standaloneSetup(partnerController).build();

        ListRequest json = new ListRequest();
        json.setKeyword("서울");

        this.mock.perform(
                post("/partners/stats")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(json)))
                .andDo(print())
                .andExpect(status().isOk());
    }


    /**
     * json Obj.
     */

    public class JSAccount {
        public int account_type;
        public String account;
        public String password;
        public String name;
    }
}

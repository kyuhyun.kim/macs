package com.zipdoc.service;

import com.zipdoc.acs.controller.ArticleController;
import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.domain.entity.AppVersion;
import com.zipdoc.acs.domain.entity.Article;
import com.zipdoc.acs.domain.service.AppVersionService;
import com.zipdoc.acs.domain.service.ArticleService;
import com.zipdoc.acs.domain.service.CommentService;
import com.zipdoc.acs.persistence.dao.AppVersionDao;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ZipACSRealTest extends AbstractApplicationContextTestForReal{

    private static final Logger log = LoggerFactory.getLogger(ZipACSTest.class);

    /*
     * controller, dao, service TEST 예제
     * *Controller, *Service, *Dao 등록, @Autowired 필수
     */
    @Autowired
    private ArticleController articleController;

    @Autowired
    private AppVersionDao appVersionDao;

    @Autowired
    private AppVersionService appVersionService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private CommentService commentService;

    /*
     * @Test 어노테이션 표기시 테스트에 포함
     * @Autowired bean 생성 테스트 ( not null test )
     */
    @Test
    public void test_AppVersionDao_test1() {

        // 예제 1. Junit 테스트 창에 나타남, 3 > 2 가 true 인지 체크
        assertTrue(3 > 2);

        // @Autowired bean 생성 확인, not null 인지 체크, null 이 아닐 경우 TEST 통과
        assertNotNull(articleController);
        assertNotNull(appVersionDao);

        assertNotNull(appVersionService);
        assertNotNull(articleService);
        assertNotNull(commentService);

        // 여기 까지 실행시 테스트 통과 완료
        log.info("test complete!!  " );
    }

    /*
     * 필요한 TEST 계속 추가 가능
     */
    @Test
    public void test_AppVersionDao_test2() {

		/*
		 *  appVersionService 테스트 , 실제 구현은 AppVersionServiceImpl 에 있음,
		 *  파라미터는 android 앱에서 "android", "google" 호출 ( AuthenticationController - 에서 사용 )
		 *  DB 접근 후 AppVersion appVer 객체 생성
		 */
        AppVersion appVer = appVersionService.selectVersion("android", "google");

        // appVer 이 null 인지 체크
        assertNotNull(appVer);

        log.info( " appVer.getApp_market() : " +  appVer.getApp_market() );
        log.info( " appVer.getApp_version() " +  appVer.getApp_version() );

        log.info("test complete!!  " );
    }

    /*
     *  필요한 TEST 계속 추가 가능, articleService.selectArticle 메소드 테스트
     */
    @Test
    public void test_AppVersionDao_test3() {

        Article article = articleService.selectArticle(ArticleType.NOTICE, 8);

        log.info("article.getSubject() "  + article.getSubject() );
        log.info("article.getContents() "  + article.getContents() );
        log.info("article.getCno() "  + article.getCno() );

        log.info("test complete!!  " );
    }

}

package com.zipdoc.service;

import com.zipdoc.acs.domain.service.PartnerService;
import com.zipdoc.acs.model.PartnerProductStats;
import com.zipdoc.acs.model.PartnerStatsResponse;
import com.zipdoc.acs.persistence.dao.PartnerDao;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

public class ACSCmsModuleTest extends AbstractApplicationContextTest {

    private static final Logger log = LoggerFactory.getLogger(ACSCmsModuleTest.class);

    @Autowired
    PartnerDao partnerDao;

    @Before
    public void setup() {
        assertNotNull(partnerDao);
    }

    @Test
    public void test_partnerDao_evaluation() {

        PartnerStatsResponse result = partnerDao.selectPartnerEvaluationStats(null);
        assertTrue(result.getTotal_count() > 0);

    }

    @Test
    public void test_partnerDao_contracts() {

        PartnerStatsResponse result = partnerDao.selectPartnerContractStats(null);
        assertTrue(result.getTotal_contract_count() > 0);

    }

    @Test
    public void test_partnerDao_products() {

        List<PartnerProductStats> products = partnerDao.selectPartnerProductsRanks(null);
        assertNotNull(products);

    }

    @Test
    public void test() {

        PartnerStatsResponse result = new PartnerStatsResponse();

        for(int i = 0; i < 1000; i++) {
            result = partnerDao.selectPartnerEvaluationStats(null);
            partnerDao.selectPartnerContractStats(null);
            partnerDao.selectPartnerProductsRanks(null);
        }

        log.info("total_count : " + result.getTotal_count());
    }
}

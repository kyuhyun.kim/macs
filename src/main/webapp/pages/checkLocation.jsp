<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>현재 나의 위치는</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"  content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            getLocation();
        });

        function getLocation()
        {
            url = '';
            var data = {};
            isMobileOS = isMobileOS();

            if(isMobileOS === true) {
                url = 'checkLocationByCoords';
                if(navigator) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        data = {longitude: position.coords.longitude, latitude: position.coords.latitude};
                        getLocationAJAX(url, data);
                    });
                } else {
                    alert("위치정보를 가져올 수 없습니다. 부정확할 수 있습니다.");
                }
                return;
            }
            url = 'checkLocationByIP';
            getLocationAJAX(url, data);
        }

        function getLocationAJAX(url, data)
        {
            $.ajax({
                url: url,
                method : 'POST',
                cache : true,
                dataType : "json",
                data: data,
                success: function (data) {
                    if(data.geoLocation == "unknown") {
                        content = "알 수 없는 곳(* 위치정보 가져오기실패)";
                    } else {
                        content = data.geoLocation.sido + " " + data.geoLocation.sigugun + " " + data.geoLocation.dongmyun;
                    }
                    $("#whereami").html("현재 나의 위치는 " + content + "입니다.");
                },
                error : function(data) {
                }
            });
        }

        function isMobileOS()
        {
            if (navigator.userAgent.match(/iPad/i)){
                return true;
            }
            if (navigator.userAgent.match(/Tablet/i)){
                return true;
            }
            if(navigator.userAgent.match(/Android/i)){
                return true;
            }
            if(navigator.userAgent.match(/iPhone|iPod/i)){
                return true;
            }
            if(navigator.userAgent.match(/BlackBerry/i)){
                return true;
            }
            if(navigator.userAgent.match(/Windows CE/i)){
                return true;
            }
            if(navigator.userAgent.match(/Nokia/i)){
                return true;
            }
            if(navigator.userAgent.match(/Webos/i)){
                return true;
            }
            if(navigator.userAgent.match(/Opera Mini/i)){
                return true;
            }
            if(navigator.userAgent.match(/Opera Mobi/i)){
                return true;
            }
            if(navigator.userAgent.match(/IEMobild/i)){
                return true;
            }
            return false;
        }
    </script>
</head>
<body>
<p id="whereami"></p>
</body>
</html>
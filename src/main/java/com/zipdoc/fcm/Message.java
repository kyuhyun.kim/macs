package com.zipdoc.fcm;

/**
 * Created by ZIPDOC on 2016-10-07.
 */
import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public final class Message implements Serializable {
    private final String collapseKey;
    private final Boolean delayWhileIdle;
    private final Boolean contentAvailable;
    private final Integer timeToLive;
    private final String priority;
    private final Map<String, Object> data;
    private final Map<String, Object> notification;

    private Message(Message.Builder builder) {
        this.collapseKey = builder.collapseKey;
        this.delayWhileIdle = builder.delayWhileIdle;
        this.contentAvailable = builder.contentAvailable;
        this.priority = builder.priority;
        this.data = Collections.unmodifiableMap(builder.data);
        this.notification = Collections.unmodifiableMap(builder.notification);
        this.timeToLive = builder.timeToLive;
    }

    public String getCollapseKey() {
        return this.collapseKey;
    }

    public Boolean isDelayWhileIdle() {
        return this.delayWhileIdle;
    }

    public Boolean isContentAvaliable() {
        return this.contentAvailable;
    }

    public Integer getTimeToLive() {
        return this.timeToLive;
    }

    public String getPriority() {
        return this.priority;
    }

    public Map<String, Object> getData() {
        return this.data;
    }

    public Map<String, Object> getNotification() {
        return notification;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("Message(");
        if(this.collapseKey != null) {
            builder.append("collapseKey=").append(this.collapseKey).append(", ");
        }

        if(this.timeToLive != null) {
            builder.append("timeToLive=").append(this.timeToLive).append(", ");
        }

        if(this.delayWhileIdle != null) {
            builder.append("delayWhileIdle=").append(this.delayWhileIdle).append(", ");
        }

        if(this.contentAvailable != null) {
            builder.append("content_available=").append(this.contentAvailable).append(", ");
        }

        if(this.priority != null) {
            builder.append("priority=").append(this.priority).append(", ");
        }

        if(!this.notification.isEmpty()){
            builder.append("notification: {");
            Iterator i$ = this.notification.entrySet().iterator();

            while(i$.hasNext()) {
                Entry entry = (Entry)i$.next();
                builder.append((String)entry.getKey()).append("=").append(entry.getValue()).append(",");
            }

            builder.delete(builder.length() - 1, builder.length());
            builder.append("}");
        }

        if(!this.data.isEmpty()) {
            builder.append("data: {");
            Iterator i$ = this.data.entrySet().iterator();

            while(i$.hasNext()) {
                Entry entry = (Entry)i$.next();
                builder.append((String)entry.getKey()).append("=").append(entry.getValue()).append(",");
            }

            builder.delete(builder.length() - 1, builder.length());
            builder.append("}");
        }

        if(builder.charAt(builder.length() - 1) == 32) {
            builder.delete(builder.length() - 2, builder.length());
        }

        builder.append(")");
        return builder.toString();
    }

    public static final class Builder {
        private final Map<String, Object> data = new LinkedHashMap();
        private final Map<String, Object> notification = new LinkedHashMap();
        private String collapseKey;
        private Boolean delayWhileIdle;
        private Boolean contentAvailable;
        private Integer timeToLive;
        private String priority = PRIORITY_HIGH;

        public static final String PRIORITY_NORMAL = "normal";
        public static final String PRIORITY_HIGH = "high";

        public Builder() {
        }

        public Message.Builder collapseKey(String value) {
            this.collapseKey = value;
            return this;
        }

        public Message.Builder delayWhileIdle(boolean value) {
            this.delayWhileIdle = Boolean.valueOf(value);
            return this;
        }

        public Message.Builder contentAvailable(boolean value) {
            this.contentAvailable = Boolean.valueOf(value);
            return this;
        }

        public Message.Builder timeToLive(int value) {
            this.timeToLive = Integer.valueOf(value);
            return this;
        }

        public Message.Builder priority(String value) {
            this.priority = value;
            return this;
        }

        public Message.Builder addData(String key, Object value) {
            this.data.put(key, value);
            return this;
        }

        public Message.Builder addNotification(String key, Object value) {
            this.notification.put(key, value);
            return this;
        }

        public Message build() {
            return new Message(this);
        }
    }
}

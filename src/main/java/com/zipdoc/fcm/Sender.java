package com.zipdoc.fcm;

/**
 * Created by ZIPDOC on 2016-10-07.
 */

import com.zipdoc.fcm.Result.Builder;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Sender {
    protected static final String UTF8 = "UTF-8";
    protected static final int BACKOFF_INITIAL_DELAY = 1000;
    protected static final int MAX_BACKOFF_DELAY = 1024000;
    protected final Random random = new Random();
    protected final Logger logger = Logger.getLogger(this.getClass().getName());
    private final String key;

    public Sender(String key) {
        this.key = (String)nonNull(key);
    }

    public Result send(Message message, String registrationId, int retries) throws IOException {
        int attempt = 0;
        Result result = null;
        int backoff = 1000;

        boolean tryAgain;
        do {
            ++attempt;
            if(this.logger.isLoggable(Level.FINE)) {
                this.logger.fine("Attempt #" + attempt + " to send message " + message + " to regIds " + registrationId);
            }

            result = this.sendNoRetry(message, registrationId);
            tryAgain = result == null && attempt <= retries;
            if(tryAgain) {
                int sleepTime = backoff / 2 + this.random.nextInt(backoff);
                this.sleep((long)sleepTime);
                if(2 * backoff < 1024000) {
                    backoff *= 2;
                }
            }
        } while(tryAgain);

        if(result == null) {
            throw new IOException("Could not send message after " + attempt + " attempts");
        } else {
            return result;
        }
    }

    public Result sendNoRetry(Message message, String registrationId) throws IOException {
        StringBuilder body = newBody("registration_id", registrationId);
        Boolean delayWhileIdle = message.isDelayWhileIdle();
        if(delayWhileIdle != null) {
            addParameter(body, "delay_while_idle", delayWhileIdle.booleanValue()?"1":"0");
        }

        String collapseKey = message.getCollapseKey();
        if(collapseKey != null) {
            addParameter(body, "collapse_key", collapseKey);
        }

        Integer timeToLive = message.getTimeToLive();
        if(timeToLive != null) {
            addParameter(body, "time_to_live", Integer.toString(timeToLive.intValue()));
        }

        String priority = message.getPriority();
        if(priority != null) {
            addParameter(body, "priority", priority);
        }

        Iterator requestNotification = message.getNotification().entrySet().iterator();

        while(requestNotification.hasNext()) {
            Entry conn = (Entry)requestNotification.next();
            String status = "notification." + (String)conn.getKey();
            String reader = (String)conn.getValue();
            addParameter(body, status, URLEncoder.encode(reader, "UTF-8"));
        }

        Iterator requestBody = message.getData().entrySet().iterator();

        while(requestBody.hasNext()) {
            Entry conn = (Entry)requestBody.next();
            String status = "data." + (String)conn.getKey();
            String reader = (String)conn.getValue();
            addParameter(body, status, URLEncoder.encode(reader, "UTF-8"));
        }

        String requestBody1 = body.toString();
        this.logger.finest("Request body: " + requestBody1);
        HttpURLConnection conn1 = this.post("https://fcm.googleapis.com/fcm/send", requestBody1);
        int status1 = conn1.getResponseCode();
        if(status1 == 503) {
            this.logger.fine("GCM service is unavailable");
            return null;
        } else if(status1 != 200) {
            throw new InvalidRequestException(status1);
        } else {
            try {
                BufferedReader reader1 = new BufferedReader(new InputStreamReader(conn1.getInputStream()));

                try {
                    String line = reader1.readLine();
                    if(line != null && !line.equals("")) {
                        String[] responseParts = this.split(line);
                        String token = responseParts[0];
                        String value = responseParts[1];
                        Object builder;
                        if(token.equals("id")) {
                            builder = (new Builder()).messageId(value);
                            line = reader1.readLine();
                            if(line != null) {
                                responseParts = this.split(line);
                                token = responseParts[0];
                                value = responseParts[1];
                                if(token.equals("registration_id")) {
                                    ((Builder)builder).canonicalRegistrationId(value);
                                } else {
                                    this.logger.warning("Received invalid second line from GCM: " + line);
                                }
                            }

                            Result result = ((Builder)builder).build();
                            if(this.logger.isLoggable(Level.FINE)) {
                                this.logger.fine("Message created succesfully (" + result + ")");
                            }

                            Result var17 = result;
                            return var17;
                        } else if(token.equals("Error")) {
                            builder = (new Builder()).errorCode(value).build();
                            return (Result)builder;
                        } else {
                            throw new IOException("Received invalid response from GCM: " + line);
                        }
                    } else {
                        throw new IOException("Received empty response from GCM service.");
                    }
                } finally {
                    reader1.close();
                }
            } finally {
                conn1.disconnect();
            }
        }
    }

    public MulticastResult send(Message message, List<String> regIds, int retries) throws IOException {
        int attempt = 0;
        MulticastResult multicastResult = null;
        int backoff = 1000;
        HashMap results = new HashMap();
        Object unsentRegIds = new ArrayList(regIds);
        ArrayList multicastIds = new ArrayList();

        boolean tryAgain;
        int canonicalIds;
        do {
            ++attempt;
            if(this.logger.isLoggable(Level.FINE)) {
                this.logger.fine("Attempt #" + attempt + " to send message " + message + " to regIds " + unsentRegIds);
            }

            multicastResult = this.sendNoRetry(message, (List)unsentRegIds);
            long success = multicastResult.getMulticastId();
            this.logger.fine("multicast_id on attempt # " + attempt + ": " + success);
            multicastIds.add(Long.valueOf(success));
            unsentRegIds = this.updateStatus((List)unsentRegIds, results, multicastResult);
            tryAgain = !((List)unsentRegIds).isEmpty() && attempt <= retries;
            if(tryAgain) {
                canonicalIds = backoff / 2 + this.random.nextInt(backoff);
                this.sleep((long)canonicalIds);
                if(2 * backoff < 1024000) {
                    backoff *= 2;
                }
            }
        } while(tryAgain);

        int var20 = 0;
        int failure = 0;
        canonicalIds = 0;
        Iterator multicastId = results.values().iterator();

        while(multicastId.hasNext()) {
            Result result = (Result)multicastId.next();
            if(result.getMessageId() != null) {
                ++var20;
                if(result.getCanonicalRegistrationId() != null) {
                    ++canonicalIds;
                }
            } else {
                ++failure;
            }
        }

        long var21 = ((Long)multicastIds.remove(0)).longValue();
        com.zipdoc.fcm.MulticastResult.Builder builder = (new com.zipdoc.fcm.MulticastResult.Builder(var20, failure, canonicalIds, var21)).retryMulticastIds(multicastIds);
        Iterator i$ = regIds.iterator();

        while(i$.hasNext()) {
            String regId = (String)i$.next();
            Result result1 = (Result)results.get(regId);
            builder.addResult(result1);
        }

        return builder.build();
    }

    private List<String> updateStatus(List<String> unsentRegIds, Map<String, Result> allResults, MulticastResult multicastResult) {
        List results = multicastResult.getResults();
        if(results.size() != unsentRegIds.size()) {
            throw new RuntimeException("Internal error: sizes do not match. currentResults: " + results + "; unsentRegIds: " + unsentRegIds);
        } else {
            ArrayList newUnsentRegIds = new ArrayList();

            for(int i = 0; i < unsentRegIds.size(); ++i) {
                String regId = (String)unsentRegIds.get(i);
                Result result = (Result)results.get(i);
                allResults.put(regId, result);
                String error = result.getErrorCodeName();
                if(error != null && error.equals("Unavailable")) {
                    newUnsentRegIds.add(regId);
                }
            }

            return newUnsentRegIds;
        }
    }

    public MulticastResult sendNoRetry(Message message, List<String> registrationIds) throws IOException {
        if(((List)nonNull(registrationIds)).isEmpty()) {
            throw new IllegalArgumentException("registrationIds cannot be empty");
        } else {
            HashMap jsonRequest = new HashMap();
            this.setJsonField(jsonRequest, "time_to_live", message.getTimeToLive());
            this.setJsonField(jsonRequest, "collapse_key", message.getCollapseKey());
            this.setJsonField(jsonRequest, "delay_while_idle", message.isDelayWhileIdle());
            this.setJsonField(jsonRequest, "content_available", message.isContentAvaliable());
            this.setJsonField(jsonRequest, "priority", message.getPriority());
            jsonRequest.put("registration_ids", registrationIds);

            Map notification = message.getNotification();
            if(!notification.isEmpty()){
                jsonRequest.put("notification", notification);
            }

            Map payload = message.getData();
            if(!payload.isEmpty()) {
                jsonRequest.put("data", payload);
            }

            String requestBody = JSONValue.toJSONString(jsonRequest);
            this.logger.finest("JSON request: " + requestBody);
            HttpURLConnection conn = this.post("https://fcm.googleapis.com/fcm/send", "application/json", requestBody);
            int status = conn.getResponseCode();
            String responseBody;
            if(status != 200) {
                responseBody = getString(conn.getErrorStream());
                this.logger.finest("JSON error response: " + responseBody);
                throw new InvalidRequestException(status, responseBody);
            } else {
                responseBody = getString(conn.getInputStream());
                this.logger.finest("JSON response: " + responseBody);
                JSONParser parser = new JSONParser();

                try {
                    JSONObject jsonResponse = (JSONObject)parser.parse(responseBody);
                    int e = this.getNumber(jsonResponse, "success").intValue();
                    int failure = this.getNumber(jsonResponse, "failure").intValue();
                    int canonicalIds = this.getNumber(jsonResponse, "canonical_ids").intValue();
                    long multicastId = this.getNumber(jsonResponse, "multicast_id").longValue();
                    com.zipdoc.fcm.MulticastResult.Builder builder = new com.zipdoc.fcm.MulticastResult.Builder(e, failure, canonicalIds, multicastId);
                    List results = (List)jsonResponse.get("results");
                    if(results != null) {
                        Iterator multicastResult = results.iterator();

                        while(multicastResult.hasNext()) {
                            Map jsonResult = (Map)multicastResult.next();
                            String messageId = (String)jsonResult.get("message_id");
                            String canonicalRegId = (String)jsonResult.get("registration_id");
                            String error = (String)jsonResult.get("error");
                            Result result = (new Builder()).messageId(messageId).canonicalRegistrationId(canonicalRegId).errorCode(error).build();
                            builder.addResult(result);
                        }
                    }

                    MulticastResult multicastResult1 = builder.build();
                    return multicastResult1;
                } catch (ParseException var24) {
                    throw this.newIoException(responseBody, var24);
                } catch (Sender.CustomParserException var25) {
                    throw this.newIoException(responseBody, var25);
                }
            }
        }
    }

    private IOException newIoException(String responseBody, Exception e) {
        String msg = "Error parsing JSON response (" + responseBody + ")";
        this.logger.log(Level.WARNING, msg, e);
        return new IOException(msg + ":" + e);
    }

    private void setJsonField(Map<Object, Object> json, String field, Object value) {
        if(value != null) {
            json.put(field, value);
        }

    }

    private Number getNumber(Map<?, ?> json, String field) {
        Object value = json.get(field);
        if(value == null) {
            throw new Sender.CustomParserException("Missing field: " + field);
        } else if(!(value instanceof Number)) {
            throw new Sender.CustomParserException("Field " + field + " does not contain a number: " + value);
        } else {
            return (Number)value;
        }
    }

    private String[] split(String line) throws IOException {
        String[] split = line.split("=", 2);
        if(split.length != 2) {
            throw new IOException("Received invalid response line from GCM: " + line);
        } else {
            return split;
        }
    }

    protected HttpURLConnection post(String url, String body) throws IOException {
        return this.post(url, "application/x-www-form-urlencoded;charset=UTF-8", body);
    }

    protected HttpURLConnection post(String url, String contentType, String body) throws IOException {
        if(url != null && body != null) {
            if(!url.startsWith("https://")) {
                this.logger.warning("URL does not use https: " + url);
            }

            this.logger.fine("Sending POST to " + url);
            this.logger.finest("POST body: " + body);
            byte[] bytes = body.getBytes();
            HttpURLConnection conn = this.getConnection(url);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", contentType);
            conn.setRequestProperty("Authorization", "key=" + this.key);
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            return conn;
        } else {
            throw new IllegalArgumentException("arguments cannot be null");
        }
    }

    protected static final Map<String, String> newKeyValues(String key, String value) {
        HashMap keyValues = new HashMap(1);
        keyValues.put(nonNull(key), nonNull(value));
        return keyValues;
    }

    protected static StringBuilder newBody(String name, String value) {
        return (new StringBuilder((String)nonNull(name))).append('=').append((String)nonNull(value));
    }

    protected static void addParameter(StringBuilder body, String name, String value) {
        ((StringBuilder)nonNull(body)).append('&').append((String)nonNull(name)).append('=').append((String)nonNull(value));
    }

    protected HttpURLConnection getConnection(String url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection)(new URL(url)).openConnection();
        return conn;
    }

    protected static String getString(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader((InputStream)nonNull(stream)));
        StringBuilder content = new StringBuilder();

        String newLine;
        do {
            newLine = reader.readLine();
            if(newLine != null) {
                content.append(newLine).append('\n');
            }
        } while(newLine != null);

        if(content.length() > 0) {
            content.setLength(content.length() - 1);
        }

        return content.toString();
    }

    static <T> T nonNull(T argument) {
        if(argument == null) {
            throw new IllegalArgumentException("argument cannot be null");
        } else {
            return argument;
        }
    }

    void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException var4) {
            Thread.currentThread().interrupt();
        }

    }

    class CustomParserException extends RuntimeException {
        CustomParserException(String message) {
            super(message);
        }
    }
}

package com.zipdoc.fcm;

/**
 * Created by ZIPDOC on 2016-10-07.
 */

import java.io.Serializable;

public final class Result implements Serializable {
    private final String messageId;
    private final String canonicalRegistrationId;
    private final String errorCode;

    private Result(Result.Builder builder) {
        this.canonicalRegistrationId = builder.canonicalRegistrationId;
        this.messageId = builder.messageId;
        this.errorCode = builder.errorCode;
    }

    public String getMessageId() {
        return this.messageId;
    }

    public String getCanonicalRegistrationId() {
        return this.canonicalRegistrationId;
    }

    public String getErrorCodeName() {
        return this.errorCode;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("[");
        if(this.messageId != null) {
            builder.append(" messageId=").append(this.messageId);
        }

        if(this.canonicalRegistrationId != null) {
            builder.append(" canonicalRegistrationId=").append(this.canonicalRegistrationId);
        }

        if(this.errorCode != null) {
            builder.append(" errorCode=").append(this.errorCode);
        }

        return builder.append(" ]").toString();
    }

    static final class Builder {
        private String messageId;
        private String canonicalRegistrationId;
        private String errorCode;

        Builder() {
        }

        public Result.Builder canonicalRegistrationId(String value) {
            this.canonicalRegistrationId = value;
            return this;
        }

        public Result.Builder messageId(String value) {
            this.messageId = value;
            return this;
        }

        public Result.Builder errorCode(String value) {
            this.errorCode = value;
            return this;
        }

        public Result build() {
            return new Result(this);
        }
    }
}

package com.zipdoc.fcm;

/**
 * Created by ZIPDOC on 2016-10-07.
 */
import java.io.IOException;

public final class InvalidRequestException extends IOException {
    private final int status;
    private final String description;

    public InvalidRequestException(int status) {
        this(status, (String)null);
    }

    public InvalidRequestException(int status, String description) {
        super(getMessage(status, description));
        this.status = status;
        this.description = description;
    }

    private static String getMessage(int status, String description) {
        StringBuilder base = (new StringBuilder("HTTP Status Code: ")).append(status);
        if(description != null) {
            base.append("(").append(description).append(")");
        }

        return base.toString();
    }

    public int getHttpStatusCode() {
        return this.status;
    }

    public String getDescription() {
        return this.description;
    }
}

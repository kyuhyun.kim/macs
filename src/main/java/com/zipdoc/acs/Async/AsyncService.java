package com.zipdoc.acs.Async;

import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.FileType;
import com.zipdoc.acs.define.PictureType;
import com.zipdoc.acs.define.agreement.AgreementStatus;
import com.zipdoc.acs.define.agreement.AgreementType;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.pns.MsgSendRequest;
import com.zipdoc.acs.model.pns.NotificationPhrase;
import com.zipdoc.acs.persistence.dao.EstimateImgFileDao;
import com.zipdoc.acs.persistence.dao.NotificationPhraseDao;
import com.zipdoc.acs.utils.*;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by zipdoc on 2018-11-28.
 */
@Service
public class AsyncService {

    private static Logger log = LoggerFactory.getLogger(AsyncService.class);

    @Autowired
    private AcsConfig acsConfig;

    @Autowired
    private EstimateImgFileDao estimateImgFileDao;

    @Autowired
    NotificationPhraseDao notificationPhraseDao;


    @Async("asyncMsgSndExecutor")
    public void EstimateCustmerSendMsg(Estimate estimate) throws InterruptedException {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        BufferedReader br = null;InputStreamReader is = null;CloseableHttpResponse response = null;HttpPost httpPost = null;

        try {


            NotificationPhrase notificationPhrase = notificationPhraseDao.selectNotificationPhrase(acsConfig.getProperty("EST.MATCH.ESTIMATE"));
            if(notificationPhrase == null) {
                return;
            }

            log.info("[EstimateSendMsg] ######################################################################################################################");
            log.info("[EstimateSendMsg]  견적 신청 고객 메시지 발송 시작. PHONE_NO[{}] WRITER[{}]",  estimate.getPhone_no(), estimate.getWriter());
            log.info("[EstimateSendMsg] SEND NIGNX_SERVER_URL URL[{}{}]", acsConfig.getProperty("NIGNX.SERVER_URL"), acsConfig.getProperty("PNS.MESSAGE.URL"));

            httpPost = new HttpPost(acsConfig.getProperty("NIGNX.SERVER_URL") + acsConfig.getProperty("PNS.MESSAGE.URL"));

            RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                    .setSocketTimeout(3000).build();
            httpPost.setConfig(requestConfig);
            httpPost.addHeader("Content-type", "application/json;charset=UTF-8");

            String body = Json.toStringJson((Object) new MsgSendRequest( estimate, notificationPhrase)).replace("#{url}", acsConfig.getProperty("NIGNX.SERVER_URL") + "x/" + AgreementType.알림문자.getName() + AgreementStatus.철회_거부.getName() + estimate.getEstimate_no() + ".do");
            /*log.info(acsConfig.getProperty("NIGNX.SERVER_URL") + "x/" + AgreementType.알림문자.getName() + AgreementStatus.철회_거부.getName() + estimate.getEstimate_no() + ".do");*/
            /*body = body.replace("#{url}", acsConfig.getProperty("NIGNX.SERVER_URL") + "x/" + AgreementType.알림문자.getName() + AgreementStatus.철회_거부.getName() + estimate.getEstimate_no() + ".do");*/
            httpPost.setEntity(new StringEntity(body, HTTP.UTF_8));

            log.info("[EstimateSendMsg] 요청 메시지 [{}]", body);

            response = httpClient.execute(httpPost);
            is = new InputStreamReader(response.getEntity().getContent());
            br = new BufferedReader(is);
            String read = null;
            StringBuffer sb = new StringBuffer();
            while ((read = br.readLine()) != null) {
                sb.append(read);
            }

            br.close();
            is.close();
            response.close();
            httpPost.releaseConnection();

            String Message = System.lineSeparator() + "--------------------------------------------------------------------------------"+System.lineSeparator() + response.toString()+ System.lineSeparator() +
                    "\n================================================================================" +
                    System.lineSeparator();


            log.info("[EstimateSendMsg] 응답 메시지 [{}]", Message);

            br.close();
            is.close();
            response.close();
            httpPost.releaseConnection();

        } catch (Exception e) {
            log.error("[EstimateSendMsg] 파트너스 고객 메시지 발송 실패. [{}]", e);
        }

        log.info("[EstimateSendMsg] ######################################################################################################################");


    }

    @Async("asyncExecutor")
    public void sendEstimateMsg(List<MultipartFile> images, Estimate estimate) throws InterruptedException {

        try {
                /* 2018.11.27 (김규현 추가)*/
            if (images != null && images.size() > 0) {
                for (int nIdx = 0; nIdx < images.size(); nIdx++) {
                    String relativePath = Constants.RELATIVE_ESTIMATE_PATH + estimate.getEstimate_no() + "/" + nIdx + "/";

                    FileInfo fileInfo = FileUploadUtil.uploadFile(images.get(nIdx), relativePath);

                    log.debug("[ESTIMATE-AWS-S3-IMAGE-PUT] [" + estimate.getPhone_no() + "] 이미지 등록 완료.[{}] ESTIMATE-NO=" + estimate.getEstimate_no(),
                            (acsConfig.getProperty("STATIC_URL_TYPE").equals("0")) ? acsConfig.getProperty("STATIC_URL_C") + relativePath + fileInfo.getFile().getName() : acsConfig.getProperty("STATIC_URL_Q") + relativePath + fileInfo.getFile().getName());

                    estimate.setEstimateImgFiles(new EstimateImgFile(estimate.getEstimate_no(), nIdx, null, "00",
                            (acsConfig.getProperty("STATIC_URL_TYPE").equals("0")) ? acsConfig.getProperty("STATIC_URL_C") + relativePath + fileInfo.getFile().getName() : acsConfig.getProperty("STATIC_URL_Q") + relativePath + fileInfo.getFile().getName(), fileInfo.getOriginFilename()));
                }
            }

				/* 이미지 견적 파일에 저장. */
            estimateImgFileDao.createEstimateImageFile(estimate.getEstimate_img_info());

        } catch (Exception e) {
            log.error("[ESTIMATE-PUT] 견적 등록 오류. [이미지 등록 실패]");
        }


    }

    @Async("asyncExecutor")
    public void EstimateUploadFile(List<MultipartFile> attached_files, Estimate estimate) throws InterruptedException {


        String upload_path = acsConfig.getProperty("UPLOAD_FILE.STATIC_PATH");

        final String file_path = "estimate/" + estimate.getEstimate_no() + "/";

        try {

            File destDir = null;

            if (attached_files != null && attached_files.size() > 0) {

                int nIdx = 0;
                for (MultipartFile multipartFile : attached_files) {


                    String file_name = DateUtil.toFormatString(DateUtil.DATE_FORMAT_FOR_MILLISECOND) + "_" + RandomStringUtils.randomAlphanumeric(10) + "_" + nIdx + "_";
                    String thumbnail = null;
                    String file_type = FileType.IMAGE.value();

                    FileInfo fileInfo = new FileInfo(multipartFile, upload_path + file_path + file_name);

                    // 폴더 생성
                    if (!fileInfo.getFile().getParentFile().exists()) {
                        fileInfo.getFile().getParentFile().mkdirs();
                    }

                    //첨부파일이 이미지인 경우 썸네일을 추출한다.

                    multipartFile.transferTo(fileInfo.getFile());

                    if (fileInfo.isImageFile()) {

                        //원본 파일을 리사이징해서 저장
                        /*ImageResizeUtil.imageResize(multipartFile.getInputStream(), fileInfo.getFile(), PictureType.XXXHDPI.getWidth(), PictureType.XXXHDPI.getHeight());*/
                        //썸네일 파일 생성
                        thumbnail = file_name + "thumb." + fileInfo.getExtension();
                        ImageResizeUtil.imageResize(fileInfo.getFile(), upload_path + file_path + thumbnail, PictureType.MDPI.getWidth(), PictureType.MDPI.getHeight());

                    } else
                        file_type = FileType.ETC.value();


                    destDir = new File(upload_path + file_path);

                    FileUploadUtil.getInstance().uploadToS3(destDir, file_path);

                    //DB저장을 위해 파일 확장자를 추가한다.
                    file_name = file_name + "." + fileInfo.getExtension();


                    estimate.setEstimateImgFiles(new EstimateImgFile(estimate.getEstimate_no(), nIdx,
                            (thumbnail==null) ? null : (acsConfig.getProperty("STATIC_URL_TYPE").equals("0")) ? acsConfig.getProperty("STATIC_URL_C") + file_path + thumbnail : acsConfig.getProperty("STATIC_URL_Q") + file_path + thumbnail,
                            file_type,
                            (acsConfig.getProperty("STATIC_URL_TYPE").equals("0")) ? acsConfig.getProperty("STATIC_URL_C") + file_path + file_name : acsConfig.getProperty("STATIC_URL_Q") + file_path + file_name,
                            fileInfo.getOriginFilename()));

                    nIdx++;

                }
                /* 이미지 견적 파일에 저장. */
                estimateImgFileDao.createEstimateImageFile(estimate.getEstimate_img_info());

                if (destDir != null && destDir.isDirectory())
                    destDir.delete();
            }

        } catch (Exception e) {

            log.error("[ESTIMATE-IMAGE-PUT] 견적 현장 사진 등록 오류. [현장 사진 등록 실패] 오류 [{}]", e);
        }

    }


    @Async("asyncMsgSndExecutor")
    public void SatisfactionZipmanSendMsg(MyContract contract, SatisfactionRequest satisfactionRequest) throws InterruptedException {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        BufferedReader br = null;InputStreamReader is = null;CloseableHttpResponse response = null;HttpPost httpPost = null;

        try {

            NotificationPhrase notificationPhrase = notificationPhraseDao.selectNotificationPhrase(acsConfig.getProperty("ZIPMAN.MATCH.SURVEY"));
            if(notificationPhrase == null) {
                return;
            }

            log.info("[SatisfactionZipmanSendMsg] ######################################################################################################################");
            log.info("[SatisfactionZipmanSendMsg] 집닥맨 만족도 조사 고객 메시지 발송 시작. 수신번호[{}] 계약자명[{}]",  contract.getPhone_no(), contract.getContracter());
            log.info("[SatisfactionZipmanSendMsg] SEND NIGNX_SERVER_URL URL[{}{}]", acsConfig.getProperty("NIGNX.SERVER_URL"), acsConfig.getProperty("PNS.MESSAGE.URL"));

            httpPost = new HttpPost(acsConfig.getProperty("NIGNX.SERVER_URL") + acsConfig.getProperty("PNS.MESSAGE.URL"));

            RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                    .setSocketTimeout(3000).build();
            httpPost.setConfig(requestConfig);
            httpPost.addHeader("Content-type", "application/json;charset=UTF-8");
            httpPost.addHeader("member-no", satisfactionRequest.getMember_no().toString());

            String body = Json.toStringJson((Object) new MsgSendRequest( contract, satisfactionRequest, notificationPhrase));
            httpPost.setEntity(new StringEntity(body, HTTP.UTF_8));

            log.info("[SatisfactionZipmanSendMsg] 요청 메시지 [{}]", body);

            response = httpClient.execute(httpPost);
            is = new InputStreamReader(response.getEntity().getContent());
            br = new BufferedReader(is);
            String read = null;
            StringBuffer sb = new StringBuffer();
            while ((read = br.readLine()) != null) {
                sb.append(read);
            }

            br.close();
            is.close();
            response.close();
            httpPost.releaseConnection();

            String Message = System.lineSeparator() +
                    "--------------------------------------------------------------------------------"
                    + System.lineSeparator() + response.toString()+ System.lineSeparator() +
                    "\n================================================================================" +
                    System.lineSeparator();

            log.info("[SatisfactionZipmanSendMsg] 응답 메시지 [{}]", Message);

            br.close();
            is.close();
            response.close();
            httpPost.releaseConnection();

        } catch (Exception e) {
            log.error("[SatisfactionZipmanSendMsg] 파트너스 고객 메시지 발송 실패. [{}]", e);
        }

        log.info("[SatisfactionZipmanSendMsg] ######################################################################################################################");


    }
}

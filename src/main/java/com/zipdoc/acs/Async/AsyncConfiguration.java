package com.zipdoc.acs.Async;

import java.util.concurrent.Executor;

import com.zipdoc.acs.utils.AcsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncConfiguration
{
    final Integer CorePoolSize = 10;
    final Integer MaxPoolSize = 20;


    @Bean(name = "asyncExecutor")
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(CorePoolSize);
        executor.setMaxPoolSize(MaxPoolSize);
        executor.setQueueCapacity(CorePoolSize * CorePoolSize * 2);
        executor.setThreadNamePrefix("ACS-IMG-Thread-");

        executor.initialize();
        return executor;
    }

    @Bean(name = "asyncMsgSndExecutor")
    public Executor asyncMsgSndExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(CorePoolSize);
        executor.setMaxPoolSize(MaxPoolSize);
        executor.setQueueCapacity(CorePoolSize * CorePoolSize * 2);
        executor.setThreadNamePrefix("ACS-LMS-Thread-");

        executor.initialize();
        return executor;
    }
}
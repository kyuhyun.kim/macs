package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.define.LayoutCategory;
import com.zipdoc.acs.domain.entity.*;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface HomeLayoutDao {

	// 홈 메인화면 표시 문구
	List<LayoutConfig> selectHomeLayoutText(Map<String, Object> map) throws DataAccessException;

	// 홈 메인화면 표시 정보
	List<LayoutHome> selectHomeLayout(Map<String, Object> map) throws DataAccessException;

    // 2018.08.21. KJB. 베스트 파트너 조회
    List<Partner> selectBestPartner(Map<String, Object> map) throws DataAccessException;

	// 2018.08.21. KJB. 베스트 파트너 조회
	List<Partner> selectBestPartnerRevision(Map<String, Object> map) throws DataAccessException;



}



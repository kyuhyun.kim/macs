package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.LayoutConfig;
import com.zipdoc.acs.domain.entity.LayoutHome;
import com.zipdoc.acs.domain.entity.Partner;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface CashedHomeLayoutDao {



	// 홈 메인화면 표시 문구(웹 캐리 처리)
	List<LayoutConfig> selectCashedHomeLayoutTextWeb(Map<String, Object> map) throws DataAccessException;


	// 홈 메인화면 표시 정보(캐리 처리)
	List<LayoutHome> selectCashedHomeLayout(Map<String, Object> map) throws DataAccessException;

	// 2018.08.21. KJB. 베스트 파트너 조회(캐리 처리)
	List<Partner> selectCashedBestPartner(Map<String, Object> map) throws DataAccessException;
}



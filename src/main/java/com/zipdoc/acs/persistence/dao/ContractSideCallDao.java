package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ContractSideCall;
import com.zipdoc.acs.domain.entity.VividComment;
import org.springframework.dao.DataAccessException;
import java.util.List;
import java.util.Map;

public interface ContractSideCallDao {

    public int createContractSideCall(ContractSideCall contractSideCall) throws DataAccessException;
}

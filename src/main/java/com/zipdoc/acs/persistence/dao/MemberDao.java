package com.zipdoc.acs.persistence.dao;

import java.util.List;
import java.util.Map;

import com.zipdoc.acs.domain.entity.*;
import org.springframework.dao.DataAccessException;

public interface MemberDao {

	long findNextID() throws DataAccessException;
	
	int createDevice(Member member) throws DataAccessException;
	
	Member selectMember(Map<String, Object> map) throws DataAccessException;

	Member selectMemberByZid(long zid) throws DataAccessException;

	Member selectMemberByWebToken(String token) throws DataAccessException;

	Member selectMemberByMemberId(long member_id) throws DataAccessException;

	Member selectMemberByForgotAccount(ForgotAccount account) throws DataAccessException;
	
	Member selectMemberByAccount(Account account) throws DataAccessException;
	
	int updateDeviceUserLevel(Member member) throws DataAccessException;

	String findAccount(String account) throws DataAccessException;
	
	int updateToken(Member member) throws DataAccessException;

	int createMember(Member member) throws DataAccessException;

	int selectMemberDeviceCount(Member member) throws DataAccessException;

	void createMemberDevice(Member member) throws DataAccessException;

	Member selectDeviceByDeviceId(Map<String, Object> map) throws DataAccessException;

	void updateMemberLoginInfo(Member member) throws DataAccessException;

	void updateDevice(Member member) throws DataAccessException;

	MyPage selectMyPageSummary(Member member) throws DataAccessException;

	Profile selectProfile(long member_no) throws DataAccessException;

	void updateProfile(Member member) throws DataAccessException;

	void updateNickname(Member member) throws DataAccessException;

	int selectCountUsingNicknameCount(String nickname) throws DataAccessException;

	//2017-03-30 적립금 업데이트
	void updateMileage(Member member) throws DataAccessException;

    void updateDeviceAgree(Member member) throws DataAccessException;

    void updateDeviceAgreePrivacy(Member member) throws DataAccessException;

    void deleteMemberDevice(Member member) throws DataAccessException;

    void deleteMember(Member member) throws DataAccessException;

	ZipdocmanUser selectZipdocmanUser(long member_no) throws DataAccessException;

	List<ZipdocmanUser> selectZipdocmanGroup(Integer group_no) throws DataAccessException;

	int updateMember(Account account) throws DataAccessException;

	void updateMemberAlarm(AlarmInfo alarmInfo) throws DataAccessException;

	AlarmInfo selectMemberAlarm(Member member)throws DataAccessException;

	void updateDeviceAlarm(AlarmInfo alarmInfo) throws DataAccessException;

	AlarmInfo selectDeviceAlarm(Member member)throws DataAccessException;
}

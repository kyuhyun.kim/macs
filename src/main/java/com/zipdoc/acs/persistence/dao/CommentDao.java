package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.Comment;
import com.zipdoc.acs.model.CommentListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface CommentDao {
	public List<Comment> selectList(CommentListRequest searchCondition) throws DataAccessException;
	public int selectListTotalCount(CommentListRequest searchCondition) throws DataAccessException;
	public Comment selectById(Comment comment) throws DataAccessException;
	public int selectNextReplyNo(Comment comment) throws DataAccessException;
	public void insert(Comment comment) throws DataAccessException;
	public void update(Comment comment) throws DataAccessException;
	public void updateDeleteFlag(Comment comment) throws DataAccessException;

	public void delete(Comment comment) throws DataAccessException;
}

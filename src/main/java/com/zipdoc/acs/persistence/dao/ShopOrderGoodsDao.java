package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.OrderSummary;
import com.zipdoc.acs.domain.entity.ShopOrderGoods;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface ShopOrderGoodsDao {
	void createOrderGoods(ShopOrderGoods shopOrderGoods) throws DataAccessException;

	List<ShopOrderGoods> findList(String order_no) throws DataAccessException;

	ShopOrderGoods findById(Long sno) throws DataAccessException;

	void updateOrderStatus(Long goods_sno) throws DataAccessException;

	void updatePurchaseComplete(Map<String, Object> condition) throws DataAccessException;

	OrderSummary findSummary(Long member_no) throws DataAccessException;
}

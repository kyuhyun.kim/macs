package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.ItemListRequest;
import com.zipdoc.acs.model.ProductListRequest;
import com.zipdoc.acs.model.ZzimListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface ProductDao {
	List<Product> selectList(ProductListRequest searchCondition) throws DataAccessException;
	int selectListTotalCount(ProductListRequest searchCondition) throws DataAccessException;
	int selectListTotalBeforeAfterCount(ProductListRequest searchCondition) throws DataAccessException;
	Product selectById(Map<String, Object> map) throws DataAccessException;
	Product selectByPid(long pid) throws DataAccessException;
	List<Item> selectItemListByPid(Map<String, Object> map) throws DataAccessException;
	int selectItemListTotalCount(ItemListRequest searchCondition) throws DataAccessException;
	List<Item> selectItemList(ItemListRequest searchCondition) throws DataAccessException;
	Item selectItem(long item_id) throws DataAccessException;
	Partner selectPartner(Map<String, Object> map) throws DataAccessException;
	void updateIncreaseZzimCnt(long pid) throws DataAccessException;
	void updateDecreaseZzimCnt(long pid) throws DataAccessException;
	void updateIncreaseLikeCnt(long pid) throws DataAccessException;
	void updateDecreaseLikeCnt(long pid) throws DataAccessException;
	void updateIncreaseHitCnt(long pid) throws DataAccessException;
	void updateIncreaseShareCnt(long pid) throws DataAccessException;

	List<Product> selectCommunityList(ProductListRequest searchCondition) throws DataAccessException;
	int selectCommunityListTotalCount(ProductListRequest searchCondition) throws DataAccessException;
	Product selectCommunityById(Map<String, Object> map) throws DataAccessException;

	void updateProductStatus(Product product) throws DataAccessException;

	void updateProductStatusAllByMemberNo(Long member_no) throws DataAccessException;

	void insert(Product product) throws DataAccessException;
	void update(Product product) throws DataAccessException;
	void insertItem(Item item) throws DataAccessException;
	void updateItem(Item item) throws DataAccessException;
	void deleteItem(Item item) throws DataAccessException;
	void insertProductItem(Item item) throws DataAccessException;
	void deleteProductItem(Item item) throws DataAccessException;
	void updateProductFileInfo(Product entity) throws DataAccessException;
	void updateItemFileInfo(Item entity) throws DataAccessException;

	// 2018.09.03. KJB. 시공컨셉 조회
	Product selectConcept(long pid) throws DataAccessException;

	// 2018.10.02. KJB. 비슷한 가격의 시공사례
	List<Product> selectSamePriceProductList(Map<String,Object> condition) throws DataAccessException;

	int searchGalleryTotalCount(ProductListRequest searchCondition) throws DataAccessException;

	int searchGalleryTotalCountRevision(ProductListRequest searchCondition) throws DataAccessException;

	int searchGalleryTotalCountRevision(Map<String, Object> searchCondition) throws DataAccessException;

	List<Product> searchGalleryList(ProductListRequest searchCondition) throws DataAccessException;

	List<Product> searchGalleryListRevision(ProductListRequest searchCondition) throws DataAccessException;

	List<Product> searchGalleryListRevision(Map<String, Object> searchCondition) throws DataAccessException;

    int searchGalleryPictureTotalCount(ItemListRequest searchCondition) throws DataAccessException;

	int searchGalleryPictureTotalCountRevision(ItemListRequest searchCondition) throws DataAccessException;

	int searchGalleryPictureTotalCountRevision(Map<String, Object> searchCondition) throws DataAccessException;

    List<Item> searchGalleryPictureList(ItemListRequest searchCondition) throws DataAccessException;

	List<Item> searchGalleryPictureListRevision(ItemListRequest searchCondition) throws DataAccessException;

	List<Item> searchGalleryPictureListRevision(Map<String, Object> searchCondition) throws DataAccessException;

	List<Product> selectBeforeAfterList(ProductListRequest searchCondition) throws DataAccessException;



}

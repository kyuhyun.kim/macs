package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.Customer;
import com.zipdoc.acs.domain.entity.CustomerPartnerInquiry;
import com.zipdoc.acs.domain.entity.CustomerReply;
import com.zipdoc.acs.domain.service.ServiceException;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface CustomerDao {

	// 1:1 문의하기 내역 조회
	int findListCount(Map<String, Object> condition) throws DataAccessException;
	List<Customer> findList(Map<String, Object> condition) throws DataAccessException;
	Customer findById(Map<String, Object> condition) throws DataAccessException;

	// 1:1 문의하기 등록/수정/삭제
    void insert(Customer customer) throws DataAccessException;
    int update(Customer customer) throws DataAccessException;
    int delete(int cno) throws DataAccessException;

	// 1:1 문의하기 댓글 조회
	List<CustomerReply> findReplyList(int cno) throws DataAccessException;

	// 1:1 문의하기 댓글 등록/수정/삭제
	void insertReply(CustomerReply customerReply) throws DataAccessException;
	int updateReply(CustomerReply customerReply) throws DataAccessException;
	int deleteReply(int reply_no) throws DataAccessException;
    void deleteReplyAll(int cno) throws DataAccessException;

	// 파트너 문의 등록
	void insertPartnerInquiry(CustomerPartnerInquiry inquiry) throws ServiceException;
}

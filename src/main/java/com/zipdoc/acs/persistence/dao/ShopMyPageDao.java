package com.zipdoc.acs.persistence.dao;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.OrderSummary;
import com.zipdoc.acs.domain.entity.ShopOrderInfo;

public interface ShopMyPageDao {

	public OrderSummary selectShopMyPageSummary(Member member) throws DataAccessException;

	public List<ShopOrderInfo> findRecentList(Map<String, Object> condition) throws DataAccessException;
	
}

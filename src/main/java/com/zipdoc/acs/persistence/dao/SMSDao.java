package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.CertificationTempKey;
import org.springframework.dao.DataAccessException;


public interface SMSDao {

    void insertCertificationKey(CertificationTempKey certificationTempKey) throws DataAccessException;

    CertificationTempKey getTempKey(String phone) throws DataAccessException;

    void deleteTempKey(String phone) throws DataAccessException;

}

package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ShopGoods;
import com.zipdoc.acs.model.shop.ShopGoodsListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

/**
 * Created by dskim on 2017. 5. 11..
 */
public interface ShopGoodsDao {

    List<ShopGoods> selectList(ShopGoodsListRequest searchCondition) throws DataAccessException;
    int selectListTotalCount(ShopGoodsListRequest searchCondition) throws DataAccessException;

    ShopGoods selectByGoodsNo(long goods_no) throws DataAccessException;

    //2017-03-27 상품별 주문카운트 수정
    void updateIncreaseOrderCnt(Map<String, Object> condition) throws DataAccessException;
	void updateIncreaseZzimCnt(long pid);
    void updateStockCnt(Map<String, Object> params) throws DataAccessException;
}

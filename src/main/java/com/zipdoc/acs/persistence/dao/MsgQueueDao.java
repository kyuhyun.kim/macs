package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.EstimateListRequest;
import com.zipdoc.acs.model.ListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface MsgQueueDao {

	// 알림 문자 발송 금지 등록
	void updateMsgQueueBlock(Long estimate_no) throws DataAccessException;
}

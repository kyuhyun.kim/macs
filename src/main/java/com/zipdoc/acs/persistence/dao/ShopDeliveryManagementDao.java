package com.zipdoc.acs.persistence.dao;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.zipdoc.acs.domain.entity.ShopDeliveryManagement;
import com.zipdoc.acs.domain.entity.ShopDeliveryManagementRequest;

public interface ShopDeliveryManagementDao {

	public int selectDeliveryListTotalCount(ShopDeliveryManagementRequest shopDeliveryManagementRequest);

	public List<ShopDeliveryManagement> selectDeliveryList(ShopDeliveryManagementRequest shopDeliveryManagementRequest);

	public ShopDeliveryManagement selectById(Map<String, Object> condition) throws DataAccessException;

	public void create(ShopDeliveryManagement shopDeliveryManagement) throws DataAccessException;

	public void delete(Map<String, Object> condition) throws DataAccessException;

	public void updateDefaultAddress(Map<String, Object> condition) throws DataAccessException;

	public void update(ShopDeliveryManagement shopDeliveryManagement) throws DataAccessException;
	
}

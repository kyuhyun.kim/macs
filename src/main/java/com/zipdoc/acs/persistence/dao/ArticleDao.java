package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.ArticleListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface ArticleDao {
	public int selectNoticeListTotalCount(ArticleListRequest articleListRequest) throws DataAccessException;
	public List<Article> selectNoticeList(ArticleListRequest articleListRequest) throws DataAccessException;
	public Article selectNotice(int cno) throws DataAccessException;
	public void updateNoticeViewCnt(int cno) throws DataAccessException;

	public int selectEventListTotalCount(ArticleListRequest articleListRequest) throws DataAccessException;
	public List<Article> selectEventList(ArticleListRequest articleListRequest) throws DataAccessException;
	public Article selectEvent(int cno) throws DataAccessException;
	public void updateEventViewCnt(int cno) throws DataAccessException;

	public int selectNewsListTotalCount(ArticleListRequest articleListRequest) throws DataAccessException;
	public List<Article> selectNewsList(ArticleListRequest articleListRequest) throws DataAccessException;
	public Article selectNews(int cno) throws DataAccessException;
	public void updateNewsViewCnt(int cno) throws DataAccessException;

	public int selectFaqListTotalCount(ArticleListRequest articleListRequest) throws DataAccessException;
	public List<Article> selectFaqList(ArticleListRequest articleListRequest) throws DataAccessException;

	public int selectInteriorTipListTotalCount(ArticleListRequest articleListRequest) throws DataAccessException;
	public List<Article> selectInteriorTipList(ArticleListRequest articleListRequest) throws DataAccessException;
	public Article selectInteriorTip(int cno) throws DataAccessException;
	public void updateInteriorTipViewCnt(int cno) throws DataAccessException;

	public ArticleInfo selectArticleCreateDateInfo() throws DataAccessException;

	public int selectPatnerNoticeListTotalCount(ArticleListRequest articleListRequest) throws DataAccessException;
	public List<Article> selectPatnerNoticeList(ArticleListRequest articleListRequest) throws DataAccessException;
	public Article selectPatnerNotice(int cno) throws DataAccessException;
	public void updatePatnerNoticeViewCnt(int cno) throws DataAccessException;

	public int selectZipdocmanListTotalCount(ArticleListRequest articleListRequest) throws DataAccessException;
	public List<Article> selectZipdocmanList(ArticleListRequest articleListRequest) throws DataAccessException;
	public Article selectZipdocman(int cno) throws DataAccessException;
	public void updateZipdocmanViewCnt(int cno) throws DataAccessException;
}



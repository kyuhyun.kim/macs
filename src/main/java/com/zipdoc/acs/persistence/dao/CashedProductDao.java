package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.Product;
import com.zipdoc.acs.model.ProductListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;


public interface CashedProductDao {

	int selectListTotalBeforeAfterCount(ProductListRequest searchCondition) throws DataAccessException;
	List<Product> selectBeforeAfterList(ProductListRequest searchCondition) throws DataAccessException;


}

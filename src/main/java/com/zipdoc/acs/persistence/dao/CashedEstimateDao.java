package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.EstimateListRequest;
import com.zipdoc.acs.model.ListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface CashedEstimateDao {

	// 주거 - 웹 메인 견적신청 현황 조회
	List<Estimate> selectWebMainList() throws DataAccessException;

	EstimateSummary selectEstimateSummary() throws DataAccessException;

}

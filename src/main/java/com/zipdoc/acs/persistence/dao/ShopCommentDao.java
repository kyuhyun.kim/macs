package com.zipdoc.acs.persistence.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.zipdoc.acs.domain.entity.ShopComment;
import com.zipdoc.acs.model.CommentListRequest;

public interface ShopCommentDao {

	public List<ShopComment> selectList(CommentListRequest searchCondition) throws DataAccessException;

	public int selectListTotalCount(CommentListRequest searchCondition) throws DataAccessException;

	public ShopComment selectById(ShopComment comment) throws DataAccessException;

	public int selectNextReplyNo(ShopComment comment) throws DataAccessException;

	public void insert(ShopComment comment) throws DataAccessException;

	public void update(ShopComment comment) throws DataAccessException;

	public void updateDeleteFlag(ShopComment comment) throws DataAccessException;

	public int findByCreater(ShopComment comment) throws DataAccessException;

}

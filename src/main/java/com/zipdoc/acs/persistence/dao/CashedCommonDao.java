package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.*;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface CashedCommonDao {

	List<EventPromotion> selectEventPromotionList(int channel) throws DataAccessException;

}



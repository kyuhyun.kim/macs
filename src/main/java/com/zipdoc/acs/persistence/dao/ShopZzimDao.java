package com.zipdoc.acs.persistence.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.zipdoc.acs.domain.entity.ShopZzim;
import com.zipdoc.acs.domain.entity.ShopZzimInfo;
import com.zipdoc.acs.model.shop.ShopZzimListRequest;

public interface ShopZzimDao {

	public int selectShopZzimListTotalCount(ShopZzimListRequest condition) throws DataAccessException;

	public List<ShopZzimInfo> selectShopZzimList(ShopZzimListRequest condition) throws DataAccessException;

	public int selectShopZzimCount(ShopZzim shopZzim) throws DataAccessException;

	public void createZzim(ShopZzim shopZzim) throws DataAccessException;

	public void deleteZzim(ShopZzim shopZzim) throws DataAccessException;

	public void deleteZzimAll(ShopZzim shopZzim) throws DataAccessException;

}

package com.zipdoc.acs.persistence.dao;

import org.springframework.dao.DataAccessException;

import com.zipdoc.acs.domain.entity.ShopPartnerReq;

public interface ShopPartnerDao {

	void create(ShopPartnerReq shopPartnerReq) throws DataAccessException;

}

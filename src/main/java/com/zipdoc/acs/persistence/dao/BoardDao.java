package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.ArticleListRequest;
import com.zipdoc.acs.model.FAQListRequest;
import com.zipdoc.acs.model.ListRequest;
import com.zipdoc.acs.model.MagazineListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface BoardDao {

	// 웹 매거진 메인
	List<Magazine> selectBestMagazine(MagazineListRequest searchCondition) throws DataAccessException;
	List<Magazine> selectRecentMagazine(MagazineListRequest searchCondition) throws DataAccessException;

	// 매거진 목록 및 상세
	int searchMagazineTotalCount(MagazineListRequest searchCondition) throws DataAccessException;
	List<Magazine> searchMagazineList(MagazineListRequest searchCondition) throws DataAccessException;
	Magazine selectMagazineDetail(int cno) throws DataAccessException;

	int searchMagazineTotalCountRevision(MagazineListRequest searchCondition) throws DataAccessException;
	List<Magazine> searchMagazineListRevision(MagazineListRequest searchCondition) throws DataAccessException;

	int searchMagazineTotalCountRevision(Map<String, Object> searchCondition) throws DataAccessException;
	List<Magazine> searchMagazineListRevision(Map<String, Object> searchCondition) throws DataAccessException;


	// 집닥소식 목록 및 상세
	int searchNewsTotalCount(ListRequest searchCondition) throws DataAccessException;
	List<News> searchNewsList(ListRequest searchCondition) throws DataAccessException;
	List<News> selectRecentNews(MagazineListRequest searchCondition) throws DataAccessException;
	News selectNewsDetail(int cno) throws DataAccessException;

	// 매거진 및 집닥소식 조회 수 증가
	void updateViewMagazineCnt(int cno) throws DataAccessException;

	// 집닥이벤트 목록 및 상세
	int searchEventTotalCount(ListRequest searchCondition) throws DataAccessException;
	List<Event> searchEventList(ListRequest searchCondition) throws DataAccessException;
	Event selectEventDetail(Map<String, Object> searchCondition) throws DataAccessException;
	void updateViewEventCnt(int cno) throws DataAccessException;

	// FAQ 목록
	int searchFAQTotalCount(FAQListRequest searchCondition) throws DataAccessException;
	List<FAQ> searchFAQList(FAQListRequest searchCondition) throws DataAccessException;

	/* 최신 매거진 정보 조회 */
	List<Magazine> selectRecentMagazineType(Integer magazine_type) throws DataAccessException;
}



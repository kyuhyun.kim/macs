package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ShopOrder;
import com.zipdoc.acs.domain.entity.ShopOrderInfo;
import com.zipdoc.acs.model.shop.ShopOrderListRequest;

import java.util.List;

import org.springframework.dao.DataAccessException;

/**
 * Created by dskim on 2017. 5. 11..
 */
public interface ShopOrderDao {

    void insert(ShopOrder shopOrder) throws DataAccessException;

    ShopOrder selectByOrderNo(String order_no) throws DataAccessException;

	int selectListTotalCount(ShopOrderListRequest condition) throws DataAccessException;

	List<ShopOrder> selectList(ShopOrderListRequest condition) throws DataAccessException;

	ShopOrderInfo findByIdWithOrderGoods(String order_no) throws DataAccessException;

	ShopOrder findById(String order_no) throws DataAccessException;

}

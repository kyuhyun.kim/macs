package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ShopOrderHandle;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface ShopOrderHandleDao {
	void create(ShopOrderHandle shopOrderHandle) throws DataAccessException;

	int findListCount(Map<String, Object> condition) throws DataAccessException;

	List<ShopOrderHandle> findList(Map<String, Object> condition) throws DataAccessException;

	ShopOrderHandle findBySno(Long sno) throws DataAccessException;

	void updateHandleCancleStatus(Map<String, Object> cancle_condition) throws DataAccessException;

	ShopOrderHandle findById(Long sno) throws DataAccessException;

	void update(ShopOrderHandle shopOrderHandle) throws DataAccessException;

	void delete(Long sno) throws DataAccessException;
}

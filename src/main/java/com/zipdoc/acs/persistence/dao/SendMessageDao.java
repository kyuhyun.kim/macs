package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.MemberDevice;
import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Created by ZIPDOC on 2016-10-06.
 */
public interface SendMessageDao {
    public List<MemberDevice> findListByPartnerId(int partner_id) throws DataAccessException;
    public List<MemberDevice> findListByMemberNo(long member_no) throws DataAccessException;
    public List<MemberDevice> findListByMemberId(String member_id) throws DataAccessException;
    public void updateExpiredToken(Long zid) throws DataAccessException;
    public List<MemberDevice> findListByMobileNo(String phone_no) throws DataAccessException;
}

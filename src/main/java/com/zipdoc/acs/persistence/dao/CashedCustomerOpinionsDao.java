package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.VividComment;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface CashedCustomerOpinionsDao {

    public List<VividComment> selectVividComment(Map<String, Object> map) throws DataAccessException;

    public Integer selectVividCommentCount(Map<String, Object> map) throws DataAccessException;
}

package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.Whatever;
import com.zipdoc.acs.model.WhateverListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface WhateverDao {
	public List<Whatever> selectList(WhateverListRequest searchCondition) throws DataAccessException;
	public int selectListTotalCount(WhateverListRequest searchCondition) throws DataAccessException;
	public Whatever selectById(long cno) throws DataAccessException;
	public void create(Whatever whatever) throws DataAccessException;
	public void delete(long cno) throws DataAccessException;
	public void updateIncreaseViewCnt(long cno) throws DataAccessException;
	public void deleteReply(long cno) throws DataAccessException;
}

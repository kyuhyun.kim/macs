package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.BenefitRequest;
import com.zipdoc.acs.domain.entity.Magazine;
import com.zipdoc.acs.domain.entity.News;
import com.zipdoc.acs.model.MagazineListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface BenefitDao {

	void insertBenefit(BenefitRequest request) throws DataAccessException;
}



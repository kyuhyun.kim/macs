package com.zipdoc.acs.persistence.dao;

import java.util.List;
import java.util.Map;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.*;

import com.zipdoc.acs.model.partners.RecomPartners;
import org.springframework.dao.DataAccessException;

public interface PartnerDao {

	public AppVersion selectVersion(Map<String, String> map) throws DataAccessException;
	
	public int deleteDeviceByDeviceId(String device_id) throws DataAccessException;
	
	public Partner selectPartner(Map<String, Object> map);
	
	public PartnerProduct selectProduct(int product_code);
	
	public Estimate selectEstimate(long estimate_no) throws DataAccessException;
	
	public int countAssignTotal(int partner_id) throws DataAccessException;
	
	public int countAssignResidential(int partner_id) throws DataAccessException;
	
	public int countAssignCommercial(int partner_id) throws DataAccessException;
	
	public int countAssignFree(int partner_id) throws DataAccessException;
	
	public int countAssignAcceptTotal(int partner_id) throws DataAccessException;
	
	public int countAssignRejectTotal(int partner_id) throws DataAccessException;
	
	public int countAssignNew(int partner_id) throws DataAccessException;

	public int countAssignNewAccept(int partner_id) throws DataAccessException;
	
	public int countAssignOngoing(int partner_id) throws DataAccessException;
	
	public PartnerContractSummary summaryContract(int partner_id) throws DataAccessException;
	
	public List<PartnerAssign> selectAssignNew(int partner_id) throws DataAccessException;
	
	public List<PartnerAssign> selectAssignOngoing(PartnerAssignSearchRequest searchCondition) throws DataAccessException;
	
	public PartnerContract selectContractOngoing(Map<String, Object> map) throws DataAccessException;
	
	public List<PartnerAssign> selectAssignCompletion(PartnerAssignSearchRequest searchCondition) throws DataAccessException;
	
	public PartnerContract selectContractCompletion(Map<String, Object> map) throws DataAccessException;
	
	public List<PartnerAssign> selectAssignWithdraw(PartnerAssignSearchRequest searchCondition) throws DataAccessException;

	public List<PartnerAssignGeo> selectAssignWithdrawGeo(int partner_id) throws DataAccessException;

	public List<PartnerAssignGeo> selectCSbehindContractList(int partner_id) throws DataAccessException;

	public PartnerAssign selectAssign(Map<String, Object> map) throws DataAccessException;
	
	public PartnerAssign selectAssignDetail(Map<String, Object> map) throws DataAccessException;
	
	public List<PartnerAssign> selectAssignAll(Map<String, Integer> map) throws DataAccessException;
	
	public int acceptAssign(PartnerAssign assign) throws DataAccessException;
	
	public int updateAssignByMap(Map<String, Object> map) throws DataAccessException;
	
	public int restoreAssign(Map<String, Object> map) throws DataAccessException;
	
	public List<PartnerContract> selectContractList_estimateNo(long estimate_no) throws DataAccessException;
	
	public PartnerContract selectContract(long contract_no) throws DataAccessException;
	
	public PartnerContract selectContract_estimateNo(Map<String, Object> map) throws DataAccessException;

	PartnerContract selectContract_Cancel(Map<String, Object> map) throws DataAccessException;
	
	public int createContract(PartnerContract contract) throws DataAccessException;
	
	public int updateContract_agreementStatus(PartnerContract contract) throws DataAccessException;
	
	public int updateContract_app(PartnerContractModify modify) throws DataAccessException;
	
	public int updateContract_start(Map<String, Object> map) throws DataAccessException;
	
	public int updateContract_stop(Map<String, Object> map) throws DataAccessException;
	
	public int restoreContract(long contract_no) throws DataAccessException;
	
	public int updateEstimate_contract(Map<String, Object> map) throws DataAccessException;
	
	public List<PartnerContract> selectUnpaidContract(Map<String, Integer> map) throws DataAccessException;
	
	public PartnerContractSummary summaryUnpaidContract(Map<String, Integer> map) throws DataAccessException;
	
	public int createContractHistory(PartnerContractHistory history) throws DataAccessException;
	
	public int createContractHistoryFile(PartnerContractHistoryFile file) throws DataAccessException;
	
	public List<PartnerContractHistory> selectContractHistoryByContractNo(long contract_no) throws DataAccessException;
	
	public List<PartnerContractHistoryFile> selectContractHistoryFile(long history_no) throws DataAccessException;
	
	public int createInquiry(PartnerInquiry inquiry) throws DataAccessException;
	
	public List<PartnerInquiry> selectInquiry(Map<String, Object> map) throws DataAccessException;
	
	public int createInquiryFile(PartnerInquiryFile file) throws DataAccessException;
	
	public List<PartnerInquiryFile> selectInquiryFile(long cno) throws DataAccessException;

	public List<PartnerAssign> selectAssignStopTargetList(Map<String, Object> map) throws DataAccessException;

	public void updateAssignStop(PartnerAssign partnerAssign) throws DataAccessException;

	public void updateEstimateStatus(Map<String, Object> map) throws DataAccessException;

	public void updateContractFileInfo(PartnerContract contract) throws DataAccessException;

	public void updateAssignMemo(PartnerAssign partnerAssign) throws DataAccessException;

	public void updateAssignRetryCall(PartnerAssign partnerAssign) throws DataAccessException;

	public void insertCallCounselStatus(CallEstimate callEstimate) throws DataAccessException;

	public List<Long> selectCallEstimateList(Map<String, Object> condition) throws DataAccessException;

	public int selectListTotalCount(ListRequest searchCondition) throws DataAccessException;

	public List<PartnerFull> selectList(ListRequest searchCondition) throws DataAccessException;

	public PartnerFull selectPartnerDetail(Integer partner_id) throws DataAccessException;

	public PartnerStatsResponse selectPartnerEvaluationStats(ListRequest searchCondition) throws DataAccessException;

	public PartnerStatsResponse selectPartnerContractStats(ListRequest searchCondition) throws DataAccessException;

	public List<PartnerProductStats> selectPartnerProductsRanks(ListRequest searchCondition) throws DataAccessException;

	// 2018.05.24. KJB. 파트너사 블랙리스트 전화번호 검색
	PartnerBlackList selectBlackList(String mobile_no) throws DataAccessException;

	// 2018.08.21. KJB. 파트너 목록 조회
	int selectPartnerListCount(PartnerListRequest condition) throws DataAccessException;
	int selectPartnerListCount(Map<String, Object> condition) throws DataAccessException;

	List<Partner> selectPartnerList(PartnerListRequest condition) throws DataAccessException;

	List<Partner> selectPartnerListRevision(Map<String, Object> condition) throws DataAccessException;

	int selectPartnerListCountRevision(Map<String, Object> condition) throws DataAccessException;

	List<Partner> selectPartnerListRevision(PartnerListRequest condition) throws DataAccessException;

	List<Partner> selectMonthBestPartnerList(Map<String, Object> condition) throws DataAccessException;

	List<Partner> selectMonthRecoBestPartnerList(Map<String, Object> condition) throws DataAccessException;

	List<Partner> selectQuarterRecoBestPartnerList(Map<String, Object> condition) throws DataAccessException;

	// 파트너 추천 월 조회
	List<RecomPartners> selectMonthBestPartner(Map<String, Object> condition) throws DataAccessException;

	/* 파트너 추천 분기 별 조회 */
	List<RecomPartners> selectQuarterBestPartner(Map<String, Object> condition) throws DataAccessException;

	// 2019.01.14 월별 추천 파트너 조회
	List<Partner> selectMonthPartnerList(Map<String, Object> condition) throws DataAccessException;

	List<Partner> selectQuarterPartnerList(Map<String, Object> condition) throws DataAccessException;

	int selectQuarterPartnerListCount(Map<String, Object> condition) throws DataAccessException;

	int selectMonthPartnerListCount(Map<String, Object> condition) throws DataAccessException;

	int selectReliefPartnerListCount() throws DataAccessException;

	List<Partner> selectReliefPartner(Map<String, Object> condition) throws DataAccessException;





}

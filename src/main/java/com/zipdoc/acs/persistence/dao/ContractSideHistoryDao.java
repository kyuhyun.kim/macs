package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ContractSideHistory;
import org.springframework.dao.DataAccessException;

/**
 * Created by dskim on 2017. 9. 22..
 */
public interface ContractSideHistoryDao {

    public int createContractSideHistory(ContractSideHistory contractSideHistory) throws DataAccessException;

}

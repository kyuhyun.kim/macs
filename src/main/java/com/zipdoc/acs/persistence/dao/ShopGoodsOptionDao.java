package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ShopGoodsOption;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface ShopGoodsOptionDao {
		public List<ShopGoodsOption> findList(Long goods_no) throws DataAccessException;
		public List<ShopGoodsOption> findOptionConCatList(Long goods_no) throws DataAccessException;
//		public ShopGoodsOption findById(Long goods_no) throws DataAccessException;
		public void updateStockCnt(ShopGoodsOption shopGoodsOption) throws DataAccessException;
}

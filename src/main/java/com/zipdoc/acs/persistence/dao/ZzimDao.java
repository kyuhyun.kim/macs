package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.ItemListRequest;
import com.zipdoc.acs.model.ProductListRequest;
import com.zipdoc.acs.model.ZzimListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface ZzimDao {

	List<Product> selectZzimList(ZzimListRequest searchCondition) throws DataAccessException;
	int selectZzimListTotalCount(ZzimListRequest searchCondition) throws DataAccessException;
	void createZzim(Zzim zzim) throws DataAccessException;
	int deleteZzim(Zzim zzim) throws DataAccessException;


	/**
	 * 스크랩북의 스크랩을 복수건으로 삭제
	 * @param zzim
	 * @return
	 * @throws DataAccessException
     */
	int deleteZzimMulti(Zzim zzim) throws DataAccessException;

	/**
	 * 스크랩북의 스크랩을 일괄삭제
	 * @param zzim
	 * @return
	 * @throws DataAccessException
	 */
	int deleteZzimAll(Zzim zzim) throws DataAccessException;
	/**
	 * 해당 사용자가 찜한 건수가 있는지 조회
	 * @param zzim
	 * @return
	 * @throws DataAccessException
     */
	int selectZzimCount(Zzim zzim) throws DataAccessException;

	List<Long> selectZzimPidList(Map<String, Object> map) throws DataAccessException;

	ZzimBook selectZzimBook(long book_no) throws DataAccessException;
	int selectZzimBookListTotalCount(ZzimListRequest searchCondition) throws DataAccessException;
	List<ZzimBook> selectZzimBookList(ZzimListRequest searchCondition) throws DataAccessException;
	void createZzimBook(ZzimBook zzimBook) throws DataAccessException;
	void deleteZzimBook(ZzimBook zzimBook) throws DataAccessException;
	void deleteZzimByBookNo(ZzimBook zzimBook) throws DataAccessException;

	int selectZzimItemListTotalCount(ZzimListRequest searchCondition) throws DataAccessException;
	List<Item> selectZzimItemList(ZzimListRequest searchCondition) throws DataAccessException;

}

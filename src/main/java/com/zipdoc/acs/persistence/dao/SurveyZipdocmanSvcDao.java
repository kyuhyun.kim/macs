package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.SatisfactionRequest;
import org.springframework.dao.DataAccessException;

public interface SurveyZipdocmanSvcDao {

	public SatisfactionRequest selectSurveyZipdocmanSvc(SatisfactionRequest satisfactionRequest) throws DataAccessException;

	public void createSurveyZipdocmanSvc(SatisfactionRequest satisfactionRequest) throws DataAccessException;


}

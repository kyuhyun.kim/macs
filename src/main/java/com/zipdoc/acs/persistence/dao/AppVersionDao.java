package com.zipdoc.acs.persistence.dao;

import org.springframework.dao.DataAccessException;

import com.zipdoc.acs.domain.entity.AppVersion;

import java.util.Map;

public interface AppVersionDao {
	public AppVersion selectVersion(Map<String, String> map) throws DataAccessException;
}

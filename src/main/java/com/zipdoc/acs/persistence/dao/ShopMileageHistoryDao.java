package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ShopMileageHistory;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

/**
 * Created by dskim on 2017. 3. 3..
 */
public interface ShopMileageHistoryDao {

    int findListCount(Map<String, Object> condition) throws DataAccessException;
    List<ShopMileageHistory> findShopMileageHistoryList(Map<String, Object> condition) throws DataAccessException;

    ShopMileageHistory findByCurrentShopMileageHistory(long member_no) throws DataAccessException;

    void insertShopMileageHistory(ShopMileageHistory shopReservesHistory) throws DataAccessException;
}

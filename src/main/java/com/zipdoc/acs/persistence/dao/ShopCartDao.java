package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ShopCart;
import com.zipdoc.acs.model.shop.ShopCartRequest;

import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;


public interface ShopCartDao {
	List<ShopCart> findList(ShopCartRequest condition) throws DataAccessException;
	int findListCount(ShopCartRequest condition) throws DataAccessException;
	void create(ShopCart shopCart) throws DataAccessException;
	ShopCart findById(Map<String, Object> condition) throws DataAccessException;
	void updateCnt(Map<String, Object> condition) throws DataAccessException;
	void delete(Map<String, Object> condition) throws DataAccessException;
	void deleteCartAll(Map<String, Object> condition) throws DataAccessException;
	void updateOption(Map<String, Object> condition) throws DataAccessException;
	void deleteBecauseNoStock(long sno) throws DataAccessException;
	void deleteBecauseSingleGoodsNoStock(long goods_no) throws DataAccessException;
	void deleteCartMulti(ShopCart cart) throws DataAccessException;
}

package com.zipdoc.acs.persistence.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.ArticleListRequest;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

public interface CommonDao {

	List<RecommendKeyword> selectRecommendKeywordList() throws DataAccessException;

	List<EventPromotion> selectEventPromotionList(int channel) throws DataAccessException;

	List<AddressStep> selectAddressStepList() throws DataAccessException;

	List<Category> selectCategoryList() throws DataAccessException;

	List<Category> selectCategoryListForPartner() throws DataAccessException;

	List<InteriorProduct> selectInteriorProductList() throws DataAccessException;

	InteriorProduct selectInteriorProduct(int product_code) throws DataAccessException;

	List<Code> selectCodeList() throws DataAccessException;

	List<PartnerProduct> selectPartnerProducts() throws DataAccessException;

	List<String> selectColorList() throws DataAccessException;

	String selectCategoryName(String category_code) throws DataAccessException;

	RecommendKeyword selectKeywordList(String keyword_type) throws DataAccessException;
}



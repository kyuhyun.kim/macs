package com.zipdoc.acs.persistence.dao;



import com.zipdoc.acs.model.pns.NotificationPhrase;
import org.springframework.dao.DataAccessException;


public interface NotificationPhraseDao {

	/**
	 * LMS 발송 대상 여부 확인.

	 * @return selectNotificationPhrase
	 * @throws org.springframework.dao.DataAccessException
	 */
	NotificationPhrase selectNotificationPhrase(String phrase_cd) throws DataAccessException;

}

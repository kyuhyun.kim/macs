package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.SatisfactionRequest;
import com.zipdoc.acs.domain.entity.ShopZzim;
import com.zipdoc.acs.domain.entity.ShopZzimInfo;
import com.zipdoc.acs.model.shop.ShopZzimListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface ShortUrlMapDao {

	public SatisfactionRequest selectShortUrlMap(SatisfactionRequest satisfactionRequest) throws DataAccessException;

	public void createShortUrlMap(SatisfactionRequest satisfactionRequest) throws DataAccessException;

	public void createSurveyZipdocmanSvc(SatisfactionRequest satisfactionRequest) throws DataAccessException;



}

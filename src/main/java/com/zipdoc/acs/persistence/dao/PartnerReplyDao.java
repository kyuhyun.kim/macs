package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.Product;
import com.zipdoc.acs.domain.entity.PartnerReply;
import com.zipdoc.acs.model.PartnerReplyListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface PartnerReplyDao {
	public List<PartnerReply> selectPartnerReplyList(PartnerReplyListRequest searchCondition) throws DataAccessException;
	public int selectPartnerReplyListTotalCount(PartnerReplyListRequest searchCondition) throws DataAccessException;
	public void createPartnerReply(PartnerReply partnerReply) throws DataAccessException;
	public void updatePartnerReply(PartnerReply partnerReply) throws DataAccessException;
	public void deletePartnerReply(PartnerReply partnerReply) throws DataAccessException;
}

package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.CrashLog;
import org.springframework.dao.DataAccessException;

public interface CrashLogDao {
	public void createCrashLog(CrashLog crashLog) throws DataAccessException;
}

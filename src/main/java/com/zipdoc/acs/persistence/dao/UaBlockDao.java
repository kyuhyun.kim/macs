package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.CrashLog;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface UaBlockDao {
	List<String> getUaKeywordList() throws DataAccessException;
}

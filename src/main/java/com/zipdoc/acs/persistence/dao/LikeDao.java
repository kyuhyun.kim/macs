package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.Like;
import org.springframework.dao.DataAccessException;

public interface LikeDao {
	public int findCount(Like like) throws DataAccessException;
	public void create(Like like) throws DataAccessException;
	public void delete(Like like) throws DataAccessException;
}

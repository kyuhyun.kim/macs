package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ShopOrderPayHistory;
import org.springframework.dao.DataAccessException;

public interface ShopOrderPayHistoryDao {

	void createOrderPayHistory(ShopOrderPayHistory shopOrderPayHistory) throws DataAccessException;

	ShopOrderPayHistory findById(String order_no) throws DataAccessException;

	void updateSettlePrice(String order_no) throws DataAccessException;

}

package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ShopOrderUserInfo;
import org.springframework.dao.DataAccessException;

import java.util.Map;

public interface ShopOrderUserInfoDao {
	void createOrderUserInfo(ShopOrderUserInfo shopOrderUserInfo) throws DataAccessException;
	ShopOrderUserInfo findById(String order_no) throws DataAccessException;
	ShopOrderUserInfo findByNonMember(Map<String, Object> condition) throws DataAccessException;
	ShopOrderUserInfo findByMemberNo(long member_no) throws DataAccessException;
}

package com.zipdoc.acs.persistence.dao;

import java.util.List;
import java.util.Map;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.EstimateListRequest;
import com.zipdoc.acs.model.ListRequest;
import com.zipdoc.acs.model.subs.ZID;
import org.springframework.dao.DataAccessException;

public interface EstimateDao {
	EstimateSummary selectEstimateSummary() throws DataAccessException;
	int createEstimate(Estimate estimate) throws DataAccessException;
	List<MyInterior> selectMyEstimateList(Map<String, Object> map) throws DataAccessException;
	List<Partner> selectAssignedPartnerList(long estimate_no) throws DataAccessException;
	Contract selectContract(long estimate_no) throws DataAccessException;
	void createEstimateConcept(EstimateConcept estimateConcept) throws DataAccessException;
	List<Product> selectEstimateConceptProductList(long estimate_no) throws DataAccessException;
	int selectListTotalCount(ListRequest searchCondition) throws DataAccessException;
	List<Estimate> selectEstimateList(ListRequest searchCondition) throws DataAccessException;

	List<EstimateCounsel> findCounselList(Long estimate_no) throws DataAccessException;

	// 2018.08.03. KJB. 견적 프로모션 코드 조회
	PromotionCode selectPromotionCode(String promotion_code) throws DataAccessException;

	// 마이인테리어 상세조회
    MyEstimate selectMyInterior(Map<String, Object> searchCondition) throws DataAccessException;

    // 마이인테리어 수락 파트너 조회
	List<MyEstimatePartner> selectAssignPartners(long estimate_no) throws DataAccessException;

	// 내 견적 등록
	void updateEstimateOwner(EstimateListRequest searchCondition) throws DataAccessException;

	// 견적 회원 번호에 매칭

	void updateEstimateMember(Map<String, Object> map) throws DataAccessException;

	// 주거 - 웹 메인 견적신청 현황 조회
	List<Estimate> selectWebMainList() throws DataAccessException;

	// 이벤트 알림 약관 상태 변경
	void updateAgreementEstimate(Map<String, Object> map) throws DataAccessException;
}

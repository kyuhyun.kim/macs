package com.zipdoc.acs.persistence.dao;


import org.springframework.dao.DataAccessException;

public interface KeywordSynonymDao {
	String selectKeywordSynonym(String keyword) throws DataAccessException;
}

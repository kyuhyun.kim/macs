package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.EstimateListRequest;
import com.zipdoc.acs.model.ListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface EstimateImgFileDao {
	// 견적 이미지 등록
	void createEstimateImageFile(List<EstimateImgFile> estimateImgFiles) throws DataAccessException;

	List<EstimateImgFile>  selectEstimateImageFile(long estimate_no) throws DataAccessException;
}

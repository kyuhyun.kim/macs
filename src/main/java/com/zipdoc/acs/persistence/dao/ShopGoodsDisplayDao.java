package com.zipdoc.acs.persistence.dao;

import com.zipdoc.acs.domain.entity.ShopGoods;
import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Created by dskim on 2017. 5. 15..
 */
public interface ShopGoodsDisplayDao {

    List<ShopGoods> bestGoodsList() throws DataAccessException;
    List<ShopGoods> newGoodsList() throws DataAccessException;
    List<ShopGoods> packageGoodsList() throws DataAccessException;

}

package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.PictureType;
import com.zipdoc.acs.define.ProductType;
import com.zipdoc.acs.domain.entity.Item;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.Product;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.ProductService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.ProductListRequest;
import com.zipdoc.acs.model.ProductListResponse;
import com.zipdoc.acs.utils.Json;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.regex.Pattern;


/**
 * 내방어때 조회 기능
 * @author 이동규
 *
 */
@Controller
public class ShareController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(ShareController.class);
	
	private static final Pattern urlPattern = Pattern.compile("<img[^>]*src=[\"']?([^>\"']+)[\"']?[^>]*>");
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private ProductService productService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@ResponseBody
	@RequestMapping(value = "/share/{share_type}/{pid}")
	public ResponseEntity<String> list(HttpServletRequest request,
									   @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									   @RequestHeader(value = "ZID", required = true) Long zid,
									   @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									   @PathVariable("share_type") int share_type,
									   @PathVariable("pid") long pid) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, false);

			if(share_type != 0 && share_type != 1){
				log.error("[/community/add] SHARE_TYPE이 유효하지 않음");
				return makeResponseEntity(request, "SHARE_TYPE이 유효하지 않음", HttpStatus.BAD_REQUEST);
			}

			if(pid <=0 ) {
				log.error("[/community/add] PID가 유효하지 않음");
				return makeResponseEntity(request, "PID가 유효하지 않음", HttpStatus.BAD_REQUEST);
			}
			productService.updateIncreaseShareCnt(pid);
			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/add] 내방어때 등록 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

package com.zipdoc.acs.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.zipdoc.acs.domain.service.ContractSideCallService;
import com.zipdoc.acs.model.ContractSideCallRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.domain.service.ContractSideHistoryService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.ContractSideHistoryRequest;
import com.zipdoc.acs.utils.Messages;

/**
 * 이면 계약 의심 등록 API
 * - 위도,경도
 * - 견적받은 해당주소반경에 자주 출몰시 앱에서 api로 보냄
 * <p>
 * Created by dskim on 2017. 9. 21..
 */
@Controller
public class ContractController extends ResponseEntityService {

    private static final Logger log = LoggerFactory.getLogger(ContractHistoryController.class);

    @Autowired
    private ContractSideHistoryService contractSideHistoryService;

    @Autowired
    private ContractSideCallService contractSideCallService;

    @ResponseBody
    @RequestMapping(value = "/contract/side/key", method = RequestMethod.GET)
    public ResponseEntity<String> sendKey(HttpServletRequest request,
                                          @RequestHeader(value = "APP-VERSION", required = true) String app_version,
                                          @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                          @RequestHeader(value = "ZID", required = true) long zid,
                                          @RequestHeader(value = "ZID-KEY", required = true) String zid_key) {
        try {
            Map<String, Object> result = new HashMap<>();
            result.put("aes128_key", Messages.getMessage("AES128.KEY"));
            return makeResponseEntity(request, result, HttpStatus.OK);
        } catch (ServiceException e) {
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch (Exception e) {
            log.error("[CONTRACT-KEY] 이면계약 키 조회 오류.", e);
            return makeResponseEntity(request, "fail to view", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/contract/side/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> contract(HttpServletRequest request,
                                           @RequestHeader(value = "APP-VERSION", required = true) String app_version,
                                           @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                           @RequestHeader(value = "ZID", required = true) long zid,
                                           @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
                                           @RequestBody ContractSideHistoryRequest contractSideHistoryRequest) {
        try {

            log.debug("[CONTRACT-PUT] [" + zid + "]의 이면계약 의심 요청 정보." + contractSideHistoryRequest.toString());

            // Json 필수 파라미터 체크
            if (StringUtils.isEmpty(contractSideHistoryRequest.getPartner_id())) {
                log.error("[CONTRACT-PUT] [" + zid + "]. 필수 항목 오류. [partner_id, latitude, longtitude]");
                return makeResponseEntity(request, "Missing mandotory filed. (partner_id, latitude, longtitude)", HttpStatus.BAD_REQUEST);
            }

            contractSideHistoryService.createContractSideHistory(contractSideHistoryRequest.buildEstimate(zid));

            log.debug("[CONTRACT-PUT] [" + zid + "] 이면계약 의심 등록 완료.");

            return makeResponseEntity(HttpStatus.OK);
        } catch (ServiceException e) {
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch (Exception e) {
            log.error("[CONTRACT-PUT] WEB DB - 이면계약 의심 등록 오류.", e);
            return makeResponseEntity(request, "WEB DB - failed to create", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/contract/side/call/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> doubtCall(HttpServletRequest request,
                                            @RequestHeader(value = "APP-VERSION") String app_version,
                                            @RequestHeader(value = "MEMBER-NO") Long member_no,
                                            @RequestHeader(value = "ZID", required = true) long zid,
                                            @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
                                            @RequestBody ContractSideCallRequest contractSideCallRequest) {
        try {

            log.debug("[CONTRACT-CALL-PUT] [" + zid + "]의 이면계약 의심 통화이력 요청 정보." + contractSideCallRequest.toString());

            //check necessary parameters
            if(contractSideCallRequest == null
                    || StringUtils.isEmpty(contractSideCallRequest.getPartner_id())
                    || StringUtils.isEmpty(contractSideCallRequest.getPartner_phone())
                    || StringUtils.isEmpty(contractSideCallRequest.getWriter_phone())) {
                log.error("[CONTRACT-CALL-PUT] [" + zid + "]. 필수 항목 오류. [partner_id, partner_phone, writer_phone]");
                return makeResponseEntity(request, "Missing mandotory filed. (partner_id, latitude, longtitude)", HttpStatus.BAD_REQUEST);
            }

            contractSideCallService.createContractSideCall(contractSideCallRequest.build(zid));

            log.debug("[CONTRACT-CALL-PUT] [" + zid + "] 이면계약 의심 통화이력 등록 완료.");

            return makeResponseEntity(HttpStatus.OK);
        } catch (ServiceException e) {
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch (Exception e) {
            log.error("[CONTRACT-CALL-PUT] WEB DB - 이면계약 의심 통화이력 등록 오류.", e);
            return makeResponseEntity(request, "WEB DB - failed to create", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

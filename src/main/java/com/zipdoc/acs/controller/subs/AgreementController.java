package com.zipdoc.acs.controller.subs;


import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.ServiceException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AgreementController extends ResponseEntityService {

    private static final Logger log = LoggerFactory.getLogger(AgreementController.class);

    @Autowired
    private MemberService memberService;

    @RequestMapping(value = "/agree", method = RequestMethod.POST)
    public ResponseEntity<String> join(HttpServletRequest request,
                                       @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                       @RequestHeader(value = "ZID", required = false) Long zid) {

        // 1. 파라미터 체크
        if (StringUtils.isEmpty(app_version))
            return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
        if (zid == null || zid <= 0) return makeResponseEntity(request, "Missing header [zid]", HttpStatus.BAD_REQUEST);

        try {

            // 2. 이전 자동 가입 정보 조회
            Member member = memberService.selectMemberByZid(zid);

            if (member != null) {
                memberService.updateDeviceAgree(member);
            }

            return makeResponseEntity(HttpStatus.OK);

        } catch (Exception e) {
            log.error("error" + e);
        }

        return makeResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/agree/privacy", method = RequestMethod.POST)
    public ResponseEntity<String> updateAgreePrivacy(HttpServletRequest request,
                                                  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                                  @RequestHeader(value = "ZID", required = false) Long zid) {

        // 파라미터 체크
        if (StringUtils.isEmpty(app_version))
            return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
        if (zid == null || zid <= 0) return makeResponseEntity(request, "Missing header [zid]", HttpStatus.BAD_REQUEST);

        try {

            // 이전 자동 가입 정보 조회
            Member member = memberService.selectMemberByZid(zid);

            if (member != null) {
                memberService.updateDeviceAgreePrivacy(member);
            }

            return makeResponseEntity(HttpStatus.OK);

        } catch (ServiceException e) {
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch (Exception e) {
            log.error("/estimate/list", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}

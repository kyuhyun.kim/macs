package com.zipdoc.acs.controller.subs;

import javax.servlet.http.HttpServletRequest;

import com.zipdoc.acs.define.Cause;
import com.zipdoc.acs.define.MemberType;
import com.zipdoc.acs.domain.entity.ForgotAccount;
import com.zipdoc.acs.domain.service.EstimateService;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.model.CauseResponse;
import com.zipdoc.acs.model.EstimateListRequest;
import com.zipdoc.acs.utils.*;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.model.AccountType;
import com.zipdoc.acs.model.SubscriberType;
import com.zipdoc.acs.domain.entity.Account;
import com.zipdoc.acs.model.subs.ZID;

/**
 * 기기번경/번호변경등의 처리를 위해 기 가입된 가입정보에 계정을 등록하는 절차 및 로그인 과정
 * @author 이동규
 *
 */
@Controller
public class RegistrationController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(RegistrationController.class);
	
	@Autowired
	private MemberService memberService;

	@Autowired
	private EstimateService estimateService;
	
	@ResponseBody
	@RequestMapping(value = "/registration", method = RequestMethod.POST, produces="application/json;charset=utf-8")
	public ResponseEntity<String> registration(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = false) String app_version,
			@RequestHeader(value = "ZID", required = false) Long zid,
			@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
			@RequestBody Account account) {

		try {

			// 웹 요청인 경우
			if (zid == null) return registration_web(request, account);

			// 파라미터 체크
			if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
			if (StringUtils.isEmpty(zid_key)) return makeResponseEntity(request, "Missing header [zid-key]", HttpStatus.BAD_REQUEST);

			// 가입자 조회
			Member member = memberService.selectMemberByZid(zid);

			// 가입자 정보가 없을 경우
			if (member == null) {
				log.error("[REGISTRATION] ZID["+zid+"] 가입자는 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[REGISTRATION] ZID["+zid+"] 가입자의 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 계정이 이미 등록되어 있을 경우
			if (member.getAccount_type() != AccountType.NO_ACCOUNT.value()) {
				log.error("[REGISTRATION] ZID["+zid+"]. 계정["+account.getAccount()+"/"+account.getUsername()+"/"+AccountType.valueOf(member.getAccount_type())+"]을 이미 사용중입니다.");
				return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
			}

			// 동일한 계정이 이미 등록되어 있을 경우
			if (memberService.IsExistAccount(account.getAccount())) {
				log.error("[REGISTRATION] ZID["+zid+"]. 이미 사용중인 계정["+account.getAccount()+"]입니다.");
				return makeResponseEntity(HttpStatus.CONFLICT);
			}

			// 계정 사용자 구분 검사 (개인 또는 업체)
			SubscriberType subsType = SubscriberType.valueOf(account.getSubsType());
			if (subsType != SubscriberType.PERSONAL && subsType != SubscriberType.CORPORATE) {
				log.error("[REGISTRATION] ZID["+zid+"]의 subs_type("+account.getSubsType()+")을 처리 할 수 없습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 계정 구분 검사 (ZIPDOC, FACEBOOK, KAKAO, NAVER, GOOGLE)
			AccountType accoutType = AccountType.valueOf(account.getAccountType());
			if (accoutType == AccountType.NO_ACCOUNT) {
				log.error("[REGISTRATION] ZID["+zid+"]의 account_type["+account.getAccountType()+"]을 처리 할 수 없습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 정보 업데이트
			account.setZid(zid);

			member.setMember_id( ACSUtils.getReplaceEmoji(account.getAccount()));
			member.setPassword(account.getPassword());
			member.setAccount_type(account.getAccountType());
			member.setUsername(ACSUtils.getReplaceEmoji(account.getUsername()));
			member.setUser_level(account.getSubsType());
			member.setMember_type(account.getSubsType());
			member.setLogin_ip(IpUtil.getClientIP(request));

			//2018.1.2 장한솔 (마케팅수신동의 추가)
			member.setAgreement_marketing(account.getAgreement_marketing());

			//이메일 계정인 경우 계정아이디를 이메일에 세팅한다.
			if(accoutType==AccountType.ACCOUNT_ZIPDOC) {
				member.setEmail(account.getAccount());
			}

			memberService.treatMember(member);

			log.debug("[REGISTRATION] ZID["+zid+"] 계정 등록 완료"+System.lineSeparator()+Json.toStringJson(account));

			ZID result = new ZID(zid, zid_key);
			account.setMember_no(member.getMember_no());
			result.setAccount(account);
			return makeResponseEntity(request, result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Regisition Error", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private ResponseEntity<String> registration_web(HttpServletRequest request, Account account) {

		try {

			// 파라미터 체크
			if (StringUtils.isEmpty(account.getAccount())) {
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "Missing parameters [account]"), HttpStatus.NOT_ACCEPTABLE);
			}
			if (StringUtils.isEmpty(account.getPassword())) {
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "Missing parameters [password]"), HttpStatus.NOT_ACCEPTABLE);
			}
			if (StringUtils.isEmpty(account.getUsername())) {
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "Missing parameters [name]"), HttpStatus.NOT_ACCEPTABLE);
			}
			if (StringUtils.isEmpty(account.getMobile_no())) {
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "Missing parameters [mobile_no]"), HttpStatus.NOT_ACCEPTABLE);
			}

			// 동일한 계정이 이미 등록되어 있을 경우
			if (memberService.IsExistAccount(account.getAccount())) {
				log.error("[REGISTRATION-WEB] 이미 사용중인 계정["+account.getAccount()+"]입니다.");
				return makeResponseEntity(request, new CauseResponse(Cause.CONFLICT_MEMBER_ID), HttpStatus.NOT_ACCEPTABLE);
			}

			// 계정 사용자 구분 검사 (개인)
			SubscriberType subsType = SubscriberType.valueOf(account.getSubsType());
			if (subsType != SubscriberType.PERSONAL) {
				log.error("[REGISTRATION-WEB] subs_type("+account.getSubsType()+")을 처리 할 수 없습니다.");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "[subs_type]"), HttpStatus.NOT_ACCEPTABLE);
			}

			// 계정 구분 검사 (ZIPDOC, FACEBOOK, KAKAO, NAVER, GOOGLE)
			AccountType accoutType = AccountType.valueOf(account.getAccountType());
			if (accoutType == AccountType.NO_ACCOUNT) {
				log.error("[REGISTRATION-WEB] account_type["+account.getAccountType()+"]을 처리 할 수 없습니다.");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "[account_type]"), HttpStatus.NOT_ACCEPTABLE);
			}

			// 비밀번호 유효성 검사
			if (!ACSUtils.validate_password_for_user(account.getPassword())) {
				log.error("[REGISTRATION-WEB] 비밀번호 복잡도 오류.");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PWD_USER), HttpStatus.NOT_ACCEPTABLE);
			}

			// 회원 가입
			Member member = new Member();

			member.setMember_id(account.getAccount());
			member.setPassword(account.getPassword());
			member.setAccount_type(account.getAccountType());
			member.setUsername(account.getUsername());
			member.setMobile_no(account.getMobile_no());
			member.setMember_type(account.getSubsType());
			member.setLogin_ip(IpUtil.getClientIP(request));
			member.setAgreement_marketing(account.getAgreement_marketing());

			// 이메일 계정인 경우 계정아이디를 이메일에 세팅한다.
			if (accoutType == AccountType.ACCOUNT_ZIPDOC) {
				member.setEmail(account.getAccount());
			}

			memberService.treatMember(member);
			account.setMember_no(member.getMember_no());
			account.setPassword(null);

			log.debug("[REGISTRATION-WEB] 계정 등록 완료 ["+account.getAccount()+"]");

			// 이름과 전화번호로 신청한 견적을 업데이트한다.
			EstimateListRequest owner = new EstimateListRequest();

			// 견적 업데이트
			owner.setWriter(member.getUsername());
			owner.setPhone_no(member.getMobile_no());
			owner.setMember_no(member.getMember_no());
			estimateService.updateEstimateOwner(owner);

			return makeResponseEntity(request, account, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Regisition Error", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ResponseBody
	@RequestMapping(value = "/withdraw", method = RequestMethod.POST)
	public ResponseEntity<String> withdraw(HttpServletRequest request,
											   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
											   @RequestHeader(value = "ZID", required = false) Long zid,
											   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
											   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										       @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token) {
		try {

			// 웹 요청인 경우
			if (StringUtils.isNotEmpty(web_token)) return withdraw_web(web_token);

			// 파라미터 체크
			if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
			if (zid == null) return makeResponseEntity(request, "Missing header [zid]", HttpStatus.BAD_REQUEST);
			if (StringUtils.isEmpty(zid_key)) return makeResponseEntity(request, "Missing header [zid-key]", HttpStatus.BAD_REQUEST);
			if (member_no == null) return makeResponseEntity(request, "Missing header [member-no]", HttpStatus.BAD_REQUEST);

			// 가입자 조회
			Member member = memberService.selectMember(member_no, zid);

			// 가입자 정보가 없을 경우
			if (member == null) {
				log.error("[WITHDRAW] ZID["+zid+"] 가입자는 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[WITHDRAW] ZID["+zid+"] 가입자의 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 개인회원이 아닌 경우
			if (member.getMember_type() == null || member.getMember_type() != MemberType.USER.getCode()) {
				log.error("[WITHDRAW] MEMBER-NO["+member_no+"] 가입자는 개인회원이 아닙니다.");
				return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
			}

			// 자동가입 회원상태로 변경
			member.setUser_level(SubscriberType.AUTO.value());

			// 계정 삭제
			memberService.deleteMember(member);
			log.debug("[WITHDRAW] 계정 삭제 완료. MEMBER-NO["+member.getMember_no()+"] ID["+member.getMember_id()+"]");

			return makeResponseEntity(HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("[WITHDRAW] Error", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public ResponseEntity<String> withdraw_web(String web_token) {

		try {

			// 가입자 조회
			Member member = memberService.selectMemberByWebToken(web_token);

			// 가입자 정보가 없을 경우
			if (member == null) {
				log.error("[WITHDRAW-WEB] TOKEN["+web_token+"]값에 등록된 가입자는 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 자동가입 회원상태로 변경
			member.setUser_level(SubscriberType.AUTO.value());

			// 계정 삭제
			memberService.deleteMember(member);
			log.debug("[WITHDRAW-WEB] 계정 삭제 완료. MEMBER-NO["+member.getMember_no()+"] ID["+member.getMember_id()+"]");

			return makeResponseEntity(HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("[WITHDRAW-WEB] Error", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ResponseBody
	@RequestMapping(value = "/forgot/account", method = RequestMethod.POST)
	public ResponseEntity<String> forgot_account(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
												 @RequestHeader(value = "ZID", required = false) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												 @RequestBody ForgotAccount account) {
		try {

			// 파라미터 체크 (이름 / 휴대폰 번호)
			if (StringUtils.isEmpty(account.getUsername())) {
				log.error("[FORGOT-ID] 필수항목누락. [USERNAME]");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "USERNAME"), HttpStatus.NOT_ACCEPTABLE);
			}

			if (StringUtils.isEmpty(account.getMobile_no())) {
				log.error("[FORGOT-ID] 필수항목누락. [MOBILE-NO]");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "MOBILE-NO"), HttpStatus.NOT_ACCEPTABLE);
			}

			// 이메일 NULL 처리
			account.setEmail(null);

			// 가입자 조회
			Member member = memberService.selectMemberByForgotAccount(account);

			// 가입자 정보가 없을 경우
			if (member == null) {
				log.error("[FORGOT-ID] 이름["+account.getUsername()+"] 휴대폰번호["+account.getMobile_no()+"]로 검색된 가입자는 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			ForgotAccount result = new ForgotAccount();
			result.setEmail(ACSUtils.maskingCharsStartWith(member.getMember_id(), "*", 2));

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("[FORGOT-ID] Error", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ResponseBody
	@RequestMapping(value = "/forgot/password", method = RequestMethod.POST)
	public ResponseEntity<String> forgot_password(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
												 @RequestHeader(value = "ZID", required = false) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												 @RequestBody ForgotAccount account) {
		try {

			if (StringUtils.isEmpty(account.getEmail())) {
				log.error("[FORGOT-PWD] 필수항목누락. [E-MAIL]");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "E-MAIL"), HttpStatus.NOT_ACCEPTABLE);
			}

			// 파라미터 체크 (이메일 / 이름)
			if (StringUtils.isEmpty(account.getUsername())) {
				log.error("[FORGOT-PWD] 필수항목누락. [USERNAME]");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "USERNAME"), HttpStatus.NOT_ACCEPTABLE);
			}

			// 휴대폰 번호 NULL 처리
			account.setMobile_no(null);

			// 가입자 조회
			Member member = memberService.selectMemberByForgotAccount(account);

			// 가입자 정보가 없을 경우
			if (member == null) {
				log.error("[FORGOT-PWD] 이메일["+account.getEmail()+"] 이름["+account.getUsername()+"]로 검색된 가입자는 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			// 임시비밀번호 및 메일본문 생성
			String sendMsgFormat = Messages.getMessage("MAIL.PASSWORD.CHANGE");
			String sendMsg = String.format(sendMsgFormat, RandomStringUtils.randomAlphanumeric(12));

			// 이메일 전송
			SendZipdocMail.send(account.getEmail(), account.getUsername(), "[집닥]비밀번호 변경 안내", sendMsg);

			return makeResponseEntity(HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("[FORGOT-ID] Error", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

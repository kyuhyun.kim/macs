package com.zipdoc.acs.controller.subs;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.define.AppType;
import com.zipdoc.acs.define.MemberStatus;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.EstimateService;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.AccountType;
import com.zipdoc.acs.model.SubscriberType;
import com.zipdoc.acs.model.subs.ZID;
import com.zipdoc.acs.utils.ACSUtils;
import com.zipdoc.acs.utils.IpUtil;
import com.zipdoc.acs.utils.RandomString;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 기기번경/번호변경등의 처리를 위해 기 가입된 가입정보에 계정을 등록하는 절차 및 로그인 과정
 * @author 이동규
 *
 */
@Controller
public class LoginController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private MemberService memberService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@Autowired
	private EstimateService estimateService;




	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces="application/json;charset=utf-8")
	public ResponseEntity<String> login(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = false) String app_version,
			@RequestHeader(value = "ZID", required = false) Long zid,
			@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
			@RequestBody Account account) {
		
		// 파라미터 체크
		if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
		if (zid == null) return makeResponseEntity(request, "Missing header [zid]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(zid_key)) return makeResponseEntity(request, "Missing header [zid-key]", HttpStatus.BAD_REQUEST);

		// 계정 사용자 구분 검사 (개인 또는 업체)
		SubscriberType subsType = SubscriberType.valueOf(account.getSubsType());
		if (subsType != SubscriberType.PERSONAL &&
				subsType != SubscriberType.CORPORATE) {
			log.error("[REGISTRATION] ZID["+zid+"]의 subs_type("+account.getSubsType()+")을 처리 할 수 없습니다.");
			return makeResponseEntity(HttpStatus.BAD_REQUEST);
		}

		// 계정 구분 검사 (ZIPDOC, FACEBOOK, KAKAO, NAVER, GOOGLE)
		AccountType accoutType = AccountType.valueOf(account.getAccountType());
		if (accoutType == AccountType.NO_ACCOUNT) {
			log.error("[REGISTRATION] ZID["+zid+"]의 account_type["+account.getAccountType()+"]을 처리 할 수 없습니다.");
			return makeResponseEntity(HttpStatus.BAD_REQUEST);
		}

		// 가입자 조회
		Member device = memberService.selectMemberByZid(zid);
		
		// 가입자 정보가 없을 경우
		if (device == null) {
			log.error("[LOGIN] ZID["+zid+"] 가입자는 존재하지 않습니다.");
			return makeResponseEntity(HttpStatus.FORBIDDEN);
		}
		
		// 인증키 오류
		if (device.getZid_key() == null || !device.getZid_key().equals(zid_key)) {
			log.error("[LOGIN] ZID["+zid+"] 가입자의 인증키가 일치하지 않습니다.");
			return makeResponseEntity(HttpStatus.FORBIDDEN);
		}

		log.info("이모지 제거 전 username[{}] account[{}]", account.getUsername(), account.getAccount());
		String username = ACSUtils.getReplaceEmoji(account.getUsername() );
		String account_name = ACSUtils.getReplaceEmoji( account.getAccount() );

		account.setUsername(username);
		account.setAccount(account_name);

		log.info("이모지 제거 후 username[{}] account_name[{}]", username, account_name);


		// 계정명으로 가입자 정보를 조회한다.
		Member accountMember = memberService.selectMemberByAccount(account);
		
		// 계정명으로 가입자가 검색되지 않을 경우
		if (accountMember == null) {
			if(accoutType == AccountType.ACCOUNT_ZIPDOC) { //집닥 계정인 경우
				log.error("[LOGIN] ZID[" + zid + "] 요청 계정[" + account.getAccount() + "] 또는 비밀번호가 유효하지 않습니다.");
				return makeResponseEntity(HttpStatus.NOT_FOUND);
			} else { //oAuth 로그인인 경우 회원 가입 처리한다.(ZIPDOC, FACEBOOK, KAKAO, NAVER, GOOGLE)
				accountMember = new Member();
				accountMember.setZid(zid);
				accountMember.setMobile_no(device.getMobile_no());
				accountMember.setMember_id(account.getAccount());
				//웹에서 자동로그인 처리를 위해 비빌번호를 oAuth ID값으로 세팅한다.
				accountMember.setPassword(account.getAccount());
				accountMember.setAccount_type(account.getAccountType());
				accountMember.setUsername(ACSUtils.getReplaceEmoji( account.getUsername() ));
				accountMember.setUser_level(account.getSubsType());
				accountMember.setMember_type(account.getSubsType());
				accountMember.setLogin_ip(IpUtil.getClientIP(request));

				//이메일 계정인 경우 계정아이디를 이메일에 세팅한다.
				if(accoutType==AccountType.ACCOUNT_ZIPDOC) {
					accountMember.setEmail(ACSUtils.getReplaceEmoji(account.getAccount()));
				}

				memberService.treatMember(accountMember);
			}
		} else {

			if(accountMember.getStatus() == MemberStatus.WITHDRAWAL.getCode()) {
				log.error("[LOGIN] ZID["+zid+"] 탈퇴한 계정입니다.");
				return makeResponseEntity(HttpStatus.UNAUTHORIZED);
			}

			accountMember.setZid(zid);
			if(device.getUser_level() == SubscriberType.AUTO.value()) { //자동가입 고객으로 되어있는 경우 갱신 처리
				accountMember.setUser_level(account.getSubsType());
				memberService.updateDeviceUserLevel(accountMember);
			} else {
				accountMember.setUser_level(device.getUser_level());
			}
			//T_MEMBER_DEVICE 정보를 등록 처리한다.
			memberService.treatMemberDevice(accountMember);

			//마지막 로그인 정보를 갱신 처리한다.
			accountMember.setLogin_ip(IpUtil.getClientIP(request));
			memberService.updateMemberLoginInfo(accountMember);
		}
		
		ZID result = new ZID(zid, zid_key);
		account.setMember_no(accountMember.getMember_no());
		account.setSubsType(accountMember.getUser_level());
		account.setAccountType(accountMember.getAccount_type());
		account.setUsername(accountMember.getUsername());
		account.setPassword(null); // Hidden
		
		result.setAccount(account);
		
		if (zid != accountMember.getZid()) {
			log.debug("[LOGIN] ZID["+zid+" => "+accountMember.getZid()+"]변경.");
		}

		/* 해당 ZID 로 매핑 된 견적 모두 login 계정으로 활성화 반영 */
		estimateService.updateEstimateMember( result );
		
		return makeResponseEntity(request, result, HttpStatus.OK);
	}

	/**
	 * CMS 관리자 계정 로그인
	 * @param request
	 * @param app_version
	 * @param os_type
	 * @param os_version
	 * @param device_id
	 * @param device_model
	 * @param mdn
	 * @param token
	 * @param account
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/cms/login", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<String> cms_login(HttpServletRequest request,
										@RequestHeader(value = "APP-VERSION", required = false) String app_version,
										@RequestParam(value = "os_type", required = false) String os_type,
										@RequestParam(value = "os_version", required = false) String os_version,
										@RequestParam(value = "device_id", required = false) String device_id,
										@RequestParam(value = "device_model", required = false) String device_model,
										@RequestParam(value = "mdn", required = false) String mdn,
										@RequestParam(value = "token", required = false) String token,
										@RequestBody Account account) {
		try {

			// 헤더 검사
			if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);

			// 파라미터 체크
			if (StringUtils.isEmpty(os_type)) return makeResponseEntity(request, "Missing parameter [os-type]", HttpStatus.BAD_REQUEST);
			if (StringUtils.isEmpty(os_version)) return makeResponseEntity(request, "Missing parameter [os-version]", HttpStatus.BAD_REQUEST);
			if (StringUtils.isEmpty(device_id)) return makeResponseEntity(request, "Missing parameter [device-id]", HttpStatus.BAD_REQUEST);
			if (StringUtils.isEmpty(device_model)) return makeResponseEntity(request, "Missing parameter [device-model]", HttpStatus.BAD_REQUEST);

			// 이전 가입 정보 조회
			Member device = memberService.selectDeviceByDeviceId(device_id, AppType.CMS.value());
			Member accountMember = memberService.selectMemberByAccount(account);

			// 디바이스 정보가 없는 경우 생성
			if (device == null) {
				device = new Member(-1, generationKey(), mdn, app_version, os_type, os_version, device_id, device_model, token);
				int rows = memberService.createDevice(device);
				if (rows == 0) {
					log.error("[CMS-LOGIN] MDN[" + mdn + "] 디바이스 등록에 실패했습니다.");
					return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}

			// 조회된 계정이 없을 경우, 로그인 불가
			if (accountMember == null) {
				log.error("[CMS-LOGIN] 요청 계정[" + account.getAccount() + "]이 없거나 비밀번호가 유효하지 않습니다.");
				return makeResponseEntity(HttpStatus.NOT_FOUND);
			}

			// 가입자 권한 확인 (3: 관리자) (T_MEMBER의 member_type)
			SubscriberType subsType = SubscriberType.valueOf(accountMember.getMember_type());
			if (subsType != SubscriberType.ADMIN) {
				log.error("[CMS-LOGIN] 요청 계정[" + account.getAccount() + "/" + subsType + "]은 어드민 계정이 아닙니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 계정 상태 확인 (1 : 정상)
			if (accountMember.getStatus() != 1) {
				log.error("[CMS-LOGIN] 요청 계정[" + account.getAccount() + "/" + accountMember.getStatus() + "]은 휴면/차단/탈퇴 상태입니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 집닥맨 구성원 확인
			ZipdocmanUser user = memberService.selectZipdocmanUser(accountMember.getMember_no());
			if (user == null) {
				log.error("[CMS-LOGIN] 요청 계정[" + account.getAccount() + "/" + accountMember.getStatus() + "]은 집닥맨앱 구성원이 아닙니다..");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 토큰 정보가 다를 경우 갱신
			if (!StringUtils.isEmpty(token) && !StringUtils.equals(device.getPush_token(), token)) {
				device.setPush_token(token);
				memberService.updateToken(device);
			}

			// 계정과 디바이스 매핑 정보 등록
			accountMember.setZid(device.getZid());
			if (device.getUser_level() == SubscriberType.AUTO.value()) { // 자동가입 고객으로 되어있는 경우 갱신 처리
				accountMember.setUser_level(subsType.value());
				memberService.updateDeviceUserLevel(accountMember);
			} else {
				accountMember.setUser_level(device.getUser_level());
			}

			// 디바이스 정보 갱신
			accountMember.setApp_version(app_version);
			accountMember.setMobile_no(mdn);
			accountMember.setOs_type(os_type);
			accountMember.setOs_version(os_version);
			accountMember.setDevice_id(device_id);
			accountMember.setDevice_model(device_model);
			memberService.updateDevice(accountMember);

			// T_MEMBER_DEVICE 정보를 등록
			memberService.treatMemberDevice(accountMember);

			// 마지막 로그인 정보를 갱신 처리한다.
			accountMember.setLogin_ip(IpUtil.getClientIP(request));
			memberService.updateMemberLoginInfo(accountMember);

			ZID result = new ZID(device.getZid(), device.getZid_key());
			account.setMember_no(accountMember.getMember_no());
			account.setSubsType(accountMember.getUser_level());
			account.setAccountType(accountMember.getAccount_type());
			account.setUsername(accountMember.getUsername());
			account.setPassword(null); // Hidden

			result.setAccount(account);
			result.setZipdocman_account(user);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[CMS-LOGIN] 실패 ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/setup/alarm")
	public ResponseEntity<String> setup(HttpServletRequest request,
										@RequestHeader(value = "APP-VERSION", required = true) String app_version,
										@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										@RequestHeader(value = "ZID", required = true) Long zid,
										@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
										@RequestBody(required = false) Alarm  alarm_info
	) {
		try {

			// 파라미터 체크
			if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
			if (StringUtils.isEmpty(zid_key)) return makeResponseEntity(request, "Missing header [zid-key]", HttpStatus.BAD_REQUEST);
			if(  alarm_info == null) {
				log.error("[PARTNER-SETUP-ALRAM] AlRAM-INFO 필수 요청 항목 누락.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);

			}
			// 가입자 조회
			Member member = memberService.selectMemberByZid(zid);

			// 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[REGISTRATION] ZID["+zid+"] 가입자의 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}


			alarm_info.getAlarm_info().setZid(zid);
			alarm_info.getAlarm_info().setMember_no( member_no );

			memberService.updateDeviceAlarm(alarm_info.getAlarm_info());

			return makeResponseEntity(request, memberService.selectDeviceAlarm( member ), HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("환경 설정 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private String generationKey() {
		RandomString randomString = new RandomString();
		return randomString.getString(16, "");
	}
}

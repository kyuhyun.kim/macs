package com.zipdoc.acs.controller.subs;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.PartnerBlackList;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.PartnerService;

import com.zipdoc.acs.utils.IpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.AppVersion;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.Partner;
import com.zipdoc.acs.model.PartnerType;
import com.zipdoc.acs.model.SubscriberType;
import com.zipdoc.acs.domain.entity.Account;
import com.zipdoc.acs.model.subs.AuthRes;
import com.zipdoc.acs.model.subs.ZID;
import com.zipdoc.acs.utils.RandomString;

/**
 * 웹에서 생성한 계정으로 파트너사의 단말에서 로그인 하는 절차
 * @author 김종부
 *
 */

@Controller
public class PartnersLoginController extends ResponseEntityService {
	
	private static final Logger log = LoggerFactory.getLogger(PartnersLoginController.class);
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private PartnerService partnerService;

	@ResponseBody
	@RequestMapping(value = "/partner/login", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<String> login(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = false) String app_version,
			@RequestParam(value = "os_type", required = false) String os_type,
			@RequestParam(value = "os_version", required = false) String os_version,
			@RequestParam(value = "device_id", required = false) String device_id,
			@RequestParam(value = "device_model", required = false) String device_model,
			@RequestParam(value = "mdn", required = false) String mdn,
			@RequestParam(value = "token", required = false) String token,
			@RequestBody Account account) {
		
		// 헤더 검사
		if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
		
		// 파라미터 검사
		if (StringUtils.isEmpty(os_type)) return makeResponseEntity(request, "Missing parameter [os-type]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(os_version)) return makeResponseEntity(request, "Missing parameter [os-version]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(device_id)) return makeResponseEntity(request, "Missing parameter [device-id]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(device_model)) return makeResponseEntity(request, "Missing parameter [device-model]", HttpStatus.BAD_REQUEST);

		// 이전 자동 가입 정보 조회
		Member device = memberService.selectDeviceByDeviceId(device_id, AppType.PARTNER.value());
		
		// 이전 자동 가입 정보가 없는 경우
		if (device == null) {
			device = new Member(
					-1,
					generationKey(),
					mdn,
					app_version,
					os_type,
					os_version,
					device_id,
					device_model,
					token);

			int rows = memberService.createDevice(device);
			if (rows == 0) {
				log.error("[PARTNER-LOGIN] MDN[" + mdn + "] 디바이스 등록에 실패했습니다.");
				return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}  else { // 이전 기기 정보가 있는 경우
			//푸시토큰이 불일치한는 경우 토큰값을 갱신한다.
			if(StringUtils.isNotEmpty(token) && !StringUtils.equals(device.getPush_token(), token)){
				device.setPush_token(token);
				memberService.updateToken(device);
			}
		}
		
		// 계정명으로 가입자 정보를 조회한다.
		Member accountMember = memberService.selectMemberByAccount(account);

		// 계정명으로 가입자가 검색되지 않을 경우. 파트너 계정은 웹을 통해 미리 등록되어 있어야 한다.
		if (accountMember == null) {
			log.error("[PARTNER-LOGIN] 요청 계정[" + account.getAccount() + "]이 없거나 비밀번호가 유효하지 않습니다.");
			return makeResponseEntity(HttpStatus.NOT_FOUND);
		}
		
		// 계정 사용자 구분 검사. 파트너 계정이 아닌 경우
		SubscriberType subsType = SubscriberType.valueOf(accountMember.getMember_type());
		if (subsType != SubscriberType.CORPORATE) {
			log.error("[PARTNER-LOGIN] 요청 계정[" + account.getAccount() + "/"+subsType+"]은 파트너 계정이 아닙니다.");
			return makeResponseEntity(HttpStatus.FORBIDDEN);
		}
		
		// 2017.12.14. 장한솔
		// 회원탈퇴만 처리하도록 변경
		if (accountMember.getStatus() != null && accountMember.getStatus() == MemberStatus.WITHDRAWAL.getCode()) {
			log.error("[PARTNER-LOGIN] 요청 계정[" + account.getAccount() + "/"+accountMember.getStatus()+"]은 탈퇴 상태입니다.");
			return makeResponseEntity(HttpStatus.UNAUTHORIZED);
		}
		
		// 파트너 업체 정보 조회
		Partner partner = partnerService.selectPartner(accountMember.getPartner_id());
		
		// 파트너 사 승인 여부
		if (partner == null || partner.getPartner_type() == PartnerType.UNAPPROVED.value()) {
			log.error("[PARTNER-LOGIN] 요청 계정[" + account.getAccount() + "/"+accountMember.getStatus()+"]은 파트너 가입자가 아니거나 미승인 상태입니다.");
			return makeResponseEntity(HttpStatus.FORBIDDEN);
		}
		
		partner.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
		
		// 계정과 디바이스 매핑 정보 등록
		accountMember.setZid(device.getZid());
		if (device.getUser_level() == SubscriberType.AUTO.value()) { // 자동가입 고객으로 되어있는 경우 갱신 처리
			accountMember.setUser_level(subsType.value());
			memberService.updateDeviceUserLevel(accountMember);
		} else {
			accountMember.setUser_level(device.getUser_level());
		}
		
		// T_MEMBER_DEVICE 정보를 등록 처리한다.
		memberService.treatMemberDevice(accountMember);

		//마지막 로그인 정보를 갱신 처리한다.
		accountMember.setLogin_ip(IpUtil.getClientIP(request));
		memberService.updateMemberLoginInfo(accountMember);
		
		ZID result = new ZID(device.getZid(), device.getZid_key());
		account.setMember_no(accountMember.getMember_no());
		account.setSubsType(accountMember.getUser_level());
		account.setAccountType(accountMember.getAccount_type());
		account.setUsername(accountMember.getUsername());
		account.setPassword(null); // Hidden
		
		// 파트너 업체 정보
		account.setPartner(partner);
		
		result.setAccount(account);
		
		return makeResponseEntity(request, result, HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/partner/authentication", method = RequestMethod.GET)
	public ResponseEntity<String> authentication(HttpServletRequest request,
		 	@RequestHeader(value = "APP-VERSION", required = false) String app_version,
		 	@RequestHeader(value = "APP-MARKET", required = false) String app_market,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = false) Long zid,
			@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
			@RequestParam(value = "os_type", required = false) String os_type,
			@RequestParam(value = "os_version", required = false) String os_version,
			@RequestParam(value = "device_id", required = false) String device_id,
			@RequestParam(value = "device_model", required = false) String device_model,
		    @RequestParam(value = "mdn", required = false) String mdn) {
		
		// 1. 파라미터 체크
		if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
		if (zid == null || zid <= 0) return makeResponseEntity(request, "Missing header [zid]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(zid_key)) return makeResponseEntity(request, "Missing header [zid-key]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(os_type)) return makeResponseEntity(request, "Missing parameter [os-type]", HttpStatus.BAD_REQUEST);

		try {

			OsType reqOsType = OsType.get(os_type);
			if (reqOsType==null) return makeResponseEntity(request, "Invalid value [os-type]", HttpStatus.BAD_REQUEST);

			// APP 버전 조회
			AppVersion version = partnerService.selectVersion(os_type, StringUtils.defaultIfEmpty(app_market, "google"));
			
			// OS구분에 따른 버전 정보가 조회되지 않을 경우
			if (version == null) {
				log.error("[PARTNER-AUTH] ZID["+zid+"] MEMBER_NO["+member_no+"]. 시스템에 OS["+os_type+"]을 위한 APP-VERSION정보가 설정되어 있지 않습니다.");
				return makeResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
			}
			
			// 가입자 조회
			Member member = memberService.selectMember(member_no, zid);
			
			// 가입자 정보가 없을 경우
			if (member == null) {
				log.error("[PARTNER-AUTH] 회원번호["+member_no+"] 또는 디바이스["+zid+"]가 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.NOT_FOUND);
			}

			// 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[PARTNER-AUTH] ["+member.getMember_id()+"]의 ZID["+zid+"] 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 회원상태가 미사용, 휴면, 차단, 탈퇴의 경우
			OsType osType = OsType.get(os_type);
			if ((osType == OsType.ANDROID && !member.isAccountNormal())||
					osType == OsType.IOS && member_no != null && !member.isAccountNormal()) {
				log.error("[PARTNER-AUTH] ["+member.getMember_id()+"]의 STATUS["+member.getStatus()+"]가 유효하지 않습니다.");

				return makeResponseEntity(HttpStatus.UNAUTHORIZED);
			}

			// 파트너 업체 정보 조회
			Partner partner = partnerService.selectPartner(member.getPartner_id());

			// 파트너 사 승인 여부
			if (partner == null || partner.getPartner_type() == PartnerType.UNAPPROVED.value()) {
				log.error("[PARTNER-AUTH] 요청 계정[" + member.getMember_id()+"]은 파트너 가입자가 아니거나 미승인 상태입니다.");
				return makeResponseEntity(HttpStatus.UNAUTHORIZED);
			}

			// 파트너 사 계약 유지 여부
			if (partner.getStatus() == PartnerStatus.END.getCode()) {
				log.error("[PARTNER-AUTH] 요청 계정[" + member.getMember_id()+"]은 계약종료 상태입니다.");
				return makeResponseEntity(HttpStatus.UNAUTHORIZED);
			}

			// 블랙리스트 검사
			if (member.getMobile_no() != null) {
				PartnerBlackList BL = partnerService.selectBlackList(member.getMobile_no());
				if (BL != null) {
					log.error("[PARTNER-AUTH] [BL-CHECK] 요청 계정[" + member.getMember_id()+"]에서 사용하는 전화번호["+
							member.getMobile_no()+"]는 "+BL.getCreater()+"님이 로그인을 차단하였습니다.");
					return makeResponseEntity(HttpStatus.UNAUTHORIZED);
				}
			}

			if(member.isAccountNormal()){
				// 로그인 정보를 갱신 처리한다.
				member.setLogin_ip(IpUtil.getClientIP(request));
				memberService.updateMemberLoginInfo(member);

				//회원디바이스 정보의 앱버전과 다른 경우 디바이스 정보를 변경한다.
				if(!StringUtils.equals(member.getApp_version(), app_version)) {
					member.setApp_version(app_version);
					member.setMobile_no(mdn);
					member.setOs_type(os_type);
					member.setOs_version(os_version);
					member.setDevice_id(device_id);
					member.setDevice_model(device_model);
					memberService.updateDevice(member);
				}
			}

			AuthRes result = new AuthRes(version, member);
			
			// 약관 URL
			Map<String, String> urls = new HashMap<String, String>();
			
			urls.put("agreement_info", Constants.AGREEMENT_URL);
			urls.put("privacy_info", Constants.PRIVACY_URL);
			
			// URL 설정
			result.setUrl_info(urls);
			
			// 인증 결과. 강제 업데이트 여부에 따라 응답 코드 구분 (200 OK / 406 NOT ACCEPTABLE)
            return makeResponseEntity(request, result, (version.isForceUpdate(app_version) ? HttpStatus.NOT_ACCEPTABLE : HttpStatus.OK));
			
		} catch (Exception e) {
			log.error("[PARTNER-AUTH]. 인증오류.", e);
		}
		
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
    private String generationKey() {
        RandomString randomString = new RandomString();
        return randomString.getString(16, "");
    }
}

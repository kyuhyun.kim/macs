package com.zipdoc.acs.controller.subs;

import javax.servlet.http.HttpServletRequest;

import com.zipdoc.acs.define.MemberStatus;
import com.zipdoc.acs.define.OsType;
import com.zipdoc.acs.domain.entity.AlarmInfo;
import com.zipdoc.acs.domain.service.AppVersionService;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.UaBlockService;
import com.zipdoc.acs.model.SubscriberType;
import com.zipdoc.acs.utils.IpUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.AppVersion;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.model.subs.AuthRes;

import java.util.HashMap;
import java.util.Map;

/**
 * 앱 실행 시 유효한 가입 정보인지 확인하는 절차
 * @author 이동규
 *
 */
@Controller
public class AuthenticationController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(AuthenticationController.class);

	private static final String DEFAULT_APP_MARKET = "google";

	@Autowired
	private MemberService memberService;
	
	@Autowired
	private AppVersionService versionService;

	@Autowired
	private UaBlockService uaBlockService;

	/**
	 * 1.0 앱 사용자가 인증을 요청할 경우 처리하기 위한 임시 소스임.
	 * 2.0 오픈 후 일정 시간 유지 후 삭제할 것
	 * @param request
	 * @param app_version
	 * @param zid
	 * @param zid_key
	 * @param os_type
	 * @param os_version
	 * @param device_id
     * @param device_model
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/auth", method = RequestMethod.GET)
	public ResponseEntity<String> auth(HttpServletRequest request,
									   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
									   @RequestHeader(value = "ZID", required = false) Long zid,
									   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
									   @RequestParam(value = "os-type", required = false) String os_type,
									   @RequestParam(value = "os-version", required = false) String os_version,
									   @RequestParam(value = "device-id", required = false) String device_id,
									   @RequestParam(value = "device-model", required = false) String device_model) {

		// 1. 파라미터 체크
		if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
		if (zid == null || zid <= 0) return makeResponseEntity(request, "Missing header [zid]", HttpStatus.BAD_REQUEST);
		//if (StringUtils.isEmpty(zid_key)) return makeResponseEntity(request, "Missing header [zid-key]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(os_type)) return makeResponseEntity(request, "Missing parameter [os-type]", HttpStatus.BAD_REQUEST);

		try {

			// 2. APP 버전 조회
			AppVersion version = versionService.selectVersion(os_type, DEFAULT_APP_MARKET);

			// 2.1 OS구분에 따른 버전 정보가 조회되지 않을 경우
			if (version == null) {
				log.error("[AUTH] ZID["+zid+"]. 시스템에 OS["+os_type+"]을 위한 APP-VERSION정보가 설정되어 있지 않습니다.");
				return makeResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
			}

			Map<String, Object> result = new HashMap<String, Object>();
			Map<String, String> appUpdate = new HashMap<String, String>();

			appUpdate.put("app-version", version.getApp_version());
			appUpdate.put("market-url", version.getDownload_url());
			appUpdate.put("description", version.getDescription());

			result.put("app-update", appUpdate);

			// 3.강제 업데이트 여부에 따라 응답 코드 구분 (200 OK / 406 NOT ACCEPTABLE)
			return makeResponseEntity(request, result, (version.isForceUpdate(app_version) ? HttpStatus.NOT_ACCEPTABLE : HttpStatus.OK));

		} catch (Exception e) {
			log.error("error"+e);
		}

		return makeResponseEntity(HttpStatus.BAD_REQUEST);
	}

	@ResponseBody
	@RequestMapping(value = "/authentication", method = RequestMethod.GET)
	public ResponseEntity<String> authentication(HttpServletRequest request,
		 	@RequestHeader(value = "APP-VERSION", required = false) String app_version,
		 	@RequestHeader(value = "APP-MARKET", required = false) String app_market,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = false) Long zid,
			@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
			@RequestHeader(value = "user-agent", required = false) String user_agent,
			@RequestParam(value = "os_type", required = false) String os_type,
			@RequestParam(value = "os_version", required = false) String os_version,
			@RequestParam(value = "device_id", required = false) String device_id,
			@RequestParam(value = "device_model", required = false) String device_model,
		    @RequestParam(value = "mdn", required = false) String mdn) {

		// 1. 파라미터 체크
		if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
		if (zid == null || zid <= 0) return makeResponseEntity(request, "Missing header [zid]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(zid_key)) return makeResponseEntity(request, "Missing header [zid-key]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(os_type)) return makeResponseEntity(request, "Missing parameter [os-type]", HttpStatus.BAD_REQUEST);

		//TODO 버전 올라가고나면 주석 해제할 것
		//if (StringUtils.isEmpty(app_market)) return makeResponseEntity(request, "Missing header [App-Market]", HttpStatus.BAD_REQUEST);

		// 옵션 파라미터
		//if (StringUtils.isEmpty(os_version)) return makeResponseEntity(request, "Missing parameter [os-version]", HttpStatus.BAD_REQUEST);
		//if (StringUtils.isEmpty(device_id)) return makeResponseEntity(request, "Missing parameter [device-id]", HttpStatus.BAD_REQUEST);
		//if (StringUtils.isEmpty(device_model)) return makeResponseEntity(request, "Missing parameter [device-model]", HttpStatus.BAD_REQUEST);
		
		try {

			OsType reqOsType = OsType.get(os_type);
			if(reqOsType==null) return makeResponseEntity(request, "Invalid value [os-type]", HttpStatus.BAD_REQUEST);

			// 2. APP 버전 조회
			AppVersion version = versionService.selectVersion(os_type, StringUtils.defaultIfEmpty(app_market, DEFAULT_APP_MARKET));
			
			// 2.1 OS구분에 따른 버전 정보가 조회되지 않을 경우
			if (version == null) {
				log.error("[AUTH] ZID["+zid+"] MEMBER_NO["+member_no+"]. 시스템에 OS["+os_type+"]을 위한 APP-VERSION정보가 설정되어 있지 않습니다.");
				return makeResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
			}
			
			// 3. 가입자 조회
			Member member = memberService.selectMember(member_no, zid);

			// 3.1 가입자 정보가 없을 경우
			if (member == null) {
				//MEMBER_NO가 0보다 큰 경우 가입자 정보를 조회 후 T_MEMBER_DEVICE 정보를 생성한다.
				if(member_no != null && member_no > 0){
					member = memberService.selectMemberByMemberId(member_no);
					if(member == null) { //T_MEMBER 테이블에 정보가 없는 경우 에러 처리
						log.error("[AUTH] ZID["+zid+"] MEMBER_NO["+member_no+"]가입자는 존재하지 않습니다.");
						return makeResponseEntity(HttpStatus.FORBIDDEN);
					} else { //T_MEMBER 테이블에 정보가 존재하는 경우 T_DEVICE 정보를 조회 후 둘다 존재하는 경우 T_MEMBER_DEVICE 테이블에 등록한다.
						Member device = memberService.selectMemberByZid(zid);
						if(device == null) {
							log.error("[AUTH] ZID["+zid+"] MEMBER_NO["+member_no+"]가입자는 존재하지 않습니다.");
							return makeResponseEntity(HttpStatus.FORBIDDEN);
						}
						//T_MEMBER_DEVICE 정보가 없는 것으로 간주하고 등록 처리한다.
						member.setZid(zid);
						member.setZid_key(device.getZid_key());
						memberService.treatMemberDevice(member);

						//자동가입 고객으로 되어있는 경우 갱신 처리
						if(device.getUser_level() == SubscriberType.AUTO.value()) {
							device.setUser_level(SubscriberType.PERSONAL.value());
							memberService.updateDeviceUserLevel(device);
						}
					}
				}
			}
			
			// 3.2 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[AUTH] ZID["+zid+"] MEMBER_NO["+member_no+"] 가입자의 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			if(member_no != null && member.getMember_no() != null && member.getMember_no() > 0){

				//탈퇴한 회원일 경우
				if(member.getStatus() == MemberStatus.WITHDRAWAL.getCode()) {
					log.error("[AUTH] ZID["+zid+"] MEMBER_NO["+member_no+"] 탈퇴한 계정입니다.");
					return makeResponseEntity(HttpStatus.UNAUTHORIZED);
				}

				//자동로그인으로 간주하고 로그인 정보를 갱신 처리한다.
				member.setLogin_ip(IpUtil.getClientIP(request));
				memberService.updateMemberLoginInfo(member);
			}

			//회원디바이스 정보의 앱버전과 다른 경우 디바이스 정보를 변경한다.
			if(!StringUtils.equals(member.getApp_version(), app_version)) {
				member.setApp_version(app_version);
				member.setMobile_no(mdn);
				member.setOs_type(os_type);
				member.setOs_version(os_version);
				member.setDevice_id(device_id);
				member.setDevice_model(device_model);
				memberService.updateDevice(member);
			}

			/* 알람 설정 여부 확인 */

			AlarmInfo alarmInfo = memberService.selectDeviceAlarm(member);
			AuthRes result = new AuthRes(version, member, alarmInfo);
			
			// 4. 인증 결과. 강제 업데이트 여부에 따라 응답 코드 구분 (200 OK / 406 NOT ACCEPTABLE)
            return makeResponseEntity(request, result, (version.isForceUpdate(app_version) ? HttpStatus.NOT_ACCEPTABLE : HttpStatus.OK));
			
		} catch (Exception e) {
			log.error("error "+e.toString());
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/push/{token}", method = RequestMethod.POST)
	public ResponseEntity<String> push(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = false) String app_version,
		    @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = false) Long zid,
			@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
			@PathVariable("token") String token) {
		
		// 1. 파라미터 체크
		if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
		if (zid == null || zid <= 0) return makeResponseEntity(request, "Missing header [zid]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(zid_key)) return makeResponseEntity(request, "Missing header [zid-key]", HttpStatus.BAD_REQUEST);
		
		try {
			
			// 가입자 조회
			Member member = memberService.selectMember(member_no, zid);
			
			// 가입자 정보가 없을 경우
			if (member == null) {
				log.error("[PUSH] ZID["+zid+"] 가입자는 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}
			
			// 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[PUSH] ZID["+zid+"] 가입자의 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}
			
			// 가입자 정보 업데이트
			member.setPush_token(token);
			int rows = memberService.updateToken(member);
			
			if (rows == 0) {
				log.error("[PUSH] ZID["+zid+"] PUSH-TOKEN["+token+"] 등록 실패.");
				return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			log.debug("[PUSH] ZID["+zid+"] PUSH-TOKEN["+token+"] 등록 완료.");
			
			return makeResponseEntity(HttpStatus.OK);
			
		} catch (Exception e) {
			log.error("error"+e);
		}
		
		return makeResponseEntity(HttpStatus.BAD_REQUEST);
	}
}

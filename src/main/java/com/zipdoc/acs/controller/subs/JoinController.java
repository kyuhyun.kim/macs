package com.zipdoc.acs.controller.subs;

import javax.servlet.http.HttpServletRequest;

import com.zipdoc.acs.define.AppType;
import com.zipdoc.acs.domain.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.model.subs.JoinReq;
import com.zipdoc.acs.model.subs.ZID;
import com.zipdoc.acs.utils.RandomString;

/**
 * 앱 설치 후 디바이스 정보를 포함하여 전화번호 기반으로 서비스 가입하는 절차
 * @author 이동규
 *
 */

@Controller
public class JoinController extends ResponseEntityService {
	
	private static final Logger log = LoggerFactory.getLogger(JoinController.class);
	
	@Autowired
	private MemberService memberService;

	@ResponseBody
	@RequestMapping(value = "/join", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<String> join(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = false) String app_version,
			@RequestParam(value = "os_type", required = false) String os_type,
			@RequestParam(value = "os_version", required = false) String os_version,
			@RequestParam(value = "device_id", required = false) String device_id,
			@RequestParam(value = "device_model", required = false) String device_model,
			@RequestBody JoinReq body) {
		
		// 1. 파라미터 체크
		if (StringUtils.isEmpty(app_version)) return makeResponseEntity(request, "Missing header [app-version]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(os_type)) return makeResponseEntity(request, "Missing parameter [os-type]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(os_version)) return makeResponseEntity(request, "Missing parameter [os-version]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(device_id)) return makeResponseEntity(request, "Missing parameter [device-id]", HttpStatus.BAD_REQUEST);
		if (StringUtils.isEmpty(device_model)) return makeResponseEntity(request, "Missing parameter [device-model]", HttpStatus.BAD_REQUEST);
		
		try {

			// 3. 이전 자동 가입 정보 조회
			Member member = memberService.selectDeviceByDeviceId(device_id, AppType.ZIPDOC.value());

			// 4. 자동 가입(이전 자동 가입 정보가 없는 경우)
			if (member == null) {

				member = new Member(
						-1,
						generationKey(),
						body.getMdn(),
						app_version,
						os_type,
						os_version,
						device_id,
						device_model,
						body.getPush_token());

				int rows = memberService.createDevice(member);
				if (rows == 0) {
					log.error("[JOIN] MDN[" + body.getMdn() + "] 가입이 실패하였습니다.");
					return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}

			else {
				// TODO. 2018.04.09. KJB. 이 경우 변경된 값으로 업데이트해야 하지 않을까 ?
			}
			
			ZID result = new ZID(member.getZid(), member.getZid_key());
			return makeResponseEntity(request, result, HttpStatus.OK);
			
		} catch (Exception e) {
			log.error("error"+e);
		}
		
		return makeResponseEntity(HttpStatus.BAD_REQUEST);
	}

    private String generationKey() {
        RandomString randomString = new RandomString();
        return randomString.getString(16, "");
    }
}

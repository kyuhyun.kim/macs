package com.zipdoc.acs.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.zipdoc.acs.Async.AsyncService;
import com.zipdoc.acs.define.*;
import com.zipdoc.acs.define.pns.MsgEnableType;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.*;
import com.zipdoc.acs.model.*;
import com.zipdoc.acs.persistence.dao.EstimateImgFileDao;
import com.zipdoc.acs.utils.*;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class EstimateController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(EstimateController.class);

	private static final Logger keywordLog = LoggerFactory.getLogger("KEYWORD");

	@Autowired
	private AcsConfig acsConfig;

	@Autowired
	private EstimateService estimateService;

	@Autowired
	private CommonService commonService;

	@Autowired
	private ProductService productService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@Autowired
	private EstimateImgFileDao estimateImgFileDao;

	@Autowired
	private AsyncService asyncService;


	@Value("${UPLOAD_FILE.TEMPORARY_PATH}")
	private String temporary_path;
	
	@Value("${ESTIMATE_UPLOAD_FILE.ABSOLUTE_PATH}")
	private String absolute_path;
	
	@Value("${ESTIMATE_EXPOSURE_DAY_RANGE}")
	private int estimate_exposure_day_range;


	/*@ResponseBody
	@RequestMapping(value = "/estimate/summary", method = RequestMethod.GET)
	public ResponseEntity<String> summary(HttpServletRequest request,
											   @RequestHeader(value = "APP-VERSION", required = true) String app_version,
											   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											   @RequestHeader(value = "ZID", required = true) long zid,
											   @RequestHeader(value = "ZID-KEY", required = true) String zid_key) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			EstimateSummary summary = estimateService.selectEstimateSummary();
			return makeResponseEntity(request, summary, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("내부 오류.", e);
			return makeResponseEntity(request, "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	*//**
	 * 외부 인터페이스를 통한 견적 요청에 대한 처리(JSON)
	 * @param request
	 * @param channel
	 * @param estimate
	 * @return
	 *//*
	@ResponseBody
	@RequestMapping(value = "/external/estimateInterface", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> estimateInterfaceJson(HttpServletRequest request,
										   @RequestHeader(value = "CHANNEL", required = true) String channel,
										   @RequestBody ExternalEstimate estimate) {
		return estimateInterface(request, channel, estimate);
	}

	*//**
	 * 외부 인터페이스를 통한 견적 요청에 대한 처리(HTML)
	 * @param request
	 * @param channel
	 * @return
	 *//*
	@ResponseBody
	@RequestMapping(value = "/external/estimateInterface", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<String> estimateInterfaceHtml(HttpServletRequest request, HttpSession session,
														@RequestHeader(value = "CHANNEL", required = true) String channel,
														@ModelAttribute ExternalEstimate estimate) {
	 	return estimateInterface(request, channel, estimate);
	}

	public ResponseEntity<String> estimateInterface(HttpServletRequest request,
														String channel,
														ExternalEstimate estimate) {
		// 채널 정보가 없을 경우
		if (StringUtils.isEmpty(channel)) {
			log.error("[ESTIMATE-PUT] CHANNEL["+channel+"] 채널은 존재하지 않습니다.");
			return makeResponseEntity(HttpStatus.FORBIDDEN);
		}

		estimate.setChannel(Channel.EXTERNAL.value());
		if(StringUtils.equals(channel, "band") || StringUtils.equals(channel, "E_BAND") || StringUtils.equals(channel, "app_479")|| StringUtils.equals(channel, "app_420")|| StringUtils.equals(channel, "app_3834")){
			estimate.setChannel_sub_code(ChannelSubCode.BAND.getCode());
		} else if(StringUtils.equals(channel, "app_580")){
			estimate.setChannel_sub_code(ChannelSubCode.OLD_WORLD.getCode());
		} else if(StringUtils.equals(channel, "app_3270")){
			estimate.setChannel_sub_code(ChannelSubCode.CONTENTS_CLUSTER.getCode());
		} else if(StringUtils.equals(channel, "app_3835")) {
			estimate.setChannel_sub_code(ChannelSubCode.KAKAO.getCode());
		} else if(StringUtils.equals(channel, "app_3836")) {
			estimate.setChannel_sub_code(ChannelSubCode.OKCASHBAG.getCode());
		} else if(StringUtils.equals(channel, "app_3837")){
			estimate.setChannel_sub_code(ChannelSubCode.FACEBOOK.getCode());
		} else if(StringUtils.equals(channel, "app_3838")) {
			estimate.setChannel_sub_code(ChannelSubCode.REPORTER.getCode());
		} else if(StringUtils.equals(channel, "BNAVER") || StringUtils.equals(channel, "CNAVER") || StringUtils.equals(channel, "KNAVER") || StringUtils.equals(channel, "NPNAVER")||StringUtils.equals(channel, "NBNAVER")||StringUtils.equals(channel, "TISTORY")||StringUtils.equals(channel, "CDAUM")||StringUtils.equals(channel, "NCDAUM")|| StringUtils.equals(channel, "FACEBOOK")){
			estimate.setChannel_sub_code(ChannelSubCode.INNO.getCode());
		} else if(StringUtils.equals(channel, "ETC")) {
			estimate.setChannel_sub_code(ChannelSubCode.ETC.getCode());
		} else if(StringUtils.equals(channel, "kca")) {
			estimate.setChannel_sub_code(ChannelSubCode.KCA.getCode());
		} else if(StringUtils.equals(channel, "app_3982") || StringUtils.equals(channel, "app_4038") || StringUtils.equals(channel, "app_4307") || StringUtils.equals(channel, "app_3882")) {
			estimate.setChannel_sub_code(ChannelSubCode.LABS1.getCode());
		} else if(StringUtils.equals(channel, "mixnfix")) {
			estimate.setChannel_sub_code(ChannelSubCode.MIXNFIX.getCode());
		}

		// 로그 출력
		try {
			log.debug("[ESTIMATE-PUT] ["+channel+"]의 견적 요청 정보."+System.lineSeparator()+estimate.toString());
		} catch (Exception e) {
			log.error("[ESTIMATE-PUT] ["+channel+"]의 견적 요청 정보 오류.", e);
			return makeResponseEntity(request, "Json Parsing failed.", HttpStatus.BAD_REQUEST);
		}

		// Json 필수 파라미터 체크 (writer, phone_no)
		if (StringUtils.isEmpty(estimate.getWriter()) ||
				StringUtils.isEmpty(estimate.getPhone_no())) {

			log.error("[ESTIMATE-PUT] ["+channel+"]. 필수 항목 오류. [writer, phone_no]");
			return makeResponseEntity(request, "Missing mandotory filed. (writer, phone_no)", HttpStatus.BAD_REQUEST);
		}

		try {

			//신청상품을 기본형으로 설정
			estimate.setInterior_product_code(Constants.DEFAULT_INTERIOR_PRODUCT_CODE);
			estimate.setPassword(RandomStringUtils.randomAlphanumeric(15));

			// SUBJECT 처리. 웹에 노출되기 때문에 강제로 변경
			estimate.setSubject("서울 아파트 인테리어");

			// DB 등록
			int fid = estimateService.createEstimate(estimate);
			if (fid == 0) {
				log.error("[ESTIMATE-PUT] ["+channel+"]. WEB DB - 견적 등록 실패. ["+Json.toStringJson(estimate)+"]");
				return makeResponseEntity(request, "WEB DB - failed to create", HttpStatus.INTERNAL_SERVER_ERROR);
			}

			log.debug("[ESTIMATE-PUT] ["+channel+"] 견적 등록 완료. FID["+fid+"] ["+Json.toStringJson(estimate)+"]");

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[ESTIMATE] ["+estimate.toString()+"]. WEB DB - 견적 등록 오류.", e);
			return makeResponseEntity(request, "WEB DB - failed to create", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
*/
	/**
	 * 견적 요청에 대한 처리
	 * @param request
	 * @param app_version
	 * @param zid
	 * @param zid_key
	 * @param estimateRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/estimate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> estimate(HttpServletRequest request,
										   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										   @RequestHeader(value = "ZID", required = false) Long zid,
										   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										   @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										   @RequestBody EstimateRequest estimateRequest
	) {
		try {

			Member member = null;

			// 비회원 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
			}

			// 단말 유형
			AgentType agentType = zid == null ? AgentType.WEB : AgentType.APP;

			// 파라미터 검사
			String missing_parameters = null;
			if ((missing_parameters = estimateRequest.validate(agentType)) != null) {
				log.error("[ESTIMATE-PUT] 필수 항목 오류.");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, missing_parameters), HttpStatus.NOT_ACCEPTABLE);
			}

			// 견적명 자동 생성을 위해 CATEGORY_CODE2 표시명을 설정한다.
			if (estimateRequest.getCategory_code2() != null) {
				estimateRequest.setCategory_name2(commonService.selectCategoryName(estimateRequest.getCategory_code2()));
			}
			Estimate estimate = estimateRequest.buildEstimate();

			// 단말 IP
			estimate.setIp(IpUtil.getClientIP(request));
			// 접속 단말 유형
			estimate.setChannel(agentType == AgentType.APP ? Channel.APP.value() : Channel.WEB.value());
			// 견적상태. 신청접수
			estimate.setStatus(EstimateStatus.REGISTER.getCode());

			// DB 등록
			if (estimateService.createEstimate(estimate) == 0) {
				log.error("[ESTIMATE-PUT] [" + estimate.getPhone_no() + "]. 견적 등록 실패.");
				return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
			}

			log.debug("[ESTIMATE-PUT] [" + estimate.getPhone_no() + "] 견적 등록 완료. ESTIMATE-NO=" + estimate.getEstimate_no());

			return makeResponseEntity(request, estimate, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[ESTIMATE-PUT] 견적 등록 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[ESTIMATE-PUT] 견적 등록 오류.", e);
			return makeResponseEntity(request, "WEB DB - failed to create", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


/*	*//**
	 * 사진이 포함된 견적 요청에 대한 처리
	 * @param request
	 * @param app_version
	 * @param zid
	 * @param zid_key
	 * @param
	 * @return
	 *//*
	@ResponseBody
	@RequestMapping(value = "/estimate/image")
	public ResponseEntity<String> estimate_image(HttpServletRequest request,
										   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										   @RequestHeader(value = "ZID", required = false) Long zid,
										   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										   @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										   *//*@RequestBody EstimateRequest estimateRequest,*//*
										   @RequestPart(value = "meta-data", required = false)  EstimateRequest estimateRequest,
										   @RequestPart(value = "attached_file", required = false) List<MultipartFile> images
	) {
		try {


			*//*if( estimateRequest == null)
				estimateRequest = meta_data;

			if(estimateRequest == null)
			{
				log.error("[ESTIMATE-PUT] 요청 파라메터 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}*//*

			*//* 파일 업로드 invalidation 확인 *//*
			if (images != null && images.size() > 0) {

				if (images.size() > acsConfig.getPropertyToInteger("LIMIT.MULTIPART.CONTENTS.CNT")) {
					log.error("[ESTIMATE-PUT] 제한 된 현장 사진 갯수 초과 [{]] 허용 갯수[{}]", images.size(), acsConfig.getProperty("LIMIT.MULTIPART.CONTENTS.CNT"));
					return makeResponseEntity(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
				}

				if(request.getContentLength() > acsConfig.getPropertyToInteger("LIMIT.MULTIPART.CONTENTS.SIZE"))
				{
					log.error("[ESTIMATE-PUT] 제한 된 현장 사진 용량 초과 [{]] 허용 용량[{}]", request.getContentLength(), acsConfig.getPropertyToInteger("LIMIT.MULTIPART.CONTENTS.SIZE"));
					return makeResponseEntity(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
				}

			}

			Member member = null;

			// 비회원 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
			}

			// 단말 유형
			AgentType agentType = zid == null ? AgentType.WEB : AgentType.APP;

			// 파라미터 검사
			String missing_parameters = null;
			if ((missing_parameters = estimateRequest.validate(agentType)) != null) {
				log.error("[ESTIMATE-PUT] 필수 항목 오류.");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, missing_parameters), HttpStatus.NOT_ACCEPTABLE);
			}

			// 견적명 자동 생성을 위해 CATEGORY_CODE2 표시명을 설정한다.
			if (estimateRequest.getCategory_code2() != null) {
				estimateRequest.setCategory_name2(commonService.selectCategoryName(estimateRequest.getCategory_code2()));
			}
			Estimate estimate = estimateRequest.buildEstimate();

			// 프로모션 코드 검사
			int promotion_check_result = 0;
			if ((promotion_check_result = checkPromotionCode(estimate.getPromotion_code(), agentType)) > 0) {
				return makeResponseEntity(request, new PromotionCode(promotion_check_result), HttpStatus.GONE);
			}

			// 앱 처리
			if (agentType == AgentType.APP) {

				if (StringUtils.equals(member.getOs_type(), OsType.ANDROID.getCode())) {
					estimate.setChannel_sub_code(ChannelSubCode.APP_ANDROID.getCode());
				} else if (StringUtils.equals(member.getOs_type(), OsType.IOS.getCode())) {
					estimate.setChannel_sub_code(ChannelSubCode.APP_IPHONE.getCode());
				}

				if (estimateRequest.getEstimate_app_type() != null && estimateRequest.getEstimate_app_type() == AppType.CACULATOR.value()) {
					estimate.setEstimate_app_type(AppType.CACULATOR.value());
				}
			}

			// 웹 처리
			else {

				// REFERER

				// 웹인 경우 평수를 평형대로 계산. 앱은 맞춰서 오고 있음.
				try {
					if (StringUtils.isNotEmpty(estimate.getSpace())) {
						SpaceCode spaceCode = SpaceCode.get(Integer.parseInt(estimate.getSpace()));
						estimate.setSpace(spaceCode.getName());
					}
				} catch (Exception e) {}
			}

			// 단말 IP
			estimate.setIp(IpUtil.getClientIP(request));

			// 접속 단말 유형
			estimate.setChannel(agentType == AgentType.APP ? Channel.APP.value() : Channel.WEB.value());

			// 견적상태. 신청접수
			estimate.setStatus(EstimateStatus.REGISTER.getCode());

			// 사용자 구분
			estimate.setUser_type(member == null ? UserType.NO_MEMBER.value() : member.getUserType(agentType).value());

			// 사용자 ID
			estimate.setUser_id(member == null ? null : member.getUserId(agentType));

			// 알림톡 전송 시 견적 확인을 위한 비밀번호 생성
			estimate.setPassword(RandomStringUtils.randomAlphanumeric(15));

			estimate.setReferer(estimateRequest.getReferer());

			try {
				ParseRereferUrl parseRereferUrl = new ParseRereferUrl();
				parseRereferUrl.parseUrl(estimate);
				log.info("estimate referer[{}] referer_code[{}]", estimate.getReferer(), estimate.getReferer_code());
			}
			catch (Exception e)
			{
				log.error("referer_code 매핑 오류 [{}]", e);
			}

			// DB 등록
			if (estimateService.createEstimate(estimate) == 0) {
				log.error("[ESTIMATE-PUT] ["+estimate.getPhone_no()+"]. 견적 등록 실패.");
				return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
			}

			log.debug("[ESTIMATE-PUT] ["+estimate.getPhone_no()+"] 견적 등록 완료. ESTIMATE-NO="+estimate.getEstimate_no());

			try {

				if (images != null) {
					asyncService.EstimateUploadFile(images, estimate);
				}

				if (MsgEnableType.find(acsConfig.getProperty("EST.MATCH.ESTIMATE.ENABLE")) == MsgEnableType.OK)
					if (PhoneNo.isCellphone(estimate.getPhone_no()))
						asyncService.EstimateCustmerSendMsg(estimate);
			}
		 	catch (Exception e) {
				log.error("[ESTIMATE-IMAGE-PUT] 견적 현장 사진 등록 및 LMS 발송 오류. [현장 사진 등록 및 LMS 발송 실패] 오류 [{}]", e);
			}
		*//*	try {

				if (images != null && images.size() > 0) {
					for (int nIdx = 0; nIdx < images.size(); nIdx++) {
						String relativePath = Constants.RELATIVE_ESTIMATE_PATH + estimate.getEstimate_no() + "/" + nIdx + "/";

						FileInfo fileInfo = FileUploadUtil.uploadFile(images.get(nIdx), relativePath);

						log.debug("[ESTIMATE-AWS-S3-IMAGE-PUT] [" + estimate.getPhone_no() + "] 이미지 등록 완료.[{}] ESTIMATE-NO=" + estimate.getEstimate_no(),
								(acsConfig.getProperty("STATIC_URL_TYPE").equals("0")) ? acsConfig.getProperty("STATIC_URL_C") + relativePath + fileInfo.getFile().getName() : acsConfig.getProperty("STATIC_URL_Q") + relativePath + fileInfo.getFile().getName());

						estimate.setEstimateImgFiles(new EstimateImgFile(estimate.getEstimate_no(), nIdx, null, "00",
								(acsConfig.getProperty("STATIC_URL_TYPE").equals("0")) ? acsConfig.getProperty("STATIC_URL_C") + relativePath + fileInfo.getFile().getName() : acsConfig.getProperty("STATIC_URL_Q") + relativePath + fileInfo.getFile().getName(), fileInfo.getOriginFilename()));
					}
				}

			}
			catch (Exception e)
			{
				log.error("[ESTIMATE-PUT] 견적 등록 오류. [이미지 등록 실패]");
			}*//*



			if (agentType == AgentType.WEB) {

				Estimate result = new Estimate();

				result.setEstimate_no(estimate.getEstimate_no());
				result.setWriter(estimate.getWriter());
				result.setPhone_no(estimate.getPhone_no());
				result.setAddress(estimate.getAddress());
				result.setRegion1(estimate.getRegion1());
				result.setRegion2(estimate.getRegion2());
				result.setComment(estimate.getComment());
				result.setCategory_code1(estimate.getCategory_code1());
				result.setCategory_name1(commonService.selectCategoryName(estimate.getCategory_code1()));

				result.setCategory_code2(estimate.getCategory_code2());
				result.setCategory_name2(commonService.selectCategoryName(estimate.getCategory_code2()));

				result.setConstruction_str(estimate.getConstruction_str());
				result.setBath_cnt(estimate.getBath_cnt());
				result.setStruct_room_cnt(estimate.getStruct_room_cnt());
				if(estimate.getResident() != null && !estimate.getResident().equals(""))
				{
					if(ResidentType.get(estimate.getResident()) != null) {
						result.setResident(estimate.getResident());
						result.setResident_name(ResidentType.get(estimate.getResident()));
					}
				}

				if (estimate.getEstimate_type() == EstimateType.DETAIL_TYPE.getCode()) {
					result.setAddress(estimate.getAddress());
					result.setSpace(estimate.getSpace());
					result.setLiving_type(estimate.getLiving_type());
				}

				if(estimate.getEstimate_img_info() != null)
					result.setEstimate_img_info(estimate.getEstimate_img_info());

				return makeResponseEntity(request, result, HttpStatus.OK);
			}


			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[ESTIMATE-PUT] 견적 등록 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[ESTIMATE-PUT] 견적 등록 오류.", e);
			return makeResponseEntity(request, "WEB DB - failed to create", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/estimate/promotion", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> estimate_promotion_code(HttpServletRequest request,
										   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										   @RequestHeader(value = "ZID", required = false) Long zid,
										   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										   @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										   @RequestBody EstimateRequest estimateRequest) {
		try {

			Member member = null;

			// 비회원 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
			}

			// 단말 유형
			AgentType agentType = zid == null ? AgentType.WEB : AgentType.APP;

			// 파라미터 검사
			if (StringUtils.isEmpty(estimateRequest.getPromotion_code())) {
				log.error("[ESTIMATE-CHECK-PROMOTION] 필수 항목 오류.");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "No promotion-code"), HttpStatus.NOT_ACCEPTABLE);
			}

			// 프로모션 코드 검사
			int promotion_check_result = 0;
			if ((promotion_check_result = checkPromotionCode(estimateRequest.getPromotion_code(), agentType)) > 0) {
				return makeResponseEntity(request, new PromotionCode(promotion_check_result), HttpStatus.GONE);
			}

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[ESTIMATE-CHECK-PROMOTION] 프로모션 코드 검사 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[ESTIMATE-CHECK-PROMOTION] 프로모션 코드 검사 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/estimate/concept", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> estimate_concept_thumbnails(HttpServletRequest request,
														  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
														  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
														  @RequestHeader(value = "ZID", required = false) Long zid,
														  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
														  @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
														  @RequestBody EstimateRequest estimateRequest) {
		try {

			Member member = null;

			// 비회원 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
			}

			// 단말 유형
			AgentType agentType = zid == null ? AgentType.WEB : AgentType.APP;

			// 파라미터 검사
			if (estimateRequest.getConcept_pid() == null) {
				log.error("[ESTIMATE-CONCEPT-THUMBNAILS] 필수 항목 오류.");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "No concept-pid"), HttpStatus.NOT_ACCEPTABLE);
			}

			Product concept = productService.selectConcept(estimateRequest.getConcept_pid());
			if (concept == null) {
				log.error("[ESTIMATE-CONCEPT-THUMBNAILS] PID[" + estimateRequest.getConcept_pid() + "]로 등록된 시공사례가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			concept.buildPictureUrl(PictureType.XXHDPI, PictureType.MDPI);
			*//*concept.setPid(null);
			concept.setSubject(null);
			concept.setImage_url(null);*//*

			return makeResponseEntity(request, concept, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[ESTIMATE-CONCEPT-THUMBNAILS] 시공사례 썸네일 조회 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[ESTIMATE-CONCEPT-THUMBNAILS] 시공사례 썸네일 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}*/

	@ResponseBody
	@RequestMapping(value = "/estimate/my")
	public ResponseEntity<String> myMoveList(HttpServletRequest request,
												@RequestHeader(value = "APP-VERSION", required = false) String app_version,
												@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												@RequestHeader(value = "ZID", required = false) Long zid,
												@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												@RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
												@RequestBody(required = false) EstimateListRequest searchCondition) {
		try {

			MyInteriorListResponse result = null;

			if (searchCondition != null) {
				if (StringUtils.isEmpty(searchCondition.getWriter()) || StringUtils.isEmpty(searchCondition.getPhone_no())) {
					log.error("[MY-MOVE-LIST] 내 견적 확인을 위한 필수 정보가 없습니다. 이름:" + searchCondition.getWriter() + " 전화번호:" + searchCondition.getPhone_no());
					return makeResponseEntity(HttpStatus.BAD_REQUEST);
				}
			}

			result = estimateService.selectMyMoveEstimateList( searchCondition );

			// 조회된 결과가 없을 경우
			if (result == null || result.getList().size() == 0) {
				log.debug("[MY-MOVE-LIST] 진행중인 견적정보가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}
			log.debug("[MY-MOVE-LIST] 조회 견적수="+result.getTotal_count());

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error("[MY-MOVE-LIST] 나의 견적 조회 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[MY-MOVE-LIST] 나의 견적 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*@ResponseBody
	@RequestMapping(value = {"/estimates/{page}/{limit}", "/estimate/list/{page}/{limit}"})
	public ResponseEntity<String> list(HttpServletRequest request,
									   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
									   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									   @RequestHeader(value = "ZID", required = false) Long zid,
									   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
									   @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
									   @PathVariable("page") int page,
									   @PathVariable("limit") int limit,
									   @RequestBody(required = false) EstimateListRequest searchCondition) {
		try {

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
			}
			AgentType agent = zid != null ? AgentType.APP : AgentType.WEB;

			if (searchCondition == null) {
				searchCondition = new EstimateListRequest();
			}
			searchCondition.setPageLimit(page, limit);
			searchCondition.setAgent(agent.getCode());

			int total_count = estimateService.selectListTotalCount(searchCondition);

			// 조회된 결과가 없을 경우
			if (total_count == 0) {
				log.debug("[ESTIMATE-LIST] "+page+"page 에는 견적 목록이 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			List<Estimate> list = estimateService.selectEstimateList(searchCondition);

			// 이름의 첫글자만 남기고 O로 치환한다.
			if (list != null) {
				for (Estimate estimate : list) {
					if (StringUtils.isNotEmpty(estimate.getWriter())) {
						estimate.setWriter(ACSUtils.maskingName(estimate.getWriter(), "O"));
					}

					estimate.setEstimate_img_info(estimateImgFileDao.selectEstimateImageFile(estimate.getEstimate_no()));
				}
			}

			EstimateList result = new EstimateList(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error("[ESTIMATE-LIST] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[ESTIMATE-LIST] 견적 신청 현황 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/estimate/myinterior/{estimate_no}")
	public ResponseEntity<String> myinteriorDetail(HttpServletRequest request,
													@RequestHeader(value = "APP-VERSION", required = false) String app_version,
													@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
													@RequestHeader(value = "ZID", required = false) Long zid,
													@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
													@RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
													@PathVariable("estimate_no") long estimate_no,
													@RequestBody(required = false) EstimateListRequest searchCondition) {
		try {

			MyEstimate estimate = null;

			if (searchCondition != null) {
				if (StringUtils.isEmpty(searchCondition.getUsername()) || StringUtils.isEmpty(searchCondition.getPhone_no())) {
					log.error("[MY-INTERIOR-DETAIL] 견적 상세 확인을 위한 필수 정보가 없습니다. 이름:" + searchCondition.getUsername() + " 전화번호:" + searchCondition.getPhone_no());
					return makeResponseEntity(HttpStatus.BAD_REQUEST);
				}
			}

			// 비회원 견적 확인인 경우
			if (zid == null && StringUtils.isEmpty(web_token)) {

				if (searchCondition == null) {
					log.error("[MY-INTERIOR-DETAIL] 견적 상세 확인을 위한 JSON 데이터가 없습니다.");
					return makeResponseEntity(HttpStatus.BAD_REQUEST);
				}

				// 계약정보 조회
				estimate = estimateService.selectMyInterior(searchCondition, estimate_no);
			}

			// 앱/웹 회원인 경우
			else {

				// 요청 단말 구분
				AgentType agent = StringUtils.isNotEmpty(web_token) ? AgentType.WEB : AgentType.APP;

				// 가입자 조회
				Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

				// 계약정보 조회
				estimate = estimateService.selectMyInterior(member.getMember_no(), estimate_no, agent);
			}

			return estimate != null ? makeResponseEntity(request, estimate, HttpStatus.OK) : makeResponseEntity(HttpStatus.NO_CONTENT);

		} catch (ServiceException e) {
			log.error("[MY-INTERIOR-DETAIL] 진행정보 조회 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[MY-INTERIOR-DETAIL] 진행정보 조회 오류", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private int checkPromotionCode(String promotion_code, AgentType agentType) {

		// 프로모션 코드 검사
		if (StringUtils.isNotEmpty(promotion_code)) {

			// 프로모션 코드 정보 조회
			PromotionCode promotionCode = estimateService.selectPromotionCode(promotion_code);

			// 오류코드(1) - 미등록코드
			if (promotionCode == null) {
				log.error("[CHECK-PROMOTION] 프로모션코드["+promotion_code+"]가 없습니다.");
				return 1;
			}

			// 오류코드(3) - 웹/앱 서비스 대상이 잘못된 경우
			if ((agentType == AgentType.APP && promotionCode.getService_target() == 2) ||
					(agentType == AgentType.WEB && promotionCode.getService_target() == 1)) {
				log.error("[CHECK-PROMOTION] 프로모션코드["+promotion_code+"]가 서비스 대상 오류입니다.");
				return 3;
			}

			Date now = new Date();

			// 오류코드(2) - 사용기간 오류
			if (promotionCode.getStatus() == 1 &&
					(promotionCode.getStart_date() == null ||
							now.compareTo(promotionCode.getStart_date()) < 0 ||
							promotionCode.getEnd_date() == null ||
							now.compareTo(promotionCode.getEnd_date()) > 0)) {


				log.error("[CHECK-PROMOTION] 프로모션코드["+promotion_code+"] 사용기간 오류. ("+
						DateUtil.toFormatString(promotionCode.getStart_date(), DateUtil.DATE_FORMAT_FOR_SEACH)+" ~ "+
						DateUtil.toFormatString(promotionCode.getEnd_date(), DateUtil.DATE_FORMAT_FOR_SEACH)+")");
				return 2;
			}
		}
		return 0;
	}*/
}

package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.pns.MessageSender;
import com.zipdoc.acs.domain.pns.MsgType;
import com.zipdoc.acs.domain.pns.SendMessage;
import com.zipdoc.acs.domain.service.*;
import com.zipdoc.acs.model.*;
import com.zipdoc.acs.utils.FileInfo;
import com.zipdoc.acs.utils.FileUploadUtil;
import com.zipdoc.acs.utils.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 협의확인서 조회 및 등록 기능
 * @author 이동규
 *
 */
@Controller
public class ContractHistoryController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(ContractHistoryController.class);

	@Value("${UPLOAD_FILE.STATIC_PATH}")
	private String static_path;
	
	@Value("${STATIC_URL}")
	private String static_download_url;
	
	@Autowired
	private MemberService memberService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private MessageSender messageSender;

	@Autowired
	private SendMessageService sendMessageService;

	@ResponseBody
	@RequestMapping(value = "/partner/contract/history/{contract_no}", method = RequestMethod.GET)
	public ResponseEntity<String> partnerContractHistoryShow(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("contract_no") long contract_no) {
		
		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());
			
			// 계약 조회
			PartnerContract contract = partnerService.selectContract(contract_no);
			if (contract == null) {
				log.error("[CONTRACT-HISTORY-DOWNLOAD] ["+member.getMember_id()+"]. 요청한 계약["+contract_no+"]은 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			PartnerHistoryResponse result = buildPartnerHistoryResponse(contract);

			return makeResponseEntity(request, result, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[CONTRACT-HISTORY-DOWNLOAD] 협의확인서 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@ResponseBody
	@RequestMapping(value = "/partner/contract/history/{contract_no}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<String> partnerContractHistoryCreate(HttpServletRequest request,
															   @RequestHeader(value = "APP-VERSION", required = true) String app_version,
															   @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
															   @RequestHeader(value = "ZID", required = true) Long zid,
															   @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
															   @PathVariable("contract_no") long contract_no,
															   @RequestParam("meta-data") String metaData) {
		return treatPartnerContractHistoryCreate(request, app_version, member_no, zid, zid_key, contract_no, metaData, null, null);
	}

	@ResponseBody
	@RequestMapping(value = "/partner/contract/history/{contract_no}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> partnerContractHistoryCreateWithPicture(HttpServletRequest request,
															   @RequestHeader(value = "APP-VERSION", required = true) String app_version,
															   @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
															   @RequestHeader(value = "ZID", required = true) Long zid,
															   @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
															   @PathVariable("contract_no") long contract_no,
															   @RequestParam("meta-data") String metaData,
															   @RequestPart(value = "image", required = false) List<MultipartFile> images,
															   @RequestPart(value = "thumbnail", required = false) List<MultipartFile> thumbnails) {
		return treatPartnerContractHistoryCreate(request, app_version, member_no, zid, zid_key, contract_no, metaData, images, thumbnails);
	}

	public ResponseEntity<String> treatPartnerContractHistoryCreate(HttpServletRequest request,
			String app_version,
			Long member_no,
			Long zid,
			String zid_key,
			long contract_no,
			String metaData,
			List<MultipartFile> images,
			List<MultipartFile> thumbnails) {
		
		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());
			
			// 업로드 파일 개수 체크. 이미지와 썸네일의 갯수가 일치하여야 한다.
			if ((images == null && thumbnails != null) ||
				(images != null && thumbnails == null) ||
				(images != null && images.size() != thumbnails.size())) {
				log.error("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"]이 업로드한 협의확인서 원본사진과 썸네일의 사진 수가 일치하지 않습니다.");
				return makeResponseEntity(request, "The number of images and thumbnails is different.", HttpStatus.BAD_REQUEST);
			}

			if (StringUtils.isEmpty(metaData)) {
				log.error("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"] 계약번호["+contract_no+"]. 등록할 협의확인서 내용이 없습니다."); 
				return makeResponseEntity(request, "Invalid meta-data.", HttpStatus.BAD_REQUEST);
			}
			
			// 계약 조회
			PartnerContract contract = partnerService.selectContract(contract_no);
			if (contract == null) {
				log.error("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"]. 요청한 계약["+contract_no+"]은 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}
			
			// 상태 오류
			if (contract.getCompleted_code() != null && contract.getCompleted_code() != 0) {
				//|| contract.getAgreement_status() == 1 || contract.getAgreement_status() == 3) { // TODO: 협의 상태 확인은 추후 협의
				log.error("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"]. 요청한 계약["+contract_no+"]은 협의확인서를 등록 할 수 없는 상태입니다. [COMPLETED="+
						contract.getCompleted_code()+",AGREEMENT="+contract.getAgreement_status()+"]");
				return makeResponseEntity(HttpStatus.GONE);
			}
			
			// 협의확인서 생성
			PartnerContractHistory history = new PartnerContractHistory();
			
			history.setContents(metaData);
			history.setType(1);
			history.setMember_no(member_no);
			history.setContract_no(contract_no);
			
			// 협의 확인서 등록
			partnerService.createContractHistory(history);
			
			// 계약 상태 변경
			contract.setAgreement_status(1);
			partnerService.updateContract_agreementStatus(contract);
			
			log.debug("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"]. 요청한 계약["+contract_no+"]의 협의확인서 등록. [내용:"+metaData+"] 사진개수="+(images == null ? 0 : images.size())+" 썸네일="+(thumbnails == null ? 0 : thumbnails.size())+"]");

			treatContractHistoryFile(member, history, images, thumbnails);

			//고객에게 알림메시지 전송
			List<MemberDevice> targetList = sendMessageService.findListByMobileNo(contract.getPhone_no());
			if(targetList.size()>0) {
				SendMessage sendMsg = new SendMessage();
				sendMsg.setBody(contract.getContract_name());
				sendMsg.setEpid(contract.getEstimate_no());
				sendMsg.setTargetPartner(false);
				sendMsg.setTitle(contract.getPartner_name() + "에서 고객님께 협의확인서에 대해 승인 요청하였습니다.");
				sendMsg.setMsg_type(MsgType.E210.getCode());

				sendMsg.setTargetList(targetList);
				messageSender.sendPush(sendMsg);
			}

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[CONTRACT-HISTORY-CREATE] 협의확인서 등록 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}




	@ResponseBody
	@RequestMapping(value = "/customer/contract/history/{contract_no}", method = RequestMethod.GET)
	public ResponseEntity<String> customerContractHistoryShow(HttpServletRequest request,
														@RequestHeader(value = "APP-VERSION", required = true) String app_version,
														@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
														@RequestHeader(value = "ZID", required = true) Long zid,
														@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
														@PathVariable("contract_no") long contract_no) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			// 계약 조회
			PartnerContract contract = partnerService.selectContract(contract_no);
			if (contract == null) {
				log.error("[CONTRACT-HISTORY-DOWNLOAD] ["+member.getMember_id()+"]. 요청한 계약["+contract_no+"]은 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			PartnerHistoryResponse result = buildPartnerHistoryResponse(contract);

			return makeResponseEntity(request, result, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[CONTRACT-HISTORY-DOWNLOAD] 협의확인서 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/customer/contract/history/{contract_no}/{agreement_type}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<String> customerContractHistoryCreate(HttpServletRequest request,
																@RequestHeader(value = "APP-VERSION", required = true) String app_version,
																@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
																@RequestHeader(value = "ZID", required = true) Long zid,
																@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
																@PathVariable("contract_no") long contract_no,
																@PathVariable("agreement_type") int agreement_type,
																@RequestParam(value = "meta-data", required = false) String metaData) {
		return treatCustomerContractHistoryCreate(request, app_version, member_no, zid, zid_key, contract_no, agreement_type, metaData, null, null);
	}

	@ResponseBody
	@RequestMapping(value = "/customer/contract/history/{contract_no}/{agreement_type}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> customerContractHistoryCreate(HttpServletRequest request,
																@RequestHeader(value = "APP-VERSION", required = true) String app_version,
																@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
																@RequestHeader(value = "ZID", required = true) Long zid,
																@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
																@PathVariable("contract_no") long contract_no,
																@PathVariable("agreement_type") int agreement_type,
																@RequestParam(value = "meta-data", required = false) String metaData,
																@RequestPart(value = "image", required = false) List<MultipartFile> images,
																@RequestPart(value = "thumbnail", required = false) List<MultipartFile> thumbnails) {
		return treatCustomerContractHistoryCreate(request, app_version, member_no, zid, zid_key, contract_no, agreement_type, metaData, images, thumbnails);
	}

	private ResponseEntity<String> treatCustomerContractHistoryCreate(HttpServletRequest request,
														  String app_version,
														  Long member_no,
														  Long zid,
														  String zid_key,
														  long contract_no,
														  int agreement_type,
														  String metaData,
														  List<MultipartFile> images,
														  List<MultipartFile> thumbnails) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			// 업로드 파일 개수 체크. 이미지와 썸네일의 갯수가 일치하여야 한다.
			if ((images == null && thumbnails != null) ||
					(images != null && thumbnails == null) ||
					(images != null && images.size() != thumbnails.size())) {
				log.error("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"]이 업로드한 협의확인서 원본사진과 썸네일의 사진 수가 일치하지 않습니다.");
				return makeResponseEntity(request, "The number of images and thumbnails is different.", HttpStatus.BAD_REQUEST);
			}

			AgreementStatus agreementStatus = AgreementStatus.get(agreement_type);
			if(agreementStatus!=AgreementStatus.REQ_ADJUST && agreementStatus!=AgreementStatus.COMPLETED){
				log.error("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"] 계약번호["+contract_no+"]. 협의 상태값이 유효하지 없습니다.");
				return makeResponseEntity(request, "Invalid agreement_type.", HttpStatus.BAD_REQUEST);
			}

			//동의완료 시 메시지가 없으면 동의함으로 처리
			if(agreementStatus==AgreementStatus.COMPLETED && StringUtils.isEmpty(metaData)){
				metaData = "동의함";
			}

			if (agreementStatus==AgreementStatus.REQ_ADJUST && StringUtils.isEmpty(metaData)) {
				log.error("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"] 계약번호["+contract_no+"]. 등록할 협의확인서 내용이 없습니다.");
				return makeResponseEntity(request, "Invalid meta-data.", HttpStatus.BAD_REQUEST);
			}

			// 계약 조회
			PartnerContract contract = partnerService.selectContract(contract_no);
			if (contract == null) {
				log.error("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"]. 요청한 계약["+contract_no+"]은 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// 상태 오류
			if (contract.getCompleted_code() != null && contract.getCompleted_code() != 0) {
				//|| contract.getAgreement_status() == 1 || contract.getAgreement_status() == 3) { // TODO: 협의 상태 확인은 추후 협의
				log.error("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"]. 요청한 계약["+contract_no+"]은 협의확인서를 등록 할 수 없는 상태입니다. [COMPLETED="+
						contract.getCompleted_code()+",AGREEMENT="+contract.getAgreement_status()+"]");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// 협의확인서 생성
			PartnerContractHistory history = new PartnerContractHistory();

			history.setContents(metaData);
			history.setType(agreement_type);
			history.setMember_no(member_no==null?-1:member_no); //자동가입자가 등록하는 경우 -1을 등록한다.
			history.setContract_no(contract_no);

			// 협의 확인서 등록
			partnerService.createContractHistory(history);

			// 계약 상태 변경
			contract.setAgreement_status(agreement_type);
			partnerService.updateContract_agreementStatus(contract);

			log.debug("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"]. 요청한 계약["+contract_no+"]의 협의확인서 등록. [내용:"+metaData+"] 사진개수="+(images == null ? 0 : images.size())+" 썸네일="+(thumbnails == null ? 0 : thumbnails.size())+"]");

			treatContractHistoryFile(member, history, images, thumbnails);


			//파트너사에 알림메시지 전송
			SendMessage sendMsg = new SendMessage();
			sendMsg.setBody(contract.getContract_name());
			sendMsg.setEpid(contract.getEstimate_no());
			sendMsg.setTargetPartner(true);

			if(agreementStatus==AgreementStatus.REQ_ADJUST) {
				sendMsg.setTitle(contract.getContracter() + " 고객에게 요청한 협의확인서에 대해 고객이 정정을 요청하였습니다.");
				sendMsg.setMsg_type(MsgType.E211.getCode());
			} else if(agreementStatus==AgreementStatus.COMPLETED){
				sendMsg.setTitle(contract.getContracter() + " 고객에게 요청한 협의확인서가 승인되었습니다.");
				sendMsg.setMsg_type(MsgType.E212.getCode());
			}

			List<MemberDevice> targetList = sendMessageService.findListByPartnerId(contract.getPartner_id());
			sendMsg.setTargetList(targetList);
			messageSender.sendPush(sendMsg);

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[CONTRACT-HISTORY-CREATE] 협의확인서 등록 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	private PartnerHistoryResponse buildPartnerHistoryResponse(PartnerContract contract){

		PartnerHistoryResponse result = new PartnerHistoryResponse();

		result.setContract_no(contract.getContract_no());
		result.setPartner_name(contract.getPartner_name());
		result.setCustomer(contract.getContracter());
		result.setCategory_code1(contract.getCategory_code1());
		result.setCategory_code2(contract.getCategory_code2());
		result.setAgreement_status(contract.getAgreement_status());

		// 협의내용 조회
		List<PartnerContractHistory> list = partnerService.selectContractHistoryByContractNo(contract.getContract_no());

		// 다운로드 URL 확인
		String upload_path = static_path;
		if (!upload_path.endsWith("/")) { upload_path += "/"; }

		String download_url = static_download_url;
		if (!download_url.endsWith("/")) { download_url += "/"; }

		// 히스토리 설정 확인
		for(PartnerContractHistory history : list){
			if (history.getType() == 1) { history.setWriter(result.getPartner_name()); }
			else { history.setWriter(result.getCustomer()); }

			// 히스토리 파일 조회
			List<PartnerContractHistoryFile> images = partnerService.selectContractHistoryFile(history.getHistory_no());

			for (int j = 0; images != null && j < images.size(); j++) {
				PartnerContractHistoryFile image = images.get(j);
				image.setImage(static_download_url+image.getFile_path()+image.getFile_name());
				if (image.getThumb_name() != null) { image.setThumbnail(static_download_url+image.getFile_path()+image.getThumb_name()); }
			}

			if (images != null && images.size() > 0) { history.setImages(images); }
		}

		result.setHistory(list);
		return result;
	}

	private void treatContractHistoryFile(Member member, PartnerContractHistory history, List<MultipartFile> images, List<MultipartFile> thumbnails){

		String upload_path = static_path;
		if (!upload_path.endsWith("/")) { upload_path += "/"; }

		final String relativeDirPath = "contract/" + history.getContract_no() + "/image_";
		final String relativeDirThumPath = "contract/" + history.getContract_no() + "/thumb_";

		// 파일 업로드 처리
		for (int i = 0; images != null && i < images.size(); i++) {

			MultipartFile image = images.get(i);
			MultipartFile thumb = thumbnails.get(i);

			try {
				final String relativeFilePath = relativeDirPath + history.getHistory_no()+"_"+i;
				FileInfo imageFile = new FileInfo(image, upload_path + relativeFilePath);

				final String relativeThumFilePath = relativeDirThumPath + history.getHistory_no()+"_"+i;
				FileInfo thumbFile = new FileInfo(thumb, upload_path + relativeThumFilePath);

				// 파일 저장
				// 폴더 생성
				imageFile.getFile().getParentFile().mkdirs();

				// 파일 저장
				image.transferTo(imageFile.getFile());
				thumb.transferTo(thumbFile.getFile());
				FileUploadUtil.getInstance().uploadToS3(imageFile.getFile(), relativeFilePath + "." +imageFile.getExtension());
				FileUploadUtil.getInstance().uploadToS3(thumbFile.getFile(), relativeThumFilePath+ "." + imageFile.getExtension());

				// DB 저장
				partnerService.createContractHistoryFile(
						new PartnerContractHistoryFile(
								history.getHistory_no(),
								imageFile.getOriginFilename(),
								imageFile.getFile().getName(),
								thumbFile.getFile().getName(),
								"contract/" + history.getContract_no() + "/"));

				FileUploadUtil.getInstance().deleteFile(imageFile.getFile());
				FileUploadUtil.getInstance().deleteFile(thumbFile.getFile());

			} catch (Exception e) {
				log.error("[CONTRACT-HISTORY-CREATE] ["+member.getMember_id()+"]. 업로드 파일 ["+image.getOriginalFilename()+","+thumb.getOriginalFilename()+"] 오류. ("+e.getMessage()+")");
			}
		}

	}
}

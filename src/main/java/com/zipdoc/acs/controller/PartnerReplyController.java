package com.zipdoc.acs.controller;

import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.PartnerReply;
import com.zipdoc.acs.domain.entity.Product;
import com.zipdoc.acs.domain.entity.Zzim;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.PartnerReplyService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ZzimService;
import com.zipdoc.acs.model.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * 찜하기
 * @author 이동규
 *
 */
@Controller
public class PartnerReplyController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(PartnerReplyController.class);

	@Autowired
	private MemberService memberService;
	
	@Autowired
	private PartnerReplyService partnerReplyService;

	@ResponseBody
	@RequestMapping(value = "/partner/reply/list/{partner_id}/{page}/{limit}")
	public ResponseEntity<String> list(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
		    @PathVariable("partner_id") Integer partner_id,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit) {
		try {
			// 가입자 조회
			Member member = memberService.selectMember(member_no, zid);

			// 가입자 정보가 없을 경우
			if (member == null) {
				log.error("[PARTNER-REPLY-LIST] MEMBER_NO["+member_no + "] ZID["+zid+"] 가입자만 이용 가능합니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[PARTNER-REPLY-LIST] MEMBER_NO["+member_no + "] ZID["+zid+"] 가입자의 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			if(partner_id == null || partner_id <= 0){
				log.error("[PARTNER-REPLY-LIST] MEMBER_NO["+member_no + "] ZID["+zid+"] PARTNER_ID["+partner_id+"] 필수 항목(partner_id)이 누락되었습니다.");
				return makeResponseEntity(request, "필수 항목(partner_id)이 누락되었습니다." , HttpStatus.BAD_REQUEST);
			}

			PartnerReplyListRequest listRequest = new PartnerReplyListRequest(page, limit);
			listRequest.setPartner_id(partner_id);
			int total_count = partnerReplyService.selectPartnerReplyListTotalCount(listRequest);
			// 조회된 결과가 없을 경우
			if(total_count==0) {
				log.debug("[ZZIM-LIST] "+page+"page 에는 업체 후기 정보가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			List<PartnerReply> list = partnerReplyService.selectPartnerReplyList(listRequest);
			PartnerReplyListResponse result = new PartnerReplyListResponse(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("[PARTNER-REPLY-LIST] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/reply/my-list/{page}/{limit}")
	public ResponseEntity<String> myList(HttpServletRequest request,
									   @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									   @RequestHeader(value = "ZID", required = true) Long zid,
									   @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									   @PathVariable("page") int page,
									   @PathVariable("limit") int limit) {
		try {
			// 가입자 조회
			Member member = memberService.selectMember(member_no, zid);

			// 가입자 정보가 없을 경우
			if (member == null || member.getMember_no() <= 0) {
				log.error("[PARTNER-REPLY-MY-LIST] MEMBER_NO["+member_no + "] ZID["+zid+"] 가입자만 이용 가능합니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[PARTNER-REPLY-MY-LIST] MEMBER_NO["+member_no + "] ZID["+zid+"] 가입자의 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			PartnerReplyListRequest listRequest = new PartnerReplyListRequest(page, limit);
			listRequest.setMember_no(member_no);
			int total_count = partnerReplyService.selectPartnerReplyListTotalCount(listRequest);
			// 조회된 결과가 없을 경우
			if(total_count==0) {
				log.debug("[PARTNER-REPLY-MY-LIST] "+page+"page 에는 업체 후기 정보가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			List<PartnerReply> list = partnerReplyService.selectPartnerReplyList(listRequest);
			PartnerReplyListResponse result = new PartnerReplyListResponse(total_count, list);
			result.buildPictureUrl(); //업체 로고 이미지 URL 생성

			return makeResponseEntity(request, result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("[PARTNER-REPLY-MY-LIST] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@ResponseBody
	@RequestMapping(value = "/partner/reply/add", method = RequestMethod.POST)
	public ResponseEntity<String> add(HttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									  @RequestBody PartnerReply partnerReply) {
		try {
			// 가입자 조회
			Member member = memberService.selectMember(member_no, zid);

			// 가입자 정보가 없을 경우
			if (member == null || member.getMember_no() <= 0) {
				log.error("[PARTNER-REPLY-ADD] MEMBER_NO["+member_no + "] ZID["+zid+"] 가입자만 이용 가능합니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[PARTNER-REPLY-ADD] MEMBER_NO["+member_no + "] ZID["+zid+"] 가입자의 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			if(partnerReply == null){
				log.error("[PARTNER-REPLY-ADD] 전문 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			if (StringUtils.isEmpty(partnerReply.getSubject()) || StringUtils.length(partnerReply.getSubject()) >= 100
					|| partnerReply.getPartner_id() == null || partnerReply.getPartner_id() <= 0
					|| StringUtils.length(partnerReply.getDescription()) >= 1500
			) {
				log.error("[PARTNER-REPLY-ADD] 입력값 오류" + partnerReply.toString());
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			partnerReply.setMember_no(member_no);
			partnerReplyService.createPartnerReply(partnerReply);

			return makeResponseEntity(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("[PARTNER-REPLY-ADD] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/reply/modify", method = RequestMethod.POST)
	public ResponseEntity<String> modify(HttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									  @RequestBody PartnerReply partnerReply) {
		try {
			// 가입자 조회
			Member member = memberService.selectMember(member_no, zid);

			// 가입자 정보가 없을 경우
			if (member == null || member.getMember_no() <= 0) {
				log.error("[PARTNER-REPLY-MODIFY] MEMBER_NO["+member_no + "] ZID["+zid+"] 가입자만 이용 가능합니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[PARTNER-REPLY-MODIFY] MEMBER_NO["+member_no + "] ZID["+zid+"] 가입자의 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			log.debug(partnerReply.toString());

			if(partnerReply == null){
				log.error("[PARTNER-REPLY-ADD] 전문 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}


			if (StringUtils.isEmpty(partnerReply.getSubject()) || StringUtils.length(partnerReply.getSubject()) >= 100
					|| partnerReply.getPartner_id() == null || partnerReply.getPartner_id() <= 0
					|| StringUtils.length(partnerReply.getDescription()) >= 1500
					) {
				log.error("[PARTNER-REPLY-MODIFY] 입력값 오류" + partnerReply.toString());
				return makeResponseEntity(request, "입력값이 유효하지 않습니다", HttpStatus.BAD_REQUEST);
			}

			partnerReply.setMember_no(member_no);
			partnerReplyService.updatePartnerReply(partnerReply);

			return makeResponseEntity(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("[PARTNER-REPLY-MODIFY] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/reply/delete/{reply_no}", method = RequestMethod.POST)
	public ResponseEntity<String> zzimDelete(HttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									  @PathVariable("reply_no") Long reply_no) {
		try {
			// 가입자 조회
			Member member = memberService.selectMember(member_no, zid);

			// 가입자 정보가 없을 경우
			if (member == null || member.getMember_no() <= 0) {
				log.error("[PARTNER-REPLY-DELETE] MEMBER_NO["+member_no + "] ZID["+zid+"] 가입자만 이용 가능합니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			// 인증키 오류
			if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
				log.error("[PARTNER-REPLY-DELETE] MEMBER_NO["+member_no + "] ZID["+zid+"] 가입자의 인증키가 일치하지 않습니다.");
				return makeResponseEntity(HttpStatus.FORBIDDEN);
			}

			if(reply_no == null || reply_no <= 0) {
				log.error("[PARTNER-REPLY-DELETE] REPLY_ID 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			PartnerReply partnerReply = new PartnerReply();
			partnerReply.setMember_no(member_no);
			partnerReply.setReply_no(reply_no);

			partnerReplyService.deletePartnerReply(partnerReply);

			return makeResponseEntity(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("[PARTNER-REPLY-DELETE] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}

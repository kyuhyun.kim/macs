package com.zipdoc.acs.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.zipdoc.acs.utils.Json;

public class ResponseEntityService {

	
    private static Logger logger = LoggerFactory.getLogger(ResponseEntityService.class);

    public ResponseEntity<String> makeResponseEntity(HttpStatus httpStatus) {
        return new ResponseEntity<>(httpStatus);
    }

    public ResponseEntity<String> makeResponseEntity(HttpServletRequest request, Object content, HttpStatus status) {
    	
        String responseBody;
        HttpHeaders headers = new HttpHeaders();
        
        if (content instanceof String) {
            responseBody = (String) content;
            headers.set("Content-Type", "text/plain;charset=utf-8");
        } else {
            try {
                responseBody = Json.toStringJson(content);
                headers.set("Content-Type", "application/json;charset=utf-8");
            } catch (Exception e) {
                logger.error("", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        request.setAttribute("content.response", responseBody);
        return new ResponseEntity<>(responseBody, headers, status);
    }

    public ResponseEntity<String> makeResponseEntity(HttpServletRequest request, HttpHeaders headers, Object content, HttpStatus status) {
    	
        String responseBody;
        
        if (content instanceof String) {
            responseBody = (String) content;
            headers.set("Content-Type", "text/plain;charset=utf-8");
        } else {
            try {
                responseBody = Json.toStringJson(content);
                headers.set("Content-Type", "application/json;charset=utf-8");
            } catch (Exception e) {
                logger.error("", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        request.setAttribute("content.response", responseBody);
        return new ResponseEntity<>(responseBody, headers, status);
    }

    public ResponseEntity<String> makeResponseEntityWithHeaders(HttpHeaders headers, HttpStatus status) {
        return new ResponseEntity<>(headers, status);
    }
}

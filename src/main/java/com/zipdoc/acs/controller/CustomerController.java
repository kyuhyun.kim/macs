package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.Cause;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.*;
import com.zipdoc.acs.model.CauseResponse;
import com.zipdoc.acs.model.ListResponse;
import com.zipdoc.acs.utils.IpUtil;
import com.zipdoc.acs.utils.ACSUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 1:1 문의
 * @author 김종부
 *
 */
@Controller
public class CustomerController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	private CustomerService customerService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@ResponseBody
	@RequestMapping(value = "/customer/list/{page}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<String> list(HttpServletRequest request,
												@RequestHeader(value = "APP-VERSION", required = false) String app_version,
												@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												@RequestHeader(value = "ZID", required = false) Long zid,
												@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												@RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
												@PathVariable("page") int page,
												@PathVariable("limit") int limit) {
		try {

			Map<String, Object> condition = new HashMap<>();
			condition.put("startRow", page*limit);
			condition.put("limit", limit);

			int total_count = customerService.findListCount(condition);

			// 조회된 결과가 없을 경우
			if (total_count == 0) {
				log.debug("[CUSTOMER-INQUIRY-LIST] "+page+"page 에는 1:1문의 내역이 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}
			List<Customer> list = customerService.findList(condition);

			// 이름의 첫글자만 남기고 O로 치환한다.
			if (list != null) {
				for (Customer customer : list) {
					if (StringUtils.isNotEmpty(customer.getUsername())) {
						customer.setUsername(ACSUtils.maskingName(customer.getUsername(), "O"));
					}
				}
			}

			ListResponse<Customer> result = new ListResponse<>(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);
		} catch (ServiceException e) {
			log.error("[CUSTOMER-INQUIRY-LIST] 1:1 문의 내역 조회 오류: "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[CUSTOMER-INQUIRY-LIST] 1:1 문의 내역 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/mypage/customer")
	public ResponseEntity<String> my_list(HttpServletRequest request,
										  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID", required = false) Long zid,
										  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										  @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token) {
		return my_list(request, app_version, member_no, zid, zid_key, web_token, null, null);
	}

	@ResponseBody
	@RequestMapping(value = "/mypage/customer/{page}/{limit}")
	public ResponseEntity<String> my_list(HttpServletRequest request,
										@RequestHeader(value = "APP-VERSION", required = false) String app_version,
										@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										@RequestHeader(value = "ZID", required = false) Long zid,
										@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										@RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										@PathVariable("page") Integer page,
										@PathVariable("limit") Integer limit) {
		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			Map<String, Object> condition = new HashMap<>();
			if (page != null && limit != null) {
				condition.put("startRow", page * limit);
				condition.put("limit", limit);
			}
			condition.put("member_no", member.getMember_no());

			int total_count = customerService.findListCount(condition);

			// 조회된 결과가 없을 경우
			if (total_count == 0) {
				log.debug("[MY-INQUIRY-LIST] 1:1문의 내역이 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}
			List<Customer> list = customerService.findList(condition);

			for (Customer customer : list) {
				List<CustomerReply> reply_list = customerService.findReplyList(customer.getCno());
				if (reply_list != null && reply_list.size() > 0) {
					customer.setReply_list(reply_list);
				}
			}

			ListResponse<Customer> result = new ListResponse<>(total_count, list);
			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[MY-INQUIRY-LIST] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[MY-INQUIRY-LIST] 문의내역 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/mypage/customer/detail/{cno}")
	public ResponseEntity<String> detail(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												 @RequestHeader(value = "ZID", required = false) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												 @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
												 @PathVariable("cno") int cno) {
		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			Map<String, Object> condition = new HashMap<>();
			condition.put("member_no", member.getMember_no());
			condition.put("cno", cno);

			// 문의내역 조회
			Customer customer = customerService.findById(condition);
			if (customer == null) {
				log.debug("[MY-INQUIRY-DETAIL] CNO["+cno+"]로 등록된 1:1문의 내역이 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			// 댓글 조회
			customer.setReply_list(customerService.findReplyList(customer.getCno()));

			return makeResponseEntity(request, customer, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[MY-INQUIRY-DETAIL] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[MY-INQUIRY-DETAIL] 문의내역 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/mypage/customer/{type}")
	public ResponseEntity<String> update_inquiry(HttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										 @RequestHeader(value = "ZID", required = false) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										 @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
                                         @PathVariable("type") String type,
										 @RequestBody Customer inquiry) {
		try {

			// URL PATH 확인
			if (!type.equalsIgnoreCase("insert") && !type.equalsIgnoreCase("update") && !type.equalsIgnoreCase("delete")) {
				log.error("[MY-INQUIRY] 문의하기 URL["+type+"] 오류. [insert | update | delete]. ");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			// 문의 하기 등록
			if (type.equalsIgnoreCase("insert")) {

				// 필수 파라미터 체크
				if (!inquiry.insertValidation()) {
					log.error("[MY-INQUIRY] 문의하기 ["+type.toUpperCase()+"] 필수 파라미터 오류. [contents/update_phone/update_email/inquire_type]");
					return makeResponseEntity(HttpStatus.BAD_REQUEST);
				}

				// 문의내용 등록
				inquiry.setMember_no(member.getMember_no());
				customerService.insert(inquiry);

				// 등록 오류
				if (inquiry.getCno() <= 0) {
					log.error("[MY-INQUIRY] 문의하기 ["+type.toUpperCase()+"] 등록 오류.");
					return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}

			// 문의하기 수정
			else if (type.equalsIgnoreCase("update")) {

				// 필수 파라미터 체크
				if (!inquiry.updateValidation()) {
					log.error("[MY-INQUIRY] 문의하기 ["+type.toUpperCase()+"] 필수 파라미터 오류. [cno/contents/update_phone/update_email/inquire_type]");
					return makeResponseEntity(HttpStatus.BAD_REQUEST);
				}

				// 문의내용 등록
				inquiry.setMember_no(member.getMember_no());
				if (customerService.update(inquiry) == 0) {
					// 업데이트 내역이 없을 경우
					log.error("[MY-INQUIRY] 문의하기 ["+type.toUpperCase()+"] CNO["+inquiry.getCno()+"]로 업데이트된 내용이 없습니다.");
					return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
				}
			}

			// 문의하기 삭제
			else {

				// 필수 파라미터 체크
				if (!inquiry.deleteValidation()) {
					log.error("[MY-INQUIRY] 문의하기 ["+type.toUpperCase()+"] 필수 파라미터 오류. [cno]");
					return makeResponseEntity(HttpStatus.BAD_REQUEST);
				}

				// 문의내용 삭제
				if (customerService.delete(inquiry.getCno()) == 0) {
					// 업데이트 내역이 없을 경우
					log.error("[MY-INQUIRY] 문의하기 ["+type.toUpperCase()+"] CNO["+inquiry.getCno()+"]로 삭제된 내용이 없습니다.");
					return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
				}
			}

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[MY-INQUIRY] 문의하기 ["+type.toUpperCase()+"] 실패"+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[MY-INQUIRY] 문의하기 ["+type.toUpperCase()+"] 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @ResponseBody
    @RequestMapping(value = "/mypage/customer/reply/{type}")
    public ResponseEntity<String> update_reply(HttpServletRequest request,
                                         @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                         @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                         @RequestHeader(value = "ZID", required = false) Long zid,
                                         @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                         @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
                                         @PathVariable("type") String type,
                                         @RequestBody CustomerReply reply) {
        try {

            // URL PATH 확인
            if (!type.equalsIgnoreCase("insert") && !type.equalsIgnoreCase("update") && !type.equalsIgnoreCase("delete")) {
                log.error("[MY-INQUIRY-REPLY] 문의하기 댓글 URL["+type+"] 오류. [insert | update | delete]. ");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            // 가입자 조회
            Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

            // 댓글 등록
            if (type.equalsIgnoreCase("insert")) {

                // 필수 파라미터 체크
                if (!reply.insertValidation()) {
                    log.error("[MY-INQUIRY-REPLY] 문의하기 댓글 ["+type.toUpperCase()+"] 필수 파라미터 오류. [cno/contents]");
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }

                // 문의내용 등록
                reply.setMember_no(member.getMember_no());
                customerService.insertReply(reply);

                // 등록 오류
                if (reply.getReply_no() == null) {
                    log.error("[MY-INQUIRY-REPLY] 문의하기 댓글 ["+type.toUpperCase()+"] 등록 오류.");
                    return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }

            // 문의하기 수정
            else if (type.equalsIgnoreCase("update")) {

                // 필수 파라미터 체크
                if (!reply.updateValidation()) {
                    log.error("[MY-INQUIRY-REPLY] 문의하기 댓글 ["+type.toUpperCase()+"] 필수 파라미터 오류. [cno/reply_no/contents]");
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }

                // 문의내용 등록
                reply.setMember_no(member.getMember_no());
                if (customerService.updateReply(reply) == 0) {
                    // 업데이트 내역이 없을 경우
                    log.error("[MY-INQUIRY-REPLY] 문의하기 댓글 ["+type.toUpperCase()+"] REPLY_NO["+reply.getReply_no()+"]로 업데이트된 내용이 없습니다.");
                    return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
                }
            }

            // 문의하기 삭제
            else {

                // 필수 파라미터 체크
                if (!reply.deleteValidation()) {
                    log.error("[MY-INQUIRY-REPLY] 문의하기 댓글 ["+type.toUpperCase()+"] 필수 파라미터 오류. [cno]");
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }

                // 문의내용 삭제
                if (customerService.deleteReply(reply.getReply_no()) == 0) {
                    // 업데이트 내역이 없을 경우
                    log.error("[MY-INQUIRY-REPLY] 문의하기 댓글 ["+type.toUpperCase()+"] REPLY_NO["+reply.getReply_no()+"]로 삭제된 내용이 없습니다.");
                    return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
                }
            }

            return makeResponseEntity(HttpStatus.OK);

        } catch (ServiceException e){
            log.error("[MY-INQUIRY-REPLY] 문의하기 댓글 ["+type.toUpperCase()+"] 실패"+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch(Exception e) {
            log.error("[MY-INQUIRY-REPLY] 문의하기 댓글 ["+type.toUpperCase()+"] 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	@ResponseBody
	@RequestMapping(value = "/customer/partners")
	public ResponseEntity<String> partners_inquiry(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												 @RequestHeader(value = "ZID", required = false) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												 @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
												 @RequestBody CustomerPartnerInquiry inquiry) {
		try {

			Member member = null;

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
			}

			// 파라미터 검사
			if (!inquiry.validation()) {
				log.error("[CUSTOMER-PARTNERS-INQUIRY] 필수 파라미터 오류.");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS), HttpStatus.NOT_ACCEPTABLE);
			}

			// 단말 IP
			inquiry.setIp(IpUtil.getClientIP(request));

			// 회원인 경우
			if (member != null) {
				inquiry.setMember_no(member.getMember_no());
			}

			// DB 등록
			customerService.insertPartnerInquiry(inquiry);

			// 등록 오류
			if (inquiry.getCno() <= 0) {
				log.error("[CUSTOMER-PARTNERS-INQUIRY] DB 등록 오류.");
				return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
			}

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[CUSTOMER-PARTNERS-INQUIRY] 파트너스 가입 문의 처리 실패. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[CUSTOMER-PARTNERS-INQUIRY] 파트너스 가입 문의 처리 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

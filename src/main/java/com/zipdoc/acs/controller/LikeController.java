package com.zipdoc.acs.controller;

import com.zipdoc.acs.domain.entity.Like;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.LikeService;
import com.zipdoc.acs.domain.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 댓글 컨트롤러
 * @author 이동규
 *
 */
@Controller
@RequestMapping(value = "/like")
public class LikeController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(LikeController.class);
	
	@Autowired
	private LikeService likeService;

	@Autowired
	private AuthenticationHelper authenticationHelper;


	@ResponseBody
	@RequestMapping(value = "/add/{category}/{pid}", method = RequestMethod.POST)
	public ResponseEntity<String> add(HttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										 @RequestHeader(value = "ZID", required = true) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
										 @PathVariable("category") int category,
										 @PathVariable("pid") long pid) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			if(pid <= 0){
				log.debug("[/like/add] 필수항목 누락");
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}

			likeService.create(new Like(category, member.getMember_no(), pid));

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/like/add] 등록 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/delete/{category}/{pid}", method = RequestMethod.POST)
	public ResponseEntity<String> delete(HttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									  @PathVariable("category") int category,
									  @PathVariable("pid") long pid) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			if(pid <= 0){
				log.debug("[/like/delete] 필수항목 누락");
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}

			likeService.delete(new Like(category, member.getMember_no(), pid));

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/like/delete] 삭제 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

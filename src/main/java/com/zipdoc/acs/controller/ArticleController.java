package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.domain.entity.Article;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.service.ArticleService;
import com.zipdoc.acs.domain.service.CommonService;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.model.ArticleListRequest;
import com.zipdoc.acs.model.ArticleListResponse;
import com.zipdoc.acs.model.ServiceInfo;
import com.zipdoc.acs.utils.Messages;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class ArticleController extends ResponseEntityService {

    private static final Logger log = LoggerFactory.getLogger(ArticleController.class);

    @Autowired
    private MemberService memberService;

    @Autowired
    private ArticleService articleService;

    private int page_item_limit = NumberUtils.toInt(Messages.getMessage("PAGE_ITEM_LIMIT", "20"));
    private String temporary_path = Messages.getMessage("UPLOAD_FILE.TEMPORARY_PATH");

    @ResponseBody
    @RequestMapping(value = "/article/list/{article_type}/{page}/{limit}")
    public ResponseEntity<String> list(HttpServletRequest request,
                                       @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                       @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                       @RequestHeader(value = "ZID", required = false) Long zid,
                                       @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                       @PathVariable("article_type") String article_type,
                                       @PathVariable("page") Integer page,
                                       @PathVariable("limit") Integer limit,
                                       @RequestBody(required = false) ArticleListRequest articleListRequest) {


        //2017.12.12 hansol
        // 건축o2o에서 써야하므로
        // zid, zid-key 없어도 사용가능하도록 수정

        // 가입자 조회
        Member member = null;
        if (member_no != null && member_no > 0 && zid != null && zid > 0) {
            member = memberService.selectMember(member_no, zid);
        }
//		// 가입자 정보가 없을 경우
//		if (member == null) {
//			log.error("[" + article_type + "] ZID["+zid+"] 가입자는 존재하지 않습니다.");
//			return makeResponseEntity(HttpStatus.FORBIDDEN);
//		}
//
//		// 인증키 오류
//		if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
//			log.error("[" + article_type + "] ["+member.getMobile_no()+"] 가입자의 인증키가 일치하지 않습니다.");
//			return makeResponseEntity(HttpStatus.FORBIDDEN);
//		}

        try {
            ArticleType articleType = ArticleType.find(article_type);

            if (articleType == ArticleType.UNDEF) {
                log.error("[" + article_type + "] [" + (member != null ? member.getMobile_no() : "멤버정보없음") + "] 유효하지 않은 URL입니다..");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if (page == null) page = 0;
            if (limit == null) limit = page_item_limit;

            if (articleListRequest == null) {
                articleListRequest = new ArticleListRequest(articleType, page, limit);
            } else {
                articleListRequest.setArticle_type(articleType);
                articleListRequest.setPageLimit(page, limit);
            }

            // DB 조회
            ArticleListResponse result = articleService.selectArticleList(articleListRequest);

            // 조회된 결과가 없을 경우
            if (result == null || result.getList() == null || result.getList().size() == 0) {
                log.debug("[" + article_type + "] [" + (member != null ? member.getMobile_no() : "멤버정보없음") + "]. 등록된 정보가 없습니다.");
                return makeResponseEntity(HttpStatus.NO_CONTENT);
            }

            log.debug("[" + article_type + "] [" + (member != null ? member.getMobile_no() : "멤버정보없음") + "]. 총 " + result.getList().size() + "건.");

            if (articleType != ArticleType.FAQ) { //FAQ가 아닌 경우에만 수행한다.
                result.buildUrl(articleType); //상세 조회 및 썸네일 URL을 세팅한다.
            }
            return makeResponseEntity(request, result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("[" + article_type + "] [" + (member != null ? member.getMobile_no() : "멤버정보없음") + "]. DB - 조회 오류.", e);
            return makeResponseEntity(request, "DB - " + article_type, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/article/detail/{article_type}/{cno}")
    public String detail(HttpServletRequest request, HttpSession session, Model model,
                         @PathVariable("article_type") String article_type,
                         @PathVariable("cno") Integer cno) {

        try {
            ArticleType articleType = ArticleType.find(article_type);
            if (articleType == ArticleType.UNDEF) {
                log.error("[" + article_type + "] 유효하지 않은 ARTICLE_TYPE 입니다.");
                model.addAttribute("failMessage", "요청하신 파라미터가 유효하지 않습니다.");
                return "common/error";
            }

            // DB 조회
            Article article = articleService.selectArticle(articleType, cno);

            // 조회된 결과가 없을 경우
            if (article == null) {
                log.error("[" + article_type + "] 조회 결과가 없습니다.");
                model.addAttribute("failMessage", "요청하신 글이 존재하지 않습니다.");
                return "common/error";
            }

            article.buildUrl(articleType);

            model.addAttribute("article", article);

            return "article/detail";

        } catch (Exception e) {
            log.error("[" + article_type + "]. DB - 조회 오류.", e);
            model.addAttribute("failMessage", "요청 처리중 오류가 발생하였습니다.");
            return "common/error";
        }
    }



}

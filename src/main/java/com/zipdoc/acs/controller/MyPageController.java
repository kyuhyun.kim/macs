package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.Cause;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.*;
import com.zipdoc.acs.model.CauseResponse;
import com.zipdoc.acs.utils.FileInfo;
import com.zipdoc.acs.utils.FileUploadUtil;
import com.zipdoc.acs.utils.ACSUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;


/**
 * 마이페이지 조회 기능
 * @author 김종부
 *
 */
@Controller
public class MyPageController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(MyPageController.class);
	
	@Autowired
	private MemberService memberService;

	@Autowired
	private ContractService contractService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@ResponseBody
	@RequestMapping(value = "/mypage/summary")
	public ResponseEntity<String> summary(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			MyPage myPage = memberService.selectMyPageSummary(member);
			return makeResponseEntity(request, myPage, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("마이페이지 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/mypage/nicknameChange", method = RequestMethod.POST)
	public ResponseEntity<String> nicknameChange(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@RequestBody Member reqMember) {

		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			if(StringUtils.isEmpty(reqMember.getNickname())) {
				log.error("[mypage/nicknameChange] 필수항목누락");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}
			int count = memberService.selectCountUsingNicknameCount(reqMember.getNickname());
			if(count>0){
				log.error("[mypage/nicknameChange] 이미사용중인 닉네임");
				return makeResponseEntity(HttpStatus.CONFLICT);
			}

			member.setNickname(ACSUtils.getReplaceEmoji(reqMember.getNickname()));
			memberService.updateNickname(member);
			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[mypage/nicknameChange] 닉네임 변경 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/mypage/profilePictureUpload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> profilePictureUpload(HttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
										 @RequestHeader(value = "ZID", required = true) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
										 @RequestPart(value = "attached_file", required = true) MultipartFile image) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			if(image.isEmpty()){
				log.error("[mypage/profilePictureUpload] 필수항목누락");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			//이전 파일 삭제를 위해 변수 저장
			String prevProfilePath = member.getProfile_path();
			String prevProfileFile = member.getProfile_file();

			//신규 프로필 사진 파일 생성
			String relativePath = Constants.RELATIVE_PROFILE_PATH + member.getMember_no() + "/";
			FileInfo fileInfo = FileUploadUtil.uploadFile(image, relativePath);

			//프로필 사진 정보 저장
			String fileName = fileInfo.getFile().getName();
			member.setProfile_file(fileName);
			member.setProfile_path(relativePath);
			member.setProfile_url(Constants.STATIC_URL + relativePath + fileName);
			memberService.updateProfile(member);

			FileUploadUtil.getInstance().deleteFile(fileInfo.getFile());

			//이전 프로필사진 삭제 처리
//			if(StringUtils.isNotEmpty(prevProfileFile)){
//				File file = new File(Constants.UPLOAD_STATIC_PATH + prevProfilePath + prevProfileFile);
//				FileUtils.forceDelete(file);
//			}

			/*
			s3 로 변경
			*/
			FileUploadUtil.getInstance().deleteFromS3(prevProfilePath, prevProfileFile);

			Profile result = memberService.selectProfile(member.getMember_no());

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("갤러리 상세 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/mypage/contract/{contract_no}", method = RequestMethod.GET)
	public ResponseEntity<String> contract(HttpServletRequest request,
										  @RequestHeader(value = "APP-VERSION") String app_version,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID") long zid,
										  @RequestHeader(value = "ZID-KEY") String zid_key,
										  @PathVariable("contract_no") Long contract_no) {
		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			MyContract contract = contractService.selectMyContract(contract_no);
			if (contract == null) {
				log.error("[MY-CONTRACT] 계약정보 조회 오류. 계약번호["+contract_no+"]로 검색된 결과가 없습니다.");
				return makeResponseEntity(HttpStatus.NOT_FOUND);
			}

			return makeResponseEntity(request, contract, HttpStatus.OK);
		} catch (ServiceException e) {
			log.error("[MY-CONTRACT] 계약정보 조회 오류: "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[MY-CONTRACT] 계약정보 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/mypage/edit/personal", method = RequestMethod.POST)
	public ResponseEntity<String> edit_personal(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												 @RequestHeader(value = "ZID", required = false) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												 @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
												 @RequestBody Account account) {

		try {

			// 필수 파라미터 체크
			if (StringUtils.isEmpty(account.getPassword()) || StringUtils.isEmpty(account.getUsername()) || StringUtils.isEmpty(account.getMobile_no())) {
				log.error("[MY-EDIT-PERSONAL] 필수항목누락. [PASSWORD/NAME/MOBILE_NO]");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PARAMETERS, "PASSWORD/NAME/MOBILE_NO"), HttpStatus.OK);
			}

			// 비밀번호 유효성 검사
			if (!ACSUtils.validate_password_for_user(account.getPassword())) {
				log.error("[MY-EDIT-PERSONAL] 비밀번호 복잡도 오류.");
				return makeResponseEntity(request, new CauseResponse(Cause.INVALID_PWD_USER), HttpStatus.OK);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);
			account.setMember_no(member.getMember_no());

			// 정보 업데이트
			memberService.updateMember(account);
			return makeResponseEntity(request, new CauseResponse(Cause.SUCCESS), HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[MY-EDIT-PERSONAL] 개인정보 변경 실패. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[MY-EDIT-PERSONAL] 개인정보 변경 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

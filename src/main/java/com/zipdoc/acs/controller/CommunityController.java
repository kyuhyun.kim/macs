package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.PictureType;
import com.zipdoc.acs.define.ProductType;
import com.zipdoc.acs.domain.entity.Item;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.Product;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.ProductService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.ProductListRequest;
import com.zipdoc.acs.model.ProductListResponse;
import com.zipdoc.acs.utils.Json;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 내방어때 조회 기능
 * @author 이동규
 *
 */
@Controller
public class CommunityController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(CommunityController.class);
	
	private static final Pattern urlPattern = Pattern.compile("<img[^>]*src=[\"']?([^>\"']+)[\"']?[^>]*>");
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private ProductService productService;

	@Autowired
	private AuthenticationHelper authenticationHelper;
	
	@ResponseBody
	@RequestMapping(value = "/community/list/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> list(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit,
			@RequestBody(required = false) ProductListRequest searchCondition) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			if(searchCondition == null){
				searchCondition = new ProductListRequest();
			}

			searchCondition.setPageLimit(page, limit);
			searchCondition.setMember_no(member_no);
			searchCondition.setProduct_type(ProductType.COMMUNITY.getCode());

			log.info(searchCondition.toString());

			int total_count = productService.selectCommunityListTotalCount(searchCondition);
			// 조회된 결과가 없을 경우
			if(total_count==0) {
				log.debug("[/community/list] "+page+"page 에는 내방어때 목록이 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			List<Product> list = productService.selectCommunityList(searchCondition);

			for(Product product:list){
				product.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}

			ProductListResponse result = new ProductListResponse(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/list] 내방어때 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



	@ResponseBody
	@RequestMapping(value = "/community/detail/{pid}", method = RequestMethod.GET)
	public ResponseEntity<String> detail(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("pid") long pid) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			if(pid <= 0) {
				log.error("[/community/detail] PID 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 내방어때 정보 가져오기
			Product result = productService.selectCommunityById(pid, member_no);
			if (result == null) {
				log.debug("[/community/detail] 내방어때 검색되지 않습니다. PID["+pid+"]");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			productService.updateIncreaseHitCnt(pid);

			//공유하기 URL 설정
			result.buildShareUrl();
			//사진 URL 구성
			result.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/detail] 내방어때 상세 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/community/add", method = RequestMethod.POST)
	public ResponseEntity<String> add(MultipartHttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
										 @RequestHeader(value = "ZID", required = true) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = true) String zid_key) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			String metaData = request.getParameter("meta-data");

			log.debug("[/community/add] meta-data : " + metaData);

			Product product = Json.toObjectJson(metaData, Product.class);

			if(product==null || StringUtils.isEmpty(product.getForm_data_name())){
				log.error("[/community/add] meta-data 누락");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			//사진정보를 Product 객체에 Assign한다.
			MultipartFile mainImage = request.getFile(product.getForm_data_name());
			if(mainImage==null || mainImage.isEmpty()){
				log.error("[/community/add] 대표 이미지 누락");
				return makeResponseEntity(request, "대표 이미지 누락", HttpStatus.BAD_REQUEST);
			}
			product.setMultipartFile(mainImage);
			product.setCreater(member.getMember_no());

			if(product.getItems()!=null) {
				for (Item item : product.getItems()) {
					if (!item.isCreateCmd()) {
						log.error("[/community/add] 정의되지 않은 CMD : " + item.getCmd());
						return makeResponseEntity(request, "정의되지 않은 CMD : " + item.getCmd(), HttpStatus.BAD_REQUEST);
					}

					MultipartFile itemImage = request.getFile(item.getForm_data_name());
					if (itemImage == null || itemImage.isEmpty()) {
						log.error("[/community/add] 사진 이미지 누락");
						return makeResponseEntity(request, "사진 이미지 누락", HttpStatus.BAD_REQUEST);
					}

					item.setMultipartFile(itemImage);
					item.setCreater(member.getMember_no());
					item.buildTagsByTagList();
				}
			}

			productService.insert(product);

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/add] 내방어때 등록 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/community/modify", method = RequestMethod.POST)
	public ResponseEntity<String> modify(MultipartHttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			String metaData = request.getParameter("meta-data");

			log.debug("[/community/modify] meta-data : " + metaData);

			Product product = Json.toObjectJson(metaData, Product.class);

			if(product==null || StringUtils.isEmpty(product.getForm_data_name())){
				log.error("[/community/modify] meta-data 누락");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			//사진정보를 Product 객체에 Assign한다.
			MultipartFile mainImage = request.getFile(product.getForm_data_name());
			if(mainImage==null || mainImage.isEmpty()){
				log.error("[/community/modify] 대표 이미지 누락");
				return makeResponseEntity(request, "대표 이미지 누락", HttpStatus.BAD_REQUEST);
			}
			product.setMultipartFile(mainImage);
			product.setUpdater(member.getMember_no());

			if(product.getItems()!=null) {
				for (Item item : product.getItems()) {
					if (!item.isCreateCmd() && !item.isUpdateCmd() && !item.isDeleteCmd()) {
						log.error("[/community/modify] 정의되지 않은 CMD : " + item.getCmd());
						return makeResponseEntity(request, "정의되지 않은 CMD : " + item.getCmd(), HttpStatus.BAD_REQUEST);
					}

					if (item.isCreateCmd()) {
						MultipartFile itemImage = request.getFile(item.getForm_data_name());
						if (itemImage == null || itemImage.isEmpty()) {
							log.error("[/community/modify] 사진 이미지 누락");
							return makeResponseEntity(request, "사진 이미지 누락", HttpStatus.BAD_REQUEST);
						}
						item.setMultipartFile(itemImage);
						item.buildTagsByTagList();
					} else if (item.isUpdateCmd()) {
						if (StringUtils.isNotEmpty(item.getForm_data_name())) {
							MultipartFile itemImage = request.getFile(item.getForm_data_name());
							if (itemImage == null || itemImage.isEmpty()) {
								log.error("[/community/modify] 사진 이미지 누락");
								return makeResponseEntity(request, "사진 이미지 누락", HttpStatus.BAD_REQUEST);
							}
							item.setMultipartFile(itemImage);
							item.buildTagsByTagList();
						}
					}
					item.setPid(product.getPid());
					item.setCreater(member.getMember_no());
					item.setUpdater(member.getMember_no());
				}
			}

			productService.update(product);

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/add] 내방어때 등록 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/community/delete/{pid}", method = RequestMethod.POST)
	public ResponseEntity<String> delete(HttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
										 @RequestHeader(value = "ZID", required = true) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
										 @PathVariable("pid") long pid) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			if(pid <= 0) {
				log.error("[/community/delete] PID 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 내방어때 정보 가져오기
			Product deleteTarget = productService.selectCommunityById(pid, member_no);
			if (deleteTarget == null) {
				log.debug("[/community/delete] 내방어때 검색되지 않습니다. PID["+pid+"]");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			treatDelete(deleteTarget, member);
			log.info("[/community/delete] 내방어때 삭제 성공");
			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/delete] 내방어때 삭제 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/community/delete-multi", method = RequestMethod.POST)
	public ResponseEntity<String> delete(HttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
										 @RequestHeader(value = "ZID", required = true) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
										 @RequestBody Product target) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			if(target.getArticle_no_list()==null || target.getArticle_no_list().size()<=0) {
				log.error("[/community/delete-multi] article_no_list 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			for(Long pid:target.getArticle_no_list()) {
				Product deleteTarget = productService.selectCommunityById(pid, member.getMember_no());
				treatDelete(deleteTarget, member);
			}
			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/detail] 내방어때 삭제 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/community/delete-all", method = RequestMethod.POST)
	public ResponseEntity<String> deleteAll(HttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
										 @RequestHeader(value = "ZID", required = true) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = true) String zid_key) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			productService.updateProductStatusAllByMemberNo(member.getMember_no());
			log.info("[/community/delete-all] 내방어때 삭제 성공");
			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/delete-all] 내방어때 삭제 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private void treatDelete(Product target, Member member) throws ServiceException{
		if (target == null) {
			return;
		}

		if(target.getCreater()!=null && member.getMember_no()!=null && target.getCreater().longValue()==member.getMember_no().longValue()){
			//서비스 안함으로 변경
			target.setUpdater(member.getMember_no());
			productService.updateProductStatus(target);
			log.info("[/community/treatDelete] 내방어때 삭제 성공 PID=" + target.getPid());
		} else {
			throw new ServiceException(HttpStatus.UNAUTHORIZED.value(), "삭제권한이 없습니다.");
		}
	}
}

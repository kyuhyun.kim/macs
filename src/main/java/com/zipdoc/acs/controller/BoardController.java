package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.AgentType;
import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.define.FaqType;
import com.zipdoc.acs.define.MagazineType;
import com.zipdoc.acs.domain.entity.Article;
import com.zipdoc.acs.domain.entity.Event;
import com.zipdoc.acs.domain.entity.Magazine;
import com.zipdoc.acs.domain.entity.News;
import com.zipdoc.acs.domain.service.ArticleService;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.BoardService;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.model.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
public class BoardController extends ResponseEntityService {

    private static final Logger log = LoggerFactory.getLogger(BoardController.class);

    @Autowired
    private MemberService memberService;

    @Autowired
    private BoardService boardService;

    @Autowired
    private AuthenticationHelper authenticationHelper;


    @ResponseBody
    @RequestMapping(value = "/magazine/list/{magazine_type}/{page}/{limit}")
    public ResponseEntity<String> magazine_list(HttpServletRequest request,
                                       @PathVariable("magazine_type") Integer magazine_type,
                                       @PathVariable("page") Integer page,
                                       @PathVariable("limit") Integer limit) {


        try {

            // 매거진 유형 체크
            MagazineType magazineType = MagazineType.get(magazine_type);
            if (magazineType == null) {
                log.error("[MAGAZINE-LIST] 매거진유형[" + magazine_type + "]이 유효하지 않습니다.");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            MagazineListRequest condition = new MagazineListRequest();
            condition.setMagazine_type(magazine_type);
            condition.setAgent(AgentType.WEB.getCode());
            condition.setPageLimit(page, limit);

            // DB 조회
            ListResponse result = boardService.searchMagazineList(condition);

            // 조회된 결과가 없을 경우
            if (result == null || result.getList() == null || result.getList().size() == 0) {
                log.debug("[MAGAZINE-LIST] 요청한 매거진유형[" + magazineType.getName() + "]으로 등록된 정보가 없습니다.");
                return makeResponseEntity(HttpStatus.NO_CONTENT);
            }

            log.debug("[MAGAZINE-LIST] 매거진유형[" + magazineType.getName() + "]. 총 " + result.getList().size() + "건.");

            return makeResponseEntity(request, result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("[MAGAZINE-LIST] 매거진유형[" + magazine_type + "] DB - 조회 오류.", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/magazine/detail/{cno}")
    public ResponseEntity<String> magazine_detail(HttpServletRequest request,
                                                  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                                  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                                  @RequestHeader(value = "ZID", required = false) Long zid,
                                                  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                                  @PathVariable("cno") Integer cno) {
        try {

            AgentType agent = AgentType.WEB;

            // 앱 요청인 경우 가입 여부 조회
            if (zid != null && StringUtils.isNotEmpty(zid_key)) {
                authenticationHelper.findMember(member_no, zid, zid_key);
                agent = AgentType.APP;
            }

            // DB 조회
            Magazine magazine = boardService.selectMagazineDetail(cno);

            // 조회된 결과가 없을 경우
            if (magazine == null) {
                log.debug("[MAGAZINE-DETAIL] CNO["+cno+"]로  등록된 게시글이 없습니다.");
                return makeResponseEntity(HttpStatus.NO_CONTENT);
            }

            MagazineType magazineType = MagazineType.get(magazine.getMagazine_type());
            switch (magazineType)
            {
                case TREND:
                case ZIPDOCMAN:
                case INTERIOR_GUIDE:
                    MagazineListRequest condition = new MagazineListRequest();
                    condition.setMagazine_type(magazine.getMagazine_type());
                    condition.setPageLimit(0, 3);
                    condition.setEx_cno(cno);
                    ListResponse result =  boardService.searchMagazineList( condition );
                    magazine.setList(result);
                    break;
                    default:
                        break;
            }

            return makeResponseEntity(request, magazine, HttpStatus.OK);

        } catch (Exception e) {
            log.error("[MAGAZINE-DETAIL] CNO["+cno+"] 조회 오류.", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/news/list/{page}/{limit}")
    public ResponseEntity<String> news_list(HttpServletRequest request,
                                            @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                            @RequestHeader(value = "ZID", required = false) Long zid,
                                            @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                            @PathVariable("page") Integer page,
                                            @PathVariable("limit") Integer limit) {

        try {

            MagazineListRequest condition = new MagazineListRequest();
            condition.setAgent(AgentType.WEB.getCode());
            condition.setPageLimit(page, limit);

            // DB 조회
            ListResponse result = boardService.searchNewsList(condition);

            // 조회된 결과가 없을 경우
            if (result == null || result.getList() == null || result.getList().size() == 0) {
                log.debug("[NEWS-LIST] 등록된 집닥 소식이 없습니다.");
                return makeResponseEntity(HttpStatus.NO_CONTENT);
            }

            log.debug("[NEWS-LIST] 집닥소식. 총 " + result.getList().size() + "건.");

            return makeResponseEntity(request, result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("[NEWS-LIST] 집닥소식 DB - 조회 오류.", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/news/detail/{cno}")
    public ResponseEntity<String> news_detail(HttpServletRequest request,
                                              @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                              @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                              @RequestHeader(value = "ZID", required = false) Long zid,
                                              @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                              @PathVariable("cno") Integer cno) {

        try {

            AgentType agent = AgentType.WEB;

            // 앱 요청인 경우 가입 여부 조회
            if (zid != null && StringUtils.isNotEmpty(zid_key)) {
                authenticationHelper.findMember(member_no, zid, zid_key);
                agent = AgentType.APP;
            }

            // DB 조회
            News news = boardService.selectNewsDetail(cno);

            // 조회된 결과가 없을 경우
            if (news == null) {
                log.debug("[NEWS-DETAIL] CNO["+cno+"]로  등록된 게시글이 없습니다.");
                return makeResponseEntity(HttpStatus.NO_CONTENT);
            }
            return makeResponseEntity(request, news, HttpStatus.OK);

        } catch (Exception e) {
            log.error("[NEWS-DETAIL] CNO["+cno+"] 조회 오류.", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/event/list/{page}/{limit}")
    public ResponseEntity<String> event_list(HttpServletRequest request,
                                            @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                            @RequestHeader(value = "ZID", required = false) Long zid,
                                            @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                            @PathVariable("page") Integer page,
                                            @PathVariable("limit") Integer limit) {

        try {

            AgentType agent = AgentType.WEB;

            // 앱 요청인 경우 가입 여부 조회
            if (zid != null && StringUtils.isNotEmpty(zid_key)) {
                authenticationHelper.findMember(member_no, zid, zid_key);
                agent = AgentType.APP;
            }

            ListRequest condition = new ListRequest();
            condition.setAgent(agent.getCode());
            condition.setPageLimit(page, limit);

            // DB 조회
            ListResponse result = boardService.searchEventList(condition);

            // 조회된 결과가 없을 경우
            if (result == null || result.getList() == null || result.getList().size() == 0) {
                log.debug("[EVENT-LIST] 등록된 집닥 이벤트가 없습니다.");
                return makeResponseEntity(HttpStatus.NO_CONTENT);
            }

            log.debug("[EVENT-LIST] 집닥이벤트. 총 " + result.getList().size() + "건.");



            return makeResponseEntity(request, result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("[EVENT-LIST] 집닥 이벤트 DB - 조회 오류.", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/event/detail/{cno}")
    public ResponseEntity<String> event_detail(HttpServletRequest request,
                                               @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                               @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                               @RequestHeader(value = "ZID", required = false) Long zid,
                                               @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                               @PathVariable("cno") Integer cno) {

        try {

            AgentType agent = AgentType.WEB;

            // 앱 요청인 경우 가입 여부 조회
            if (zid != null && StringUtils.isNotEmpty(zid_key)) {
                authenticationHelper.findMember(member_no, zid, zid_key);
                agent = AgentType.APP;
            }

            Map<String, Object> searchCondition = new HashMap<>();
            searchCondition.put("cno", cno);
            searchCondition.put("agent", agent.getCode());

            // DB 조회
            Event event = boardService.selectEventDetail(searchCondition);

            // 조회된 결과가 없을 경우
            if (event == null) {
                log.debug("[EVENT-DETAIL] CNO["+cno+"]로 등록된 집닥이벤트 게시글이 없습니다.");
                return makeResponseEntity(HttpStatus.NO_CONTENT);
            }
            return makeResponseEntity(request, event, HttpStatus.OK);

        } catch (Exception e) {
            log.error("[EVENT-DETAIL] CNO["+cno+"] 집닥이벤트 조회 오류.", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/faq/list/{page}/{limit}")
    public ResponseEntity<String> faq_list(HttpServletRequest request,
                                        @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                        @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                        @RequestHeader(value = "ZID", required = false) Long zid,
                                        @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                        @PathVariable("page") Integer page,
                                        @PathVariable("limit") Integer limit) {

        return faq_category_list(request, app_version, member_no, zid, zid_key, 0, page, limit);
    }

    @ResponseBody
    @RequestMapping(value = "/faq/list/{category}/{page}/{limit}")
    public ResponseEntity<String> faq_category_list(HttpServletRequest request,
                                            @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                            @RequestHeader(value = "ZID", required = false) Long zid,
                                            @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                            @PathVariable("category") Integer category,
                                            @PathVariable("page") Integer page,
                                            @PathVariable("limit") Integer limit) {

        try {

            // CATEGORY 파라미터 검사
            if (category != 0) {
                FaqType type = FaqType.get(category);
                if (type == null) {
                    log.error("[FAQ-LIST] 요청한 CATEGORY["+category+"]값은 정의되어 있지 않습니다..");
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }
            }

            FAQListRequest condition = new FAQListRequest();
            condition.setPageLimit(page, limit);
            condition.setCategory_code(category);

            // DB 조회
            ListResponse result = boardService.searchFAQList(condition);

            // 조회된 결과가 없을 경우
            if (result == null || result.getList() == null || result.getList().size() == 0) {
                log.debug("[FAQ-LIST] 등록된 FAQ 가  없습니다.");
                return makeResponseEntity(HttpStatus.NO_CONTENT);
            }

            return makeResponseEntity(request, result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("[FAQ-LIST] FAQ DB - 조회 오류.", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

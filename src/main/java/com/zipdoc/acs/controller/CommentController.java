package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.CommentType;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.Comment;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.CommentService;
import com.zipdoc.acs.model.ListResponse;
import com.zipdoc.acs.model.CommentListRequest;
import com.zipdoc.acs.utils.FileUploadUtil;
import com.zipdoc.acs.utils.IpUtil;
import com.zipdoc.acs.utils.Json;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * 댓글 컨트롤러
 * @author 이동규
 *
 */
@Controller
@RequestMapping(value = "/comment")
public class CommentController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(CommentController.class);
	
	//private static final Pattern urlPattern = Pattern.compile("<img[^>]*src=[\"']?([^>\"']+)[\"']?[^>]*>");
	
	@Autowired
	private CommentService commentService;

	@Autowired
	private AuthenticationHelper authenticationHelper;
	
	@ResponseBody
	@RequestMapping(value = "/list/{comment_type}/{cno}/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> list(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
		    @PathVariable("comment_type") int comment_type,
		    @PathVariable("cno") int cno,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit,
			@RequestBody(required = false) CommentListRequest searchCondition) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			if(searchCondition == null){
				searchCondition = new CommentListRequest();
			}

			searchCondition.setComment_type(comment_type);
			searchCondition.setCno(cno);
			searchCondition.setPageLimit(page, limit);
			searchCondition.setMember_no(member_no);

			log.info(searchCondition.toString());

			if(!searchCondition.validation()){
				log.debug("[/comment/list] 필수항목 누락");
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}

			int total_count = commentService.selectListTotalCount(searchCondition);
			// 조회된 결과가 없을 경우
			if(total_count==0) {
				log.debug("[/comment/list] "+page+"page 에는 데이터가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			List<Comment> list = commentService.selectList(searchCondition);

			//궁금해요. 이미지가 첨부되므로 URL을 build한다.
			if(comment_type== CommentType.WHATEVER.getCode()){
				for(Comment target:list){
					target.replaceStaticImageUrl();
				}
			}

			ListResponse result = new ListResponse(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/comment/list] 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> add(HttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
										 @RequestHeader(value = "ZID", required = true) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
										 @RequestBody Comment comment) {
		return treatAdd(request, app_version, member_no, zid, zid_key, comment, null);
	}

	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> add(HttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									  @RequestParam(value = "meta-data", required = true) String metaData,
									  @RequestPart(value = "attached_file", required = false) List<MultipartFile> images) {
		try {
			Comment comment = Json.toObjectJson(metaData, Comment.class);
			return treatAdd(request, app_version, member_no, zid, zid_key,	comment, images);
		} catch (Exception e){
			return makeResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}

	public ResponseEntity<String> treatAdd(HttpServletRequest request, String app_version, Long member_no, Long zid, String zid_key, Comment comment, List<MultipartFile> images) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			if(!comment.validation() || StringUtils.isEmpty(comment.getContents())){
				log.debug("[/comment/list] 필수항목 누락 " + comment.toString() );
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}

			// 파일 업로드 처리
			if(images != null && images.size() > 0) {
				String imageTag = FileUploadUtil.getInstance().uploadSmartEditorFile(images);
				comment.setContents(comment.getContents() + imageTag);
			}

			comment.setIp(IpUtil.getClientIP(request));
			comment.setCreater(member.getMember_no());

			commentService.insert(comment);
			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/comment/add] 등록 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@ResponseBody
	@RequestMapping(value = "/delete/{comment_type}/{cno}/{reply_no}", method = RequestMethod.POST)
	public ResponseEntity<String> delete(HttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									  @PathVariable("comment_type") int comment_type,
									  @PathVariable("cno") int cno,
									  @PathVariable("reply_no") int reply_no) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			Comment comment = new Comment();
			comment.setComment_type(comment_type);
			comment.setCno(cno);
			comment.setReply_no(reply_no);

			Comment result = commentService.selectById(comment);

			if(!comment.validation()){
				log.debug("[/comment/delete] 필수항목 누락");
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}

			comment.setIp(IpUtil.getClientIP(request));
			comment.setUpdater(member.getMember_no());
			comment.setContents("삭제된 글입니다.");

			//글등록자인 경우에만 삭제가능
			if(result.getCreater()!=null && result.getCreater()>0
					&& member.getMember_no()!=null && member.getMember_no() > 0
					&& result.getCreater().longValue() == member.getMember_no().longValue()){
				commentService.delete(comment);
				return makeResponseEntity(HttpStatus.OK);
			} else {
				log.error("[/comment/delete] 글삭제 권한이 없습니다. reply_no=" + reply_no + ", creater=" + result.getCreater() + ", member_no=" + member.getMember_no());
				return makeResponseEntity(request, "글삭제 권한이 없습니다", HttpStatus.FORBIDDEN);
			}
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/comment/delete] 삭제 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

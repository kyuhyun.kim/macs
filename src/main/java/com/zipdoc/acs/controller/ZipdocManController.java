package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.pns.MessageSender;
import com.zipdoc.acs.domain.pns.MsgType;
import com.zipdoc.acs.domain.pns.SendMessage;
import com.zipdoc.acs.domain.service.*;
import com.zipdoc.acs.model.*;
import com.zipdoc.acs.persistence.dao.PartnerDao;
import com.zipdoc.acs.utils.*;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 집닥맨 앱 API
 * @author 김종부
 *
 */
@Controller
@RequestMapping(value = "/zipman")
public class ZipdocManController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(ZipdocManController.class);

	@Autowired
	private ContractService contractService;

	@Autowired
	private EstimateService estimateService;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private MessageSender messageSender;

	@Autowired
	private SendMessageService sendMessageService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@ResponseBody
	@RequestMapping(value = "/contract/{page}/{limit}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> listContractVisit(HttpServletRequest request,
										@RequestHeader(value = "APP-VERSION", required = false) String app_version,
										@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										@RequestHeader(value = "ZID", required = false) Long zid,
										@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										@PathVariable("page") int page,
										@PathVariable("limit") int limit,
										@RequestBody ContractVisitRequest visitRequest) {
		try {

			// 검색 옵션
			Map<String, Object> condition = new HashMap<>();

			condition.put("startRow", page*limit);
			condition.put("limit", limit);

			// 방문시작일
			if (visitRequest.getVisit_start_date() != null) {
				condition.put("visit_start_date", DateUtil.toFormatString(new Date(visitRequest.getVisit_start_date()), "yyyy-MM-dd"));
			}

			// 방문종료일
			if (visitRequest.getVisit_end_date() != null) {
				condition.put("visit_end_date", DateUtil.toFormatString(new Date(visitRequest.getVisit_end_date()), "yyyy-MM-dd"));
			}

			// 검색옵션
			if (StringUtils.isNotEmpty(visitRequest.getSearch_condition()) && StringUtils.isNotEmpty(visitRequest.getSearch_value())) {
				condition.put(visitRequest.getSearch_condition().toUpperCase(), visitRequest.getSearch_value());
			}

			log.debug("[ZIPDOCMAN-CONTRACT-LIST] ["+member_no+"]");

			int total_count = contractService.findListCountContractVisit(condition);

			// 조회된 결과가 없을 경우
			if (total_count == 0) {
				log.debug("[ZIPDOCMAN-CONTRACT-LIST] ["+member_no+"] "+page+"page 에는 계약내용이 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			List<ContractVisit> list = contractService.findListContractVisit(condition);

			ContractVisitResponse result = new ContractVisitResponse(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("/estimates", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/contract/{contract_no}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateContractVisit(HttpServletRequest request,
									   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
									   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									   @RequestHeader(value = "ZID", required = false) Long zid,
									   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
									   @PathVariable("contract_no") Long contract_no,
									   @RequestBody ContractVisit visitRequest) {
		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			log.debug("[ZIPDOCMAN-CONTRACT-UPDATE] ["+member_no+"]");

			visitRequest.setContract_no(contract_no);
			int rows = contractService.updateContractVisit(visitRequest);

			// 하위버전 호환
			if (visitRequest.getCheck_visit_step() == null || visitRequest.getCheck_visit_step() > 0) {

				// DB 조회
				SiteVisitHistory dbHistory = contractService.selectLastSupervisionHistory(visitRequest.getContract_no());

				SiteVisitHistory history = new SiteVisitHistory();

				history.setVisit_type(SiteVisitType.SUPERVISION.getCode());
				switch (visitRequest.getCheck_visit_step()) {
					case 1:
						history.setVisit_seq(1);
						history.setVisit_status(SiteVisitStatus.SCHEDULE.getCode());
						break;
					case 2:
						history.setVisit_seq(1);
						history.setVisit_status(SiteVisitStatus.COMPLETE.getCode());
						break;
					case 3:
						history.setVisit_seq(2);
						history.setVisit_status(SiteVisitStatus.SCHEDULE.getCode());
						break;
					case 4:
						history.setVisit_seq(2);
						history.setVisit_status(SiteVisitStatus.COMPLETE.getCode());
						break;
					case 5:
						history.setVisit_seq(3);
						history.setVisit_status(SiteVisitStatus.SCHEDULE.getCode());
						break;
					case 6:
						history.setVisit_seq(3);
						history.setVisit_status(SiteVisitStatus.COMPLETE.getCode());
						break;
					default:
						history.setVisit_seq(1);
						history.setVisit_status(SiteVisitStatus.NONE.getCode());
						break;
				}
				history.setVisiting_date(visitRequest.getCheck_visit_date());
				history.setVisitors(visitRequest.getCheck_visit_name());
				history.setClient_meeting_yn(visitRequest.getClient_meeting_flag());
				history.setPartner_meeting_yn(visitRequest.getPartners_meeting_flag());
				history.setUpdater(member.getUsername());

				if (dbHistory == null) {
					history.setContract_no(visitRequest.getContract_no());
					log.debug("[ZIPDOCMAN-VISIT-HISTORY-CREATE] 하위버전지원. 사용자:" + member.getUsername());
					contractService.createVisitHistory(history);
				} else {
					history.setHistory_no(dbHistory.getHistory_no());
					log.debug("[ZIPDOCMAN-VISIT-HISTORY-UPDATE] 하위버전지원. 사용자:" + member.getUsername());
					contractService.updateVisitHistory(history);
				}
			}

			// 업데이트 결과가 없을 경우
			if(rows == 0) {
				log.debug("[ZIPDOCMAN-CONTRACT-UPDATE] ["+member_no+"] 계약번호("+contract_no+")가 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e) {
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("/estimates", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/sitehistory", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateSiteVisitHistory(HttpServletRequest request,
													  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
													  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
													  @RequestHeader(value = "ZID", required = false) Long zid,
													  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
													  @RequestBody SiteVisitHistory history) {
		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			// 이력 수정자
			history.setUpdater(member.getUsername());

			// 히스토리 구분자가 있을 경우 기존 정보 업데이트
			if (history.getHistory_no() != null) {
				SiteVisitHistory dbHistory = contractService.selectVisitHistory(history.getHistory_no());
				if (dbHistory == null) {
					log.error("[ZIPDOCMAN-VISIT-HISTORY-UPDATE] 앱사용자:"+member.getUsername()+" HISTORY_NO["+history.getHistory_no()+"]로 등록된 현장방문이력이 없습니다.");
					return makeResponseEntity(HttpStatus.GONE);
				}

				log.debug("[ZIPDOCMAN-VISIT-HISTORY-UPDATE] ["+member_no+"] 앱사용자: "+member.getUsername());
				contractService.updateVisitHistory(history);
			}

			// 히스토리 번호가 없을 경우 신규 등록
			else {

				// 계약 정보가 없을 경우 오류 처리
				if (history.getContract_no() == null) {
					log.error("[ZIPDOCMAN-VISIT-HISTORY-CREATE] 앱사용자:"+member.getUsername()+" 등록을 위한 계약번호가 없습니다.");
					return makeResponseEntity(HttpStatus.BAD_REQUEST);
				}

				// 계약 조회
				Contract contract = contractService.findById(history.getContract_no());
				if (contract == null) {
					log.error("[ZIPDOCMAN-VISIT-HISTORY-CREATE] 앱사용자:"+member.getUsername()+" 계약정보["+history.getContract_no()+"]가 없습니다.");
					return makeResponseEntity(HttpStatus.GONE);
				}

				log.debug("[ZIPDOCMAN-VISIT-HISTORY-CREATE] ["+member_no+"] 앱사용자: "+member.getUsername());
				contractService.createVisitHistory(history);
			}

			SiteVisitHistory dbHistory = contractService.selectVisitHistory(history.getHistory_no());
			return makeResponseEntity(request, dbHistory, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error("[ZIPDOCMAN-VISIT-HISTORY-UPDATE] 업데이트 오류 "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[ZIPDOCMAN-VISIT-HISTORY-UPDATE] 업데이트 오류", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/counsel/{estimate_no}", method = RequestMethod.GET)
	public ResponseEntity<String> counselList(HttpServletRequest request,
									   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
									   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									   @RequestHeader(value = "ZID", required = false) Long zid,
									   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
									   @PathVariable("estimate_no") long estimate_no) {
		try {

			log.debug("[ZIPDOCMAN-COUNSEL-LIST] ["+member_no+"] 견적번호: "+estimate_no);

			List<EstimateCounsel> list = estimateService.findCounselList(estimate_no);

			// 조회된 결과가 없을 경우
			if (list == null || list.size() == 0) {
				log.debug("[ZIPDOCMAN-COUNSEL-LIST] ["+member_no+"] 견적번호 ["+estimate_no+"]에는 상담내용이 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			return makeResponseEntity(request, list, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("/estimates", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/push/{history_no}")
	public ResponseEntity<String> push(HttpServletRequest request,
											  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
											  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											  @RequestHeader(value = "ZID", required = false) Long zid,
											  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
											  @PathVariable("history_no") long history_no,
									   		  @RequestBody SendMessage sendMsg) {
		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			// 현장방문 이력 조회
			SiteVisitHistory history = contractService.selectVisitHistory(history_no);
			if (history == null) {
				log.error("[ZIPDOCMAN-PUSH] ["+member.getUsername()+"] 요청하신 현장방문이력정보가 없습니다. HISTORY_NO["+history_no+"]");
				return makeResponseEntity(HttpStatus.NOT_FOUND);
			}

			// 현장방문 상태 체크
			if (history.getVisit_status() != SiteVisitStatus.SCHEDULE.getCode()) {
				log.error("[ZIPDOCMAN-PUSH] ["+member.getUsername()+"] 현장방문 상태가 ["+history.getVisit_status_name()+"] 예정 상태가 아닙니다.");
				return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
			}

			// 계약 정보 조회
			Contract contract = contractService.findById(history.getContract_no());
			if (contract == null) {
				log.error("[ZIPDOCMAN-PUSH] ["+member.getUsername()+"] HISTORY_NO["+history_no+"]의 계약정보가 없습니다. CONTRACT_NO["+history.getContract_no()+"]");
				return makeResponseEntity(HttpStatus.NOT_FOUND);
			}

			// 파트너 정보 조회
			Partner partner = partnerService.selectPartner(contract.getPartner_id());
			if (partner == null) {
				log.error("[ZIPDOCMAN-PUSH] ["+member.getUsername()+"] HISTORY_NO["+history_no+"] / CONTRACT_NO["+history.getContract_no()+"]에 등록된 파트너 업체["+contract.getPartner_id()+"]가 검색되지 않습니다.");
				return makeResponseEntity(HttpStatus.NOT_FOUND);
			}

			if (sendMsg == null) {
				sendMsg = new SendMessage();
			}

			// 제목
			if (StringUtils.isEmpty(sendMsg.getTitle())) {
				sendMsg.setTitle(history.getHistorySubject() + " 알림(" + contract.getContracter() + " 고객님)");
			}

			// 전송 내용
			if (StringUtils.isEmpty(sendMsg.getBody())) {
				sendMsg.setBody("안녕하세요. " + contract.getAddress() + " " + contract.getAddress_detail() +
						" 공사 현장에 집닥맨이 현장방문예정입니다. 현장에 계시면 뵙겠습니다. 현장 부재이신 경우 카톡으로 현장 비밀번호를 알려주세요. (예정일시 : " +
						DateUtil.toFormatString(history.getVisiting_date(), DateUtil.DATE_FORMAT_FOR_DISPLAY_MINUTE) + ")");
			}
			sendMsg.setMsg_type(MsgType.ZIPDOCMAN.getCode());
			sendMsg.setTargetPartner(true);

			List<MemberDevice> targetList = sendMessageService.findListByPartnerId(partner.getPartner_id());
			sendMsg.setTargetList(targetList);
			messageSender.sendPush(sendMsg);

			log.debug("[ZIPDOCMAN-PUSH] ["+member.getUsername()+"] 파트너["+partner.getPartner_name()+"] PUSH 전송. SUBJECT: "+sendMsg.getTitle());

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e) {
			log.error("[ZIPDOCMAN-PUSH] ["+member_no+"] 현장방문이력["+history_no+"] PUSH 요청 실패(1)", e);
		} catch (Exception e) {
			log.error("[ZIPDOCMAN-PUSH] ["+member_no+"] 현장방문이력["+history_no+"] PUSH 요청 실패(2)", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

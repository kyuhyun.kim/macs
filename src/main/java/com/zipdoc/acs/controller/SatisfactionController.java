package com.zipdoc.acs.controller;

import com.zipdoc.acs.Async.AsyncService;
import com.zipdoc.acs.domain.entity.BenefitRequest;
import com.zipdoc.acs.domain.entity.MyContract;
import com.zipdoc.acs.domain.entity.SatisfactionRequest;
import com.zipdoc.acs.domain.service.BenefitService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.persistence.dao.ContractDao;
import com.zipdoc.acs.persistence.dao.ShortUrlMapDao;
import com.zipdoc.acs.utils.ACSUtils;
import com.zipdoc.acs.utils.AcsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 집닥 혜택 서비스
 * @author 김종부
 *
 */
@Controller
/*@RequestMapping(value = "/satisfaction")*/
public class SatisfactionController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(SatisfactionController.class);


	@Autowired
	private AcsConfig acsConfig;

	@Autowired
	private ShortUrlMapDao shortUrlMapDao;

	@Autowired
	private ContractDao contractDao;

	@Autowired
	private AsyncService asyncService;

	/**
	 * 만족도 조사 요청
	 */
	@ResponseBody
	@RequestMapping(value = "/satisfaction/zipman", method = RequestMethod.POST)
	public ResponseEntity<String> satisfaction_result(HttpServletRequest request,
											  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,

											  @RequestBody(required = true)  SatisfactionRequest satisfactionRequest) {

		try {


			/* 계약 확인 */
			MyContract myContract = contractDao.selectMyContract(satisfactionRequest.getContract_no());
			if(myContract == null)
			{
				log.error("[SATISFACTION-{}] 요청 오류. 조회 되지 않는 계약 번호[{}]", "zipman", satisfactionRequest.getContract_no());
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			// SHORT URL 생성 등록
			satisfactionRequest.setMember_no(member_no);
			satisfactionRequest.setOrigin_url(acsConfig.getProperty("NIGNX.SERVER_URL") + acsConfig.getProperty("ZIPMAN.SATISFACTION") + satisfactionRequest.getContract_no());
			satisfactionRequest.setExpired_month(acsConfig.getProperty("ZIPMAN.EXPIRED_MONTH"));
			SatisfactionRequest target = shortUrlMapDao.selectShortUrlMap(satisfactionRequest);
			if(target == null)
			{
				satisfactionRequest.setShort_url(ACSUtils.encodeToLong(System.currentTimeMillis()));
				shortUrlMapDao.createShortUrlMap(satisfactionRequest);
			}

			satisfactionRequest.setShort_url(acsConfig.getProperty("NIGNX.SERVER_URL") + "s/" + ((target == null)? satisfactionRequest.getShort_url() : target.getShort_url()));
			shortUrlMapDao.createSurveyZipdocmanSvc(satisfactionRequest);
			/* LMS 발송 */
			asyncService.SatisfactionZipmanSendMsg(myContract, satisfactionRequest);

			log.debug("[SATISFACTION-{}] 신청완료. contract_no[{}] ", "zipman", satisfactionRequest.getContract_no());

			return makeResponseEntity(request, satisfactionRequest, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[BENEFIT-REQUEST] 내부 처리 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[BENEFIT-REQUEST] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
}

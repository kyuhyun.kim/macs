package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.*;
import com.zipdoc.acs.model.*;
import com.zipdoc.acs.model.partners.RecomPartners;
import com.zipdoc.acs.model.partners.RelaxPartners;
import com.zipdoc.acs.persistence.dao.*;
import com.zipdoc.acs.utils.AcsConfig;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.util.*;

/**
 * 앱 3.5버전부터 제공되는 메인화면 정보제공 컨트롤러
 * @author 김종부
 *
 */
@Controller
@RequestMapping(value = "/main")
public class HomeScreenController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(HomeScreenController.class);

	@Autowired
	private AcsConfig acsConfig;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@Autowired
	private HomeScreenService homeScreenService;

	@Autowired
	private ProductService productService;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private CommonDao commonDao;

	@Autowired
	private CashedCommonDao cashedCommonDao;

	@Autowired
	private EstimateDao estimateDao;

	@Autowired
	private CashedEstimateDao cashedEstimateDao;

	@Autowired
	private CustomerOpinionsService customerOpinionsService;



	/**
	 * 주거공간 메인화면 조회
	 */

	@ResponseBody
	@RequestMapping(value = "/residential", method = RequestMethod.GET)
	public ResponseEntity<String> residential(HttpServletRequest request,
											  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											  @RequestHeader(value = "ZID", required = false) Long zid,
											  @RequestHeader(value = "ZID-KEY", required = false) String zid_key) {

		try {

			AgentType agent = AgentType.APP;

			// 가입자 조회
			if (zid != null) {
				Member member = authenticationHelper.findMember(member_no, zid, zid_key);
				log.debug("[MAIN-RESIDENTIAL] MEMBER[" + member.getMember_id() + "]");
			} else {
				agent = AgentType.WEB;
				log.debug("[MAIN-RESIDENTIAL] WEB");
			}

			// 표시 항목 조회
			HomeScreenLayout layout = homeScreenService.getCashedHomeScreenLayout(CategoryCode.RESIDENT, agent);

			// 태그 사진 조회
			for (LayoutPartial item : layout.getPartials()) {
				// 태그 별 부분공간 사진 조회
				findGalleryPictureList(item, CategoryCode.RESIDENT, agent);
			}

			// 필터 시공사례 조회
			if (layout.getFilter_gallery() != null) {
				for (LayoutGallery item : layout.getFilter_gallery()) {
					findGalleryList(item, 3);
				}
			}

			// 파트너 조회
			if(agent == AgentType.APP)
				layout.getPartners().setList(getLayoutPartners(CategoryCode.RESIDENT, agent));

			// 고객 생생 후기 조회
			layout.setVividcomments( customerOpinionsService.getVividComment (CategoryCode.RESIDENT, agent, 0, 3));

			// 비포애프터 조회
			layout.setBefore_after_photos( findGalleryBeforeAfterList( new LayoutGallery(), agent== AgentType.WEB ?  3 : 2 ));

			return makeResponseEntity(request, layout, HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[MAIN-RESIDENTIAL] 내부 처리 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[MAIN-RESIDENTIAL] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*@ResponseBody
	@RequestMapping(value = "/residential", method = RequestMethod.GET)
	public ResponseEntity<String> residential(HttpServletRequest request,
											  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											  @RequestHeader(value = "ZID", required = false) Long zid,
											  @RequestHeader(value = "ZID-KEY", required = false) String zid_key) {

		try {

			AgentType agent = AgentType.APP;

			// 가입자 조회
			if (zid != null) {
				Member member = authenticationHelper.findMember(member_no, zid, zid_key);
				log.debug("[MAIN-RESIDENTIAL] MEMBER[" + member.getMember_id() + "]");
			} else {
				agent = AgentType.WEB;
				log.debug("[MAIN-RESIDENTIAL] WEB");
			}

			// 표시 항목 조회
			HomeScreenLayout layout = homeScreenService.getHomeScreenLayout(CategoryCode.RESIDENT, agent);

			// 태그 사진 조회
			for (LayoutPartial item : layout.getPartials()) {
				// 태그 별 부분공간 사진 조회
				findGalleryPictureList(item, CategoryCode.RESIDENT, agent);
			}

            // 필터 시공사례 조회
			if (layout.getFilter_gallery() != null) {
				for (LayoutGallery item : layout.getFilter_gallery()) {
					findGalleryList(item, 3);
				}
			}

			// 파트너 조회
			*//*layout.getPartners().setList(getLayoutPartners(CategoryCode.RESIDENT, agent));*//*

			// 고객 생생 후기 조회
			layout.setVividcomments( customerOpinionsService.getVividComment (CategoryCode.RESIDENT, agent, 0, 3));

			// 비포애프터 조회
			layout.setBefore_after_photos( findGalleryBeforeAfterList( new LayoutGallery(), agent== AgentType.WEB ?  3 : 2 ));

			return makeResponseEntity(request, layout, HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[MAIN-RESIDENTIAL] 내부 처리 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[MAIN-RESIDENTIAL] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
*/
	/**
	 * 배너 조회
	 */
/*
	@ResponseBody
	@RequestMapping(value = "/banners/{type}", method = RequestMethod.GET)
	public ResponseEntity<String> banners(HttpServletRequest request,
											  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											  @RequestHeader(value = "ZID", required = false) Long zid,
											  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										      @PathVariable("type") int type) {

		try {

			HomeScreenLayout layout = new HomeScreenLayout();

			// 파라미터 오류
			EventChannelType channelType = EventChannelType.get(type);
			if (channelType == null || channelType.getCode() < EventChannelType.APP_COMMERCE.getCode()) {
				log.error("[MAIN-BANNERS] 파라미터 오류. TYPE["+type+"]");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 매거진인 경우 중간 배너 제외
			if (type == EventChannelType.WEB_MAGAZINE.getCode()) {

				List<EventPromotion> dbList = commonDao.selectEventPromotionList(type);
				if (dbList != null) {
					List<EventPromotion> banners = new ArrayList<>();
					for (EventPromotion banner : dbList) {
						if (banner.getPromotion_type().equals("B")) {
							banners.add(banner);
						}
					}
					if (banners.size() > 0) {
						layout.setBanners(banners);
					}
				}
			}
			else {
				layout.setBanners(commonDao.selectEventPromotionList(type));
				if (layout.getBanners() != null) {
					for (EventPromotion banner : layout.getBanners()) {
						// 배너 유형중에 견적현황 배너가 포함된 경우
						if (banner.getLanding_type().equalsIgnoreCase("S")) {
							// 견적 현황 정보 설정
							layout.setEstimate(cashedEstimateDao.selectEstimateSummary());
							layout.getEstimate().setDoing_count(null);
							break;
						}
					}
				}
			}

			return makeResponseEntity(request, layout, HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[MAIN-BANNERS] 내부 처리 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[MAIN-BANNERS] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
*/

	/**
	 * 배너 조회 (cashed 기능 반영)
	 */
	@ResponseBody
	@RequestMapping(value = "/banners/{type}", method = RequestMethod.GET)
	public ResponseEntity<String> banners(HttpServletRequest request,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID", required = false) Long zid,
										  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										  @PathVariable("type") int type) {

		try {

			HomeScreenLayout layout = new HomeScreenLayout();

			// 파라미터 오류
			EventChannelType channelType = EventChannelType.get(type);
			if (channelType == null || channelType.getCode() < EventChannelType.APP_COMMERCE.getCode()) {
				log.error("[MAIN-BANNERS] 파라미터 오류. TYPE["+type+"]");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 매거진인 경우 중간 배너 제외
			if (type == EventChannelType.WEB_MAGAZINE.getCode()) {

				List<EventPromotion> dbList = commonDao.selectEventPromotionList(type);
				if (dbList != null) {
					List<EventPromotion> banners = new ArrayList<>();
					for (EventPromotion banner : dbList) {
						if (banner.getPromotion_type().equals("B")) {
							banners.add(banner);
						}
					}
					if (banners.size() > 0) {
						layout.setBanners(banners);
					}
				}
			}
			else {
				layout.setBanners( cashedCommonDao.selectEventPromotionList(type));
				if (layout.getBanners() != null) {
					for (EventPromotion banner : layout.getBanners()) {
						// 배너 유형중에 견적현황 배너가 포함된 경우
						if (banner.getLanding_type().equalsIgnoreCase("S")) {
							// 견적 현황 정보 설정
							layout.setEstimate(cashedEstimateDao.selectEstimateSummary());
							layout.getEstimate().setDoing_count(null);
							break;
						}
					}
				}
			}

			return makeResponseEntity(request, layout, HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[MAIN-BANNERS] 내부 처리 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[MAIN-BANNERS] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * 상업공간 메인화면 조회
	 */
	@ResponseBody
	@RequestMapping(value = "/commercial", method = RequestMethod.GET)
	public ResponseEntity<String> commercial(HttpServletRequest request,
											  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											  @RequestHeader(value = "ZID", required = false) Long zid,
											  @RequestHeader(value = "ZID-KEY", required = false) String zid_key) {

		try {

			AgentType agent = AgentType.APP;

			// 가입자 조회
			if (zid != null) {
				Member member = authenticationHelper.findMember(member_no, zid, zid_key);
				log.debug("[MAIN-COMMERCIAL] MEMBER[" + member.getMember_id() + "]");
			} else {
				agent = AgentType.WEB;
				log.debug("[MAIN-COMMERCIAL] WEB");
			}

			// 표시 항목 조회
			HomeScreenLayout layout = homeScreenService.getHomeScreenLayout(CategoryCode.COMMERCE, agent);

			// 시공사례 조회
			for (LayoutGallery item : layout.getGallery()) {
				findGalleryList(item, item.getSearchCondition().getLimit());
			}

            // 필터 시공사례 조회
            if (layout.getFilter_gallery() != null) {
                for (LayoutGallery item : layout.getFilter_gallery()) {
                    findGalleryList(item, 3);
                }
            }

			// 파트너 조회
			layout.getPartners().setList(getLayoutPartners(CategoryCode.COMMERCE, agent));

			// 고객 생생 후기 조회
			layout.setVividcomments(customerOpinionsService.getVividComment(CategoryCode.COMMERCE, agent, 0, 3));

			return makeResponseEntity(request, layout, HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[MAIN-COMMERCIAL] 내부 처리 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[MAIN-COMMERCIAL] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 파트너스 홈화면 조회
	 */
	@ResponseBody
	@RequestMapping(value = "/partners", method = RequestMethod.GET)
	public ResponseEntity<String> partners(HttpServletRequest request) {

		try {

			AgentType agent = AgentType.WEB;
			log.debug("[MAIN-PARTNERS] WEB");

			// 표시 항목 조회
			HomeScreenLayout layout = homeScreenService.getHomeScreenLayoutForPartners(agent);

			// 주거 우수 파트너 조회
			layout.setResident_best_partners(getLayoutPartners(CategoryCode.RESIDENT, agent));

			// 상업 우수 파트너 조회
			layout.setCommerce_best_partners(getLayoutPartners(CategoryCode.COMMERCE, agent));

			/*// 월간 추천 파트너 조회
			layout.setMonth_best_partners(getLayoutMonthPartners() );*/

			return makeResponseEntity(request, layout, HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[MAIN-PARTNERS] 내부 처리 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[MAIN-PARTNERS] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * 안심 파트너스 홈화면 조회(메인용)
	 */
	@ResponseBody
	@RequestMapping(value = "/relief_partners", method = RequestMethod.GET)
	public ResponseEntity<String> relief_partners(HttpServletRequest request) {

		return relief_partnersResp(request, null, null);
	}



	/**
	 * 안심 파트너스 홈화면 조회(페이징용)
	 */
	@ResponseBody
	@RequestMapping(value = "/relief_partners/{page}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<String> relief_partnersPaging(HttpServletRequest request,
													  @PathVariable("page") Integer page,
													  @PathVariable("limit") Integer limit) {
		return relief_partnersResp(request, page, limit);

	}

	public ResponseEntity<String> relief_partnersResp(HttpServletRequest request, Integer page, Integer limit)
	{
		try {

			AgentType agent = AgentType.WEB;
			log.debug("[RELIEF-PARTNERS] WEB");

			Map<String, Object> map = new HashMap<>();

			/* 안심 파트너스 조회 */
			map.put("relief_partners", getLayoutReliefPartners(page, limit));

			return makeResponseEntity(request, map, HttpStatus.OK);
		} catch (ServiceException e) {
			log.error("[RELIEF-PARTNERS] 내부 처리 오류. " + e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[RELIEF-PARTNERS] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



	/**
	 * 추천 파트너스 홈화면 조회(메인용)
	 */
	@ResponseBody
	@RequestMapping(value = "/reco_partners", method = RequestMethod.GET)
	public ResponseEntity<String> reco_partners(HttpServletRequest request) {

		return reco_partnersResp(request, null, null);
	}


	/**
    * 추천 파트너스 홈화면 조회(페이징용)
    */
	@ResponseBody
	@RequestMapping(value = "/reco_partners/{page}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<String> reco_partnersPaging(HttpServletRequest request,
												@PathVariable("page") Integer page,
												@PathVariable("limit") Integer limit) {
		return reco_partnersResp(request, page, limit);

	}


	public ResponseEntity<String> reco_partnersResp(HttpServletRequest request, Integer page, Integer limit)
	{
		try {

			AgentType agent = AgentType.WEB;
			log.debug("[RECO-PARTNERS] WEB");

			Map<String, Object> map = new HashMap<>();

			/* 추천 파트너스 조회 */
			map.put("month_best_partners", getLayoutMonthPartners(page, limit));

			/* 분기 별 파트너스 조회 */
			map.put("quarter_best_partners", getLayoutQuarterPartners(page, limit));

			return makeResponseEntity(request, map, HttpStatus.OK);
		} catch (ServiceException e) {
			log.error("[MAIN-RECO-PARTNERS] 내부 처리 오류. " + e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[MAIN-RECO-PARTNERS] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 매거진 홈화면 조회
	 */
	@ResponseBody
	@RequestMapping(value = "/magazine", method = RequestMethod.GET)
	public ResponseEntity<String> magazine(HttpServletRequest request) {

		try {

			log.debug("[MAIN-MAGAZINE] WEB");

			// 표시 항목 조회
			HomeScreenLayout layout = homeScreenService.getHomeScreenLayoutForMagazine(AgentType.WEB);

			CategoryCode categoryCode = null;

			// 고객 생생 후기 조회
			layout.setVividcomments( customerOpinionsService.getVividComment (categoryCode, AgentType.WEB, 0, 3));

			return makeResponseEntity(request, layout, HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[MAIN-MAGAZINE] 내부 처리 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[MAIN-MAGAZINE] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 집닥혜택 홈화면 조회
	 */
	@ResponseBody
	@RequestMapping(value = "/benefit", method = RequestMethod.GET)
	public ResponseEntity<String> benefit(HttpServletRequest request) {

		try {

			log.debug("[MAIN-BENEFIT] WEB");

			// 표시 항목 조회
			HomeScreenLayout layout = homeScreenService.getHomeScreenLayoutForBenefit(AgentType.WEB);

			return makeResponseEntity(request, layout, HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[MAIN-BENEFIT] 내부 처리 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[MAIN-BENEFIT] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 지정한 태그가 설정된 사진 정보를 조회한다.
	 * @param partial
	 * @return
	 */
	private void findGalleryPictureList(LayoutPartial partial, CategoryCode categoryCode, AgentType agent) {

		ItemListRequest searchCondition = new ItemListRequest();

		searchCondition.setPageLimit(0, (agent == AgentType.APP ? 4 : 8));
		searchCondition.setKeyword(partial.getTag());
		searchCondition.setCategory_code1(categoryCode.getCode());
		searchCondition.setRandom(1);

		// 제외할 태그 정보
		searchCondition.setExclude_tags(new ArrayList<String>( Arrays.asList( "BEFORE" )));

		// 전체 개수 조회
		partial.setTotal_count(productService.selectItemListTotalCount(searchCondition));

		// 랜덤 4개 조회
		List<Item> list = productService.selectItemList(searchCondition);
		if (list != null) {
			for(Item item:list){
				item.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
				/*item.buildPictureUrl(PictureType.XXHDPI, PictureType.HDPI);*/
			}
		}
		partial.setList(list);
	}



	/**
	 * 베스트 파트너 조회
	 * @param category
	 * @return
	 * @throws Exception
	 */
	private List<Partner> getLayoutPartners(CategoryCode category, AgentType agentType) throws Exception {

		Map<String, Object> condition = new HashMap<>();
		condition.put("startRow", 0);
		condition.put("limit", 3);
        condition.put("category_code1", category.getCode());
		condition.put("main_part", category.getName().substring(0,2));

		List<Partner> partners = partnerService.selectBestPartner(condition, 2);
		if (partners == null || partners.size() == 0) {
			log.error("[MAIN-RESIDENTIAL] "+category.getName()+" 베스트파트너가 없습니다.");
			throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Best Partners");
		}

		for (Partner partner : partners) {

			// 노출순위가 10인 경우 최우수 파트너로 변경
			if (partner.getSeq() != null && partner.getSeq() == 10) {
				partner.setPartner_type(3);
			}
			partner.buildPictureUrl(PictureType.XXHDPI, PictureType.HDPI);
		}

		return partners;
	}

	/**
	 * 분기별 추천 파트너 조회
	 * @param
	 * @return
	 * @throws Exception
	 */
	private List<RecomPartners> getLayoutQuarterPartners(Integer page, Integer limit) throws Exception {

		if(page == null) page =0;
		if(limit == null) limit = 6;
		Map<String, Object> condition = new HashMap<>();
		condition.put("startRow", page * limit );
		condition.put("limit", limit);
		condition.put("minCount", 6);
		condition.put("monthRange", 3);

		List<RecomPartners> recomPartnerses = partnerService.selectQuarterBestPartner(condition);
		if(recomPartnerses != null && recomPartnerses.size() > 0)
		{
			for(int nIdx = 0; nIdx < recomPartnerses.size(); nIdx++) {
				condition.put("best_year", recomPartnerses.get(nIdx).getBest_year());
				condition.put("best_quarter", recomPartnerses.get(nIdx).getBest_quarter());
				List<Partner> partners = partnerService.selectQuarterRecoBestPartnerList(condition);
				recomPartnerses.get(nIdx).setList(partners);
			}
		}


		if (recomPartnerses == null || recomPartnerses.size() == 0) {
			log.error("[MAIN-PARTNER-QUARTER] 분기별 추천 파트너가 없습니다.");
		}

		for (RecomPartners recomPartners : recomPartnerses) {

			// 노출순위가 10인 경우 최우수 파트너로 변경
			for (Partner partner : recomPartners.getList()) {
				partner.setPartner_type(1);
			/*	if (partner.getSeq() != null && partner.getSeq() == 10) {
					partner.setPartner_type(3);
				}*/
				partner.buildPictureUrl(PictureType.XXHDPI, PictureType.HDPI);
			}
		}


		return recomPartnerses;
	}


	/**
	 * 이달의 추천 파트너 조회
	 * @param
	 * @return
	 * @throws Exception
	 */
	private List<RecomPartners> getLayoutMonthPartners(Integer page, Integer limit) throws Exception {

		if(page == null) page =0;
		if(limit == null) limit = 6;
		Map<String, Object> condition = new HashMap<>();
		condition.put("startRow", page * limit );
		condition.put("limit", limit);
		condition.put("minCount", 6);
		condition.put("monthRange", 3);

		List<RecomPartners> recomPartnerses = partnerService.selectMonthBestPartner(condition);
		if(recomPartnerses != null && recomPartnerses.size() > 0)
		{
			for(int nIdx = 0; nIdx < recomPartnerses.size(); nIdx++) {
				condition.put("month", recomPartnerses.get(nIdx).getMonth());
				List<Partner> partners = partnerService.selectMonthRecoBestPartnerList(condition);
				recomPartnerses.get(nIdx).setList(partners);
			}
		}


		if (recomPartnerses == null || recomPartnerses.size() == 0) {
			log.error("[MAIN-MONTH] 이달의 추천 파트너가 없습니다.");
			throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No month recommendation Partners");
		}

		for (RecomPartners recomPartners : recomPartnerses) {


			// 노출순위가 10인 경우 최우수 파트너로 변경
			for (Partner partner : recomPartners.getList()) {
				partner.setPartner_type(1);
			/*	if (partner.getSeq() != null && partner.getSeq() == 10) {
					partner.setPartner_type(3);
				}*/
				partner.buildPictureUrl(PictureType.XXHDPI, PictureType.HDPI);
			}
		}


		return recomPartnerses;
	}

	/**
	 * 안심 파트너 조회
	 * @param
	 * @return
	 * @throws Exception
	 */
	private Map<String, Object> getLayoutReliefPartners(Integer page, Integer limit) throws Exception {

		if(page == null) page =0;
		if(limit == null) limit = 6;

		Map<String, Object> condition = new HashMap<>();
		condition.put("startRow", page * limit );
		condition.put("limit", limit);


		Integer relief_total_count = partnerService.selectReliefPartnerListCount();

		if (relief_total_count == null || relief_total_count.intValue() == 0) {
			log.error("안심 파트너스가 없습니다.");
			throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Not found Relief Partners");
		}

		List<Partner> relief_list = partnerService.selectReliefPartner(condition);

		if (relief_list == null) return null;

		relief_list = partnerService.setPartnerProductThumbnails(relief_list, 2, null);

		for (Partner partner : relief_list) {
			partner.buildPictureUrl(PictureType.XXHDPI, PictureType.HDPI);
		}


		condition.clear();
		condition.put("total_count", relief_total_count);
		condition.put("list", relief_list );

		return condition;
	}



	/**
	 * 지정한 태그가 설정된 시공사례를 조회한다.
	 * @param gallery
	 * @return
	 */
	private void findGalleryList(LayoutGallery gallery, int limit) {

		ProductListRequest searchCondition = gallery.getSearchCondition() != null ? gallery.getSearchCondition() : new ProductListRequest();
		searchCondition.setPageLimit(0, limit);
		searchCondition.setProduct_type(ProductType.GALLERY.getCode());

		searchCondition.setRandom(1);

		// 전체 개수 조회
		gallery.setTotal_count(productService.selectListTotalCount(searchCondition));

		// 랜덤 조회
		List<Product> list = productService.selectList(searchCondition);
		if (list != null) {
			for(Product item:list){
				item.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
				/*item.buildPictureUrl(PictureType.XXHDPI, PictureType.HDPI);*/
			}
		}
		gallery.setList(list);
	}

	/**
	 * 비포에프터 아이템을 조회한다.
	 * @param gallery
	 * @return
	 */
	private LayoutGallery findGalleryBeforeAfterList(LayoutGallery gallery, int limit) {

		ProductListRequest searchCondition = gallery.getSearchCondition() != null ? gallery.getSearchCondition() : new ProductListRequest();
		searchCondition.setPageLimit(0, limit);
		searchCondition.setProduct_type(ProductType.GALLERY.getCode());
		searchCondition.setTag("Before&After");
		searchCondition.setRandom(1);

		// 전체 개수 조회
		gallery.setTotal_count(productService.selectListTotalBeforeAfterCount(searchCondition));

		// 랜덤 조회
		List<Product> list = null;
			list = productService.selectBeforeAfterList(searchCondition);
		if (list != null) {
			for(Product item:list){
				item.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}
		}
		gallery.setList(list);
		return gallery;
	}


}

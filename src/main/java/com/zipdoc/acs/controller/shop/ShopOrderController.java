package com.zipdoc.acs.controller.shop;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.siot.IamportRestClient.IamportClient;
import com.siot.IamportRestClient.response.IamportResponse;
import com.siot.IamportRestClient.response.Payment;
import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.define.Channel;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.OrderGoodsStatus;
import com.zipdoc.acs.define.OrderMethodType;
import com.zipdoc.acs.define.PaymentStatus;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.ShopGoods;
import com.zipdoc.acs.domain.entity.ShopMileageHistory;
import com.zipdoc.acs.domain.entity.ShopOrder;
import com.zipdoc.acs.domain.entity.ShopOrderGoods;
import com.zipdoc.acs.domain.entity.ShopOrderInfo;
import com.zipdoc.acs.domain.entity.ShopOrderPayHistory;
import com.zipdoc.acs.domain.entity.ShopOrderUserInfo;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopGoodsService;
import com.zipdoc.acs.domain.service.ShopMileageHistoryService;
import com.zipdoc.acs.domain.service.ShopOrderGoodsService;
import com.zipdoc.acs.domain.service.ShopOrderPayHistoryService;
import com.zipdoc.acs.domain.service.ShopOrderService;
import com.zipdoc.acs.domain.service.ShopOrderUserInfoService;
import com.zipdoc.acs.model.shop.ShopOrderDetailResponse;
import com.zipdoc.acs.model.shop.ShopOrderListRequest;
import com.zipdoc.acs.model.shop.ShopOrderListResponse;
import com.zipdoc.acs.model.shop.ShopOrderRequest;
import com.zipdoc.acs.utils.DateUtil;
import com.zipdoc.acs.utils.Messages;

/**
 * 주문 기능
 * @author 김대성
 *
 */
@Controller
@RequestMapping(value = "/shop")
public class ShopOrderController extends ResponseEntityService {


    private static final Logger log = LoggerFactory.getLogger(ShopOrderController.class);

    @Autowired
    private ShopOrderService shopOrderService;

    @Autowired
    private ShopGoodsService shopGoodsService;

    @Autowired
    private ShopOrderGoodsService shopOrderGoodsService;

    @Autowired
    private ShopOrderPayHistoryService shopOrderPayHistoryService;

    @Autowired
    private ShopOrderUserInfoService shopOrderUserInfoService;

    @Autowired
    private ShopMileageHistoryService shopMileageHistoryService;

    @Autowired
    private AuthenticationHelper authenticationHelper;

    private final String SHOP_RESERVES_TYPE = "01005002"; //상품구매시 적립금사용코드

    
    @ResponseBody
    @RequestMapping(value="/order/changeOrderStatus/{sno}/{order_status}", method = RequestMethod.POST)
    public ResponseEntity<String> change(@RequestHeader(value = "APP-VERSION", required = true) String app_version,
                                      @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                      @RequestHeader(value = "ZID", required = true) Long zid,
                                      @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
                                      @PathVariable("sno") long sno,
                                      @PathVariable("order_status") int order_status){
    	try{
    		if(sno <= 0){
    			log.error("[/shop/order/handle/detail] SNO ERROR");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
    		}
    		
    		Map<String, Object> condition = new HashMap<>();
    		condition.put("sno", sno);
    		
    		switch(order_status){
			case 3:
				condition.put("status", OrderGoodsStatus.CANCLE.getCode());
				break;
			case 5:
				condition.put("status", OrderGoodsStatus.PURCHASE_COMPLETE.getCode());
				break;
			case 7:
				condition.put("status", OrderGoodsStatus.DELIVERY_COMPLETE.getCode());
				break;
    		}
    		
    		shopOrderGoodsService.updatePurchasComplete(condition);
    		return makeResponseEntity(HttpStatus.OK);
    	} catch (ServiceException e){
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch(Exception e) {
            log.error("[/shop/order/add] 주문 등록 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/order/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> add(@RequestHeader(value = "APP-VERSION", required = true) String app_version,
                                      @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                      @RequestHeader(value = "ZID", required = true) Long zid,
                                      @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
                                      @RequestBody ShopOrderRequest shopOrderRequest) {
        try {

            String orderNo = shopOrderService.getOrderNo();

            ShopOrder shopOrder = shopOrderRequest.getShopOrder();
            shopOrder.setOrder_no(orderNo);

            ShopOrderUserInfo shopOrderUserInfo = shopOrderRequest.getShopOrderUserInfo();
            shopOrderUserInfo.setOrder_no(orderNo);

            List<ShopOrderGoods> list = shopOrderRequest.getShopOrderGoods();

            // 가입자 조회
            Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

            if(member != null) {
                shopOrder.setCreater(member.getMember_no());
                shopOrderUserInfo.setMember_no(member.getMember_no());
            } else {
                //비회원일때 주문번호로 creater
                shopOrder.setCreater(Constants.NON_MEMBER);
                shopOrderUserInfo.setMember_no(Constants.NON_MEMBER);
            }

            //총주문 금액
            Integer total_order_price = shopOrder.getTotal_order_price();

            //적립금
            Integer reserves = shopOrderRequest.getReserves();

            ShopGoods shopGoods = shopGoodsService.selectById(shopOrderRequest.getGoods_no());

            //현재 등록되어있는 상품이고 판매중인상품인지
            if(shopGoods != null && shopGoods.getGoods_sell_status() == 2) {
                shopOrder.setOrder_type(Channel.APP.value());

                //적립금 내역 추가
                if(reserves != null && reserves > 0) {

                    total_order_price = total_order_price - reserves;

                    if(member_no != 0) {

                        ShopMileageHistory shopReservesHistory = shopMileageHistoryService.findByCurrentShopMileageHistory(member_no);

                        if(shopReservesHistory == null) {
                            shopReservesHistory = new ShopMileageHistory();
                            shopReservesHistory.setMember_no(member_no);
                        }

                        //이전 적립금
                        shopReservesHistory.setPrevious_mileage(member.getMileage());

                        //상품 구매시 적립금 사용
                        shopReservesHistory.setMileage_usage_history(SHOP_RESERVES_TYPE);
                        shopReservesHistory.setMileage_status(0);

                        int resultData = Math.abs(reserves)-Math.abs(shopReservesHistory.getMileage_payment());

                        shopReservesHistory.setAfter_mileage(resultData);

                        shopMileageHistoryService.insert(shopReservesHistory);

                    }

                }


                shopOrder.setTotal_order_price(total_order_price); //총 주문금액 ( 배송비 포함 )

                //카트주문일떄 배송비
                if(shopOrder.isCart_order() != true){
                    shopOrder.setTotal_delivery_charge(shopOrderRequest.getShopGoods().getGoods_delivery());
                }else{

                    Integer total_delivery_charge = shopOrder.getTotal_delivery_charge();

                    if(total_delivery_charge != null) {
                        shopOrder.setTotal_delivery_charge(total_delivery_charge);
                    }

                }

                //할인금액
                if(shopOrder.getTotal_discount_price() > 0){
                    shopOrder.setTotal_discount_price(shopOrderRequest.getShopOrder().getTotal_discount_price());
                }

                Integer orderMethod = shopOrder.getOrder_method();

                if(orderMethod != OrderMethodType.BANK_TRANSFER.getCode()) {
                    //아임포트 결제내역 확인 및 bean 셋팅
                    shopOrder = iamportResultOrderSet(shopOrder);

                    //아임포트 결제 에러
                    if(shopOrder.getPayment_status() == PaymentStatus.FAILED.getCode()) {
                        log.debug("[INTERIOR-LIST] 아임포트 결제에러.");
                        return makeResponseEntity(HttpStatus.BAD_REQUEST);
                    }
                } else {
                    //2017-04-21 계좌이체는 우선 미결제로 상태값 해놓음
                    shopOrder.setPayment_status(PaymentStatus.READY.getCode());

                    //입금 마감기한 7일더한값
                    shopOrder.setBank_date(DateUtil.toFormatAddWeekString());

                }

                shopOrderService.insert(shopOrder);

                //2017-03-28 상품테이블 주문수 업데이트
                Integer orderCnt = shopOrder.getOrder_goods_cnt();


                if(orderCnt != null && orderCnt > 0) {
                    //2017-03-28 상품테이블 주문수정
                    Map<String, Object> parameter = new HashMap<String,Object>();
                    parameter.put("goods_no",shopGoods.getGoods_no());
                    parameter.put("order_cnt",shopGoods.getOrder_cnt() + orderCnt);
                    shopGoodsService.updateIncreaseOrderCnt(parameter);
                }


                shopOrderUserInfoService.createOrderUserInfo(shopOrderUserInfo);

                //주문상품정보
                if(shopOrder.getOrder_no() != null && (list != null && list.size() > 0)){
                    for(ShopOrderGoods shopOrderGoods : list){
                        shopOrderGoods.setOrder_no(shopOrder.getOrder_no());
                        shopOrderGoods.setDelivery_status(Constants.NON_MEMBER);

                        if(orderMethod != OrderMethodType.BANK_TRANSFER.getCode()) {
                            shopOrderGoods.setOrder_status(OrderGoodsStatus.PAYMENT_COMPLETE.getCode());
                        } else {
                            // 2017-04-21 집닥 계좌이체로 하면 결제상태를 입금대기중
                            shopOrderGoods.setOrder_status(OrderGoodsStatus.DEPOSIT_WAIT.getCode());
                        }



                        shopOrderGoods.setModel_name(shopGoods.getGoods_model_no());
                        shopOrderGoods.setMaker_name(shopGoods.getMaker_name());
                        shopOrderGoods.setOrigin_name(shopGoods.getOrigin_name());
                        shopOrderGoods.setGoods_code(shopOrderGoods.getGoods_code());
                        shopOrderGoods.setGoods_name(shopOrderGoods.getGoods_name());
                        shopOrderGoods.setGoods_price(shopOrderGoods.getGoods_price());
                        shopOrderGoods.setGoods_option_sno(shopOrderGoods.getGoods_option_sno());
                        shopOrderGoods.setGoods_main_img(shopOrderGoods.getGoods_main_img());

                        //할인율
                        shopOrderGoods.setOption_discount(shopGoods.getGoods_discount());

                        //할인가
                        shopOrderGoods.setOption_discount_price(shopGoods.getGoods_discount_price());

                        //할인금액
                        shopOrderGoods.setOption_discount_purchase(shopGoods.getGoods_discount_purchase());

                        //할인상태
                        shopOrderGoods.setOption_discount_status(shopGoods.getGoods_discount_status());

                        if(shopOrderRequest.isOrder_type() == true){
                            shopOrderGoods.setGoods_no(shopOrderGoods.getGoods_no());
                        }else{
                            shopOrderGoods.setGoods_no(shopGoods.getGoods_no());
                        }
                        shopOrderGoodsService.createOrderGoods(shopOrderGoods);
                    }
                    //단품일경우
                } else {
                    ShopOrderGoods shopOrderGoods = new ShopOrderGoods();
                    shopOrderGoods.setOrder_no(shopOrder.getOrder_no());
                    shopOrderGoods.setDelivery_status(Constants.NON_MEMBER);

                    if(orderMethod != OrderMethodType.BANK_TRANSFER.getCode()) {
                        shopOrderGoods.setOrder_status(OrderGoodsStatus.PAYMENT_COMPLETE.getCode());
                    } else {
                        // 2017-04-21 집닥 계좌이체로 하면 결제상태를 입금대기중
                        shopOrderGoods.setOrder_status(OrderGoodsStatus.DEPOSIT_WAIT.getCode());
                    }

                    shopOrderGoods.setModel_name(shopGoods.getGoods_model_no());
                    shopOrderGoods.setMaker_name(shopGoods.getMaker_name());
                    shopOrderGoods.setOrigin_name(shopGoods.getOrigin_name());
                    shopOrderGoods.setGoods_code(shopGoods.getGoods_code());
                    shopOrderGoods.setGoods_name(shopGoods.getGoods_name());
                    shopOrderGoods.setDelivery_price(shopGoods.getGoods_delivery());
                    shopOrderGoods.setGoods_price(shopGoods.getGoods_price());
                    shopOrderGoods.setGoods_no(shopGoods.getGoods_no());

                    //할인율
                    shopOrderGoods.setOption_discount(shopGoods.getGoods_discount());

                    //할인가
                    shopOrderGoods.setOption_discount_price(shopGoods.getGoods_discount_price());

                    //할인금액
                    shopOrderGoods.setOption_discount_purchase(shopGoods.getGoods_discount_purchase());

                    //할인상태
                    shopOrderGoods.setOption_discount_status(shopGoods.getGoods_discount_status());

                    shopOrderGoods.setGoods_option_cnt(shopOrder.getOrder_goods_cnt());
                    shopOrderGoods.setGoods_main_img(shopOrderGoods.getGoods_main_img());
                    shopOrderGoodsService.createOrderGoods(shopOrderGoods);
                }

            }

            return makeResponseEntity(HttpStatus.OK);

        } catch (ServiceException e){
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch(Exception e) {
            log.error("[/shop/order/add] 주문 등록 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value="/order/list/{page}/{limit}", method = RequestMethod.POST)
    public ResponseEntity<String> list(HttpServletRequest request,
            @RequestHeader(value = "APP-VERSION", required = true) String app_version,
            @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
            @RequestHeader(value = "ZID", required = true) Long zid,
            @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
            @PathVariable("page") int page,
            @PathVariable("limit") int limit){
    	try{
    		ShopOrderListRequest condition = new ShopOrderListRequest(page, limit);
    		
    		Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
    		
    		condition.setMember_no(member.getMember_no());
    		
    		int total_count = shopOrderService.selectListTotalCount(condition);
    		
    		if(total_count == 0){
    			log.debug("[ORDER-LIST] "+page+"page 에는 주문 목록이 없습니다.");
    			return makeResponseEntity(HttpStatus.NO_CONTENT);
    		}
    		
    		List<ShopOrder> list = shopOrderService.selectList(condition);
    		
    		ShopOrderListResponse result = new ShopOrderListResponse(total_count, list);
    		
    		return makeResponseEntity(request, result, HttpStatus.OK);
    		
    	} catch (ServiceException e) {
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch (Exception e) {
            log.error("주문 리스트 조회 실패", e);
           return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @ResponseBody
    @RequestMapping(value="/order/detail/{order_no}", method = RequestMethod.GET)
    public ResponseEntity<String> detail(HttpServletRequest request,
            @RequestHeader(value = "APP-VERSION", required = true) String app_version,
            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
            @RequestHeader(value = "ZID", required = true) Long zid,
            @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
            @PathVariable("order_no") String order_no,
            @RequestParam(value="order_name", required = false) String order_name){
    	try{
    		ShopOrderDetailResponse result = null;
    		Member member = authenticationHelper.findMember(member_no, zid, zid_key);
    		
    		if(order_no == null){
    			log.error("[/detail/order_no] order_no error");
    			return makeResponseEntity(HttpStatus.BAD_REQUEST);
    		}
    		
    		ShopOrderInfo shopOrderInfo = shopOrderService.findByIdWithOrderGoods(order_no);
    		
    		Map<String,Object> condition = new HashMap<>();
    		condition.put("order_no", order_no);
    		condition.put("order_name", order_name);
    		
    		if(member_no != null){
    			ShopOrderUserInfo shopOrderUserInfo = shopOrderUserInfoService.findById(order_no);
	    		result = new ShopOrderDetailResponse(shopOrderUserInfo, shopOrderInfo);
    		}else{
    			ShopOrderUserInfo shopOrderUserInfo = shopOrderUserInfoService.findByNonMember(condition);
    			result = new ShopOrderDetailResponse(shopOrderUserInfo, shopOrderInfo, order_name);
    		}
    		
    		return makeResponseEntity(request, result, HttpStatus.OK);
    	} catch (ServiceException e) {
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch (Exception e) {
            log.error("주문 상세 조회 실패", e);
           return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //2017-03-30 아임포트 REST API 주문번호르 결제내역상태확인 및 결제히스토리 저장 메서드
    public ShopOrder iamportResultOrderSet(final ShopOrder shopOrder) {

        ShopOrder tempShopOrder = shopOrder;

        ShopOrderPayHistory shopOrderPayHistory = null;

        try {


            IamportResponse<Payment> paymentIamportResponse = paymentIamportResponse(shopOrder.getOrder_no());

            //0이면 정상적인 조회, 0아닌 값이면 message를 확인해봐야 합니다 ,
            if(paymentIamportResponse != null && paymentIamportResponse.getCode() == 0) {

                //아임포트 결제정보
                Payment paymentResult = paymentIamportResponse.getResponse();

                if(paymentResult != null) {

                    //아임포트 결제정보 복사
                    //BeanUtils.copyProperties(paymentResult,shopOrderPayHistory);
                    shopOrderPayHistory = paymentDataCopy(paymentResult);

                    //아임포트와 가맹점 주문번호 매칭 확인
                    if(paymentResult.getMerchantUid().equalsIgnoreCase(tempShopOrder.getOrder_no())) {

                        //결제상태
                        String status = paymentResult.getStatus();

                        //결제까지 성공적으로 완료
                        if(status.equalsIgnoreCase("paid") && paymentResult.getAmount().intValue() == tempShopOrder.getTotal_order_price()) {
                            tempShopOrder.setPayment_status(PaymentStatus.PAID.getCode());
                            //가상계좌발급성공
                        } else if(status.equalsIgnoreCase("ready") && paymentResult.getPayMethod().equalsIgnoreCase("vbank")) {
                            tempShopOrder.setPayment_status(PaymentStatus.READY.getCode());
                            //입금받을 가상계좌 은행명
                            String vbank_name = paymentResult.getVbankName();
                            //입금받을 가상계좌 계좌번호
                            String vbank_num = paymentResult.getVbankNum();
                            //입금받을 가상계좌 예금주 -> 나이스정보통신은 예금주 데이터를 안내려준다고함 아임포트 문의

                            //입금받을 가상계좌 마감기한 UNIX timestamp
                            Date vbank_date = paymentResult.getVbankDate();

                            if(vbank_name != null)
                                tempShopOrder.setBank_account(vbank_name);

                            //가상계좌에 공백이 포함되어있어서 공백제거해줘야함
                            if(vbank_num != null)
                                tempShopOrder.setBank_number(StringUtils.deleteWhitespace(vbank_num));

                            if(vbank_date != null)
                                tempShopOrder.setBank_date(vbank_date);

                            //결제취소,결제실패 처리
                        } else {

                            //결제취소시
                            if(status.equalsIgnoreCase("cancelled")) {
                                tempShopOrder.setPayment_status(PaymentStatus.CANCELLED.getCode());
                                //결제실패시
                            } else {
                                tempShopOrder.setPayment_status(PaymentStatus.FAILED.getCode());
                            }

                        }
                    }

                }

                //code값이 0이 아닐 때, '존재하지 않는 결제정보입니다'와 같은 오류 메세지를 포함합니다 ,
            } else {
                tempShopOrder.setPayment_status(PaymentStatus.FAILED.getCode());
            }

        } catch (Exception e) {
            e.printStackTrace();
            tempShopOrder.setPayment_status(PaymentStatus.FAILED.getCode());
        }

        //결제 히스토리 저장
        if(shopOrderPayHistory.getStatus() != null) {
            shopOrderPayHistoryService.createOrderPayHistory(shopOrderPayHistory);
        }

        return tempShopOrder;
    }


    //아임포트 클라이언트 생성 및 Response 셋팅
    public IamportResponse<Payment> paymentIamportResponse(String order_no) {

        IamportResponse<Payment> paymentIamportResponse = null;

        try {

            String iamportApiKey = Messages.getMessage("IAMPORT_API_KEY");
            String iamportApiSecretKey = Messages.getMessage("IAMPORT_API_SECRET");

            IamportClient iamportClient = new IamportClient(iamportApiKey,iamportApiSecretKey);

            paymentIamportResponse = iamportClient.paymentByImpUid(order_no);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return paymentIamportResponse;

    }




    // 2017-03-30 BeanUtils.copyProperties 메소드가 안먹혀서 우선 Payment Bean셋팅용 메서드 만듬
    public ShopOrderPayHistory paymentDataCopy(final Payment payment) {

        ShopOrderPayHistory shopOrderPayHistory = new ShopOrderPayHistory();

        if(payment != null) {

            shopOrderPayHistory.setOrder_no(payment.getMerchantUid());
            shopOrderPayHistory.setImp_uid(payment.getImpUid());
            shopOrderPayHistory.setPay_method(payment.getPayMethod());
            shopOrderPayHistory.setPg_provider(payment.getPgProvider());
            shopOrderPayHistory.setPg_tid(payment.getPgTid());
            shopOrderPayHistory.setEscrow(payment.isEscrow());
            shopOrderPayHistory.setApply_num(payment.getApplyNum());
            shopOrderPayHistory.setCard_name(payment.getCardName());
            shopOrderPayHistory.setCard_quota(payment.getCardQuota());
            shopOrderPayHistory.setVbank_name(payment.getVbankName());
            shopOrderPayHistory.setVbank_num(payment.getVbankNum());
            shopOrderPayHistory.setVbank_holder(payment.getVbankHolder());

            if(payment.getVbankDate().getTime() > 0) {
                shopOrderPayHistory.setVbank_date(payment.getVbankDate());
            }

            shopOrderPayHistory.setOrder_name(payment.getName());
            shopOrderPayHistory.setOrder_amount(payment.getAmount().intValue());
            shopOrderPayHistory.setOrder_cancel_amount(payment.getCancelAmount().intValue());
            shopOrderPayHistory.setBuyer_name(payment.getBuyerName());
            shopOrderPayHistory.setBuyer_tel(payment.getBuyerTel());
            shopOrderPayHistory.setBuyer_addr(payment.getBuyerAddr());
            shopOrderPayHistory.setBuyer_postcode(payment.getBuyerPostcode());
            shopOrderPayHistory.setCustom_data(payment.getCustomData());
            shopOrderPayHistory.setStatus(payment.getStatus());


            if(payment.getPaidAt().getTime() > 0) {
                shopOrderPayHistory.setPaid_at(payment.getPaidAt());
            }

            if(payment.getFailedAt().getTime() > 0) {
                shopOrderPayHistory.setFailed_at(payment.getFailedAt());
            }

            shopOrderPayHistory.setCancel_reason(payment.getCancelReason());
            shopOrderPayHistory.setReceipt_url(payment.getReceiptUrl());
        }



        return shopOrderPayHistory;
    }

}

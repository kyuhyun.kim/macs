package com.zipdoc.acs.controller.shop;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.ShopPartnerReq;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopPartnerService;

@Controller
@RequestMapping(value="/shop/partner")
public class ShopPartnerController extends ResponseEntityService{
	
	private Logger logger = LoggerFactory.getLogger(ShopPartnerController.class);
	
	@Autowired
	private ShopPartnerService shopPartnerService;
	
	@ResponseBody
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ResponseEntity<String> add(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
            @RequestHeader(value = "ZID", required = true) Long zid,
            @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
            @RequestBody ShopPartnerReq shopPartnerReq){
		try{
			
			if(StringUtils.isEmpty(shopPartnerReq.getSubject()) ||
					StringUtils.isEmpty(shopPartnerReq.getContents()) ||
							shopPartnerReq.getRequest_type() < 0){
				logger.error("[/shopPartnerReq/add] 필수항목이 누락되었습니다." + shopPartnerReq.toString());
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}
			
			shopPartnerService.create(shopPartnerReq);
			return makeResponseEntity(HttpStatus.OK);
		}catch (ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			logger.error("[/shopPartnerReq/add] 등록 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

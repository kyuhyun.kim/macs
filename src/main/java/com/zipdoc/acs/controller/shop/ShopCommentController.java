package com.zipdoc.acs.controller.shop;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.ShopComment;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopCommentService;
import com.zipdoc.acs.model.CommentListRequest;
import com.zipdoc.acs.model.ListResponse;
import com.zipdoc.acs.utils.FileUploadUtil;
import com.zipdoc.acs.utils.IpUtil;
import com.zipdoc.acs.utils.Json;

@Controller
@RequestMapping(value="/shop/comment")
public class ShopCommentController extends ResponseEntityService{
	private Logger logger = LoggerFactory.getLogger(ShopCommentController.class);
	
	@Autowired
	private ShopCommentService shopCommentService;
	
	@Autowired
	private AuthenticationHelper authenticationHelper;
	
	@ResponseBody
	@RequestMapping(value="/list/{comment_type}/{page}/{limit}", method=RequestMethod.POST)
	public ResponseEntity<String> list(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("comment_type") int comment_type,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit,
			@RequestBody(required=false) CommentListRequest condition){
		try{
			if(condition == null){
				condition = new CommentListRequest();
			}
			
  			condition.setComment_type(comment_type);
			condition.setPageLimit(page, limit);
			condition.setMember_no(member_no);
			
			logger.info(condition.toString());
			
			/*if(!condition.validation()){
				logger.debug("[/shop/comment/list] 필수항목 누락");
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}*/
			
			int total_count = shopCommentService.selectListTotalCount(condition);
			// 조회된 결과가 없을 경우
			if(total_count==0) {
				logger.debug("[/comment/list] "+page+"page 에는 데이터가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

 			List<ShopComment> list = shopCommentService.selectList(condition);
			
			ListResponse result = new ListResponse(total_count, list);
			
			return makeResponseEntity(request, result, HttpStatus.OK);
		}catch (ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
 			logger.error("[/comment/list] 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/list/{comment_type}/{cno}/{page}/{limit}", method=RequestMethod.POST)
	public ResponseEntity<String> boardCommentList(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("comment_type") int comment_type,
			@PathVariable("cno") int cno,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit,
			@RequestBody(required=false) CommentListRequest condition){
		try{
			if(condition == null){
				condition = new CommentListRequest();
			}
			
			condition.setComment_type(comment_type);
			condition.setCno(cno);
			condition.setPageLimit(page, limit);
			
			if(!condition.validation()){
				logger.debug("[/comment/list] 필수항목 누락");
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}
			
			List<ShopComment> list = shopCommentService.selectList(condition);
			
			int total_cnt = shopCommentService.selectListTotalCount(condition);
			if(total_cnt == 0){
				logger.debug("[/comment/list/cno] "+page+"page에는 데이터가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}
			
			ListResponse result = new ListResponse<>(total_cnt, list);
			
			return makeResponseEntity(request, result, HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			logger.error("[/comment/list] 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value ="/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> add(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			 @RequestHeader(value = "ZID", required = true) Long zid,
			 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			 @RequestBody ShopComment shopComment){
		return treatAdd(request, app_version, member_no, zid, zid_key, shopComment, null);
	}
	
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> add(HttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									  @RequestParam(value = "meta-data", required = true) String metaData,
									  @RequestPart(value = "attached_file", required = false) List<MultipartFile> images) {
		try {
			ShopComment comment = Json.toObjectJson(metaData, ShopComment.class);
			return treatAdd(request, app_version, member_no, zid, zid_key,	comment, images);
		} catch (Exception e){
			return makeResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	
	public ResponseEntity<String> treatAdd(HttpServletRequest request, String app_version, Long member_no, Long zid, String zid_key, ShopComment comment, List<MultipartFile> images) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			if(!comment.validation() || StringUtils.isEmpty(comment.getContents())){
				logger.debug("[/comment/list] 필수항목 누락 " + comment.toString() );
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}

			// 파일 업로드 처리
			if(images != null && images.size() > 0) {
				String imageTag = FileUploadUtil.uploadSmartEditorFile(images);
				comment.setContents(comment.getContents() + imageTag);
			}
			
			if(comment.getComment_type() == 6){
				comment.setPoint(comment.getPoint());
			}
			
			comment.setIp(IpUtil.getClientIP(request));
			comment.setCreater(member.getMember_no());
			
			int row = shopCommentService.findByCreater(comment);
			if(row > 0){
				logger.error("[comment/add] 이미 상품후기를 작성했습니다.");
				return makeResponseEntity(HttpStatus.CONFLICT);
			}

			shopCommentService.insert(comment);
			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			logger.error("[/comment/add] 등록 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{comment_type}/{cno}/{reply_no}", method = RequestMethod.POST)
	public ResponseEntity<String> delete(HttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									  @PathVariable("comment_type") int comment_type,
									  @PathVariable("cno") int cno,
									  @PathVariable("reply_no") int reply_no) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			ShopComment comment = new ShopComment();
			comment.setComment_type(comment_type);
			comment.setCno(cno);
			comment.setReply_no(reply_no);

			ShopComment result = shopCommentService.selectById(comment);

			if(!comment.validation()){
				logger.debug("[/comment/delete] 필수항목 누락");
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}

			comment.setIp(IpUtil.getClientIP(request));
			comment.setUpdater(member.getMember_no());
			comment.setContents("삭제된 글입니다.");

			//글등록자인 경우에만 삭제가능
			if(result.getCreater()!=null && result.getCreater()>0
					&& member.getMember_no()!=null && member.getMember_no() > 0
					&& result.getCreater().longValue() == member.getMember_no().longValue()){
				shopCommentService.updateDeleteFlag(comment);
				return makeResponseEntity(HttpStatus.OK);
			} else {
				logger.error("[/comment/delete] 글삭제 권한이 없습니다. reply_no=" + reply_no + ", creater=" + result.getCreater() + ", member_no=" + member.getMember_no());
				return makeResponseEntity(request, "글삭제 권한이 없습니다", HttpStatus.FORBIDDEN);
			}
		} catch (ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			logger.error("[/comment/delete] 삭제 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

package com.zipdoc.acs.controller.shop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.ShopCart;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopCartService;
import com.zipdoc.acs.model.shop.ShopCartRequest;
import com.zipdoc.acs.model.shop.ShopCartResponse;

/**
 * Created by dskim on 2017. 5. 10..
 */
@Controller
@RequestMapping(value="/shop/cart")
public class ShopCartController extends ResponseEntityService{
	
	private Logger logger = LoggerFactory.getLogger(ShopCartController.class);
	
	@Autowired
	private ShopCartService shopCartService;
	
	@Autowired
	private AuthenticationHelper authenticationHelper;
	
	@ResponseBody
	@RequestMapping(value="/list/{page}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<String > list(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
            @RequestHeader(value = "ZID", required = true) Long zid,
            @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
            @PathVariable("page") int page,
            @PathVariable("limit") int limit,
            @RequestParam(value="nonmember_no", required = false) String nonmember_no,
            @RequestBody(required = false) ShopCartRequest cartRequest){
		try{
			if(cartRequest == null){
				cartRequest = new ShopCartRequest();
			}
			cartRequest.setPageLimit(page, limit);
			
			if(nonmember_no != null){
				cartRequest.setNonmember_no(nonmember_no);
			}else{
				cartRequest.setMember_no(member_no);
			}
			
			List<ShopCart> list = shopCartService.findList(cartRequest);
			int total_count = shopCartService.findListCount(cartRequest);
			
			//총 상품금액
	        int totalGoodsPrice = 0;

	        //상품 총 기본할인
	        int totalDiscountPrice = 0;

	        //총 배송비
	        int totalDeliveryPrice = 0;

	        //총 결제예정금액
	        int totalGoodsPayment = 0;
			
	        if(list != null){
	        	long goods_no = 0;
	        	
	        	for(ShopCart shopCart : list){
	        		totalDiscountPrice += shopCart.getGoods_discount_price() * shopCart.getGoods_cnt();
	        		
	        		Long option_sno = shopCart.getOption_sno();
	        		
	        		long tempGoodsNo = shopCart.getGoods_no();
	        		
	        		if(goods_no != tempGoodsNo){
	        			totalDeliveryPrice += shopCart.getDelivery_price();
	        			goods_no = tempGoodsNo;
	        		}
	        		
	        		if(option_sno != null && option_sno != 0){
	        			totalGoodsPrice += shopCart.getOption_price() * shopCart.getGoods_cnt();
	        		}else{
	        			totalGoodsPrice += shopCart.getGoods_price() * shopCart.getGoods_cnt();
	        		}
	        	}
	        }
	        
	        totalGoodsPayment = totalGoodsPrice + totalDeliveryPrice;
	        
	        if(totalDiscountPrice > 0){
	        	totalGoodsPayment = totalGoodsPayment - totalDiscountPrice;
	        }
	        
	        //ShopCartRequest shopCartRequest = new ShopCartRequest();
	        cartRequest.getCartPrice().setTotalGoodsPrice(totalGoodsPrice);
	        cartRequest.getCartPrice().setTotalGoodsPayment(totalGoodsPayment);
	        cartRequest.getCartPrice().setTotalDiscountPrice(totalDiscountPrice);
	        cartRequest.getCartPrice().setTotalDeliveryPrice(totalDeliveryPrice);
	        
			ShopCartResponse result = new ShopCartResponse(total_count, list, cartRequest.getCartPrice());
			
			return makeResponseEntity(request, result, HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		}catch(Exception e){
			logger.error("장바구니 목록 조회 실패",e);
			return makeResponseEntity(request, "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public ResponseEntity<String> add(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
            @RequestHeader(value = "ZID", required = true) Long zid,
            @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
            @RequestParam(value="nonmember_no", required = false) String nonmember_no,
            @RequestBody List<ShopCart> cartList){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);
			
			Map<String, Object> condition = new HashMap<>();
			Map<String, Object> cnt_condition = new HashMap<>();
			int index = 0;
			ShopCart cart = null;
			
			
			//회원인 경우
			if(member_no != null){
				condition.put("member_no", member.getMember_no());
				
				//List<ShopCart> cartList = shopCartService.findList(condition);
				
				for(ShopCart shopCart : cartList){
					shopCart.setMember_no(member.getMember_no());
					
					if (cartList.get(index).getOption_discount_status() != null
							&& cartList.get(index).getOption_discount_status() == 2) {
						shopCart.setOption_price(shopCart.getOption_discount_purchase());
					}
					
					if(cartList.get(index).getOption_sno() > 0){
						condition.put("sno", cartList.get(index).getOption_sno());
					}else{
						condition.put("goods_no", cartList.get(index).getGoods_no());
					}
					
					cart = shopCartService.findById(condition);
					
					if(cart != null){
						cnt_condition.put("goods_cnt", cartList.get(index).getGoods_cnt() + cart.getGoods_cnt());
						if(cart.getOption_sno() > 0){
							cnt_condition.put("sno", cart.getSno());
						}else{
							cnt_condition.put("goods_no", cart.getGoods_no());
						}
						
						shopCartService.updateCnt(cnt_condition);
					}else{
						shopCartService.create(shopCart);
					}
					index++;
				}
			}else{
				
				for(ShopCart shopCart : cartList){
					condition.put("nonmember_no", nonmember_no);
					
					if(cartList.get(index).getOption_discount_status() != null && cartList.get(index).getOption_discount_status() == 2){
						shopCart.setOption_price(shopCart.getOption_discount_purchase());
					}
					
					if (cartList.get(index).getOption_sno() > 0) {
						condition.put("sno", cartList.get(index).getOption_sno());
					} else {
						condition.put("goods_no", cartList.get(index).getGoods_no());
					}

					cart = shopCartService.findById(condition);
					if (cart != null) {
						cnt_condition.put("goods_cnt",
								cartList.get(index).getGoods_cnt() + cart.getGoods_cnt());
						if (cart.getOption_sno() > 0) {
							cnt_condition.put("sno", cart.getSno());
						} else {
							cnt_condition.put("goods_no", cart.getGoods_no());
						}

						shopCartService.updateCnt(cnt_condition);
					} else {
						shopCart.setNonmember_no(nonmember_no);
						shopCartService.create(shopCart);
					}
					index++;
				}
			}
			
			return makeResponseEntity(HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		}catch(Exception e){
			logger.error("장바구니 추가 실패",e);
			return makeResponseEntity(request, "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/updateCnt/{sno}" , method = RequestMethod.POST)
	public ResponseEntity<String> updateCnt(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
            @RequestHeader(value = "ZID", required = true) Long zid,
            @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
            @PathVariable long sno,
            @RequestParam(value="goods_cnt") int goods_cnt){
		try{
			Map<String,Object> condition = new HashMap<>();
			condition.put("sno", sno);
			condition.put("goods_cnt", goods_cnt);
			shopCartService.updateCnt(condition);
			return makeResponseEntity(HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		}catch(Exception e){
			logger.error("장바구니 수량변경 실패",e);
			return makeResponseEntity(request, "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/updateOption/{sno}" , method = RequestMethod.POST)
	public ResponseEntity<String> updateOption(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
            @RequestHeader(value = "ZID", required = true) Long zid,
            @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
            @PathVariable long sno,
            @RequestParam(value="option_sno") long option_sno){
		try{
			Map<String,Object> condition = new HashMap<>();
			condition.put("sno", sno);
			condition.put("option_sno", option_sno);
			shopCartService.updateOption(condition);
			return makeResponseEntity(HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		}catch(Exception e){
			logger.error("장바구니 옵션 변경 실패",e);
			return makeResponseEntity(request, "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/delete/{sno}" , method = RequestMethod.POST)
	public ResponseEntity<String> delete(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
            @RequestHeader(value = "ZID", required = true) Long zid,
            @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
            @PathVariable long sno,
            @RequestParam(value="nonmember_no") String nonmember_no){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);
			
			Map<String, Object> condition = new HashMap<>();
			
			if(member != null){
				condition.put("member_no", member_no);
			}else{
				condition.put("nonmember_no", nonmember_no);
			}
			condition.put("sno", sno);
			
			shopCartService.delete(condition);
			return makeResponseEntity(HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		}catch(Exception e){
			logger.error("장바구니 옵션 변경 실패",e);
			return makeResponseEntity(request, "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value ="/delete-multi", method = RequestMethod.POST)
	public ResponseEntity<String> deleteMulti(HttpServletRequest request,
												@RequestHeader(value = "APP-VERSION", required = true) String app_version,
												@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
												@RequestHeader(value = "ZID", required = true) Long zid,
												@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
												@RequestBody ShopCart cart,
												@RequestParam(value="nonmember_no", required = false) String nonmember_no){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);
			
			if(cart.getSno_list() == null || cart.getSno_list().size() <= 0){
				logger.error("[/shop/cart/delete-multi] 필수 항목 누락");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}
			
			
			if(member != null){
				cart.setMember_no(member.getMember_no());
			}else{
				cart.setNonmember_no(nonmember_no);
			}
			
			shopCartService.deleteCartMulti(cart);
			return makeResponseEntity(HttpStatus.OK);
		} catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		}catch(Exception e){
			logger.error("장바구니 선택 삭제 실패",e);
			return makeResponseEntity(request, "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/allDelete", method = RequestMethod.POST)
	public ResponseEntity<String> allDelete(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
            @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
            @RequestHeader(value = "ZID", required = true) Long zid,
            @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
            @RequestParam(value="nonmember_no") String nonmember_no){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);
			Map<String, Object> condition = new HashMap<>();
			if(member != null){
				condition.put("member_no", member_no);
			}else{
				condition.put("nonmember_no", nonmember_no);
			}
			
			shopCartService.deleteCartAll(condition);
			return makeResponseEntity(HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		}catch(Exception e){
			logger.error("장바구니 옵션 변경 실패",e);
			return makeResponseEntity(request, "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

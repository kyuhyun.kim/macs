package com.zipdoc.acs.controller.shop;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.ShopGoods;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopGoodsService;
import com.zipdoc.acs.model.shop.ShopGoodsListRequest;
import com.zipdoc.acs.model.shop.ShopGoodsListResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 상품 조회 기능
 * @author 김대성
 *
 */
@Controller
@RequestMapping(value = "/shop")
public class ShopGoodsController extends ResponseEntityService {

    private static final Logger log = LoggerFactory.getLogger(ShopGoodsController.class);


    @Autowired
    private ShopGoodsService shopGoodsService;

    @Autowired
    private AuthenticationHelper authenticationHelper;

    @ResponseBody
    @RequestMapping(value = "/goods/list/{page}/{limit}", method = RequestMethod.POST)
    public ResponseEntity<String> list(HttpServletRequest request,
                                       @RequestHeader(value = "APP-VERSION", required = true) String app_version,
                                       @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                       @RequestHeader(value = "ZID", required = true) Long zid,
                                       @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
                                       @PathVariable("page") int page,
                                       @PathVariable("limit") int limit,
                                       @RequestBody(required = false) ShopGoodsListRequest searchCondition) {
        try {

            if(searchCondition == null){
                searchCondition = new ShopGoodsListRequest();
            }

            searchCondition.setPageLimit(page, limit);

            log.info(searchCondition.toString());

            int total_count = shopGoodsService.selectListTotalCount(searchCondition);

            // 조회된 결과가 없을 경우
            if(total_count==0) {
                log.debug("[INTERIOR-LIST] "+page+"page 에는 상품 목록이 없습니다.");
                return makeResponseEntity(HttpStatus.NO_CONTENT);
            }

            List<ShopGoods> list = shopGoodsService.selectList(searchCondition);

            for(ShopGoods shopGoods:list){
                shopGoods.buildGoodsImageUrl();
            }

            ShopGoodsListResponse result = new ShopGoodsListResponse(total_count, list);


            return makeResponseEntity(request, result, HttpStatus.OK);

        } catch (ServiceException e) {
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch (Exception e) {
            log.error("상품 목록 조회 실패", e);
           return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @ResponseBody
    @RequestMapping(value = "/goods/detail/{goods_no}", method = RequestMethod.GET)
    public ResponseEntity<String> detail(HttpServletRequest request,
                                         @RequestHeader(value = "APP-VERSION", required = true) String app_version,
                                         @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                         @RequestHeader(value = "ZID", required = true) Long zid,
                                         @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
                                         @PathVariable("goods_no") long goods_no) {

        try {

            if(goods_no <= 0) {
                log.error("[INTERIOR-DETAIL] PID 오류");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            // 상품 정보 가져오기
            ShopGoods result = shopGoodsService.selectById(goods_no);
            if (result == null) {
                log.debug("[INTERIOR-DETAIL] 상품이 검색되지 않습니다. PID["+goods_no+"]");
                return makeResponseEntity(HttpStatus.NO_CONTENT);
            }

            //상품 사진 URL 구성
            result.buildGoodsImageUrl();

            return makeResponseEntity(request, result, HttpStatus.OK);

        } catch (ServiceException e){
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch(Exception e) {
            log.error("상품 상세 조회 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}

package com.zipdoc.acs.controller.shop;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.ShopOrder;
import com.zipdoc.acs.domain.entity.ShopOrderGoods;
import com.zipdoc.acs.domain.entity.ShopOrderHandle;
import com.zipdoc.acs.domain.entity.ShopOrderUserInfo;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopOrderGoodsService;
import com.zipdoc.acs.domain.service.ShopOrderHandleService;
import com.zipdoc.acs.domain.service.ShopOrderService;
import com.zipdoc.acs.domain.service.ShopOrderUserInfoService;

@Controller
@RequestMapping(value = "/shop/order/handle")
public class ShopOrderHandleController extends ResponseEntityService {

	public static final long NON_MEMBER = 0L;
	
	@Autowired
	private AuthenticationHelper authenticationHelper;

	@Autowired
	private ShopOrderService shopOrderService;

	@Autowired
	private ShopOrderGoodsService shopOrderGoodsService;

	@Autowired
	private ShopOrderUserInfoService shopOrderUserInfoService;

	@Autowired
	private ShopOrderHandleService shopOrderHandleService;

	private Logger logger = LoggerFactory.getLogger(ShopOrderHandleController.class);

	@ResponseBody
	@RequestMapping(value = "/add/{sno}/{order_no}", method = RequestMethod.POST)
	public ResponseEntity<String> add(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key, @PathVariable("sno") long sno,
			@PathVariable("order_no") String order_no, @RequestBody ShopOrderHandle shopOrderHandle) {
		try {
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);
			
			if(sno<=0 || order_no == null){
				logger.error("[/shop/order/handle/detail] VALUE ERROR");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}
			
			if(shopOrderHandle.getHandle_mode() <= 0){
				logger.error("[/handle/add] 필수항목이 누락되었습니다." + shopOrderHandle.toString());
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}
			
			if (member_no != null) {
				shopOrderHandle.setCreater(member_no);
			} else {
				shopOrderHandle.setCreater(NON_MEMBER);
			}

			ShopOrderGoods shopOrderGoods = shopOrderGoodsService.findById(sno);
			ShopOrder shopOrder = shopOrderService.findById(shopOrderGoods.getOrder_no());

			if (shopOrder != null) {
				shopOrderHandle.setRefund_price(shopOrder.getTotal_order_price());
			}

			shopOrderHandle.setOrder_goods_sno(sno);
			shopOrderHandle.setOrder_no(order_no);

			shopOrderHandleService.create(shopOrderHandle);

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			logger.error("[/handle/add] 등록 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/detail/{sno}/{order_no}", method = RequestMethod.GET)
	public ResponseEntity<String> detail(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("sno") long sno,
			@PathVariable("order_no") String order_no){
		try{
			if(sno<=0 || order_no == null){
				logger.error("[/shop/order/handle/detail] VALUE ERROR");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}
			
			ShopOrderHandle result = shopOrderHandleService.findById(sno);
			if(result == null){
				logger.debug("[/shop/order/handle/detail] NO DATA. SNO["+sno+"]");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}
			
			return makeResponseEntity(request, result, HttpStatus.OK);
			
		} catch (ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			logger.error("[/handle/detail] 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/update/{sno}/{order_no}", method = RequestMethod.POST)
	public ResponseEntity<String> update(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("sno") long sno,
			@PathVariable("order_no") String order_no,
			@RequestBody ShopOrderHandle shopOrderHandle){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);
				
			if(member_no != null){
				shopOrderHandle.setUpdater(member_no);
			}else{
				shopOrderHandle.setUpdater(NON_MEMBER);
			}
			
			if(sno<=0 || order_no == null){
				logger.error("[/shop/order/handle/detail] VALUE ERROR");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}
			
			shopOrderHandle.setSno(sno);
			
			shopOrderHandleService.update(shopOrderHandle);
			return makeResponseEntity(HttpStatus.OK);
		}catch (ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			logger.error("[/handle/detail] 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

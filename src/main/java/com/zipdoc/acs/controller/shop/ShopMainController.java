package com.zipdoc.acs.controller.shop;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.ShopGoods;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopGoodsDisplayService;
import com.zipdoc.acs.model.shop.ShopMainResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * Created by dskim on 2017. 5. 15..
 */
@Controller
@RequestMapping("/shop")
public class ShopMainController extends ResponseEntityService {


    private Logger log = LoggerFactory.getLogger(ShopMainController.class);

    @Autowired
    private ShopGoodsDisplayService shopGoodsDisplayService;


    @ResponseBody
    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public ResponseEntity<String> main(HttpServletRequest request,
                                       @RequestHeader(value = "APP-VERSION", required = true) String app_version,
                                       @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                       @RequestHeader(value = "ZID", required = true) Long zid,
                                       @RequestHeader(value = "ZID-KEY", required = true) String zid_key) {

        try {

            //베스트 상품
            List<ShopGoods> bestGoodsList = shopGoodsDisplayService.bestGoodsList();

            //신상품
            List<ShopGoods> newGoodsList = shopGoodsDisplayService.newGoodsList();

            //패키지상품
            List<ShopGoods> packageGoodsList = shopGoodsDisplayService.packageGoodsList();

            ShopMainResponse result = new ShopMainResponse();

            result.setBestGoodsList(bestGoodsList);
            result.setNewGoodsList(newGoodsList);
            result.setPackageGoodsList(packageGoodsList);

            return makeResponseEntity(request, result, HttpStatus.OK);

        } catch (ServiceException e) {
            log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
            return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
        } catch (Exception e) {
            log.error("상품 목록 조회 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }




}

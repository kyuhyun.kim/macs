package com.zipdoc.acs.controller.shop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.OrderSummary;
import com.zipdoc.acs.domain.entity.ShopOrderInfo;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopMyPageService;
import com.zipdoc.acs.model.ShopMyPageResponse;

/**
 * 상품 조회 기능
 * @author 문지영
 *
 */
@Controller
@RequestMapping(value = "/shop/mypage")
public class ShopMyPageController extends ResponseEntityService{
	private static final Logger logger = LoggerFactory.getLogger(ShopMyPageController.class);
	
	@Autowired
	private ShopMyPageService shopMyPageService;
	
	@Autowired
	private AuthenticationHelper authenticationHelper;
	
	/*
	 * 나의 쇼핑 메인 페이지
	 */
	@ResponseBody
	@RequestMapping(value ="/summary", method = RequestMethod.GET)
	public ResponseEntity<String> summary(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key){
		try{
			//가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);
			Map<String, Object> condition = new HashMap<>();
			condition.put("member_no", member.getMember_no());
			
			OrderSummary orderSummary = shopMyPageService.selectShopMyPageSummary(member);
			List<ShopOrderInfo> list = shopMyPageService.findRecentList(condition);
			
			ShopMyPageResponse result = new ShopMyPageResponse(orderSummary, list);
			return makeResponseEntity(request, result, HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		}catch(Exception e){
			logger.error("마이쇼핑 목록 조회 실패",e);
			return makeResponseEntity(request, "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

package com.zipdoc.acs.controller.shop;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.ShopZzim;
import com.zipdoc.acs.domain.entity.ShopZzimInfo;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopZzimService;
import com.zipdoc.acs.model.shop.ShopZzimListRequest;
import com.zipdoc.acs.model.shop.ShopZzimListResponse;

@Controller
@RequestMapping(value="/shop/zzim")
public class ShopZzimController extends ResponseEntityService{
	private static final Logger logger = LoggerFactory.getLogger(ShopZzimController.class);
	
	@Autowired
	private ShopZzimService shopZzimService;
	
	@Autowired
	private AuthenticationHelper authenticationHelper;
	
	@ResponseBody
	@RequestMapping(value = "/list/{page}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<String> list(HttpServletRequest request, 
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			ShopZzimListRequest shopZzimListRequest = new ShopZzimListRequest(member_no, page, limit);
			int total_count = shopZzimService.selectShopZzimListTotalCount(shopZzimListRequest);
			if(total_count == 0){
				logger.debug("[/shop/zzim/list] "+page+"page 에는 쇼핑몰 관심상품 정보가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}
			
			List<ShopZzimInfo> list = shopZzimService.selectShopZzimList(shopZzimListRequest);
			
			ShopZzimListResponse result = new ShopZzimListResponse(total_count, list);
			
			return makeResponseEntity(request, result, HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		}catch(Exception e){
			e.printStackTrace();
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ResponseBody
	@RequestMapping(value="/add/{goods_no}", method=RequestMethod.POST)
	public ResponseEntity<String> shopZzimCreate(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("goods_no") int goods_no){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			
			if(goods_no <= 0){
				logger.error("[/shop/zzim/add] 필수항목오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}
			
			ShopZzim shopZzim = new ShopZzim(member_no, goods_no);
			shopZzimService.createZzim(shopZzim);
			return makeResponseEntity(HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ZZIM-ADD] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/delete/{goods_no}", method = RequestMethod.POST)
	public ResponseEntity<String> shopZzimDelete(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("goods_no") int goods_no){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			
			if(goods_no <= 0){
				logger.error("[/shop/zzim/delete] GOODS_NO 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}
			
			ShopZzim shopZzim = new ShopZzim(member_no, goods_no);
			
			shopZzimService.deleteZzim(shopZzim);
			
			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ZZIM-DEL] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/delete-all", method = RequestMethod.POST)
	public ResponseEntity<String> shopZzimDeleteAll(HttpServletRequest requset,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			
			ShopZzim shopZzim = new ShopZzim(member.getMember_no(), -1);
			shopZzimService.deleteZzimAll(shopZzim);
			return makeResponseEntity(HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ZZIM-DEL] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

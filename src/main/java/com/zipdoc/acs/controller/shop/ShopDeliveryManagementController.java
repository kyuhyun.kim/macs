package com.zipdoc.acs.controller.shop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zipdoc.acs.controller.ResponseEntityService;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.ShopDeliveryManagement;
import com.zipdoc.acs.domain.entity.ShopDeliveryManagementRequest;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopDeliveryManagementService;
import com.zipdoc.acs.model.shop.ShopDeliveryListResponse;
import com.zipdoc.acs.utils.Json;

@Controller
@RequestMapping(value="/shop/delivery")
public class ShopDeliveryManagementController extends ResponseEntityService{
	
	private Logger logger = LoggerFactory.getLogger(ShopDeliveryManagementController.class);
	
	@Autowired
	private AuthenticationHelper authenticationHelper;
	
	@Autowired
	private ShopDeliveryManagementService shopDeliveryManagementService;
	
	@ResponseBody
	@RequestMapping(value="/list/{page}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<String> list(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			
			ShopDeliveryManagementRequest shopDeliveryManagementRequest = new ShopDeliveryManagementRequest(member_no, page, limit);
			
			int total_count = shopDeliveryManagementService.selectDeliveryListTotalCount(shopDeliveryManagementRequest);
			if(total_count == 0){
				logger.debug("[/shop/zzim/list] "+page+"page 에는 쇼핑몰 관심상품 정보가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}
			
			List<ShopDeliveryManagement> list = shopDeliveryManagementService.selectDeliveryList(shopDeliveryManagementRequest);
			ShopDeliveryListResponse result = new ShopDeliveryListResponse(total_count, list);
			
			return makeResponseEntity(request, result, HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		}catch(Exception e){
			logger.error("배송지 목록 조회 실패",e);
			return makeResponseEntity(request, "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/detail/{sno}", method = RequestMethod.GET)
	public ResponseEntity<String> detail(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("sno") long sno){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			if(sno <= 0){
				logger.error("[/shop/delivery/detail] sno 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}
			
			Map<String,Object> condition = new HashMap<>();
			condition.put("sno", sno);
			condition.put("member_no", member_no);
			
			ShopDeliveryManagement result = shopDeliveryManagementService.selectById(condition);
			
			if(result == null){
				logger.debug("[/delivery/detail] 데이터 없음. sno["+sno+"]");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}
			
			return makeResponseEntity(request, result, HttpStatus.OK);
		}catch (ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			logger.error("[/delivery/detail] 상세 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ResponseEntity<String> add(HttpServletRequest request,
			  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
			  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			  @RequestHeader(value = "ZID", required = true) Long zid,
			  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			  @RequestBody ShopDeliveryManagement shopDeliveryManagement){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			
			shopDeliveryManagement.setCreater(member.getMember_no());
			
			shopDeliveryManagementService.create(shopDeliveryManagement);
			return makeResponseEntity(HttpStatus.OK);
		}catch(Exception e){
			return makeResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{sno}", method=RequestMethod.POST)
	public ResponseEntity<String> delete(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			  @RequestHeader(value = "ZID", required = true) Long zid,
			  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			  @PathVariable("sno") long sno){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			Map<String,Object> condition = new HashMap<>();
			condition.put("sno", sno);
			condition.put("member_no", member_no);
			
			ShopDeliveryManagement result = shopDeliveryManagementService.selectById(condition);
			
			if(sno <= 0 || result == null){
				logger.error("[/delivery/delete] SNO가 유효하지 않음 sno=" + sno);
				return makeResponseEntity(request, "SNO가 유효하지 않음", HttpStatus.BAD_REQUEST);
			}
			
			if(result.getCreater() != null && member.getMember_no()!=null && member.getMember_no() > 0
					&& result.getCreater().longValue() == member.getMember_no().longValue()){
				shopDeliveryManagementService.delete(condition);
				return makeResponseEntity(HttpStatus.OK);
			}else{
				return makeResponseEntity(request, "글삭제 권한이 없습니다", HttpStatus.FORBIDDEN);
			}
		}catch (ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			logger.error("[/delivery/delete] 배송지 삭제 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/update/{sno}", method=RequestMethod.POST)
	public ResponseEntity<String> update(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			  @RequestHeader(value = "ZID", required = true) Long zid,
			  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			  @PathVariable("sno") long sno,
			  @RequestBody ShopDeliveryManagement shopDeliveryManagement){
		try{
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			
			if(StringUtils.isEmpty(shopDeliveryManagement.getDelivery_areaname()) || StringUtils.isEmpty(shopDeliveryManagement.getDelivery_address())){
				logger.error("[delivery/update] 필수항목누락");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}
			shopDeliveryManagement.setUpdater(member.getMember_no());
			shopDeliveryManagement.setSno(sno);
			shopDeliveryManagementService.update(shopDeliveryManagement);
			return makeResponseEntity(HttpStatus.OK);
		}catch(ServiceException e){
			logger.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			logger.error("[/delivery/update] 배송지 업뎃 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

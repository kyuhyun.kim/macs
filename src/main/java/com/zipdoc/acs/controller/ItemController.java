package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.AgentType;
import com.zipdoc.acs.define.PictureType;
import com.zipdoc.acs.domain.entity.Item;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.ProductService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.ItemListRequest;
import com.zipdoc.acs.model.ItemListResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 시공 사진 조회 기능
 * @author 김종부
 *
 */
@Controller
public class ItemController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(ItemController.class);
	
	@Autowired
	private ProductService productService;

	@Autowired
	private AuthenticationHelper authenticationHelper;
	
	@ResponseBody
	@RequestMapping(value = "/picture/list/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> list(HttpServletRequest request,
									@RequestHeader(value = "APP-VERSION", required = false) String app_version,
									@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									@RequestHeader(value = "ZID", required = false) Long zid,
									@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
									@RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
									@PathVariable("page") int page,
									@PathVariable("limit") int limit,
									@RequestBody(required = false) ItemListRequest searchCondition) {
		try {

			// 단말 구분
			AgentType agent = StringUtils.isEmpty(web_token) ? AgentType.APP : AgentType.WEB;

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				Member member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
				if (member != null) {
					member_no = member.getMember_no();
				}
			}

			if (searchCondition == null) {
				searchCondition = new ItemListRequest();
			}

			// 키워드를 TAG 리스트로 변경한다.
			if (StringUtils.isNotEmpty(searchCondition.getKeyword())) {
				List<String> list = Arrays.asList(StringUtils.split(searchCondition.getKeyword(), " "));
				if (searchCondition.getTags() == null) {
					searchCondition.setTags(list);
				} else {
					if (list != null) {
						for (String tag : list) {
							searchCondition.getTags().add(tag);
						}
					}
				}
				searchCondition.setKeyword(null);
			}

			// 제외할 태그 정보
			searchCondition.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));
			searchCondition.setPageLimit(page, limit);

			// 가입자인 경우 스크랩 여부를 조회한다.
			if (member_no != null) {
				searchCondition.setMember_no(member_no);
			}

			// 개수 조회
			int total_count = productService.selectItemListTotalCount(searchCondition);

			// 조회된 결과가 없을 경우
			if (total_count == 0) {
				log.debug("[PICTURE-LIST] AGENT[" + agent.name() + "] PAGE[" + page + "]에 등록된 시공 사진이 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			List<Item> list = productService.selectItemList(searchCondition);

			for (Item item : list) {
				item.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}

			ItemListResponse result = new ItemListResponse(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		}catch (ServiceException e)
		{
			return makeResponseEntity(request, e.getInvokeResultMsg(), HttpStatus.valueOf(e.getInvokeResult()));
		}
		catch (Exception e) {
			log.error("[PICTURE-LIST] 사진검색에러", e);
		}

		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}

package com.zipdoc.acs.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.CrashLogService;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.CommonService;
import com.zipdoc.acs.model.EmailValidation;
import com.zipdoc.acs.model.ServiceInfo;
import com.zipdoc.acs.utils.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CommonController extends ResponseEntityService {

    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private CrashLogService crashLogService;

    @Autowired
    private MemberService memberService;

    private int page_item_limit = NumberUtils.toInt(Messages.getMessage("PAGE_ITEM_LIMIT", "20"));

    private String temporary_path = Messages.getMessage("UPLOAD_FILE.TEMPORARY_PATH");

    private String fcm_key = Messages.getMessage("FCM.SERVER_KEY");

    /**
     * 서비스 정보 조회 요청
     *
     * @param request
     * @param app_version
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/serviceinfo", method = RequestMethod.GET)
    public ResponseEntity<String> serviceinfo(HttpServletRequest request,
                                              @RequestHeader(value = "APP-VERSION", required = true) String app_version) {

        try {
            if (StringUtils.isEmpty(app_version)) {
                log.error("[SERVICEINFO] APP-VERSION[" + app_version + "] 헤더 정보가 존재하지 않습니다.");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            ServiceInfo result = commonService.selectServiceInfo();
            result.putUrl_info("agreement_info", Constants.AGREEMENT_URL);
            result.putUrl_info("privacy_info", Constants.PRIVACY_URL);
            result.putUrl_info("estimate_privacy_info", Constants.ESTIMATE_PRIVACY_URL);
            result.putUrl_info("estimate_info", Constants.ESTIMATE_LIST_URL);
            result.putUrl_info("third_party_info", Constants.THIRD_PARTY_URL);
            result.putUrl_info("image_server_url", Constants.IMAGE_SERVER_URL);
            result.putUrl_info("marketing_url", Constants.MARKETING_URL);
            result.buildPictureUrl();

            return makeResponseEntity(request, result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("서비스 정보 조회 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 파트너스앱을위한 서비스 정보 조회 요청
     *
     * @param request
     * @param app_version
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/partner/serviceinfo", method = RequestMethod.GET)
    public ResponseEntity<String> partnerServiceinfo(HttpServletRequest request,
                                                     @RequestHeader(value = "APP-VERSION", required = true) String app_version,
                                                     @RequestHeader(value = "ZID", required = false) Long zid) {

        try {
            if (StringUtils.isEmpty(app_version)) {
                log.error("[SERVICEINFO] APP-VERSION[" + app_version + "] 헤더 정보가 존재하지 않습니다.");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            ServiceInfo result = commonService.selectPartnerSerivceInfo();
            result.putUrl_info("agreement_info", Constants.AGREEMENT_URL);
            result.putUrl_info("privacy_info", Constants.PRIVACY_URL);
            result.buildPictureUrl();
            result.setPerson_agree("N");
            result.setPerson_agree_privacy("N");
            result.setAgree_url(Constants.AGREEMENT_GEO_URL);

            // 2. 이전 자동 가입 정보 조회
            if (zid != null) {

                Member member = memberService.selectMemberByZid(zid);

                if (member != null) {
                    result.setPerson_agree(member.getPerson_agree());
                    result.setPerson_agree_privacy(member.getPerson_agree_privacy());
                }

            }

            return makeResponseEntity(request, result, HttpStatus.OK);

        } catch (Exception e) {
            log.error("서비스 정보 조회 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/crash", method = RequestMethod.POST)
    public ResponseEntity<String> crash(HttpServletRequest request,
                                        @RequestHeader(value = "APP-VERSION", required = true) String app_version,
                                        @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                        @RequestHeader(value = "ZID", required = false) Long zid,
                                        @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                        @RequestBody CrashLog crashLog) {

        try {
            if (crashLog != null) {
                crashLog.setApp_version(app_version);
                crashLog.setMember_no(member_no);
                crashLog.setZid(zid);
                crashLogService.createCrashLog(crashLog);
            }
            return makeResponseEntity(HttpStatus.OK);

        } catch (Exception e) {
            log.error("CRASH LOG 등록 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/interiorProduct/detail/{product_code}")
    public String interiorProductDetail(HttpServletRequest request, HttpSession session, Model model,
                                        @PathVariable("product_code") Integer product_code) {
        try {
            if (product_code == null) {
                log.error("[" + product_code + "] 조회 결과가 없습니다.");
                model.addAttribute("failMessage", "요청하신 상품이 존재하지 않습니다.");
                return "common/error";
            }

            InteriorProduct interiorProduct = commonService.selectInteriorProduct(product_code);


            // 조회된 결과가 없을 경우
            if (interiorProduct == null) {
                log.error("[" + product_code + "] 조회 결과가 없습니다.");
                model.addAttribute("failMessage", "요청하신 글이 존재하지 않습니다.");
                return "common/error";
            }

            model.addAttribute("interiorProduct", interiorProduct);

            return "common/detail";

        } catch (Exception e) {
            log.error("[" + product_code + "]. DB - 조회 오류.", e);
            model.addAttribute("failMessage", "요청 처리중 오류가 발생하였습니다.");
            return "common/error";
        }
    }

    @ResponseBody
    @RequestMapping(value = "/pushtest", method = RequestMethod.POST)
    public ResponseEntity<String> pushtest(HttpServletRequest request,
                                           @RequestHeader(value = "APP-VERSION", required = true) String app_version,
                                           @RequestHeader(value = "REGISTRATION-ID", required = true) String registration_id,
                                           @RequestBody EventPromotion message) {

        try {
            if (message != null) {
                /*GCM gcmSender = new GCM(Messages.getMessage("FCM.SERVER_KEY"));
				String gcmMsg = Json.toStringJson(message);
				gcmSender.sendGCM(registration_id, gcmMsg);*/
            }
            return makeResponseEntity(HttpStatus.OK);

        } catch (Exception e) {
            log.error("푸시 테스트 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/cms/serviceinfo", method = RequestMethod.GET)
    public ResponseEntity<String> cmsServiceinfo(HttpServletRequest request,
                                                 @RequestHeader(value = "APP-VERSION", required = false) String app_version) {
        try {

            ServiceInfo result = commonService.selectCmsServiceInfo();

            return makeResponseEntity(request, result, HttpStatus.OK);
        } catch (Exception e) {
            log.error("[/cms/serviceinfo] cms 서비스 정보 조회 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/email_validation", method = RequestMethod.POST)
    public ResponseEntity<String> email_validation(HttpServletRequest request, @RequestBody EmailValidation email) {
        try {

            // 파라미터 체크
            if (StringUtils.isEmpty(email.getMember_id())) return makeResponseEntity(request, "Missing parameters [member_id]", HttpStatus.BAD_REQUEST);

            EmailValidation validation = new EmailValidation();

            // 이미 사용중인 ID인 경우
            if (memberService.IsExistAccount(email.getMember_id())) {
                log.debug("[EMAIL-VALIDATION] ID["+email.getMember_id()+"]는 이미 사용중인 ID 입니다.");
                validation.setEmailExist(true);
            } else {
                log.debug("[EMAIL-VALIDATION] ID["+email.getMember_id()+"]는 사용 가능합니다.");
                validation.setEmailExist(false);
            }

            return makeResponseEntity(request, validation, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("[EMAIL-VALIDATION] Error", e);
        }
        return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

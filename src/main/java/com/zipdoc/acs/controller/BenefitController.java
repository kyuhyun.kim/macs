package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.AgentType;
import com.zipdoc.acs.define.CategoryCode;
import com.zipdoc.acs.define.PictureType;
import com.zipdoc.acs.define.ProductType;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.*;
import com.zipdoc.acs.model.HomeScreenLayout;
import com.zipdoc.acs.model.ItemListRequest;
import com.zipdoc.acs.model.ProductListRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 집닥 혜택 서비스
 * @author 김종부
 *
 */
@Controller
@RequestMapping(value = "/benefit")
public class BenefitController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(BenefitController.class);

	@Autowired
	private BenefitService benefitService;

	/**
	 * 혜택 신청
	 */
	@ResponseBody
	@RequestMapping(value = "/request", method = RequestMethod.POST)
	public ResponseEntity<String> residential(HttpServletRequest request,
											  @RequestBody BenefitRequest benefitRequest) {

		try {

			// 필수 항목 체크
			if (!benefitRequest.isVallid()) {
				log.error("[BENEFIT-REQUEST] 요청 오류. 필수 항목 누락");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// DB 등록
			benefitService.insertBenefit(benefitRequest);

			log.debug("[BENEFIT-REQUEST] 신청완료. ID["+benefitRequest.getBenefit_id()+"] 신청자["+benefitRequest.getReq_name()+"/"+benefitRequest.getReq_phone_no()+"]");

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[BENEFIT-REQUEST] 내부 처리 오류. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[BENEFIT-REQUEST] 내부 오류.", e);
			return makeResponseEntity(request, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}

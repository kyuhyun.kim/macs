package com.zipdoc.acs.controller;

import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.Whatever;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.WhateverService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.ListResponse;
import com.zipdoc.acs.model.WhateverListRequest;
import com.zipdoc.acs.utils.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.regex.Pattern;


/**
 * 궁금해요 컨트롤러
 * @author 이동규
 *
 */
@Controller
@RequestMapping(value = "/whatever")
public class WhateverController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(WhateverController.class);
	
	//private static final Pattern urlPattern = Pattern.compile("<img[^>]*src=[\"']?([^>\"']+)[\"']?[^>]*>");
	
	@Autowired
	private WhateverService whateverService;

	@Autowired
	private AuthenticationHelper authenticationHelper;
	
	@ResponseBody
	@RequestMapping(value = "/list/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> list(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit,
			@RequestBody(required = false) WhateverListRequest searchCondition) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			if(searchCondition == null){
				searchCondition = new WhateverListRequest();
			}

			searchCondition.setPageLimit(page, limit);
			searchCondition.setMember_no(member_no);

			log.info(searchCondition.toString());

			int total_count = whateverService.selectListTotalCount(searchCondition);
			// 조회된 결과가 없을 경우
			if(total_count==0) {
				log.debug("[/whatever/list] "+page+"page 에는 데이터가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			List<Whatever> list = whateverService.selectList(searchCondition);

			//이미지 상대경로를 URL로 변경
			for(Whatever whatever:list){
				whatever.buildForSendMessage();
			}

			ListResponse result = new ListResponse(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/whatever/list] 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@ResponseBody
	@RequestMapping(value = "/detail/{cno}", method = RequestMethod.GET)
	public ResponseEntity<String> detail(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("cno") long cno) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			if(cno <= 0) {
				log.error("[/whatever/detail] cno 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 궁금해요 정보 가져오기
			Whatever result = whateverService.selectById(cno);
			if (result == null) {
				log.debug("[/whatever/detail] 데이터 없음. cno["+cno+"]");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			whateverService.updateIncreaseViewCnt(cno);

			//이미지 상대경로를 URL로 변경
			result.buildForSendMessage();

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/whatever/detail] 상세 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> add(HttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
										 @RequestHeader(value = "ZID", required = true) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
										 @RequestBody Whatever whatever) {
		return treatAdd(request, app_version, member_no, zid, zid_key,	whatever, null);
	}

	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> add(HttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									  @RequestParam(value = "meta-data", required = true) String metaData,
									  @RequestPart(value = "attached_file", required = false) List<MultipartFile> images) {
		try {
			Whatever whatever = Json.toObjectJson(metaData, Whatever.class);
			return treatAdd(request, app_version, member_no, zid, zid_key,	whatever, images);
		} catch (Exception e){
			return makeResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}

	private ResponseEntity<String> treatAdd(HttpServletRequest request,
											String app_version,
											Long member_no,
											Long zid,
											String zid_key,
											Whatever whatever,
											List<MultipartFile> images){

		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			if(StringUtils.isEmpty(whatever.getSubject()) || StringUtils.isEmpty(whatever.getCategory()) || StringUtils.isEmpty(whatever.getContents())){
				log.error("[/whatever/add] 필수항목이 누락되었습니다." + whatever.toString());
				return makeResponseEntity(request, "필수항목이 누락되었습니다.", HttpStatus.BAD_REQUEST);
			}


			// 파일 업로드 처리
			if(images != null && images.size() > 0) {
				String imageTag = FileUploadUtil.uploadSmartEditorFile(images);
				whatever.setContents(whatever.getContents() + imageTag);
			}

			whatever.setIp(IpUtil.getClientIP(request));
			whatever.setCreater(member.getMember_no());
			whatever.buildSummary();

			whateverService.create(whatever);
			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/whatever/add] 등록 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/delete/{cno}", method = RequestMethod.POST)
	public ResponseEntity<String> delete(HttpServletRequest request,
									  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
									  @RequestHeader(value = "ZID", required = true) Long zid,
									  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
									  @PathVariable("cno") long cno) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			Whatever result = whateverService.selectById(cno);
			if(cno <= 0 || result==null){
				log.error("[/whatever/delete] CNO가 유효하지 않음 cno=" + cno);
				return makeResponseEntity(request, "CNO가 유효하지 않음", HttpStatus.BAD_REQUEST);
			}

			//글등록자인 경우에만 삭제가능
			if(result.getCreater()!=null && result.getCreater()>0
					&& member.getMember_no()!=null && member.getMember_no() > 0
					&& result.getCreater().longValue() == member.getMember_no().longValue()){
				whateverService.delete(cno);
				log.info("[/zipdocReply/delete] 삭제 성공");
				return makeResponseEntity(HttpStatus.OK);
			} else {
				log.error("[/whatever/delete] 글삭제 권한이 없습니다. cno=" + cno + ", creater=" + result.getCreater() + ", member_no=" + member.getMember_no());
				return makeResponseEntity(request, "글삭제 권한이 없습니다", HttpStatus.FORBIDDEN);
			}
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/whatever/delete] 삭제 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/delete-multi", method = RequestMethod.POST)
	public ResponseEntity<String> deleteMulti(HttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
										 @RequestHeader(value = "ZID", required = true) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
										 @RequestBody Whatever target) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			target.setUpdater(member.getMember_no());
			whateverService.deleteMulti(target);
			log.info("[/whatever/delete-multi] 삭제 성공");
			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/whatever/delete-multi] 삭제 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/delete-all", method = RequestMethod.POST)
	public ResponseEntity<String> deleteMulti(HttpServletRequest request,
											  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
											  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
											  @RequestHeader(value = "ZID", required = true) Long zid,
											  @RequestHeader(value = "ZID-KEY", required = true) String zid_key) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);
			whateverService.deleteAllByMemberNo(member.getMember_no());
			log.info("[/whatever/delete-all] 삭제 성공");
			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/whatever/delete-all] 삭제 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

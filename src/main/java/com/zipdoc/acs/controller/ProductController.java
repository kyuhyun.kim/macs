package com.zipdoc.acs.controller;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.zipdoc.acs.define.AgentType;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.PictureType;
import com.zipdoc.acs.define.ProductType;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.service.*;
import com.zipdoc.acs.model.ProductListRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.zipdoc.acs.domain.entity.Product;
import com.zipdoc.acs.model.ProductListResponse;

/**
 * 시공사례 조회 기능
 * @author 김종부
 *
 */
@Controller
public class ProductController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(ProductController.class);
	
	private static final Pattern urlPattern = Pattern.compile("<img[^>]*src=[\"']?([^>\"']+)[\"']?[^>]*>");
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private ProductService productService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@Autowired
	private KeywordSynonymService keywordSynonymService;

	@ResponseBody
	@RequestMapping(value = "/interior/list/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> list(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = false) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = false) Long zid,
			@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit,
			@RequestBody(required = false) ProductListRequest searchCondition) {

		try {

			AgentType agent = AgentType.WEB;

			// 앱 요청인 경우 가입 여부 조회
			if (zid != null && StringUtils.isNotEmpty(zid_key)) {
				authenticationHelper.findMember(member_no, zid, zid_key);
				agent = AgentType.APP;
			}

			if (searchCondition == null){
				searchCondition = new ProductListRequest();
			}

			searchCondition.setPageLimit(page, limit);
			searchCondition.setMember_no(member_no);
			searchCondition.setProduct_type(ProductType.GALLERY.getCode());

			if( searchCondition.getStyles() != null && searchCondition.getStyles().size() > 0) {
				List<String> styles = Arrays.asList(StringUtils.split(keywordSynonymService.selectKeywordSynonym(searchCondition.getStyles().get(0)), "\\|"));
				searchCondition.setStyles(new ArrayList<String>(new TreeSet<String>(styles)));
			}

			int total_count = productService.selectListTotalCount(searchCondition);

			// 조회된 결과가 없을 경우
			if (total_count == 0) {
				log.debug("[INTERIOR-LIST] AGENT["+agent.name()+"] PAGE["+page+"]에 등록된 시공사례가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			List<Product> list = productService.selectList(searchCondition);

			for (Product product:list) {
				product.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}

			ProductListResponse result = new ProductListResponse(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[INTERIOR-LIST] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[INTERIOR-LIST] 시공사례 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/interior/detail/{pid}", method = RequestMethod.GET)
	public ResponseEntity<String> detail(HttpServletRequest request,
										@RequestHeader(value = "APP-VERSION", required = false) String app_version,
										@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										@RequestHeader(value = "ZID", required = false) Long zid,
										@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										@RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										@PathVariable("pid") long pid) {

		try {

			// 파라미터 체크
			if (pid <= 0) {
				log.error("[INTERIOR-DETAIL] PID 오류");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 단말 구분
			AgentType agent = zid != null ? AgentType.APP : AgentType.WEB;

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				Member member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
				if (member != null) {
					member_no = member.getMember_no();
				}
			}

			// 조회수 증가
			productService.updateIncreaseHitCnt(pid);

			// 갤러리 정보 가져오기
			Product result = productService.selectById(pid, member_no);
			if (result == null) {
				log.debug("[INTERIOR-DETAIL] 갤러리가 검색되지 않습니다. PID["+pid+"]");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			// 공유하기 URL 설정
			result.buildShareUrl();

			// 사진 URL 구성
			result.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);

			if (agent == AgentType.WEB) {
				Map<String, Object> condition = new HashMap<>();
				condition.put("category_code1", result.getCategory_code1());
				condition.put("own_pid", result.getPid());
				condition.put("price", result.getPrice());

				List<Product> same_price_list = productService.selectSamePriceProductList(condition);

				if (same_price_list != null) {
					for (Product product : same_price_list) {
						if (!StringUtils.isEmpty(product.getFile_path()) && !StringUtils.isEmpty(product.getThumbnail())) {
							product.setThumbnail_url(Constants.STATIC_URL + product.getFile_path() + PictureType.XHDPI.getPrefix()  + product.getFile_name());
						}
					}
				}

				// 웹 요청인 경우 비슷한 비용의 인기 인테리어 3개 조회
				result.setSame_price_list(same_price_list);
			}

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[INTERIOR-DETAIL] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[INTERIOR-DETAIL] 갤러리 상세 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/interior/view/{pid}", method = RequestMethod.GET)
	public String view(HttpServletRequest request, HttpSession session, Model model,
					   @PathVariable("pid") long pid) {
		try {
			if(pid <= 0) {
				log.error("[INTERIOR-DETAIL] PID 오류");
				model.addAttribute("failMessage", "요청하신 정보가 유효하지 않습니다.");
				return "common/error";
			}

			// 시공사례 정보 가져오기
			Product result = productService.selectById(pid, null);
			if (result == null) {
				log.debug("[INTERIOR-DETAIL] 갤러리가 검색되지 않습니다. PID["+pid+"]");
				model.addAttribute("failMessage", "요청하신 정보가 유효하지 않습니다.");
				return "common/error";
			}

			//사진 URL 구성
			result.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			model.addAttribute("interior", result);

			return "interior/detail";
		} catch (Exception e) {
			e.printStackTrace();
			return "common/error";
		}
	}


	private List<String> parseURLforHTML(String html) {
		
		List<String> result = new ArrayList<String>();
		StringBuffer out = new StringBuffer();
		
		Matcher matcher = urlPattern.matcher(html);
		
		while (matcher.find()) {
			String temp = matcher.group(1);
			temp = temp.replaceFirst("zipdoc.kr", "zipdoc.co.kr");
			
			// 원본 파일을 통해 썸네일 파일 여부 확인 후 썸네일 파일이 있을 경우 해당 파일을 사용한다.
			temp = getThumbnail(temp);
			
			// 목록 추가
			result.add(temp);
		}
		
		matcher.appendTail(out);
		return result;
	}
	
	private String getThumbnail(String origin) {
		
		try {

			File originFile = new File(origin.replaceFirst("http://zipdoc.co.kr", "/home/zipdockr/www"));
			if (originFile.exists()) {

				// 썸네일 규칙 설정
				String thumbnail = "thumb-" + originFile.getName().substring(0, originFile.getName().lastIndexOf('.'));

				// 썸네일 파일 검색
				File parent = originFile.getParentFile();
				if (parent.exists()) {
					File [] files = parent.listFiles();
					for (File file : files) {
						if (file.getName().startsWith(thumbnail)) {
							origin = file.getAbsolutePath().replaceFirst("/home/zipdockr/www", "http://zipdoc.co.kr");
							return origin;
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("파일 검색 오류.", e);
		}
		return origin;
	}
}

package com.zipdoc.acs.controller;


import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * 사용자 현재 위치 검사 테스트
 * @author 임혜현
 */
@Controller
@RequestMapping(value = "/")
public class GeoLocationController {

    private static final Logger logger = LoggerFactory.getLogger(GeoLocationController.class);

    /**
     * 현재 위치 체크 (Naver API호출)
     * @param request HttpRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/checkLocation", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
    public ModelAndView checkLocation(HttpServletRequest request) throws Exception
    {
        logger.info("user-agent : " + request.getHeader("user-agent"));
        logger.info("IP : " + getUserIp(request));

        ModelAndView view = new ModelAndView();
        view.setViewName("/checkLocation");
        return view;
    }

    /**
     * IP주소로 위치 검사
     * @param request
     * @return
     */
    @RequestMapping(value = "/checkLocationByIP", method = RequestMethod.POST)
    public @ResponseBody String checkLocationByIP(HttpServletRequest request)
    {
        return getLocationNaverGeoLocation(request).toJSONString();
    }

    /**
     * 위경도 좌표로 위치 검사
     * @param longitude 경도
     * @param latitude 위도
     * @return
     */
    @RequestMapping(value = "/checkLocationByCoords", method = RequestMethod.POST)
    public @ResponseBody String checkLocationByCoords(@RequestParam("longitude") String longitude, @RequestParam("latitude") String latitude)
    {
        logger.info("longitude : " + longitude + ", latitude : " + latitude);
        return getLocationNaverOpenAPI(longitude, latitude).toJSONString();
    }

    /**
     * 좌표->주소 변환 NaverOpenAPI 호출
     * @param longitude 경도
     * @param latitude 위도
     * @return
     */
    private JSONObject getLocationNaverOpenAPI(String longitude, String latitude)
    {
        String url = "https://openapi.naver.com/v1/map/reversegeocode?encoding=utf8&query="+longitude+ ","+ latitude;

        JSONObject locationData = new JSONObject();
        try {
            StringBuilder sendUrl = new StringBuilder("");
            sendUrl.append(url);

            HttpHeaders headers = new HttpHeaders();
            headers.add("X-Naver-Client-Id", "X74ZqmsdpXqIF3Wv_Oda");
            headers.add("X-Naver-Client-Secret", "IqoHFE8KO5");

            HttpEntity entity = new HttpEntity(headers);
            RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
            ResponseEntity<String> response = restTemplate.exchange(sendUrl.toString(), HttpMethod.GET, entity, String.class);

            ObjectMapper mapper = new ObjectMapper();
            JSONObject geolocation = new JSONObject();

            JsonNode items = mapper.readTree(response.getBody()).get("result").get("items");
            JsonNode item = items.isArray() ? items.get(0) : items;

            geolocation.put("sido", item.get("addrdetail").get("sido"));
            geolocation.put("sigugun", item.get("addrdetail").get("sigugun"));
            geolocation.put("dongmyun", item.get("addrdetail").get("dongmyun"));

            locationData.put("geoLocation", geolocation);
            locationData.put("isSuccess", "true");

        } catch (Exception exception) {
            locationData.put("geoLocation", "unknown");
            locationData.put("isSuccess", "false");
        }
        return locationData;
    }

    /**
     * IP -> 주소 변환 Naver Cloud API 호출
     * @param request
     * @return
     */
    private JSONObject getLocationNaverGeoLocation(HttpServletRequest request)
    {
        String currentTime = Long.toString(System.currentTimeMillis());
        String apiKey = "jCsA6PyrgjKA6c2GeufO2XjHq6bB5tR90UcP7DZf";
        String accessKey = "3hgjz96IbhVGTjJeRwak";

        String domain = "https://ncloud.apigw.ntruss.com";
        String uri = "/geolocation/v1/geoLocation?enc=utf8&ext=t&responseFormatType=json&ip=" + getUserIp(request);

        JSONObject locationData = new JSONObject();
        try {
            StringBuilder sendUrl = new StringBuilder("");
            sendUrl.append(domain + uri);

            HttpHeaders headers = new HttpHeaders();
            headers.add("x-ncp-apigw-timestamp", currentTime);
            headers.add("x-ncp-apigw-api-key", apiKey);
            headers.add("x-ncp-iam-access-key", accessKey);
            headers.add("x-ncp-apigw-signature-v1", makeSignature(uri, currentTime, apiKey, accessKey));

            HttpEntity entity = new HttpEntity(headers);
            RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

            ResponseEntity<String> response = restTemplate.exchange(sendUrl.toString(), HttpMethod.GET, entity, String.class);

            ObjectMapper mapper = new ObjectMapper();
            JSONObject geolocation = new JSONObject();

            JsonNode item = mapper.readTree(response.getBody()).get("geoLocation");

            geolocation.put("sido", item.get("r1"));
            geolocation.put("sigugun", item.get("r2"));
            geolocation.put("dongmyun", item.get("r3"));

            locationData.put("geoLocation", geolocation);
            locationData.put("isSuccess", "true");

        } catch (Exception exception) {
            locationData.put("geoLocation", "unknown");
            locationData.put("isSuccess", "false");
        }
        return locationData;
    }

    /**
     * Naver Signature 키 생성
     * @param uri 호출 API uri값(query포함)
     * @param currentTime 현재시각 timestamp
     * @param apiKey 사용자 고유 api key
     * @param accessKey 사용자 고유 accessKey
     * @return Signature 키 값
     */
    private String makeSignature(String uri, String currentTime, String apiKey, String accessKey) {
        String space = " ";                    // one space
        String newLine = "\n";                    // new line
        String method = "GET";                    // method
        String secretKey = "FWWxxAUtKuZGz5aiScd3xQ4Jr4Ijmlh1cTJ2YvEs";

        String message = new StringBuilder()
                .append(method)
                .append(space)
                .append(uri)
                .append(newLine)
                .append(currentTime)
                .append(newLine)
                .append(apiKey)
                .append(newLine)
                .append(accessKey)
                .toString();

        try {
            SecretKeySpec signingKey = null;
            signingKey = new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(message.getBytes("UTF-8"));
            String encodeBase64String = Base64.encodeBase64String(rawHmac);

            return encodeBase64String;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return "false";
    }

    private String getUserIp(HttpServletRequest request)
    {
        String ip = request.getHeader("X-Forwarded-For");

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP"); // 웹로직
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            String ipAddrs = request.getHeader("HTTP_X_FORWARDED_FOR");
            ip = ipAddrs != null ? ipAddrs.split(",")[0].trim() : ip;
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }
}

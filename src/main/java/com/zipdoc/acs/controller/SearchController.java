package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.*;
import com.zipdoc.acs.model.*;
import com.zipdoc.acs.persistence.dao.CustomerOpinionsDao;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


/**
 * 통합 검색 기능
 *
 */
@Controller
public class SearchController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(SearchController.class);

	@Autowired
	private CommonService commonService;

	@Autowired
	private ProductService productService;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private BoardService magazineService;

	@Autowired
	private WhateverService whateverService;

	@Autowired
	private ZipdocReplyService zipdocReplyService;

	@Autowired
	private ArticleService articleService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@Autowired
	private CustomerOpinionsService customerOpinionsService;

	@Autowired
	private KeywordSynonymService keywordSynonymService;

	/**
	 * 통합검색 (Revision)
	 * @param request
	 * @param listRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/search/complex", method = RequestMethod.POST)
	public ResponseEntity<String> complex(HttpServletRequest request,
										  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID", required = false) Long zid,
										  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										  @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										  @RequestBody SearchListRequest listRequest) {
		try {

			log.debug("[SEARCH-COMPLEX] 통합검색. 검색키워드["+listRequest.getKeyword()+"]");

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				Member member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
				if (member != null) {
					member_no = member.getMember_no();
				}
			}

			 /*Before & After 를 처리 하자*/
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				listRequest.setKeyword("Before&After");

			log.debug("[SEARCH-COMPLEX] 통합검색. 검색키워드["+listRequest.getKeyword()+"]");


			List<String> keywords = Arrays.asList(StringUtils.split(listRequest.getKeyword(), " "));

			Map<String, Object> objectMap = new HashMap<>();

			// 동의어 검색을 위해 입력된 키워드를 리스트로 변경
			for(int nIdx =0; nIdx < keywords.size(); nIdx++)
				objectMap.put( "keywords" + nIdx, new ArrayList<String>(new TreeSet<String>(Arrays.asList( StringUtils.split(keywordSynonymService.selectKeywordSynonym( keywords.get(nIdx) ), "\\|")))));


			Map<String, Object> resultMap = new HashMap<String, Object>();

			// 주거 - 시공사례 (9개)
			ProductListRequest searchGallery = new ProductListRequest();
			searchGallery.setPageLimit(0, listRequest.getResident_gallery_limit());
			searchGallery.setCategory_code1(CategoryCode.RESIDENT.getCode());
			searchGallery.setKeywords(keywords);
			searchGallery.setKeywordsMap(objectMap);

			 /*비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*/
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				searchGallery.setBnA("1");

			resultMap.put("resident_gallery", searchGalleryListRevision(searchGallery));

			// 주거 - 사진 (16개)
			ItemListRequest searchPicture = new ItemListRequest();
			searchPicture.setPageLimit(0, listRequest.getResident_picture_limit());
			searchPicture.setCategory_code1(CategoryCode.RESIDENT.getCode());
			searchPicture.setKeywords(keywords);
			searchPicture.setKeywordsMap(objectMap);
			searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));


			 /*비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*/
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
				searchPicture.setBnA("1");
				searchPicture.setExclude_tags(null);
			}

			// 가입자인 경우 스크랩 여부를 조회한다.
			if (member_no != null) {
				searchPicture.setMember_no(member_no);
			}

			resultMap.put("resident_picture", searchGalleryPictureListRevision(searchPicture));

			// 상업 - 시공사례 (15개)
			searchGallery = new ProductListRequest();
			searchGallery.setPageLimit(0, listRequest.getCommerce_gallery_limit());
			searchGallery.setCategory_code1(CategoryCode.COMMERCE.getCode());
			searchGallery.setKeywordsMap(objectMap);
			searchGallery.setKeywords(keywords);

			 /*비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*/
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				searchGallery.setBnA("1");

			resultMap.put("commerce_gallery", searchGalleryListRevision(searchGallery));

			// 상업 - 사진 (16개)
			searchPicture = new ItemListRequest();
			searchPicture.setPageLimit(0, listRequest.getResident_picture_limit());
			searchPicture.setCategory_code1(CategoryCode.COMMERCE.getCode());
			searchPicture.setKeywords(keywords);
			searchPicture.setKeywordsMap(objectMap);
			searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));

			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
				searchPicture.setBnA("1");
				searchPicture.setExclude_tags(null);
			}
			// 가입자인 경우 스크랩 여부를 조회한다.
			if (member_no != null) {
				searchPicture.setMember_no(member_no);
			}

			resultMap.put("commerce_picture", searchGalleryPictureListRevision(searchPicture));

			// 매거진 (9개)
			MagazineListRequest searchMagazine = new MagazineListRequest();
			searchMagazine.setPageLimit(0,listRequest.getMagazine_limit());
			searchMagazine.setKeywords(keywords);
			searchMagazine.setKeywordsMap(objectMap);

			resultMap.put("magazine", searchMagazineListRevision(searchMagazine));

			// 파트너스 (15개)
			PartnerListRequest searchPartners = new PartnerListRequest();
			searchPartners.setPageLimit(0, listRequest.getPartners_limit());
			searchPartners.setBest_flag(1);
			searchPartners.setThumbnail_cnt(2);
			searchPartners.setKeywords(keywords);
			searchPartners.setKeywordsMap(objectMap);

			resultMap.put("partners", searchPartnerList(searchPartners));

			return makeResponseEntity(request, resultMap, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error("[SEARCH-COMPLEX] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[SEARCH-COMPLEX] 통합검색 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * 통합검색 (개선)
	 * @param request
	 * @param listRequest
	 * @return
	 *//*
	@ResponseBody
	@RequestMapping(value = "/search/complex1", method = RequestMethod.POST)
	public ResponseEntity<String> complex(HttpServletRequest request,
										  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID", required = false) Long zid,
										  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										  @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										  @RequestBody SearchListRequest listRequest) {
		try {

			log.debug("[SEARCH-COMPLEX] 통합검색. 검색키워드["+listRequest.getKeyword()+"]");

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				Member member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
				if (member != null) {
					member_no = member.getMember_no();
				}
			}


			*//* Before & After 를 처리 하자 *//*
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				listRequest.setKeyword("Before&After");

			log.debug("[SEARCH-COMPLEX] 통합검색. 검색키워드["+listRequest.getKeyword()+"]");
			// 다중검색을 위해 입력된 키워드를 리스트로 변경
			List<String> keywords = Arrays.asList(StringUtils.split(listRequest.getKeyword(), " "));

			Map<String, Object> resultMap = new HashMap<String, Object>();

			*//* 검색 패턴 형식으로 문자열 치환 *//*

			String searchKeyword = "";
			for(int nIdx = 0; nIdx < keywords.size(); nIdx++)
			{
				*//*keywords.set (nIdx,  String.format( "(.*%s.*)" ,  keywordSynonymService.selectKeywordSynonym( keywords.get(nIdx)) ) );*//*
				keywords.set (nIdx,    keywordSynonymService.selectKeywordSynonym( keywords.get(nIdx))  );

				*//*searchKeyword +=  String.format( "(.*%s.*)" ,  keywords.get(nIdx) ) ;*//*
			}



			// 주거 - 시공사례 (9개)
			ProductListRequest searchGallery = new ProductListRequest();
			searchGallery.setPageLimit(0, listRequest.getResident_gallery_limit());
			searchGallery.setCategory_code1(CategoryCode.RESIDENT.getCode());
			searchGallery.setKeywords(keywords);
			searchGallery.setSearchKeyword(searchKeyword);

			*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				searchGallery.setBnA("1");

			resultMap.put("resident_gallery", searchGalleryListRevision(searchGallery));

			// 주거 - 사진 (16개)
			ItemListRequest searchPicture = new ItemListRequest();
			searchPicture.setPageLimit(0, listRequest.getResident_picture_limit());
			searchPicture.setCategory_code1(CategoryCode.RESIDENT.getCode());
			searchPicture.setKeywords(keywords);
			searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));
			searchPicture.setSearchKeyword(searchKeyword);

			*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
				searchPicture.setBnA("1");
				searchPicture.setExclude_tags(null);
			}

			// 가입자인 경우 스크랩 여부를 조회한다.
			if (member_no != null) {
				searchPicture.setMember_no(member_no);
			}

			resultMap.put("resident_picture", searchGalleryPictureListRevision(searchPicture));

			// 상업 - 시공사례 (15개)
			searchGallery = new ProductListRequest();
			searchGallery.setPageLimit(0, listRequest.getCommerce_gallery_limit());
			searchGallery.setCategory_code1(CategoryCode.COMMERCE.getCode());
			searchGallery.setKeywords(keywords);
			searchGallery.setSearchKeyword(searchKeyword);

			*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				searchGallery.setBnA("1");

			resultMap.put("commerce_gallery", searchGalleryListRevision(searchGallery));

			// 상업 - 사진 (16개)
			searchPicture = new ItemListRequest();
			searchPicture.setPageLimit(0, listRequest.getResident_picture_limit());
			searchPicture.setCategory_code1(CategoryCode.COMMERCE.getCode());
			searchPicture.setKeywords(keywords);
			searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));
			searchPicture.setSearchKeyword(searchKeyword);

			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
				searchPicture.setBnA("1");
				searchPicture.setExclude_tags(null);
			}
			// 가입자인 경우 스크랩 여부를 조회한다.
			if (member_no != null) {
				searchPicture.setMember_no(member_no);
			}

			resultMap.put("commerce_picture", searchGalleryPictureListRevision(searchPicture));


			// 파트너스 (15개)
			PartnerListRequest searchPartners = new PartnerListRequest();
			searchPartners.setPageLimit(0, listRequest.getPartners_limit());
			searchPartners.setBest_flag(1);
			searchPartners.setThumbnail_cnt(2);
			searchPartners.setKeywords(Arrays.asList(StringUtils.split(listRequest.getKeyword(), " ")));
			resultMap.put("partners", searchPartnerList(searchPartners));


			// 매거진 (9개)
			MagazineListRequest searchMagazine = new MagazineListRequest();
			searchMagazine.setPageLimit(0,listRequest.getMagazine_limit());
			searchMagazine.setKeywords(keywords);
			searchMagazine.setSearchKeyword(searchKeyword);

			resultMap.put("magazine", searchMagazineListRevision(searchMagazine));

			return makeResponseEntity(request, resultMap, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error("[SEARCH-COMPLEX] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[SEARCH-COMPLEX] 통합검색 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}*/

	/**
	 * 통합검색
	 * @param request
	 * @param listRequest
	 * @return
	 */
	/*@ResponseBody
	@RequestMapping(value = "/search/complex", method = RequestMethod.POST)
	public ResponseEntity<String> complex(HttpServletRequest request,
										  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID", required = false) Long zid,
										  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										  @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										  @RequestBody SearchListRequest listRequest) {
		try {

			log.debug("[SEARCH-COMPLEX] 통합검색. 검색키워드["+listRequest.getKeyword()+"]");

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				Member member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
				if (member != null) {
					member_no = member.getMember_no();
				}
			}


			*//* Before & After 를 처리 하자 *//*
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				listRequest.setKeyword("Before&After");

			log.debug("[SEARCH-COMPLEX] 통합검색. 검색키워드["+listRequest.getKeyword()+"]");
			// 다중검색을 위해 입력된 키워드를 리스트로 변경
			List<String> keywords = Arrays.asList(StringUtils.split(listRequest.getKeyword(), " "));


			Map<String, Object> resultMap = new HashMap<String, Object>();

			// 주거 - 시공사례 (9개)
			ProductListRequest searchGallery = new ProductListRequest();
			searchGallery.setPageLimit(0, listRequest.getResident_gallery_limit());
			searchGallery.setCategory_code1(CategoryCode.RESIDENT.getCode());
			searchGallery.setKeywords(keywords);

			*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				searchGallery.setBnA("1");

			resultMap.put("resident_gallery", searchGalleryList(searchGallery));

			// 주거 - 사진 (16개)
			ItemListRequest searchPicture = new ItemListRequest();
			searchPicture.setPageLimit(0, listRequest.getResident_picture_limit());
			searchPicture.setCategory_code1(CategoryCode.RESIDENT.getCode());
			searchPicture.setKeywords(keywords);
			searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));

			*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
				searchPicture.setBnA("1");
				searchPicture.setExclude_tags(null);
			}

			// 가입자인 경우 스크랩 여부를 조회한다.
			if (member_no != null) {
				searchPicture.setMember_no(member_no);
			}

			resultMap.put("resident_picture", searchGalleryPictureList(searchPicture));

			// 상업 - 시공사례 (15개)
			searchGallery = new ProductListRequest();
			searchGallery.setPageLimit(0, listRequest.getCommerce_gallery_limit());
			searchGallery.setCategory_code1(CategoryCode.COMMERCE.getCode());
			searchGallery.setKeywords(keywords);
			*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				searchGallery.setBnA("1");

			resultMap.put("commerce_gallery", searchGalleryList(searchGallery));

			// 상업 - 사진 (16개)
			searchPicture = new ItemListRequest();
			searchPicture.setPageLimit(0, listRequest.getResident_picture_limit());
			searchPicture.setCategory_code1(CategoryCode.COMMERCE.getCode());
			searchPicture.setKeywords(keywords);
			searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));

			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
				searchPicture.setBnA("1");
				searchPicture.setExclude_tags(null);
			}
			// 가입자인 경우 스크랩 여부를 조회한다.
			if (member_no != null) {
				searchPicture.setMember_no(member_no);
			}

			resultMap.put("commerce_picture", searchGalleryPictureList(searchPicture));

			// 파트너스 (15개)
			PartnerListRequest searchPartners = new PartnerListRequest();
			searchPartners.setPageLimit(0, listRequest.getPartners_limit());
			searchPartners.setBest_flag(1);
			searchPartners.setThumbnail_cnt(2);
			searchPartners.setKeywords(keywords);

			resultMap.put("partners", searchPartnerList(searchPartners));

			// 매거진 (9개)
			MagazineListRequest searchMagazine = new MagazineListRequest();
			searchMagazine.setPageLimit(0,listRequest.getMagazine_limit());
			searchMagazine.setKeywords(keywords);

			resultMap.put("magazine", searchMagazineList(searchMagazine));

			return makeResponseEntity(request, resultMap, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error("[SEARCH-COMPLEX] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[SEARCH-COMPLEX] 통합검색 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
*/

	/**
	 * 통합검색
	 * @param request
	 * @param listRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/search/complex/{type}/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> complex(HttpServletRequest request,
										  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID", required = false) Long zid,
										  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										  @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										  @PathVariable("type") int type,
										  @PathVariable("page") int page,
										  @PathVariable("limit") int limit,
										  @RequestBody SearchListRequest listRequest) {


		try {

			log.debug("[SEARCH-COMPLEX-MORE] 통합검색-더보기. TYPE["+type+"] 검색키워드["+listRequest.getKeyword()+"]");

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				Member member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
				if (member != null) {
					member_no = member.getMember_no();
				}
			}

			 /*Before & After 를 처리 하자*/
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				listRequest.setKeyword("before&after");

			// 다중검색을 위해 입력된 키워드를 리스트로 변경
			List<String> keywords = Arrays.asList(StringUtils.split(listRequest.getKeyword(), " "));

			 /*검색 패턴 형식으로 문자열 치환 */
			Map<String, Object> resultMap = new HashMap<String, Object>();
			Map<String, Object> objectMap = new HashMap<>();
			for(int nIdx =0; nIdx < keywords.size(); nIdx++)
				objectMap.put( "keywords" + nIdx, new ArrayList<String>(new TreeSet<String>(Arrays.asList( StringUtils.split(keywordSynonymService.selectKeywordSynonym( keywords.get(nIdx) ), "\\|")))));


			// 주거 - 시공사례
			if (type == 1) {

				ProductListRequest searchGallery = new ProductListRequest();
				searchGallery.setPageLimit(page, limit);
				searchGallery.setCategory_code1(CategoryCode.RESIDENT.getCode());
				searchGallery.setKeywordsMap(objectMap);
				searchGallery.setKeywords(keywords);

				 /*비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*/
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
					searchGallery.setBnA("1");
				}
				resultMap.put("resident_gallery", searchGalleryListRevision(searchGallery));
			}

			// 주거 - 사진
			else if (type == 2) {

				ItemListRequest searchPicture = new ItemListRequest();
				searchPicture.setPageLimit(page, limit);
				searchPicture.setCategory_code1(CategoryCode.RESIDENT.getCode());
				searchPicture.setKeywords(keywords);
				searchPicture.setKeywordsMap(objectMap);
				searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));

				 /*비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*/
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
					searchPicture.setBnA("1");
					searchPicture.setExclude_tags(null);

				}

				// 가입자인 경우 스크랩 여부를 조회한다.
				if (member_no != null) {
					searchPicture.setMember_no(member_no);
				}

				resultMap.put("resident_picture", searchGalleryPictureListRevision(searchPicture));
			}

			// 상업 - 시공사례
			else if (type == 3) {

				ProductListRequest searchGallery = new ProductListRequest();
				searchGallery.setPageLimit(page, limit);
				searchGallery.setCategory_code1(CategoryCode.COMMERCE.getCode());
				searchGallery.setKeywords(keywords);
				searchGallery.setKeywordsMap(objectMap);

				 /*비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*/
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
					searchGallery.setBnA("1");

				resultMap.put("commerce_gallery", searchGalleryListRevision(searchGallery));
			}

			// 파트너스
			else if (type == 4) {

				PartnerListRequest searchPartners = new PartnerListRequest();
				searchPartners.setPageLimit(page, limit);
				searchPartners.setBest_flag(1);
				searchPartners.setThumbnail_cnt(2);
				searchPartners.setKeywords( keywords);

				resultMap.put("partners", searchPartnerList(searchPartners));
			}

			// 매거진 (9개)
			else if (type == 5) {

				MagazineListRequest searchMagazine = new MagazineListRequest();
				searchMagazine.setPageLimit(page, limit);
				searchMagazine.setKeywords(keywords);
				searchMagazine.setKeywordsMap(objectMap);

				resultMap.put("magazine", searchMagazineListRevision(searchMagazine));
			}

			// 상업 - 사진
			else if (type == 6) {

				ItemListRequest searchPicture = new ItemListRequest();
				searchPicture.setPageLimit(page, limit);
				searchPicture.setCategory_code1(CategoryCode.COMMERCE.getCode());
				searchPicture.setKeywords(keywords);
				searchPicture.setKeywordsMap(objectMap);
				searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));

				 /*비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*/
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
					searchPicture.setBnA("1");
					searchPicture.setExclude_tags(null);
				}

				// 가입자인 경우 스크랩 여부를 조회한다.
				if (member_no != null) {
					searchPicture.setMember_no(member_no);
				}

				resultMap.put("commerce_picture", searchGalleryPictureListRevision(searchPicture));
			}

			else {
				log.error("[SEARCH-COMPLEX-MORE] 통합검색-더보기. TYPE["+type+"] 오류. [1~6]의 값만 사용할 수 있습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			return makeResponseEntity(request, resultMap, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error("[SEARCH-COMPLEX-MORE] 통합검색-더보기 실패. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[SEARCH-COMPLEX-MORE] 통합검색-더보기 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 통합검색
	 * @param request
	 * @param listRequest
	 * @return
	 */
	/*@ResponseBody
	@RequestMapping(value = "/search/complex/{type}/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> complex(HttpServletRequest request,
										  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID", required = false) Long zid,
										  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										  @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										  @PathVariable("type") int type,
										  @PathVariable("page") int page,
										  @PathVariable("limit") int limit,
										  @RequestBody SearchListRequest listRequest) {


		try {

			log.debug("[SEARCH-COMPLEX-MORE] 통합검색-더보기. TYPE["+type+"] 검색키워드["+listRequest.getKeyword()+"]");

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				Member member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
				if (member != null) {
					member_no = member.getMember_no();
				}
			}

			*//* Before & After 를 처리 하자 *//*
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				listRequest.setKeyword("before&after");

			// 다중검색을 위해 입력된 키워드를 리스트로 변경
			List<String> keywords = Arrays.asList(StringUtils.split(listRequest.getKeyword(), " "));

			*//* 검색 패턴 형식으로 문자열 치환 *//*

			String searchKeyword = "";
			for(int nIdx = 0; nIdx < keywords.size(); nIdx++)
			{
				*//*keywords.set (nIdx,  String.format( "(.*%s.*)" ,  keywordSynonymService.selectKeywordSynonym( keywords.get(nIdx)) ) );*//*
				keywords.set (nIdx,    keywordSynonymService.selectKeywordSynonym( keywords.get(nIdx))  );
				searchKeyword +=  String.format( "(.*%s.*)" ,  keywords.get(nIdx) ) ;
			}

			Map<String, Object> resultMap = new HashMap<String, Object>();

			// 주거 - 시공사례
			if (type == 1) {

				ProductListRequest searchGallery = new ProductListRequest();
				searchGallery.setPageLimit(page, limit);
				searchGallery.setCategory_code1(CategoryCode.RESIDENT.getCode());
				searchGallery.setKeywords(keywords);


				*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
					searchGallery.setBnA("1");
				}
				resultMap.put("resident_gallery", searchGalleryListRevision(searchGallery));
			}

			// 주거 - 사진
			else if (type == 2) {

				ItemListRequest searchPicture = new ItemListRequest();
				searchPicture.setPageLimit(page, limit);
				searchPicture.setCategory_code1(CategoryCode.RESIDENT.getCode());
				searchPicture.setKeywords(keywords);
				searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));

				*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
					searchPicture.setBnA("1");
					searchPicture.setExclude_tags(null);

				}

				// 가입자인 경우 스크랩 여부를 조회한다.
				if (member_no != null) {
					searchPicture.setMember_no(member_no);
				}

				resultMap.put("resident_picture", searchGalleryPictureListRevision(searchPicture));
			}

			// 상업 - 시공사례
			else if (type == 3) {

				ProductListRequest searchGallery = new ProductListRequest();
				searchGallery.setPageLimit(page, limit);
				searchGallery.setCategory_code1(CategoryCode.COMMERCE.getCode());
				searchGallery.setKeywords(keywords);

				*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
					searchGallery.setBnA("1");

				resultMap.put("commerce_gallery", searchGalleryListRevision(searchGallery));
			}

			// 파트너스
			else if (type == 4) {

				PartnerListRequest searchPartners = new PartnerListRequest();
				searchPartners.setPageLimit(page, limit);
				searchPartners.setBest_flag(1);
				searchPartners.setThumbnail_cnt(2);
				searchPartners.setKeywords( Arrays.asList(StringUtils.split(listRequest.getKeyword(), " ")));

				resultMap.put("partners", searchPartnerList(searchPartners));
			}

			// 매거진 (9개)
			else if (type == 5) {

				MagazineListRequest searchMagazine = new MagazineListRequest();
				searchMagazine.setPageLimit(page, limit);
				searchMagazine.setKeywords(keywords);

				resultMap.put("magazine", searchMagazineListRevision(searchMagazine));
			}

			// 상업 - 사진
			else if (type == 6) {

				ItemListRequest searchPicture = new ItemListRequest();
				searchPicture.setPageLimit(page, limit);
				searchPicture.setCategory_code1(CategoryCode.COMMERCE.getCode());
				searchPicture.setKeywords(keywords);
				searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));

				*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
					searchPicture.setBnA("1");
					searchPicture.setExclude_tags(null);
				}

				// 가입자인 경우 스크랩 여부를 조회한다.
				if (member_no != null) {
					searchPicture.setMember_no(member_no);
				}

				resultMap.put("commerce_picture", searchGalleryPictureListRevision(searchPicture));
			}

			else {
				log.error("[SEARCH-COMPLEX-MORE] 통합검색-더보기. TYPE["+type+"] 오류. [1~6]의 값만 사용할 수 있습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			return makeResponseEntity(request, resultMap, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error("[SEARCH-COMPLEX-MORE] 통합검색-더보기 실패. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[SEARCH-COMPLEX-MORE] 통합검색-더보기 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	*/

	/**
	 * 통합검색
	 * @param request
	 * @param listRequest
	 * @return
	 */
	/*@ResponseBody
	@RequestMapping(value = "/search/complex/{type}/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> complex(HttpServletRequest request,
										  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID", required = false) Long zid,
										  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										  @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										  @PathVariable("type") int type,
										  @PathVariable("page") int page,
										  @PathVariable("limit") int limit,
										  @RequestBody SearchListRequest listRequest) {


		try {

			log.debug("[SEARCH-COMPLEX-MORE] 통합검색-더보기. TYPE["+type+"] 검색키워드["+listRequest.getKeyword()+"]");

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				Member member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
				if (member != null) {
					member_no = member.getMember_no();
				}
			}

			*//* Before & After 를 처리 하자 *//*
			if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
				listRequest.setKeyword("before&after");

			// 다중검색을 위해 입력된 키워드를 리스트로 변경
			List<String> keywords = Arrays.asList(StringUtils.split(listRequest.getKeyword(), " "));

			Map<String, Object> resultMap = new HashMap<String, Object>();

			// 주거 - 시공사례
			if (type == 1) {

				ProductListRequest searchGallery = new ProductListRequest();
				searchGallery.setPageLimit(page, limit);
				searchGallery.setCategory_code1(CategoryCode.RESIDENT.getCode());
				searchGallery.setKeywords(keywords);

				*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
					searchGallery.setBnA("1");
				}
				resultMap.put("resident_gallery", searchGalleryList(searchGallery));
			}

			// 주거 - 사진
			else if (type == 2) {

				ItemListRequest searchPicture = new ItemListRequest();
				searchPicture.setPageLimit(page, limit);
				searchPicture.setCategory_code1(CategoryCode.RESIDENT.getCode());
				searchPicture.setKeywords(keywords);
				searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));

				*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
					searchPicture.setBnA("1");
					searchPicture.setExclude_tags(null);

				}

				// 가입자인 경우 스크랩 여부를 조회한다.
				if (member_no != null) {
					searchPicture.setMember_no(member_no);
				}

				resultMap.put("resident_picture", searchGalleryPictureList(searchPicture));
			}

			// 상업 - 시공사례
			else if (type == 3) {

				ProductListRequest searchGallery = new ProductListRequest();
				searchGallery.setPageLimit(page, limit);
				searchGallery.setCategory_code1(CategoryCode.COMMERCE.getCode());
				searchGallery.setKeywords(keywords);

				*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") ))
					searchGallery.setBnA("1");

				resultMap.put("commerce_gallery", searchGalleryList(searchGallery));
			}

			// 파트너스
			else if (type == 4) {

				PartnerListRequest searchPartners = new PartnerListRequest();
				searchPartners.setPageLimit(page, limit);
				searchPartners.setBest_flag(1);
				searchPartners.setThumbnail_cnt(2);
				searchPartners.setKeywords(keywords);

				resultMap.put("partners", searchPartnerList(searchPartners));
			}

			// 매거진 (9개)
			else if (type == 5) {

				MagazineListRequest searchMagazine = new MagazineListRequest();
				searchMagazine.setPageLimit(page, limit);
				searchMagazine.setKeywords(keywords);

				resultMap.put("magazine", searchMagazineList(searchMagazine));
			}

			// 상업 - 사진
			else if (type == 6) {

				ItemListRequest searchPicture = new ItemListRequest();
				searchPicture.setPageLimit(page, limit);
				searchPicture.setCategory_code1(CategoryCode.COMMERCE.getCode());
				searchPicture.setKeywords(keywords);
				searchPicture.setExclude_tags(new ArrayList<String>(Arrays.asList("BEFORE")));

				*//* 비포앤애프터 항목만 보고 싶을 시 해당 keyword를 통해 요청 한다 (BnA)*//*
				if(listRequest.getKeyword() != null && (listRequest.getKeyword().equals("비포앤애프터") || listRequest.getKeyword().toLowerCase().replace(" ", "").equals("before&after") )) {
					searchPicture.setBnA("1");
					searchPicture.setExclude_tags(null);
				}

				// 가입자인 경우 스크랩 여부를 조회한다.
				if (member_no != null) {
					searchPicture.setMember_no(member_no);
				}

				resultMap.put("commerce_picture", searchGalleryPictureList(searchPicture));
			}

			else {
				log.error("[SEARCH-COMPLEX-MORE] 통합검색-더보기. TYPE["+type+"] 오류. [1~6]의 값만 사용할 수 있습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			return makeResponseEntity(request, resultMap, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error("[SEARCH-COMPLEX-MORE] 통합검색-더보기 실패. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[SEARCH-COMPLEX-MORE] 통합검색-더보기 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}*/

	/**
	 * 통합검색 - 추천키워드 조회
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/search/complex/keyword", method = RequestMethod.GET)
	public ResponseEntity<String> complex_keyword(HttpServletRequest request,
										  @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID", required = false) Long zid,
										  @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										  @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token) {


		try {
			log.debug("[SEARCH-COMPLEX-KEYWORD] 통합검색-추천키워드 조회");

			// 일반 웹 접속인 경우를 제외하고는 가입 여부를 조회한다.
			if (StringUtils.isNotEmpty(web_token) || zid != null) {
				Member member = authenticationHelper.findMemberOrDevice(member_no, zid, zid_key, web_token);
			}

			// 키워드정보
			RecommendKeyword keyword = commonService.selectKeywordList("0000");
			if (keyword != null && keyword.getKeyword_list() != null) {
				return makeResponseEntity(request, keyword.getKeyword_list(), HttpStatus.OK);
			}
			return makeResponseEntity(HttpStatus.NO_CONTENT);
		} catch (ServiceException e) {
			log.error("[SEARCH-COMPLEX-KEYWORD] 키워드 조회 실패. "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[SEARCH-COMPLEX-KEYWORD] 키워드 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 통합검색
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @param page
	 * @param limit
     * @param listRequest
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/search/complex/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> complex(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit,
			@RequestBody(required = false) ListRequest listRequest) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			if(listRequest == null){
				listRequest = new ListRequest();
			}
			listRequest.setPageLimit(page, limit);
			log.info(listRequest.toString());

			Map<String, Object> resultMap = new HashMap<String, Object>();

			//갤러리
			resultMap.put("gallery", findGalleryList(listRequest));

			//갤러리사진
			resultMap.put("gallery_picture", findGalleryPictureList(listRequest));

			//내방어때
			resultMap.put("community", findCommunityList(listRequest));

			//궁금해요
			resultMap.put("whatever", findWhateverList(listRequest));

			//집닥후기
			resultMap.put("zipdoc_reply", findZipdocReplyList(listRequest));

			//인테리어팁
			resultMap.put("interiortip", findInteriortipList(listRequest));

			//집닥맨 다이어리
			resultMap.put("zipdocman", findZipdocmanList(listRequest));

			return makeResponseEntity(request, resultMap, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/list] 내방어때 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 커뮤니티 메인
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @param page
	 * @param limit
     * @param listRequest
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/search/community/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> community(HttpServletRequest request,
											@RequestHeader(value = "APP-VERSION", required = true) String app_version,
											@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											@RequestHeader(value = "ZID", required = true) Long zid,
											@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
											@PathVariable("page") int page,
											@PathVariable("limit") int limit,
											@RequestBody(required = false) ListRequest listRequest) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			if(listRequest == null){
				listRequest = new ListRequest();
			}
			listRequest.setPageLimit(page, limit);
			log.info(listRequest.toString());

			Map<String, Object> resultMap = new HashMap<String, Object>();

			//내방어때
			resultMap.put("community", findCommunityList(listRequest));

			//궁금해요
			resultMap.put("whatever", findWhateverList(listRequest));

			//집닥후기
			resultMap.put("zipdoc_reply", findZipdocReplyList(listRequest));

			return makeResponseEntity(request, resultMap, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/list] 내방어때 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * 고객 생생 후기 조회
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @param page
	 * @param limit
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/search/magazine/vivid/comment/{type}/{page}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<String> vividComment(HttpServletRequest request,
										   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										   @RequestHeader(value = "ZID", required = false) Long zid,
										   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										   @PathVariable("type") String type,
										   @PathVariable("page") Integer page,
										   @PathVariable("limit") Integer limit) {

		try {

			if(page == null) page = 0;
			if(limit == null) limit = 3;

			// 유형 조회
			VividCommentCategoryCode vividCommentCategoryCode = VividCommentCategoryCode.get(type);
			if (vividCommentCategoryCode == null) {
				log.error("[SEARCH-MAGAZINE-VIVID-COMMENT] 고객생생후기-더보기. TYPE[{}] 오류. 유효하지 않은 타입 조회 시도.", type);
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			AgentType agent = AgentType.APP;

			// 가입자 조회
			if (zid != null) {
				Member member = authenticationHelper.findMember(member_no, zid, zid_key);
				log.debug("[MAIN-MAGAZINE-VIVID-COMMENT] MEMBER[" + member.getMember_id() + "]");
			} else {
				agent = AgentType.WEB;
				log.debug("[MAIN-MAGAZINE-VIVID-COMMENT] WEB");
			}

			Map<String, Object> map = new HashMap<>();
			map.put("total_count", customerOpinionsService.getVividCommentCount(vividCommentCategoryCode, agent) );
			map.put("vividcomments",  customerOpinionsService.getVividComment(vividCommentCategoryCode, agent, (page *= limit), limit));

			return makeResponseEntity(request, map, HttpStatus.OK);

		} catch (Exception e) {
			log.error("[MAIN-MAGAZINE-VIVID-COMMENT] 고객 생생 후기 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * 매거진 메인
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @param page
	 * @param limit
     * @param listRequest
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/search/magazine/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> magazine(HttpServletRequest request,
										  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										  @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										  @RequestHeader(value = "ZID", required = true) Long zid,
										  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
										  @PathVariable("page") int page,
										  @PathVariable("limit") int limit,
										  @RequestBody(required = false) ListRequest listRequest) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			if(listRequest == null){
				listRequest = new ListRequest();
			}
			listRequest.setPageLimit(page, limit);
			log.info(listRequest.toString());

			Map<String, Object> resultMap = new HashMap<String, Object>();

			//인테리어팁
			resultMap.put("interiortip", findInteriortipList(listRequest));

			//집닥맨 다이어리
			resultMap.put("zipdocman", findZipdocmanList(listRequest));

			return makeResponseEntity(request, resultMap, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[/community/list] 내방어때 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * 갤러리 목록 조회 (V3.5 (revision))
	 * @param searchCondition
	 * @return
	 */
	private ProductListResponse searchGalleryListRevision(ProductListRequest searchCondition) {

		// 시공사례
		searchCondition.setProduct_type(ProductType.GALLERY.getCode());

		//정렬방식추가
		searchCondition.setSort(2);
		Map<String, Object> objectMap = new HashMap<>();
		objectMap = searchCondition.getKeywordsMap();
		objectMap.put("keywords", searchCondition.getKeywords());

		if(searchCondition.getBnA() != null) {
			objectMap.put("BnA", searchCondition.getBnA());
			if( StringUtils.equals( searchCondition.getBnA(), "1") )
				objectMap.remove("exclude_tags");
		}
		if(searchCondition.getCategory_code1() != null)
			objectMap.put("category_code1", searchCondition.getCategory_code1());
		if(searchCondition.getMember_no() != null)
			objectMap.put("member_no", searchCondition.getMember_no());
		objectMap.put("startRow", searchCondition.getStartRow());
		objectMap.put("limit", searchCondition.getLimit());
		objectMap.put("sort",searchCondition.getSort() );
		objectMap.put("product_type", searchCondition.getProduct_type() );

		int total_count = productService.searchGalleryTotalCountRevision(objectMap);

		List<Product> list = null;

		// 조회된 결과가 없을 경우
		if (total_count>0) {
			list = productService.searchGalleryListRevision(objectMap);

			for (Product product:list){
				product.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}
		}
		ProductListResponse result = new ProductListResponse(total_count, list);
		return result;
	}


	/**
	 * 갤러리 목록 조회 (V3.5)
	 * @param searchCondition
	 * @return
     */
	private ProductListResponse searchGalleryList(ProductListRequest searchCondition) {

		// 시공사례
		searchCondition.setProduct_type(ProductType.GALLERY.getCode());

		//정렬방식추가
		searchCondition.setSort(2);
		int total_count = productService.searchGalleryTotalCount(searchCondition);

		List<Product> list = null;

		// 조회된 결과가 없을 경우
 		if (total_count>0) {
			list = productService.searchGalleryList(searchCondition);

			for (Product product:list){
				product.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}
		}
		ProductListResponse result = new ProductListResponse(total_count, list);
		return result;
	}

	/**
	 * 갤러리 목록 조회
	 * @param listRequest
	 * @return
	 */
	private ProductListResponse findGalleryList(ListRequest listRequest){
		ProductListRequest searchCondition = new ProductListRequest();
		searchCondition.setPageLimit(listRequest.getPage(), listRequest.getLimit());
		searchCondition.setKeyword(listRequest.getKeyword());
		searchCondition.setProduct_type(ProductType.GALLERY.getCode());
		//정렬방식추가
		searchCondition.setSort(2);
		int total_count = productService.selectListTotalCount(searchCondition);

		List<Product> list = null;

		// 조회된 결과가 없을 경우
		if(total_count>0) {
			list = productService.selectList(searchCondition);

			for(Product product:list){
				product.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}
		}
		ProductListResponse result = new ProductListResponse(total_count, list);
		return result;
	}

	/**
	 * 갤러리 사진 조회 (V3.5 Revision)
	 * @param searchCondition
	 * @return
	 */
	private ItemListResponse searchGalleryPictureListRevision(ItemListRequest searchCondition) {

		Map<String, Object> objectMap = new HashMap<>();
		objectMap = searchCondition.getKeywordsMap();
		objectMap.put("keywords", searchCondition.getKeywords());
		if(searchCondition.getExclude_tags() != null)
			objectMap.put("exclude_tags", searchCondition.getExclude_tags());
		if(searchCondition.getBnA() != null) {
			objectMap.put("BnA", searchCondition.getBnA());
			if( StringUtils.equals( searchCondition.getBnA(), "1") )
				objectMap.remove("exclude_tags");
		}
		if(searchCondition.getCategory_code1() != null)
			objectMap.put("category_code1", searchCondition.getCategory_code1());
		if(searchCondition.getMember_no() != null)
			objectMap.put("member_no", searchCondition.getMember_no());
		objectMap.put("startRow", searchCondition.getStartRow());
		objectMap.put("limit", searchCondition.getLimit());

		int total_count = productService.searchGalleryPictureTotalCountRevision(objectMap);
		List<Item> list = null;

		if (total_count > 0) {
			list = productService.searchGalleryPictureListRevision(objectMap);
			for (Item item:list){
				item.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}
		}
		ItemListResponse result = new ItemListResponse(total_count, list);
		return result;
	}

	/**
	 * 갤러리 사진 조회 (V3.5)
	 * @param searchCondition
	 * @return
	 */
	private ItemListResponse searchGalleryPictureList(ItemListRequest searchCondition) {

		int total_count = productService.searchGalleryPictureTotalCount(searchCondition);
		List<Item> list = null;

		if (total_count > 0) {
			list = productService.searchGalleryPictureList(searchCondition);
			for (Item item:list){
				item.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}
		}
		ItemListResponse result = new ItemListResponse(total_count, list);
		return result;
	}

	/**
	 * 갤러리 사진 조회
	 * @param listRequest
	 * @return
     */
	private ItemListResponse findGalleryPictureList(ListRequest listRequest) {
		ItemListRequest searchCondition = new ItemListRequest();

		searchCondition.setPageLimit(listRequest.getPage(), listRequest.getLimit());
		searchCondition.setKeyword(listRequest.getKeyword());

		int total_count = productService.selectItemListTotalCount(searchCondition);
		List<Item> list = null;

		if(total_count>0) {
			list = productService.selectItemList(searchCondition);
			for(Item item:list){
				item.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}
		}
		ItemListResponse result = new ItemListResponse(total_count, list);
		return result;
	}

	/**
	 * 파트너 조회 (V3.5 Revision)
	 * @param searchCondition
	 * @return
	 */
	private ListResponse searchPartnerListRevision(PartnerListRequest searchCondition) {

		Map<String, Object> objectMap = new HashMap<>();
		objectMap = searchCondition.getKeywordsMap();
		objectMap.put("keywords", searchCondition.getKeywords());
		objectMap.put("best_flag", searchCondition.getBest_flag());
		objectMap.put("thumbnail_cnt", searchCondition.getThumbnail_cnt());
		objectMap.put("startRow", searchCondition.getStartRow());
		objectMap.put("limit", searchCondition.getLimit());


		int total_count = partnerService.selectPartnerListCountRevision(objectMap);
		List<Partner> list = null;

		if (total_count > 0) {
			list = partnerService.selectPartnerList(searchCondition);
			for (Partner partner : list) {

				// 노출순위가 10인 경우 최우수 파트너로 변경
				if (partner.getSeq() != null && partner.getSeq() == 10) {
					partner.setPartner_type(3);
				}
				partner.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}
		}
		ListResponse result = new ListResponse(total_count, list);
		return result;
	}


	/**
	 * 파트너 조회 (V3.5)
	 * @param searchCondition
	 * @return
	 */
	private ListResponse searchPartnerList(PartnerListRequest searchCondition) {

		int total_count = partnerService.selectPartnerListCount(searchCondition);
		List<Partner> list = null;

		if (total_count > 0) {
			list = partnerService.selectPartnerList(searchCondition);
			for (Partner partner : list) {

				// 노출순위가 10인 경우 최우수 파트너로 변경
				if (partner.getSeq() != null && partner.getSeq() == 10) {
					partner.setPartner_type(3);
				}
				partner.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}
		}
		ListResponse result = new ListResponse(total_count, list);
		return result;
	}

	/**
	 * 매거진 조회 (V3.5 Revision)
	 * @param searchCondition
	 * @return
	 */
	private ListResponse searchMagazineListRevision(MagazineListRequest searchCondition) {
		return magazineService.searchMagazineListRevision(searchCondition);
	}

	/**
	 * 매거진 조회 (V3.5)
	 * @param searchCondition
	 * @return
	 */
	private ListResponse searchMagazineList(MagazineListRequest searchCondition) {
		return magazineService.searchMagazineList(searchCondition);
	}

	/**
	 * 내방어때 조회
	 * @param listRequest
	 * @return
     */
	private ProductListResponse findCommunityList(ListRequest listRequest){
		ProductListRequest searchCondition = new ProductListRequest();
		searchCondition.setPageLimit(listRequest.getPage(), listRequest.getLimit());
		searchCondition.setKeyword(listRequest.getKeyword());
		searchCondition.setProduct_type(ProductType.COMMUNITY.getCode());
		int total_count = productService.selectCommunityListTotalCount(searchCondition);

		List<Product> list = null;
		if(total_count>0) {
			list = productService.selectCommunityList(searchCondition);
			for(Product product:list){
				product.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}
		}

		ProductListResponse result = new ProductListResponse(total_count, list);
		return result;
	}

	/**
	 * 궁금해요 조회
	 * @param listRequest
	 * @return
     */
	private ListResponse findWhateverList(ListRequest listRequest){
		WhateverListRequest searchCondition = new WhateverListRequest();
		searchCondition.setPageLimit(listRequest.getPage(), listRequest.getLimit());
		searchCondition.setKeyword(listRequest.getKeyword());

		int total_count = whateverService.selectListTotalCount(searchCondition);
		List<Whatever> list = null;
		if(total_count>0) {
			list = whateverService.selectList(searchCondition);
			//이미지 상대경로를 URL로 변경
			for(Whatever whatever:list){
				whatever.buildForSendMessage();
			}
		}
		ListResponse result = new ListResponse(total_count, list);
		return result;
	}

	/**
	 * 집닥후기 조회
	 * @param listRequest
	 * @return
     */
	private ListResponse findZipdocReplyList(ListRequest listRequest){
		ZipdocReplyListRequest searchCondition = new ZipdocReplyListRequest();
		searchCondition.setPageLimit(listRequest.getPage(), listRequest.getLimit());
		searchCondition.setKeyword(listRequest.getKeyword());
		int total_count = zipdocReplyService.selectListTotalCount(searchCondition);
		List<ZipdocReply> list = null;

		if(total_count>0) {
			list = zipdocReplyService.selectList(searchCondition);
			//이미지 상대경로를 URL로 변경
			for(ZipdocReply zipdocReply:list){
				zipdocReply.buildForSendMessage();
			}
		}
		ListResponse result = new ListResponse(total_count, list);
		return result;
	}

	/**
	 * 인테리어팁 조회
	 * @param listRequest
	 * @return
     */
	private ArticleListResponse findInteriortipList(ListRequest listRequest){
		ArticleListRequest searchCondition = new ArticleListRequest(ArticleType.INTERIOR_TIP, listRequest.getPage(), listRequest.getLimit());
		searchCondition.setKeyword(listRequest.getKeyword());
		ArticleListResponse result = articleService.selectArticleList(searchCondition);
		result.buildUrl(ArticleType.INTERIOR_TIP);
		return result;
	}

	/**
	 * 집닥맨 다이어리 조회
	 * @param listRequest
	 * @return
     */
	private ArticleListResponse findZipdocmanList(ListRequest listRequest){
		ArticleListRequest searchCondition = new ArticleListRequest(ArticleType.ZIPDOCMAN, listRequest.getPage(), listRequest.getLimit());
		searchCondition.setKeyword(listRequest.getKeyword());
		ArticleListResponse result = articleService.selectArticleList(searchCondition);
		result.buildUrl(ArticleType.ZIPDOCMAN);
		return result;
	}
}

package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.SMSStatus;
import com.zipdoc.acs.domain.service.SMSService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;


import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


@Controller
public class SMSController extends ResponseEntityService {

    private static final Logger log = LoggerFactory.getLogger(SMSController.class);

    @Autowired
    private SMSService smsService;

    //인증번호 생성
    @ResponseBody
    @RequestMapping(value = "/sms/tempkey", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<String> tempkey(HttpServletRequest request,
                                        @RequestParam(value = "phone", required = true) String phone) {


        // 파라미터 검사
        if (StringUtils.isEmpty(phone)) return makeResponseEntity(request, "Missing parameter [phone]", HttpStatus.BAD_REQUEST);

        String resultCode = smsService.tempKeyGenarator(phone);

        Map<String, String> result = new HashMap<String, String>();

        if(resultCode != null) {

            if(resultCode.equals(SMSStatus.SUCCESS.getCode())
                    || resultCode.equals(SMSStatus.RESERVED.getCode())
                    || resultCode.equals(SMSStatus.TEST_SUCCESS.getCode())) {

                result.put("result","success");
            } else {
                result.put("result","fail");
            }

        }

        //Test
//        resultCode = "3208";
//        result.put("result","fail");
        result.put("result_code",resultCode);


        return makeResponseEntity(request, result, HttpStatus.OK);

    }

    //인증번호 확인
    @ResponseBody
    @RequestMapping(value = "/sms/certification", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<String> certification(HttpServletRequest request,
                                          @RequestParam(value = "phone", required = true) String phone,
                                          @RequestParam(value = "temp_key", required = true) String temp_key,
                                          @RequestParam(value = "current_timestamp", required = true) String current_timestamp
    ) {

        // 파라미터 검사
        if (StringUtils.isEmpty(phone)) return makeResponseEntity(request, "Missing parameter [phone]", HttpStatus.BAD_REQUEST);
        if (StringUtils.isEmpty(temp_key)) return makeResponseEntity(request, "Missing parameter [temp_key]", HttpStatus.BAD_REQUEST);
        if (StringUtils.isEmpty(current_timestamp)) return makeResponseEntity(request, "Missing parameter [current_timestamp]", HttpStatus.BAD_REQUEST);

        log.debug("sms certification call");
        log.debug("sms temp_key:"+temp_key);

        Map<String, String> result = new HashMap<String, String>();

        String resultCode = smsService.isCorrectCertifiKey(phone,temp_key,current_timestamp);


        if(resultCode != null) {

            if(resultCode.equals("success")) {
                result.put("result","success");
            } else if(resultCode.equals("expiration")) {
                result.put("result","expiration");
            } else {
                result.put("result","fail");
            }

        }

        return makeResponseEntity(request, result, HttpStatus.OK);
    }

}

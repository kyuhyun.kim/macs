package com.zipdoc.acs.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.define.PictureType;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.pns.MessageSender;
import com.zipdoc.acs.domain.pns.MsgType;
import com.zipdoc.acs.domain.pns.SendMessage;
import com.zipdoc.acs.domain.service.*;
import com.zipdoc.acs.model.*;
import com.zipdoc.acs.persistence.dao.PartnerDao;
import com.zipdoc.acs.utils.*;

import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * 시공 업체 조회 기능
 * @author 이동규
 *
 */
@Controller
public class PartnerController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(PartnerController.class);

	private static SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");

	@Value("${UPLOAD_FILE.STATIC_PATH}")
	private String static_path;

	@Value("${STATIC_URL}")
	private String static_download_url;

	@Value("${ASSIGN_EXPIRATION_HOUR}")
	private int assign_expiration_hour;

	@Autowired
	private MemberService memberService;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private ProductService productService;

	@Autowired
	private EstimateService estimateService;

	@Autowired
	private ContractService contractService;

	@Autowired
	private MessageSender messageSender;

	@Autowired
	private SendMessageService sendMessageService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@Autowired
	private PartnerDao partnerDao;

	@ResponseBody
	@RequestMapping(value = "/partner/detail/{partner_id}", method = RequestMethod.GET)
	public ResponseEntity<String> detail(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = false) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
			@RequestHeader(value = "ZID", required = false) Long zid,
			@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
			@PathVariable("partner_id") Integer partner_id) {

		try {

			if (partner_id == null || partner_id <= 0){
				log.error("[PARTNER-DETAIL] PARTNER_ID["+partner_id+"] 필수 요청 항목 누락.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 조회할 시공사례 개수 (0은 개수 제한 없음)
			int gallery_cnt = 0;

			// 앱 요청인 경우 가입 여부 조회
			if (zid != null && StringUtils.isNotEmpty(zid_key)) {
				authenticationHelper.findMember(member_no, zid, zid_key);
				// 앱인 경우 최대 시공사례 개수를 4개로 설정
				gallery_cnt = 4;
			}

			// 시공사례가 포함된 파트너 정보 조회
			Partner result = productService.selectPartner(partner_id, gallery_cnt);

			// 조회된 결과가 없을 경우
			if (result == null) {
				log.debug("[PARTNER-DETAIL] "+partner_id+" 업체 정보가 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			// 2018.11.27. KJB. 사업자등록주소를 토근 2개까지만 사용
			if (StringUtils.isNotEmpty(result.getAddress())) {
				String [] token = result.getAddress().split(" ");
				if (token != null && token.length >= 2) {
					result.setAddress(token[0] + " "+ token[1]);
				} else {
					result.setAddress(result.getSite_address());
				}
			} else {
				result.setAddress(result.getSite_address());
			}

			if(StringUtils.isEmpty(result.getSite_address()))
				result.setSite_address(result.getAddress());


			// URL 생성 (로고, 대표이미지, 시공사례 URL 등)
			result.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);

			// 요청 응답
			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[PARTNER-DETAIL] " +e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-DETAIL] 정보 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/list/{page}/{limit}", method = RequestMethod.POST)
	public ResponseEntity<String> list(HttpServletRequest request,
									   @RequestHeader(value = "APP-VERSION", required = true) String app_version,
									   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									   @RequestHeader(value = "ZID", required = false) Long zid,
									   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
									   @PathVariable("page") int page,
									   @PathVariable("limit") int limit,
									   @RequestBody(required = false) PartnerListRequest searchCondition) {
		return partners(request, app_version, member_no, zid, zid_key, page, limit, 1, searchCondition);
	}

	@ResponseBody
	@RequestMapping(value = "/partner/list/{page}/{limit}/{best_flag}", method = RequestMethod.POST)
	public ResponseEntity<String> list(HttpServletRequest request,
										 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										 @RequestHeader(value = "ZID", required = false) Long zid,
										 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										 @PathVariable("page") int page,
										 @PathVariable("limit") int limit,
									     @PathVariable("best_flag") int best_flag,
									     @RequestBody(required = false) PartnerListRequest searchCondition) {
		return partners(request, app_version, member_no, zid, zid_key, page, limit, best_flag, searchCondition);
	}

	@ResponseBody
	@RequestMapping(value = "/partner/list/{page}/{limit}/{best_flag}/{month}", method = RequestMethod.POST)
	public ResponseEntity<String> zip_list(HttpServletRequest request,
									   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
									   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
									   @RequestHeader(value = "ZID", required = false) Long zid,
									   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
									   @PathVariable("page") int page,
									   @PathVariable("limit") int limit,
									   @PathVariable("best_flag") int best_flag,
									   @PathVariable("month") String month,
									   @RequestBody(required = false) PartnerListRequest searchCondition) {
		if(month != null)
		{
			if(month.trim().length() == "yyyy-q".length())
				return quarter_partners(request, app_version, member_no, zid, zid_key, page, limit, best_flag, month, searchCondition);

		}
		return month_partners(request, app_version, member_no, zid, zid_key, page, limit, best_flag, month, searchCondition);
	}


	private ResponseEntity<String> zip_partners(HttpServletRequest request,
											String app_version,
											Long member_no,
											Long zid,
											String zid_key,
											int page,
											int limit,
											int best_flag,
											PartnerListRequest searchCondition) {
		try {

			AgentType agent = AgentType.APP;

			// 가입자 조회
			if (zid != null) {
				Member member = authenticationHelper.findMember(member_no, zid, zid_key);
				log.debug("[PARTNER-LIST] APP. MEMBER[" + member.getMember_id() + "]");
			} else {
				agent = AgentType.WEB;
				log.debug("[PARTNER-LIST] WEB. ");
			}

			if (searchCondition == null) {
				searchCondition = new PartnerListRequest();
			}
			searchCondition.setPageLimit(page, limit);
			searchCondition.setBest_flag(best_flag);
			searchCondition.setThumbnail_cnt(agent == AgentType.WEB ? 2 : 3);

			// 다중검색을 위해 입력된 키워드를 리스트로 변경
			if (StringUtils.isNotEmpty(searchCondition.getKeyword())) {
				List<String> keywords = Arrays.asList(StringUtils.split(searchCondition.getKeyword(), " "));
				searchCondition.setKeywords(keywords);
				searchCondition.setKeyword(null);
			}

			List<Partner> list = partnerService.selectPartnerList(searchCondition);

			// 조회된 결과가 없을 경우
			if (list == null || list.size() == 0) {
				log.debug("[PARTNER-LIST] 업체 정보가 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			for (Partner partner : list) {

				// 노출순위가 10인 경우 최우수 파트너로 변경
				if (partner.getSeq() != null && partner.getSeq() == 10) {
					partner.setPartner_type(3);
				}
				partner.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}

			ListResponse<Partner> response = new ListResponse<>();
			response.setTotal_count(partnerService.selectPartnerListCount(searchCondition));
			response.setList(list);


			return makeResponseEntity(request, response, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("파트너사 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private ResponseEntity<String> month_partners(HttpServletRequest request,
											String app_version,
											Long member_no,
											Long zid,
											String zid_key,
											int page,
											int limit,
											int best_flag,
											String month,
											PartnerListRequest searchCondition) {
		try {

			AgentType agent = AgentType.APP;

			// 가입자 조회
			if (zid != null) {
				Member member = authenticationHelper.findMember(member_no, zid, zid_key);
				log.debug("[PARTNER-LIST] APP. MEMBER[" + member.getMember_id() + "]");
			} else {
				agent = AgentType.WEB;
				log.debug("[PARTNER-LIST] WEB. ");
			}

			Map<String, Object> condition = new HashMap<>();
			condition.put("startRow", page * limit);
			condition.put("limit", limit);
			condition.put("month", month);
			condition.put("thumbnail_cnt", agent == AgentType.WEB ? 2 : 3);

			List<Partner> list = partnerService.selectMonthPartnerList(condition);

			// 조회된 결과가 없을 경우
			if (list == null || list.size() == 0) {
				log.debug("[MONTH-PARTNER-LIST] 업체 정보가 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			for (Partner partner : list) {

				// 노출순위가 10인 경우 최우수 파트너로 변경
				if (partner.getSeq() != null && partner.getSeq() == 10) {
					partner.setPartner_type(3);
				}
				partner.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}


			ListResponse<Partner> response = new ListResponse<>();
			response.setTotal_count(partnerService.selectMonthPartnerListCount(condition));
			response.setList(list);


			return makeResponseEntity(request, response, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("월 추천 파트너사 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	private ResponseEntity<String> quarter_partners(HttpServletRequest request,
												  String app_version,
												  Long member_no,
												  Long zid,
												  String zid_key,
												  int page,
												  int limit,
												  int best_flag,
												  String month,
												  PartnerListRequest searchCondition) {
		try {

			AgentType agent = AgentType.APP;

			// 가입자 조회
			if (zid != null) {
				Member member = authenticationHelper.findMember(member_no, zid, zid_key);
				log.debug("[PARTNER-LIST] APP. MEMBER[" + member.getMember_id() + "]");
			} else {
				agent = AgentType.WEB;
				log.debug("[PARTNER-LIST] WEB. ");
			}

			Map<String, Object> condition = new HashMap<>();
			condition.put("startRow", page * limit);
			condition.put("limit", limit);
			condition.put("best_year", month.substring(0, 4));
			condition.put("best_quarter", month.substring(5, 6));
			condition.put("thumbnail_cnt", agent == AgentType.WEB ? 2 : 3);

			List<Partner> list = partnerService.selectQuarterPartnerList(condition);

			// 조회된 결과가 없을 경우
			if (list == null || list.size() == 0) {
				log.debug("[QUARTER-PARTNER-LIST] 업체 정보가 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			for (Partner partner : list) {

				// 노출순위가 10인 경우 최우수 파트너로 변경
				if (partner.getSeq() != null && partner.getSeq() == 10) {
					partner.setPartner_type(3);
				}
				partner.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}


			ListResponse<Partner> response = new ListResponse<>();
			response.setTotal_count(partnerService.selectQuarterPartnerListCount(condition));
			response.setList(list);


			return makeResponseEntity(request, response, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("월 추천 파트너사 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private ResponseEntity<String> partners(HttpServletRequest request,
											String app_version,
									   		Long member_no,
									   		Long zid,
									   		String zid_key,
									   		int page,
									   		int limit,
									   		int best_flag,
											PartnerListRequest searchCondition) {
		try {

 			AgentType agent = AgentType.APP;

			// 가입자 조회
			if (zid != null) {
				Member member = authenticationHelper.findMember(member_no, zid, zid_key);
				log.debug("[PARTNER-LIST] APP. MEMBER[" + member.getMember_id() + "]");
			} else {
				agent = AgentType.WEB;
				log.debug("[PARTNER-LIST] WEB. ");
			}

			if (searchCondition == null) {
				searchCondition = new PartnerListRequest();
			}
			searchCondition.setPageLimit(page, limit);
			searchCondition.setBest_flag(best_flag);
			searchCondition.setThumbnail_cnt(agent == AgentType.WEB ? 2 : 3);

			// 다중검색을 위해 입력된 키워드를 리스트로 변경
			if (StringUtils.isNotEmpty(searchCondition.getKeyword())) {
				List<String> keywords = Arrays.asList(StringUtils.split(searchCondition.getKeyword(), " "));
				searchCondition.setKeywords(keywords);
				searchCondition.setKeyword(null);
			}


			List<Partner> list = partnerService.selectPartnerList(searchCondition);

			// 조회된 결과가 없을 경우
			if (list == null || list.size() == 0) {
				log.debug("[PARTNER-LIST] 업체 정보가 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			for (Partner partner : list) {

				// 노출순위가 10인 경우 최우수 파트너로 변경
				if (partner.getSeq() != null && partner.getSeq() == 10) {
					partner.setPartner_type(3);
				}
				partner.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}

 			ListResponse<Partner> response = new ListResponse<>();
			response.setTotal_count(partnerService.selectPartnerListCount(searchCondition));
			response.setList(list);

			/* 신규 추가 */

			/*if(searchCondition.getMonth_best() == new Integer(1)) {
				List<Partner> best_list = partnerService.selectMonthBestPartnerList();
				response.setBest_list(best_list);
			}*/

			return makeResponseEntity(request, response, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("파트너사 목록 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 파트너 정보를 조회한다.
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/partner/detail", method = RequestMethod.GET)
	public ResponseEntity<String> partner_detail(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			partner.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);

			return makeResponseEntity(request, partner, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-DETAIL] 파트너사 정보 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 파트너에게 할당된 계약 상세 정보를 조회한다.
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/partner/assign/detail/{estimate_no}", method = RequestMethod.GET)
	public ResponseEntity<String> assign_detail(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("estimate_no") Long estimate_no) {

		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 견적 조회
			PartnerAssign result = partnerService.selectAssignDetail(partner.getPartner_id(), estimate_no);

			// 견적 정보가 조회 되지 않을 경우
			if (result == null) {
				log.debug("[PARTNER-ASSIGN-DETAIL] ["+member.getMember_id()+"]님이 요청한 견적[ID="+estimate_no+"]이 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			//1차 통화상담 리스트
			if(result.getCall_status() != null){
				result.setCall_list(partnerService.selectCallEstimateList(partner.getPartner_id(), estimate_no));
			}

			// 컨셉 조회
			List<Product> conceptList = estimateService.selectEstimateConceptProductList(estimate_no);
			if(conceptList != null && conceptList.size() > 0){
				for(Product concept:conceptList) {
					//공유하기 URL 설정
					concept.buildShareUrl();
					//사진 URL 구성
					concept.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
				}
				result.setConcepts(conceptList);
			}

			// 2018.05.03. KJB. 파트너별 수수료 정책 적용
			result.setCommission_rate(partner.getPartner_commission());

			// 계약 조회
			if (result.getAssign_status() == AssignStatus.CONTRACT_CANCEL.value()) {
				result.setContract(partnerService.selectContract_Cancel(partner.getPartner_id(), estimate_no));
			} else {
				result.setContract(partnerService.selectContract_estimateNo(partner.getPartner_id(), estimate_no));
			}

			// 파트너 이름
			if (result.getContract() != null) {
				// 계약 첨부 파일 조회
				List<ContractFile> attachedFiles = contractService.selectContractFileList(result.getContract().getContract_no());
				result.getContract().setAttached_files(attachedFiles);
				result.getContract().buildUrl();
			}

			return makeResponseEntity(request, result, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-ASSIGN-DETAIL] 계약 상세 정보 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 파트너에게 할당된 신규 의뢰 정보를 조회한다.
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/partner/state/assign", method = RequestMethod.GET)
	public ResponseEntity<String> state_assign(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 응답 메시지 생성
			PartnerAssignResponse result = new PartnerAssignResponse();

			// 다음 결제일 계산 (20일 기준)
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());

			if (cal.get(Calendar.DAY_OF_MONTH) >= 20) {
				cal.add(Calendar.MONTH, 1);
			}
			cal.set(Calendar.DAY_OF_MONTH, 20);

			Date next = date.parse(date.format(new Date(cal.getTimeInMillis())));
			Date current = date.parse(date.format(new Date(System.currentTimeMillis())));

			long days = (next.getTime() - current.getTime()) / (24 * 60 * 60 * 1000);

			log.debug("[PARTNER-STATE-ASSIGN] ["+member.getMember_id()+"]의 다음 결제일: "+date.format(new Date(cal.getTimeInMillis())));
			result.setRemaining((int)days);

			// 이번달 수락 누적 건수
			result.setTotal_accept(partnerService.getAssignAcceptCount(partner.getPartner_id()));

			// 이번달 거절 누적 건수
			result.setTotal_reject(partnerService.getAssignRejectCount(partner.getPartner_id()));

            // 진행중인 DB 전체 건수
			result.setOngoing_count(partnerService.getAssignOngoingCount(partner.getPartner_id()));

			// 할당된 견적의 자동 회수 시간
			result.setAssign_expiration_hour(assign_expiration_hour);

			// 신규 견적 할당 조회
			result.setList(partnerService.selectAssignNew(partner.getPartner_id()));

			return makeResponseEntity(request, result, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-STATE-ASSIGN] 신규 의뢰 DB 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/state/ongoing/{page}/{limit}"/*, method = RequestMethod.GET*/)
	public ResponseEntity<String> state_ongoing(HttpServletRequest request,
												@RequestHeader(value = "APP-VERSION", required = true) String app_version,
												@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
												@RequestHeader(value = "ZID", required = true) Long zid,
												@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
												@RequestBody(required = false) PartnerAssignSearchRequest searchRequest,
												@PathVariable("page")int page,
												@PathVariable("limit") int limit) {
		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			PartnerAssignResponse result = new PartnerAssignResponse();

			// 신규 의뢰 건수
			result.setAssign_count(partnerService.getAssignNewCount(partner.getPartner_id()));

			// 진행중인 DB 전체 건수
			result.setOngoing_count(partnerService.getAssignOngoingCount(partner.getPartner_id()));

			// 진행중 DB 조회
			if(searchRequest==null){
				searchRequest = new PartnerAssignSearchRequest();
			}
			searchRequest.setPageLimit(page, limit);
			searchRequest.setPartner_id(partner.getPartner_id());

			List<PartnerAssign> list = partnerService.selectAssignOngoing(searchRequest);

			// 상태에 따른 UPDATE_DATE 설정
			for (int i = 0; list != null && i < list.size(); i++) {
				PartnerAssign assign = list.get(i);
				AssignStatus status = AssignStatus.valueOf(assign.getAssign_status());
				switch (status) {

					case ASSIGNED:
					case SCHEDULE_COUNSELING:
					case COMPLETE_COUNSELING:
					case PUBLISH_ESTIMATE:
					{
						break;
					}
					/*
					case ASSIGNED:
					{
						assign.setUpdate_date(assign.getAccept_date());
						break;
					}
					case SCHEDULE_COUNSELING:
					{
						assign.setUpdate_date(assign.getAccept_date());
						break;
					}
					case COMPLETE_COUNSELING:
					{
						assign.setUpdate_date(assign.getVisit_date());
						break;
					}
					case PUBLISH_ESTIMATE:
					{
						assign.setUpdate_date(assign.getPublish_date());
						break;
					}*/
					case COMPLETE_CONTRACT:
					{
						assign.setContract(partnerService.selectContractOngoing(partner.getPartner_id(), assign.getEstimate_no()));
						if (assign.getContract() != null) {
							//assign.setUpdate_date(assign.getContract().getContract_date());
							assign.getContract().setContract_date(null);
						}
						break;
					}
				}

				// UPDATE_DATE만 남겨놓고 다른 값들은 전달되지 않도록 한다.
				assign.setAccept_date(null);
				assign.setVisit_date(null);
				assign.setPublish_date(null);
			}
			result.setList(list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-STATE-ONGOING] 진행중 DB 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/state/completion/{page}/{limit}"/*, method = RequestMethod.POST*/)
	public ResponseEntity<String> state_completion(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
		    @RequestBody(required = false) PartnerAssignSearchRequest searchRequest,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			PartnerAssignResponse result = new PartnerAssignResponse();

			// 공사 완료 DB 조회
			if(searchRequest==null){
				searchRequest = new PartnerAssignSearchRequest();
			}
			searchRequest.setPageLimit(page, limit);
			searchRequest.setPartner_id(partner.getPartner_id());
			List<PartnerAssign> list = partnerService.selectAssignCompletion(searchRequest);

			// 상태에 따른 UPDATE_DATE 설정
			for (int i = 0; list != null && i < list.size(); i++) {
				PartnerAssign assign = list.get(i);
				assign.setContract(partnerService.selectContractCompletion(partner.getPartner_id(), assign.getEstimate_no()));
			}
			result.setList(list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-STATE-ONGOING] 진행중 DB 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/state/withdraw/{page}/{limit}"/*, method = RequestMethod.POST*/)
	public ResponseEntity<String> state_withdraw(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@RequestBody(required = false) PartnerAssignSearchRequest searchRequest,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			PartnerAssignResponse result = new PartnerAssignResponse();

			// 계약 미성사 DB 조회
			if(searchRequest==null){
				searchRequest = new PartnerAssignSearchRequest();
			}
			searchRequest.setPageLimit(page, limit);
			searchRequest.setPartner_id(partner.getPartner_id());
			result.setList(partnerService.selectAssignWithdraw(searchRequest));

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-STATE-WITHDRAW] 계약 미성사 DB 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/state/withdraw/{pid}/list"/*, method = RequestMethod.POST*/)
	public ResponseEntity<String> state_withdraw_list(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
												 @RequestHeader(value = "ZID", required = true) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
												 @PathVariable("pid") int pid
												 ) {
		try {
			List<PartnerAssignGeo> list1 = partnerService.selectAssignWithdrawGeo(pid);
			List<PartnerAssignGeo> list2 = partnerDao.selectCSbehindContractList(pid);

			PartnerAssignResponse result = new PartnerAssignResponse();

			// 계약 미성사 DB 조회
			if (pid!=0){

				List<PartnerAssignGeo> list = new ArrayList<PartnerAssignGeo>();
				if(list1 != null && list1.size() > 0) {
					list.addAll(list1);
				}

				if(list2 != null && list2.size() > 0) {
					list.addAll(list2);
				}

				List<PartnerAssignGeo> processedList = null;

				if(list != null && list.size() > 0) {

					List<PartnerAssignGeo> resultList = new ArrayList<PartnerAssignGeo>();

					resultList.addAll(list);

					HashSet<PartnerAssignGeo> listSet = new HashSet<PartnerAssignGeo>(resultList);

					processedList = new ArrayList<PartnerAssignGeo>(listSet);

				}

				result.setGeo_list(processedList);
				result.setAes128_key(Messages.getMessage("AES128.KEY"));
			}

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-STATE-WITHDRAW] 계약 미성사 DB 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



	/**
	 * 파트너에게 신규로 할당된 견적을 수락/거절 한다.
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/partner/assign/{estimate_no}/{decision}", method = RequestMethod.POST)
	public ResponseEntity<String> assign_decision(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("estimate_no") Long estimate_no,
			@PathVariable("decision") String decision_text) {

		try {

			// 파라미터 확인. 견적 의뢰 ID
			if (estimate_no == null || estimate_no <= 0){
				log.error("[PARTNER-ASSIGN-DECISION] ESTIMATE_NO["+estimate_no+"] 필수 요청 항목 누락 또는 오류.");
				return makeResponseEntity(request, "Invalid path-variables ["+estimate_no+"]", HttpStatus.BAD_REQUEST);
			}

			// 파라미터 확인. 수락 또는 거절 여부
			if (StringUtils.isEmpty(decision_text) || (!decision_text.equalsIgnoreCase("accept") && !decision_text.equalsIgnoreCase("reject"))){
				log.error("[PARTNER-ASSIGN-DECISION] DECISION["+decision_text+"] 필수 요청 항목 누락 또는 오류.");
				return makeResponseEntity(request, "Invalid path-variables ["+decision_text+"]. Must be [accept|reject].", HttpStatus.BAD_REQUEST);
			}
			AssignAcceptType decision = decision_text.equalsIgnoreCase("accept") ? AssignAcceptType.ACCEPT : AssignAcceptType.REJECT;

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 견적 할당 정보 및 견적 정보 조회
			PartnerAssign assign = partnerService.selectAssign(partner.getPartner_id(), estimate_no);

			// 견적 정보가 조회 되지 않을 경우
			if (assign == null || assign.getAccept_step() != AssignAcceptType.WAITING.value()) {
				log.debug("[PARTNER-ASSIGN-DECISION] ["+member.getMember_id()+"]님이 요청한 견적[ID="+estimate_no+"] 정보가 없거나 수락 대기상태가 아닙니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// DB 업데이트
			log.debug("[PARTNER-ASSIGN-DECISION] ["+member.getMember_id()+"]님에게 할당된 견적[ID="+estimate_no+"]을 ["+decision+"]상태로 변경합니다.");

			assign.setAccept_step(decision.value());
			partnerService.acceptAssign(assign);

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-ASSIGN-DECISION] 견적 할당 업데이트 오류. ["+decision_text+"]", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 파트너사의 견적 정보를 변경한다. 사진 첨부가 없는 경우
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/partner/assign/counsel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> assign_counsel(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@RequestBody PartnerAssignCounsel counsel) {

		try {

			if (counsel.getEstimate_no() == 0) {
				log.error("[PARTNER-PGOGRESS-COUNSEL] 파라미터 오류. [ESTIMATE_NO] 값이 없습니다.");
				return makeResponseEntity(request, "Missing parameter [estimate_no]", HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 견적 할당 정보 및 견적 정보 조회
			PartnerAssign assign = partnerService.selectAssign(partner.getPartner_id(), counsel.getEstimate_no());

			// 견적 정보가 조회 되지 않을 경우
			if (assign == null || assign.getAccept_step() != AssignAcceptType.ACCEPT.value()) {
				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님이 요청한 견적[ID="+counsel.getEstimate_no()+"] 정보가 없거나 진행중 상태가 아닙니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			AssignStatus status = AssignStatus.valueOf(counsel.getStatus());

			switch (status) {

				// 상담 예정
				case SCHEDULE_COUNSELING:
				{
					return counsel_schedule(request, member, assign, counsel);
				}

				// 상담 완료
				case COMPLETE_COUNSELING:
				{

					return counsel_complete(request, member, assign, counsel);
				}

				// 견적 발송
				case PUBLISH_ESTIMATE:
				{
					return counsel_publish_estimate(request, member, assign, counsel);
				}

				// 계약 완료
				case COMPLETE_CONTRACT:
				{
					return counsel_complete_contract(request, member, assign, counsel, partner);
				}

				// 신규 할당
				default:
				{
					break;
				}
			}

			return makeResponseEntity(request, "", HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] 견적 진행현황 상세 설정 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 파트너사의 견적 정보를 변경한다. 사진 첨부가 없는 경우
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/partner/assign/counsel", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> counsel_complete_contract(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
												 @RequestHeader(value = "ZID", required = true) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
												 @RequestParam(value = "meta-data", required = false) String metaData,
												 @RequestPart(value = "image", required = false) List<MultipartFile> images,
 												 @RequestPart(value = "thumbnail", required = false) List<MultipartFile> thumbnails,
												 @RequestPart(value = "attached_file", required = false) List<MultipartFile> attached_files) {

//												 @RequestBody PartnerAssignCounsel counsel) {

		try {
			log.debug(request.getParameter("meta-data"));
			PartnerAssignCounsel counsel = Json.toObjectJson(metaData, PartnerAssignCounsel.class);
			if (counsel==null || counsel.getEstimate_no() == 0) {
				log.error("[PARTNER-PGOGRESS-COUNSEL] 파라미터 오류. [ESTIMATE_NO] 값이 없습니다.");
				return makeResponseEntity(request, "Missing parameter [estimate_no]", HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 견적 할당 정보 및 견적 정보 조회
			PartnerAssign assign = partnerService.selectAssign(partner.getPartner_id(), counsel.getEstimate_no());

			// 견적 정보가 조회 되지 않을 경우
			if (assign == null || assign.getAccept_step() != AssignAcceptType.ACCEPT.value()) {
				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님이 요청한 견적[ID="+counsel.getEstimate_no()+"] 정보가 없거나 진행중 상태가 아닙니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			AssignStatus status = AssignStatus.valueOf(counsel.getStatus());

			switch (status) {

				// 상담 예정
				case SCHEDULE_COUNSELING:
				{
					return counsel_schedule(request, member, assign, counsel);
				}

				// 상담 완료
				case COMPLETE_COUNSELING:
				{

					return counsel_complete(request, member, assign, counsel);
				}

				// 견적 발송
				case PUBLISH_ESTIMATE:
				{
					return counsel_publish_estimate(request, member, assign, counsel);
				}

				// 계약 완료
				case COMPLETE_CONTRACT:
				{
					return counsel_complete_contract(request, member, assign, counsel, partner, images, thumbnails, attached_files);
				}

				// 신규 할당
				default:
				{
					break;
				}
			}

			return makeResponseEntity(request, "", HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] 견적 진행현황 상세 설정 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 계약 정보 수정(사진 첨부 안된 경우)
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @param modify
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/partner/contract/modify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> contract_modify(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@RequestBody PartnerContractModify modify) {

		try {
			return contractModify(request, member_no, zid, zid_key, modify, null, null, null);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-CONTRACT-MODIFY] 견적 정보 업데이트 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 계약 정보 수정(파일 첨부 된 경우)
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @param metaData
	 * @param images
     * @param thumbnails
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/partner/contract/modify", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> contract_modify(HttpServletRequest request,
												  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
												  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
												  @RequestHeader(value = "ZID", required = true) Long zid,
												  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
												  @RequestParam(value = "meta-data", required = false) String metaData,
												  @RequestPart(value = "image", required = false) List<MultipartFile> images,
												  @RequestPart(value = "thumbnail", required = false) List<MultipartFile> thumbnails,
												  @RequestPart(value = "attached_file", required = false) List<MultipartFile> attached_files) {
		try {
			log.debug(request.getParameter("meta-data"));
			PartnerContractModify modify = Json.toObjectJson(metaData, PartnerContractModify.class);
			return contractModify(request, member_no, zid, zid_key, modify, images, thumbnails, attached_files);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-CONTRACT-MODIFY] 견적 정보 업데이트 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private ResponseEntity<String> contractModify(HttpServletRequest request,
												  long member_no,
												  long zid,
												  String zid_key,
												  PartnerContractModify modify,
												  List<MultipartFile> images,
												  List<MultipartFile> thumbnails,
												  List<MultipartFile> attached_files){
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 파라미터 체크
			if (modify == null || modify.getContract_no() == null) {
				log.error("[PARTNER-CONTRACT-MODIFY] ["+member.getMember_id()+"] 필수 항목 오류. 변경할 계약 정보가 없습니다.");
				return makeResponseEntity(request, "Invalid request body.", HttpStatus.BAD_REQUEST);
			}

			// 견적 조회
			PartnerContract contract = partnerService.selectContract(modify.getContract_no());

			// 계약정보가 검색되지 않을 경우
			if (contract == null) {
				log.error("[PARTNER-CONTRACT-MODIFY] ["+member.getMember_id()+"] 요청한 계약번호["+modify.getContract_no()+"]는 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// 계약 정보의 파트너가 요청한 가입자의 파트너와 다른 경우
			if (member.getPartner_id() != contract.getPartner_id()) {
				log.error("[PARTNER-CONTRACT-MODIFY] ["+member.getMember_id()+"] 파트너ID["+member.getPartner_id()+"]. 요청한 계약번호["+modify.getContract_no()+"]는 다른 파트너["+contract.getPartner_id()+"]의 계약입니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// DB 업데이트
			log.debug("[PARTNER-CONTRACT-MODIFY] ["+member.getMember_id()+"]님의 계약 정보를 업데이트 합니다.");

			modify.setCommission_rate(contract.getCommission_rate());
			modify.calculateCommition();

			partnerService.updateContract_app(modify);

			String upload_path = static_path;
			if (!upload_path.endsWith("/")) { upload_path += "/"; }

			//첨부사진 삭제 처리
			if(modify.getDelete_files()!=null && modify.getDelete_files().size() > 0){
				for(long file_no : modify.getDelete_files()){
					ContractFile contractFile = contractService.selectContractFile(file_no);
					try{
						/*
							s3 로 변경
							 */
						FileUploadUtil.getInstance().deleteFromS3(contractFile.getFile_path(), contractFile.getThumbnail());
						FileUploadUtil.getInstance().deleteFromS3(contractFile.getFile_path(), contractFile.getFile_name());

//						if(!StringUtils.isEmpty(contractFile.getThumbnail())){
//							File file = new File(upload_path + contractFile.getFile_path() + contractFile.getThumbnail());
//							if(file.exists() && file.isFile()){
//								FileUtils.forceDelete(file);
//							}
//						}
//						if(!StringUtils.isEmpty(contractFile.getFile_name())){
//							File file = new File(upload_path + contractFile.getFile_path() + contractFile.getFile_name());
//							if(file.exists() && file.isFile()){
//								FileUtils.forceDelete(file);
//							}
//						}

						contractService.deleteContractFile(file_no);
					} catch (Exception e){
						log.error("[CONTRACT-MODIFY] ["+member.getMember_id()+"]. 파일 삭제 오류 파일번호 ["+file_no+"]. ("+e.getMessage()+")");
					}
				}
			}

			treatContractFileUpload(contract, attached_files, member);

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-CONTRACT-MODIFY] 견적 정보 업데이트 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/contract/start/{contract_no}", method = RequestMethod.POST)
	public ResponseEntity<String> contract_start(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("contract_no") long contract_no) {
		return contract_start(request, app_version, member_no, zid, zid_key, contract_no, 0);
	}

	@ResponseBody
	@RequestMapping(value = "/partner/contract/start/{contract_no}/{date}", method = RequestMethod.POST)
	public ResponseEntity<String> contract_start(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("contract_no") long contract_no,
			@PathVariable("date") long event_date) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 견적 조회
			PartnerContract contract = partnerService.selectContract(contract_no);

			// 계약정보가 검색되지 않을 경우
			if (contract == null) {
				log.error("[PARTNER-CONTRACT-START] ["+member.getMember_id()+"] 요청한 계약번호["+contract_no+"]는 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// 계약 정보의 파트너가 요청한 가입자의 파트너와 다른 경우
			if (member.getPartner_id() != contract.getPartner_id()) {
				log.error("[PARTNER-CONTRACT-START] ["+member.getMember_id()+"] 파트너ID["+member.getPartner_id()+"]. 요청한 계약번호["+contract_no+"]는 다른 파트너["+contract.getPartner_id()+"]의 계약입니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// 공사 시작 가능 상태가 아닌 경우
			if ((contract.getContract_status() != ContractStatus.COMPLETE_CONTRACT.value()) // 계약 완료 상태에서만 공사 시작 가능
				|| (contract.getCompleted_flag() != 0))		// 계약이 진행중인 상태에서만 공사 시작 가능
				//|| (contract.getAgreement_status() != 3))	// TODO: 협의완료 상태는 추후 협의. 협의 완료 상태에서만 공사 시작 가능
			{
				log.error("[PARTNER-CONTRACT-START] ["+member.getMember_id()+"] 요청한 계약["+contract_no+"]은 공사를 시작할 수 없는 상태입니다. ["+
						ContractStatus.valueOf(contract.getContract_status())+"|종료여부="+contract.getCompleted_code()+"|협의상태="+contract.getAgreement_status()+"]");
				return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
			}

			// DB 업데이트
			log.debug("[PARTNER-CONTRACT-START] ["+member.getMember_id()+"]님의 계약 정보를 [공사시작] 상태로 업데이트 합니다.");
			partnerService.updateContract_start(contract_no, contract.getEstimate_no(), (event_date <= 0 ? System.currentTimeMillis() : event_date));

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-CONTRACT-START] 공사 시작 업데이트 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/contract/stop/{contract_no}", method = RequestMethod.POST)
	public ResponseEntity<String> contract_stop(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("contract_no") long contract_no) {
		return contract_stop(request, app_version, member_no, zid, zid_key, contract_no, 0);
	}

	@ResponseBody
	@RequestMapping(value = "/partner/contract/stop/{contract_no}/{date}", method = RequestMethod.POST)
	public ResponseEntity<String> contract_stop(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("contract_no") long contract_no,
			@PathVariable("date") long event_date) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 견적 조회
			PartnerContract contract = partnerService.selectContract(contract_no);

			// 계약정보가 검색되지 않을 경우
			if (contract == null) {
				log.error("[PARTNER-CONTRACT-STOP] ["+member.getMember_id()+"] 요청한 계약번호["+contract_no+"]는 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// 계약 정보의 파트너가 요청한 가입자의 파트너와 다른 경우
			if (member.getPartner_id() != contract.getPartner_id()) {
				log.error("[PARTNER-CONTRACT-STOP] ["+member.getMember_id()+"] 파트너ID["+member.getPartner_id()+"]. 요청한 계약번호["+contract_no+"]는 다른 파트너["+contract.getPartner_id()+"]의 계약입니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// 공사 시작 가능 상태가 아닌 경우
			if ((contract.getContract_status() != ContractStatus.CONSTRUCT_START.value()) // 공사 진행중인 상태에서만 공사 완료 가능
				|| (contract.getCompleted_flag() != 0))		// 계약이 진행중인 상태에서만 공사 완료 가능
			{
				log.error("[PARTNER-CONTRACT-STOP] ["+member.getMember_id()+"] 요청한 계약["+contract_no+"]은 공사를 완료 할 수 없는 상태입니다. ["+
						ContractStatus.valueOf(contract.getContract_status())+"|종료여부="+contract.getCompleted_code()+"]");
				return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
			}

			// DB 업데이트
			log.debug("[PARTNER-CONTRACT-STOP] ["+member.getMember_id()+"]님의 계약 정보를 [공사완료] 상태로 업데이트 합니다.");
			partnerService.updateContract_stop(contract_no, contract.getEstimate_no(), (event_date <= 0 ? System.currentTimeMillis() : event_date));

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-CONTRACT-START] 공사 완료 업데이트 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/assign/restore/{estimate_no}", method = RequestMethod.POST)
	public ResponseEntity<String> assign_restore(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("estimate_no") Long estimate_no) {

		try {

			// 파라미터 확인. 견적 의뢰 ID
			if (estimate_no == null || estimate_no <= 0){
				log.error("[PARTNER-ASSIGN-RESTORE] ESTIMATE_NO["+estimate_no+"] 필수 요청 항목 누락 또는 오류.");
				return makeResponseEntity(request, "Invalid path-variables ["+estimate_no+"]", HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 견적 조회
			PartnerAssign assign = partnerService.selectAssignDetail(partner.getPartner_id(), estimate_no);

			// 견적 정보가 조회 되지 않을 경우
			if (assign == null) {
				log.debug("[PARTNER-ASSIGN-RESTORE] ["+member.getMember_id()+"]님이 요청한 견적[ID="+estimate_no+"]이 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// 견적 진행 상태가 중단 상태인지 확인
			if (assign.getStop_code() == null || assign.getStop_code() < 1) {
				log.debug("[PARTNER-ASSIGN-RESTORE] ["+member.getMember_id()+"]님이 요청한 견적[ID="+estimate_no+"]은 중단된 상태의 견적이 아닙니다.");
				return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
			}

			// 다른 파트너에 의해 공사가 계약된 경우
			if (assign.getStop_code() == AssignStopCode.ALREADY_CONTRACT.value()) {
				log.debug("[PARTNER-ASSIGN-RESTORE] ["+member.getMember_id()+"]님이 요청한 견적[ID="+estimate_no+"]은 다른 파트너와 계약이 완료된 상태입니다.");
				return makeResponseEntity(HttpStatus.PRECONDITION_FAILED);
			}

			// 계약 내역 조회
			if (assign.getAssign_status() == AssignStatus.COMPLETE_CONTRACT.value()) {
				assign.setContract(partnerService.selectContract_estimateNo(partner.getPartner_id(),estimate_no));
			}

			// DB: 계약 정보에서 중단 정보 제거
			if (assign.getContract() != null && assign.getContract().getContract_no() > 0) {
				partnerService.restoreContract(assign.getContract().getContract_no());
			}

			// DB: 견적 정보에서 중단 정보 제거
			partnerService.restoreAssign(partner.getPartner_id(), assign.getEstimate_no());

			log.debug("[PARTNER-ASSIGN-RESTORE] ["+member.getMember_id()+"]님에게 할당된 견적[ID="+estimate_no+"]을 다시 진행합니다.");

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-ASSIGN-RESTORE] 중단 견적 재진행 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@ResponseBody
	@RequestMapping(value = "/partner/assign/memo", method = RequestMethod.POST)
	public ResponseEntity<String> assignMemo(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
												 @RequestHeader(value = "ZID", required = true) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
											     @RequestBody PartnerAssign requestAssign
											  ) {

		try {

			// 파라미터 확인. 견적 의뢰 ID
			if (requestAssign.getEstimate_no() <= 0){
				log.error("[PARTNER-ASSIGN-MEMO] ESTIMATE_NO["+requestAssign.getEstimate_no()+"] 필수 요청 항목 누락 또는 오류.");
				return makeResponseEntity(request, "Invalid path-variables ["+requestAssign.getEstimate_no()+"]", HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 견적 조회
			PartnerAssign assign = partnerService.selectAssignDetail(partner.getPartner_id(), requestAssign.getEstimate_no());

			// 견적 정보가 조회 되지 않을 경우
			if (assign == null) {
				log.debug("[PARTNER-ASSIGN-MEMO] ["+member.getMember_id()+"]님이 요청한 견적[ID="+requestAssign.getEstimate_no()+"]이 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// 견적 할당 정보의 메모 수정
			requestAssign.setPartner_id(partner.getPartner_id());

			String memo = "";
			if(StringUtils.isNotEmpty(requestAssign.getMemo())){
				if(StringUtils.isNotEmpty(assign.getMemo())){
					memo = assign.getMemo() + "\r\n\r\n";
				}
				memo =  memo + "[" + DateUtil.toFormatString(new Date(), DateUtil.DATE_FORMAT_FOR_DISPLAY_MINUTE) +  "]\r\n" + requestAssign.getMemo();
				requestAssign.setMemo(memo);
				partnerService.updateAssignMemo(requestAssign);
			} else {
				memo = assign.getMemo();
			}
			return makeResponseEntity(request, requestAssign, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-ASSIGN-MEMO] 견적 메모 등록 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/assign/counsel/{estimate_no}/{status}", method = RequestMethod.POST)
	public ResponseEntity<String> counselCallEstimate(HttpServletRequest request,
											  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
											  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
											  @RequestHeader(value = "ZID", required = true) Long zid,
											  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
											  @PathVariable("estimate_no") Long estimate_no,
											  @PathVariable("status") Integer status

	) {

		try {

			// 파라미터 확인. 견적 의뢰 ID
			if (estimate_no==null || estimate_no <= 0){
				log.error("[PARTNER-ASSIGN-STATUS] ESTIMATE_NO["+estimate_no+"] 필수 요청 항목 누락 또는 오류.");
				return makeResponseEntity(request, "Invalid path-variables ["+estimate_no+"]", HttpStatus.BAD_REQUEST);
			}

			if (status==null || status < 0 || status > 1){
				log.error("[PARTNER-ASSIGN-STATUS] STATUS["+status+"] 필수 요청 항목 누락 또는 오류.");
				return makeResponseEntity(request, "Invalid path-variables status ["+status+"]", HttpStatus.BAD_REQUEST);
			}


			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 견적 조회
			PartnerAssign assign = partnerService.selectAssignDetail(partner.getPartner_id(), estimate_no);

			// 견적 정보가 조회 되지 않을 경우
			if (assign == null) {
				log.debug("[PARTNER-ASSIGN-STATUS] ["+member.getMember_id()+"]님이 요청한 견적[ID="+estimate_no+"]이 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			log.debug("[PARTNER-ASSIGN-STATUS] ["+member.getMember_id()+"]님에게 할당된 견적["+estimate_no
					+"]의 전화상담 상태를 업데이트 합니다. ["+(status == 1 ? "통화완료" : "통화부재")+"]");

			//견적 할당 정보의 1차 통화상담 여부
			CallEstimate callEstimate = new CallEstimate();
			callEstimate.setEstimate_no(estimate_no);
			callEstimate.setCreater(member_no);
			callEstimate.setStatus(status);
			callEstimate.setPartner_id(partner.getPartner_id());

			partnerService.insertCallCounselStatus(callEstimate);

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-ASSIGN-STATUS] 통화상담 설정 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@ResponseBody
	@RequestMapping(value = "/partner/assign/retry_call/{estimate_no}/{retry_call}", method = RequestMethod.GET)
	public ResponseEntity<String> assignRetryCall(HttpServletRequest request,
											  @RequestHeader(value = "APP-VERSION", required = true) String app_version,
											  @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
											  @RequestHeader(value = "ZID", required = true) Long zid,
											  @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
											  @PathVariable("estimate_no") Long estimate_no,
											  @PathVariable("retry_call") Integer retry_call

	) {

		try {

			// 파라미터 확인. 견적 의뢰 ID
			if (estimate_no==null || estimate_no <= 0){
				log.error("[PARTNER-ASSIGN-RETRY-CALL] ESTIMATE_NO["+estimate_no+"] 필수 요청 항목 누락 또는 오류.");
				return makeResponseEntity(request, "Invalid path-variables ["+estimate_no+"]", HttpStatus.BAD_REQUEST);
			}

			if (retry_call==null || retry_call < 0 || retry_call > 1){
				log.error("[PARTNER-ASSIGN-RETRY-CALL] RETRY_CALL["+retry_call+"] 필수 요청 항목 누락 또는 오류.");
				return makeResponseEntity(request, "Invalid path-variables retry_call ["+retry_call+"]", HttpStatus.BAD_REQUEST);
			}


			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 견적 조회
			PartnerAssign assign = partnerService.selectAssignDetail(partner.getPartner_id(), estimate_no);

			// 견적 정보가 조회 되지 않을 경우
			if (assign == null) {
				log.debug("[PARTNER-ASSIGN-RETRY-CALL] ["+member.getMember_id()+"]님이 요청한 견적[ID="+estimate_no+"]이 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.GONE);
			}

			// 견적 할당 정보의 메모 수정
			assign.setRetry_call(retry_call);
			assign.setPartner_id(partner.getPartner_id());
			partnerService.updateAssignRetryCall(assign);

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-ASSIGN-RETRY-CALL] 견적 재통화 설정 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



	@ResponseBody
	@RequestMapping(value = "/partner/inquiry/{estimate_no}/{page}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<String> inquiry_download(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("estimate_no") long estimate_no,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 문의 내역 조회
			List<PartnerInquiry> list = partnerService.selectInquiry(member.getPartner_id(), estimate_no, page, limit);

			// 다운로드 URL 확인
			String upload_path = static_path;
			if (!upload_path.endsWith("/")) { upload_path += "/"; }

			String download_url = static_download_url;
			if (!download_url.endsWith("/")) { download_url += "/"; }

			//  이미지 설정 확인
			for (int i = 0; list != null && i < list.size(); i++) {

				PartnerInquiry inquiry = list.get(i);

				// 히스토리 파일 조회
				List<PartnerInquiryFile> images = partnerService.selectInquiryFile(inquiry.getCno());

				for (int j = 0; images != null && j < images.size(); j++) {
					PartnerInquiryFile image = images.get(j);
					image.setImage(static_download_url+image.getFile_path()+image.getFile_name());
					if (image.getThumb_name() != null) { image.setThumbnail(static_download_url+image.getFile_path()+image.getThumb_name()); }
				}

				if (images != null && images.size() > 0) { inquiry.setImages(images); }

				// 답변이 없는 경우 UPDATE_DATE를 초기화
				if (inquiry.getReply() == null) { inquiry.setUpdate_date(null); }
			}

			return makeResponseEntity(request, list, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-INQUIRY-DOWNLOAD] 문의 내역 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@ResponseBody
	@RequestMapping(value = "/partner/inquiry/{estimate_no}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<String> inquiryUpload(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
												 @RequestHeader(value = "ZID", required = true) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
												 @PathVariable("estimate_no") long estimate_no,
												 @RequestParam("meta-data") String metaData) {
		return treatInquiryUpload(request, app_version, member_no, zid, zid_key, estimate_no, metaData, null, null);
	}

	@ResponseBody
	@RequestMapping(value = "/partner/inquiry/{estimate_no}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> inquiryUpload(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = true) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
												 @RequestHeader(value = "ZID", required = true) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
												 @PathVariable("estimate_no") long estimate_no,
												 @RequestParam(value = "meta-data", required = false) String metaData,
												 @RequestPart(value = "image", required = false) List<MultipartFile> images,
												 @RequestPart(value = "thumbnail", required = false) List<MultipartFile> thumbnails) {
		return treatInquiryUpload(request, app_version, member_no, zid, zid_key, estimate_no, metaData, images, thumbnails);
	}

	private ResponseEntity<String> treatInquiryUpload(HttpServletRequest request,
			String app_version,
			Long member_no,
			Long zid,
			String zid_key,
			long estimate_no,
			String metaData,
			List<MultipartFile> images,
			List<MultipartFile> thumbnails) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 업로드 파일 개수 체크. 이미지와 썸네일의 갯수가 일치하여야 한다.
			if ((images == null && thumbnails != null) ||
				(images != null && thumbnails == null) ||
				(images != null && images.size() != thumbnails.size())) {
				log.error("[PARTNER-INQUIRY-UPLOAD] ["+member.getMember_id()+"]이 업로드한 협의확인서 원본사진과 썸네일의 사진 수가 일치하지 않습니다.");
				return makeResponseEntity(request, "The number of images and thumbnails is different.", HttpStatus.BAD_REQUEST);
			}

			log.debug("[PARTNER-INQUIRY-UPLOAD] ["+member.getMember_id()+"]. 문의 내용 ["+metaData+"] 사진개수="+(images == null ? 0 : images.size())+" 썸네일="+(thumbnails == null ? 0 : thumbnails.size()));

			Estimate estimate = partnerService.selectEstimate(estimate_no);
			if(estimate==null){
				log.error("[PARTNER-INQUIRY-UPLOAD] 견적정보["+estimate_no+"]가 존재하지 않습니다.");
				return makeResponseEntity(HttpStatus.NOT_FOUND);
			}


			// 문의 내용 생성
			PartnerInquiry inquiry = new PartnerInquiry();
			inquiry.setContents(metaData);
			inquiry.setEstimate_no(estimate_no);
			inquiry.setMember_no(member_no);
			inquiry.setPartner_id(member.getPartner_id());
			inquiry.setSubject(estimate.getSubject());
			inquiry.setUpdate_telephone(member.getMobile_no());

			// DB: 문의 내용 등록
			partnerService.createInquiry(inquiry);

			String upload_path = static_path;
			if (!upload_path.endsWith("/")) { upload_path += "/"; }

			final String relativeDirPath = "inquiry/" + member_no + "/image_";
			final String relativeDirThumPath ="inquiry/" + member_no + "/thumb_";

			// 파일 업로드 처리
			for (int i = 0; images != null && i < images.size(); i++) {

				MultipartFile image = images.get(i);
				MultipartFile thumb = thumbnails.get(i);

				try {

					final String imageFileName = relativeDirPath + inquiry.getCno() + "_" + i;
					final String imageThumFileName = relativeDirThumPath + inquiry.getCno() + "_" + i;

					FileInfo imageFile = new FileInfo(image, static_path + imageFileName );
					FileInfo thumbFile = new FileInfo(thumb, static_path + imageThumFileName);

					// 파일 저장
					try {

						// 폴더 생성
						imageFile.getFile().getParentFile().mkdirs();

						// 파일 저장
						image.transferTo(imageFile.getFile());
						thumb.transferTo(thumbFile.getFile());

						FileUploadUtil.getInstance().uploadToS3(imageFile.getFile(),  imageFileName + "." + imageFile.getExtension());
						FileUploadUtil.getInstance().uploadToS3(thumbFile.getFile(),  imageThumFileName+ "." + imageFile.getExtension());

						// DB 저장
						partnerService.createInquiryFile(
										new PartnerInquiryFile(
												inquiry.getCno(),
												imageFile.getOriginFilename(),
												imageFile.getFile().getName(),
												thumbFile.getFile().getName(),
												"inquiry/" + member_no + "/"));

						FileUploadUtil.getInstance().deleteFile(imageFile.getFile());
						FileUploadUtil.getInstance().deleteFile(thumbFile.getFile());

					} catch (Exception e) {
						log.error("[PARTNER-INQUIRY-UPLOAD] 파일 저장 오류. ["+imageFile.getFile().getAbsolutePath()+","+thumbFile.getFile().getAbsolutePath()+"]", e);
						continue;
					}

				} catch (Exception e) {
					log.error("[PARTNER-INQUIRY-UPLOAD] ["+member.getMember_id()+"]. 업로드 파일 ["+image.getOriginalFilename()+","+thumb.getOriginalFilename()+"] 오류. ("+e.getMessage()+")");
					return makeResponseEntity(request, e.getMessage(), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
				}
			}

			return makeResponseEntity(HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-INQUIRY-UPLOAD] 문의 내역 등록 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/stats/{page}/{limit}", method = RequestMethod.GET)
	public ResponseEntity<String> stats(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key,
			@PathVariable("page") int page,
			@PathVariable("limit") int limit) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			PartnerStats result = new PartnerStats();

			if (page == 0) {

				// 상품 조회
				PartnerProduct product = partnerService.selectProduct(partner.getContract_code());

				// 계약 정보 조회
				PartnerContractSummary summary = partnerService.summaryContract(member.getPartner_id());

				// 파트너 명
				result.setPartner_name(partner.getPartner_name());

				if (summary != null) {

					// 누적 매출 건수
					result.setTotal_contracts(summary.getTotal_count());

					// 누적 매출액
					result.setTotal_sales(summary.getTotal_price());
				}

				// 계약된 상품 정보
				if (product != null) {

					// 가입 상품 명
					result.setProduct_name(product.getProduct_name());

					// 월 할당 대상 건수
					result.setService_cnt(product.getTotal_cnt());
				}

				// 당월 견적 할당 수
				result.setAssign_cnt(partnerService.getAssignCount(member.getPartner_id()));

				// 당월 주거공간 할당 수
				result.setResidential_cnt(partnerService.getAssignResidentialCount(member.getPartner_id()));

				// 당월 상업공간 할당 수
				result.setCommercial_cnt(partnerService.getAssignCommercialCount(member.getPartner_id()));

				// 당월 무료견적 할당 건수
				result.setFree_cnt(partnerService.getAssignFreeCount(member.getPartner_id()));
			}

			// 제공된 견적 리스트
			List<PartnerAssign> list = partnerService.selectAssignList(member.getPartner_id(), page, limit);

			for (int i = 0; list != null && i < list.size(); i++) {
				PartnerAssign assign = list.get(i);
				if (assign.getAssign_status() == AssignStatus.COMPLETE_CONTRACT.value()) {
					assign.setContract(partnerService.selectContractOngoing(partner.getPartner_id(), assign.getEstimate_no()));
				}
			}
			result.setList(list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-STATS] 파트너 통계 정보 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partner/charge", method = RequestMethod.GET)
	public ResponseEntity<String> charge(HttpServletRequest request,
			@RequestHeader(value = "APP-VERSION", required = true) String app_version,
			@RequestHeader(value = "MEMBER-NO", required = true) Long member_no,
			@RequestHeader(value = "ZID", required = true) Long zid,
			@RequestHeader(value = "ZID-KEY", required = true) String zid_key) {

		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			// 파트너 업체 정보 조회
			Partner partner = authenticationHelper.findPartner(member.getPartner_id());

			// 계약일로부터 납부기한 일자
			int expiration_days = 3;

			PartnerCharge result = new PartnerCharge();

			// 상품 조회
			PartnerProduct product = partnerService.selectProduct(partner.getContract_code());

			// 월회비
			result.setMonth_fee(product.getMonth_fee());

			// 서비스 비용 (소수점은 올림으로 계산)
			result.setCost((int)Math.ceil((result.getMonth_fee() / (double)1.1)));

			// 부가세
			result.setVat(result.getMonth_fee() - result.getCost());

			// 미납 건수 및 총액 조회
			PartnerContractSummary summary = partnerService.summaryUnpaidContract(partner.getPartner_id(), expiration_days);

			// 미납 건수
			result.setUnpaid_count((int)summary.getTotal_count());

			// 미납 총액
			result.setUnpaid_sum((int)summary.getTotal_price());

			// 미납 기간
			result.setExpiration_days(expiration_days);

			// 미납 내역 조회
			List<PartnerContract> list = partnerService.selectUnpaidContract(partner.getPartner_id(), expiration_days);

			Calendar cal = Calendar.getInstance();

			// 각 내역별로 계약일 3일 후로 납부기한 설정
			for (int i = 0; list != null && i < list.size(); i++) {

				PartnerContract contract = list.get(i);

				cal.setTime(new Date(contract.getContract_date()));
				cal.add(Calendar.DAY_OF_MONTH, expiration_days);

				// 납부 기한 일자 (계약 일자 + expiration_days)
				contract.setExpiration_date(cal.getTimeInMillis());
			}

			// 미납 내역 설정
			result.setUnpaid(list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
			log.error("[PARTNER-CHARGE] 파트너 비용관리 조회 오류.", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 신규 할당 후 상담 1단계 설문 완료 - 상담예약
	 * @param counsel
	 */
	private ResponseEntity<String> counsel_schedule(HttpServletRequest request, Member member, PartnerAssign assign, PartnerAssignCounsel counsel) {

		// 상담 예약인 경우 견적 할당 상태가 신규할당(0) 상태이어야 한다.
		if (assign.getAssign_status() != AssignStatus.ASSIGNED.value()) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]은 신규가 아닙니다. 현재상태["+AssignStatus.valueOf(assign.getAssign_status())+"]");
			return makeResponseEntity(HttpStatus.GONE);
		}

		// 계약이 중단되 경우
		if (assign.getCompleted_flag() > 0) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]은 이미 종료되었습니다.");
			return makeResponseEntity(HttpStatus.GONE);
		}

		AssignCounselType counselType = AssignCounselType.valueOf(counsel.getCounsel_type());

		switch (counselType) {

			// 방문 견적 상담
			case VISIT:
			{
				long visit_date = counsel.getVisit_date();

				// 방문 견적 상담정보에서 방문 예정일이 포함되어 있지 않은 경우
				if (visit_date == 0) {
					log.error("[PARTNER-PGOGRESS-COUNSEL] 방문견적 상담 예정일[visit_date] 이 없습니다.");
					return makeResponseEntity(request, "Missing parameter [visit_date]", HttpStatus.BAD_REQUEST);
				}

				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"] 상태를 업데이트 합니다. ["+AssignStatus.SCHEDULE_COUNSELING+":"+AssignCounselType.VISIT+":"+date.format(new Date(visit_date))+"]");

				// DB 업데이트 (STATUS, COUNSEL_TYPE, SCHD_VISIT_DATE, UPDATE_DATE)
				partnerService.updateAssignWithCounsel(member.getPartner_id(), counsel);
				break;
			}

			// 비방문/서면/전화 견적 상담
			case NO_VISIT:
			{
				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"] 상태를 업데이트 합니다. ["+AssignStatus.SCHEDULE_COUNSELING+":"+AssignCounselType.NO_VISIT+"]");

				// DB 업데이트 (STATUS, COUNSEL_TYPE, UPDATE_DATE)
				partnerService.updateAssignWithCounsel(member.getPartner_id(), counsel);

				break;
			}

			// 견적 완료
			case ESTIMATE:
			{
				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"] 상태를 업데이트 합니다. ["+AssignStatus.COMPLETE_COUNSELING+"]");

				// 1차 통화시 견적까지 완료한 경우 상담 완료 일자를 위해 VISIT_DATE값을 업데이트한다.
				counsel.setVisit_date(System.currentTimeMillis());

				// DB 업데이트 (STATUS, VISIT_DATE, UPDATE_DATE)
				partnerService.updateAssignWithCounsel(member.getPartner_id(), counsel);

				break;
			}

			// 상담 중단
			default:
			{
				//아이폰에서 stop_code가 올라오지 않는 버그가 있어서 방어코드 적용함.
				if(counsel.getStop_code()==AssignStopCode.NO_CODE.value()){
					counsel.setStop_code(AssignStopCode.ETC.value());
				}

				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]을 철회합니다. 사유["+AssignStopCode.valueOf(counsel.getStop_code())+(counsel.getStop_code()==AssignStopCode.ETC.value() ? ":"+counsel.getStop_reason() : "")+"]");

				// DB 업데이트 (STATUS, COMPLETE_FLAG, STOP_CODE, STOP_REASON, UPDATE_DATE)
				partnerService.updateAssignWithCounsel(member.getPartner_id(), counsel);

				break;
			}
		}

		return makeResponseEntity(HttpStatus.OK);
	}

	/**
	 * 상담 2단계 설문 완료 - 상담완료
	 * @param counsel
	 */
	private ResponseEntity<String> counsel_complete(HttpServletRequest request, Member member, PartnerAssign assign, PartnerAssignCounsel counsel) {

		// 상담 완료인 경우 견적 할당 상태가 상담예정(1) 상태이어야 한다.
		if (assign.getAssign_status() != AssignStatus.SCHEDULE_COUNSELING.value()) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]은 상담예약 상태가 아닙니다. 현재상태["+AssignStatus.valueOf(assign.getAssign_status())+"]");
			return makeResponseEntity(HttpStatus.GONE);
		}

		// 계약이 중단되 경우
		if (assign.getCompleted_flag() > 0) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]은 이미 종료되었습니다.");
			return makeResponseEntity(HttpStatus.GONE);
		}

		AssignCounselType counselType = AssignCounselType.valueOf(counsel.getCounsel_type());

		switch (counselType) {

			// 방문 견적 상담
			case VISIT:
			{
				long visit_date = counsel.getVisit_date();

				// 방문 견적 상담정보에서 방문 예정일이 포함되어 있지 않은 경우
				if (visit_date == 0) {
					log.error("[PARTNER-PGOGRESS-COUNSEL] 방문견적 상담 완료일[visit_date] 이 없습니다.");
					return makeResponseEntity(request, "Missing parameter [visit_date]", HttpStatus.BAD_REQUEST);
				}

				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"] 상태를 업데이트 합니다. ["+AssignStatus.COMPLETE_COUNSELING+":"+AssignCounselType.VISIT+":"+date.format(new Date(visit_date))+"]");

				// DB 업데이트 (STATUS, COUNSEL_TYPE, SCHD_VISIT_DATE, UPDATE_DATE)
				partnerService.updateAssignWithCounsel(member.getPartner_id(), counsel);
				break;
			}

			// 비방문/서면/전화 견적 상담
			case NO_VISIT:
			{
				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"] 상태를 업데이트 합니다. ["+AssignStatus.COMPLETE_COUNSELING+":"+AssignCounselType.NO_VISIT+"]");

				// DB 업데이트 (STATUS, COUNSEL_TYPE, UPDATE_DATE)
				partnerService.updateAssignWithCounsel(member.getPartner_id(), counsel);

				break;
			}

			// 견적 완료
			case ESTIMATE:
			{
				log.error("[PARTNER-PGOGRESS-COUNSEL] 상담완료 설문에서는 ["+counselType+"]이 값을 설정할 수 없습니다.");
				return makeResponseEntity(request, "Invalid parameter [counsel_type=3]", HttpStatus.BAD_REQUEST);
			}

			// 상담 중단
			default:
			{
				//아이폰에서 stop_code가 올라오지 않는 버그가 있어서 방어코드 적용함.
				if(counsel.getStop_code()==AssignStopCode.NO_CODE.value()){
					counsel.setStop_code(AssignStopCode.ETC.value());
				}

				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]을 철회합니다. 사유["+AssignStopCode.valueOf(counsel.getStop_code())+(counsel.getStop_code()==AssignStopCode.ETC.value() ? ":"+counsel.getStop_reason() : "")+"]");

				// DB 업데이트 (STATUS, COMPLETE_FLAG, STOP_CODE, STOP_REASON, UPDATE_DATE)
				partnerService.updateAssignWithCounsel(member.getPartner_id(), counsel);

				break;
			}
		}

		return makeResponseEntity(HttpStatus.OK);
	}

	/**
	 * 상담 3단계 설문 완료 - 견적발송
	 * @param counsel
	 */
	private ResponseEntity<String> counsel_publish_estimate(HttpServletRequest request, Member member, PartnerAssign assign, PartnerAssignCounsel counsel) {

		// 견적발송인 경우 견적 할당 상태가 상담완료(2) 상태이어야 한다.
		if (assign.getAssign_status() != AssignStatus.COMPLETE_COUNSELING.value()) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]은 상담완료 상태가 아닙니다. 현재상태["+AssignStatus.valueOf(assign.getAssign_status())+"]");
			return makeResponseEntity(HttpStatus.GONE);
		}

		// 계약이 중단되 경우
		if (assign.getCompleted_flag() > 0) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]은 이미 종료되었습니다.");
			return makeResponseEntity(HttpStatus.GONE);
		}

		// 중단 코드가 있는 경우
		if (counsel.getStop_code() > 0) {

			log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
					+"]을 철회합니다. 사유["+AssignStopCode.valueOf(counsel.getStop_code())+(counsel.getStop_code()==AssignStopCode.ETC.value() ? ":"+counsel.getStop_reason() : "")+"]");
		}

		else {

			// 고객에게 견적발송에 대한 답변을 받기로한 경우
			if (counsel.getFeedback_date() > 0) {
				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"] 상태를 업데이트 합니다. ["+AssignStatus.PUBLISH_ESTIMATE+":"+date.format(new Date(counsel.getFeedback_date()))+"]");
			}

			// 고객 답변 일정이 확정되지 않은 경우
			else {
				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"] 상태를 업데이트 합니다. ["+AssignStatus.PUBLISH_ESTIMATE+"]");
			}
		}

		// DB 업데이트 (견적 발송)
		partnerService.updateAssignWithCounsel(member.getPartner_id(), counsel);

		return makeResponseEntity(HttpStatus.OK);
	}


	private ResponseEntity<String> counsel_complete_contract(HttpServletRequest request, Member member, PartnerAssign assign, PartnerAssignCounsel counsel, Partner partner) {
		return this.counsel_complete_contract(request, member, assign, counsel, partner, null, null, null);
	}

	/**
	 * 상담 4단계 설문 완료 - 계약완료
	 * @param counsel
	 */
	private ResponseEntity<String> counsel_complete_contract(HttpServletRequest request,
															 Member member, PartnerAssign assign,
															 PartnerAssignCounsel counsel, Partner partner,
															 List<MultipartFile> images,
															 List<MultipartFile> thumbnails,
															 List<MultipartFile> attached_files) {

		// 계약완료인 경우 견적 할당 상태가 견적발송(3) 상태이어야 한다.
		if (assign.getAssign_status() != AssignStatus.PUBLISH_ESTIMATE.value()) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]은 견적발송 상태가 아닙니다. 현재상태["+AssignStatus.valueOf(assign.getAssign_status())+"]");
			return makeResponseEntity(HttpStatus.GONE);
		}

		// 계약이 중단되 경우
		if (assign.getCompleted_flag() > 0) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]은 이미 종료되었습니다.");
			return makeResponseEntity(HttpStatus.GONE);
		}

		// 견적 의뢰 정보 조회
		Estimate estimate = partnerService.selectEstimate(counsel.getEstimate_no());
		if (estimate == null) {
			log.error("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
					+"]이 검색되지 않습니다.");
			return makeResponseEntity(HttpStatus.GONE);
		}

		// 동일한 견적이 이미 계정 정보에 등록된 경우
		List<PartnerContract> list = partnerService.selectContractList_estimateNo(counsel.getEstimate_no());
		if (list != null && list.size() > 0) {

			PartnerContract aContract = list.get(0);
			Partner aPartner = partnerService.selectPartner(aContract.getPartner_id());
			log.error("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
					+"]은 다른 PARTNER["+aContract.getPartner_id()+":"+(aPartner != null ? aPartner.getPartner_name() : "")+"]에서 계약한 상태입니다.");

			return makeResponseEntity(HttpStatus.CONFLICT);
		}

		// 중단 코드가 있는 경우
		if (counsel.getStop_code() > 0) {

			log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
					+"]을 철회합니다. 사유["+AssignStopCode.valueOf(counsel.getStop_code())+(counsel.getStop_code()==AssignStopCode.ETC.value() ? ":"+counsel.getStop_reason() : "")+"]");

			// 견적할당 DB 업데이트 (계약완료)
			partnerService.updateAssignWithCounsel(member.getPartner_id(), counsel);

		} else {

			// 파라미터 체크
			if (counsel.getContract_price() == 0 ||
				counsel.getContract_date() == 0 ||
				counsel.getStart_date() == 0 ||
				counsel.getEnd_date() == 0) {

				log.error("[PARTNER-PGOGRESS-COUNSEL] 계약완료 설문에는 공사금액/계약일/시작일/완료일이 포함되어야 합니다. ["+
								counsel.getContract_price()+":"+counsel.getContract_date()+":"+counsel.getStart_date()+":"+counsel.getEnd_date()+"]");
				return makeResponseEntity(request, "Missing parameter. Check ! [contract_price,contract_date,start_date,end_date]", HttpStatus.BAD_REQUEST);
			}

			// 계약이 완료된 경우
			log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
					+"] 상태를 업데이트 합니다. ["+AssignStatus.COMPLETE_CONTRACT+":PRICE="+counsel.getEstimate_price()
					+":DATE="+date.format(new Date(counsel.getContract_date()))+","+date.format(new Date(counsel.getStart_date()))+","+date.format(new Date(counsel.getEnd_date()))
					+":STD="+counsel.getStd_contract_status()+"]");

			// 견적할당 DB 업데이트 (계약완료)
			partnerService.updateAssignWithCounsel(member.getPartner_id(), counsel);

			// 계약 정보 생성
			PartnerContract contract = new PartnerContract();

			contract.setContract_name(estimate.getSubject());
			contract.setContract_date(counsel.getContract_date());
			contract.setContract_price(counsel.getContract_price());
			contract.setContracter(estimate.getWriter());
			contract.setPhone_no(estimate.getPhone_no());
			contract.setEmail(estimate.getEmail());
			contract.setCategory_code1(estimate.getCategory_code1());
			contract.setCategory_code2(estimate.getCategory_code2());
			contract.setBudget(estimate.getBudget());
			contract.setSpace(estimate.getSpace());
			contract.setAddress(estimate.getAddress());
			contract.setAddress_detail(estimate.getAddress_detail());
			if (estimate.getUser_type() != null && estimate.getUser_type() == UserType.MEMBER.value()
					&& estimate.getUser_id() != null) {
				contract.setMember_no(estimate.getUser_id());
			}

			contract.setStart_date(counsel.getStart_date());
			contract.setExpect_end_date(counsel.getEnd_date());

			// 2018.05.03. KJB. 파트너별 수수료율 정책 적용
			contract.setCommission_rate(partner.getPartner_commission());

			// 수수료 계산
			contract.calculateCommition();

			// 계약 상태
			contract.setContract_status(ContractStatus.COMPLETE_CONTRACT.value());

			contract.setEstimate_no(counsel.getEstimate_no());
			contract.setPartner_id(member.getPartner_id());
			contract.setStd_contract_status(counsel.getStd_contract_status());
			contract.setCreater(member.getMember_no());
			contract.setCollection_status(0);
			contract.setInterior_product_code(estimate.getInterior_product_code());

			// 계약 정보 등록
			long rows = partnerService.createContract(contract);
			if (rows > 0) {
				log.debug("[PARTNER-PGOGRESS-COUNSEL] ["+member.getMember_id()+"]님에게 할당된 견적["+counsel.getEstimate_no()
						+"]으로 계약정보를 생성하였습니다. 계약ID["+contract.getContract_no()+"]");
			} else {
				log.error("[PARTNER-PGOGRESS-COUNSEL] 계약정보 생성 실패. ["+member.getMember_id()+"] / 견적["+counsel.getEstimate_no()	+"]");
			}

			// 견적 의뢰 테이블 상태 변경. 계약완료(3)
			partnerService.updateEstimate_contract(counsel.getEstimate_no()	, ContractStatus.COMPLETE_CONTRACT.value());

			// 동일 의뢰 견적이 할당된 내역을 조회하여 자동 중단 처리 및 PUSH 발송
			List<PartnerAssign> partnerAssigns = partnerService.selectAssignStopTargetList(counsel.getEstimate_no(), contract.getPartner_id());

			SendMessage sendMsg = new SendMessage();
			String notifyMsg = estimate.getWriter() +" 고객의 견적 의뢰건이 종료되었습니다.";
			sendMsg.setTitle(notifyMsg);
			sendMsg.setBody(estimate.getSubject());
			sendMsg.setEpid(estimate.getEstimate_no());
			sendMsg.setMsg_type(MsgType.E298.getCode());
			sendMsg.setTargetPartner(true);

			for (PartnerAssign otherAssign:partnerAssigns) {

				// 견적 진행중인 파트너에 알림 메시지 전송 및 진행 상태 변경
				if (otherAssign.getCompleted_flag()==0) {

					List<MemberDevice> targetList = sendMessageService.findListByPartnerId(otherAssign.getPartner_id());
					sendMsg.setTargetList(targetList);
					messageSender.sendPush(sendMsg);

					// 타 파트너사와 계약됨으로 종료 처리
					otherAssign.setCompleted_flag(1);
					otherAssign.setStop_code(AssignStopCode.ALREADY_CONTRACT.value());
					partnerService.updateAssignStop(otherAssign);
				}
			}

			treatContractFileUpload(contract, attached_files, member);
		}
		return makeResponseEntity(HttpStatus.OK);
	}

	private void treatContractFileUpload(PartnerContract contract, List<MultipartFile> attached_files, Member member){
		String upload_path = static_path;
		if (!upload_path.endsWith("/")) { upload_path += "/"; }

		final String file_path = "contract/" + contract.getContract_no() + "/";
		if(attached_files!=null && attached_files.size() > 0){
			for(MultipartFile multipartFile:attached_files){

				try {

					String file_name = DateUtil.toFormatString(DateUtil.DATE_FORMAT_FOR_MILLISECOND) + "_" + RandomStringUtils.randomAlphanumeric(10);
					String thumbnail = null;
					String file_type = FileType.IMAGE.value();

					FileInfo fileInfo = new FileInfo(multipartFile, upload_path + file_path + file_name);

					// 폴더 생성
					if(!fileInfo.getFile().getParentFile().exists()) {
						fileInfo.getFile().getParentFile().mkdirs();
					}

					//첨부파일이 이미지인 경우 썸네일을 추출한다.
					if(fileInfo.isImageFile()) {

						//원본 파일을 리사이징해서 저장
						ImageResizeUtil.imageResize(multipartFile.getInputStream(), fileInfo.getFile(), PictureType.XXXHDPI.getWidth(), PictureType.XXXHDPI.getHeight());
						//썸네일 파일 생성
						thumbnail = file_name + "_thumb." + fileInfo.getExtension();
						ImageResizeUtil.imageResize(fileInfo.getFile(), upload_path + file_path + thumbnail, PictureType.MDPI.getWidth(), PictureType.MDPI.getHeight());

					} else {
						// 파일 저장
						multipartFile.transferTo(fileInfo.getFile());
					}
					File destDir = new File(upload_path + file_path);
					FileUploadUtil.getInstance().uploadToS3(destDir, file_path );

					//DB저장을 위해 파일 확장자를 추가한다.
					file_name = file_name + "." + fileInfo.getExtension();
					ContractFile contractFile = new ContractFile(contract.getContract_no(), file_type, multipartFile.getOriginalFilename(), file_name, thumbnail, file_path, member.getMember_no());

					contractService.createContractFile(contractFile);

					treatContractFileHistoryUpload(multipartFile,contractFile);

				} catch (Exception e) {
					log.error("[CONTRACT-FILE-UPLOAD] 업로드 파일 ["+multipartFile.getOriginalFilename()+"] 오류.", e);
				}
			}
		}

		File destDir = new File(upload_path + file_path);
		FileUploadUtil.getInstance().uploadToS3(destDir, file_path );
	}


	private void treatContractFileHistoryUpload(MultipartFile multipartFile,ContractFile contractFile) {

		ContractFileHistory contractFileHistory = new ContractFileHistory();

		String upload_path = static_path;

		String file_path = "contract/" + contractFile.getContract_no() + "/history/";
		String file_name = DateUtil.toFormatString(DateUtil.DATE_FORMAT_FOR_MILLISECOND) + "_" + RandomStringUtils.randomAlphanumeric(10);
		String thumbnail = null;

		try {

			FileInfo fileInfo = new FileInfo(multipartFile, upload_path + file_path + file_name);

			// 폴더 생성
			if(!fileInfo.getFile().getParentFile().exists()) {
				fileInfo.getFile().getParentFile().mkdirs();
			}

			//첨부파일이 이미지인 경우 썸네일을 추출한다.
			if(fileInfo.isImageFile()) {

				//원본 파일을 리사이징해서 저장
				ImageResizeUtil.imageResize(multipartFile.getInputStream(), fileInfo.getFile(), PictureType.XXXHDPI.getWidth(), PictureType.XXXHDPI.getHeight());
				//썸네일 파일 생성
				thumbnail = file_name + "_thumb." + fileInfo.getExtension();
				ImageResizeUtil.imageResize(fileInfo.getFile(), upload_path + file_path + thumbnail, PictureType.MDPI.getWidth(), PictureType.MDPI.getHeight());
                FileUploadUtil.getInstance().uploadToS3(new File(static_path + file_path) , file_path );

			} else {
				// 파일 저장
				multipartFile.transferTo(fileInfo.getFile());
                File destDir = new File(upload_path + file_path);
                FileUploadUtil.getInstance().uploadToS3(destDir, file_path );
			}


			//DB저장을 위해 파일 확장자를 추가한다.
			file_name = file_name + "." + fileInfo.getExtension();

			BeanUtils.copyProperties(contractFile,contractFileHistory);
			contractFileHistory.setFile_name(file_name);
			contractFileHistory.setThumbnail(thumbnail);
			contractFileHistory.setFile_path(file_path);

			contractService.createContractHistoryFile(contractFileHistory);

		} catch (Exception e) {
			log.error("[CONTRACT-FILE-UPLOAD] 업로드 파일 ["+multipartFile.getOriginalFilename()+"] 오류. ("+e.getMessage()+")");
		}


	}


    /**
     * cms앱 요청
     * 파트너스 목록 조회
     * @param request
     * @param page
     * @param limit
     * @param searchCondition
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/partners/{page}/{limit}", method = RequestMethod.POST)
    public ResponseEntity<String> partner_list(HttpServletRequest request,
                                               @RequestHeader(value = "APP-VERSION", required = false) String app_version,
                                               @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
                                               @RequestHeader(value = "ZID", required = false) Long zid,
                                               @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
                                               @PathVariable("page") int page,
                                               @PathVariable("limit") int limit,
                                               @RequestBody(required = false) ListRequest searchCondition) {

	    try {

            if(searchCondition == null) {
                searchCondition = new ListRequest();
            }
            searchCondition.setPageLimit(page, limit);
	        log.info(searchCondition.toString());

	        int total_count = partnerService.selectListTotalCount(searchCondition);
	        if(total_count == 0) {
	        	log.debug("[/partners " + (searchCondition.getKeyword()) + "] " + page + "페이지 0건 조회 성공");
	            return makeResponseEntity(HttpStatus.NO_CONTENT);
            }

            ListResponse<PartnerFull> result = new ListResponse<>();
	        result.setTotal_count(total_count);
            result.setList(partnerService.selectList(searchCondition));

	        return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch(Exception e) {
            log.error("[/partners] " + page + "페이지 파트너스 목록 조회 실패", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	@ResponseBody
	@RequestMapping(value = "/partners/{partner_id}", method = RequestMethod.GET)
	public ResponseEntity<String> partner_detail(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												 @RequestHeader(value = "ZID", required = false) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												 @PathVariable("partner_id") int partner_id) {

		try {

			if(partner_id <= 0) {
				log.error("[/partners/0] - 400 파트너스 번호 정보 없음");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			PartnerFull result = partnerService.selectPartnerDetail(partner_id);
			if(result == null) {
				log.error("[/partners/"+partner_id+"] - 404 조회된 파트너스 없음");
				return makeResponseEntity(HttpStatus.NOT_FOUND);
			}

//			if (result.getStatus() == PartnerStatus.READY.getCode()
//					|| result.getStatus() == PartnerStatus.END.getCode()
//					|| result.getPartner_type() == PartnerType.UNAPPROVED.value()) {
//				log.error("[/partners/" + partner_id + "] 계약중이 아니거나 미승인 파트너");
//				return makeResponseEntity(HttpStatus.UNAUTHORIZED);
//			}

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e ) {
			log.error("[/partners/" + partner_id + "] 파트너스 상세 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/partners/stats", method = RequestMethod.POST)
	public ResponseEntity<String> partner_stats(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												 @RequestHeader(value = "ZID", required = false) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												 @RequestBody(required = false) ListRequest searchCondition) {
		try {

			if(searchCondition == null) {
				searchCondition = new ListRequest();
			}

			PartnerStatsResponse result = partnerService.selectPartnerStatsByArea(searchCondition);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e) {
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e ) {
			log.error("[/partners/stats " + (searchCondition != null ? searchCondition.getKeyword() : "") + "] 파트너스 통계 조회 실패", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

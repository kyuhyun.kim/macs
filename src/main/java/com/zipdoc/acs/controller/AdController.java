package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.agreement.AgreementStatus;
import com.zipdoc.acs.define.agreement.AgreementType;
import com.zipdoc.acs.persistence.dao.EstimateDao;
import com.zipdoc.acs.persistence.dao.MsgQueueDao;
import com.zipdoc.acs.utils.AcsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class AdController extends ResponseEntityService {

    private static final Logger log = LoggerFactory.getLogger(AdController.class);

    @Autowired
    private AcsConfig acsConfig;

    @Autowired
    private EstimateDao estimateDao;

    @Autowired
    private MsgQueueDao msgQueueDao;

    @ResponseBody
    @RequestMapping(value = "/{type}/{status}/{estimate_no}/")
    public ResponseEntity<String> agreement_estimate(HttpServletRequest request,
                                       @PathVariable("type") Integer type,
                                       @PathVariable("estimate_no") Long estimate_no,
                                       @PathVariable("status") Integer status) {


        try {



            if(type == null || status == null)
            {
                log.error("[AGREEMENT-ESTIMATE] 이벤트 유형 [{}] 및 상태 [{}] 정보가 유효하지 않습니다.", type, status);
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            // 이벤트 타입 확인
            AgreementType agreementType = AgreementType.get(type);
            if( agreementType == null)
            {
                log.error("[AGREEMENT-ESTIMATE] 이벤트 유형 [{}]이 유효하지 않습니다.", type);
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            // 이벤트 상태 처리 확인
            AgreementStatus agreementStatus = AgreementStatus.get(status);
            if( agreementStatus == null)
            {
                log.error("[AGREEMENT-ESTIMATE] 이벤트 상태가 [{}]이 유효하지 않습니다.", status);
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            // 상태 반영
            if(agreementType == AgreementType.이벤트약관)
            {
                Map<String, Object> map = new HashMap<>();
                map.put("estimate_no", estimate_no);
                map.put("status", status);
                estimateDao.updateAgreementEstimate(map);
            }

            if(agreementType == AgreementType.알림문자)
            {
                msgQueueDao.updateMsgQueueBlock(estimate_no);
            }


            return makeResponseEntity(HttpStatus.OK);

        } catch (Exception e) {
            log.error("[AGREEMENT-ESTIMATE] 이벤트 약관 수신거부 처리 오류.[{}]", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ResponseBody
    @RequestMapping(value = "/{type}/{estimate_no}/{status}.do")
    public ResponseEntity<String> ad(HttpServletRequest request,
                                                     @PathVariable("type") Integer type,
                                                     @PathVariable("estimate_no") Long estimate_no,
                                                     @PathVariable("status") Integer status) {


        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", acsConfig.getProperty("NIGNX.SERVER_URL") + acsConfig.getProperty("AD.ERROR.PAGE"));
        try {

            if(type == null || status == null)
            {
                log.error("[AGREEMENT-ESTIMATE] 이벤트 유형 [{}] 및 상태 [{}] 정보가 유효하지 않습니다.", type, status);
                return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);
            }

            // 이벤트 타입 확인
            AgreementType agreementType = AgreementType.get(type);
            if( agreementType == null)
            {
                log.error("[AGREEMENT-ESTIMATE] 이벤트 유형 [{}]이 유효하지 않습니다.", type);
                return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);

            }

            // 이벤트 상태 처리 확인
            AgreementStatus agreementStatus = AgreementStatus.get(status);
            if( agreementStatus == null)
            {
                log.error("[AGREEMENT-ESTIMATE] 이벤트 상태가 [{}]이 유효하지 않습니다.", status);
                return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);
            }

            // 상태 반영

            if(agreementType == AgreementType.이벤트약관)
            {
                Map<String, Object> map = new HashMap<>();
                map.put("estimate_no", estimate_no);
                map.put("status", status);
                estimateDao.updateAgreementEstimate(map);
            }

            if(agreementType == AgreementType.알림문자)
            {
                msgQueueDao.updateMsgQueueBlock(estimate_no);
            }



            headers.clear();
            headers.add("Location", acsConfig.getProperty("NIGNX.SERVER_URL") + acsConfig.getProperty("AD.SUCCESS.PAGE"));
            return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);


        } catch (Exception e) {
            log.error("[AGREEMENT-ESTIMATE] 이벤트 약관 수신거부 처리 오류.[{}]", e);
            headers.clear();
            headers.add("Location", acsConfig.getProperty("NIGNX.SERVER_URL") + acsConfig.getProperty("AD.ERROR.PAGE"));
            return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);

        }

    }


    @ResponseBody
    @RequestMapping(value = "/{agreement}.do")
    public ResponseEntity<String> agreementad(HttpServletRequest request,
                                     @PathVariable("agreement") String agreement
                                     ) {


        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", acsConfig.getProperty("NIGNX.SERVER_URL") + acsConfig.getProperty("AD.ERROR.PAGE"));
        try {


            if(agreement == null)
            {
                log.error("[AGREEMENT-ESTIMATE] 정보가 [{}] 유효하지 않습니다.", agreement);
                return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);
            }

            Integer type = Integer.valueOf(agreement.substring(0, 1));
            Integer status = Integer.valueOf(agreement.substring(1, 2));
            Long estimate_no = Long.valueOf(agreement.substring(2, agreement.length() ));

            if(type == null || status == null)
            {
                log.error("[AGREEMENT-ESTIMATE] 이벤트 유형 [{}] 및 상태 [{}] 정보가 유효하지 않습니다.", type, status);
                return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);
            }

            // 이벤트 타입 확인
            AgreementType agreementType = AgreementType.get(type);
            if( agreementType == null)
            {
                log.error("[AGREEMENT-ESTIMATE] 이벤트 유형 [{}]이 유효하지 않습니다.", type);
                return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);

            }

            // 이벤트 상태 처리 확인
            AgreementStatus agreementStatus = AgreementStatus.get(status);
            if( agreementStatus == null)
            {
                log.error("[AGREEMENT-ESTIMATE] 이벤트 상태가 [{}]이 유효하지 않습니다.", status);
                return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);
            }

            // 상태 반영

            if(agreementType == AgreementType.이벤트약관)
            {
                Map<String, Object> map = new HashMap<>();
                map.put("estimate_no", estimate_no);
                map.put("status", status);
                estimateDao.updateAgreementEstimate(map);
            }

            if(agreementType == AgreementType.알림문자)
            {
                msgQueueDao.updateMsgQueueBlock(estimate_no);
            }


            headers.clear();
            headers.add("Location", acsConfig.getProperty("NIGNX.SERVER_URL") + acsConfig.getProperty("AD.SUCCESS.PAGE"));
            return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);


        } catch (Exception e) {
            log.error("[AGREEMENT-ESTIMATE] 이벤트 약관 수신거부 처리 오류.[{}]", e);
            headers.clear();
            headers.add("Location", acsConfig.getProperty("NIGNX.SERVER_URL") + acsConfig.getProperty("AD.ERROR.PAGE"));
            return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);

        }
    }
}

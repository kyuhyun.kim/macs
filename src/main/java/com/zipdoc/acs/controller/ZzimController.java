package com.zipdoc.acs.controller;

import com.zipdoc.acs.define.AgentType;
import com.zipdoc.acs.define.PictureType;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ZzimService;
import com.zipdoc.acs.model.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * 찜하기
 * @author 이동규
 *
 */
@Controller
public class ZzimController extends ResponseEntityService {

	private static final Logger log = LoggerFactory.getLogger(ZzimController.class);

	@Autowired
	private MemberService memberService;
	
	@Autowired
	private ZzimService zzimService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@ResponseBody
	@RequestMapping(value = "/zzim/complex")
	public ResponseEntity<String> zzimList_Complex(HttpServletRequest request,
										   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
										   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										   @RequestHeader(value = "ZID", required = false) Long zid,
										   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
										   @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										   @RequestBody ZzimListRequest scrapRequest) {
		try {

			// 스크랩북조회
			ZzimBook book = zzimService.selectZzimBook(scrapRequest.getBook_no());
			if (book == null) {
				log.error("[SCRAP-COMPLEX-LIST] 목록을 조회할 스크랩북 정보가 없습니다. BOOK-NO[["+scrapRequest.getBook_no());
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			ScrapListResponse result = new ScrapListResponse();
			result.setBook_no(book.getBook_no());
			result.setBook_name(book.getBook_name());

			// 시공사례 조회 정보 설정
			ZzimListRequest listRequest = new ZzimListRequest(member.getMember_no(), 0, scrapRequest.getGallery_limit(), scrapRequest.getBook_no());

			// 시공사례 개수 조회
			result.setGallery_total_count(zzimService.selectZzimListTotalCount(listRequest));

			// 등록된 시공사례가 있는 경우
			if (result.getGallery_total_count() > 0) {
				List<Product> list = zzimService.selectZzimList(listRequest);
				for (Product product : list) {
					product.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
				}
				result.setGallery_list(list);
			}

			// 시공사진 설정.
			listRequest.setLimit(scrapRequest.getPicture_limit());

			// 시공사진 개수 조회
			result.setPicture_total_count(zzimService.selectZzimItemListTotalCount(listRequest));

			// 등록된 시공사진이 있는 경우
			if (result.getPicture_total_count() > 0) {
				List<Item> list = zzimService.selectZzimItemList(listRequest);
				for (Item item : list) {
					item.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
				}
				result.setPicture_list(list);
			}

			// 총합산 개수 설정
			result.setTotal_count(result.getGallery_total_count() + result.getPicture_total_count());

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[SCRAP-COMPLEX-LIST] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[SCRAP-COMPLEX-LIST] ERROR ", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/list/{book_no}/{page}/{limit}")
	public ResponseEntity<String> zzimList_Gallery(HttpServletRequest request,
											@RequestHeader(value = "APP-VERSION", required = false) String app_version,
											@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											@RequestHeader(value = "ZID", required = false) Long zid,
											@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
											@RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
											@PathVariable("book_no") long book_no,
											@PathVariable("page") int page,
											@PathVariable("limit") int limit) {
		try {

			if (book_no <= 0) {
				log.error("[SCRAP-GALLERY-LIST] 목록을 조회할 스크랩북 정보가 없습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			ZzimListRequest listRequest = new ZzimListRequest(member.getMember_no(), page, limit, book_no);

			int total_count = zzimService.selectZzimListTotalCount(listRequest);

			List<Product> list = zzimService.selectZzimList(listRequest);

			// 조회된 결과가 없을 경우
			if (list == null || list.size() == 0) {
				log.debug("[SCRAP-GALLERY-LIST] ID["+member.getMember_id()+"] "+page+"page 에는 스크랩한 시공사례가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			for (Product product:list) {
				product.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}

			ProductListResponse result = new ProductListResponse(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[SCRAP-GALLERY-LIST] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[SCRAP-GALLERY-LIST] ERROR ", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/item/{book_no}/{page}/{limit}")
	public ResponseEntity<String> zzimList_Picture(HttpServletRequest request,
										   @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
										   @PathVariable("book_no") long book_no,
										   @PathVariable("page") int page,
										   @PathVariable("limit") int limit) {
		try {

			if (book_no <= 0) {
				log.error("[SCRAP-ITEM-LIST] 목록을 조회할 스크랩북 정보가 없습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember((long)0, (long)0, null, web_token);

			ZzimListRequest listRequest = new ZzimListRequest(member.getMember_no(), page, limit, book_no);

			int total_count = zzimService.selectZzimItemListTotalCount(listRequest);

			List<Item> list = zzimService.selectZzimItemList(listRequest);

			// 조회된 결과가 없을 경우
			if (list == null || list.size() == 0) {
				log.debug("[SCRAP-ITEM-LIST] ID["+member.getMember_id()+"] "+page+"page 에는 스크랩한 시공사진이 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			for (Item item : list) {
				item.buildPictureUrl(PictureType.XXHDPI, PictureType.XHDPI);
			}

			ItemListResponse result = new ItemListResponse(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[SCRAP-ITEM-LIST] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[SCRAP-ITEM-LIST] ERROR ", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * 찜한 상품아이디 리스트 전체를 조회한다.
	 * @param request
	 * @param app_version
	 * @param member_no
	 * @param zid
	 * @param zid_key
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/zzim/pid-list/{category_type}")
	public ResponseEntity<String> zzimPidList(HttpServletRequest request,
										   @RequestHeader(value = "APP-VERSION", required = true) String app_version,
										   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
										   @RequestHeader(value = "ZID", required = true) Long zid,
										   @RequestHeader(value = "ZID-KEY", required = true) String zid_key,
										   @PathVariable("category_type") int category_type) {
		try {
			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, true);

			if (member.getMember_no()==null){
				log.debug("[SCRAP-PID-LIST] 찜하기 정보가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			List<Long> list = zzimService.selectZzimPidList(member_no, category_type);
			// 조회된 결과가 없을 경우
			if(list == null || list.size() == 0) {
				log.debug("[SCRAP-PID-LIST] 찜하기 정보가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			ZzimPidListResponse result = new ZzimPidListResponse(list.size(), list);

			return makeResponseEntity(request, result, HttpStatus.OK);
		} catch (ServiceException e){
			log.error(e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ZZIM-PID-LIST", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/add/{book_no}/{pid}", method = RequestMethod.POST)
	public ResponseEntity<String> zzimCreate_Gallery(HttpServletRequest request,
											 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
											 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											 @RequestHeader(value = "ZID", required = false) Long zid,
											 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
											 @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
											 @PathVariable("book_no") Long book_no,
											 @PathVariable("pid") Long pid) {

		return zzimCreate(request, app_version, member_no, zid, zid_key, web_token, book_no, pid, (long)0);
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/add/{book_no}/{pid}/{item_id}", method = RequestMethod.POST)
	public ResponseEntity<String> zzimCreate_GalleryOrPicture(HttpServletRequest request,
											 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
											 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											 @RequestHeader(value = "ZID", required = false) Long zid,
											 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
											 @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
											 @PathVariable("book_no") Long book_no,
											 @PathVariable("pid") Long pid,
											 @PathVariable("item_id") Long item_id) {
		return zzimCreate(request, app_version, member_no, zid, zid_key, web_token, book_no, pid, item_id);
	}

	public ResponseEntity<String> zzimCreate(HttpServletRequest request, String app_version,
											 Long member_no, Long zid, String zid_key,
											 String web_token,
											 Long book_no, Long pid, Long item_id) {
		try {

			if (book_no == null || pid == null) {
				log.error("[SCRAP-ADD] 필수항목오류.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			Zzim zzim = new Zzim(member.getMember_no(), pid, book_no);
			zzim.setItem_id(item_id == null ? 0 : item_id);

			zzimService.createZzim(zzim);

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e) {
			log.error("]SCRAP-ADD] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("[SCRAP-ADD] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/delete/{pid}", method = RequestMethod.POST)
	public ResponseEntity<String> zzimDelete_Gallery(HttpServletRequest request,
													@RequestHeader(value = "APP-VERSION", required = false) String app_version,
													@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
													@RequestHeader(value = "ZID", required = false) Long zid,
													@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
													@RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
													@PathVariable("pid") Long pid) {
		return zzimDelete(request, app_version, member_no, zid, zid_key, web_token, pid, (long)0);
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/delete/{pid}/{item_id}", method = RequestMethod.POST)
	public ResponseEntity<String> zzimDelete_GalleryOrPicture(HttpServletRequest request,
													 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
													 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
													 @RequestHeader(value = "ZID", required = false) Long zid,
													 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
													 @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
													 @PathVariable("pid") Long pid,
													 @PathVariable("item_id") Long item_id) {
		return zzimDelete(request, app_version, member_no, zid, zid_key, web_token, pid, item_id);
	}

	private ResponseEntity<String> zzimDelete(HttpServletRequest request, String app_version,
											  Long member_no, Long zid, String zid_key,
											  String web_token,
											  Long pid, Long item_id) {
		try {
			if (pid == null) {
				log.error("[SCRAP-DELETE] 필수항목오류.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			Zzim zzim = new Zzim(member.getMember_no(), pid);
			zzim.setItem_id(item_id == null ? 0 : item_id);

			zzimService.deleteZzim(zzim);

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[SCRAP-DELETE] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[SCRAP-DELETE] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/delete-multi", method = RequestMethod.POST)
	public ResponseEntity<String> zzimDeleteMulti(HttpServletRequest request,
												@RequestHeader(value = "APP-VERSION", required = false) String app_version,
												@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												@RequestHeader(value = "ZID", required = false) Long zid,
												@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												@RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
												@RequestBody Zzim zzim) {
		try {

			if ((zzim.getPid_list() == null || zzim.getPid_list().size() == 0) &&
					(zzim.getItem_list() == null || zzim.getItem_list().size() == 0)) {
				log.error("[SCRAP-DELETE-MULTI] 필수항목누락. 시공사례 및 시공사진 목록이 모두 없습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			zzim.setMember_no(member.getMember_no());
			zzimService.deleteZzimMulti(zzim);

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[SCRAP-DELETE-MULTI] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[SCRAP-DELETE-MULTI] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/delete-all/{book_no}", method = RequestMethod.POST)
	public ResponseEntity<String> zzimDeleteAll(HttpServletRequest request,
												@RequestHeader(value = "APP-VERSION", required = false) String app_version,
												@RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												@RequestHeader(value = "ZID", required = false) Long zid,
												@RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												@RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
												@PathVariable("book_no") long book_no) {
		try {

			if (book_no <= 0) {
				log.error("[SCRAP-DELETE-ALL] 삭제할 스크랩북 정보가 없습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			Zzim zzim = new Zzim(member.getMember_no(), -1, book_no);
			zzimService.deleteZzimAll(zzim);

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[SCRAP-DELETE-ALL] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[SCRAP-DELETE-ALL] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/book/list/{page}/{limit}")
	public ResponseEntity<String> zzimBookList(HttpServletRequest request,
											   @RequestHeader(value = "APP-VERSION", required = false) String app_version,
											   @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
											   @RequestHeader(value = "ZID", required = false) Long zid,
											   @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
											   @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
											   @PathVariable("page") int page,
											   @PathVariable("limit") int limit) {
		try {

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			ZzimListRequest listRequest = new ZzimListRequest(member.getMember_no(), page, limit);

			AgentType agentType = StringUtils.isEmpty(web_token) ? AgentType.APP : AgentType.WEB;
			listRequest.setAgent(agentType.getCode());

			int total_count = zzimService.selectZzimBookListTotalCount(listRequest);

			// 조회된 결과가 없을 경우
			if (total_count == 0) {
				log.debug("[SCRAP-BOOK-LIST] "+page+"page 에는 스크랩북 정보가 없습니다.");
				return makeResponseEntity(HttpStatus.NO_CONTENT);
			}

			log.debug("[SCRAP-LIST] "+page+"page 에서 총 "+total_count+"개의 스크랩북 정보를 조회했습니다.");

			List<ZzimBook> list = zzimService.selectZzimBookList(listRequest);
			ListResponse<ZzimBook> result = new ListResponse<ZzimBook>(total_count, list);

			return makeResponseEntity(request, result, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[SCRAP-BOOK--LIST] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			log.error("[SCRAP-BOOK--LIST] ERROR ", e);
		}
		return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/book/add", method = RequestMethod.POST)
	public ResponseEntity<String> zzimBookCreate(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												 @RequestHeader(value = "ZID", required = false) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												 @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
												 @RequestBody ZzimBook zzimBook) {
		try {

			if (StringUtils.isEmpty(zzimBook.getBook_name())) {
				log.error("[SCRAP-BOOK--ADD] 등록할 스크랩북 이름이 없습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			log.debug("[SCRAP-BOOK--ADD] ID["+member.getMember_id()+"] 스크랩북 등록. ["+zzimBook.getBook_name()+"]");

			zzimBook.setMember_no(member.getMember_no());
			zzimService.createZzimBook(zzimBook);

			return makeResponseEntity(request, zzimBook, HttpStatus.OK);

		} catch (ServiceException e){
			log.error("[SCRAP-BOOK--ADD] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("[SCRAP-BOOK--ADD] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/zzim/book/delete", method = RequestMethod.POST)
	public ResponseEntity<String> zzimBookDelete(HttpServletRequest request,
												 @RequestHeader(value = "APP-VERSION", required = false) String app_version,
												 @RequestHeader(value = "MEMBER-NO", required = false) Long member_no,
												 @RequestHeader(value = "ZID", required = false) Long zid,
												 @RequestHeader(value = "ZID-KEY", required = false) String zid_key,
												 @RequestHeader(value = "ZWS-TOKEN", required = false) String web_token,
												 @RequestBody ZzimBook zzimBook) {
		try {

			if (zzimBook.getBook_no_list() == null || zzimBook.getBook_no_list().size() == 0) {
				log.error("[SCRAP-BOOK-DELETE] 삭제할 스크랩북 목록이 없습니다.");
				return makeResponseEntity(HttpStatus.BAD_REQUEST);
			}

			// 가입자 조회
			Member member = authenticationHelper.findMember(member_no, zid, zid_key, web_token);

			log.debug("[SCRAP-BOOK-DELETE] ID["+member.getMember_id()+"] 스크랩북 삭제. ["+zzimBook.getBook_no_list().toString()+"]");

			zzimBook.setMember_no(member.getMember_no());
			zzimService.deleteZzimBook(zzimBook);

			return makeResponseEntity(HttpStatus.OK);
		} catch (ServiceException e){
			log.error("[SCRAP-BOOK-DELETE] "+e.getInvokeResultMsg() + "(" + e.getInvokeResult() + ")");
			return makeResponseEntity(HttpStatus.valueOf(e.getInvokeResult()));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("[SCRAP-BOOK-DELETE] ERROR ", e);
			return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}

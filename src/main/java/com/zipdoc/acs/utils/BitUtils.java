package com.zipdoc.acs.utils;

public class BitUtils {

	public static boolean getBit(int num, int idx) {
		return ((1 << idx) & num) != 0;
	}

	public static int setBit(int num, int idx) {
		return num | (1 << idx);
	}

	public static int clearBit(int num, int idx) {
		int mask = ~(1 << idx);
		return num & mask;
	}

	public static int updateBit(int num, int idx, int value) {
		return (num & ~(1 << idx)) | (value << idx);
	}
}
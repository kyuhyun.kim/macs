package com.zipdoc.acs.utils;

import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.PictureType;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;

/**
 * Created by ZIPDOC on 2016-06-06.
 */
public class ImageResizeUtil {

    public static void imageResize(InputStream inputStream, File targetFilePath, int width, int height) throws Exception{
        BufferedImage image = ImageIO.read(inputStream);
        int targetWidth = width;
        int targetHeight = height;

        if(image.getWidth()<width){
            targetWidth = image.getWidth();
        }

        if(image.getHeight()<height){
            targetHeight = image.getHeight();
        }

        //세로사진인 경우 값을 뒤바꿈
        if(image.getWidth()<image.getHeight()){
            int temp = targetWidth;
            targetWidth = targetHeight;
            targetHeight = temp;
        }

        Thumbnails.of(image).size(targetWidth, targetHeight).keepAspectRatio(true).outputQuality(1.0f).toFile(targetFilePath);
    }

    public static void imageResize(BufferedImage image, String targetFilePath, int width, int height) throws Exception{
        int targetWidth = width;
        int targetHeight = height;

        if(image.getWidth()<width){
            targetWidth = image.getWidth();
        }

        if(image.getHeight()<height){
            targetHeight = image.getHeight();
        }

        //세로사진인 경우 값을 뒤바꿈
        if(image.getWidth()<image.getHeight()){
            int temp = targetWidth;
            targetWidth = targetHeight;
            targetHeight = temp;
        }

        Thumbnails.of(image).size(targetWidth, targetHeight).keepAspectRatio(true).outputQuality(1.0f).toFile(targetFilePath);
    }

    public static void imageResize(File orgFilePath, String targetFilePath, int width, int height) throws Exception{
        BufferedImage image = ImageIO.read(orgFilePath);
        imageResize(image, targetFilePath, width, height);
    }


    /**
     * 서버에 사용될 이미지 파일을 유형별로 리사이징한다.
     * @param orgFileDir 원본파일 디렉토리
     * @param orgFileName 원본파일명
     * @param targetFileDir 변환파일 디렉토리
     * @throws Exception
     */
    public static void imageResizeForThurmbnails(String orgFileDir, String orgFileName, String targetFileDir) throws Exception{
        BufferedImage image = ImageIO.read(new File(orgFileDir + orgFileName));
        for(PictureType pictureType : PictureType.values()){
            if(pictureType.isCrop()){ //정사각형은 잘라내기를 수행한다.
                Thumbnails.of(image).crop(Positions.CENTER).size(pictureType.getWidth(), pictureType.getHeight()).toFile(targetFileDir + pictureType.getPrefix() + orgFileName);
            } else {
                int width = PictureType.XXHDPI.getWidth();
                int height = PictureType.XXHDPI.getHeight();

                if(image.getWidth()<image.getHeight()){
                    width = PictureType.XXHDPI.getHeight();
                    height = PictureType.XXHDPI.getWidth();
                }

                imageResize(image, targetFileDir + pictureType.getPrefix() + orgFileName, width, height);
                //Thumbnails.of(image).size(width, height).toFile(targetFileDir + pictureType.getPrefix() + orgFileName);
                //Thumbnails.of(image).size(width, height).keepAspectRatio(true).outputQuality(1.0f).toFile(targetFileDir + pictureType.getPrefix() + orgFileName);
            }
        }
    }

}

package com.zipdoc.acs.utils;

import org.apache.commons.lang.StringUtils;

public class MultiLangUtil {
	
	/**
	 * 한글이 포함된 문자열에서 특정위치까지 자른 후  ...을 붙여 리턴한다.
	 * T그룹온 DB가 UTF-8 Charset이므로 한글을 3byte로 처리함.
	 * @param str 입력문자열
	 * @param byteLength 
	 * @return
	 */
	public static String subStringBytes(String str, int byteLength, boolean isAddDot) {   
		// String 을 byte 길이 만큼 자르기.    
	    int retLength = 0;    
	    int tempSize = 0;    
	    int asc;    
	    if(str == null || "".equals(str) || "null".equals(str)){
	        str = "";
	    }
	 
	    int length = str.length();
	     
	    for (int i = 1; i <= length; i++) {        
	        asc = (int) str.charAt(i - 1);        
	        if (asc > 127) {            
	            if (byteLength >= tempSize + 3) {                
	                tempSize += 3;                
	                retLength++;            
	            } else {                
	                return isAddDot?str.substring(0, retLength)+"...":str.substring(0, retLength);            
	            }       
	        } else {           
	            if (byteLength > tempSize) {
	                tempSize++;
	                retLength++;            
	            }
	        }    
	    }   
	    return str.substring(0, retLength);
	}

	public static int length(String str){
	    int tempSize = 0;    
	    int asc;
	    
		if(StringUtils.isBlank(str)) return 0;
		
	    int length = str.length();
	     
	    for (int i = 1; i <= length; i++) {
	        asc = (int) str.charAt(i - 1);        
	        if (asc > 127) {            
                tempSize += 3;                
	        } else {           
                tempSize++;
	        }    
	    }
		return tempSize;
	}
	
	/*
	public static void main(String[] args){
		System.out.println(MultiLangUtil.subStringBytes("가나다라마바사아자차카타파하", 30, false));
		System.out.println(MultiLangUtil.subStringBytes("가나다라마바사아자차카타파하", 30, true));
		System.out.println(MultiLangUtil.subStringBytes("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 30, true));
		System.out.println(MultiLangUtil.subStringBytes("!@#$%^&*()_+===============!@#$", 30, false));
	}
	*/

}

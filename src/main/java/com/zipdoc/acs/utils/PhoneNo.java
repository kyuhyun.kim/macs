package com.zipdoc.acs.utils;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.regex.Pattern;

public class PhoneNo implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 8363356027289869609L;
    private static final String DEFAULT_COUNTRY = "KR";
    private static final Logger logger = LoggerFactory.getLogger(PhoneNo.class);
    private static PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    PhoneNumber number;



    public PhoneNo(String phoneNo, String regionCode) throws NumberParseException {
        if (regionCode == null) {
            regionCode = DEFAULT_COUNTRY;
        }
        this.number = phoneUtil.parseAndKeepRawInput(phoneNo, regionCode);
    }

    //Get E164 Format
    public static String getE164Fmt(String phoneNo) {
        return getE164Fmt(phoneNo, DEFAULT_COUNTRY);
    }

    public static boolean isCellphone(String phone_no)
    {
        return Pattern.matches("^01(?:0|1[6-9])(?:\\d{3}|\\d{4})\\d{4}$", phone_no);
    }

    //Get E164 Format
    public static String getE164Fmt(String phoneNo, String region) {
        try {
            logger.debug("phoneNo(" + phoneNo + ")");
            PhoneNumber number = phoneUtil.parseAndKeepRawInput(phoneNo, region);
            //phoneUtil.truncateTooLongNumber(number);
            String E164Fmt = phoneUtil.format(number, PhoneNumberFormat.E164);
            logger.info("phoneNo(" + phoneNo + ") --> E164(" + E164Fmt + ")");
            return E164Fmt;
        } catch (NumberParseException e) {
            logger.error("Invalid phone number:" + phoneNo + ":" + e.toString());
            return null;
        }
    }

    //Get E164 Format
    public static String getNationalFmt(String phoneNo) {
        try {
            PhoneNumber number = phoneUtil.parseAndKeepRawInput(phoneNo, DEFAULT_COUNTRY);
            String nationalNumber = phoneUtil.format(number, PhoneNumberFormat.NATIONAL).replaceAll("-", "").replaceAll(" ", "");
            logger.info("phoneNo(" + phoneNo + ") --> nationalNumber(" + nationalNumber + ")");
            return nationalNumber;
        } catch (NumberParseException e) {
            logger.error("Invalid phone number:" + phoneNo + ":" + e.toString());
            return null;
        }
    }
    
    //Get E164 Format
    public static String getNationalDashFmt(String phoneNo) {
        try {
            PhoneNumber number = phoneUtil.parseAndKeepRawInput(phoneNo, DEFAULT_COUNTRY);
            String nationalNumber = phoneUtil.format(number, PhoneNumberFormat.NATIONAL);
            logger.info("phoneNo(" + phoneNo + ") --> nationalNumber(" + nationalNumber + ")");
            return nationalNumber;
        } catch (NumberParseException e) {
            logger.error("Invalid phone number:" + phoneNo + ":" + e.toString());
            return null;
        }
    }

    //Get Country Code
    public static String getCountryCode(String phoneNo) {
        try {
            PhoneNumber number = phoneUtil.parseAndKeepRawInput(phoneNo, DEFAULT_COUNTRY);
            String countryCode = Integer.toString(number.getCountryCode());
            logger.info("phoneNo(" + phoneNo + ") --> Country code(" + countryCode + ")");
            return countryCode;
        } catch (NumberParseException e) {
            logger.error("Invalid phone number:" + phoneNo + ":" + e.toString());
            return null;
        }
    }

    public static String getRegionCode(String phoneNo) {
        try {
            PhoneNumber number = phoneUtil.parseAndKeepRawInput(phoneNo, DEFAULT_COUNTRY);
            String regionCode = phoneUtil.getRegionCodeForNumber(number);
            logger.info("phoneNo(" + phoneNo + ") --> Region code(" + regionCode + ")");
            return regionCode;
        } catch (NumberParseException e) {
            logger.error("Invalid phone number:" + phoneNo + ":" + e.toString());
            return null;
        }
    }

    public static void convert(String phoneNo, String regionCode, String e164Fmt, String nationalFmt) throws NumberParseException {
        if (regionCode == null) {
            regionCode = DEFAULT_COUNTRY;
        }

        PhoneNumber number = phoneUtil.parseAndKeepRawInput(phoneNo, regionCode);
        e164Fmt = phoneUtil.format(number, PhoneNumberFormat.E164);
        nationalFmt = phoneUtil.format(number, PhoneNumberFormat.NATIONAL).replaceAll("-", "").replaceAll(" ", "");
    }

    public String getRegionCode() {
        return phoneUtil.getRegionCodeForNumber(number);
    }

    public String getNationalFmt() {
        return phoneUtil.format(number, PhoneNumberFormat.NATIONAL).replaceAll("-", "").replaceAll(" ", "");
    }

    public String getE164Fmt() {
        return phoneUtil.format(number, PhoneNumberFormat.E164);
    }

    public String getCountryCode() {
        return Integer.toString(number.getCountryCode());
    }
}
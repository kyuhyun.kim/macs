package com.zipdoc.acs.utils;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.domain.service.ServiceException;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 이동규 on 2017-03-10.
 */
public class FileUploadUtil {
    private static final Logger log = LoggerFactory.getLogger(FileUploadUtil.class);

    // zipdoc s3 settings
    private static final String AWS_REGIONS = Messages.getMessage("AWS_REGIONS");
    private static final String AWS_BUCKET_NAME = Messages.getMessage("AWS_BUCKET_NAME");
    private static final String AWS_BUCKET_PATH = Messages.getMessage("AWS_BUCKET_PATH");
    private static final String AWS_SECRET_KEY = Messages.getMessage("AWS_SECRET_KEY");
    private static final String AWS_ACCESS_KEY = Messages.getMessage("AWS_ACCESS_KEY");

    private static final String AWS_UPLOAD_PATH = AWS_BUCKET_NAME + AWS_BUCKET_PATH;

    private static final String AWS_BUCKET_PREFIX = AWS_BUCKET_PATH.replace("/", "");

    private static FileUploadUtil instance;
    private static AWSCredentials awsCredentials = new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY);
    private Regions regions = Regions.fromName(AWS_REGIONS);

    public static synchronized FileUploadUtil getInstance() {
        if (instance == null) {
            instance = new FileUploadUtil();
        }
//        instance.setAwsConfig();
        return instance;
    }


    public AmazonS3Client getAwsS3Client() {
        AmazonS3Client amazonS3Client = new AmazonS3Client(awsCredentials);
        amazonS3Client.setRegion(Region.getRegion(regions));
        return amazonS3Client;
    }

    /**
     * 웹에서 스마트에디터를 사용하는 기능인 경우 본문에 img태그를 추가할 수 있도록
     * 파일을 업로드 후 태그를 생성하여 리턴한다.
     * @param mFiles
     * @return 이미지태그
     * @throws ServiceException
     */
    public static String uploadSmartEditorFile(List<MultipartFile> mFiles) throws ServiceException{
        StringBuffer sb = new StringBuffer();
        if(mFiles!=null && mFiles.size()>0) {
            for (MultipartFile mFile : mFiles) {
                try {
                    if(mFile.isEmpty()) continue;

                    String upload_path = Constants.UPLOAD_STATIC_PATH + Constants.RELATIVE_SMARTEDITOR_PATH;
                    String file_name = DateUtil.toFormatString(new Date(), DateUtil.DATE_FORMAT_FOR_MILLISECOND) +  "_" + RandomStringUtils.randomAlphanumeric(10);;
                    FileInfo imageFile = new FileInfo(mFile, upload_path + file_name);

                    // 파일 저장
                    mFile.transferTo(imageFile.getFile());
                    getInstance().uploadToS3(imageFile.getFile(), Constants.RELATIVE_SMARTEDITOR_PATH
                            + file_name + "." + imageFile.getExtension()) ;

                    getInstance().deleteFile(imageFile.getFile());

                    log.info( " relative_path  : " + Constants.RELATIVE_SMARTEDITOR_PATH );
                    log.info( " file_name  : " + file_name );

                    //웹이 smarteditor를 사용하므로 본분에 image tag를 append한다.
                    sb.append("<p>")
                            .append("<img src=\"")
                            .append(Constants.RELATIVE_STATIC_PATH).append(Constants.RELATIVE_SMARTEDITOR_PATH)
                            .append(imageFile.getFile().getName())
                            .append("\"/>")
                            .append("<br style=\"clear:both;\">")
                            .append("</p>");
                } catch (Exception e) {
                    log.error("업로드 파일 [" + mFile.getOriginalFilename() + "] 오류. (" + e.getMessage() + ")");
                    throw new ServiceException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "파일업로드 중 오류가 발생되었습니다.");
                }
            }
        }
        return sb.toString();
    }

    public static FileInfo uploadFile(MultipartFile multipartFile, String relative_path) throws ServiceException{
        try {
            String upload_path = Constants.UPLOAD_STATIC_PATH + relative_path;
            String file_name = DateUtil.toFormatString(new Date(), DateUtil.DATE_FORMAT_FOR_MILLISECOND) +  "_"
                    + RandomStringUtils.randomAlphanumeric(10);
            FileInfo imageFile = new FileInfo(multipartFile, upload_path + file_name);

            //상위디렉토리 생성
            if(!imageFile.getFile().getParentFile().exists()){
                imageFile.getFile().getParentFile().mkdirs();
            }
            // 파일 저장
            multipartFile.transferTo(imageFile.getFile());
            getInstance().uploadToS3(imageFile.getFile(), relative_path + file_name
                    + "."+ imageFile.getExtension());

            log.info( " relative_path  : " + relative_path );
            log.info( " file_name  : " + file_name );

            return imageFile;
        } catch (Exception e) {
            log.error("업로드 파일 [" + multipartFile.getOriginalFilename() + "] 오류. (" + e.getMessage() + ")");
            throw new ServiceException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "파일업로드 중 오류가 발생되었습니다.");
        }
    }


    /**
     * 디렉토리에 suffix로 끝나는 파일을 삭제한다.
     * S3 로 변경
     * @param dirPath
     * @param suffix
     */
    public void deleteFilesBySuffix(String dirPath, String suffix){
        ArrayList<String> fileNamesToDelete = isExistInSuffix(dirPath, suffix);
        if ( fileNamesToDelete != null && fileNamesToDelete.size() > 0 ){
            for ( String fileName : fileNamesToDelete ){
                getAwsS3Client().deleteObject(AWS_UPLOAD_PATH, fileName);
            }
        }
    }

    public void uploadToS3(File destDir, final String s3_path_with_fileName) {
        try{
            if (destDir != null) {
                // 파일인지 판단 // todo :: fileName 관련 확인
                if (destDir.isDirectory()) {
                    File[] fileList = destDir.listFiles();
                    for (File tempFile : fileList) {
                        if ( tempFile.isFile() ){
                            String tempFileName = tempFile.getName();
                            putObjectToS3ByFile(AWS_UPLOAD_PATH, s3_path_with_fileName, tempFileName, tempFile,
                                    StorageClass.Standard, CannedAccessControlList.PublicRead);
                        }
                    }
                    // upload 후 local 일괄 삭제 ( in ec2 )
                    for (File tempFile : fileList) {
                        if ( tempFile.isFile() ){
                            tempFile.delete();
                        }
                    }

                }else if (destDir.isFile() ) {
                    putObjectToS3ByFile(AWS_UPLOAD_PATH, s3_path_with_fileName, "", destDir,
                            StorageClass.Standard, CannedAccessControlList.PublicRead);
                    destDir.delete();
                }
            }
        }catch (InterruptedException ie){
            ie.printStackTrace();
        }

    }


    public void uploadToS3(File destDir, final String s3_path_with_fileName, Boolean delDir) {
        try{
            if (destDir != null) {
                // 파일인지 판단 // todo :: fileName 관련 확인
                if (destDir.isDirectory()) {
                    File[] fileList = destDir.listFiles();
                    for (File tempFile : fileList) {
                        if ( tempFile.isFile() ){
                            String tempFileName = tempFile.getName();
                            putObjectToS3ByFile(AWS_UPLOAD_PATH, s3_path_with_fileName, tempFileName, tempFile,
                                    StorageClass.Standard, CannedAccessControlList.PublicRead);
                        }
                    }
                    if(delDir)
                    {
                        if(destDir.isDirectory())
                            destDir.delete();
                    }
                    else {
                        // upload 후 local 일괄 삭제 ( in ec2 )
                        for (File tempFile : fileList) {
                            if (tempFile.isFile()) {
                                tempFile.delete();
                            }
                        }
                    }

                }else if (destDir.isFile() ) {
                    putObjectToS3ByFile(AWS_UPLOAD_PATH, s3_path_with_fileName, "", destDir,
                            StorageClass.Standard, CannedAccessControlList.PublicRead);
                    destDir.delete();
                }
            }
        }catch (InterruptedException ie){
            ie.printStackTrace();
        }

    }

    public void deleteFromS3( final String s3_path, final String s3_fileName){
        if ( isExist(s3_path, s3_fileName) ) {
            getAwsS3Client().deleteObject(AWS_UPLOAD_PATH, s3_path + s3_fileName);
        }
    }

    public void deleteFilesFromS3( final String s3_path, final String containedKeys){
        if ( isExist(s3_path, containedKeys) ) {
//            getAwsS3Client().deleteObject(AWS_UPLOAD_PATH, s3_path + s3_fileName);
        }
    }

    public void listS3Objects(){
        System.out.println("Listing objects");
        final ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(AWS_BUCKET_NAME).withPrefix("akmTest");
        ListObjectsV2Result result;
        do {
            result = getAwsS3Client().listObjectsV2(req);

            for (S3ObjectSummary objectSummary :
                    result.getObjectSummaries()) {
                System.out.println(" - " + objectSummary.getKey() + "  " +
                        "(size = " + objectSummary.getSize() +
                        ")");
            }
            System.out.println("Next Continuation Token : " + result.getNextContinuationToken());
            req.setContinuationToken(result.getNextContinuationToken());
        } while(result.isTruncated() == true );
    }

    public void deleteFile(File fileToDelete){
        if ( fileToDelete != null ){
            fileToDelete.delete();
        }
    }

    /**
     * File 로 S3 에 파일을 업로드 해야 할 경우
     *
     * @param bucket
     * @param s3Path
     * @param fileName
     * @param file
     * @param storageClass
     * @param cannedAccessControlList
     * @return
     */
    public void putObjectToS3ByFile (
            String bucket,
            String s3Path,
            String fileName,
            File file,
            StorageClass storageClass,
            CannedAccessControlList cannedAccessControlList ) throws InterruptedException {
        String storagePath = s3Path + fileName;

        TransferManager tx = new TransferManager();

        PutObjectRequest putObjectRequest = new PutObjectRequest(
                bucket,
                storagePath,
                file
        );

        putObjectRequest.setStorageClass(storageClass);
        putObjectRequest.setCannedAcl(cannedAccessControlList);
        Upload myUpload = tx.upload(putObjectRequest);
        myUpload.waitForCompletion();

    }

    //prefix : "images/seminar/1/"
    public boolean isExist(  String prefix, String key)
    {
        final String prefixForStatic =  AWS_BUCKET_PREFIX +"/" + prefix;

        final ListObjectsV2Request listObjectsRequest = new ListObjectsV2Request().withBucketName(
                AWS_BUCKET_NAME )
                .withPrefix(prefixForStatic + key);

        ListObjectsV2Result objects = getAwsS3Client().listObjectsV2(listObjectsRequest);
        if (objects != null && objects.getObjectSummaries().size() > 0 ){
            log.info (  " foundSize : " +  objects.getObjectSummaries().size() );
            return true;
        }
        return false;
    }

    public ArrayList<String> isExistInSuffix(  String prefix, String suffix)
    {
        final String prefixForStatic =  AWS_BUCKET_PREFIX +"/" + prefix;
        ArrayList<String> files = new ArrayList<>();

        final ListObjectsV2Request listObjectsRequest = new ListObjectsV2Request().withBucketName(
                AWS_BUCKET_NAME )
                .withPrefix(prefixForStatic);

        ListObjectsV2Result objects = getAwsS3Client().listObjectsV2(listObjectsRequest);
        for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
            String keyName = objectSummary.getKey();
            if (keyName.contains(suffix)){
                files.add(keyName);
            }
        }
        return files;
    }


}

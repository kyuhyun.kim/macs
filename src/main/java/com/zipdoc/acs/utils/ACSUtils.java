package com.zipdoc.acs.utils;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 김종부
 */
public class ACSUtils {

    private static String passwd_validation_user = "^(?=.*[0-9])(?=.*[a-zA-Z])(?=\\S+$).{10,16}$";
    private static String passwd_validation_cms = "^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*\\W)(?=\\S+$).{8,16}$";

    static final char[] BASE62 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();


    public static void main(String[] args) throws ParseException {

        System.out.println("result = "+ RandomStringUtils.randomAlphanumeric(14));
    }

    /**
     * 첫두글자 마스킹 처리
     * @param origin
     * @return
     */
    public static String maskingCharsStartWith(String origin, String maskingChar, int length) {

        if (length <= 0 || origin == null || origin.length() < length) return origin;

        String masking = "";
        for (int i = 0; i < length; i++) {
            masking += maskingChar;
        }

        return masking + origin.substring(length);
    }

    /**
     * 첫글자만 남겨놓고 모두 O로 처리
     * @param name
     * @return
     */
    public static String maskingName(String name, String maskingChar) {

        String maskedName = "";    // 마스킹 이름
        String firstName = "";    // 성
        String lastName = "";    // 이름
        int lastNameStartPoint;    // 이름 시작 포인터

        if(!name.equals("") || name != null){
            if(name.length() > 1){
                firstName = name.substring(0, 1);
                lastNameStartPoint = name.indexOf(firstName);
                lastName = name.substring(lastNameStartPoint + 1, name.length());

                String makers = "";

                for(int i = 0; i < lastName.length(); i++){
                    makers += maskingChar;
                }

                lastName = lastName.replace(lastName, makers);
                maskedName = firstName + lastName;
            }else{
                maskedName = maskingChar;
            }
        }

        return maskedName;
    }

    /**
     * 앱 및 웹 사용자의 비밀번호 복잡도 검사
     * @param password
     * @return
     */
    public static boolean validate_password_for_user(String password) {
        if (StringUtils.isEmpty(password)) return false;
        return password.matches(passwd_validation_user);
    }

    /**
     * CMS사용자의 비밀번호 복잡도 검사
     * @param password
     * @return
     */
    public static boolean validate_password_for_cms(String password) {
        if (StringUtils.isEmpty(password)) return false;
        return password.matches(passwd_validation_cms);
    }

    public static String encodeToLong(long value) {
        final StringBuilder sb = new StringBuilder();
        do {
            int i = (int)(value % 62);
            sb.append(BASE62[i]);
            value /= 62;
        } while (value > 0);
        return sb.toString();
    }

    public static String getReplaceEmoji(String str) {

        if(str == null)
                return null;

        Pattern emoticons = Pattern.compile("[\\uD83C-\\uDBFF\\uDC00-\\uDFFF]+");

        Matcher emoticonsMatcher = emoticons.matcher(str);

        str = emoticonsMatcher.replaceAll("");

        return str;

    }



}

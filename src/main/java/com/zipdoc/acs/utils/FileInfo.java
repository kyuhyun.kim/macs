package com.zipdoc.acs.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class FileInfo {
	
	private static final Logger log = LoggerFactory.getLogger(FileInfo.class);
	
	private File file;
	
	private String originFilename;
	private String extension;
	private long size;
	
	/**
	 * 사용자가 업로드 한 파일을 저장
	 * @param uploadFile
	 * @param filename
	 * @throws Exception
	 */
	public FileInfo(MultipartFile uploadFile, String filename) throws Exception {

		// 파일명이 없을 경우
		if (uploadFile.getOriginalFilename() == null) {
			throw new Exception ("No filename.");
		}
		
		// 사용자 원본 파일명
		originFilename = uploadFile.getOriginalFilename();
		
		// dot 위치 확인
		int idx = uploadFile.getOriginalFilename().lastIndexOf(".");
		
		if (idx == -1) {
			throw new Exception ("No file extension. ("+uploadFile.getOriginalFilename()+")");
		}
		
		// 확장자 추출
		extension = uploadFile.getOriginalFilename().substring(idx+1);
		log.info( " FileInfo extension : " +extension);
		if (StringUtils.isEmpty(extension)) {
			throw new Exception ("No file extension. ("+uploadFile.getOriginalFilename()+")");
		}
		
		// 파일 사이즈
		size = uploadFile.getSize();
		
		// 파일 정보 저장
		file = new File(filename+"."+extension);
	}
	
	/**
	 * 여러장의 파일을 압축하여 하나의 ZIP파일로 구성한다.
	 * @param list
	 * @param filename
	 */
	public FileInfo(LinkedList<FileInfo> list, String path, String filename) throws Exception {
		
		// 파일 확장자
		extension = "tgz";
		
		// 파일 정보 저장
		file = new File(path + "/" + filename+"."+extension);
		
		// 사용자 원본 파일명
		originFilename = file.getName();

		// 시스템 명령어
		String tgz = "tar -zcvf "+file.getName();
		
		for (int i = 0; i < list.size(); i++) {
			tgz += (" " + list.get(i).getFile().getName());
		}
		
		String [] cmd = { "/bin/sh", "-c", "cd "+path+"; "+tgz };
		
		try {

		    Process p = Runtime.getRuntime().exec(cmd);
		    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    String line = null;

		    while((line = br.readLine()) != null){
		    	log.debug(line);
		    }

		} catch(Exception e){
			log.debug("[UTILS] 파일 압축 명령 실패 ["+cmd[2]+"]", e);
		}
	}

	public File getFile() {
		return file;
	}

	public String getOriginFilename() {
		return originFilename;
	}

	public String getExtension() {
		return extension;
	}

	public long getSize() {
		return size;
	}

	public boolean isImageFile(){
		String lower = extension.toLowerCase();
		if (!lower.equals("png") &&
				!lower.equals("jpg") && !lower.equals("jpeg") &&
				!lower.equals("gif")) {
			return false;
		}
		return true;
	}
}

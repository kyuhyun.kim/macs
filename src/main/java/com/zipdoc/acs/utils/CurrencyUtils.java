package com.zipdoc.acs.utils;

public class CurrencyUtils {

    private static String[] smallForm = {"영", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구"};
    private static String[] mediumForm = {"", "십", "백", "천"};
    private static String[] largeForm = {"", "만", "억", "조"};

    public static String getKoreanCurrency(int currency) {
        if (currency == 0) {
            return smallForm[0] + "원";
        }
        StringBuffer rvsBuffer = new StringBuffer();
        int remainder = 0;
        for (int i = 0; i < 4; i++) {
            rvsBuffer.append(largeForm[i]);
            for (int j = 0; j < 4; j++) {
                remainder = currency % 10;
                if (remainder > 0) {
                    rvsBuffer.append(mediumForm[j]);
                    if (remainder == 1) {
                        if(j == 0 || rvsBuffer.toString().length() == 0) {
                            rvsBuffer.append(smallForm[1]);
                        }
                    } else if (remainder > 1) {
                        rvsBuffer.append(smallForm[remainder]);
                    }
                }
                currency /= 10;
                if (currency == 0) break;
            }
            if (currency == 0) break;
        }
        return rvsBuffer.reverse().toString() + "원";
    }

    public static String getNumberKoreanCurrency(int curreny) {
        if (curreny == 0) {
            return "0원";
        }
        StringBuffer rvsBuffer = new StringBuffer();
        int remainder = 0;
        for (int i = 0; i < 4; i++) {
            remainder = curreny % 10000;
            if (remainder > 0) {
                rvsBuffer.append(" " + largeForm[i]);
                for (int j = 0; j < 4; j++) {
                    rvsBuffer.append(remainder % 10);
                    remainder /= 10;
                    if (remainder == 0) break;
                }
            }
            curreny /= 10000;
            if (curreny == 0) break;
        }
        return rvsBuffer.reverse().toString().trim() + "원";
    }
}

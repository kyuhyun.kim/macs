
package com.zipdoc.acs.utils;


import org.apache.commons.configuration.CompositeConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AcsConfig {

    @Autowired
    private CompositeConfiguration  runtimeProperties;

//    @PostConstruct
//    private void init() {
//        try {
//            String filePath = PropertiesConstants.PROPERTIES_FILE_PATH;
//            System.out.println("Loading the properties file: " + filePath);
//            configuration = new PropertiesConfiguration(filePath);
//            //Create new FileChangedReloadingStrategy to reload the properties file based on the given time interval
//            FileChangedReloadingStrategy fileChangedReloadingStrategy = new FileChangedReloadingStrategy();
//            fileChangedReloadingStrategy.setRefreshDelay(PropertiesConstants.REFRESH_DELAY);
//            configuration.setReloadingStrategy(fileChangedReloadingStrategy);
//        } catch (ConfigurationException e) {
//            e.printStackTrace();
//        }
//    }


    public String getProperty(String key) {
        return (String) runtimeProperties.getProperty(key);
    }

    public Integer getPropertyToInteger(String key) {
        return Integer.valueOf((String) runtimeProperties.getProperty(key));
    }


//
//    public void setProperty(String key, Object value) {
//        configuration.setProperty(key, value);
//    }
//
//
//
//    public String getSpecial_svc_cd() {
//        return runtimeProperties.getString("special_service_code");
//    }
//
//
//
//    @Override
//    public String toString() {
//        final StringBuilder sb = new StringBuilder("{");
//        Iterator<String> keys = runtimeProperties.getKeys();
//        boolean isFirst = true;
//
//        while (keys.hasNext()) {
//            String key = keys.next();
//            if (isFirst) {
//                sb.append(key).append("=").append(runtimeProperties.getProperty(key));
//                isFirst = false;
//            } else {
//                sb.append(", ").append(key).append("=").append(runtimeProperties.getProperty(key));
//            }
//        }
//        sb.append("}");
//
//        return sb.toString();
//    }
}

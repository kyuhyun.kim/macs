package com.zipdoc.acs.utils;


import com.zipdoc.acs.domain.entity.Estimate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class ParseRereferUrl {
    final String NAVER_SA_MOBILE1 = "m.ad.search.naver.com/search.naver";
    final String NAVER_SA_MOBILE2 = "m.search.naver.com/search.naver";

    //  NAVER_SA_PC,  오가닉과 같이 사용
    final String NAVER_SA_PC = "search.naver.com/search.naver";

    // 네이버 sa, gdn 같이 사용
    final String ZIPDOC_HOME = "zipdoc.co.kr/zws/index.do";

    // 다음SA pc
    final String DAUM_ZIPDOC = "www.zipdoc.co.kr/zws";
    final String DAUM_ZIPDOC_PARAM1 = "DMKW=";
    final String DAUM_ZIPDOC_PARAM2 = "DMSKW=";
    final String DAUM_ZIPDOC_PARAM3 = "DMCOL=AM";

    final String DAUM_SA1 = "tip.daum.net/question/";
    final String DAUM_SA2 = "search.daum.net/search";
    final String DAUM_SA3 = "search.daum.net/search";

    // 다음SA m
    final String DAUM_ZIPDOC_M = "www.zipdoc.co.kr/zws";
    final String DAUM_ZIPDOC_M_PARAM1 = "DMKW=";
    final String DAUM_ZIPDOC_M_PARAM2 = "DMSKW=";
    final String DAUM_ZIPDOC_M_PARAM3 = "DMCOL=MOBILESA";

    final String DAUM_SA_M1 = "m.tip.daum.net/question/";
    final String DAUM_SA_M2 = "m.search.daum.net/search";

    // 다음오가닉pc
    final String DAUM_ORG_PC1 = "search.daum.net/search";
    final String DAUM_ORG_PC2 = "search.daum.net/search";
    final String DAUM_ORG_PARAM1 = "q=wlqekr";

    // 다음오가닉 m(다음오가닉pc과 하위 로직은 동일 )
    final String DAUM_ORG_M = "m.search.daum.net/";

    // 구글SA
    final String GOOG_SA = "www.google.co.kr/search?";

//    final String GDN1                   = "https://googleads.g.doubleclick.net/pagead/ads";

    // 구글DA
    final String GOOG_DA = "googleads.g.doubleclick.net/pagead/ads";
    final String GOOG_DA_cpa1 = "utm_souce=gdn_cpa1";

//    final String GDN_q1         = "src=";
//    final String GDN_q2         = "kw=";

    final String GDN_gclid = "gclid=";

    // 카카오AD
    final String KAKAO_AD_PARAM1 = "utm_source=kakao";
    final String KAKAO_AD_PARAM2 = "utm_medium=display_cpc";

    final String KAKAO_AD_P1 = "m.media.daum.net/m/entertain/photo-viewer";
    final String KAKAO_AD_P2 = "m.cafe.daum.net";
    final String KAKAO_AD_P3 = "svc=kakaotalkTab";

    // 카카오플러스
    final String KAKAO_PLUS = "utm_source=kakaoplus";

    // 서치엔
    final String SEARCHN = "sccl.dreamad.co.kr/Referer.asp";

    // 페이스북
    final String FACEBOOK_1 = "m.facebook.com";
    final String FACEBOOK_2 = "l.facebook.com";
    final String FACEBOOK_3 = "www.facebook.com/";


    /*
        query=집닥 또는
        query=인테리어 집닥 또는
        query=인테리어집닥 또는
        query=집닥 인테리어 또는
        query=집닥인테리어 또는
        query=wlqekr
         */
    String STR_ZIPDOC = null;
    String STR_INT_ZIPDOC = null;
    String STR_INTZIPDOC = null;
    String STR_ZIPDOC_INT = null;
    String STR_ZIPDOCINT = null;
    String STR_wlqekr = null;

    String STR_ZIPDOC_D = null;
    String STR_INT_ZIPDOC_D = null;
    String STR_INTZIPDOC_D = null;
    String STR_ZIPDOC_INT_D = null;
    String STR_ZIPDOCINT_D = null;
    String STR_wlqekr_D = null;

    // ! setZipdocKeyword before use
    public boolean checkZipdocKeyWord(String url) {
        if (url.contains(STR_ZIPDOC) ||
                url.contains(STR_INT_ZIPDOC) ||
                url.contains(STR_INTZIPDOC) ||

                url.contains(STR_ZIPDOC_INT) ||
                url.contains(STR_ZIPDOCINT) ||
                url.contains(STR_wlqekr)) {
            return true;
        }
        return false;
    }

    public boolean checkDaumKeyWord(String url) {
        if (url.contains(STR_ZIPDOC_D) ||
                url.contains(STR_INT_ZIPDOC_D) ||
                url.contains(STR_INTZIPDOC_D) ||

                url.contains(STR_ZIPDOC_INT_D) ||
                url.contains(STR_ZIPDOCINT_D) ||
                url.contains(STR_wlqekr_D)) {
            return true;
        }
        return false;
    }

    //네이버, 다음 오가닉
    public void setZipdocKeyword() {

        STR_ZIPDOC = "query=집닥";
        STR_INT_ZIPDOC = "query=인테리어 집닥";
        STR_INTZIPDOC = "query=인테리어집닥";
        STR_ZIPDOC_INT = "query=집닥 인테리어";
        STR_ZIPDOCINT = "query=집닥인테리어";
        STR_wlqekr = "query=wlqekr";

        STR_ZIPDOC_D = "q=집닥";
        STR_INT_ZIPDOC_D = "q=인테리어 집닥";
        STR_INTZIPDOC_D = "q=인테리어집닥";
        STR_ZIPDOC_INT_D = "q=집닥 인테리어";
        STR_ZIPDOCINT_D = "query=집닥인테리어";
        STR_wlqekr_D = "q=wlqekr";

    }

    enum RefererType{

        NAVER_ORGARNIC(1001),
        NAVER_ORGARNIC_PC(1002),
        NAVER_ORGARNIC_M(1003),
        NAVER_SA(1004),
        NAVER_SA_PC(1005),
        NAVER_SA_M(1006),
        GOOGLE_DA(1007),
        GOOGLE_SA(1008),
        DAUM_SA_PC(1009),
        DAUM_ORGARNIC_PC(1010),
        DAUM_ORGARNIC_M(1011),
        DAUM_SA_M(1012),
        KAKAO_PLUS(1013),
        KAKAO_AD(1014),
        SEARCN_N_PLUS(1015),
        FACEBOOK(1016)
        ;

        private int code;

        RefererType(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }

        public static int get(int code){
            for(RefererType item : values()){
                if(item.getCode() == code){
                    return item.getCode();
                }
            }
            return 0;
        }
    }

    Estimate estimate = null;
    List<Estimate> parsedList = new ArrayList<>();

    public List<Estimate> parseUrl(List<Estimate> list) throws UnsupportedEncodingException{
        setZipdocKeyword();

        for (int i = 0; i < list.size(); i++) {
            estimate = list.get(i);

            final String REF_STR = URLDecoder.decode(estimate.getReferer(), "UTF-8");

            // logic start
            // if (REF_STR != null && !REF_STR.equals("")) {
            //네이버 SA M
            if (REF_STR.contains(NAVER_SA_MOBILE1) || REF_STR.contains(NAVER_SA_MOBILE2)) {

                if (checkZipdocKeyWord(REF_STR)) {
                    //네이버 오가닉
                    estimate.setReferer_code(RefererType.NAVER_ORGARNIC_M.getCode());
                } else {
                    //네이버SA M
                    estimate.setReferer_code(RefererType.NAVER_SA_M.getCode());
                }

            } else if (REF_STR.contains(ZIPDOC_HOME)
                    && REF_STR.contains("n_media=")
                    && REF_STR.contains("n_query=")
                    && REF_STR.contains("n_rank=")
                    && REF_STR.contains("n_ad_group=")
                    && REF_STR.contains("n_ad=")
                    && REF_STR.contains("n_keyword_id=")
                    && REF_STR.contains("n_keyword=")
                    && REF_STR.contains("n_campaign_type=")
                    && REF_STR.contains("NaPm=")
                    ) {

                estimate.setReferer_code(RefererType.NAVER_SA.getCode());

            } else if (REF_STR.contains(NAVER_SA_PC)) {

                if (checkZipdocKeyWord(REF_STR)) {
                    estimate.setReferer_code(RefererType.NAVER_ORGARNIC_PC.getCode());
                } else {
                    estimate.setReferer_code(RefererType.NAVER_SA_PC.getCode());
                }

            } else if (REF_STR.contains(GOOG_DA)) {
                estimate.setReferer_code(RefererType.GOOGLE_DA.getCode());

            } else if (REF_STR.contains(ZIPDOC_HOME) &&
                    REF_STR.contains("src=") &&
                    REF_STR.contains("kw=")) {

                if (REF_STR.contains("utm_source=gdn_cpa1") || REF_STR.contains("gclid=")) {
                    estimate.setReferer_code(RefererType.GOOGLE_DA.getCode());
                }

            } else if (REF_STR.contains(ZIPDOC_HOME)) {
                if (REF_STR.contains(GDN_gclid)) {
                    estimate.setReferer_code(RefererType.GOOGLE_SA.getCode());

                } else if (REF_STR.contains("utm_source=naver") &&
                        REF_STR.contains("utm_medium=keyword_brand") &&
                        REF_STR.contains("utm_campaign=1801_PC_zipdoc")) {

                    estimate.setReferer_code(RefererType.NAVER_ORGARNIC_PC.getCode());
                } else if (REF_STR.contains("DMKW=") &&
                        REF_STR.contains("DMSKW=") &&
                        REF_STR.contains("DMCOL=AM")) {
                    estimate.setReferer_code(RefererType.DAUM_SA_PC.getCode());

                }
            } else if (REF_STR.contains("http://tip.daum.net/question")) {
                estimate.setReferer_code(RefererType.DAUM_SA_PC.getCode());
            } else if (REF_STR.contains("search.daum.net/search")) {
                // https:// http:// 동일
                if (checkDaumKeyWord(REF_STR)) {
                    estimate.setReferer_code(RefererType.DAUM_ORGARNIC_PC.getCode());
                } else {
                    estimate.setReferer_code(RefererType.DAUM_SA_PC.getCode());
                }
            } else if (REF_STR.contains("http://m.tip.daum.net/question/")) {

            } else if (REF_STR.contains("https://m.search.daum.net/")) {
                if (checkDaumKeyWord(REF_STR)) {
                    estimate.setReferer_code(RefererType.DAUM_ORGARNIC_M.getCode());
                } else {
                    estimate.setReferer_code(RefererType.DAUM_SA_M.getCode());
                }
            } else if (REF_STR.contains("DMKW=") &&
                    REF_STR.contains("DMSKW=") &&
                    REF_STR.contains("DMCOL=MOBILESA")) {
                estimate.setReferer_code(RefererType.DAUM_SA_M.getCode());

            } else if (REF_STR.contains(GOOG_SA)) {
                estimate.setReferer_code(RefererType.GOOGLE_SA.getCode());
            } else if (REF_STR.contains(ZIPDOC_HOME) ||
                    REF_STR.contains("m.media.daum.net/m/entertain/photo-viewer") ||
                    REF_STR.contains("m.cafe.daum.net")) {

                if (REF_STR.contains(KAKAO_PLUS)) {
                    estimate.setReferer_code(RefererType.KAKAO_PLUS.getCode());
                } else {
                    estimate.setReferer_code(RefererType.KAKAO_AD.getCode());
                }


            } else if (REF_STR.contains(SEARCHN)) {
                estimate.setReferer_code(RefererType.SEARCN_N_PLUS.getCode());

            } else if (REF_STR.contains(FACEBOOK_1) ||
                    REF_STR.contains(FACEBOOK_2) ||
                    REF_STR.contains(FACEBOOK_3)) {
                estimate.setReferer_code(RefererType.FACEBOOK.getCode());
            }

            parsedList.add(estimate);
        }

        return parsedList;
    }


    public Estimate parseUrl(Estimate estimate) throws UnsupportedEncodingException{
        setZipdocKeyword();

        if(estimate.getReferer() != null) {


            final String REF_STR = URLDecoder.decode(estimate.getReferer(), "UTF-8");

            // logic start
            // if (REF_STR != null && !REF_STR.equals("")) {
            //네이버 SA M
            if (REF_STR.contains(NAVER_SA_MOBILE1) || REF_STR.contains(NAVER_SA_MOBILE2)) {

                if (checkZipdocKeyWord(REF_STR)) {
                    //네이버 오가닉
                    estimate.setReferer_code(RefererType.NAVER_ORGARNIC_M.getCode());
                } else {
                    //네이버SA M
                    estimate.setReferer_code( RefererType.NAVER_SA_M.getCode());
                }

            } else if (REF_STR.contains(ZIPDOC_HOME)
                    && REF_STR.contains("n_media=")
                    && REF_STR.contains("n_query=")
                    && REF_STR.contains("n_rank=")
                    && REF_STR.contains("n_ad_group=")
                    && REF_STR.contains("n_ad=")
                    && REF_STR.contains("n_keyword_id=")
                    && REF_STR.contains("n_keyword=")
                    && REF_STR.contains("n_campaign_type=")
                    && REF_STR.contains("NaPm=")
                    ) {

                estimate.setReferer_code( RefererType.NAVER_SA.getCode());

            } else if (REF_STR.contains(NAVER_SA_PC)) {

                if (checkZipdocKeyWord(REF_STR)) {
                    estimate.setReferer_code( RefererType.NAVER_ORGARNIC_PC.getCode());
                } else {
                    estimate.setReferer_code( RefererType.NAVER_SA_PC.getCode());
                }

            } else if (REF_STR.contains(GOOG_DA)) {
                estimate.setReferer_code( RefererType.GOOGLE_DA.getCode());

            } else if (REF_STR.contains(ZIPDOC_HOME) &&
                    REF_STR.contains("src=") &&
                    REF_STR.contains("kw=")) {

                if (REF_STR.contains("utm_source=gdn_cpa1") || REF_STR.contains("gclid=")) {
                    estimate.setReferer_code( RefererType.GOOGLE_DA.getCode());
                }

            } else if (REF_STR.contains(ZIPDOC_HOME)) {
                if (REF_STR.contains(GDN_gclid)) {
                    estimate.setReferer_code( RefererType.GOOGLE_SA.getCode());

                } else if (REF_STR.contains("utm_source=naver") &&
                        REF_STR.contains("utm_medium=keyword_brand") &&
                        REF_STR.contains("utm_campaign=1801_PC_zipdoc")) {

                    estimate.setReferer_code( RefererType.NAVER_ORGARNIC_PC.getCode());
                } else if (REF_STR.contains("DMKW=") &&
                        REF_STR.contains("DMSKW=") &&
                        REF_STR.contains("DMCOL=AM")) {
                    estimate.setReferer_code( RefererType.DAUM_SA_PC.getCode());

                }
            } else if (REF_STR.contains("http://tip.daum.net/question")) {
                estimate.setReferer_code( RefererType.DAUM_SA_PC.getCode());
            } else if (REF_STR.contains("search.daum.net/search")) {
                // https:// http:// 동일
                if (checkDaumKeyWord(REF_STR)) {
                    estimate.setReferer_code( RefererType.DAUM_ORGARNIC_PC.getCode());
                } else {
                    estimate.setReferer_code( RefererType.DAUM_SA_PC.getCode());
                }
            } else if (REF_STR.contains("http://m.tip.daum.net/question/")) {

            } else if (REF_STR.contains("https://m.search.daum.net/")) {
                if (checkDaumKeyWord(REF_STR)) {
                    estimate.setReferer_code( RefererType.DAUM_ORGARNIC_M.getCode());
                } else {
                    estimate.setReferer_code( RefererType.DAUM_SA_M.getCode());
                }
            } else if (REF_STR.contains("DMKW=") &&
                    REF_STR.contains("DMSKW=") &&
                    REF_STR.contains("DMCOL=MOBILESA")) {
                estimate.setReferer_code( RefererType.DAUM_SA_M.getCode());

            } else if (REF_STR.contains(GOOG_SA)) {
                estimate.setReferer_code( RefererType.GOOGLE_SA.getCode());
            } else if (REF_STR.contains(ZIPDOC_HOME) ||
                    REF_STR.contains("m.media.daum.net/m/entertain/photo-viewer") ||
                    REF_STR.contains("m.cafe.daum.net")) {

                if (REF_STR.contains(KAKAO_PLUS)) {
                    estimate.setReferer_code( RefererType.KAKAO_PLUS.getCode());
                } else {
                    estimate.setReferer_code( RefererType.KAKAO_AD.getCode());
                }


            } else if (REF_STR.contains(SEARCHN)) {
                estimate.setReferer_code( RefererType.SEARCN_N_PLUS.getCode());

            } else if (REF_STR.contains(FACEBOOK_1) ||
                    REF_STR.contains(FACEBOOK_2) ||
                    REF_STR.contains(FACEBOOK_3)) {
                estimate.setReferer_code( RefererType.FACEBOOK.getCode());
            }


        }

        return estimate;
    }
}

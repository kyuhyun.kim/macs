package com.zipdoc.acs.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by kimjh on 2016-04-04.
 */
public class DateUtil {

    public static final String DATE_FORMAT = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT_FOR_IM = "yyyyMMddHHmmssZ";
    public static final String DATE_FORMAT_FOR_SEACH = "yyyy-MM-dd";
    public static final String DATE_FORMAT_FOR_MILLISECOND = "yyyyMMddHHmmssSSS";
    public static final String DATE_FORMAT_FOR_DISPLAY_MILLISECOND = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_FOR_DISPLAY_MINUTE = "yyyy-MM-dd HH:mm";

    public static String toFormatString(long time, String format) {
        DateFormat df  = new SimpleDateFormat(format);
        java.util.Date now = new java.util.Date(time);

        return df.format(now);
    }

    public static String toFormatString(java.util.Date date) {
        return toFormatString(date, DATE_FORMAT_FOR_SEACH);
    }

    public static String toFormatString(java.util.Date date, String format) {
        if(date == null || StringUtils.isEmpty(format)) return "";
        DateFormat df  = new SimpleDateFormat(format);
        return df.format(date);
    }

    public static String toFormatString(String format) {
        return toFormatString(System.currentTimeMillis(), format);
    }
    public static String toFormatString(long time) {
        return toFormatString(time, DATE_FORMAT);
    }
    public static String toFormatString() {
        return toFormatString(System.currentTimeMillis(), DATE_FORMAT);
    }
    public static String toImFormatString() {
        String defaultFormatString = toFormatString(DATE_FORMAT_FOR_IM);

        StringBuffer sb = new StringBuffer();
        sb.append(defaultFormatString.substring(0 , 4 )); sb.append("-");
        sb.append(defaultFormatString.substring(4 , 6 )); sb.append("-");
        sb.append(defaultFormatString.substring(6 , 8 )); sb.append("T");
        sb.append(defaultFormatString.substring(8 , 10)); sb.append(":");
        sb.append(defaultFormatString.substring(10, 12)); sb.append(":");
        sb.append(defaultFormatString.substring(12, 14));
        sb.append(defaultFormatString.substring(14));

        return sb.toString();
    }

    public static String toDisplayFormatString(String timeStr) {
        if(StringUtils.length(timeStr)!=14) return timeStr;
        StringBuffer sb = new StringBuffer();
        sb.append(timeStr.substring(0 , 4 )); sb.append("-");
        sb.append(timeStr.substring(4 , 6 )); sb.append("-");
        sb.append(timeStr.substring(6 , 8 )); sb.append(" ");
        sb.append(timeStr.substring(8 , 10)); sb.append(":");
        sb.append(timeStr.substring(10, 12)); sb.append(":");
        sb.append(timeStr.substring(12, 14));
        sb.append(timeStr.substring(14));

        return sb.toString();
    }

    public static long toFormatTimeMillis(String timeStr, String format) throws ParseException {
        DateFormat df   = new SimpleDateFormat(format);
        java.util.Date date = df.parse(timeStr);

        return date.getTime();
    }

    public static void main(String[] args) throws ParseException {
        long time = toFormatTimeMillis("20120901230000", "yyyyMMddHHmmss");

        System.out.println("time   : " + time + ", time: " + toFormatString(time, "yyyyMMddHHmmss"));
        System.out.println("ImTime1: " + toImFormatString());
    }


    public static String doDateAddMonth(int month){
        Calendar cal = new GregorianCalendar(Locale.KOREA);
        cal.setTime(new java.util.Date());
        //cal.add(Calendar.YEAR, 1); // 년을 더한다.

        cal.add(Calendar.MONTH, month); // 달을 더한다.

        //cal.add(Calendar.DAY_OF_YEAR, 1); // 일자를 더한다.
        //cal.add(Calendar.HOUR, 1); // 시간을 더한다

        SimpleDateFormat fm = new SimpleDateFormat(DATE_FORMAT_FOR_SEACH);

        String strDate = fm.format(cal.getTime());
        return strDate;
    }

    public static java.util.Date toFormatDate(String str){
        return toFormatDate(str, DATE_FORMAT_FOR_SEACH);
    }

    public static java.util.Date toFormatDate(String str, String format) {
        if(StringUtils.isEmpty(str) || StringUtils.isEmpty(format)) return null;
        try {
            return DateUtils.parseDate(str, new String[]{format});
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date getDateNow(){
        return new Date(Calendar.getInstance().getTimeInMillis());
    }

    public static int getCurrentYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public static int getCurrentMonth() {
        return Calendar.getInstance().get(Calendar.MONTH) + 1;
    }

    public static Timestamp getNow() {
        return new Timestamp(Calendar.getInstance().getTimeInMillis());
    }

    public static Timestamp getStartOfToday(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp getEndOfToday(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        return new Timestamp(cal.getTimeInMillis());
    }

    //2017-04-21 현재일자에서 7일더한 값 구하는 메소드
    public static java.util.Date toFormatAddWeekString() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);

        //23:59:59초로 맞춰야함
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE,59);
        cal.set(Calendar.SECOND,59);

        java.util.Date date = cal.getTime();
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);

        String strDate = dateFormatter.format(date.getTime());

        return DateUtil.toFormatDate(strDate,DATE_FORMAT);

    }

}

package com.zipdoc.acs.utils;

import java.io.File;
import java.io.FilenameFilter;

public class ImageFileFilter implements FilenameFilter {

	private String [] filters;
	
	public ImageFileFilter(String filter) {
		filters = new String [1];
		filters[0] = filter;
	}
	
	public ImageFileFilter(String [] filter) {
		filters = filter;
	}
	
	@Override
	public boolean accept(File dir, String name) {
		for (int i = 0; i < filters.length; i++) {
			if (filters[i] != null && name.startsWith(filters[i])) return true;
		}
		return false;
	}
}

package com.zipdoc.acs.domain.pns;

import org.apache.commons.lang.StringUtils;

public enum MsgType {
    PROMOTION("promotion",   	"이벤트/프로모션"),
    NOTICE("notice", 			"공지사항"),
    E100("E100",               "신규 견적 요청 도착 알림"),
    E101("E101",               "신규 견적 수락/거절 미선택 알림"),
    E198("E198",               "신규 견적 자동 거절 알림(시스템에 의해 회수)"),
    E199("E199",               "신규 견적이 관리자에 의해 회수됨"),
    E200("E200",               "진행 상황 결과 입력 요청 알림"),
    E210("E210",               "협의확인서 협의요청 알림"),
    E211("E211",               "협의확인서 피드백 알림"),
    E212("E212",               "협의확인서 승인 알림"),
    E280("E280",               "완공 확인 요청 알림"),
    E281("E281",               "집닥맨 현장 방문 사전 알림"),
    E298("E298",               "타 파트너사와 계약되어 견적 종료됨"),
    E299("E299",               "관리자에의해 견적이 종료됨"),
    M100("M100",               "파트너스 비용 결제일 알림"),
    M101("M101",               "미납 수수료 알림"),
    INSTANCE_MSG("instance_msg",	"일반알림"),
    ZIPDOCMAN("zipdocman",	"집닥맨앱"),
    ;

    private String code;
    private String name;
    MsgType(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }
    public String getName(){
        return name;
    }
    public static MsgType get(String code){
        for(MsgType item : values()){
            if(StringUtils.equals(code, item.getCode())){
                return item;
            }
        }
        return null;
    }
}

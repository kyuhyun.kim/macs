package com.zipdoc.acs.domain.pns;

import com.zipdoc.acs.define.OsType;
import com.zipdoc.acs.domain.entity.MemberDevice;
import com.zipdoc.acs.domain.service.SendMessageService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.utils.Messages;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

@Service
public class MessageSenderImpl implements MessageSender, Runnable {

	private static final Logger logger = LoggerFactory.getLogger("PNS.RESULT");
	
	private static boolean isSupport = BooleanUtils.toBoolean(Messages.getMessage("FCM.SUPPORT"));
	private static int PUSH_THREAD_COUNT = NumberUtils.toInt(Messages.getMessage("FCM.THREAD.COUNT"), 1);
	private static String SERVER_KEY = Messages.getMessage("FCM.SERVER_KEY");


	private LinkedBlockingQueue<SendMessage> queue = new LinkedBlockingQueue<SendMessage>();
	
	private Thread[] threads = null;

	@Autowired
	private SendMessageService sendMessageService;

	@PostConstruct
	public void initialize(){
		shutdown();
		threads = new Thread[PUSH_THREAD_COUNT];
		for( int idx=0; idx < threads.length; idx++ ){
			threads[idx] = new Thread(this);
			threads[idx].setName(this.getClass().getName()+"_"+idx);
			threads[idx].start();
			
			logger.debug(threads[idx].getName()+" is started");
		}
		
		logger.info(this.getClass().getName()+" is initialized");
		
	}
	
	
	@PreDestroy
	public void shutdown(){
		if( threads != null ){
			for( int idx=0; idx < threads.length; idx++ ){
				if( threads[idx] != null ){
					threads[idx].interrupt();
					try {
						threads[idx].join();
					} catch (InterruptedException e) {
					}
					logger.debug(threads[idx].getName()+" is closed");
				}
			}
			logger.info(this.getClass().getName()+" shutdown");
		}
	}
	
	
	@Override
	public String getMsgText(String msgType) throws ServiceException {
		return Messages.getMessage(msgType);
	}
	
	
	@Override
	public void sendPush(SendMessage sendMessage) throws ServiceException{
		try {
			queue.put(sendMessage);
			logger.info(this.getClass().getName()+"|ADD|"+sendMessage.toString());
		} catch (InterruptedException e) {
			e.printStackTrace();
			logger.info(this.getClass().getName()+"|FAIL_ADD|"+sendMessage.toString());
		}
	}
	
	@Override
	public void run() {
		while(!Thread.currentThread().isInterrupted()){
			SendMessage data = null;
			try{
				data = queue.poll( 1, TimeUnit.SECONDS );
				if( data != null ){
					send(data);
					continue;
				}
			}
			catch(InterruptedException e){
				Thread.currentThread().interrupt();
				break;
			}
			catch(Exception e){
				logger.error(this.getClass().getName()+"|skip|"+e.getMessage()+"|"+( data != null ? data.toString() : ""));
			}
			
		}
		logger.warn(Thread.currentThread().getName()+" exit run.");
	}

	private void send(SendMessage sendMessage)  {
		StringBuilder sb = new StringBuilder();
		sb.append(sendMessage.toString());

		try{

			if( isSupport ){

				if(sendMessage.isSendToAll()) { //OS유 구분없이 전송하는 경우
					if (sendMessage.getTargetList() != null && sendMessage.getTargetList().size() > 0) {
						PushSender pushSender = new PushSender(SERVER_KEY);
						pushSender.send(sendMessage);
					}
				} else { //OS유형 구분하여 전송하는 경우

					//안드로이드
					if (sendMessage.getTargetAndroidList() != null && sendMessage.getTargetAndroidList().size() > 0) {
						sendMessage.setCurrentSendOsType(OsType.ANDROID);
						PushSender pushSender = new PushSender(SERVER_KEY);
						pushSender.send(sendMessage);
					}

					//iOS
					if (sendMessage.getTargetIOSList() != null && sendMessage.getTargetIOSList().size() > 0) {
						sendMessage.setCurrentSendOsType(OsType.IOS);
						PushSender pushSender = new PushSender(SERVER_KEY);
						pushSender.send(sendMessage);
					}
				}

				//유효하지 않은 토큰 처리
				if (sendMessage.getExpiredDevices() != null && sendMessage.getExpiredDevices().size() > 0) {
					for (MemberDevice memberDevice : sendMessage.getExpiredDevices()) {
						if (!memberDevice.isValidToken()) {
							sendMessageService.updateExpiredToken(memberDevice.getZid());
						}
					}
				}
			}
			else {
				sb.append("|RESULT=200|VALUE=OK|CAUSE=NOT SUPPORT FCM").append("\r\n").append(sendMessage.toString());
			}
		}
		catch(Exception e){
			logger.error("Fail send PUSH|"+e.getMessage()+"|"+sendMessage.toString(), e);
			sb.append("|RESULT=500|VALUE=500|CAUSE="+e.getMessage());
		}
		finally{
			logger.info(sb.toString());
		}
	}
//	private void send(SendMessage sendMessage)  {
//		StringBuilder sb = new StringBuilder();
//		sb.append(sendMessage.toString());
//
//		try{
//			if( isSupport ){
//				boolean success = false;
//
//				//푸시토큰이 있는 경우에만 수행한다.
//				if(sendMessage.getTargetList()!=null && sendMessage.getTargetList().size()>0) {
//					PushSender pushSender = new PushSender(SERVER_KEY);
//					success = pushSender.send(sendMessage);
//
//					//유효하지 않은 토큰 처리
//					for (MemberDevice memberDevice : sendMessage.getTargetList()) {
//						if (!memberDevice.isValidToken()) {
//							sendMessageService.updateExpiredToken(memberDevice.getZid());
//						}
//					}
//				}
//
//				if(!success){
//					//TODO 파트너사에 한건도 전송 못한 경우 우체국톡 전송??
//
//				}
//			}
//			else {
//				sb.append("|RESULT=200|VALUE=OK|CAUSE=NOT SUPPORT FCM").append("\r\n").append(sendMessage.toString());
//			}
//		}
//		catch(Exception e){
//			logger.error("Fail send PUSH|"+e.getMessage()+"|"+sendMessage.toString(), e);
//			sb.append("|RESULT=500|VALUE=500|CAUSE="+e.getMessage());
//		}
//		finally{
//			logger.info(sb.toString());
//		}
//	}
}

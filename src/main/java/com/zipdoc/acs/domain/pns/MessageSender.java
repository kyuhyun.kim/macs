package com.zipdoc.acs.domain.pns;

import com.zipdoc.acs.domain.service.ServiceException;


public interface MessageSender {

	public void sendPush(SendMessage msg) throws ServiceException;
	public String getMsgText(String msgType) throws ServiceException;
}

package com.zipdoc.acs.domain.pns;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.zipdoc.acs.define.OsType;
import com.zipdoc.acs.domain.entity.MemberDevice;
import com.zipdoc.acs.utils.MultiLangUtil;
import com.zipdoc.fcm.Message;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class SendMessage {
	private String msg_type;
	private String title;
	private String body;
	private Long epid;
	private String image_url;
	private Integer badge;
	private boolean targetPartner = true;
	private String action_url;

	private List<MemberDevice> targetList = new ArrayList<MemberDevice>();
	private List<MemberDevice> targetAndroidList = new ArrayList<MemberDevice>();
	private List<MemberDevice> targetIOSList = new ArrayList<MemberDevice>();
	private List<MemberDevice> expiredDevices = new ArrayList<MemberDevice>();

	// OS 구분 없이 전송할지 여부 : true(OS 구분 없이 전송), false(OS 구분하여 전송)
	private boolean sendToAll = false;
	private OsType currentSendOsType;

	public SendMessage(){

	}

	public SendMessage(String msg_type, String title, String body, Long epid){
		this.msg_type = msg_type;
		this.title = title;
		this.body = body;
		this.epid = epid;
		this.image_url = null;
		this.action_url = null;
	}

	public SendMessage(String msg_type, String title, String body, Long epid, String image_url, String action_url){
		this.msg_type = msg_type;
		this.title = title;
		this.body = body;
		this.epid = epid;
		this.image_url = image_url;
		this.action_url = action_url;
	}

	public String getAction_url() {
		return action_url;
	}

	public void setAction_url(String action_url) {
		this.action_url = action_url;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Long getEpid() {
		return epid;
	}

	public void setEpid(Long epid) {
		this.epid = epid;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getMsg_type() {
		return msg_type;
	}

	public void setMsg_type(String msg_type) {
		this.msg_type = msg_type;
	}

	public List<MemberDevice> getTargetList() {
		return targetList;
	}

	public void setTargetList(List<MemberDevice> targetList) {
		for(MemberDevice memberDevice:targetList){
			addTargetList(memberDevice);
		}
	}

	public void addTargetList(MemberDevice memberDevice){
		targetList.add(memberDevice);
		if(memberDevice.isAndroid()) {
			targetAndroidList.add(memberDevice);
		} else if(memberDevice.isIOS()) {
			targetIOSList.add(memberDevice);
		}
	}

	protected List<MemberDevice> getTargetAndroidList() {
		return targetAndroidList;
	}

	protected List<MemberDevice> getTargetIOSList() {
		return targetIOSList;
	}

	protected List<MemberDevice> getExpiredDevices() {
		return expiredDevices;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isTargetPartner() {
		return targetPartner;
	}

	public void setTargetPartner(boolean targetPartner) {
		this.targetPartner = targetPartner;
	}

	public List<String> getRegistrationIDs(){
		List<String> registrationIDs = new ArrayList<String>();

		if(isSendToAll()) { //OS구분없이 전송하는 경우
			for (MemberDevice memberDevice : targetList) {
				registrationIDs.add(memberDevice.getPush_token());
			}
		} else { //OS 구분하여 전송하는 경우
			if(isCurrentSendOsTypeAndroid()){
				for (MemberDevice memberDevice : targetAndroidList) {
					registrationIDs.add(memberDevice.getPush_token());
				}
			}
			if(isCurrentSendOsTypeIOS()){
				for (MemberDevice memberDevice : targetIOSList) {
					registrationIDs.add(memberDevice.getPush_token());
				}
			}
		}
		return registrationIDs;
	}

	protected void setExpiredToken(int index){
		if(isSendToAll()) { //OS구분없이 전송하는 경우
			if(this.targetList.size() > index) {
				MemberDevice expiredDevice = targetList.get(index);
				expiredDevice.setValidToken(false);
				expiredDevices.add(expiredDevice);
			}
		} else { //OS 구분하여 전송하는 경우
			if(isCurrentSendOsTypeAndroid()){
				MemberDevice expiredDevice = targetAndroidList.get(index);
				expiredDevice.setValidToken(false);
				expiredDevices.add(expiredDevice);
			} else if(isCurrentSendOsTypeIOS()){
				MemberDevice expiredDevice = targetIOSList.get(index);
				expiredDevice.setValidToken(false);
				expiredDevices.add(expiredDevice);
			}
		}
	}

	public boolean isSendToAll() {
		return sendToAll;
	}

	public void setSendToAll(boolean sendToAll) {
		this.sendToAll = sendToAll;
	}

	protected OsType getCurrentSendOsType() {
		return currentSendOsType;
	}

	protected void setCurrentSendOsType(OsType currentSendOsType) {
		this.currentSendOsType = currentSendOsType;
	}

	protected boolean isCurrentSendOsTypeAndroid(){
		return currentSendOsType != null && currentSendOsType == OsType.ANDROID;
	}

	protected boolean isCurrentSendOsTypeIOS(){
		return currentSendOsType != null && currentSendOsType == OsType.IOS;
	}

	public Integer getBadge() {
		return badge;
	}

	public void setBadge(Integer badge) {
		this.badge = badge;
	}

	protected Message buildMessage(){
		Message.Builder builder = new Message.Builder();
		builder.priority(Message.Builder.PRIORITY_HIGH);
		builder.contentAvailable(true);

		//OS구분없이 전송하거나, OS구분전송하여 iOS에 전송하는 경우 notification payload수록
		if(isSendToAll() || isCurrentSendOsTypeIOS()) {
			builder.addNotification("title", getTitle());
			builder.addNotification("body", MultiLangUtil.subStringBytes(getBody(), 120, true));
			builder.addNotification("sound", "default");
			if(getBadge()!=null) {
				builder.addNotification("badge", getBadge());
			}
			builder.addNotification("icon", "myicon");
		}

		builder.addData("msg_type", getMsg_type());

		//안드로이인 경우에만 data payload에 title, body 수록
		builder.addData("title", getTitle());
		builder.addData("body", getBody());

		if(isSendToAll() || isCurrentSendOsTypeAndroid()) {
			if(getBadge()!=null) {
				builder.addData("badge", getBadge());
			}
		}

		if(getEpid()!=null) {
			builder.addData("epid", "" + getEpid());
		}

		if(StringUtils.isNotEmpty(getAction_url())){
			builder.addData("action_url", getAction_url());
		}

		if(StringUtils.isNotEmpty(getImage_url())){
			builder.addData("image_url", getImage_url());
		}

		return builder.build();
	}

	@Override
	public String toString() {
		return "SendMessage{" +
				"title='" + title + '\'' +
				", action_url='" + action_url + '\'' +
				", body='" + body + '\'' +
				", epid=" + epid +
				", image_url='" + image_url + '\'' +
				", msg_type='" + msg_type + '\'' +
				", targetPartner=" + targetPartner +
				", targetList=" + targetList +
				'}';
	}
}

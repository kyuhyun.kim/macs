package com.zipdoc.acs.domain.pns;

import com.zipdoc.fcm.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 이동규 on 2016-10-06.
 */
public class PushSender {
    private static final Logger log = LoggerFactory.getLogger(PushSender.class);

    private String fcm_key;

    public PushSender(String fcm_key) {
        this.fcm_key = fcm_key;
    }

    public boolean send(String registraionID, SendMessage pushMessage) {
        // 키 설정
        Sender sender = new Sender(fcm_key);
        Message message = pushMessage.buildMessage();

        List<String> list = new ArrayList<>();
        list.add(registraionID);

        MulticastResult multiResult;

        try {

            log.debug("[FCM] [token="+registraionID+"] => "+pushMessage);

            multiResult = sender.send(message, list, 5);

            if (multiResult != null) {
                List<Result> results = multiResult.getResults();
                for (Result result : results) {
                    log.debug("gcm result: "+multiResult.toString());
                }
            }
        } catch (Exception e) {
            log.error("#### FCM error.", e);
            return false;
        }
        return true;
    }

    public boolean send(SendMessage sendMessage) {
        try {
            // 키 설정
            Sender sender = new Sender(fcm_key);


            Message fcmMessage = sendMessage.buildMessage();
            MulticastResult multiResult;

            log.debug("[FCM] " + sendMessage.toString());
            log.debug("[FCM] " + fcmMessage.toString());
            multiResult = sender.send(fcmMessage, sendMessage.getRegistrationIDs(), 5);

            if (multiResult != null) {
                List<Result> results = multiResult.getResults();
                int idx = 0;
                for (Result result : results) {
                    if(StringUtils.equals(result.getErrorCodeName(), Constants.ERROR_INVALID_REGISTRATION) ||
                            StringUtils.equals(result.getErrorCodeName(), Constants.ERROR_NOT_REGISTERED)||
                            StringUtils.equals(result.getErrorCodeName(), Constants.ERROR_MISMATCH_SENDER_ID)){
                        sendMessage.setExpiredToken(idx);
                    }
                    idx++;
                    log.debug("FCM result: "+result.toString());
                }
                log.debug(multiResult.toString());

                return (multiResult.getSuccess()>0);
            }
        } catch (Exception e) {
            log.error("#### FCM error.", e);
            return false;
        }
        return false;
    }
}


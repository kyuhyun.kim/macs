package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

/**
 * Created by ZIPDOC on 2016-06-14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerReply {

    private Long reply_no;
    private Integer partner_id;
    private String partner_name;
    private Long member_no;
    private String subject;
    private Integer point;
    private String description;
    private String logo_url;

    @JsonIgnore
    private String logo;
    @JsonIgnore
    private String file_path;
    @JsonIgnore
    private String file_name;
    @JsonIgnore
    private Date create_date;

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public Integer getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(Integer partner_id) {
        this.partner_id = partner_id;
    }

    public String getPartner_name() {
        return partner_name;
    }

    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Long getReply_no() {
        return reply_no;
    }

    public void setReply_no(Long reply_no) {
        this.reply_no = reply_no;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public void buildPictureUrl(){
        if(StringUtils.isNotEmpty(logo)){
            this.logo_url = Constants.STATIC_URL + getFile_path() + getLogo();
        } else {
            this.logo_url = Constants.STATIC_URL + getFile_path() + getFile_name();
        }
    }

    @Override
    public String toString() {
        return "PartnerReply{" +
                "reply_no=" + reply_no +
                ", partner_id=" + partner_id +
                ", partner_name='" + partner_name + '\'' +
                ", member_no=" + member_no +
                ", subject='" + subject + '\'' +
                ", point=" + point +
                ", description='" + description + '\'' +
                '}';
    }
}

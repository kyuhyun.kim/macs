package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.utils.AES128Util;
import org.apache.commons.lang.StringUtils;
import com.zipdoc.acs.utils.Messages;
import java.util.Date;

/**
 * Created by dskim on 2017. 9. 22..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContractSideHistory {

    private long contract_side_history_no;

    private long zid;

    private long estimate_no;

    private int partner_id;

    private String latitude;

    private String longitude;
    
    private Date staying_time;

    private Date create_date;

    public long getContract_side_history_no() {
        return contract_side_history_no;
    }

    public void setContract_side_history_no(long contract_side_history_no) {
        this.contract_side_history_no = contract_side_history_no;
    }

    public long getZid() {
        return zid;
    }

    public void setZid(long zid) {
        this.zid = zid;
    }

    public long getEstimate_no() {
        return estimate_no;
    }

    public void setEstimate_no(long estimate_no) {
        this.estimate_no = estimate_no;
    }

    public int getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(int partner_id) {
        this.partner_id = partner_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getStaying_time() {
		return staying_time;
	}

	public void setStaying_time(Date staying_time) {
		this.staying_time = staying_time;
	}

}

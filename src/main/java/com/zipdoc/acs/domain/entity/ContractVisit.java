package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.SiteVisitStatus;
import com.zipdoc.acs.define.SiteVisitType;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.zipdoc.acs.define.Constants.STATIC_URL;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContractVisit {

	private Long contract_no;
	private Long estimate_no;
	private Integer status;
	private Integer completed_flag;
	private String contracter;
	private Integer partner_id;
	private String contract_name;
	private Long contract_date;
	private String phone_no;
	private String address;
	private String address_detail;
	private String category_code1;
	private String category_code2;
	private String memo;
	private String point;
	private String visit_situation;
	private String visit_checklist;
	private String visit_result;

	private Long start_date;
	private Long end_date;
	private Long expect_end_date;
	private String partner_name;
	private Integer behind_contract;
	private String budget;
	private String space;
	private Long contract_price;
	private Integer complain_count;
	private Long customer_notify_date;
	private Integer contract_cancel_count;

	// 공사시작일 및 완료예정일 변경 일자
	private Long construct_change_date;

	// contract files
	private List<String> contractFiles;

	// complain files
	private List<String> complainFiles;

	@JsonIgnore private String raw_visit_history;
	private List<SiteVisitHistory> visit_history_list;

	// 감리앱 하위버전 지원 필드
	private Integer check_visit_step;
	private Long check_visit_date;
	private String check_visit_name;
	private Integer client_meeting_flag;
	private Integer partners_meeting_flag;

	public Long getContract_no() {
		return contract_no;
	}

	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}

	public Long getEstimate_no() {
		return estimate_no;
	}

	public void setEstimate_no(Long estimate_no) {
		this.estimate_no = estimate_no;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getCompleted_flag() {
		return completed_flag;
	}

	public void setCompleted_flag(Integer completed_flag) {
		this.completed_flag = completed_flag;
	}

	public String getContracter() {
		return contracter;
	}

	public void setContracter(String contracter) {
		this.contracter = contracter;
	}

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}

	public String getContract_name() {
		return contract_name;
	}

	public void setContract_name(String contract_name) {
		this.contract_name = contract_name;
	}

	public Long getContract_date() {
		return contract_date;
	}

	public void setContract_date(Long contract_date) {
		this.contract_date = contract_date;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress_detail() {
		return address_detail;
	}

	public void setAddress_detail(String address_detail) {
		this.address_detail = address_detail;
	}

	public String getCategory_code1() {
		return category_code1;
	}

	public void setCategory_code1(String category_code1) {
		this.category_code1 = category_code1;
	}

	public String getCategory_code2() {
		return category_code2;
	}

	public void setCategory_code2(String category_code2) {
		this.category_code2 = category_code2;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public String getVisit_situation() {
		return visit_situation;
	}

	public void setVisit_situation(String visit_situation) {
		this.visit_situation = visit_situation;
	}

	public String getVisit_checklist() {
		return visit_checklist;
	}

	public void setVisit_checklist(String visit_checklist) {
		this.visit_checklist = visit_checklist;
	}

	public String getVisit_result() {
		return visit_result;
	}

	public void setVisit_result(String visit_result) {
		this.visit_result = visit_result;
	}

	public Long getStart_date() {
		return start_date;
	}

	public void setStart_date(Long start_date) {
		this.start_date = start_date;
	}

	public Long getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Long end_date) {
		this.end_date = end_date;
	}

	public Long getExpect_end_date() {
		return expect_end_date;
	}

	public void setExpect_end_date(Long expect_end_date) {
		this.expect_end_date = expect_end_date;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public Integer getBehind_contract() {
		return behind_contract;
	}

	public void setBehind_contract(Integer behind_contract) {
		this.behind_contract = behind_contract;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public Long getContract_price() {
		return contract_price;
	}

	public void setContract_price(Long contract_price) {
		this.contract_price = contract_price;
	}

	public Integer getComplain_count() {
		return complain_count;
	}

	public void setComplain_count(Integer complain_count) {
		this.complain_count = complain_count;
	}

	public Integer getContract_cancel_count() {
		return contract_cancel_count;
	}

	public Long getCustomer_notify_date() {
		return customer_notify_date;
	}

	public void setCustomer_notify_date(Long customer_notify_date) {
		this.customer_notify_date = customer_notify_date;
	}

	public void setContract_cancel_count(Integer contract_cancel_count) {
		this.contract_cancel_count = contract_cancel_count;
	}

	public Long getConstruct_change_date() {
		return construct_change_date;
	}

	public void setConstruct_change_date(Long construct_change_date) {
		this.construct_change_date = construct_change_date;
	}

	public void setContractFiles(String files) {
		this.contractFiles = convertFiles(files);
	}

	public List<String> getContractFiles() {
		return contractFiles;
	}

	public void setComplainFiles(String files) {
		this.complainFiles = convertFiles(files);
	}

	public List<String> getComplainFiles() {
		return complainFiles;
	}

	public String getRaw_visit_history() {
		return raw_visit_history;
	}

	public void setRaw_visit_history(String raw_visit_history) {
		this.raw_visit_history = raw_visit_history;
		if (StringUtils.isEmpty(raw_visit_history)) return;

		try {
			String [] histories = raw_visit_history.split("(`\\|)");
			if (histories != null && histories.length > 0) {
				visit_history_list = new ArrayList<>();
				for (int i = 0, n = histories.length; i < n; i++) {
					String[] item = histories[i].split("(`\\^)");
					if (item != null && item.length > 0) {
						SiteVisitHistory history = new SiteVisitHistory();

						history.setHistory_no(new Long(item[0]));
						history.setContract_no(new Long(item[1]));
						history.setVisit_type(new Integer(item[2]));
						history.setVisit_seq(new Integer(item[3]));
						history.setVisit_status(new Integer(item[4]));

						String temp = item[5].trim();
						if (StringUtils.isNotEmpty(temp)) {
							history.setVisitors(temp);
						}

						history.setVisiting_date(new Double(item[6]).longValue());
						history.setClient_meeting_yn(new Integer(item[7]));
						history.setPartner_meeting_yn(new Integer(item[8]));

						temp = item[9].trim();
						if (StringUtils.isNotEmpty(temp)) {
							history.setMemo(temp);
						}

						visit_history_list.add(history);

//                        1 : 1차현장방문예정
//                        2 : 1차현장방문완료
//                        3 : 2차현장방문예정
//                        4 : 2차현장방문완료
//                        5 : 3차현장방문예정
//                        6 : 3차현장방문완료
//                        9 : 현장방문 미대상

						// 하위버전 호환
                        if (this.check_visit_step == null && history.getVisit_type() == SiteVisitType.SUPERVISION.getCode()) {
                            if (history.getVisit_status() == SiteVisitStatus.NONE.getCode()) {
                                this.check_visit_step = 9;
                            } else {
                                switch (history.getVisit_seq()) {
                                    case 2:
                                        this.check_visit_step = history.getVisit_status() == SiteVisitStatus.COMPLETE.getCode() ? 4 : 3;
                                        break;
                                    case 3:
                                        this.check_visit_step = history.getVisit_status() == SiteVisitStatus.COMPLETE.getCode() ? 6 : 5;
                                        break;
                                    default:
                                        this.check_visit_step = history.getVisit_status() == SiteVisitStatus.COMPLETE.getCode() ? 2 : 1;
                                        break;
                                }
                                check_visit_date = history.getVisiting_date();
                                check_visit_name = history.getVisitors();
                                client_meeting_flag = history.getClient_meeting_yn();
                                partners_meeting_flag = history.getPartner_meeting_yn();
                            }
                        }
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<SiteVisitHistory> getVisit_history_list() {
		return visit_history_list;
	}

	public void setVisit_history_list(List<SiteVisitHistory> visit_history_list) {
		this.visit_history_list = visit_history_list;
	}

	private List<String> convertFiles(String files) {
		if (files == null) return null;
		String [] array = StringUtils.split(files, ",");
		if (array == null || array.length == 0) return null;

		for (int i = 0, n = array.length; i < n; i++) {
			array[i] = STATIC_URL + array[i];
		}
		return new ArrayList<String>(Arrays.asList(array));
	}

	public Integer getCheck_visit_step() {
		return check_visit_step;
	}

	public void setCheck_visit_step(Integer check_visit_step) {
		this.check_visit_step = check_visit_step;
	}

	public Long getCheck_visit_date() {
		return check_visit_date;
	}

	public void setCheck_visit_date(Long check_visit_date) {
		this.check_visit_date = check_visit_date;
	}

	public String getCheck_visit_name() {
		return check_visit_name == null ? "" : check_visit_name;
	}

	public void setCheck_visit_name(String check_visit_name) {
		this.check_visit_name = check_visit_name;
	}

	public Integer getClient_meeting_flag() {
		return client_meeting_flag;
	}

	public void setClient_meeting_flag(Integer client_meeting_flag) {
		this.client_meeting_flag = client_meeting_flag;
	}

	public Integer getPartners_meeting_flag() {
		return partners_meeting_flag;
	}

	public void setPartners_meeting_flag(Integer partners_meeting_flag) {
		this.partners_meeting_flag = partners_meeting_flag;
	}
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;

/**
 * Created by ZIPDOC on 2017-01-10.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class InteriorProduct {
    private int product_code;
    private String product_name;
    @JsonIgnore private String contents;
    @JsonIgnore private Integer additional_fee;
    private int min_amount;
    private int default_flag;
    @JsonIgnore private String thumbnail;
    @JsonIgnore private String file_name;
    @JsonIgnore private String file_name_org;
    @JsonIgnore private String file_path;
    private String detail_url;

    public Integer getAdditional_fee() {
        return additional_fee;
    }

    public void setAdditional_fee(Integer additional_fee) {
        this.additional_fee = additional_fee;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_name_org() {
        return file_name_org;
    }

    public void setFile_name_org(String file_name_org) {
        this.file_name_org = file_name_org;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public int getMin_amount() {
        return min_amount;
    }

    public void setMin_amount(int min_amount) {
        this.min_amount = min_amount;
    }

    public int getDefault_flag() {
        return default_flag;
    }

    public void setDefault_flag(int default_flag) {
        this.default_flag = default_flag;
    }

    public int getProduct_code() {
        return product_code;
    }

    public void setProduct_code(int product_code) {
        this.product_code = product_code;
        this.detail_url = String.format(Constants.INTERIOR_PRODUCT_URL, product_code);
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDetail_url() {
        return detail_url;
    }

    public void setDetail_url(String detail_url) {
        this.detail_url = detail_url;
    }
}

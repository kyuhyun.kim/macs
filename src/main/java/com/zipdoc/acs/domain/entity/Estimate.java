package com.zipdoc.acs.domain.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.define.EstimateStatus;
import com.zipdoc.acs.define.LivingType;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Estimate {

    private long estimate_no;
    private String subject;
    private String category_code1;
    private String category_code2;
    private String category_name1;
    private String category_name2;
    private String writer;
    private String phone_no;
    private String address;
    private String address_detail;

    private String dest_address;
    private String dest_address_detail;

    private String email;
    private String budget;
    private Long pid;
    private String space;
    private String password;
    @JsonProperty("schd_start_date") private Long schd_start_timestamp;
    @JsonProperty("schd_visit_date") private Long schd_visit_timestamp;
    private String comment;
    private String reply;

    private int status;
    @JsonIgnore private Date schd_start_date;
    @JsonIgnore private Date schd_visit_date;
    @JsonIgnore private String resident_type;

    @JsonIgnore private Integer user_type;
    @JsonIgnore private Long user_id;
    private Long member_no;
    private Integer channel;
    private Integer channel_sub_code;
    private Integer interior_product_code;
    @JsonIgnore private Integer estimate_app_type;
    private Integer estimate_type;
    private Long create_date;
    private Integer completed_flag;

    @JsonIgnore private String adid;
    @JsonIgnore private String promotion_code;

    private List<Long> pid_list;

    //2018.1.2. 장한솔 (마케팅수신동의 추가)
    private @JsonIgnore int agreement_marketing;

    private String ip;
    private String referer;
    private Integer living_type;

    private String region1;
    private String region2;

    // 2018.11.14. KJB. 견적 계산기 JSON 정보
    private String construction_json_data;
    private String construction_str;

    private List<EstimateImgFile> estimate_img_info;

    // 2018.12.18 referer_code 추가.
    private Integer referer_code;

    /* 2019.02.22 purpose 추가 */
    private Integer purpose;
    private String purpose_name;

    /*2019.02.26 방 갯수 욕실 갯수 추가*/
    private String struct_room_cnt;
    private String bath_cnt;

    /* 2019.02.22 거주 타입 추가 */
    private String resident;

    private String resident_name;

    /* 이사 관련 추가 */
    private String floor;
    private String dest_floor;
    private Integer dest_living_type;
    private Integer load_keep;

    public String getDest_address() {
        return dest_address;
    }

    public void setDest_address(String dest_address) {
        this.dest_address = dest_address;
    }

    public String getDest_address_detail() {
        return dest_address_detail;
    }

    public void setDest_address_detail(String dest_address_detail) {
        this.dest_address_detail = dest_address_detail;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getDest_floor() {
        return dest_floor;
    }

    public void setDest_floor(String dest_floor) {
        this.dest_floor = dest_floor;
    }

    public Integer getDest_living_type() {
        return dest_living_type;
    }

    public void setDest_living_type(Integer dest_living_type) {
        this.dest_living_type = dest_living_type;
    }

    public Integer getLoad_keep() {
        return load_keep;
    }

    public void setLoad_keep(Integer load_keep) {
        this.load_keep = load_keep;
    }

    public String getResident_name() {
        return resident_name;
    }

    public void setResident_name(String resident_name) {
        this.resident_name = resident_name;
    }

    public String getResident() {
        return resident;
    }

    public void setResident(String resident) {
        this.resident = resident;
    }

    public String getPurpose_name() {
        return purpose_name;
    }

    public void setPurpose_name(String purpose_name) {
        this.purpose_name = purpose_name;
    }

    public String getStruct_room_cnt() {
        return struct_room_cnt;
    }

    public void setStruct_room_cnt(String struct_room_cnt) {
        this.struct_room_cnt = struct_room_cnt;
    }

    public String getBath_cnt() {
        return bath_cnt;
    }

    public void setBath_cnt(String bath_cnt) {
        this.bath_cnt = bath_cnt;
    }

    public Integer getPurpose() {
        return purpose;
    }

    public void setPurpose(Integer purpose) {
        this.purpose = purpose;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getReferer_code() {
        return referer_code;
    }

    public void setReferer_code(Integer referer_code) {
        this.referer_code = referer_code;
    }

    public List<EstimateImgFile> getEstimate_img_info() {
        return estimate_img_info;
    }

    public void setEstimate_img_info(List<EstimateImgFile> estimate_img_info) {
        this.estimate_img_info = estimate_img_info;
    }

    public void setEstimateImgFiles(EstimateImgFile estimateImgFile) {
        if(this.estimate_img_info == null)
            this.estimate_img_info = new ArrayList<>();
        this.estimate_img_info.add(estimateImgFile);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress_detail() {
        return address_detail;
    }

    public void setAddress_detail(String address_detail) {
        this.address_detail = address_detail;
    }

    public String getRegion1() {
        return region1;
    }

    public void setRegion1(String region1) {
        this.region1 = region1;
    }

    public String getRegion2() {
        return region2;
    }

    public void setRegion2(String region2) {
        this.region2 = region2;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getCategory_code1() {
        return category_code1;
    }

    public void setCategory_code1(String category_code1) {
        this.category_code1 = category_code1;
    }

    public String getCategory_code2() {
        return category_code2;
    }

    public void setCategory_code2(String category_code2) {
        this.category_code2 = category_code2;
    }

    public String getCategory_name1() {
        return category_name1;
    }

    public void setCategory_name1(String category_name1) {
        this.category_name1 = category_name1;
    }

    public String getCategory_name2() {
        return category_name2;
    }

    public void setCategory_name2(String category_name2) {
        this.category_name2 = category_name2;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getEstimate_no() {
        return estimate_no;
    }

    public void setEstimate_no(long estimate_no) {
        this.estimate_no = estimate_no;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public Date getSchd_start_date() {
        return schd_start_date;
    }

    public void setSchd_start_date(Date schd_start_date) {
        this.schd_start_date = schd_start_date;
    }

    public Long getSchd_start_timestamp() {
        return schd_start_timestamp;
    }

    public void setSchd_start_timestamp(Long schd_start_timestamp) {
        this.schd_start_timestamp = schd_start_timestamp;
    }

    public Date getSchd_visit_date() {
        return schd_visit_date;
    }

    public void setSchd_visit_date(Date schd_visit_date) {
        this.schd_visit_date = schd_visit_date;
    }

    public Long getSchd_visit_timestamp() {
        return schd_visit_timestamp;
    }

    public void setSchd_visit_timestamp(Long schd_visit_timestamp) {
        this.schd_visit_timestamp = schd_visit_timestamp;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }

    public Integer getStatus() {
        return status;
    }

    public String getStatus_name() {
        return EstimateStatus.get(status).getName();
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Integer getUser_type() {
        return user_type;
    }

    public void setUser_type(Integer user_type) {
        this.user_type = user_type;
    }

    public String getResident_type() {
        return resident_type;
    }

    public void setResident_type(String resident_type) {
        this.resident_type = resident_type;
    }

    public Integer getChannel_sub_code() {
        return channel_sub_code;
    }

    public void setChannel_sub_code(Integer channel_sub_code) {
        this.channel_sub_code = channel_sub_code;
    }

    public Integer getInterior_product_code() {
        return interior_product_code;
    }

    public void setInterior_product_code(Integer interior_product_code) {
        this.interior_product_code = interior_product_code;
    }

    public List<Long> getPid_list() {
        return pid_list;
    }

    public void setPid_list(List<Long> pid_list) {
        this.pid_list = pid_list;
    }

    public Integer getEstimate_app_type() {
        return estimate_app_type;
    }

    public void setEstimate_app_type(Integer estimate_app_type) {
        this.estimate_app_type = estimate_app_type;
    }

    public Integer getEstimate_type() {
        return estimate_type;
    }

    public void setEstimate_type(Integer estimate_type) {
        this.estimate_type = estimate_type;
    }

    public Long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Long create_date) {
        this.create_date = create_date;
    }

    public Integer getCompleted_flag() {
        return completed_flag;
    }

    public void setCompleted_flag(Integer completed_flag) {
        this.completed_flag = completed_flag;
    }

    public int getAgreement_marketing() {
        return agreement_marketing;
    }

    public void setAgreement_marketing(int agreement_marketing) {
        this.agreement_marketing = agreement_marketing;
    }

    public String getAdid() {
        return adid;
    }

    public void setAdid(String adid) {
        this.adid = adid;
    }

    public String getPromotion_code() {
        return promotion_code;
    }

    public void setPromotion_code(String promotion_code) {
        this.promotion_code = promotion_code;
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public Integer getLiving_type() {
        return living_type;
    }

    public String getLiving_type_name() {
        if (living_type == null) return null;
        LivingType type = LivingType.get(living_type);
        if (type == null) return null;
        return type.getName();
    }

    public void setLiving_type(Integer living_type) {
        this.living_type = living_type;
    }

    public String getConstruction_json_data() {
        return construction_json_data;
    }

    public void setConstruction_json_data(String construction_json_data) {
        this.construction_json_data = construction_json_data;
    }

    public String getConstruction_str() {
        return construction_str;
    }

    public void setConstruction_str(String construction_str) {
        this.construction_str = construction_str;
    }
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LayoutHome {

    private Integer category;
    private Integer layout_type;
    private Integer service_target;
    private String layout_name;
    private String additional_1;
    private String additional_2;
    private String additional_3;
    private Integer seq;
    private Integer file_location = 0;
    private String file_path;
    private String file_name;

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getLayout_type() {
        return layout_type;
    }

    public void setLayout_type(Integer layout_type) {
        this.layout_type = layout_type;
    }

    public Integer getService_target() {
        return service_target;
    }

    public void setService_target(Integer service_target) {
        this.service_target = service_target;
    }

    public String getLayout_name() {
        return layout_name;
    }

    public void setLayout_name(String layout_name) {
        this.layout_name = layout_name;
    }

    public String getAdditional_1() {
        return additional_1;
    }

    public void setAdditional_1(String additional_1) {
        this.additional_1 = additional_1;
    }

    public String getAdditional_2() {
        return additional_2;
    }

    public void setAdditional_2(String additional_2) {
        this.additional_2 = additional_2;
    }

    public String getAdditional_3() {
        return additional_3;
    }

    public void setAdditional_3(String additional_3) {
        this.additional_3 = additional_3;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Integer getFile_location() {
        return file_location;
    }

    public void setFile_location(Integer file_location) {
        this.file_location = file_location;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getImage_url() {
        String image_url = this.file_location == 0 ? Constants.STATIC_URL_C : Constants.STATIC_URL_Q;
        return image_url + getFile_path() + getFile_name();
    }
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.utils.MultiLangUtil;
import org.apache.commons.lang.StringUtils;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyPage {
	private int article_count;
	private int scrabbook_count;
	private int scrab_count;
	private int estimate_count;
	private Profile profile;

	@JsonIgnore private Long creater;
	@JsonIgnore private Long updater;

	public int getArticle_count() {
		return article_count;
	}

	public void setArticle_count(int article_count) {
		this.article_count = article_count;
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public int getEstimate_count() {
		return estimate_count;
	}

	public void setEstimate_count(int estimate_count) {
		this.estimate_count = estimate_count;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public int getScrab_count() {
		return scrab_count;
	}

	public void setScrab_count(int scrab_count) {
		this.scrab_count = scrab_count;
	}

	public int getScrabbook_count() {
		return scrabbook_count;
	}

	public void setScrabbook_count(int scrabbook_count) {
		this.scrabbook_count = scrabbook_count;
	}

	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.PictureType;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuickFilter {

    private Integer total_cnt;
    private String filter;
    private Integer filter_type;
    private Integer space_min;
    private Integer space_max;
    private String title;
    private String thumbnail_url;

    @JsonIgnore private String category;
    @JsonIgnore private String main_filter;

    public Integer getTotal_cnt() {
        return total_cnt;
    }

    public void setTotal_cnt(Integer total_cnt) {
        this.total_cnt = total_cnt;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMain_filter() {
        return main_filter;
    }

    public void setMain_filter(String main_filter) {
        this.main_filter = main_filter;
    }

    public Integer getFilter_type() {
        return filter_type;
    }

    public void setFilter_type(Integer filter_type) {
        this.filter_type = filter_type;
    }

    public Integer getSpace_min() {
        return space_min;
    }

    public void setSpace_min(Integer space_min) {
        this.space_min = space_min;
    }

    public Integer getSpace_max() {
        return space_max;
    }

    public void setSpace_max(Integer space_max) {
        this.space_max = space_max;
    }
}

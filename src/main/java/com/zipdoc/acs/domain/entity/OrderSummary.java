package com.zipdoc.acs.domain.entity;

public class OrderSummary {
	private int deposit_wait;
	private int payment_complete;
	private int delivery_wait;
	private int delivery_ing;
	private int delivery_complete;
	private int purchase_complete;
	private int refund;
	
	public int getDeposit_wait() {
		return deposit_wait;
	}
	public void setDeposit_wait(int deposit_wait) {
		this.deposit_wait = deposit_wait;
	}
	public int getPayment_complete() {
		return payment_complete;
	}
	public void setPayment_complete(int payment_complete) {
		this.payment_complete = payment_complete;
	}
	public int getDelivery_wait() {
		return delivery_wait;
	}
	public void setDelivery_wait(int delivery_wait) {
		this.delivery_wait = delivery_wait;
	}
	public int getDelivery_ing() {
		return delivery_ing;
	}
	public void setDelivery_ing(int delivery_ing) {
		this.delivery_ing = delivery_ing;
	}
	public int getDelivery_complete() {
		return delivery_complete;
	}
	public void setDelivery_complete(int delivery_complete) {
		this.delivery_complete = delivery_complete;
	}
	public int getPurchase_complete() {
		return purchase_complete;
	}
	public void setPurchase_complete(int purchase_complete) {
		this.purchase_complete = purchase_complete;
	}
	public int getRefund() {
		return refund;
	}
	public void setRefund(int refund) {
		this.refund = refund;
	}
}

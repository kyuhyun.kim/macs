package com.zipdoc.acs.domain.entity;

import com.zipdoc.acs.model.ListRequest;

public class ShopDeliveryManagementRequest extends ListRequest{

	public ShopDeliveryManagementRequest(long member_no,int page, int limit) {
		super.setMember_no(member_no);
		setPageLimit(page, limit);
	}
}

package com.zipdoc.acs.domain.entity;

import java.util.Date;

public class ShopOrderSummaryInfo {
	private long sno;
	private String order_goods_name;
	private int order_goods_cnt;
	private int total_discount_price;
	private int total_order_price;
	private int total_order_goods;
	private long goods_no;
	private String goods_main_img;
	private long creater;
	private Date create_date;
	
	public long getSno() {
		return sno;
	}
	public void setSno(long sno) {
		this.sno = sno;
	}
	public String getOrder_goods_name() {
		return order_goods_name;
	}
	public void setOrder_goods_name(String order_goods_name) {
		this.order_goods_name = order_goods_name;
	}
	public int getOrder_goods_cnt() {
		return order_goods_cnt;
	}
	public void setOrder_goods_cnt(int order_goods_cnt) {
		this.order_goods_cnt = order_goods_cnt;
	}
	public int getTotal_discount_price() {
		return total_discount_price;
	}
	public void setTotal_discount_price(int total_discount_price) {
		this.total_discount_price = total_discount_price;
	}
	public int getTotal_order_price() {
		return total_order_price;
	}
	public void setTotal_order_price(int total_order_price) {
		this.total_order_price = total_order_price;
	}
	public int getTotal_order_goods() {
		return total_order_goods;
	}
	public void setTotal_order_goods(int total_order_goods) {
		this.total_order_goods = total_order_goods;
	}
	public long getGoods_no() {
		return goods_no;
	}
	public void setGoods_no(long goods_no) {
		this.goods_no = goods_no;
	}
	public String getGoods_main_img() {
		return goods_main_img;
	}
	public void setGoods_main_img(String goods_main_img) {
		this.goods_main_img = goods_main_img;
	}
	public long getCreater() {
		return creater;
	}
	public void setCreater(long creater) {
		this.creater = creater;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
}

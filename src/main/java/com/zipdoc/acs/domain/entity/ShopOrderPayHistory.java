package com.zipdoc.acs.domain.entity;

import java.util.Date;

/**
 * 결제 히스토리
 * Created by dskim on 2017. 2. 15..
 */
public class ShopOrderPayHistory {

    //일련번호
    private long sno;

    //주문번호
    private String order_no;

    //아임포트 결제 고유 UID
    private String imp_uid;

    //결제타입
    private String pay_method;

    //PG사 명칭
    private String pg_provider;

    //PG사 승인정보
    private String pg_tid;

    //에스크로 결제 여부
    private boolean escrow = false;

    //카드사 승인정보
    private String apply_num;

    //카드사 명칭
    private String card_name;

    //할부개월 수
    private int card_quota;

    //입금받을 가상계좌 은행명
    private String vbank_name;

    //입금받을 가상계좌 계좌번호
    private String vbank_num;

    //입금받을 가상계좌 예금주
    private String vbank_holder;

    //입금받을 가상계좌 마감기한
    private Date vbank_date;

    //주문명칭
    private String order_name;

    //주문(결제)금액
    private int order_amount;

    //결제취소 금액
    private int order_cancel_amount;

    //주문자명
    private String buyer_name;

    //주문자 이메일
    private String buyer_email;

    //주문자 전화번호
    private String buyer_tel;

    //주문자 주소
    private String buyer_addr;

    //주문자 우편번호
    private String buyer_postcode;

    //가맹점에서 전달한 custom_data
    private String custom_data;

    //결제상태
    private String status;

    //결제완료 시점
    private Date paid_at;

    //결제실패 시점
    private Date failed_at;

    //결제취소 시점
    private Date cancelled_at;

    //결제실패 사유
    private String fail_reason;

    //결제취소 사유
    private String cancel_reason;

    //신용카드 매출전표 확인 URL
    private String receipt_url;

    //등록일
    private Date create_date;

    //수정일
    private Date update_date;


    public long getSno() {
        return sno;
    }

    public void setSno(long sno) {
        this.sno = sno;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getImp_uid() {
        return imp_uid;
    }

    public void setImp_uid(String imp_uid) {
        this.imp_uid = imp_uid;
    }

    public String getPay_method() {
        return pay_method;
    }

    public void setPay_method(String pay_method) {
        this.pay_method = pay_method;
    }

    public String getPg_provider() {
        return pg_provider;
    }

    public void setPg_provider(String pg_provider) {
        this.pg_provider = pg_provider;
    }

    public String getPg_tid() {
        return pg_tid;
    }

    public void setPg_tid(String pg_tid) {
        this.pg_tid = pg_tid;
    }

    public boolean isEscrow() {
        return escrow;
    }

    public void setEscrow(boolean escrow) {
        this.escrow = escrow;
    }

    public String getApply_num() {
        return apply_num;
    }

    public void setApply_num(String apply_num) {
        this.apply_num = apply_num;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public int getCard_quota() {
        return card_quota;
    }

    public void setCard_quota(int card_quota) {
        this.card_quota = card_quota;
    }

    public String getVbank_name() {
        return vbank_name;
    }

    public void setVbank_name(String vbank_name) {
        this.vbank_name = vbank_name;
    }

    public String getVbank_num() {
        return vbank_num;
    }

    public void setVbank_num(String vbank_num) {
        this.vbank_num = vbank_num;
    }

    public String getVbank_holder() {
        return vbank_holder;
    }

    public void setVbank_holder(String vbank_holder) {
        this.vbank_holder = vbank_holder;
    }

    public Date getVbank_date() {
        return vbank_date;
    }

    public void setVbank_date(Date vbank_date) {
        this.vbank_date = vbank_date;
    }

    public String getOrder_name() {
        return order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public int getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(int order_amount) {
        this.order_amount = order_amount;
    }

    public int getOrder_cancel_amount() {
        return order_cancel_amount;
    }

    public void setOrder_cancel_amount(int order_cancel_amount) {
        this.order_cancel_amount = order_cancel_amount;
    }

    public String getBuyer_name() {
        return buyer_name;
    }

    public void setBuyer_name(String buyer_name) {
        this.buyer_name = buyer_name;
    }

    public String getBuyer_email() {
        return buyer_email;
    }

    public void setBuyer_email(String buyer_email) {
        this.buyer_email = buyer_email;
    }

    public String getBuyer_tel() {
        return buyer_tel;
    }

    public void setBuyer_tel(String buyer_tel) {
        this.buyer_tel = buyer_tel;
    }

    public String getBuyer_addr() {
        return buyer_addr;
    }

    public void setBuyer_addr(String buyer_addr) {
        this.buyer_addr = buyer_addr;
    }

    public String getBuyer_postcode() {
        return buyer_postcode;
    }

    public void setBuyer_postcode(String buyer_postcode) {
        this.buyer_postcode = buyer_postcode;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getPaid_at() {
        return paid_at;
    }

    public void setPaid_at(Date paid_at) {
        this.paid_at = paid_at;
    }

    public Date getFailed_at() {
        return failed_at;
    }

    public void setFailed_at(Date failed_at) {
        this.failed_at = failed_at;
    }

    public Date getCancelled_at() {
        return cancelled_at;
    }

    public void setCancelled_at(Date cancelled_at) {
        this.cancelled_at = cancelled_at;
    }

    public String getFail_reason() {
        return fail_reason;
    }

    public void setFail_reason(String fail_reason) {
        this.fail_reason = fail_reason;
    }

    public String getCancel_reason() {
        return cancel_reason;
    }

    public void setCancel_reason(String cancel_reason) {
        this.cancel_reason = cancel_reason;
    }

    public String getReceipt_url() {
        return receipt_url;
    }

    public void setReceipt_url(String receipt_url) {
        this.receipt_url = receipt_url;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }
}

package com.zipdoc.acs.domain.entity;

import java.util.Date;

/**
 * 파트너 블랙리스트 전화번호 관리
 */
public class PartnerBlackList {

    /////////////////////////////////////
    // DB 데이터
    /////////////////////////////////////
    private String mobile_no;
    private String username;
    private Integer status;
    private String memo;
    private String creater;
    private String updater;
    private Date create_date;
    private Date update_date;
    /////////////////////////////////////


    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InquiryReply {
	
	private @JsonProperty("writer") String writer_name;
	
	private @JsonProperty("reply") String comment_1;

	public String getWriter_name() {
		return writer_name;
	}

	public void setWriter_name(String writer_name) {
		this.writer_name = writer_name;
	}

	public String getComment_1() {
		return comment_1;
	}

	public void setComment_1(String comment_1) {
		this.comment_1 = comment_1;
	}
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.StringUtils;

public class CustomerReply {

	private Integer cno;
	private Integer reply_no;
	private String contents;
	private Long create_date;
	private Long member_no;
	private String username;

	public Integer getCno() {
		return cno;
	}

	public void setCno(Integer cno) {
		this.cno = cno;
	}

	public Integer getReply_no() {
		return reply_no;
	}

	public void setReply_no(Integer reply_no) {
		this.reply_no = reply_no;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Long getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Long create_date) {
		this.create_date = create_date;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getMember_no() {
		return member_no;
	}

	public void setMember_no(Long member_no) {
		this.member_no = member_no;
	}

    public boolean insertValidation() {
        return StringUtils.isNotEmpty(contents) && cno != null;
    }

    public boolean updateValidation() {
        return StringUtils.isNotEmpty(contents) && reply_no != null;
    }

    public boolean deleteValidation() {
        return reply_no != null;
    }
}

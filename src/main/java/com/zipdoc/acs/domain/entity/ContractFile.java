package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang.StringUtils;

import static com.zipdoc.acs.define.Constants.STATIC_URL;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ContractFile {

	private Long file_no;
	private Long contract_no;
	private String file_type;
	private String file_name_org;
	@JsonIgnore private String file_name;
	@JsonIgnore private String file_path;
	@JsonIgnore private String thumbnail;

	private String download_url;
	private String thumbnail_url;
	private Long creater;
	private Long create_date;

	public ContractFile() {}

	public ContractFile(long contract_no, String file_type, String file_name_org, String file_name, String thumbnail, String file_path, Long creater) {
		this.contract_no = contract_no;
		this.file_type = file_type;
		this.file_name_org = file_name_org;
		this.file_name = file_name;
		this.thumbnail = thumbnail;
		this.file_path = file_path;
		this.creater = creater;
	}

	public String getDownload_url() {
		return download_url;
	}

	public void setDownload_url(String download_url) {
		this.download_url = download_url;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_name_org() {
		return file_name_org;
	}

	public void setFile_name_org(String file_name_org) {
		this.file_name_org = file_name_org;
	}

	public Long getFile_no() {
		return file_no;
	}

	public void setFile_no(Long file_no) {
		this.file_no = file_no;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public Long getContract_no() {
		return contract_no;
	}

	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}

	public String getThumbnail_url() {
		return thumbnail_url;
	}

	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}

	public void buildUrl(){
		if(StringUtils.isNotEmpty(file_name)){
			this.download_url = STATIC_URL + file_path + file_name;
		}

		if(StringUtils.isNotEmpty(thumbnail)){
			this.thumbnail_url = STATIC_URL + file_path + thumbnail;
		}
	}

	public Long getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Long create_date) {
		this.create_date = create_date;
	}
}

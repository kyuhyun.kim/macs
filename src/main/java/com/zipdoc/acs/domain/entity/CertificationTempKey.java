package com.zipdoc.acs.domain.entity;

import java.util.Date;

public class CertificationTempKey {


    private long key_no;

    private String phone;

    private String temp_key;

    private Date create_date;

    private Date expiration_date;


    public long getKey_no() {
        return key_no;
    }

    public void setKey_no(long key_no) {
        this.key_no = key_no;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTemp_key() {
        return temp_key;
    }

    public void setTemp_key(String temp_key) {
        this.temp_key = temp_key;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }
}

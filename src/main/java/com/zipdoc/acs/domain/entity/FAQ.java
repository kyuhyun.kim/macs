package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.FaqType;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FAQ {

    private Integer cno;
    private Integer category;
    private String subject;
    private String contents;

    public Integer getCno() {
        return cno;
    }

    public void setCno(Integer cno) {
        this.cno = cno;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getCategory_name() {
        if (category == null) return "미정의코드";
        FaqType type = FaqType.get(category);
        return type == null ? "미정의코드" : type.getName();
    }
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.define.EstimateStatus;
import com.zipdoc.acs.define.LivingType;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EstimateImgFile {

    @JsonIgnore private long estimate_no;
    private Integer seq;
    private String thumbnail;
    private String file_type;
    private String image_url;
    private String org_name;
    private Long create_date;
    private Long update_date;

    public EstimateImgFile()
    {

    }

    public EstimateImgFile(long estimate_no, Integer seq, String thumbnail, String file_type, String image_url, String org_name)
    {
        this.estimate_no = estimate_no;
        this.seq = seq;
        this.thumbnail = thumbnail;
        this.file_type = file_type;
        this.image_url = image_url;
        this.org_name = org_name;

    }

    public Long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Long create_date) {
        this.create_date = create_date;
    }

    public Long getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Long update_date) {
        this.update_date = update_date;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public long getEstimate_no() {
        return estimate_no;
    }

    public void setEstimate_no(long estimate_no) {
        this.estimate_no = estimate_no;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }


    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}

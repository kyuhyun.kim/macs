package com.zipdoc.acs.domain.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.define.CommentType;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.EnableFlag;
import com.zipdoc.acs.domain.entity.ShopComment;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShopComment {
	private int cno;
	private int reply_no;
	private Integer parent_reply_no;
	private String contents;
	private String ip;
	private String nickname;
	private String username;
	private String mobile_no;
	private String product_title;
	private String product_img;
	private Integer deleted_flag = 0;
	private Integer expose = 1;
	private Integer reply_flag;
	private int point;
	@JsonProperty("create_date") private Long create_timestamp;
	@JsonIgnore	private Date create_date;
	@JsonIgnore private Long creater;
	@JsonIgnore private Long updater;
	@JsonIgnore private String table_name;
	@JsonIgnore private boolean owner = false;
	private Integer comment_type;
	private List<ShopComment> recomments = new ArrayList<ShopComment>();
	private Profile profile;
	
	public ShopComment(){
		
	}
	
	public ShopComment(String table_name, int cno){
		this.table_name = table_name;
		this.cno = cno;
	}
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public int getReply_no() {
		return reply_no;
	}

	public void setReply_no(int reply_no) {
		this.reply_no = reply_no;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
		if(create_date != null){
			this.create_timestamp = create_date.getTime();
		}
	}

	public Integer getDeleted_flag() {
		return deleted_flag;
	}

	public void setDeleted_flag(Integer deleted_flag) {
		this.deleted_flag = deleted_flag;
	}

	public Integer getExpose() {
		return expose;
	}

	public void setExpose(Integer expose) {
		this.expose = expose;
	}

	public Integer getParent_reply_no() {
		return parent_reply_no;
	}

	public void setParent_reply_no(Integer parent_reply_no) {
		this.parent_reply_no = parent_reply_no;
	}

	public Integer getReply_flag() {
		return reply_flag;
	}

	public void setReply_flag(Integer reply_flag) {
		this.reply_flag = reply_flag;
	}

	public String getTable_name() {
		return table_name;
	}

	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}

	public boolean isOwner() {
		return owner;
	}

	public void setOwner(boolean owner) {
		this.owner = owner;
	}

	@JsonIgnore public boolean isRereply(){
		return parent_reply_no != null && parent_reply_no > 0 && parent_reply_no != reply_no;
	}

	public void setTreatOwner(long member_no){
		if(this.creater==member_no){
			this.owner = true;
		}

		if(!isOwner() && expose == EnableFlag.DISABLE.getCode()){
			setContents("비공개 글입니다.");
		}
		
		for(ShopComment comment:recomments){
			comment.setTreatOwner(member_no);
		}
	}

	public Long getCreate_timestamp() {
		return create_timestamp;
	}

	public void setCreate_timestamp(Long create_timestamp) {
		this.create_timestamp = create_timestamp;
	}

	public Integer getComment_type() {
		return comment_type;
	}

	public void setComment_type(Integer comment_type) {
		this.comment_type = comment_type;
		if(this.comment_type != null){
			CommentType commentType = CommentType.get(comment_type);
			if(commentType!=null){
				this.table_name = commentType.getName();
			}
		}
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getProduct_title() {
		return product_title;
	}

	public void setProduct_title(String product_title) {
		this.product_title = product_title;
	}

	public String getProduct_img() {
		return product_img;
	}

	public void setProduct_img(String product_img) {
		this.product_img = product_img;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	@JsonIgnore
	public boolean validation(){
		if(comment_type == null || StringUtils.isEmpty(table_name) || cno <= 0){
			return false;
		}
		return true;
	}

	public void replaceStaticImageUrl(){
		this.contents = StringUtils.replace(this.contents, Constants.STATIC_SRC_PATTERN, Constants.STATIC_REPLACE_URL);
	}

	public List<ShopComment> getRecomments() {
		return recomments;
	}

	public void setRecomments(List<ShopComment> recomments) {
		this.recomments = recomments;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@Override
	public String toString() {
		return "ShopComment [cno=" + cno + ", reply_no=" + reply_no + ", parent_reply_no=" + parent_reply_no
				+ ", contents=" + contents + ", ip=" + ip + ", deleted_flag=" + deleted_flag + ", expose=" + expose
				+ ", reply_flag=" + reply_flag + ", point=" + point + ", create_timestamp=" + create_timestamp
				+ ", create_date=" + create_date + ", creater=" + creater + ", updater=" + updater + ", table_name="
				+ table_name + ", owner=" + owner + ", comment_type=" + comment_type + ", recomments=" + recomments + ", profile=" + profile
				+ "]";
	}
}

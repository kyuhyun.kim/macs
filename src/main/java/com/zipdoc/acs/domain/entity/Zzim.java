package com.zipdoc.acs.domain.entity;

import java.util.List;

public class Zzim {
    private long member_no;
    private long pid;
    private Long item_id;
    private long book_no;

    private List<Long> pid_list;
    private List<Long> item_list;

    public Zzim() {
    }

    public Zzim(long member_no, long pid) {
        this.member_no = member_no;
        this.pid = pid;
    }

    public Zzim(long member_no, long pid, long book_no) {
        this.member_no = member_no;
        this.pid = pid;
        this.book_no = book_no;
    }

    public Zzim(long member_no, long pid, Long item_id, long book_no) {
        this.member_no = member_no;
        this.pid = pid;
        this.item_id = item_id;
        this.book_no = book_no;
    }

    public long getMember_no() {
        return member_no;
    }

    public void setMember_no(long member_no) {
        this.member_no = member_no;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public Long getItem_id() {
        return item_id;
    }

    public void setItem_id(Long item_id) {
        this.item_id = item_id;
    }

    public long getBook_no() {
        return book_no;
    }

    public void setBook_no(long book_no) {
        this.book_no = book_no;
    }

    public List<Long> getPid_list() {
        return pid_list;
    }

    public void setPid_list(List<Long> pid_list) {
        this.pid_list = pid_list;
    }

    public List<Long> getItem_list() {
        return item_list;
    }

    public void setItem_list(List<Long> item_list) {
        this.item_list = item_list;
    }
}

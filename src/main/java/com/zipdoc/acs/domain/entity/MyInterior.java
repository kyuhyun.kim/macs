package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.EstimateStatus;
import com.zipdoc.acs.define.LivingType;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyInterior {
	private long estimate_no;
	private String subject;
	private String writer;
	private int status;
	private Long schd_start_date;
	private String status_name;
	private String category_code1;
	private String category_code2;
	private String category_name1;
	private String category_name2;
	private String space;
	private String address;
	private String address_detail;
	private String dest_address;
	private String dest_address_detail;
	private int completed_flag;
	private Integer living_type;
	private Integer dest_living_type;
	private Long estimate_date;
	private String estimate_req_day;
	private String call_day;
	private String floor;
	private String dest_floor;
	private String promotion_code;

	private List<Partner> assign_partner_list;

	private Contract contract_info;

	public String getPromotion_code() {
		return promotion_code;
	}

	public void setPromotion_code(String promotion_code) {
		this.promotion_code = promotion_code;
	}

	public Long getSchd_start_date() {
		return schd_start_date;
	}

	public void setSchd_start_date(Long schd_start_date) {
		this.schd_start_date = schd_start_date;
	}

	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}

	public String getAddress_detail() {
		return address_detail;
	}

	public void setAddress_detail(String address_detail) {
		this.address_detail = address_detail;
	}

	public String getDest_address() {
		return dest_address;
	}

	public void setDest_address(String dest_address) {
		this.dest_address = dest_address;
	}

	public String getDest_address_detail() {
		return dest_address_detail;
	}

	public void setDest_address_detail(String dest_address_detail) {
		this.dest_address_detail = dest_address_detail;
	}

	public Integer getDest_living_type() {
		return dest_living_type;
	}

	public void setDest_living_type(Integer dest_living_type) {
		this.dest_living_type = dest_living_type;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getDest_floor() {
		return dest_floor;
	}

	public void setDest_floor(String dest_floor) {
		this.dest_floor = dest_floor;
	}

	public List<Partner> getAssign_partner_list() {
		return assign_partner_list;
	}

	public void setAssign_partner_list(List<Partner> assign_partner_list) {
		this.assign_partner_list = assign_partner_list;
	}

	public String getCall_day() {
		return call_day;
	}

	public void setCall_day(String call_day) {
		this.call_day = call_day;
	}

	public long getEstimate_no() {
		return estimate_no;
	}

	public void setEstimate_no(long estimate_no) {
		this.estimate_no = estimate_no;
	}

	public Long getEstimate_date() {
		return estimate_date;
	}

	public void setEstimate_date(Long estimate_date) {
		this.estimate_date = estimate_date;
	}

	public String getEstimate_req_day() {
		return estimate_req_day;
	}

	public void setEstimate_req_day(String estimate_req_day) {
		this.estimate_req_day = estimate_req_day;
	}

	public int getStatus() {
		return status;
	}

	public String getStatus_name() {
		return EstimateStatus.get(status).getName();
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Contract getContract_info() {
		return contract_info;
	}

	public void setContract_info(Contract contract_info) {
		this.contract_info = contract_info;
	}

	public int getCompleted_flag() {
		return completed_flag;
	}

	public void setCompleted_flag(int completed_flag) {
		this.completed_flag = completed_flag;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCategory_code1() {
		return category_code1;
	}

	public void setCategory_code1(String category_code1) {
		this.category_code1 = category_code1;
	}

	public String getCategory_code2() {
		return category_code2;
	}

	public void setCategory_code2(String category_code2) {
		this.category_code2 = category_code2;
	}

	public String getCategory_name1() {
		return category_name1;
	}

	public void setCategory_name1(String category_name1) {
		this.category_name1 = category_name1;
	}

	public String getCategory_name2() {
		return category_name2;
	}

	public void setCategory_name2(String category_name2) {
		this.category_name2 = category_name2;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public Integer getLiving_type() {
		return living_type;
	}

	public String getLiving_type_name() {
		if (living_type == null) return null;
		LivingType type = LivingType.get(living_type);
		if (type == null) return null;
		return type.getName();
	}

	public void setLiving_type(Integer living_type) {
		this.living_type = living_type;
	}
}

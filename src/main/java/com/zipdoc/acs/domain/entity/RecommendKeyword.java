package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RecommendKeyword {
	private String keyword_type;
	private String keyword_name;
	private List<String> keyword_list;

	@JsonIgnore private String keyword;

	public List<String> getKeyword_list() {
		return keyword_list;
	}

	public void setKeyword_list(List<String> keyword_list) {
		this.keyword_list = keyword_list;
	}

	public String getKeyword_name() {
		return keyword_name;
	}

	public void setKeyword_name(String keyword_name) {
		this.keyword_name = keyword_name;
	}

	public String getKeyword_type() {
		return keyword_type;
	}

	public void setKeyword_type(String keyword_type) {
		this.keyword_type = keyword_type;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
		//Array로 변환한다.
		if(StringUtils.isNotEmpty(keyword) && StringUtils.length(keyword) > 0){
			this.keyword_list = new ArrayList<String>(Arrays.asList(StringUtils.split(keyword, Constants.TAG_DELIMITER)));
		}
	}
}

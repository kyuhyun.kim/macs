package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.CustomerAgeGroup;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerAssign {

	// 신규 의뢰 DB
	private long estimate_no;
	private String category_code1;
	private String category_code2;
	private String address;
	private String address_detail;
	private String space;
	private String budget;
	private Long schd_start_date;
	private Long visit_hope_date;
	private String comment;
	@JsonIgnore
	private Integer living_type;
	private Integer free_yn;
	
	// 진행중 DB
	private String writer;
	private Long update_date;
	private Integer assign_status;
	
	// 상세 정보
	private String phone_no;
	private Long assigned_date;
	private Long accept_date;
	private Integer counsel_type;
	private Integer pid;
	private Long schd_visit_date;
	private Long visit_date;
	private Long publish_date;
	private Long feedback_date;
	private Integer commission_rate;
	private Integer estimate_price;
	private Integer completed_flag;
	private Integer accept_step;
	private Integer stop_code;
	private String stop_reason;
	private Integer retry_call;
	private String memo;

	private Integer interior_product_code;

	private Integer partner_id;

	private List<Product> concepts;

	// 계약 정보
	private PartnerContract contract;

	//1차 통화상담 여부
	private Integer call_status;

	private List<Long> call_list;

	@JsonIgnore private Integer age_group;			// 연령대
	@JsonIgnore private Integer interior_concept;	// 시공컨셉
	@JsonIgnore private String concept_style;		// 선택 컨셉 스타일

	public List<Long> getCall_list() {
		return call_list;
	}

	public void setCall_list(List<Long> call_list) {
		this.call_list = call_list;
	}

	public Integer getCall_status() {
		return call_status;
	}

	public void setCall_status(Integer call_status) {
		this.call_status = call_status;
	}

	public long getEstimate_no() {
		return estimate_no;
	}
	
	public void setEstimate_no(long estimate_no) {
		this.estimate_no = estimate_no;
	}
	
	public String getWriter() {
		return writer;
	}
	
	public void setWriter(String writer) {
		this.writer = writer;
	}
	
	public String getPhone_no() {
		return phone_no;
	}
	
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	
	public String getCategory_code1() {
		return category_code1;
	}
	
	public void setCategory_code1(String category_code1) {
		this.category_code1 = category_code1;
	}
	
	public String getCategory_code2() {
		return category_code2;
	}
	
	public void setCategory_code2(String category_code2) {
		this.category_code2 = category_code2;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAddress_detail() {
		return address_detail;
	}
	
	public void setAddress_detail(String address_detail) {
		this.address_detail = address_detail;
	}
	
	public String getSpace() {
		return space;
	}
	
	public void setSpace(String space) {
		this.space = space;
	}
	
	public String getBudget() {
		return budget;
	}
	
	public void setBudget(String budget) {
		this.budget = budget;
	}
	
	public Long getSchd_start_date() {
		return schd_start_date;
	}
	
	public void setSchd_start_date(Long schd_start_date) {
		this.schd_start_date = schd_start_date;
	}
	
	public Integer getFree_yn() {
		return free_yn;
	}

	public void setFree_yn(Integer free_yn) {
		this.free_yn = free_yn;
	}

	public Long getAccept_date() {
		return accept_date;
	}

	public void setAccept_date(Long accept_date) {
		this.accept_date = accept_date;
	}

	public Integer getAssign_status() {
		return assign_status;
	}

	public void setAssign_status(Integer assign_status) {
		this.assign_status = assign_status;
	}

	public Integer getCounsel_type() {
		return counsel_type;
	}

	public void setCounsel_type(Integer counsel_type) {
		this.counsel_type = counsel_type;
	}

	public Long getSchd_visit_date() {
		return schd_visit_date;
	}

	public void setSchd_visit_date(Long schd_visit_date) {
		this.schd_visit_date = schd_visit_date;
	}

	public Long getVisit_date() {
		return visit_date;
	}

	public void setVisit_date(Long visit_date) {
		this.visit_date = visit_date;
	}

	public Long getPublish_date() {
		return publish_date;
	}

	public void setPublish_date(Long publish_date) {
		this.publish_date = publish_date;
	}

	public Long getFeedback_date() {
		return feedback_date;
	}

	public void setFeedback_date(Long feedback_date) {
		this.feedback_date = feedback_date;
	}

	public Integer getAccept_step() {
		return accept_step;
	}

	public void setAccept_step(Integer accept_step) {
		this.accept_step = accept_step;
	}

	public Integer getStop_code() {
		return stop_code;
	}

	public void setStop_code(Integer stop_code) {
		this.stop_code = stop_code;
	}

	public String getStop_reason() {
		return stop_reason;
	}

	public void setStop_reason(String stop_reason) {
		this.stop_reason = stop_reason;
	}

	public Integer getCompleted_flag() {
		return completed_flag;
	}

	public void setCompleted_flag(Integer completed_flag) {
		this.completed_flag = completed_flag;
	}

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}

	public Integer getEstimate_price() {
		return estimate_price;
	}

	public void setEstimate_price(Integer estimate_price) {
		this.estimate_price = estimate_price;
	}

	public Long getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Long update_date) {
		this.update_date = update_date;
	}

	public PartnerContract getContract() {
		return contract;
	}

	public void setContract(PartnerContract contract) {
		this.contract = contract;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getCommission_rate() {
		return commission_rate;
	}

	public void setCommission_rate(Integer commission_rate) {
		this.commission_rate = commission_rate;
	}

	public Long getAssigned_date() {
		return assigned_date;
	}

	public void setAssigned_date(Long assigned_date) {
		this.assigned_date = assigned_date;
	}

	public String getComment() {

		// 고객연령대 설정
		if (getAge_group() != null) {
			comment = "고객연령대: "+getAge_group_name()+"\r\n"+ comment;
		}

		// 시공컨셉
		if (getInterior_concept() != null) {
			if (getInterior_concept() == 1) {
				comment = "시공컨셉: 기본공사\r\n"+comment;
			} else if (getInterior_concept() == 2) {
				comment = "시공컨셉: "+getConcept_style()+"\r\n"+comment;
			}
		}

		// 거주여부 설정
		if (getLiving_type() != null) {
			if (getLiving_type() == 1) {
				comment = "거주여부: 현재거주중\r\n"+comment;
			} else if (getLiving_type() == 2) {
				comment = "거주여부: 입주예정\r\n"+comment;
			} else if (getLiving_type() == 3) {
				comment = "거주여부: 계약전\r\n"+comment;
			}
		}

		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getVisit_hope_date() {
		return visit_hope_date;
	}

	public void setVisit_hope_date(Long visit_hope_date) {
		this.visit_hope_date = visit_hope_date;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getRetry_call() {
		return retry_call;
	}

	public void setRetry_call(Integer retry_call) {
		this.retry_call = retry_call;
	}

	public Integer getInterior_product_code() {
		return interior_product_code;
	}

	public void setInterior_product_code(Integer interior_product_code) {
		this.interior_product_code = interior_product_code;
	}

	public List<Product> getConcepts() {
		return concepts;
	}

	public void setConcepts(List<Product> concepts) {
		this.concepts = concepts;
	}

	public Integer getLiving_type() {
		return living_type;
	}

	public void setLiving_type(Integer living_type) {
		this.living_type = living_type;
	}

	public Integer getAge_group() {
		return age_group;
	}

	@JsonIgnore
	public String getAge_group_name() {
		CustomerAgeGroup group = age_group == null ? null : CustomerAgeGroup.get(age_group);
		if (group == null) return "";
		return group.getName();
	}

	public void setAge_group(Integer age_group) {
		this.age_group = age_group;
	}

	public Integer getInterior_concept() {
		return interior_concept;
	}

	public void setInterior_concept(Integer interior_concept) {
		this.interior_concept = interior_concept;
	}

	public String getConcept_style() {
		return concept_style == null ? "" : concept_style;
	}

	public void setConcept_style(String concept_style) {
		this.concept_style = concept_style;
	}
}

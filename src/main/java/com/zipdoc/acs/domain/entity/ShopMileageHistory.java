package com.zipdoc.acs.domain.entity;

import java.util.Date;

/**
 * Created by dskim on 2017. 3. 3..
 */
public class ShopMileageHistory {

    //일련번호
    private long sno;

    //회원별 게시글 순서
    private long member_sno;

    //내부회원번호
    private long member_no;

    //지급/차감 구분
    private Integer mileage_status;

    //이전 적립금
    private int previous_mileage;

    //이후 적립금
    private int after_mileage;

    //적립금액
    private int mileage_payment;

    //사유
    private String mileage_usage_history;

    //IP
    private String ip;

    //처리자
    private long manager;

    //등록일
    private Date create_date;

    //수정일
    private Date update_date;

    public long getSno() {
        return sno;
    }

    public void setSno(long sno) {
        this.sno = sno;
    }

    public long getMember_sno() {
        return member_sno;
    }

    public void setMember_sno(long member_sno) {
        this.member_sno = member_sno;
    }

    public long getMember_no() {
        return member_no;
    }

    public void setMember_no(long member_no) {
        this.member_no = member_no;
    }

    public Integer getMileage_status() {
        return mileage_status;
    }

    public void setMileage_status(Integer mileage_status) {
        this.mileage_status = mileage_status;
    }

    public int getPrevious_mileage() {
        return previous_mileage;
    }

    public void setPrevious_mileage(int previous_mileage) {
        this.previous_mileage = previous_mileage;
    }

    public int getAfter_mileage() {
        return after_mileage;
    }

    public void setAfter_mileage(int after_mileage) {
        this.after_mileage = after_mileage;
    }

    public int getMileage_payment() {
        return mileage_payment;
    }

    public void setMileage_payment(int mileage_payment) {
        this.mileage_payment = mileage_payment;
    }

    public String getMileage_usage_history() {
        return mileage_usage_history;
    }

    public void setMileage_usage_history(String mileage_usage_history) {
        this.mileage_usage_history = mileage_usage_history;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public long getManager() {
        return manager;
    }

    public void setManager(long manager) {
        this.manager = manager;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }
}

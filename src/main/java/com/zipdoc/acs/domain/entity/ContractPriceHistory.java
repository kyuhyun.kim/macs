package com.zipdoc.acs.domain.entity;

/**
 * 계약정보 추가/축소/취소 히스토리
 */
public class ContractPriceHistory {

    // 공사 구분
    private Integer contract_price_type;

    // 공사 금액
    private Long contract_price;

    // 계약정보 변경 이력 등록일
    private Long create_date;

    public Integer getContract_price_type() {
        return contract_price_type;
    }

    public void setContract_price_type(Integer contract_price_type) {
        this.contract_price_type = contract_price_type;
    }

    public Long getContract_price() {
        return contract_price;
    }

    public void setContract_price(Long contract_price) {
        this.contract_price = contract_price;
    }

    public Long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Long create_date) {
        this.create_date = create_date;
    }
}

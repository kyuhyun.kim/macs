package com.zipdoc.acs.domain.entity;

import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.GoodsDiscountStatus;
import com.zipdoc.acs.define.ShopGoodsOption;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.util.Date;
import java.util.List;

public class ShopGoods {

	//일련번호
	private Long sno;

	//상품번호
	private Long goods_no;

	//상품명
	private String goods_name;

	//상품코드
	private String goods_code;

	//상품간략한 설명
	private String short_desc;

	//상품설명
	private String goods_desc;

	//대표이미지
	private String goods_main_img;

	//카테고리 코드
	private String category_code;

	//카테고리 이름
	private String category_name;

    private String category_code2;
    private String category_name2;
    private String category_code3;
    private String category_name3;



    //배송방법 0:택배,1:방문설치
	private int delivery_status;

	//제조사
	private String maker_name;

	//원산지
	private String origin_name;

	//모델명
	private String goods_model_no;

    //사이즈
    private String goods_size;

    //재질/등급
    private String goods_material_rating;

	//상품 상태 0:판매중,1:판매중지
	private Integer goods_sell_status;
	
	//기본 배송정보
    private String delivery_desc;

    //시공 및 업체 직접배송 정보
    private String partner_delivery_desc;

    //교환/반품/환불 정보
    private String exchange_return_refund_desc;

	//현재고량
	private int use_stock;

	//총재고량
	private int total_stock;

	//배송비
	private int goods_delivery;

	//상품가격
	private int goods_price;

	//매입가
	private int goods_supply;

	//주문량
	private int order_cnt;

	//조회수
	private int hit_cnt;

    //찜하기수
    private int zzim_cnt;

	//등록일
	private Date create_date;

	//수정일
	private Date update_date;

	//상품타입
	private String goods_type;

	//시공비
	private int construction_price;

	//할인율
	private Long goods_discount;
	
	//할인금액
	private Integer goods_discount_purchase;
	
	//최종 할인가
	private Integer goods_discount_price;

	//할인시작일
	private String goods_discount_start_date;

	//할인종료일
	private String goods_discount_end_date;

	//할인여부
	private Integer goods_discount_status;

	//상품옵션리스트
	private List<ShopGoodsOption> itemList;

	//등록자
	private Long creater;

	//수정자
	private Long updater;
	
	//파트너스이름
	private String partner_name;

    //2017-04-14 적립금
    private Integer mileage;

    //2017-04-14 적립금이율
    private Long mileage_rate;

    public Long getSno() {
        return sno;
    }

    public void setSno(Long sno) {
        this.sno = sno;
    }

    public Long getGoods_no() {
        return goods_no;
    }

    public void setGoods_no(Long goods_no) {
        this.goods_no = goods_no;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_code() {
        return goods_code;
    }

    public void setGoods_code(String goods_code) {
        this.goods_code = goods_code;
    }

    public String getShort_desc() {
        return short_desc;
    }

    public void setShort_desc(String short_desc) {
        this.short_desc = short_desc;
    }

    public String getGoods_desc() {
        return goods_desc;
    }

    public void setGoods_desc(String goods_desc) {
        this.goods_desc = goods_desc;
    }

    public String getGoods_main_img() {
        return goods_main_img;
    }

    public void setGoods_main_img(String goods_main_img) {
        this.goods_main_img = goods_main_img;
    }

    public String getCategory_code() {
        return category_code;
    }

    public void setCategory_code(String category_code) {
        this.category_code = category_code;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_code2() {
        return category_code2;
    }

    public void setCategory_code2(String category_code2) {
        this.category_code2 = category_code2;
    }

    public String getCategory_code3() {
        return category_code3;
    }

    public void setCategory_code3(String category_code3) {
        this.category_code3 = category_code3;
    }

    public String getCategory_name2() {
        return category_name2;
    }

    public void setCategory_name2(String category_name2) {
        this.category_name2 = category_name2;
    }

    public String getCategory_name3() {
        return category_name3;
    }

    public void setCategory_name3(String category_name3) {
        this.category_name3 = category_name3;
    }

    public int getDelivery_status() {
        return delivery_status;
    }

    public void setDelivery_status(int delivery_status) {
        this.delivery_status = delivery_status;
    }

    public String getMaker_name() {
        return maker_name;
    }

    public void setMaker_name(String maker_name) {
        this.maker_name = maker_name;
    }

    public String getOrigin_name() {
        return origin_name;
    }

    public void setOrigin_name(String origin_name) {
        this.origin_name = origin_name;
    }

    public String getGoods_model_no() {
        return goods_model_no;
    }

    public void setGoods_model_no(String goods_model_no) {
        this.goods_model_no = goods_model_no;
    }

    public String getGoods_size() {
        return goods_size;
    }

    public void setGoods_size(String goods_size) {
        this.goods_size = goods_size;
    }

    public String getGoods_material_rating() {
        return goods_material_rating;
    }

    public void setGoods_material_rating(String goods_material_rating) {
        this.goods_material_rating = goods_material_rating;
    }

    public Integer getGoods_sell_status() {
        return goods_sell_status;
    }

    public void setGoods_sell_status(Integer goods_sell_status) {
        this.goods_sell_status = goods_sell_status;
    }

    public String getDelivery_desc() {
        return delivery_desc;
    }

    public void setDelivery_desc(String delivery_desc) {
        this.delivery_desc = delivery_desc;
    }

    public String getPartner_delivery_desc() {
        return partner_delivery_desc;
    }

    public void setPartner_delivery_desc(String partner_delivery_desc) {
        this.partner_delivery_desc = partner_delivery_desc;
    }

    public String getExchange_return_refund_desc() {
        return exchange_return_refund_desc;
    }

    public void setExchange_return_refund_desc(String exchange_return_refund_desc) {
        this.exchange_return_refund_desc = exchange_return_refund_desc;
    }

    public int getUse_stock() {
        return use_stock;
    }

    public void setUse_stock(int use_stock) {
        this.use_stock = use_stock;
    }

    public int getTotal_stock() {
        return total_stock;
    }

    public void setTotal_stock(int total_stock) {
        this.total_stock = total_stock;
    }

    public int getGoods_delivery() {
        return goods_delivery;
    }

    public void setGoods_delivery(int goods_delivery) {
        this.goods_delivery = goods_delivery;
    }

    public int getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(int goods_price) {
        this.goods_price = goods_price;
    }

    public int getGoods_supply() {
        return goods_supply;
    }

    public void setGoods_supply(int goods_supply) {
        this.goods_supply = goods_supply;
    }

    public int getOrder_cnt() {
        return order_cnt;
    }

    public void setOrder_cnt(int order_cnt) {
        this.order_cnt = order_cnt;
    }

    public int getHit_cnt() {
        return hit_cnt;
    }

    public void setHit_cnt(int hit_cnt) {
        this.hit_cnt = hit_cnt;
    }

    public int getZzim_cnt() {
        return zzim_cnt;
    }

    public void setZzim_cnt(int zzim_cnt) {
        this.zzim_cnt = zzim_cnt;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getGoods_type() {
        return goods_type;
    }

    public void setGoods_type(String goods_type) {
        this.goods_type = goods_type;
    }

    public int getConstruction_price() {
        return construction_price;
    }

    public void setConstruction_price(int construction_price) {
        this.construction_price = construction_price;
    }

    public Long getGoods_discount() {
        return goods_discount;
    }

    public void setGoods_discount(Long goods_discount) {
        this.goods_discount = goods_discount;
    }

    public Integer getGoods_discount_purchase() {
        return goods_discount_purchase;
    }

    public void setGoods_discount_purchase(Integer goods_discount_purchase) {
        this.goods_discount_purchase = goods_discount_purchase;
    }

    public Integer getGoods_discount_price() {
        return goods_discount_price;
    }

    public void setGoods_discount_price(Integer goods_discount_price) {
        this.goods_discount_price = goods_discount_price;
    }

    public String getGoods_discount_start_date() {
        return goods_discount_start_date;
    }

    public void setGoods_discount_start_date(String goods_discount_start_date) {
        this.goods_discount_start_date = goods_discount_start_date;
    }

    public String getGoods_discount_end_date() {
        return goods_discount_end_date;
    }

    public void setGoods_discount_end_date(String goods_discount_end_date) {
        this.goods_discount_end_date = goods_discount_end_date;
    }

    public Integer getGoods_discount_status() {
        return goods_discount_status;
    }

    public void setGoods_discount_status(Integer goods_discount_status) {
        this.goods_discount_status = goods_discount_status;
    }

    public boolean isDiscountGoods(){
        if(this.goods_discount_status==null) return false;
        return (this.goods_discount_status.intValue()== GoodsDiscountStatus.DISCOUNT.getCode());
    }

    public List<ShopGoodsOption> getItemList() {
        return itemList;
    }

    public void setItemList(List<ShopGoodsOption> itemList) {
        this.itemList = itemList;
    }

    public Long getCreater() {
        return creater;
    }

    public void setCreater(Long creater) {
        this.creater = creater;
    }

    public Long getUpdater() {
        return updater;
    }

    public void setUpdater(Long updater) {
        this.updater = updater;
    }

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Long getMileage_rate() {
        return mileage_rate;
    }

    public void setMileage_rate(Long mileage_rate) {
        this.mileage_rate = mileage_rate;
    }


    /**
     * 이미지 URL을 생성한다.
     */
    public void buildGoodsImageUrl(){
        if(StringUtils.isNotEmpty(getGoods_main_img())) {
            this.goods_main_img = Constants.STATIC_URL + Constants.RELATIVE_SHOP_PATH + getGoods_no() + File.separator + getGoods_main_img();
        }
    }
}
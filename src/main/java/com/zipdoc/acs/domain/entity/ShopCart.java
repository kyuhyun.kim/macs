package com.zipdoc.acs.domain.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.GoodsDiscountStatus;

/**
 *
 * 장바구니
 *
 * Created by dskim on 2017. 2. 15..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShopCart {

    //일련번호
    private long sno;
    
    //상품옵션번호
    private long option_sno;

    //내부회원번호
    private long member_no;
    
    //비회원 식별번호
    private String nonmember_no;
    
    //상품이미지
    private String goods_main_img;
    
    //상품이름
    private String goods_name;

	//상품가격
	private Integer goods_price;

    //옵션값1
    private String option_value;
    
    //옵션가격
    private Integer option_price;
    
    //옵션할인율
    private Long option_discount;
    
    //옵션할인여부
    private Integer option_discount_status;
    
    //옵션할인적용가
    private int option_discount_purchase;
    
    //옵션할인가
    private int option_discount_price;

    //택배비
    private Integer delivery_price;
    
    //상품번호
    private long goods_no;

    //주문번호
    private String order_no;

    //상품 수량
    private Integer goods_cnt;
    
    //할인율
  	private Long goods_discount;
  	
  	//할인여부
  	private Integer goods_discount_status;
    
    //옵션 재고량
    private int stock_cnt;

    //등록일
    private Date create_date;

    //수정일
    private Date update_date;
    
    private String single_option_value;
    private int single_stock_cnt;
    private int single_option_price;
    private String single_goods_main_img;
    
    //단품정보
    private Integer goods_discount_price;
    private Integer goods_discount_purchase;
    
    private List<Long> sno_list;

    public List<Long> getSno_list() {
		return sno_list;
	}

	public void setSno_list(List<Long> sno_list) {
		this.sno_list = sno_list;
	}

	public long getSno() {
        return sno;
    }

    public void setSno(long sno) {
        this.sno = sno;
    }

    public long getOption_sno() {
		return option_sno;
	}

	public void setOption_sno(long option_sno) {
		this.option_sno = option_sno;
	}

	public long getMember_no() {
        return member_no;
    }

    public void setMember_no(long member_no) {
        this.member_no = member_no;
    }

    public Integer getOption_price() {
		return option_price;
	}

	public void setOption_price(Integer option_price) {
		this.option_price = option_price;
	}

	public Integer getDelivery_price() {
		return delivery_price;
	}

	public void setDelivery_price(Integer delivery_price) {
		this.delivery_price = delivery_price;
	}

	public String getGoods_main_img() {
		return goods_main_img;
	}

	public void setGoods_main_img(String goods_main_img) {
		this.goods_main_img = goods_main_img;
	}

	public String getGoods_name() {
		return goods_name;
	}

	public void setGoods_name(String goods_name) {
		this.goods_name = goods_name;
	}

	public String getOption_value() {
		return option_value;
	}

	public void setOption_value(String option_value) {
		this.option_value = option_value;
	}

	public long getGoods_no() {
        return goods_no;
    }

    public void setGoods_no(long goods_no) {
        this.goods_no = goods_no;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public Integer getGoods_cnt() {
        return goods_cnt;
    }

    public void setGoods_cnt(Integer goods_cnt) {
        this.goods_cnt = goods_cnt;
    }

    public int getStock_cnt() {
		return stock_cnt;
	}

	public void setStock_cnt(int stock_cnt) {
		this.stock_cnt = stock_cnt;
	}

	public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

	public String getNonmember_no() {
		return nonmember_no;
	}

	public void setNonmember_no(String nonmember_no) {
		this.nonmember_no = nonmember_no;
	}

	public String getSingle_option_value() {
		return single_option_value;
	}

	public void setSingle_option_value(String single_option_value) {
		this.single_option_value = single_option_value;
	}

	public int getSingle_stock_cnt() {
		return single_stock_cnt;
	}

	public void setSingle_stock_cnt(int single_stock_cnt) {
		this.single_stock_cnt = single_stock_cnt;
	}

	public int getSingle_option_price() {
		return single_option_price;
	}

	public void setSingle_option_price(int single_option_price) {
		this.single_option_price = single_option_price;
	}

	public String getSingle_goods_main_img() {
		return single_goods_main_img;
	}

	public void setSingle_goods_main_img(String single_goods_main_img) {
		this.single_goods_main_img = single_goods_main_img;
	}

	public Long getGoods_discount() {
		return goods_discount;
	}

	public void setGoods_discount(Long goods_discount) {
		this.goods_discount = goods_discount;
	}
	
	public Integer getGoods_discount_status() {
		return goods_discount_status;
    }

    public void setGoods_discount_status(Integer goods_discount_status) {
        this.goods_discount_status = goods_discount_status;
    }

    public boolean isDiscountGoods(){
        if(this.goods_discount_status==null) return false;
        return (this.goods_discount_status.intValue()== GoodsDiscountStatus.DISCOUNT.getCode());
    }

	public Long getOption_discount() {
		return option_discount;
	}

	public void setOption_discount(Long option_discount) {
		this.option_discount = option_discount;
	}

	public Integer getOption_discount_status() {
		return option_discount_status;
	}

	public void setOption_discount_status(Integer option_discount_status) {
		this.option_discount_status = option_discount_status;
	}

	public int getOption_discount_purchase() {
		return option_discount_purchase;
	}

	public void setOption_discount_purchase(int option_discount_purchase) {
		this.option_discount_purchase = option_discount_purchase;
	}

	public int getOption_discount_price() {
		return option_discount_price;
	}

	public void setOption_discount_price(int option_discount_price) {
		this.option_discount_price = option_discount_price;
	}

	public Integer getGoods_discount_price() {
		return goods_discount_price;
	}

	public void setGoods_discount_price(Integer goods_discount_price) {
		this.goods_discount_price = goods_discount_price;
	}

	public Integer getGoods_discount_purchase() {
		return goods_discount_purchase;
	}

	public void setGoods_discount_purchase(Integer goods_discount_purchase) {
		this.goods_discount_purchase = goods_discount_purchase;
	}

	public Integer getGoods_price() {
		return goods_price;
	}

	public void setGoods_price(Integer goods_price) {
		this.goods_price = goods_price;
	}
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.utils.BitUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Event {

	private Integer cno;
	private String subject;
	private String category;
	private String summary;
	private String contents;
	private Integer status;		// 노출여부
	private Integer in_service;	// 서비스 종료 여부
	private Long start_date;
	private Long end_date;
	private Long create_date;

	private Integer next_cno;
	private Integer previous_cno;

	private String detail_url;

	@JsonIgnore private Integer file_location_bit;
	@JsonIgnore private Integer file_location;
	@JsonIgnore private String file_path;
	@JsonIgnore private String thumbnail;



	public Integer getCno() {
		return cno;
	}

	public void setCno(Integer cno) {
		this.cno = cno;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIn_service() {
		return in_service;
	}

	public void setIn_service(Integer in_service) {
		this.in_service = in_service;
	}

	public Long getStart_date() {
		return start_date;
	}

	public void setStart_date(Long start_date) {
		this.start_date = start_date;
	}

	public Long getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Long end_date) {
		this.end_date = end_date;
	}

	public Long getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Long create_date) {
		this.create_date = create_date;
	}

	public Integer getFile_location() {
		return file_location;
	}

	public void setFile_location(Integer file_location) {
		this.file_location = file_location;
	}

	public Integer getFile_location_bit() {
		return file_location_bit;
	}

	public void setFile_location_bit(Integer file_location_bit) {
		this.file_location_bit = file_location_bit;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getThumbnail_url() {
		if (StringUtils.isEmpty(this.thumbnail) || StringUtils.isEmpty(this.file_path)) return null;
		String image_url = BitUtils.getBit(this.file_location, file_location_bit == null ? 0 : file_location_bit) ? Constants.STATIC_URL_Q : Constants.STATIC_URL_C;

		return image_url + this.file_path + this.thumbnail;
	}

	public String getDetail_url() {
		return detail_url;
	}

	public void setDetail_url(String detail_url) {
		this.detail_url = String.format(Constants.ARTICLE_URL, ArticleType.EVENT.value(), cno);
	}

	public Integer getNext_cno() {
		return next_cno;
	}

	public void setNext_cno(Integer next_cno) {
		this.next_cno = next_cno;
	}

	public Integer getPrevious_cno() {
		return previous_cno;
	}

	public void setPrevious_cno(Integer previous_cno) {
		this.previous_cno = previous_cno;
	}

	public void setNeighbor_cno(String neighbor_cno) {
		if (StringUtils.isEmpty(neighbor_cno)) return;
		String [] token = neighbor_cno.split(",");
		if (token == null || token.length < 2) return;

		if (token.length == 3) {
			next_cno = Integer.parseInt(token[0]);
			previous_cno = Integer.parseInt(token[2]);
		} else if (token.length == 2) {
			if (Integer.parseInt(token[0]) == cno) {
				previous_cno = Integer.parseInt(token[1]);
			} else {
				next_cno = Integer.parseInt(token[0]);
			}
		}
	}
}

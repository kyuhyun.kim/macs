package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by KKH on 2018-11-07.
 */



@JsonInclude(JsonInclude.Include.NON_NULL)
public class AlarmInfo {
    private Integer est_alarm;
    private Integer event_alarm;
    private Integer contents_alarm;
    private Long ad_agree_date;
    private Integer ad_agree;

    private @JsonIgnore Long member_no;
    private @JsonIgnore Long zid;

    public AlarmInfo()
    {

    }

    public Long getAd_agree_date() {
        return ad_agree_date;
    }

    public void setAd_agree_date(Long ad_agree_date) {
        this.ad_agree_date = ad_agree_date;
    }

    public Integer getAd_agree() {
        return ad_agree;
    }

    public void setAd_agree(Integer ad_agree) {
        this.ad_agree = ad_agree;
    }

    public Long getZid() {
        return zid;
    }

    public void setZid(Long zid) {
        this.zid = zid;
    }

    public Integer getEst_alarm() {
        return est_alarm;
    }

    public void setEst_alarm(Integer est_alarm) {
        this.est_alarm = est_alarm;
    }

    public Integer getEvent_alarm() {
        return event_alarm;
    }

    public void setEvent_alarm(Integer event_alarm) {
        this.event_alarm = event_alarm;
    }

    public Integer getContents_alarm() {
        return contents_alarm;
    }

    public void setContents_alarm(Integer contents_alarm) {
        this.contents_alarm = contents_alarm;
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

}

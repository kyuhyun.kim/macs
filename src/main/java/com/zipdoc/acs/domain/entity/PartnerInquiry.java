package com.zipdoc.acs.domain.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerInquiry {

	@JsonIgnore private long cno;
	
	// 문의 내용
	private String contents;
	
	@JsonIgnore private long member_no;
	
	// 문의 종류 (9 : 견적/공사 관련 문의)
	@JsonIgnore private Integer request_type = 9;
	
	// 답변 여부 (0: 접수 완료, 1: 답변 완료)
	@JsonIgnore private Integer processing_type = 0;
	
	@JsonIgnore private Integer partner_id;

	@JsonIgnore private String subject;

	@JsonIgnore private String update_telephone;
	
	// 견적 번호
	private Long estimate_no;
	
	// 답변
	private String reply;
	
	// 문의 일자
	private Long create_date;
	
	// 수정 일자. 답변이 있는 경우 답변 일자로 처리
	private Long update_date;
	
	// 문의 사진 리스트
	private List<PartnerInquiryFile> images;
	
	public long getCno() {
		return cno;
	}
	
	public void setCno(long cno) {
		this.cno = cno;
	}
	
	public String getContents() {
		return contents;
	}
	
	public void setContents(String contents) {
		this.contents = contents;
	}
	
	public long getMember_no() {
		return member_no;
	}
	
	public void setMember_no(long member_no) {
		this.member_no = member_no;
	}

	public Integer getProcessing_type() {
		return processing_type;
	}

	public void setProcessing_type(Integer processing_type) {
		this.processing_type = processing_type;
	}

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}

	public Long getEstimate_no() {
		return estimate_no;
	}

	public void setEstimate_no(Long estimate_no) {
		this.estimate_no = estimate_no;
	}

	public String getReply() {
		return reply;
	}

	public void setReply(String reply) {
		this.reply = reply;
	}

	public Long getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Long create_date) {
		this.create_date = create_date;
	}

	public Long getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Long update_date) {
		this.update_date = update_date;
	}

	public List<PartnerInquiryFile> getImages() {
		return images;
	}

	public void setImages(List<PartnerInquiryFile> images) {
		this.images = images;
	}

	public Integer getRequest_type() {
		return request_type;
	}

	public void setRequest_type(Integer request_type) {
		this.request_type = request_type;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getUpdate_telephone() {
		return update_telephone;
	}

	public void setUpdate_telephone(String update_telephone) {
		this.update_telephone = update_telephone;
	}
}

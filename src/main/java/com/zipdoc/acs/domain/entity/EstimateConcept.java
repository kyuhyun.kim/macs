package com.zipdoc.acs.domain.entity;

/**
 * Created by 이동규 on 2017-03-14.
 */
public class EstimateConcept {
    private long estimate_no;
    private long pid;

    public EstimateConcept(){}

    public EstimateConcept(long estimate_no, long pid) {
        this.estimate_no = estimate_no;
        this.pid = pid;
    }

    public long getEstimate_no() {
        return estimate_no;
    }

    public void setEstimate_no(long estimate_no) {
        this.estimate_no = estimate_no;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }
}

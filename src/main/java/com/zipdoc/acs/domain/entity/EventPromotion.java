package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.define.Constants;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventPromotion {
	private String msg_type;
	private long epid;
	private String subject;
	private String description;
	@JsonProperty("start_date") private Long start_timestamp;
	@JsonProperty("end_date") private Long end_timestamp;
	private String promotion_type;
	private String landing_type;
	private String landing_menu;
	private String action_data;
	private String bg_color;

	@JsonIgnore private Date start_date;
	@JsonIgnore private Date end_date;
	@JsonIgnore private Integer file_location = 0;
	@JsonIgnore private String file_path;
	@JsonIgnore private String file_name;
	@JsonIgnore private String file_name_mobile;

	public String getMsg_type() {
		return msg_type;
	}

	public void setMsg_type(String msg_type) {
		this.msg_type = msg_type;
	}

	public long getEpid() {
		return epid;
	}

	public void setEpid(long epid) {
		this.epid = epid;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getImage_url() {
		if (StringUtils.isEmpty(this.file_name)) return null;
		String image_url = this.file_location == 0 ? Constants.STATIC_URL_C : Constants.STATIC_URL_Q;
		return image_url + getFile_path() + getFile_name();
	}

	public String getMobile_image_url() {
		if (StringUtils.isEmpty(this.file_name_mobile)) return null;
		String image_url = this.file_location == 0 ? Constants.STATIC_URL_C : Constants.STATIC_URL_Q;
		return image_url + getFile_path() + this.file_name_mobile;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;

		if(start_date != null){
			this.start_timestamp = start_date.getTime();
		}
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
		if(end_date != null){
			this.end_timestamp = end_date.getTime();
		}
	}

	public Long getEnd_timestamp() {
		return end_timestamp;
	}

	public void setEnd_timestamp(Long end_timestamp) {
		this.end_timestamp = end_timestamp;
	}

	public Long getStart_timestamp() {
		return start_timestamp;
	}

	public void setStart_timestamp(Long start_timestamp) {
		this.start_timestamp = start_timestamp;
	}

	public String getAction_data() {
		return action_data;
	}

	public void setAction_data(String action_data) {
		this.action_data = action_data;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanding_menu() {
		return landing_menu;
	}

	public void setLanding_menu(String landing_menu) {
		this.landing_menu = landing_menu;
	}

	public String getLanding_type() {
		return landing_type;
	}

	public void setLanding_type(String landing_type) {
		this.landing_type = landing_type;
	}

	public String getPromotion_type() {
		return promotion_type;
	}

	public void setPromotion_type(String promotion_type) {
		this.promotion_type = promotion_type;
	}

	public Integer getFile_location() {
		return file_location;
	}

	public void setFile_location(Integer file_location) {
		this.file_location = file_location;
	}

	public String getFile_name_mobile() {
		return file_name_mobile;
	}

	public void setFile_name_mobile(String file_name_mobile) {
		this.file_name_mobile = file_name_mobile;
	}

	public String getBg_color() {
		return bg_color;
	}

	public void setBg_color(String bg_color) {
		this.bg_color = bg_color;
	}
}

package com.zipdoc.acs.domain.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 주문 환불/반품/교환 정보
 * Created by dskim on 2017. 2. 15..
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ShopOrderHandle {

    //일련번호
    private Long sno;

    //주문번호
    private String order_no;
    
    //주문상품번호
    private Long order_goods_sno;

    //처리모드
    private Integer handle_mode;

    //처리 완료 여부
    private Integer handle_complete_status;

    //처리 사유
    private String handle_reason;

    //처리 상세 사유
    private String handle_detail_reason;

    //처리 완료 일자
    private Date handle_date;
    
    //반품받는분
    private String return_username;
    
    //반품주소
    private String return_address;
    
    //반품상세주소
    private String return_address_sub;
    
    //반품받는분 휴대폰번호
    private String return_mobile_no;

    //환불 처리사유
    private String refund_method;

    //환불 은행
    private String refund_bank_code;

    //환불 계좌
    private String refund_account_number;

    //환불 금액
    private Integer refund_price;

    //환불 수수료
    private Integer refund_charge;

    //계좌 예금주
    private String refund_depositor;
    
    //환불 완료 여부
    private Integer refund_complete_status; 

    //등록자
    private Long creater;
    
    //수정자
    private Long updater;
    
    //등록일
    private Date create_date;

    //수정일
    private Date update_date;

    public Long getSno() {
        return sno;
    }

    public void setSno(Long sno) {
        this.sno = sno;
    }

    public Long getOrder_goods_sno() {
		return order_goods_sno;
	}

	public void setOrder_goods_sno(Long order_goods_sno) {
		this.order_goods_sno = order_goods_sno;
	}

	public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }
    
    public Integer getHandle_mode() {
		return handle_mode;
	}

	public void setHandle_mode(Integer handle_mode) {
		this.handle_mode = handle_mode;
	}

	public Integer getHandle_complete_status() {
		return handle_complete_status;
	}

	public void setHandle_complete_status(Integer handle_complete_status) {
		this.handle_complete_status = handle_complete_status;
	}

	public String getHandle_reason() {
        return handle_reason;
    }

    public void setHandle_reason(String handle_reason) {
        this.handle_reason = handle_reason;
    }

    public String getHandle_detail_reason() {
        return handle_detail_reason;
    }

    public void setHandle_detail_reason(String handle_detail_reason) {
        this.handle_detail_reason = handle_detail_reason;
    }

    public Date getHandle_date() {
        return handle_date;
    }

    public void setHandle_date(Date handle_date) {
        this.handle_date = handle_date;
    }

    public String getRefund_method() {
        return refund_method;
    }

    public void setRefund_method(String refund_method) {
        this.refund_method = refund_method;
    }

    public String getRefund_bank_code() {
		return refund_bank_code;
	}

	public void setRefund_bank_code(String refund_bank_code) {
		this.refund_bank_code = refund_bank_code;
	}

	public String getRefund_account_number() {
        return refund_account_number;
    }

    public void setRefund_account_number(String refund_account_number) {
        this.refund_account_number = refund_account_number;
    }

    public Integer getRefund_price() {
        return refund_price;
    }

    public void setRefund_price(Integer refund_price) {
        this.refund_price = refund_price;
    }

    public Integer getRefund_charge() {
        return refund_charge;
    }

    public void setRefund_charge(Integer refund_charge) {
        this.refund_charge = refund_charge;
    }

    public String getRefund_depositor() {
        return refund_depositor;
    }

    public void setRefund_depositor(String refund_depositor) {
        this.refund_depositor = refund_depositor;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

	public String getReturn_username() {
		return return_username;
	}

	public void setReturn_username(String return_username) {
		this.return_username = return_username;
	}

	public String getReturn_address() {
		return return_address;
	}

	public void setReturn_address(String return_address) {
		this.return_address = return_address;
	}

	public String getReturn_address_sub() {
		return return_address_sub;
	}

	public void setReturn_address_sub(String return_address_sub) {
		this.return_address_sub = return_address_sub;
	}

	public String getReturn_mobile_no() {
		return return_mobile_no;
	}

	public void setReturn_mobile_no(String return_mobile_no) {
		this.return_mobile_no = return_mobile_no;
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}
	

	@Override
	public String toString() {
		return "ShopOrderHandle [sno=" + sno + ", order_no=" + order_no + ", order_goods_sno=" + order_goods_sno
				+ ", handle_mode=" + handle_mode + ", handle_complete_status=" + handle_complete_status
				+ ", handle_reason=" + handle_reason + ", handle_detail_reason=" + handle_detail_reason
				+ ", handle_date=" + handle_date + ", return_username=" + return_username + ", return_address="
				+ return_address + ", return_address_sub=" + return_address_sub + ", return_mobile_no="
				+ return_mobile_no + ", refund_method=" + refund_method + ", refund_bank_code=" + refund_bank_code
				+ ", refund_account_number=" + refund_account_number + ", refund_price=" + refund_price
				+ ", refund_charge=" + refund_charge + ", refund_depositor=" + refund_depositor
				+ ", refund_complete_status=" + refund_complete_status + ", creater=" + creater + ", updater=" + updater +", update_date="
				+ update_date + "]";
	}
}

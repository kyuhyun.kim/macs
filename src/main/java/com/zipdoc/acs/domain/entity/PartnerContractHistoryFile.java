package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerContractHistoryFile {

	@JsonIgnore private Long history_no;
	@JsonIgnore private String file_name_org;
	@JsonIgnore private String file_name;
	@JsonIgnore private String file_path;
	@JsonIgnore private String thumb_name;
	
	private String image;
	private String thumbnail;
	
	public PartnerContractHistoryFile() {}
	
	public PartnerContractHistoryFile(long history_no, String file_name_org, String file_name, String thumbnail, String file_path) {
		this.history_no = history_no;
		this.file_name_org = file_name_org;
		this.file_name = file_name;
		this.thumb_name = thumbnail;
		this.file_path = file_path;
	}
	
	public Long getHistory_no() {
		return history_no;
	}
	
	public void setHistory_no(Long history_no) {
		this.history_no = history_no;
	}
	
	public String getFile_name() {
		return file_name;
	}
	
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	
	public String getFile_path() {
		return file_path;
	}
	
	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}
	
	public String getThumb_name() {
		return thumb_name;
	}
	
	public void setThumb_name(String thumb_name) {
		this.thumb_name = thumb_name;
	}
	
	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
	
	public String getThumbnail() {
		return thumbnail;
	}
	
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getFile_name_org() {
		return file_name_org;
	}

	public void setFile_name_org(String file_name_org) {
		this.file_name_org = file_name_org;
	}
}

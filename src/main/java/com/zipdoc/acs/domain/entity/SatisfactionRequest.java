package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class SatisfactionRequest {

	private Long member_no;
	/* 계약 번호 */
	private Long contract_no;
	/* 단축 URL */
	private String short_url;

	/* 원본 URL */
	private String origin_url;

	private String expired_month;

	public Long getMember_no() {
		return member_no;
	}

	public void setMember_no(Long member_no) {
		this.member_no = member_no;
	}

	public String getExpired_month() {
		return expired_month;
	}

	public void setExpired_month(String expired_month) {
		this.expired_month = expired_month;
	}

	public String getOrigin_url() {
		return origin_url;
	}

	public void setOrigin_url(String origin_url) {
		this.origin_url = origin_url;
	}

	public String getShort_url() {
		return short_url;
	}

	public void setShort_url(String short_url) {
		this.short_url = short_url;
	}

	public Long getContract_no() {
		return contract_no;
	}

	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}
}

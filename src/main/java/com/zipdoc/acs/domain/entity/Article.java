package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.utils.BitUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Article {
	private int cno;
	private String category;
	private String subject;
	private String summary;
	private String contents;
	private String thumbnail_url;
	private String detail_url;
	private int view_cnt;
	@JsonProperty("start_date") private Long start_timestamp;
	@JsonProperty("end_date") private Long end_timestamp;
	@JsonProperty("create_date") private Long create_timestamp;

	@JsonIgnore private Date create_date;
	@JsonIgnore private Date start_date;
	@JsonIgnore private Date end_date;
	@JsonIgnore private Integer file_location;
	@JsonIgnore private String file_path;
	@JsonIgnore private String file_name;
	@JsonIgnore private String thumbnail;


	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getDetail_url() {
		return detail_url;
	}

	public void setDetail_url(String detail_url) {
		this.detail_url = detail_url;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
		if(end_date != null){
			this.end_timestamp = end_date.getTime();
		}
	}

	public Long getEnd_timestamp() {
		return end_timestamp;
	}

	public void setEnd_timestamp(Long end_timestamp) {
		this.end_timestamp = end_timestamp;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
		if(create_date != null){
			this.create_timestamp = create_date.getTime();
		}
	}

	public Long getCreate_timestamp() {
		return create_timestamp;
	}

	public void setCreate_timestamp(Long create_timestamp) {
		this.create_timestamp = create_timestamp;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
		if(start_date != null){
			this.start_timestamp = start_date.getTime();
		}
	}

	public Long getStart_timestamp() {
		return start_timestamp;
	}

	public void setStart_timestamp(Long start_timestamp) {
		this.start_timestamp = start_timestamp;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getThumbnail_url() {
		return thumbnail_url;
	}

	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}

	public Integer getFile_location() {
		return file_location;
	}

	public void setFile_location(Integer file_location) {
		this.file_location = file_location;
	}

	public int getView_cnt() {
		return view_cnt;
	}

	public void setView_cnt(int view_cnt) {
		this.view_cnt = view_cnt;
	}

	public void buildUrl(ArticleType articleType){
		if (!StringUtils.isEmpty(this.getThumbnail())) {
			String url = BitUtils.getBit(this.file_location == null ? 0 : this.file_location, 0) ? Constants.STATIC_URL_Q : Constants.STATIC_URL_C;
			this.thumbnail_url = url + this.file_path + this.thumbnail;
		}
		this.detail_url = String.format(Constants.ARTICLE_URL, articleType.value(), cno);
	}
}

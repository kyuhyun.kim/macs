package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ZipdocmanUser {

	private Integer group_no;
	private String group_name;
	private Integer grade;
	private Long member_no;
	private String username;

	private List<ZipdocmanUser> group_members;

	public Integer getGroup_no() {
		return group_no;
	}

	public void setGroup_no(Integer group_no) {
		this.group_no = group_no;
	}

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public List<ZipdocmanUser> getGroup_members() {
		return group_members;
	}

	public void setGroup_members(List<ZipdocmanUser> group_members) {
		this.group_members = group_members;
	}

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

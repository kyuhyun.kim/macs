package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class SurveySatisfactionRequest {

	private Long 	contract_no;
	private String	push_url;
	private String 	survey_ver;
	private String 	answer;
	private Long 	answer_date;
	private String 	memo;
	private Integer	gift_status;
	private Long 	gift_date;
	private Integer	cs_zipdocman_svc;
	private String	updater;
	private Long 	create_date;
	private Long 	update_date;

	public Long getContract_no() {
		return contract_no;
	}

	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}

	public String getPush_url() {
		return push_url;
	}

	public void setPush_url(String push_url) {
		this.push_url = push_url;
	}

	public String getSurvey_ver() {
		return survey_ver;
	}

	public void setSurvey_ver(String survey_ver) {
		this.survey_ver = survey_ver;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Long getAnswer_date() {
		return answer_date;
	}

	public void setAnswer_date(Long answer_date) {
		this.answer_date = answer_date;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getGift_status() {
		return gift_status;
	}

	public void setGift_status(Integer gift_status) {
		this.gift_status = gift_status;
	}

	public Long getGift_date() {
		return gift_date;
	}

	public void setGift_date(Long gift_date) {
		this.gift_date = gift_date;
	}

	public Integer getCs_zipdocman_svc() {
		return cs_zipdocman_svc;
	}

	public void setCs_zipdocman_svc(Integer cs_zipdocman_svc) {
		this.cs_zipdocman_svc = cs_zipdocman_svc;
	}

	public String getUpdater() {
		return updater;
	}

	public void setUpdater(String updater) {
		this.updater = updater;
	}

	public Long getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Long create_date) {
		this.create_date = create_date;
	}

	public Long getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Long update_date) {
		this.update_date = update_date;
	}
}

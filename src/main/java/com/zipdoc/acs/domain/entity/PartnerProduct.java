package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerProduct {

	private int product_code;
	private String product_name;
	private int total_cnt;
	private int min_commercial_cnt;
	private int month_fee_yn;
	private int month_fee;
	@JsonProperty("commission") private int commition_rate;
	
	public int getProduct_code() {
		return product_code;
	}
	
	public void setProduct_code(int product_code) {
		this.product_code = product_code;
	}
	
	public String getProduct_name() {
		return product_name;
	}
	
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	
	public int getTotal_cnt() {
		return total_cnt;
	}
	
	public void setTotal_cnt(int total_cnt) {
		this.total_cnt = total_cnt;
	}
	
	public int getMin_commercial_cnt() {
		return min_commercial_cnt;
	}
	
	public void setMin_commercial_cnt(int min_commercial_cnt) {
		this.min_commercial_cnt = min_commercial_cnt;
	}
	
	public int getMonth_fee_yn() {
		return month_fee_yn;
	}
	
	public void setMonth_fee_yn(int month_fee_yn) {
		this.month_fee_yn = month_fee_yn;
	}
	
	public int getMonth_fee() {
		return month_fee;
	}
	
	public void setMonth_fee(int month_fee) {
		this.month_fee = month_fee;
	}
	
	public int getCommition_rate() {
		return commition_rate;
	}
	
	public void setCommition_rate(int commition_rate) {
		this.commition_rate = commition_rate;
	}
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.domain.entity.Partner;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Account {

	private @JsonProperty("subs_type") int subsType = 1;
	private @JsonProperty("account_type") int accountType = 1;
	private @JsonProperty("account") String account;
	private @JsonProperty("name") String username;
	private @JsonProperty("password") String password;
	private String mobile_no;
	
	private @JsonIgnore long zid;
	private Long member_no;
	
	private Partner partner;

	//2018.1.2 장한솔 (마케팅수신동의 추가)
	private Integer agreement_marketing;
	
	public long getZid() {
		return zid;
	}

	public void setZid(long zid) {
		this.zid = zid;
	}

	public int getSubsType() {
		return subsType;
	}
	
	public void setSubsType(int subsType) {
		this.subsType = subsType;
	}
	
	public int getAccountType() {
		return accountType;
	}
	
	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}
	
	public String getAccount() {
		return account;
	}
	
	public void setAccount(String account) {
		this.account = account;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public Long getMember_no() {
		return member_no;
	}

	public void setMember_no(Long member_no) {
		this.member_no = member_no;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public Integer getAgreement_marketing() {
		return agreement_marketing == null ? 0 : agreement_marketing;
	}

	public void setAgreement_marketing(Integer agreement_marketing) {
		this.agreement_marketing = agreement_marketing;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
}

package com.zipdoc.acs.domain.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerContractHistory {

	// 계약 번호
	private Long contract_no;
	
	// 이력 번호
	@JsonIgnore private Long history_no;

	// 협의 내용
	private String contents;
	
	// 이력 유형
	private Integer type;
	
	// 등록자
	private String writer;
	
	// 사진 정보
	private List<PartnerContractHistoryFile> images;
	
	// 등록자 ID
	@JsonIgnore private Long member_no;
	
	private Long create_date;

	public Long getContract_no() {
		return contract_no;
	}

	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}

	public Long getHistory_no() {
		return history_no;
	}

	public void setHistory_no(Long history_no) {
		this.history_no = history_no;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getMember_no() {
		return member_no;
	}

	public void setMember_no(Long member_no) {
		this.member_no = member_no;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Long getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Long create_date) {
		this.create_date = create_date;
	}

	public List<PartnerContractHistoryFile> getImages() {
		return images;
	}

	public void setImages(List<PartnerContractHistoryFile> images) {
		this.images = images;
	}
}

package com.zipdoc.acs.domain.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Inquiry {
	
	private @JsonProperty("category") String category_2;
	
	private @JsonProperty("subject") String subject;
	
	private @JsonProperty("comment") String comment_1;
	
	private @JsonProperty("images") List<String> images;
	
	private @JsonProperty("reply") List<InquiryReply> reply;

	private @JsonIgnore int fid;
	
	private @JsonIgnore String writer_name;
	
	private @JsonIgnore String phone_2;
	
	private @JsonIgnore String email;
	
	private @JsonIgnore String passwd;
	
	private @JsonIgnore int sign_date;
	
	private @JsonIgnore int mini_uid;
	
	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public String getCategory_2() {
		return category_2;
	}

	public void setCategory_2(String category_2) {
		this.category_2 = category_2;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getComment_1() {
		return comment_1;
	}

	public void setComment_1(String comment_1) {
		this.comment_1 = comment_1;
	}

	public String getWriter_name() {
		return writer_name;
	}

	public void setWriter_name(String writer_name) {
		this.writer_name = writer_name;
	}

	public String getPhone_2() {
		return phone_2;
	}

	public void setPhone_2(String phone_2) {
		this.phone_2 = phone_2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public int getSign_date() {
		return sign_date;
	}

	public void setSign_date(int sign_date) {
		this.sign_date = sign_date;
	}

	public int getMini_uid() {
		return mini_uid;
	}

	public void setMini_uid(int mini_uid) {
		this.mini_uid = mini_uid;
	}

	public List<InquiryReply> getReply() {
		return reply;
	}

	public void setReply(List<InquiryReply> reply) {
		this.reply = reply;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}
}

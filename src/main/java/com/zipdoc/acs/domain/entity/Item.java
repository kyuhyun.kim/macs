package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.PictureType;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Item {
	private long pid;
	private long item_id;
	private String subject;
	private String address;
	private String description;
	private String image_url;
	private String thumbnail_url;
	private String before_img_url;
	private String before_thumbnail_url;

	private List<String> tag_list;
	private String source_url;

	private Integer zzim_flag;
	private String category_code1;

	@JsonIgnore private String file_path;
	@JsonIgnore private String file_name;
	@JsonIgnore private String file_name_org;
	@JsonIgnore private long file_size;
	@JsonIgnore private String file_type;
	@JsonIgnore private String thumbnail;
	@JsonIgnore private String tags;
	@JsonIgnore private String before_file_name;
	@JsonIgnore private String before_thumbnail;
	@JsonIgnore private Integer file_location = 0;
	@JsonIgnore private Integer before_file_location = 0;

	//커뮤니티에서 파일 업로드시 사용
	private String cmd;
	private String form_data_name;
	private MultipartFile multipartFile;
	@JsonIgnore int trans_status;
	@JsonIgnore Long creater;
	@JsonIgnore Long updater;

	public Integer getBefore_file_location() {
		return before_file_location;
	}

	public void setBefore_file_location(Integer before_file_location) {
		this.before_file_location = before_file_location;
	}

	public Integer getFile_location() {
		return file_location;
	}

	public void setFile_location(Integer file_location) {
		this.file_location = file_location;
	}

	public String getBefore_thumbnail() {
		return before_thumbnail;
	}

	public void setBefore_thumbnail(String before_thumbnail) {
		this.before_thumbnail = before_thumbnail;
	}

	public String getBefore_thumbnail_url() {
		return before_thumbnail_url;
	}

	public void setBefore_thumbnail_url(String before_thumbnail_url) {
		this.before_thumbnail_url = before_thumbnail_url;
	}

	public String getBefore_img_url() {
		return before_img_url;
	}

	public void setBefore_img_url(String before_img_url) {
		this.before_img_url = before_img_url;
	}

	public String getBefore_file_name() {
		return before_file_name;
	}

	public void setBefore_file_name(String before_file_name) {
		this.before_file_name = before_file_name;
	}

	public String getCategory_code1() {
		return category_code1;
	}

	public void setCategory_code1(String category_code1) {
		this.category_code1 = category_code1;
	}

	public long getPid() {
		return pid;
	}

	public void setPid(long pid) {
		this.pid = pid;
	}

	public long getItem_id() {
		return item_id;
	}

	public void setItem_id(long item_id) {
		this.item_id = item_id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getThumbnail_url() {
		return thumbnail_url;
	}

	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_name_org() {
		return file_name_org;
	}

	public void setFile_name_org(String file_name_org) {
		this.file_name_org = file_name_org;
	}

	public long getFile_size() {
		return file_size;
	}

	public void setFile_size(long file_size) {
		this.file_size = file_size;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getSource_url() {
		return source_url;
	}

	public void setSource_url(String source_url) {
		this.source_url = source_url;
	}

	public List<String> getTag_list() {
		return tag_list;
	}

	public void setTag_list(List<String> tag_list) {
		this.tag_list = tag_list;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
		//Tag를 Array로 변환한다.
		if(StringUtils.isNotEmpty(tags) && StringUtils.length(tags) > 0){
			this.tag_list = new ArrayList<String>(Arrays.asList(StringUtils.split(tags, Constants.TAG_DELIMITER)));
		}
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	@JsonIgnore
	public boolean isCreateCmd(){
		return StringUtils.equalsIgnoreCase(cmd, "create");
	}

	@JsonIgnore
	public boolean isUpdateCmd(){
		return StringUtils.equalsIgnoreCase(cmd, "update");
	}

	@JsonIgnore
	public boolean isDeleteCmd(){
		return StringUtils.equalsIgnoreCase(cmd, "delete");
	}


	public String getForm_data_name() {
		return form_data_name;
	}

	public void setForm_data_name(String form_data_name) {
		this.form_data_name = form_data_name;
	}

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	public int getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(int trans_status) {
		this.trans_status = trans_status;
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@JsonIgnore
	public boolean isPictureChanged(){
		return isUpdateCmd() && multipartFile!=null && !multipartFile.isEmpty();
	}

	public void buildPictureUrl(PictureType imageType, PictureType thumbnailType){

		String DomainURL = this.file_location == 0 ? Constants.STATIC_URL_C : Constants.STATIC_URL_Q;
		String BeforeDomainURL = this.before_file_location == 0 ? Constants.STATIC_URL_C : Constants.STATIC_URL_Q;

		if (StringUtils.isNotEmpty(this.file_name)) {
			this.image_url = DomainURL + getFile_path() + this.file_name; //원본이미지
			this.thumbnail_url = DomainURL + getFile_path() + thumbnailType.getPrefix() + this.file_name;

			if(StringUtils.isNotEmpty(this.before_file_name))
				this.before_img_url = BeforeDomainURL+ getFile_path() + getBefore_file_name(); //비포 이미지

			if(StringUtils.isNotEmpty(this.before_thumbnail))
				this.before_thumbnail_url= BeforeDomainURL + getFile_path() + thumbnailType.getPrefix()  + getBefore_file_name(); //비포 이미지

		}


	}

	public void buildTagsByTagList(){
		if(this.tag_list == null || this.tag_list.size()==0) return;

		StringBuffer sb = new StringBuffer();
		for(String tag:this.tag_list){
			if(StringUtils.isNotEmpty(tag)){
				sb.append(tag).append(",");
			}
		}
		if(sb.length()>0) {
			this.tags = StringUtils.removeEnd(sb.toString(), ",");
		}
	}

	public Integer getZzim_flag() {
		return zzim_flag;
	}

	public void setZzim_flag(Integer zzim_flag) {
		this.zzim_flag = zzim_flag;
	}
}

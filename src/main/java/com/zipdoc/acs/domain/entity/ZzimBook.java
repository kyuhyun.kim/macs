package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * SCRAP BOOK
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ZzimBook {

    private long book_no;
    private String book_name;
    private Long member_no;

    private long gallery_count;
    private Long community_count;

    private List<Long> book_no_list;

    @JsonIgnore private String gallery_file_path;
    @JsonIgnore private String gallery_thumbnail;
    @JsonIgnore private String picture_file_path;
    @JsonIgnore private String picture_thumbnail;

    public ZzimBook() {
    }

    public ZzimBook(long book_no, long member_no) {
        this(book_no, member_no, null);
    }

    public ZzimBook(long book_no, long member_no, String book_name) {
        this.book_no = book_no;
        this.member_no = member_no;
        this.book_name = book_name;
    }

    public long getBook_no() {
        return book_no;
    }

    public void setBook_no(long book_no) {
        this.book_no = book_no;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public Long getCommunity_count() {
        return community_count;
    }

    public void setCommunity_count(Long community_count) {
        this.community_count = community_count;
    }

    public long getGallery_count() {
        return gallery_count;
    }

    public void setGallery_count(long gallery_count) {
        this.gallery_count = gallery_count;
    }

    public List<Long> getBook_no_list() {
        return book_no_list;
    }

    public void setBook_no_list(List<Long> book_no_list) {
        this.book_no_list = book_no_list;
    }

    public String getThumbnail() {

        if (StringUtils.isNotEmpty(picture_file_path) && StringUtils.isNotEmpty(picture_thumbnail)) {
            return Constants.STATIC_URL + picture_file_path + picture_thumbnail;
        }

        if (StringUtils.isNotEmpty(gallery_file_path) && StringUtils.isNotEmpty(gallery_thumbnail)) {
            return Constants.STATIC_URL + gallery_file_path + gallery_thumbnail;
        }

        return null;
    }
}

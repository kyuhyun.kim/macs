package com.zipdoc.acs.domain.entity;

/**
 * Created by 이동규 on 2016-06-13.
 */
public class Like {
    private Long member_no;
    private long pid;
    private int category;

    public Like() {
    }

    public Like(int category, Long member_no, long pid) {
        this.category = category;
        this.member_no = member_no;
        this.pid = pid;
    }

    public long getMember_no() {
        return member_no;
    }

    public void setMember_no(long member_no) {
        this.member_no = member_no;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public boolean isLoginedUser(){
        return this.member_no != null && this.member_no > 0;
    }
}

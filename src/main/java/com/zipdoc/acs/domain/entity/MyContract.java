package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.ContractPriceHistoryType;
import com.zipdoc.acs.utils.CurrencyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyContract {

	private Long contract_no;
	private String contract_name;
	private String contracter;
	private String phone_no;
	private String email;
	private String partner_name;
	private String address;
	private String address_detail;

	private String category_code1;
	private String category_code2;

	private String space;
	private Long contract_price;

	private Long start_date;
	private Long expect_end_date;
	private Long end_date;

	private List<ContractPriceHistory> price_histories;
	private List<ContractFile> contract_files;
	private Long total_contract_price;

	// 보증서 내용
	private Long completion_date;
	private String total_contract_price_hangul;
	private Long warranty_start_date;
	private Long warranty_end_date;
	private Long warranty_create_date;

	public Long getContract_no() {
		return contract_no;
	}

	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}

	public String getContract_name() {
		return contract_name;
	}

	public void setContract_name(String contract_name) {
		this.contract_name = contract_name;
	}

	public String getContracter() {
		return contracter;
	}

	public void setContracter(String contracter) {
		this.contracter = contracter;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress_detail() {
		return address_detail;
	}

	public void setAddress_detail(String address_detail) {
		this.address_detail = address_detail;
	}

	public String getCategory_code1() {
		return category_code1;
	}

	public void setCategory_code1(String category_code1) {
		this.category_code1 = category_code1;
	}

	public String getCategory_code2() {
		return category_code2;
	}

	public void setCategory_code2(String category_code2) {
		this.category_code2 = category_code2;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public Long getContract_price() {
		return contract_price;
	}

	public void setContract_price(Long contract_price) {
		this.contract_price = contract_price;
	}

	public Long getStart_date() {
		return start_date;
	}

	public void setStart_date(Long start_date) {
		this.start_date = start_date;
	}

	public Long getExpect_end_date() {
		return expect_end_date;
	}

	public void setExpect_end_date(Long expect_end_date) {
		this.expect_end_date = expect_end_date;
	}

	public Long getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Long end_date) {
		this.end_date = end_date;
	}

	public List<ContractPriceHistory> getPrice_histories() {
		return price_histories;
	}

	public void setPrice_histories(List<ContractPriceHistory> price_histories) {
		this.price_histories = price_histories;
	}

	public Long getTotal_contract_price() {
		return total_contract_price;
	}

	public List<ContractFile> getContract_files() {
		return contract_files;
	}

	public void setContract_files(List<ContractFile> contract_files) {
		this.contract_files = contract_files;
		if (contract_files == null) return;

		for (ContractFile file : this.contract_files) {
			file.buildUrl();
		}
	}

	@JsonIgnore
	public void calculate_contract_price() {

		total_contract_price = contract_price == null ? new Long(0) : contract_price;

		if (price_histories != null) {
			for (ContractPriceHistory history : price_histories) {
				if (history.getContract_price() != null) {
					if (history.getContract_price_type() == ContractPriceHistoryType.ADD.getCode()) {
						total_contract_price += history.getContract_price();
					} else if (history.getContract_price_type() == ContractPriceHistoryType.CANCEL.getCode()) {
						total_contract_price -= history.getContract_price();
					} else if (history.getContract_price_type() == ContractPriceHistoryType.REDUCE.getCode()) {
						total_contract_price -= history.getContract_price();
					}
				}
			}
		}
	}

	public Long getCompletion_date() {
		return completion_date;
	}

	public void setCompletion_date(Long completion_date) {
		this.completion_date = completion_date;
	}

	public String getTotal_contract_price_hangul() {
		return total_contract_price_hangul;
	}

	public void setTotal_contract_price_hangul(String total_contract_price_hangul) {
		this.total_contract_price_hangul = total_contract_price_hangul;
	}

	public Long getWarranty_start_date() {
		return warranty_start_date;
	}

	public void setWarranty_start_date(Long warranty_start_date) {
		this.warranty_start_date = warranty_start_date;
	}

	public Long getWarranty_end_date() {
		return warranty_end_date;
	}

	public void setWarranty_end_date(Long warranty_end_date) {
		this.warranty_end_date = warranty_end_date;
	}

	public Long getWarranty_create_date() {
		return warranty_create_date;
	}

	public void setWarranty_create_date(Long warranty_create_date) {
		this.warranty_create_date = warranty_create_date;
	}

	@JsonIgnore
	public void calculate_warranty_info() {

		// 공사완료일 및 보증서발행일이 설정되어 있는 경우
		if (end_date == null || warranty_create_date == null) {
			warranty_end_date = null;
			warranty_create_date = null;
			return;
		}

		// 공사금액 한글
		if (total_contract_price != null) {
			DecimalFormat df = new DecimalFormat("#,###");
			total_contract_price_hangul = "금 " + CurrencyUtils.getKoreanCurrency(total_contract_price.intValue()) + "정";
		}

		// 공사완료일이 있는 경우
		if (end_date != null) {

			// 공사완료일
			Date c_end_date = new Date(end_date);

			// 준공일자: 공사완료일 1일후
			Date c_completion_date = DateUtils.addDays(c_end_date, 1);
			completion_date = c_completion_date.getTime();

			// 보증기간 시작일: 준공일자 1년 후
			Date c_warranty_start_date = DateUtils.addYears(c_end_date, 1);
			warranty_start_date = c_warranty_start_date.getTime();
		}
	}
}

package com.zipdoc.acs.domain.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.SiteVisitStatus;
import com.zipdoc.acs.define.SiteVisitType;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SiteVisitHistory {

    private Long history_no;
    private Long contract_no;
    private Integer visit_type;
    private Integer visit_seq;
    private Integer visit_status;
    private String visitors;
    private Long visiting_date;
    private Integer client_meeting_yn;
    private Integer partner_meeting_yn;
    private String memo;
    private Long create_date;

    @JsonIgnore private String updater;

    public Long getHistory_no() {
        return history_no;
    }

    public void setHistory_no(Long history_no) {
        this.history_no = history_no;
    }

    public Long getContract_no() {
        return contract_no;
    }

    public void setContract_no(Long contract_no) {
        this.contract_no = contract_no;
    }

    public Integer getVisit_type() {
        return visit_type;
    }

    @JsonIgnore
    public String getVisit_type_name() {
        SiteVisitType type = SiteVisitType.get(visit_type);
        return type == null ? "" : type.getName();
    }

    public void setVisit_type(Integer visit_type) {
        this.visit_type = visit_type;
    }

    public Integer getVisit_seq() {
        return visit_seq;
    }

    public void setVisit_seq(Integer visit_seq) {
        this.visit_seq = visit_seq;
    }

    public Integer getVisit_status() {
        return visit_status;
    }

    @JsonIgnore
    public String getVisit_status_name() {
        SiteVisitStatus status = SiteVisitStatus.get(visit_status);
        return status == null ? "" : status.getName();
    }

    public void setVisit_status(Integer visit_status) {
        this.visit_status = visit_status;
    }

    public String getVisitors() {
        return visitors;
    }

    public void setVisitors(String visitors) {
        this.visitors = visitors;
    }

    public Long getVisiting_date() {
        return visiting_date;
    }

    public void setVisiting_date(Long visiting_date) {
        this.visiting_date = visiting_date;
    }

    public Integer getClient_meeting_yn() {
        return client_meeting_yn;
    }

    public void setClient_meeting_yn(Integer client_meeting_yn) {
        this.client_meeting_yn = client_meeting_yn;
    }

    public Integer getPartner_meeting_yn() {
        return partner_meeting_yn;
    }

    public void setPartner_meeting_yn(Integer partner_meeting_yn) {
        this.partner_meeting_yn = partner_meeting_yn;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    @JsonIgnore
    public String getHistorySubject() {
        if (visit_status != SiteVisitStatus.NONE.getCode()) {
            return getVisit_type_name()+getVisit_seq()+"차"+getVisit_status_name();
        }
        return getVisit_type_name()+getVisit_status_name();
    }

    public Long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Long create_date) {
        this.create_date = create_date;
    }
}

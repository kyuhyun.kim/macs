package com.zipdoc.acs.domain.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ShopOrderUserInfo {

	//일련번호
	private long sno;

	//주문번호
	private String order_no;

	//내부회원번호
	private long member_no;

	//주문자 이름
	private String order_name;

	//주문자 EMAIL
	private String order_email;

	//주문자 전화번호
	private String order_phone;

	//주문자 핸드폰번호
	private String order_cell_phone;

	//주문자 우편번호
	private String order_zipcode;

	//주문자 주소
	private String order_address;

	//주문자 상세주소
	private String order_address_sub;

	//수취인 이름
	private String receiver_name;

	//수취인 전화번호
	private String receiver_phone;

	//수취인 핸드폰번호
	private String receiver_cell_phone;

	//수취인 우편번호
	private String receiver_zipcode;

	//수취인 주소
	private String receiver_address;

	//수취인 상세 주소
	private String receiver_address_sub;

	//주문시 메모
	private String order_memo;

	//등록일
	private Date create_date;

	//수정일
	private Date update_date;

	//
	private String order_detail_address;

	//
	private String memo;

	public long getSno() {
		return sno;
	}

	public void setSno(long sno) {
		this.sno = sno;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public long getMember_no() {
		return member_no;
	}

	public void setMember_no(long member_no) {
		this.member_no = member_no;
	}

	public String getOrder_name() {
		return order_name;
	}

	public void setOrder_name(String order_name) {
		this.order_name = order_name;
	}

	public String getOrder_email() {
		return order_email;
	}

	public void setOrder_email(String order_email) {
		this.order_email = order_email;
	}

	public String getOrder_phone() {
		return order_phone;
	}

	public void setOrder_phone(String order_phone) {
		this.order_phone = order_phone;
	}

	public String getOrder_cell_phone() {
		return order_cell_phone;
	}

	public void setOrder_cell_phone(String order_cell_phone) {
		this.order_cell_phone = order_cell_phone;
	}

	public String getOrder_zipcode() {
		return order_zipcode;
	}

	public void setOrder_zipcode(String order_zipcode) {
		this.order_zipcode = order_zipcode;
	}

	public String getOrder_address() {
		return order_address;
	}

	public void setOrder_address(String order_address) {
		this.order_address = order_address;
	}

	public String getOrder_address_sub() {
		return order_address_sub;
	}

	public void setOrder_address_sub(String order_address_sub) {
		this.order_address_sub = order_address_sub;
	}

	public String getReceiver_name() {
		return receiver_name;
	}

	public void setReceiver_name(String receiver_name) {
		this.receiver_name = receiver_name;
	}

	public String getReceiver_phone() {
		return receiver_phone;
	}

	public void setReceiver_phone(String receiver_phone) {
		this.receiver_phone = receiver_phone;
	}

	public String getReceiver_cell_phone() {
		return receiver_cell_phone;
	}

	public void setReceiver_cell_phone(String receiver_cell_phone) {
		this.receiver_cell_phone = receiver_cell_phone;
	}

	public String getReceiver_zipcode() {
		return receiver_zipcode;
	}

	public void setReceiver_zipcode(String receiver_zipcode) {
		this.receiver_zipcode = receiver_zipcode;
	}

	public String getReceiver_address() {
		return receiver_address;
	}

	public void setReceiver_address(String receiver_address) {
		this.receiver_address = receiver_address;
	}

	public String getReceiver_address_sub() {
		return receiver_address_sub;
	}

	public void setReceiver_address_sub(String receiver_address_sub) {
		this.receiver_address_sub = receiver_address_sub;
	}

	public String getOrder_memo() {
		return order_memo;
	}

	public void setOrder_memo(String order_memo) {
		this.order_memo = order_memo;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getOrder_detail_address() {
		return order_detail_address;
	}

	public void setOrder_detail_address(String order_detail_address) {
		this.order_detail_address = order_detail_address;
	}


	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
}

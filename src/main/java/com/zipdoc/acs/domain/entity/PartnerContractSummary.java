package com.zipdoc.acs.domain.entity;

/**
 * 파트너 누적 계약 건수 및 매출 총액
 * @author 김종부
 *
 */
public class PartnerContractSummary {

    private long total_count;
    private long total_price;
    
	public long getTotal_count() {
		return total_count;
	}
	
	public void setTotal_count(long total_count) {
		this.total_count = total_count;
	}

	public long getTotal_price() {
		return total_price;
	}

	public void setTotal_price(long total_price) {
		this.total_price = total_price;
	}
}

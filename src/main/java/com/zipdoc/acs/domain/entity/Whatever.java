package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.utils.MultiLangUtil;
import org.apache.commons.lang.StringUtils;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Whatever {
	private int cno;
	private String subject;
	private String category;
	private String summary;
	private String contents;
	private long view_cnt;
	private int reply_cnt;
	@JsonProperty("create_date") private Long create_timestamp;
	@JsonIgnore private Date create_date;
	@JsonIgnore private Long creater;
	@JsonIgnore private Long updater;
	private Profile profile;
	@JsonIgnore private String ip;

	private List<Long> article_no_list;

	private Comment reply;

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
		if(create_date != null){
			this.create_timestamp = create_date.getTime();
		}
	}

	public Long getCreate_timestamp() {
		return create_timestamp;
	}

	public void setCreate_timestamp(Long create_timestamp) {
		this.create_timestamp = create_timestamp;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public int getReply_cnt() {
		return reply_cnt;
	}

	public void setReply_cnt(int reply_cnt) {
		this.reply_cnt = reply_cnt;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public long getView_cnt() {
		return view_cnt;
	}

	public void setView_cnt(long view_cnt) {
		this.view_cnt = view_cnt;
	}

	public Comment getReply() {
		return reply;
	}

	public void setReply(Comment reply) {
		this.reply = reply;
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public List<Long> getArticle_no_list() {
		return article_no_list;
	}

	public void setArticle_no_list(List<Long> article_no_list) {
		this.article_no_list = article_no_list;
	}

	public void replaceStaticImageUrl(){
		this.contents = StringUtils.replace(this.contents, Constants.STATIC_SRC_PATTERN, Constants.STATIC_REPLACE_URL);
		if(this.reply!=null){
			reply.replaceStaticImageUrl();
		}
	}

	public void buildSummary(){
		if(StringUtils.isNotEmpty(contents)){
			String nohtml = contents.replaceAll("\\<.*?>","");
			summary = MultiLangUtil.subStringBytes(StringUtils.replace(nohtml, "&nbsp;", " "), 300, false);
		}
	}

	public void buildForSendMessage(){
		replaceStaticImageUrl();
		buildSummary();
	}

	@Override
	public String toString() {
		return "Whatever{" +
				"category='" + category + '\'' +
				", cno=" + cno +
				", subject='" + subject + '\'' +
				", summary='" + summary + '\'' +
				", contents='" + contents + '\'' +
				", view_cnt=" + view_cnt +
				", reply_cnt=" + reply_cnt +
				", create_timestamp=" + create_timestamp +
				", create_date=" + create_date +
				", creater=" + creater +
				", updater=" + updater +
				", profile=" + profile +
				", reply=" + reply +
				'}';
	}
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.LayoutType;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyEstimateStep {

	private Long event_time;
	private Integer event_type;
	private String additional_data;

	public MyEstimateStep(Long event_time, Integer event_type) {
		this.event_time = event_time;
		this.event_type = event_type;
	}

	public MyEstimateStep(Long event_time, Integer event_type, String additional_data) {
		this.event_time = event_time;
		this.event_type = event_type;
		this.additional_data = additional_data;
	}

	public Long getEvent_time() {
		return event_time;
	}

	public void setEvent_time(Long event_time) {
		this.event_time = event_time;
	}

	public Integer getEvent_type() {
		return event_type;
	}

	public void setEvent_type(Integer event_type) {
		this.event_type = event_type;
	}

	public String getAdditional_data() {
		return additional_data;
	}

	public void setAdditional_data(String additional_data) {
		this.additional_data = additional_data;
	}
}

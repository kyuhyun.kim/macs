package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by DKLEE on 2016-06-08.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EstimateSummary {

    private Long total_count;
    private Long today_count;
    private Long doing_count;
    private Long contract_price;

    public Long getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Long total_count) {
        this.total_count = total_count;
    }

    public Long getToday_count() {
        return today_count;
    }

    public void setToday_count(Long today_count) {
        this.today_count = today_count;
    }

    public Long getDoing_count() {
        return doing_count;
    }

    public void setDoing_count(Long doing_count) {
        this.doing_count = doing_count;
    }

    public Long getContract_price() {
        return contract_price;
    }

    public void setContract_price(Long contract_price) {
        this.contract_price = contract_price;
    }
}

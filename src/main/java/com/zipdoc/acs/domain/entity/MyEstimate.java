package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.LivingType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyEstimate {

	private long estimate_no;
	private String subject;
	private int status;

	// 견적 상세정보

	// 지역
	private String address;
	private String address_detail;
	private String region1;
	private String region2;

	// 공간구분
	private String category_code1;
	private String category_code2;
	private String category_name1;
	private String category_name2;

	// 공사범위
	private Integer resident_type;

	// 평수
	private String space;

	// 예산
	private String budget;

	// 거주정보
	private Integer living_type;

	// 공사예정일
	private Long schd_start_date;

	// 컨셉 (컨셉 주소 / 컨셉 이미지)
	private String concept_subject;
	private String concept_thumbnail_url;
	@JsonIgnore Long concept_pid;

	// 신청자
	private String writer;

	// 전화번호
	private String phone_no;

	// 이메일
	private String email;

	// 프로모션 코드
	private String promotion_code;

	// 견적 요청사항
	private String comment;

	// 견적신청 단계
	private Long create_date;							// 견적신청일.
	private Long counsel_complete_date;					// 상담 완료일

	// 상담진행 단계
	private List<MyEstimateStep> visit_timeline;		// 방문상담 타임라인
	private List<MyEstimatePartner> assign_partners;	// 배분 후 수락한 파트너 정보

	// 계약단계
	private Long contract_no;							// 계약번호
	private Long contract_price;						// 계약금액
	private Long contract_date;							// 계약 완료일
	private Long construct_start_date;					// 공사 시작일

	// 현장방문 이력
	private List<SiteVisitHistory> site_histories;		// 현장방문 이력

	// 공사진행 단계
	private List<MyEstimateStep> construct_timeline;	// 공사진행 타임라인
	private Long construct_expect_end_date;				// 공사완료예정일

	// 공사완료 단계
	private Long construct_end_date;					// 공사완료일
	private List<MyEstimateStep> warranty_timeline;		// 공사완료 이후 타임라인

	// 보증서 정보
	private MyContract warranty_info;

	public String getAddress_detail() {
		return address_detail;
	}

	public void setAddress_detail(String address_detail) {
		this.address_detail = address_detail;
	}

	public long getEstimate_no() {
		return estimate_no;
	}

	public void setEstimate_no(long estimate_no) {
		this.estimate_no = estimate_no;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Long getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Long create_date) {
		this.create_date = create_date;
	}

	public List<MyEstimatePartner> getAssign_partners() {
		return assign_partners;
	}

	public void setAssign_partners(List<MyEstimatePartner> assign_partners) {
		this.assign_partners = assign_partners;
	}

	public Long getCounsel_complete_date() {
		return counsel_complete_date;
	}

	public void setCounsel_complete_date(Long counsel_complete_date) {
		this.counsel_complete_date = counsel_complete_date;
	}

	public List<MyEstimateStep> getVisit_timeline() {
		return visit_timeline;
	}

	public void setVisit_timeline(List<MyEstimateStep> visit_timeline) {
		this.visit_timeline = visit_timeline;
	}

	public void setVisit_timeline_item(MyEstimateStep item) {
		if (visit_timeline == null) {
			visit_timeline = new ArrayList<>();
		}
		add_timeline(visit_timeline, item);
	}

	public Long getContract_date() {
		return contract_date;
	}

	public void setContract_date(Long contract_date) {
		this.contract_date = contract_date;
	}

	public Long getConstruct_start_date() {
		return construct_start_date;
	}

	public void setConstruct_start_date(Long construct_start_date) {
		this.construct_start_date = construct_start_date;
	}

	public List<MyEstimateStep> getConstruct_timeline() {
		return construct_timeline;
	}

	public void setConstruct_timeline(List<MyEstimateStep> construct_timeline) {
		this.construct_timeline = construct_timeline;
	}

	public void setConstruct_timeline_item(MyEstimateStep item) {
		if (construct_timeline == null) {
			construct_timeline = new ArrayList<>();
		}
		add_timeline(construct_timeline, item);
	}

	public Long getContract_no() {
		return contract_no;
	}

	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}

	public List<SiteVisitHistory> getSite_histories() {
		return site_histories;
	}

	public void setSite_histories(List<SiteVisitHistory> site_histories) {
		this.site_histories = site_histories;
	}

	public Long getContract_price() {
		return contract_price;
	}

	public void setContract_price(Long contract_price) {
		this.contract_price = contract_price;
	}

	public Long getConstruct_expect_end_date() {
		return construct_expect_end_date;
	}

	public void setConstruct_expect_end_date(Long construct_expect_end_date) {
		this.construct_expect_end_date = construct_expect_end_date;
	}

	public List<MyEstimateStep> getWarranty_timeline() {
		return warranty_timeline;
	}

	public void setWarranty_timeline(List<MyEstimateStep> warranty_timeline) {
		this.warranty_timeline = warranty_timeline;
	}

	public void setWarranty_timeline_item(MyEstimateStep item) {
		if (warranty_timeline == null) {
			warranty_timeline = new ArrayList<>();
		}
		add_timeline(warranty_timeline, item);
	}

	public Long getConstruct_end_date() {
		return construct_end_date;
	}

	public void setConstruct_end_date(Long construct_end_date) {
		this.construct_end_date = construct_end_date;
	}

	private void add_timeline(List<MyEstimateStep> timeline, MyEstimateStep item) {

		if (timeline == null) return;

		for (int i = 0, n = timeline.size(); i < n; i++) {
			MyEstimateStep step = timeline.get(i);
			if (step.getEvent_time() < item.getEvent_time()) continue;
			timeline.add(i, item);
			return;
		}
		timeline.add(item);
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRegion1() {
		return region1;
	}

	public void setRegion1(String region1) {
		this.region1 = region1;
	}

	public String getRegion2() {
		return region2;
	}

	public void setRegion2(String region2) {
		this.region2 = region2;
	}

	public String getCategory_code1() {
		return category_code1;
	}

	public void setCategory_code1(String category_code1) {
		this.category_code1 = category_code1;
	}

	public String getCategory_code2() {
		return category_code2;
	}

	public void setCategory_code2(String category_code2) {
		this.category_code2 = category_code2;
	}

	public String getCategory_name1() {
		return category_name1;
	}

	public void setCategory_name1(String category_name1) {
		this.category_name1 = category_name1;
	}

	public String getCategory_name2() {
		return category_name2;
	}

	public void setCategory_name2(String category_name2) {
		this.category_name2 = category_name2;
	}

	public Integer getResident_type() {
		return resident_type;
	}

	public String getResident_type_name() {
		return resident_type == null ? null : (resident_type == 0 ? "부분" : (resident_type == 1 ? "전체" : "미정의"));
	}

	public void setResident_type(Integer resident_type) {
		this.resident_type = resident_type;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public Integer getLiving_type() {
		return living_type;
	}

	public String getLiving_type_name() {
		if (living_type == null) return null;
		LivingType type = LivingType.get(living_type);
		return type == null ? null : type.getName();
	}

	public void setLiving_type(Integer living_type) {
		this.living_type = living_type;
	}

	public Long getSchd_start_date() {
		return schd_start_date;
	}

	public void setSchd_start_date(Long schd_start_date) {
		this.schd_start_date = schd_start_date;
	}

	public String getConcept_subject() {
		return concept_subject;
	}

	public void setConcept_subject(String concept_subject) {
		this.concept_subject = concept_subject;
	}

	public String getConcept_thumbnail_url() {
		return concept_thumbnail_url;
	}

	public void setConcept_thumbnail_url(String concept_thumbnail_url) {
		this.concept_thumbnail_url = concept_thumbnail_url;
	}

	public Long getConcept_pid() {
		return concept_pid;
	}

	public void setConcept_pid(Long concept_pid) {
		this.concept_pid = concept_pid;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPromotion_code() {
		return promotion_code;
	}

	public void setPromotion_code(String promotion_code) {
		this.promotion_code = promotion_code;
	}

	public MyContract getWarranty_info() {
		return warranty_info;
	}

	public void setWarranty_info(MyContract warranty_info) {
		this.warranty_info = warranty_info;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}

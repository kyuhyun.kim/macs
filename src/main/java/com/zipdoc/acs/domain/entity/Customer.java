package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.InquireType;
import com.zipdoc.acs.define.ProcessingStateType;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Customer {

	private int cno;
	private String contents;
	private Long create_date;
	private String file_path;
	private String file_name;
	private String thumbnail;
	private Integer inquire_type;
	private Integer processing_state = ProcessingStateType.STATE_READY.getCode();
	private Long member_no;
	private String username;
	private String update_phone;
	private String update_email;

	private List<CustomerReply> reply_list;

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Long getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Long create_date) {
		this.create_date = create_date;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Integer getInquire_type() {
		return inquire_type;
	}

	public void setInquire_type(Integer inquire_type) {
		this.inquire_type = inquire_type;
	}

	public Integer getProcessing_state() {
		return processing_state;
	}

	public void setProcessing_state(Integer processing_state) {
		this.processing_state = processing_state;
	}

	public Long getMember_no() {
		return member_no;
	}

	public void setMember_no(Long member_no) {
		this.member_no = member_no;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUpdate_phone() {
		return update_phone;
	}

	public void setUpdate_phone(String update_phone) {
		this.update_phone = update_phone;
	}

	public String getUpdate_email() {
		return update_email;
	}

	public void setUpdate_email(String update_email) {
		this.update_email = update_email;
	}

	public String getInquire_type_name(){
		InquireType inquireType = InquireType.get(inquire_type);
		return (inquireType!=null?inquireType.getName():"미정의값 : " + inquire_type);
    }
	
	public String getProcessing_state_name(){
		return ProcessingStateType.get(processing_state).getName();
	}

	public List<CustomerReply> getReply_list() {
		return reply_list;
	}

	public void setReply_list(List<CustomerReply> reply_list) {
		this.reply_list = reply_list;
	}

	public boolean insertValidation() {
	    return StringUtils.isNotEmpty(contents) && StringUtils.isNotEmpty(update_email) && StringUtils.isNotEmpty(update_phone) && inquire_type != null;
    }

    public boolean updateValidation() {
        return cno > 0 && StringUtils.isNotEmpty(contents) && StringUtils.isNotEmpty(update_email) && StringUtils.isNotEmpty(update_phone) && inquire_type != null;
    }

    public boolean deleteValidation() {
        return cno > 0;
    }
}

package com.zipdoc.acs.domain.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ShopOrderGoods {

	//일련번호
	private Long sno;

	//주문번호
	private String order_no;

	//주문상태
	private Integer order_status;

	//상품번호
	private long goods_no;

	//상품코드
	private String goods_code;

	//상품명
	private String goods_name;

	//옵션가격(할인율에 적용된 가격포함)
	private Integer goods_price;

	//모델명
	private String model_name;

	//제조사
	private String maker_name;

	//상품옵션 일련번호
	private long goods_option_sno;
	
	//상품옵션 이름
	private String goods_option_value;
	
	//상품옵션 갯수
	private Integer goods_option_cnt;
	
	//상품옵션 이름
	private String option_value;

	//원산지
	private String origin_name;

	//카테고리 코드
	private String category_code;

	//택배사 일련번호
	private Long delivery_status;
	
	//택배비
	private Integer delivery_price;
	
	//택배회사이름
	private String delivery_company_name;

	//송장번호 등록일
	private Date invoice_date;

	//배송일자
	private Date delivery_date;

	//입금 일자
	private Date payment_date;

	//배송 완료일자
	private Date delivery_complete_date;

	//구매 확정일자
	private Date finish_date;

	//등록일
	private Date create_date;

	//수정일
	private Date update_date;

	//2017-03-15 송장번호
	private String invoice_number;
	
	private String billing_status;
	
	//handle mode
	private Integer handle_mode;
	
	private Integer handle_complete_status;

	//상품정보
	private String goods_main_img;


	//상품 옵션 할인율
	private Long option_discount;

	//상품 옵션 할인시작일
	private Date option_discount_start_date;

	//상품 옵션 할인종료일
	private Date option_discount_end_date;

	//상품 옵션 할인여부 1:할인안함 , 2:할인함
	private Integer option_discount_status;

	//상품 옵션 할인금액
	private long option_discount_purchase;

	//상품 옵션 할인가
	private int option_discount_price;
	
	//상품 옵션 기존 가격
	private int orgin_goods_price;

	public Long getSno() {
		return sno;
	}

	public void setSno(Long sno) {
		this.sno = sno;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public Integer getOrder_status() {
		return order_status;
	}

	public void setOrder_status(Integer order_status) {
		this.order_status = order_status;
	}


	public long getGoods_no() {
		return goods_no;
	}

	public void setGoods_no(long goods_no) {
		this.goods_no = goods_no;
	}

	public String getGoods_code() {
		return goods_code;
	}

	public void setGoods_code(String goods_code) {
		this.goods_code = goods_code;
	}

	public String getGoods_name() {
		return goods_name;
	}

	public void setGoods_name(String goods_name) {
		this.goods_name = goods_name;
	}

	public Integer getGoods_price() {
		return goods_price;
	}

	public void setGoods_price(Integer goods_price) {
		this.goods_price = goods_price;
	}

	public String getModel_name() {
		return model_name;
	}

	public void setModel_name(String model_name) {
		this.model_name = model_name;
	}

	public String getMaker_name() {
		return maker_name;
	}

	public void setMaker_name(String maker_name) {
		this.maker_name = maker_name;
	}

	public long getGoods_option_sno() {
		return goods_option_sno;
	}

	public void setGoods_option_sno(long goods_option_sno) {
		this.goods_option_sno = goods_option_sno;
	}

	public String getGoods_option_value() {
		return goods_option_value;
	}

	public void setGoods_option_value(String goods_option_value) {
		this.goods_option_value = goods_option_value;
	}

	public Integer getGoods_option_cnt() {
		return goods_option_cnt;
	}

	public void setGoods_option_cnt(Integer goods_option_cnt) {
		this.goods_option_cnt = goods_option_cnt;
	}

	public String getOption_value() {
		return option_value;
	}

	public void setOption_value(String option_value) {
		this.option_value = option_value;
	}

	public String getOrigin_name() {
		return origin_name;
	}

	public void setOrigin_name(String origin_name) {
		this.origin_name = origin_name;
	}

	public String getCategory_code() {
		return category_code;
	}

	public void setCategory_code(String category_code) {
		this.category_code = category_code;
	}

	public Integer getDelivery_price() {
		return delivery_price;
	}

	public void setDelivery_price(Integer delivery_price) {
		this.delivery_price = delivery_price;
	}

	public String getDelivery_company_name() {
		return delivery_company_name;
	}

	public void setDelivery_company_name(String delivery_company_name) {
		this.delivery_company_name = delivery_company_name;
	}

	public Date getInvoice_date() {
		return invoice_date;
	}

	public void setInvoice_date(Date invoice_date) {
		this.invoice_date = invoice_date;
	}

	public Date getDelivery_date() {
		return delivery_date;
	}

	public void setDelivery_date(Date delivery_date) {
		this.delivery_date = delivery_date;
	}

	public Date getPayment_date() {
		return payment_date;
	}

	public void setPayment_date(Date payment_date) {
		this.payment_date = payment_date;
	}

	public Date getDelivery_complete_date() {
		return delivery_complete_date;
	}

	public void setDelivery_complete_date(Date delivery_complete_date) {
		this.delivery_complete_date = delivery_complete_date;
	}

	public Date getFinish_date() {
		return finish_date;
	}

	public void setFinish_date(Date finish_date) {
		this.finish_date = finish_date;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getInvoice_number() {
		return invoice_number;
	}

	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}

	public String getBilling_status() {
		return billing_status;
	}

	public void setBilling_status(String billing_status) {
		this.billing_status = billing_status;
	}

	public Long getDelivery_status() {
		return delivery_status;
	}

	public void setDelivery_status(Long delivery_status) {
		this.delivery_status = delivery_status;
	}

	public Integer getHandle_mode() {
		return handle_mode;
	}

	public void setHandle_mode(Integer handle_mode) {
		this.handle_mode = handle_mode;
	}

	public Integer getHandle_complete_status() {
		return handle_complete_status;
	}

	public void setHandle_complete_status(Integer handle_complete_status) {
		this.handle_complete_status = handle_complete_status;
	}

	public String getGoods_main_img() {
		return goods_main_img;
	}

	public void setGoods_main_img(String goods_main_img) {
		this.goods_main_img = goods_main_img;
	}

	public Long getOption_discount() {
		return option_discount;
	}

	public void setOption_discount(Long option_discount) {
		this.option_discount = option_discount;
	}

	public Date getOption_discount_start_date() {
		return option_discount_start_date;
	}

	public void setOption_discount_start_date(Date option_discount_start_date) {
		this.option_discount_start_date = option_discount_start_date;
	}

	public Date getOption_discount_end_date() {
		return option_discount_end_date;
	}

	public void setOption_discount_end_date(Date option_discount_end_date) {
		this.option_discount_end_date = option_discount_end_date;
	}

	public Integer getOption_discount_status() {
		return option_discount_status;
	}

	public void setOption_discount_status(Integer option_discount_status) {
		this.option_discount_status = option_discount_status;
	}

	public long getOption_discount_purchase() {
		return option_discount_purchase;
	}

	public void setOption_discount_purchase(long option_discount_purchase) {
		this.option_discount_purchase = option_discount_purchase;
	}

	public int getOption_discount_price() {
		return option_discount_price;
	}

	public void setOption_discount_price(int option_discount_price) {
		this.option_discount_price = option_discount_price;
	}

	public int getOrgin_goods_price() {
		return orgin_goods_price;
	}

	public void setOrgin_goods_price(int orgin_goods_price) {
		this.orgin_goods_price = orgin_goods_price;
	}
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.define.AgentType;
import com.zipdoc.acs.define.MemberStatus;
import com.zipdoc.acs.define.UserType;
import com.zipdoc.acs.model.AccountType;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Member {
	private @JsonProperty("subs_type") int user_level;
	private @JsonProperty("account_type") int account_type = AccountType.NO_ACCOUNT.value();
	private @JsonProperty("account") String member_id;
	private @JsonProperty("name") String username;
	private String nickname;
	private String profile_url;
	private @JsonIgnore String profile_path;
	private @JsonIgnore String profile_file;

	private Long member_no;
	private @JsonIgnore long zid;
	private @JsonIgnore String zid_key;
	private @JsonIgnore String password;
	private @JsonIgnore String mobile_no;
	private @JsonIgnore String phone_no;
	private @JsonIgnore Integer status;
	private @JsonIgnore Integer member_type;
	private @JsonIgnore String app_version;
	private @JsonIgnore String os_type;
	private @JsonIgnore String os_version;
	private @JsonIgnore String device_id;
	private @JsonIgnore String device_model;
	private @JsonIgnore String email;
	private @JsonIgnore String push_token;
	private @JsonIgnore String reg_date;
	private @JsonIgnore String mod_date;
	private @JsonIgnore String login_ip;
	private @JsonIgnore String grade;


	// 2016.09.23. 김종부. 파트너 사 연동 추가.
	private @JsonIgnore int partner_id;

	private @JsonIgnore int mileage;

	// 2017.10.17 김대성 약관동의 추가
	private @JsonIgnore String person_agree;
	private @JsonIgnore String person_agree_privacy;

	//2017.12.14. 장한솔 비밀번호 변경 시간 추가
	private Long password_ch_date;

	//2018.1.2. 장한솔 (마케팅수신동의 추가)
	private @JsonIgnore int agreement_marketing;

	public Member() {}
	
	public Member(
			long zid, 
			String zid_key, 
			String mobile_no, 
			String app_version, 
			String os_type, 
			String os_version, 
			String device_id, 
			String device_model, 
			String push_token) {
		this.zid = zid;
		this.zid_key = zid_key;
		this.mobile_no = mobile_no;
		this.app_version = app_version;
		this.os_type = os_type;
		this.os_version = os_version;
		this.device_id = device_id;
		this.device_model = device_model;
		this.push_token = push_token;
	}

	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public Long getMember_no() {
		return member_no;
	}

	public void setMember_no(Long member_no) {
		this.member_no = member_no;
	}

	public long getZid() {
		return zid;
	}
	
	public void setZid(long zid) {
		this.zid = zid;
	}
	
	public String getZid_key() {
		return zid_key;
	}
	
	public void setZid_key(String zid_key) {
		this.zid_key = zid_key;
	}
	
	public int getAccount_type() {
		return account_type;
	}
	
	public void setAccount_type(int account_type) {
		this.account_type = account_type;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getUser_level() {
		return user_level;
	}
	
	public void setUser_level(int user_level) {
		this.user_level = user_level;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getMobile_no() {
		return mobile_no;
	}
	
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	
	public String getApp_version() {
		return app_version;
	}
	
	public void setApp_version(String app_version) {
		this.app_version = app_version;
	}
	
	public String getOs_type() {
		return os_type;
	}
	
	public void setOs_type(String os_type) {
		this.os_type = os_type;
	}
	
	public String getOs_version() {
		return os_version;
	}
	
	public void setOs_version(String os_version) {
		this.os_version = os_version;
	}
	
	public String getDevice_id() {
		return device_id;
	}
	
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	
	public String getDevice_model() {
		return device_model;
	}
	
	public void setDevice_model(String device_model) {
		this.device_model = device_model;
	}
	
	public String getPush_token() {
		return push_token;
	}
	
	public void setPush_token(String push_token) {
		this.push_token = push_token;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getMember_type() {
		return member_type;
	}

	public void setMember_type(Integer member_type) {
		this.member_type = member_type;
	}

	public String getReg_date() {
		return reg_date;
	}
	
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
	public String getMod_date() {
		return mod_date;
	}
	
	public void setMod_date(String mod_date) {
		this.mod_date = mod_date;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getLogin_ip() {
		return login_ip;
	}

	public void setLogin_ip(String login_ip) {
		this.login_ip = login_ip;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@JsonIgnore
	public UserType getUserType(){
		if(member_no != null && member_no > 0) {
			return UserType.MEMBER;
		} else {
			return UserType.AUTO_REGI;
		}
	}

	@JsonIgnore
	public UserType getUserType(AgentType agentType) {
		if (member_no != null && member_no > 0) {
			return UserType.MEMBER;
		} else {
			return agentType == AgentType.APP ? UserType.AUTO_REGI : UserType.NO_MEMBER;
		}
	}

	@JsonIgnore
	public Long getUserId(){
		if(getUserType()==UserType.MEMBER){
			return member_no;
		} else {
			return zid;
		}
	}

	@JsonIgnore
	public Long getUserId(AgentType agentType) {
		if(getUserType()==UserType.MEMBER){
			return member_no;
		} else {
			return agentType == AgentType.APP ? zid : null;
		}
	}

	public int getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(int partner_id) {
		this.partner_id = partner_id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getProfile_file() {
		return profile_file;
	}

	public void setProfile_file(String profile_file) {
		this.profile_file = profile_file;
	}

	public String getProfile_path() {
		return profile_path;
	}

	public void setProfile_path(String profile_path) {
		this.profile_path = profile_path;
	}

	public String getProfile_url() {
		return profile_url;
	}

	public void setProfile_url(String profile_url) {
		this.profile_url = profile_url;
	}

	@JsonIgnore
	public boolean isAccountNormal(){
		return (this.member_no != null && this.member_no > 0 && this.status == MemberStatus.ENABLE.getCode());
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}

	public String getPerson_agree() {
		return person_agree;
	}

	public void setPerson_agree(String person_agree) {
		this.person_agree = person_agree;
	}

	public String getPerson_agree_privacy() {
		return person_agree_privacy;
	}

	public void setPerson_agree_privacy(String person_agree_privacy) {
		this.person_agree_privacy = person_agree_privacy;
	}

	public Long getPassword_ch_date() {
		return password_ch_date;
	}

	public void setPassword_ch_date(Long password_ch_date) {
		this.password_ch_date = password_ch_date;
	}

	public int getAgreement_marketing() {
		return agreement_marketing;
	}

	public void setAgreement_marketing(int agreement_marketing) {
		this.agreement_marketing = agreement_marketing;
	}
}

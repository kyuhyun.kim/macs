package com.zipdoc.acs.domain.entity;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Created by dskim on 2017. 9. 22..
 * Updated by moonji on 2017. 12. 04..
 */
public class PartnerAssignGeo {

    private long estimate_no;

    // 문의 일자
    private Long update_date;

    private String latitude;

    private String longitude;

    private int stop_code;

    private int behind_contract;

    private String phone_no;

    public long getEstimate_no() {
        return estimate_no;
    }

    public void setEstimate_no(long estimate_no) {
        this.estimate_no = estimate_no;
    }

    public Long getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Long update_date) {
        this.update_date = update_date;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getStop_code() {
        return stop_code;
    }

    public void setStop_code(int stop_code) {
        this.stop_code = stop_code;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public int getBehind_contract() {
        return behind_contract;
    }

    public void setBehind_contract(int behind_contract) {
        this.behind_contract = behind_contract;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(estimate_no).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PartnerAssignGeo){
            PartnerAssignGeo pag = (PartnerAssignGeo) obj;
            return new EqualsBuilder().append(estimate_no, pag.estimate_no).isEquals();
        }

        return false;

    }
}

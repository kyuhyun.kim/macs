package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by ZIPDOC on 2017-03-08.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Profile {

    private String nickname;
    private Long member_no;
    private String profile_url;
    @JsonIgnore private String profile_path;
    @JsonIgnore private String profile_file;

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public String getProfile_file() {
        return profile_file;
    }

    public void setProfile_file(String profile_file) {
        this.profile_file = profile_file;
    }

}

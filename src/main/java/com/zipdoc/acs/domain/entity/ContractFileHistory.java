package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ContractFileHistory {

    private Long history_no;
    private Long contract_no;
    private String file_type;
    private String file_name_org;

    @JsonIgnore private String file_name;
    @JsonIgnore private String file_path;
    @JsonIgnore private String thumbnail;

    private String download_url;
    private String thumbnail_url;
    private Long creater;

    public ContractFileHistory() {
    }

    public ContractFileHistory(Long history_no, Long contract_no, String file_type, String file_name_org, String file_name, String file_path, String thumbnail, String download_url, String thumbnail_url, Long creater) {
        this.history_no = history_no;
        this.contract_no = contract_no;
        this.file_type = file_type;
        this.file_name_org = file_name_org;
        this.file_name = file_name;
        this.file_path = file_path;
        this.thumbnail = thumbnail;
        this.download_url = download_url;
        this.thumbnail_url = thumbnail_url;
        this.creater = creater;
    }

    public Long getHistory_no() {
        return history_no;
    }

    public void setHistory_no(Long history_no) {
        this.history_no = history_no;
    }

    public Long getContract_no() {
        return contract_no;
    }

    public void setContract_no(Long contract_no) {
        this.contract_no = contract_no;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getFile_name_org() {
        return file_name_org;
    }

    public void setFile_name_org(String file_name_org) {
        this.file_name_org = file_name_org;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDownload_url() {
        return download_url;
    }

    public void setDownload_url(String download_url) {
        this.download_url = download_url;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public Long getCreater() {
        return creater;
    }

    public void setCreater(Long creater) {
        this.creater = creater;
    }
}

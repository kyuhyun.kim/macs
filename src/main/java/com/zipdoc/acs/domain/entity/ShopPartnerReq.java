package com.zipdoc.acs.domain.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShopPartnerReq {
	private int sno;
	private String subject;
	private String contents;
	private Integer partner_id;
	private String writer;
	private String username;
	
	@JsonProperty("create_date") private Long create_timestamp;
	private Date create_date;
	private Date update_date;
	private String email;
	private String telephone;
	
	private Long creater;
	private Long updater;
	
	private int request_type;
	private int processing_state;
	private String ip;
	
	public int getSno() {
		return sno;
	}
	public void setSno(int sno) {
		this.sno = sno;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public Integer getPartner_id() {
		return partner_id;
	}
	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Long getCreater() {
		return creater;
	}
	public void setCreater(Long creater) {
		this.creater = creater;
	}
	public Long getUpdater() {
		return updater;
	}
	public void setUpdater(Long updater) {
		this.updater = updater;
	}
	public int getRequest_type() {
		return request_type;
	}
	public void setRequest_type(int request_type) {
		this.request_type = request_type;
	}
	public int getProcessing_state() {
		return processing_state;
	}
	public void setProcessing_state(int processing_state) {
		this.processing_state = processing_state;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	@Override
	public String toString() {
		return "ShopPartnerReq [sno=" + sno + ", subject=" + subject + ", contents=" + contents + ", partner_id="
				+ partner_id + ", writer=" + writer + ", username=" + username + ", create_timestamp="
				+ create_timestamp + ", create_date=" + create_date + ", update_date=" + update_date + ", email="
				+ email + ", telephone=" + telephone + ", creater=" + creater + ", updater=" + updater
				+ ", request_type=" + request_type + ", processing_state=" + processing_state + ", ip=" + ip + "]";
	}
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.utils.BitUtils;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class VividComment {

	/* 집닥 서비스 만족도 */
	private String 	cs_zipdoc_svc;
	/* 집닥맨 현장방문 만족도 */
	private String 	cs_zipdocman_svc;
	/* 공사품질/파트너스 만족도 */
	private String cs_partner_quality;
	/* 후기 입력 */
	private String 	opinions;

	private Long 	end_date;
	private Long 	start_date;
	private String 	category_name1;
	private String 	category_name2;
	private String 	region;
	private String 	real_contracter;
	private String	real_partner_name;
	private String 	contracter;
	private String	partner_name;
	private Long 	contract_no;
	private BigDecimal avg_score;
	private String avg_score_msg;

	public String getAvg_score_msg() {
		return avg_score_msg;
	}

	public void setAvg_score_msg(String avg_score_msg) {
		this.avg_score_msg = avg_score_msg;
	}

	public Long getStart_date() {
		return start_date;
	}

	public void setStart_date(Long start_date) {
		this.start_date = start_date;
	}

	public BigDecimal getAvg_score() {
		return avg_score;
	}

	public void setAvg_score() {

		this.avg_score = getavgsurveyscore();
		if(this.avg_score.doubleValue() > 4.5 )
			this.avg_score_msg = "완벽해요!";
		else if(this.avg_score.doubleValue() > 	4.0 )
			this.avg_score_msg = "최고에요!";
		else if(this.avg_score.doubleValue() > 	3.5 )
			this.avg_score_msg = "매우 좋아요!";
		else if(this.avg_score.doubleValue() >= 3.0 )
			this.avg_score_msg = "좋아요!";
		else
			this.avg_score_msg = "";
	}

	public Long getContract_no() {
		return contract_no;
	}

	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}

	public String getCs_partner_quality() {
		return cs_partner_quality;
	}

	public void setCs_partner_quality(String cs_partner_quality) {
		this.cs_partner_quality = cs_partner_quality;
	}

	public String getCs_zipdoc_svc() {
		return cs_zipdoc_svc;
	}

	public void setCs_zipdoc_svc(String cs_zipdoc_svc) {
		this.cs_zipdoc_svc = cs_zipdoc_svc;
	}

	public String getCs_zipdocman_svc() {
		return cs_zipdocman_svc;
	}

	public void setCs_zipdocman_svc(String cs_zipdocman_svc) {
		this.cs_zipdocman_svc = cs_zipdocman_svc;
	}

	public String getOpinions() {
		return opinions;
	}

	public void setOpinions(String opinions) {
		this.opinions = opinions;
	}


	public Long getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Long end_date) {
		this.end_date = end_date;
	}

	public void setAvg_score(BigDecimal avg_score) {
		this.avg_score = avg_score;
	}

	public String getCategory_name1() {
		return category_name1;
	}

	public void setCategory_name1(String category_name1) {
		this.category_name1 = category_name1;
	}

	public String getCategory_name2() {
		return category_name2;
	}

	public void setCategory_name2(String category_name2) {
		this.category_name2 = category_name2;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getReal_contracter() {
		return real_contracter;
	}

	public void setReal_contracter(String real_contracter) {
		this.real_contracter = real_contracter;
	}

	public String getReal_partner_name() {
		return real_partner_name;
	}

	public void setReal_partner_name(String real_partner_name) {
		this.real_partner_name = real_partner_name;
	}

	public String getContracter() {
		return contracter;
	}

	public void setContracter(String contracter) {
		this.contracter = contracter;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public BigDecimal getavgsurveyscore(){
		int denominator = 0;
		BigDecimal score = new BigDecimal(0);
		if( cs_zipdoc_svc != null ){
			score = score.add( BigDecimal.valueOf(  Double.parseDouble( this.cs_zipdoc_svc ) ) );
			denominator++;
		}
		if( cs_partner_quality != null ){
			score = score.add(BigDecimal.valueOf(  Double.parseDouble(cs_partner_quality)));
			denominator++;
		}
		if( cs_zipdocman_svc != null ){
			score = score.add(BigDecimal.valueOf(  Double.parseDouble(cs_zipdocman_svc)));
			denominator++;
		}

		if( denominator == 0 ){
			return null;
		}
		return score.divide(new BigDecimal(denominator), 1, BigDecimal.ROUND_HALF_UP).setScale(1, BigDecimal.ROUND_HALF_UP);
	}
}

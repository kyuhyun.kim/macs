package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.utils.Messages;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerContract {

	// 계약 번호
	private long contract_no;
	
	// 계약 상태
	private int contract_status;
	
	// 계약일
	private Long contract_date;
	
	// 계약명
	private String contract_name;
	
	// 계약금액
	private Long contract_price;
	
	// 계약자
	private String contracter;
	
	// 계약자 전화번호
	@JsonIgnore private String phone_no;
	
	// 이메일
	@JsonIgnore private String email;
	
	// 분류코드-1
	@JsonIgnore private String category_code1;
	
	// 분류코드-2
	@JsonIgnore private String category_code2;
	
	// 공사예산
	@JsonIgnore private String budget;
	
	// 공사면적
	@JsonIgnore private String space;
	
	// 우편번호
	@JsonIgnore private String post_no;
	
	// 주소
	@JsonIgnore private String address;
	
	// 주소 상세
	@JsonIgnore private String address_detail;
	
	// 종료 여부
	private Integer completed_flag;
	
	// 종료 코드
	private Integer completed_code;
	
	// 계약자 회원번호
	private Long member_no;
	
	// 공사 시작일
	private Long start_date;
	
	// 공사 마감일
	private Long end_date;
	
	// 공사 마감 예정일
	private Long expect_end_date;
	
	// 무상 보증일
	private Long warranty_date;

	// 중계 수수료 비율
	private Integer commission_rate;
	
	// 중개수수료
	@JsonIgnore private Integer commission;
	
	// 부가세
	@JsonIgnore private Integer vat_tax;
	
	// 합산
	private Integer amount_sum;
	
	// 수금상태
	@JsonIgnore private Integer collection_status;
	
	// 계산서 발급여부
	@JsonIgnore private Integer tax_invoice_yn;
	
	// 견적번호
	@JsonIgnore private Long estimate_no;
	
	// 파트너 구분자
	private Integer partner_id;
	
	// 파트너 명
	private String partner_name;
	
	// 메모
	@JsonIgnore private String memo;
	
	// 협의 상태
	private Integer agreement_status;
	
	// 표준계약서 발급 상태
	private Integer std_contract_status;

	private Integer interior_product_code;
	
	// 등록일
	@JsonIgnore private Long create_date;
	
	// 수정일
	@JsonIgnore private Long update_date;
	
	// 등록자
	@JsonIgnore private Long creater;
	
	// 수정자
	@JsonIgnore private Long updater;

	// 납부 기한
	private Long expiration_date;

	private List<ContractFile> attached_files;

	//기타 수수료 flag
	private Integer etc_commition_flag;

	//기타 수수료율
	private float etc_commition;

	public long getContract_no() {
		return contract_no;
	}
	
	public void setContract_no(long contract_no) {
		this.contract_no = contract_no;
	}
	
	public int getContract_status() {
		return contract_status;
	}
	
	public void setContract_status(int contract_status) {
		this.contract_status = contract_status;
	}

	public Long getContract_date() {
		return contract_date;
	}

	public void setContract_date(Long contract_date) {
		this.contract_date = contract_date;
	}

	public String getContract_name() {
		return contract_name;
	}

	public void setContract_name(String contract_name) {
		this.contract_name = contract_name;
	}

	public Long getContract_price() {
		return contract_price;
	}

	public void setContract_price(Long contract_price) {
		this.contract_price = contract_price;
	}

	public String getContracter() {
		return contracter;
	}

	public void setContracter(String contracter) {
		this.contracter = contracter;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCategory_code1() {
		return category_code1;
	}

	public void setCategory_code1(String category_code1) {
		this.category_code1 = category_code1;
	}

	public String getCategory_code2() {
		return category_code2;
	}

	public void setCategory_code2(String category_code2) {
		this.category_code2 = category_code2;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public String getPost_no() {
		return post_no;
	}

	public void setPost_no(String post_no) {
		this.post_no = post_no;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress_detail() {
		return address_detail;
	}

	public void setAddress_detail(String address_detail) {
		this.address_detail = address_detail;
	}

	public Integer getCompleted_flag() {
		return completed_flag;
	}

	public void setCompleted_flag(Integer completed_flag) {
		this.completed_flag = completed_flag;
	}

	public Integer getCompleted_code() {
		return completed_code;
	}

	public void setCompleted_code(Integer completed_code) {
		this.completed_code = completed_code;
	}

	public Long getMember_no() {
		return member_no;
	}

	public void setMember_no(Long member_no) {
		this.member_no = member_no;
	}

	public Long getStart_date() {
		return start_date;
	}

	public void setStart_date(Long start_date) {
		this.start_date = start_date;
	}

	public Long getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Long end_date) {
		this.end_date = end_date;
	}

	public Long getExpect_end_date() {
		return expect_end_date;
	}

	public void setExpect_end_date(Long expect_end_date) {
		this.expect_end_date = expect_end_date;
	}

	public Long getWarranty_date() {
		return warranty_date;
	}

	public void setWarranty_date(Long warranty_date) {
		this.warranty_date = warranty_date;
	}

	public Integer getCommission() {
		return commission;
	}

	public void setCommission(Integer commission) {
		this.commission = commission;
	}

	public Integer getVat_tax() {
		return vat_tax;
	}

	public void setVat_tax(Integer vat_tax) {
		this.vat_tax = vat_tax;
	}

	public Integer getAmount_sum() {
		return amount_sum;
	}

	public void setAmount_sum(Integer amount_sum) {
		this.amount_sum = amount_sum;
	}

	public Integer getCollection_status() {
		return collection_status;
	}

	public void setCollection_status(Integer collection_status) {
		this.collection_status = collection_status;
	}

	public Integer getTax_invoice_yn() {
		return tax_invoice_yn;
	}

	public void setTax_invoice_yn(Integer tax_invoice_yn) {
		this.tax_invoice_yn = tax_invoice_yn;
	}

	public Long getEstimate_no() {
		return estimate_no;
	}

	public void setEstimate_no(Long estimate_no) {
		this.estimate_no = estimate_no;
	}

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getAgreement_status() {
		return agreement_status;
	}

	public void setAgreement_status(Integer agreement_status) {
		this.agreement_status = agreement_status;
	}

	public Integer getStd_contract_status() {
		return std_contract_status;
	}

	public void setStd_contract_status(Integer std_contract_status) {
		this.std_contract_status = std_contract_status;
	}

	public Long getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Long create_date) {
		this.create_date = create_date;
	}

	public Long getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Long update_date) {
		this.update_date = update_date;
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public Integer getCommission_rate() {
		return commission_rate;
	}

	public void setCommission_rate(Integer commission_rate) {
		this.commission_rate = commission_rate;
	}

	public Long getExpiration_date() {
		return expiration_date;
	}

	public void setExpiration_date(Long expiration_date) {
		this.expiration_date = expiration_date;
	}

	public Integer getInterior_product_code() {
		return interior_product_code;
	}

	public void setInterior_product_code(Integer interior_product_code) {
		this.interior_product_code = interior_product_code;
	}

	public List<ContractFile> getAttached_files() {
		return attached_files;
	}

	public void setAttached_files(List<ContractFile> attached_files) {
		this.attached_files = attached_files;
	}

	public Integer getEtc_commition_flag() {
		return etc_commition_flag;
	}

	public void setEtc_commition_flag(Integer etc_commition_flag) {
		this.etc_commition_flag = etc_commition_flag;
	}

	public float getEtc_commition() {
		return etc_commition;
	}

	public void setEtc_commition(float etc_commition) {
		this.etc_commition = etc_commition;
	}

	//수수료 및 부가세 계산
	public void calculateCommition() {
		if(this.commission_rate==null) commission_rate = 5;
		if(this.contract_price==null || contract_price <= 0) return;

		this.commission = (int)Math.round((contract_price * commission_rate)/100.0);
		this.vat_tax = (int)Math.round((Math.round((contract_price * commission_rate)/100.0) * 0.1));
		this.amount_sum = (int)Math.round(((contract_price * commission_rate)/100.0) + (Math.round((contract_price * commission_rate)/100.0) * 0.1));
	}


	public void buildUrl(){
		for(ContractFile contractFile: attached_files){
			contractFile.buildUrl();
		}
	}
}

package com.zipdoc.acs.domain.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShopDeliveryManagement {
	private Long sno;
	//배송지 이름
	private String delivery_areaname;
	//받으시는분 
	private String delivery_username;
	//배송지 우편번호
	private String delivery_zipcode;
	//배송지 주소
	private String delivery_address;
	//배송지 상세주소
	private String delivery_address_sub;
	//휴대전화
	private String delivery_mobile_no;
	//유선전화
	private String delivery_phone_no;
	//기본배송지 여부
	private Integer default_address_yn;
	
	@JsonProperty("create_date") private Long create_timestamp;
	private Long creater;
	private Long updater;
	private Date create_date;
	private Date update_date;
	
	public Long getSno() {
		return sno;
	}
	public void setSno(Long sno) {
		this.sno = sno;
	}
	public String getDelivery_areaname() {
		return delivery_areaname;
	}
	public void setDelivery_areaname(String delivery_areaname) {
		this.delivery_areaname = delivery_areaname;
	}
	public String getDelivery_username() {
		return delivery_username;
	}
	public void setDelivery_username(String delivery_username) {
		this.delivery_username = delivery_username;
	}
	public String getDelivery_zipcode() {
		return delivery_zipcode;
	}
	public void setDelivery_zipcode(String delivery_zipcode) {
		this.delivery_zipcode = delivery_zipcode;
	}
	public String getDelivery_address() {
		return delivery_address;
	}
	public void setDelivery_address(String delivery_address) {
		this.delivery_address = delivery_address;
	}
	public String getDelivery_address_sub() {
		return delivery_address_sub;
	}
	public void setDelivery_address_sub(String delivery_address_sub) {
		this.delivery_address_sub = delivery_address_sub;
	}
	public String getDelivery_mobile_no() {
		return delivery_mobile_no;
	}
	public void setDelivery_mobile_no(String delivery_mobile_no) {
		this.delivery_mobile_no = delivery_mobile_no;
	}
	public String getDelivery_phone_no() {
		return delivery_phone_no;
	}
	public void setDelivery_phone_no(String delivery_phone_no) {
		this.delivery_phone_no = delivery_phone_no;
	}
	public Integer getDefault_address_yn() {
		return default_address_yn;
	}
	public void setDefault_address_yn(Integer default_address_yn) {
		this.default_address_yn = default_address_yn;
	}
	public Long getCreater() {
		return creater;
	}
	public void setCreater(Long creater) {
		this.creater = creater;
	}
	public Long getUpdater() {
		return updater;
	}
	public void setUpdater(Long updater) {
		this.updater = updater;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	
	@Override
	public String toString() {
		return "ShopDeliveryManagement [sno=" + sno + ", delivery_areaname=" + delivery_areaname
				+ ", delivery_username=" + delivery_username + ", delivery_zipcode=" + delivery_zipcode
				+ ", delivery_address=" + delivery_address + ", delivery_address_sub=" + delivery_address_sub
				+ ", delivery_mobile_no=" + delivery_mobile_no + ", delivery_phone_no=" + delivery_phone_no
				+ ", default_address_yn=" + default_address_yn + ", creater=" + creater + ", updater=" + updater
				+ ", create_timestamp=" + create_timestamp + ", create_date=" + create_date + ", update_date=" + update_date + "]";
	}
}

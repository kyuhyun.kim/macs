package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LayoutPartner {

    private String title;
    private List<Partner> list;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Partner> getList() {
        return list;
    }

    public void setList(List<Partner> list) {
        this.list = list;
    }
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.PictureType;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * Created by dklee on 2016-06-09.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Partner {
	
    private Integer partner_id;
    private String partner_name;
    private String main_part;
    private String region;
    private String intro_msg;
    private String logo_url;
    private String enable_reply;
    
    // 2016.10.05. KJB. 추가.
    private String address;
    private Integer contract_code;
    private int status;
    private int accept_step;
    private Integer kind;   // 시공분류

    @JsonIgnore private String reply_count;
    @JsonIgnore private String file_path;
    @JsonIgnore private String file_name;
    @JsonIgnore private String thumbnail;
    @JsonIgnore private String logo;
    private int partner_type;

    @JsonIgnore private Integer commission_rate;
    @JsonIgnore private Integer partner_commission;

    // 업체 이미지
    private String image_url;
    private String thumbnail_url;

    // 업체 대표명
    private String ceo_name;

    // 업체 대표 전화 번호
    private String phone_no;

    // 업체 경력
    private Integer biz_period;

    // 노출순위
    @JsonIgnore private Integer seq;

    // 영업장 주소
    private String site_address;

    private List<Product> list;

    // 2018.08.21. KJB. 시공사례 썸네일 리스트
    private List<String> gallery_thumbnails;

    // 영업장 주소 전체
    @JsonIgnore private String real_address;

    // 2019. 01. 10 KKH 월별 베스트 파트너관련
    private Integer num_idx;

    private String month;

    private Integer is_relief;
    /* 분기별 추천 파트너스 추가 */
    private String best_year;
    private Integer best_quarter;

    private Long estimate_price;

    public int getAccept_step() {
        return accept_step;
    }

    public void setAccept_step(int accept_step) {
        this.accept_step = accept_step;
    }

    public Long getEstimate_price() {
        return estimate_price;
    }

    public void setEstimate_price(Long estimate_price) {
        this.estimate_price = estimate_price;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getBest_year() {
        return best_year;
    }

    public void setBest_year(String best_year) {
        this.best_year = best_year;
    }

    public Integer getBest_quarter() {
        return best_quarter;
    }

    public void setBest_quarter(Integer best_quarter) {
        this.best_quarter = best_quarter;
    }

    public Integer getIs_relief() {
        return is_relief;
    }

    public void setIs_relief(Integer is_relief) {
        this.is_relief = is_relief;
    }

    public Integer getNum_idx() {
        return num_idx;
    }

    public void setNum_idx(Integer num_idx) {
        this.num_idx = num_idx;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getEnable_reply() {
        return enable_reply;
    }

    public void setEnable_reply(String enable_reply) {
        this.enable_reply = enable_reply;
    }

    public String getIntro_msg() {
        return intro_msg;
    }

    public void setIntro_msg(String intro_msg) {
        this.intro_msg = intro_msg;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getMain_part() {
        return main_part;
    }

    public void setMain_part(String main_part) {
        this.main_part = main_part;
    }

    public Integer getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(Integer partner_id) {
        this.partner_id = partner_id;
    }

    public String getPartner_name() {
        return partner_name;
    }

    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getReply_count() {
        return reply_count;
    }

    public void setReply_count(String reply_count) {
        this.reply_count = reply_count;
    }

    public List<Product> getList() {
        return list;
    }

    public void setList(List<Product> list) {
        this.list = list;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void buildPictureUrl(PictureType imageType, PictureType thumbnailType){

        // 업체 로고 URL
        if (StringUtils.isNotEmpty(logo)){
            this.logo_url = Constants.STATIC_URL + getFile_path() + getLogo();
        } else {
            this.logo_url = Constants.SERVER_URL + "images/default_partner_logo.jpg";
        }

        if (StringUtils.isNotEmpty(this.file_name)) {
            this.image_url = Constants.STATIC_URL + getFile_path() + this.file_name; //원본이미지
            this.thumbnail_url = Constants.STATIC_URL + getFile_path() + thumbnailType.getPrefix() + this.file_name;
        }

        if (list != null && list.size() > 0){
            for (Product obj : list){
                obj.buildPictureUrl(imageType, thumbnailType);
            }
        }
    }

	public int getPartner_type() {
		return partner_type;
	}

	public void setPartner_type(int partner_type) {
		this.partner_type = partner_type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getContract_code() {
		return contract_code;
	}

	public void setContract_code(Integer contract_code) {
		this.contract_code = contract_code;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

    public Integer getCommission_rate() {
        return commission_rate;
    }

    public void setCommission_rate(Integer commission_rate) {
        this.commission_rate = commission_rate;
    }

    /**
     * 파트너별 수수료율이 미설정 상태인 경우에는 계약상품에 설정된 수수료율에 따른다.
     * @return
     */
    public Integer getPartner_commission() {
        if (partner_commission == null) return commission_rate;
        return partner_commission;
    }

    public void setPartner_commission(Integer partner_commission) {
        this.partner_commission = partner_commission;
    }

    public Integer getKind() {
        return kind;
    }

    public void setKind(Integer kind) {
        this.kind = kind;
    }

    public List<String> getGallery_thumbnails() {
        return gallery_thumbnails;
    }

    public void setGallery_thumbnails(List<String> gallery_thumbnails) {
        this.gallery_thumbnails = gallery_thumbnails;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getCeo_name() {
        return ceo_name;
    }

    public void setCeo_name(String ceo_name) {
        this.ceo_name = ceo_name;
    }

    public Integer getBiz_period() {
        return biz_period;
    }

    public void setBiz_period(Integer biz_period) {
        this.biz_period = biz_period;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getReal_address() {
        return real_address;
    }

    public void setReal_address(String real_address) {
        this.real_address = real_address;
        if (StringUtils.isNotEmpty(real_address)) {
            String [] token = StringUtils.split(real_address, " ");
            if (token != null && token.length >= 2) {
                site_address = token[0] + " " + token[1];
            }
        }

    }

    public String getSite_address() {
        return site_address;
    }

    public void setSite_address(String site_address) {

        this.site_address = site_address;
    }
}

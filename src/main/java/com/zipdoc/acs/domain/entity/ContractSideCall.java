package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ContractSideCall {

    private long contract_side_call_no;
    private long zid;
    private long estimate_no;
    private int partner_id;
    private String partner_phone;
    private String writer_phone;
    private int outgoing_flag;
    private Long create_date;

    public long getEstimate_no() {
        return estimate_no;
    }

    public int getPartner_id() {
        return partner_id;
    }

    public String getPartner_phone() {
        return partner_phone;
    }

    public String getWriter_phone() {
        return writer_phone;
    }

    public int getOutgoing_flag() {
        return outgoing_flag;
    }

    public Long getCreate_date() {
        return create_date;
    }

    public long getZid() {
        return zid;
    }

    public long getContract_side_call_no() {
        return contract_side_call_no;
    }

    public void setEstimate_no(long estimate_no) {
        this.estimate_no = estimate_no;
    }

    public void setPartner_id(int partner_id) {
        this.partner_id = partner_id;
    }

    public void setPartner_phone(String partner_phone) {
        this.partner_phone = partner_phone;
    }

    public void setWriter_phone(String writer_phone) {
        this.writer_phone = writer_phone;
    }

    public void setOutgoing_flag(int outgoing_flag) {
        this.outgoing_flag = outgoing_flag;
    }

    public void setCreate_date(Long create_date) {
        this.create_date = create_date;
    }

    public void setZid(long zid) {
        this.zid = zid;
    }

    public void setContract_side_call_no(long contract_side_call_no) {
        this.contract_side_call_no = contract_side_call_no;
    }
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PromotionCode {

	@JsonIgnore private String promotion_code;
	@JsonIgnore private Integer status;
	@JsonIgnore private Integer service_target;
	@JsonIgnore private Date start_date;
	@JsonIgnore private Date end_date;
	private int reason_code;

	public PromotionCode() {}

	public PromotionCode(int reason_code) {
		this.reason_code = reason_code;
	}

	public String getPromotion_code() {
		return promotion_code;
	}

	public void setPromotion_code(String promotion_code) {
		this.promotion_code = promotion_code;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getService_target() {
		return service_target;
	}

	public void setService_target(Integer service_target) {
		this.service_target = service_target;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public int getReason_code() {
		return reason_code;
	}

	public void setReason_code(int reason_code) {
		this.reason_code = reason_code;
	}
}

package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.model.ProductListRequest;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LayoutGallery {

    private Integer total_count;
    private String category;
    private String category_name;
    private String filter;
    private Integer filter_type;
    private Integer space_min;
    private Integer space_max;
    private String title;

    private List<Product> list;

    @JsonIgnore ProductListRequest searchCondition;

    public LayoutGallery() {

    }

    public LayoutGallery(String category) {
        this.category = category;
    }

    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public List<Product> getList() {
        return list;
    }

    public void setList(List<Product> list) {
        this.list = list;
    }

    public ProductListRequest getSearchCondition() {
        return searchCondition;
    }

    public void setSearchCondition(ProductListRequest searchCondition) {
        this.searchCondition = searchCondition;
    }

    public Integer getFilter_type() {
        return filter_type;
    }

    public void setFilter_type(Integer filter_type) {
        this.filter_type = filter_type;
    }

    public Integer getSpace_min() {
        return space_min;
    }

    public void setSpace_min(Integer space_min) {
        this.space_min = space_min;
    }

    public Integer getSpace_max() {
        return space_max;
    }

    public void setSpace_max(Integer space_max) {
        this.space_max = space_max;
    }
}

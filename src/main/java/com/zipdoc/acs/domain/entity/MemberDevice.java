package com.zipdoc.acs.domain.entity;

import com.zipdoc.acs.define.OsType;
import org.apache.commons.lang.StringUtils;

/**
 * Created by ZIPDOC on 2016-10-06.
 */
public class MemberDevice {
    private Long member_no;
    private String member_id;
    private String mobile_no;
    private Long zid;
    private Long partner_id;
    private String device_id;
    private String push_token;
    private String os_type;
    private boolean validToken = true;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public Long getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(Long partner_id) {
        this.partner_id = partner_id;
    }

    public String getPush_token() {
        return push_token;
    }

    public void setPush_token(String push_token) {
        this.push_token = push_token;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String phone_no) {
        this.mobile_no = mobile_no;
    }

    public Long getZid() {
        return zid;
    }

    public void setZid(Long zid) {
        this.zid = zid;
    }

    public String getOs_type() {
        return os_type;
    }

    public void setOs_type(String os_type) {
        this.os_type = os_type;
    }

    public boolean isValidToken() {
        return validToken;
    }

    public void setValidToken(boolean validToken) {
        this.validToken = validToken;
    }

    public boolean isAndroid(){
        return StringUtils.equals(os_type, OsType.ANDROID.getCode());
    }

    public boolean isIOS(){
        return StringUtils.equals(os_type, OsType.IOS.getCode());
    }

    @Override
    public String toString() {
        return "MemberDevice{" +
                "member_no=" + member_no +
                ", member_id='" + member_id + '\'' +
                ", mobile_no='" + mobile_no + '\'' +
                ", partner_id=" + partner_id +
                ", zid=" + zid +
                ", device_id='" + device_id + '\'' +
                ", push_token='" + push_token + '\'' +
                '}';
    }
}

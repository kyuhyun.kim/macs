package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.AssignStatus;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.PictureType;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyEstimatePartner {

	private Integer partner_id;
	private String partner_name;

	// 견적진행 상태
	private Integer status;

	// 견적종료 상태
	private Integer completed_flag;

    @JsonIgnore private String logo;
    @JsonIgnore private String file_path;
	private String main_part;
	private String kind;
	private String logo_url;
	private List<String> gallery_thumbnails;

	// 견적 수락일
	private Long accept_date;

	// 통화 완료일
	private Long call_complete_date;

	// 방문예정일
	private Long schd_visit_date;

	// 방문완료일
	private Long visit_date;

	// 견적발송일
	private Long publish_date;

	// 마지막 정보 업데이트일
	@JsonIgnore private Long update_date;

	// 계약 완료일
	private Long contract_date;

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getAccept_date() {
		return accept_date;
	}

	public void setAccept_date(Long accept_date) {
		this.accept_date = accept_date;
	}

	public Long getCall_complete_date() {
		return call_complete_date;
	}

	public void setCall_complete_date(Long call_complete_date) {
		this.call_complete_date = call_complete_date;
	}

	public Long getSchd_visit_date() {
		return schd_visit_date;
	}

	public void setSchd_visit_date(Long schd_visit_date) {
		this.schd_visit_date = schd_visit_date;
	}

	public Long getVisit_date() {
		return visit_date;
	}

	public void setVisit_date(Long visit_date) {
		this.visit_date = visit_date;
	}

	public Long getPublish_date() {
		return publish_date;
	}

	public void setPublish_date(Long publish_date) {
		this.publish_date = publish_date;
	}

	public Long getContract_date() {
		return contract_date;
	}

	public void setContract_complete_date(Long contract_date) {
		this.contract_date = contract_date;
	}

	public Integer getCompleted_flag() {
		return completed_flag;
	}

	public void setCompleted_flag(Integer completed_flag) {
		this.completed_flag = completed_flag;
	}

	public Long getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Long update_date) {
		this.update_date = update_date;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getMain_part() {
		return main_part;
	}

	public void setMain_part(String main_part) {
		this.main_part = main_part;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public List<String> getGallery_thumbnails() {
		return gallery_thumbnails;
	}

	public void setGallery_thumbnails(List<String> gallery_thumbnails) {
		this.gallery_thumbnails = gallery_thumbnails;
	}

	public Long getEstimate_close_date() {
		if (status < AssignStatus.COMPLETE_CONTRACT.value() && completed_flag == 1) return update_date;
		return new Long(0);
	}

    public String getLogo_url() {
        return logo_url;
    }

    public void buildPictureUrl(){

        // 업체 로고 URL
        if (StringUtils.isNotEmpty(logo)){
            this.logo_url = Constants.STATIC_URL + getFile_path() + getLogo();
        } else {
            this.logo_url = Constants.SERVER_URL + "images/default_partner_logo.jpg";
        }
    }
}

package com.zipdoc.acs.domain.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.OrderMethodType;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ShopOrderInfo {
	//일련번호
    private long sno;

    //주문번호
    private String order_no;

    //주문유형
    private Integer order_type;

    //비회원 이메일
    private String non_member_email;

    //주문 상품명
    private String order_goods_name;

    //주문 상품 갯수
    private Integer order_goods_cnt;

    //총 주문 금액
    private Integer total_order_price;

    //총 상품 금액
    private Integer total_order_goods;

    //총 할인 금액
    private Integer total_discount_price;

    //메일/SMS 전송 여부
    private String mailing_yn;

    //입금 일자
    private Date payment_date;

    //무통장 입금 은행
    private String bank_account;

    //무통장 입금자
    private String bank_sender;

    //영수증 신청여부
    private Integer receipt_yn;

    //총 배송비
    private Integer total_delivery_charge;

    //등록자
    private Long creater;

    //등록일
    private Date create_date;

    //수정일
    private Date update_date;

    //주문자
    private String order_name;

    //수취인
    private String receiver_name;

    //취소,교환,반품,환불
    private String handle_mode;

    //결제상태
    private String billing_status;
    
    //배송상태
    private String delivery_status;

    //주문 방법
    private Integer order_method;
    private String order_method_name;

    //상품정보
    private Long goods_no;
    private String goods_main_img;

    private List<ShopOrderGoods> shopOrderGoodsList;

    public long getSno() {
        return sno;
    }

    public void setSno(long sno) {
        this.sno = sno;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public Integer getOrder_type() {
        return order_type;
    }

    public void setOrder_type(Integer order_type) {
        this.order_type = order_type;
    }

    public String getNon_member_email() {
        return non_member_email;
    }

    public void setNon_member_email(String non_member_email) {
        this.non_member_email = non_member_email;
    }

    public String getOrder_goods_name() {
        return order_goods_name;
    }

    public void setOrder_goods_name(String order_goods_name) {
        this.order_goods_name = order_goods_name;
    }

    public Integer getOrder_goods_cnt() {
        return order_goods_cnt;
    }

    public void setOrder_goods_cnt(Integer order_goods_cnt) {
        this.order_goods_cnt = order_goods_cnt;
    }

    public Integer getTotal_order_price() {
        return total_order_price;
    }

    public void setTotal_order_price(Integer total_order_price) {
        this.total_order_price = total_order_price;
    }

    public Integer getTotal_order_goods() {
        return total_order_goods;
    }

    public void setTotal_order_goods(Integer total_order_goods) {
        this.total_order_goods = total_order_goods;
    }

    public Integer getTotal_discount_price() {
        return total_discount_price;
    }

    public void setTotal_discount_price(Integer total_discount_price) {
        this.total_discount_price = total_discount_price;
    }

    public String getMailing_yn() {
        return mailing_yn;
    }

    public void setMailing_yn(String mailing_yn) {
        this.mailing_yn = mailing_yn;
    }

    public Integer getOrder_method() {
        return order_method;
    }

    public void setOrder_method(Integer order_method) {
        this.order_method = order_method;
    }


    public String getOrder_method_name() {
        if(this.order_method == null){
            return "";
        }
        OrderMethodType orderMethodType = OrderMethodType.get(order_method);
        return orderMethodType == null ? "" : orderMethodType.getName();
    }

    public void setOrder_method_name(String order_method_name) {
        this.order_method_name = order_method_name;
    }

    public Date getPayment_date() {
        return payment_date;
    }

    public void setPayment_date(Date payment_date) {
        this.payment_date = payment_date;
    }

    public String getBank_account() {
        return bank_account;
    }

    public void setBank_account(String bank_account) {
        this.bank_account = bank_account;
    }

    public String getBank_sender() {
        return bank_sender;
    }

    public void setBank_sender(String bank_sender) {
        this.bank_sender = bank_sender;
    }

    public Integer getReceipt_yn() {
        return receipt_yn;
    }

    public void setReceipt_yn(Integer receipt_yn) {
        this.receipt_yn = receipt_yn;
    }

    public Integer getTotal_delivery_charge() {
        return total_delivery_charge;
    }

    public void setTotal_delivery_charge(Integer total_delivery_charge) {
        this.total_delivery_charge = total_delivery_charge;
    }

    public Long getCreater() {
        return creater;
    }

    public void setCreater(Long creater) {
        this.creater = creater;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getOrder_name() {
        return order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getHandle_mode() {
        return handle_mode;
    }

    public void setHandle_mode(String handle_mode) {
        this.handle_mode = handle_mode;
    }

    public String getBilling_status() {
        return billing_status;
    }

    public void setBilling_status(String billing_status) {
        this.billing_status = billing_status;
    }

	public String getDelivery_status() {
		return delivery_status;
	}

	public void setDelivery_status(String delivery_status) {
		this.delivery_status = delivery_status;
	}

    public String getGoods_main_img() {
        return goods_main_img;
    }

    public void setGoods_main_img(String goods_main_img) {
        this.goods_main_img = goods_main_img;
    }

    public Long getGoods_no() {
        return goods_no;
    }

    public void setGoods_no(Long goods_no) {
        this.goods_no = goods_no;
    }

    public List<ShopOrderGoods> getShopOrderGoodsList() {
        return shopOrderGoodsList;
    }

    public void setShopOrderGoodsList(List<ShopOrderGoods> shopOrderGoodsList) {
        this.shopOrderGoodsList = shopOrderGoodsList;
    }
}

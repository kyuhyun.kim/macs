package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang.StringUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class BenefitRequest {

	private Long benefit_id;
	private Integer benefit_type;
	private String req_name;
	private String req_phone_no;
	private Integer period_info;
	private Long estimate_no;

	public Long getBenefit_id() {
		return benefit_id;
	}

	public void setBenefit_id(Long benefit_id) {
		this.benefit_id = benefit_id;
	}

	public Integer getBenefit_type() {
		return benefit_type;
	}

	public void setBenefit_type(Integer benefit_type) {
		this.benefit_type = benefit_type;
	}

	public String getReq_name() {
		return req_name;
	}

	public void setReq_name(String req_name) {
		this.req_name = req_name;
	}

	public String getReq_phone_no() {
		return req_phone_no;
	}

	public void setReq_phone_no(String req_phone_no) {
		this.req_phone_no = req_phone_no;
	}

	public Integer getPeriod_info() {
		return period_info;
	}

	public void setPeriod_info(Integer period_info) {
		this.period_info = period_info;
	}

	public Long getEstimate_no() {
		return estimate_no;
	}

	public void setEstimate_no(Long estimate_no) {
		this.estimate_no = estimate_no;
	}

	@JsonIgnore
	public boolean isVallid() {
		if (benefit_type == null) return false;

		// 이름 / 전화번호
		if (StringUtils.isEmpty(req_name) || StringUtils.isEmpty(req_phone_no)) return false;

		// 짐보관 서비스인 경우 짐보관 일수
		switch (benefit_type) {
			case 1: {
				if (period_info == null || period_info <= 0) return false;
				break;
			}
			default:
		}

		return true;
	}
}

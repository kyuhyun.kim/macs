package com.zipdoc.acs.domain.entity;

public class ShopZzim {
	private long member_no;
	private long goods_no;
	
	public ShopZzim() {
	}
	
	public ShopZzim(long member_no, long goods_no) {
		this.member_no = member_no;
		this.goods_no = goods_no;
	}
	
	public long getMember_no() {
		return member_no;
	}
	public void setMember_no(long member_no) {
		this.member_no = member_no;
	}

	public long getGoods_no() {
		return goods_no;
	}

	public void setGoods_no(long goods_no) {
		this.goods_no = goods_no;
	}
}

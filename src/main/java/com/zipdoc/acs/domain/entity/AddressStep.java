package com.zipdoc.acs.domain.entity;

/**
 * Created by dklee on 2016-06-09.
 */
public class AddressStep {
    private String sido;
    private String sigungu;

    public String getSido() {
        return sido;
    }

    public void setSido(String sido) {
        this.sido = sido;
    }

    public String getSigungu() {
        return sigungu;
    }

    public void setSigungu(String sigungu) {
        this.sigungu = sigungu;
    }
}

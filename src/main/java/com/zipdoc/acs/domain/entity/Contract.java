package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang.StringUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Contract {
	private Long contract_no;
	private String contract_day;
	private Partner contract_partner = new Partner();
	private String start_day;
	private String end_day;
	private String expect_end_day;
	private String warranty_day;
	private Integer agreement_status;
	private Long contract_price;
	private Long package_price;
	private Long contract_date;
	private String memo;

	@JsonIgnore private Long estimate_no;
	@JsonIgnore private String contracter;
	@JsonIgnore private String phone_no;
	@JsonIgnore private String password;
	@JsonIgnore private Integer partner_id;
	@JsonIgnore private String address;
	@JsonIgnore private String address_detail;
	@JsonIgnore private Integer completed_flag;

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Long getContract_price() {
		return contract_price;
	}

	public void setContract_price(Long contract_price) {
		this.contract_price = contract_price;
	}

	public Long getPackage_price() {
		return package_price;
	}

	public void setPackage_price(Long package_price) {
		this.package_price = package_price;
	}

	public Long getContract_date() {
		return contract_date;
	}

	public void setContract_date(Long contract_date) {
		this.contract_date = contract_date;
	}

	public String getContract_day() {
		return contract_day;
	}

	public void setContract_day(String contract_day) {
		this.contract_day = contract_day;
	}

	public Long getContract_no() {
		return contract_no;
	}

	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}

	public Partner getContract_partner() {
		return contract_partner;
	}

	public void setContract_partner(Partner contract_partner) {
		this.contract_partner = contract_partner;
	}

	public String getEnd_day() {
		return end_day;
	}

	public void setEnd_day(String end_day) {
		this.end_day = end_day;
	}

	public String getExpect_end_day() {
		return expect_end_day;
	}

	public void setExpect_end_day(String expect_end_day) {
		this.expect_end_day = expect_end_day;
	}

	public String getStart_day() {
		return start_day;
	}

	public void setStart_day(String start_day) {
		this.start_day = start_day;
	}

	public String getWarranty_day() {
		return warranty_day;
	}

	public void setWarranty_day(String warranty_day) {
		this.warranty_day = warranty_day;
	}

	public void setContract_partner_id(Integer partner_id) {
		this.contract_partner.setPartner_id(partner_id);
	}

	public void setContract_partner_name(String partner_name) {
		this.contract_partner.setPartner_name(partner_name);
	}

	public Integer getAgreement_status() {
		return agreement_status;
	}

	public void setAgreement_status(Integer agreement_status) {
		this.agreement_status = agreement_status;
	}

	public Long getEstimate_no() {
		return estimate_no;
	}

	public void setEstimate_no(Long estimate_no) {
		this.estimate_no = estimate_no;
	}

	public String getContracter() {
		return contracter;
	}

	public void setContracter(String contracter) {
		this.contracter = contracter;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}

	public String getAddress() {
		if (StringUtils.isEmpty(address)) return "";
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress_detail() {
		if (StringUtils.isEmpty(address_detail)) return "";
		return address_detail;
	}

	public void setAddress_detail(String address_detail) {
		this.address_detail = address_detail;
	}

	public Integer getCompleted_flag() {
		return completed_flag;
	}

	public void setCompleted_flag(Integer completed_flag) {
		this.completed_flag = completed_flag;
	}
}

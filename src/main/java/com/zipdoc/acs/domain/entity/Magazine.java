package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.model.ListResponse;
import com.zipdoc.acs.utils.BitUtils;
import org.apache.commons.lang.StringUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Magazine {

	private int cno;
	private Integer magazine_type;
	private String title;
	private String subtitle;
	private String summary;
	private String contents;

	private Integer next_cno;
	private Integer previous_cno;

	@JsonIgnore private Integer file_location;
	@JsonIgnore private String file_path;
	@JsonIgnore private String thumbnail;



	private ListResponse list;

	public ListResponse getList() {
		return list;
	}

	public void setList(ListResponse list) {
		this.list = list;
	}

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public Integer getMagazine_type() {
		return magazine_type;
	}

	public void setMagazine_type(Integer magazine_type) {
		this.magazine_type = magazine_type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public Integer getFile_location() {
		return file_location;
	}

	public void setFile_location(Integer file_location) {
		this.file_location = file_location;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getThumbnail_url() {
		if (StringUtils.isEmpty(this.thumbnail) || StringUtils.isEmpty(this.file_path)) return null;
		String image_url = BitUtils.getBit(this.file_location, 0) ? Constants.STATIC_URL_Q : Constants.STATIC_URL_C;
		return image_url + this.file_path + this.thumbnail;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Integer getNext_cno() {
		return next_cno;
	}

	public void setNext_cno(Integer next_cno) {
		this.next_cno = next_cno;
	}

	public Integer getPrevious_cno() {
		return previous_cno;
	}

	public void setPrevious_cno(Integer previous_cno) {
		this.previous_cno = previous_cno;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}
}

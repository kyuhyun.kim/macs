package com.zipdoc.acs.domain.entity;

public class CartPrice {
	private int totalGoodsPrice;
	private int totalDiscountPrice;
	private int totalDeliveryPrice;
	private int totalGoodsPayment;
	
	public int getTotalGoodsPrice() {
		return totalGoodsPrice;
	}
	public void setTotalGoodsPrice(int totalGoodsPrice) {
		this.totalGoodsPrice = totalGoodsPrice;
	}
	public int getTotalDiscountPrice() {
		return totalDiscountPrice;
	}
	public void setTotalDiscountPrice(int totalDiscountPrice) {
		this.totalDiscountPrice = totalDiscountPrice;
	}
	public int getTotalDeliveryPrice() {
		return totalDeliveryPrice;
	}
	public void setTotalDeliveryPrice(int totalDeliveryPrice) {
		this.totalDeliveryPrice = totalDeliveryPrice;
	}
	public int getTotalGoodsPayment() {
		return totalGoodsPayment;
	}
	public void setTotalGoodsPayment(int totalGoodsPayment) {
		this.totalGoodsPayment = totalGoodsPayment;
	}
}

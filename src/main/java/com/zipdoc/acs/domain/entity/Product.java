package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.PictureType;
import com.zipdoc.acs.define.ProductType;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Product {
	private Long pid;
	private String subject;
	private Integer space;
	private String address;
	private String image_url;
	private String thumbnail_url;
	private String after_img_url;
	private String after_thumbnail_url;
	private String before_img_url;
	private String before_thumbnail_url;
	private String category_code1;
	private String category_code2;
	private String category_name1;
	private String category_name2;
	private Integer zzim_cnt;
	private Integer zzim_flag; //디폴트 찜안함.
	private Integer like_cnt;
	private Integer like_flag;
	private Integer share_cnt;
	private Integer hit_cnt;

	private String method_material;
	private Integer price;
	private Integer eprice_min;
	private Integer eprice_max;
	private Integer eperiod;
	private Integer eperiod_unit;
	private Integer partner_id;
	private String partner_name;
	private String partner_main_part;
	private Integer partner_kind;
	@JsonIgnore private String partner_logo;
	private String partner_logo_url;
	private Integer product_type;
	private List<String> tag_list;
	private String style;
	@JsonProperty("start_date") private Long start_timestamp;
	@JsonProperty("end_date") private Long end_timestamp;

	private String intro_msg;
	private String share_url;
	private String copywriter;
	private String youtube_url;
	private Long creater;
	@JsonIgnore private Date create_date;
	private Long updater;
	@JsonProperty("create_date") private Long create_timestamp;

	private Profile profile;
	private List<Item> items;

	@JsonIgnore private Date start_date;
	@JsonIgnore private Date end_date;
	@JsonIgnore private String file_path;
	@JsonIgnore private String after_file_path;
	@JsonIgnore private String file_name;
	@JsonIgnore private String after_file_name;
	@JsonIgnore private String file_name_org;
	@JsonIgnore private String thumbnail;
	@JsonIgnore private String after_thumbnail;
	@JsonIgnore private String before_file_name;
	@JsonIgnore private String before_thumbnail;
	@JsonIgnore private Integer file_location = 0;
	@JsonIgnore private Integer before_file_location = 0;
	@JsonIgnore private Integer after_file_location = 0;
	@JsonIgnore private int best_flag;
	@JsonIgnore private String tags;

	private List<Long> article_no_list;

	//커뮤니티에서 파일 업로드시 사용
	private String form_data_name;
	private MultipartFile multipartFile;

	private String ending_msg;

	private List<Product> same_price_list;

	private Integer before_count;

	public String getAfter_img_url() {
		return after_img_url;
	}

	public void setAfter_img_url(String after_img_url) {
		this.after_img_url = after_img_url;
	}

	public String getAfter_thumbnail_url() {
		return after_thumbnail_url;
	}

	public void setAfter_thumbnail_url(String after_thumbnail_url) {
		this.after_thumbnail_url = after_thumbnail_url;
	}

	public String getAfter_file_path() {
		return after_file_path;
	}

	public void setAfter_file_path(String after_file_path) {
		this.after_file_path = after_file_path;
	}

	public String getAfter_file_name() {
		return after_file_name;
	}

	public void setAfter_file_name(String after_file_name) {
		this.after_file_name = after_file_name;
	}

	public String getAfter_thumbnail() {
		return after_thumbnail;
	}

	public void setAfter_thumbnail(String after_thumbnail) {
		this.after_thumbnail = after_thumbnail;
	}

	public Integer getAfter_file_location() {
		return after_file_location;
	}

	public void setAfter_file_location(Integer after_file_location) {
		this.after_file_location = after_file_location;
	}

	public Integer getBefore_file_location() {
		return before_file_location;
	}

	public void setBefore_file_location(Integer before_file_location) {
		this.before_file_location = before_file_location;
	}

	public Integer getBefore_count() {
		return before_count;
	}

	public void setBefore_count(Integer before_count) {
		this.before_count = before_count;
	}

	public Integer getFile_location() {
		return file_location;
	}

	public void setFile_location(Integer file_location) {
		this.file_location = file_location;
	}

	public String getBefore_thumbnail_url() {
		return before_thumbnail_url;
	}

	public void setBefore_thumbnail_url(String before_thumbnail_url) {
		this.before_thumbnail_url = before_thumbnail_url;
	}

	public String getBefore_thumbnail() {
		return before_thumbnail;
	}

	public void setBefore_thumbnail(String before_thumbnail) {
		this.before_thumbnail = before_thumbnail;
	}

	public String getBefore_file_name() {
		return before_file_name;
	}

	public void setBefore_file_name(String before_file_name) {
		this.before_file_name = before_file_name;
	}

	public String getBefore_img_url() {
		return before_img_url;
	}

	public void setBefore_img_url(String before_img_url) {
		this.before_img_url = before_img_url;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getThumbnail_url() {
		return thumbnail_url;
	}

	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}

	public String getCategory_code1() {
		return category_code1;
	}

	public void setCategory_code1(String category_code1) {
		this.category_code1 = category_code1;
	}

	public String getCategory_code2() {
		return category_code2;
	}

	public void setCategory_code2(String category_code2) {
		this.category_code2 = category_code2;
	}

	public Integer getZzim_cnt() {
		return zzim_cnt;
	}

	public void setZzim_cnt(Integer zzim_cnt) {
		this.zzim_cnt = zzim_cnt;
	}

	public Integer getZzim_flag() {
		return zzim_flag;
	}

	public void setZzim_flag(Integer zzim_flag) {
		this.zzim_flag = zzim_flag;
	}

	public Integer getHit_cnt() {
		return hit_cnt;
	}

	public void setHit_cnt(Integer hit_cnt) {
		this.hit_cnt = hit_cnt;
	}

	public Integer getLike_cnt() {
		return like_cnt;
	}

	public void setLike_cnt(Integer like_cnt) {
		this.like_cnt = like_cnt;
	}

	public Integer getLike_flag() {
		return like_flag;
	}

	public void setLike_flag(Integer like_flag) {
		this.like_flag = like_flag;
	}

	public Integer getShare_cnt() {
		return share_cnt;
	}

	public void setShare_cnt(Integer share_cnt) {
		this.share_cnt = share_cnt;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_name_org() {
		return file_name_org;
	}

	public void setFile_name_org(String file_name_org) {
		this.file_name_org = file_name_org;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public int getBest_flag() {
		return best_flag;
	}

	public void setBest_flag(int best_flag) {
		this.best_flag = best_flag;
	}

	public List<String> getTag_list() {
		return tag_list;
	}

	public void setTag_list(List<String> tag_list) {
		this.tag_list = tag_list;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
		//Tag를 Array로 변환한다.
		if(StringUtils.isNotEmpty(tags) && StringUtils.length(tags) > 0){
			this.tag_list = new ArrayList<String>(Arrays.asList(StringUtils.split(tags, Constants.TAG_DELIMITER)));
		}
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;

		if(start_date != null){
			this.start_timestamp = start_date.getTime();
		}
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
		if(end_date != null){
			this.end_timestamp = end_date.getTime();
		}
	}

	public String getMethod_material() {
		return method_material;
	}

	public void setMethod_material(String method_material) {
		this.method_material = method_material;
	}

	public Integer getEprice_min() {
		return eprice_min;
	}

	public void setEprice_min(Integer eprice_min) {
		this.eprice_min = eprice_min;
	}

	public Integer getEprice_max() {
		return eprice_max;
	}

	public void setEprice_max(Integer eprice_max) {
		this.eprice_max = eprice_max;
	}

	public Integer getEperiod() {
		return eperiod;
	}

	public void setEperiod(Integer eperiod) {
		this.eperiod = eperiod;
	}

	public Integer getEperiod_unit() {
		return eperiod_unit;
	}

	public void setEperiod_unit(Integer eperiod_unit) {
		this.eperiod_unit = eperiod_unit;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Long getEnd_timestamp() {
		return end_timestamp;
	}

	public void setEnd_timestamp(Long end_timestamp) {
		this.end_timestamp = end_timestamp;
	}

	public Long getStart_timestamp() {
		return start_timestamp;
	}

	public void setStart_timestamp(Long start_timestamp) {
		this.start_timestamp = start_timestamp;
	}

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}

	public String getIntro_msg() {
		return intro_msg;
	}

	public void setIntro_msg(String intro_msg) {
		this.intro_msg = intro_msg;
	}

	public String getShare_url() {
		return share_url;
	}

	public void setShare_url(String share_url) {
		this.share_url = share_url;
	}

	public String getCopywriter() {
		return copywriter;
	}

	public void setCopywriter(String copywriter) {
		this.copywriter = copywriter;
	}

	public String getYoutube_url() {
		return youtube_url;
	}

	public void setYoutube_url(String youtube_url) {
		this.youtube_url = youtube_url;
	}

	public Integer getProduct_type() {
		return product_type;
	}

	public void setProduct_type(Integer product_type) {
		this.product_type = product_type;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
		if(create_date != null){
			this.create_timestamp = create_date.getTime();
		}
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public void setSpace(Integer space) {
		this.space = space;
	}

	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}

	public Long getCreate_timestamp() {
		return create_timestamp;
	}

	public void setCreate_timestamp(Long create_timestamp) {
		this.create_timestamp = create_timestamp;
	}

	public Integer getSpace() {
		return space;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getForm_data_name() {
		return form_data_name;
	}

	public void setForm_data_name(String form_data_name) {
		this.form_data_name = form_data_name;
	}

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<Long> getArticle_no_list() {
		return article_no_list;
	}

	public void setArticle_no_list(List<Long> article_no_list) {
		this.article_no_list = article_no_list;
	}

	@JsonIgnore
	public boolean isPictureChanged(){
		return multipartFile!=null && !multipartFile.isEmpty();
	}

	/**
	 * 이미지 URL을 생성한다.
	 * @param imageType 이미지크기
	 * @param thumbnailType 썸네일크기
     */
	public void buildPictureUrl(PictureType imageType, PictureType thumbnailType){

		String DomainURL = this.file_location == 0 ? Constants.STATIC_URL_C : Constants.STATIC_URL_Q;
		String BeforeDomainURL = this.before_file_location == 0 ? Constants.STATIC_URL_C : Constants.STATIC_URL_Q;
		String AfterDomainURL = this.after_file_location == 0 ? Constants.STATIC_URL_C : Constants.STATIC_URL_Q;

		if (StringUtils.isNotEmpty(this.file_name)) {
			this.image_url = DomainURL + getFile_path() + getFile_name(); //원본이미지
			this.thumbnail_url = DomainURL + getFile_path() + thumbnailType.getPrefix() + getFile_name();

			if(StringUtils.isNotEmpty(this.before_file_name))
				this.before_img_url = BeforeDomainURL + getFile_path() + getBefore_file_name(); //비포 이미지

			if(StringUtils.isNotEmpty(this.before_thumbnail))
				this.before_thumbnail_url= BeforeDomainURL + getFile_path() + thumbnailType.getPrefix() + getBefore_file_name(); //비포 이미지
		}

		if(StringUtils.isNotEmpty(this.before_file_name))
			this.before_img_url = BeforeDomainURL + getAfter_file_path() + getBefore_file_name(); //비포 이미지

		if(StringUtils.isNotEmpty(this.before_thumbnail))
			this.before_thumbnail_url= BeforeDomainURL + getAfter_file_path() + thumbnailType.getPrefix() + getBefore_file_name(); //비포 이미지

		if(StringUtils.isNotEmpty(this.after_file_name))
			this.after_img_url = AfterDomainURL + getAfter_file_path() + getAfter_file_name();
		if(StringUtils.isNotEmpty(this.after_thumbnail))
			this.after_thumbnail_url = AfterDomainURL + getAfter_file_path() + thumbnailType.getPrefix()+  getAfter_file_name();

		// 파트너 로고 URL
		if (StringUtils.isNotEmpty(this.partner_logo)) {
			this.partner_logo_url = DomainURL + this.partner_logo;
		}

		if(items != null && items.size() > 0){
			for(Item item:items){
				item.buildPictureUrl(imageType, thumbnailType);
			}
		}
	}

	public void buildShareUrl(){
		if(this.product_type!=null){
			if(this.product_type.intValue() == ProductType.GALLERY.getCode()){
				this.share_url = String.format(Constants.PRODDUCT_SHARE_URL, getPid());
			} else if(this.product_type.intValue() == ProductType.COMMUNITY.getCode()){
				this.share_url = String.format(Constants.COMMUNITY_SHARE_URL, getPid());
			}
		}
	}

	public String getEnding_msg() {
		return ending_msg;
	}

	public void setEnding_msg(String ending_msg) {
		this.ending_msg = ending_msg;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getPartner_main_part() {
		return partner_main_part;
	}

	public void setPartner_main_part(String partner_main_part) {
		this.partner_main_part = partner_main_part;
	}

	public Integer getPartner_kind() {
		return partner_kind;
	}

	public void setPartner_kind(Integer partner_kind) {
		this.partner_kind = partner_kind;
	}

	public String getPartner_logo() {
		return partner_logo;
	}

	public void setPartner_logo(String partner_logo) {
		this.partner_logo = partner_logo;
	}

	public String getPartner_logo_url() {
		return partner_logo_url;
	}

	public List<Product> getSame_price_list() {
		return same_price_list;
	}

	public void setSame_price_list(List<Product> same_price_list) {
		this.same_price_list = same_price_list;
	}

	public String getCategory_name1() {
		return category_name1;
	}

	public void setCategory_name1(String category_name1) {
		this.category_name1 = category_name1;
	}

	public String getCategory_name2() {
		return category_name2;
	}

	public void setCategory_name2(String category_name2) {
		this.category_name2 = category_name2;
	}
}

package com.zipdoc.acs.domain.entity;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InteriorCaseDetail {
	
	private @JsonIgnore String file_url_base = "http://zipdoc.co.kr/";
	
	private @JsonProperty("fid") int wr_id;
	
	// 시공 업체
	private @JsonProperty("company") String wr_5;

	// 시공 평수
	private @JsonProperty("space") String wr_3;
	
	// 시공 주소
	private @JsonProperty("address") String wr_4;
	
	// 시공명
	private @JsonProperty("subject") String wr_2;		
	
	/**
	 * 	시공분류 (주거공간, 상업공간, 부분공사)
	 */
	private @JsonProperty("category-1") String category;
	
	/**
	 * 시공상세분류: 시공분류에 따른 하위 구분값
	*/
	private @JsonProperty("category-2") String ca_name;
	
	/**
	 * 게시판 HTML EDITOR 정보
	 */
	private @JsonIgnore String wr_content;
	
	// 시공 사진 리스트
	private @JsonProperty("urls") List<String> urls;

	public int getWr_id() {
		return wr_id;
	}

	public void setWr_id(int wr_id) {
		this.wr_id = wr_id;
	}

	public String getWr_5() {
		return wr_5;
	}

	public void setWr_5(String wr_5) {
		this.wr_5 = wr_5;
	}

	public String getWr_3() {
		return wr_3;
	}

	public void setWr_3(String wr_3) {
		this.wr_3 = wr_3;
	}

	public String getWr_4() {
		return wr_4;
	}

	public void setWr_4(String wr_4) {
		this.wr_4 = wr_4;
	}

	public String getWr_2() {
		return wr_2;
	}

	public void setWr_2(String wr_2) {
		this.wr_2 = wr_2;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCa_name() {
		return ca_name;
	}

	public void setCa_name(String ca_name) {
		this.ca_name = ca_name;
	}

	public List<String> getUrls() {
		return urls;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}
	
	public void addUrl(String url) {
		if (urls == null) {
			urls = new LinkedList<String>();
		}
		urls.add(file_url_base+url);
	}

	public String getWr_content() {
		return wr_content;
	}

	public void setWr_content(String wr_content) {
		this.wr_content = wr_content;
	}
}

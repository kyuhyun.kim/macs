package com.zipdoc.acs.domain.entity;

public class ShopZzimInfo {
	private Long goods_no;
	//상품이름
	private String goods_name;
	//상품가격
	private int goods_price;
	//할인율
	private Long goods_discount;
	//최종 할인가
	private Integer goods_discount_price;
	//할인여부
	private Integer goods_discount_status;
	//대표이미지
	private String goods_main_img;
	
	
	public Long getGoods_no() {
		return goods_no;
	}
	public void setGoods_no(Long goods_no) {
		this.goods_no = goods_no;
	}
	public String getGoods_name() {
		return goods_name;
	}
	public void setGoods_name(String goods_name) {
		this.goods_name = goods_name;
	}
	public int getGoods_price() {
		return goods_price;
	}
	public void setGoods_price(int goods_price) {
		this.goods_price = goods_price;
	}
	public Long getGoods_discount() {
		return goods_discount;
	}
	public void setGoods_discount(Long goods_discount) {
		this.goods_discount = goods_discount;
	}
	public Integer getGoods_discount_price() {
		return goods_discount_price;
	}
	public void setGoods_discount_price(Integer goods_discount_price) {
		this.goods_discount_price = goods_discount_price;
	}
	public Integer getGoods_discount_status() {
		return goods_discount_status;
	}
	public void setGoods_discount_status(Integer goods_discount_status) {
		this.goods_discount_status = goods_discount_status;
	}
	public String getGoods_main_img() {
		return goods_main_img;
	}
	public void setGoods_main_img(String goods_main_img) {
		this.goods_main_img = goods_main_img;
	}
}

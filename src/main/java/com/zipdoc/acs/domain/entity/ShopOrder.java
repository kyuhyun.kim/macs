package com.zipdoc.acs.domain.entity;

import java.util.Date;

/**
 * Created by dskim on 2017. 5. 11..
 */
public class ShopOrder {

    //일련번호
    private long sno;

    //주문번호
    private String order_no;

    //주문유형
    private Integer order_type;

    //비회원 이메일
    private String non_member_email;

    //주문 상품명
    private String order_goods_name;

    //주문 상품 갯수
    private Integer order_goods_cnt;

    //총 할인 금액
    private Integer total_discount_price;

    //총 주문 금액
    private Integer total_order_price;

    //총 상품 금액
    private Integer total_order_goods;

    //시공비
    private Integer construction_price;

    //메일/SMS 전송 여부
    private Integer mailing_yn;

    //주문 방법
    private Integer order_method;

    //결제상태
    private Integer payment_status;

    //입금받을 가상계좌 입금은행
    private String bank_account;

    //입금바다을 가상계좌 예금주
    private String bank_sender;

    //입금받을 가상계좌 번호
    private String bank_number;

    //입금받을 가상계좌 마감기한
    private Date bank_date;

    private String order_method_name;

    //입금 일자
    private Date payment_date;

    //영수증 신청여부
    private Integer receipt_yn;

    //총 배송비
    private Integer total_delivery_charge;

    //등록일
    private Date create_date;

    //수정일
    private Date update_date;

    //등록자
    private Long creater;

    //수정자
    private Long updater;

    //상품정보
    private Long goods_no;
    private String goods_main_img;

    //장바구니 결제
    private boolean cart_order = true;


    public long getSno() {
        return sno;
    }

    public void setSno(long sno) {
        this.sno = sno;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public Integer getOrder_type() {
        return order_type;
    }

    public void setOrder_type(Integer order_type) {
        this.order_type = order_type;
    }

    public String getNon_member_email() {
        return non_member_email;
    }

    public void setNon_member_email(String non_member_email) {
        this.non_member_email = non_member_email;
    }

    public String getOrder_goods_name() {
        return order_goods_name;
    }

    public void setOrder_goods_name(String order_goods_name) {
        this.order_goods_name = order_goods_name;
    }

    public Integer getOrder_goods_cnt() {
        return order_goods_cnt;
    }

    public void setOrder_goods_cnt(Integer order_goods_cnt) {
        this.order_goods_cnt = order_goods_cnt;
    }

    public Integer getTotal_discount_price() {
        return total_discount_price;
    }

    public void setTotal_discount_price(Integer total_discount_price) {
        this.total_discount_price = total_discount_price;
    }

    public Integer getTotal_order_price() {
        return total_order_price;
    }

    public void setTotal_order_price(Integer total_order_price) {
        this.total_order_price = total_order_price;
    }

    public Integer getTotal_order_goods() {
        return total_order_goods;
    }

    public void setTotal_order_goods(Integer total_order_goods) {
        this.total_order_goods = total_order_goods;
    }

    public Integer getConstruction_price() {
        return construction_price;
    }

    public void setConstruction_price(Integer construction_price) {
        this.construction_price = construction_price;
    }

    public Integer getMailing_yn() {
        return mailing_yn;
    }

    public void setMailing_yn(Integer mailing_yn) {
        this.mailing_yn = mailing_yn;
    }

    public Integer getOrder_method() {
        return order_method;
    }

    public void setOrder_method(Integer order_method) {
        this.order_method = order_method;
    }

    public Integer getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(Integer payment_status) {
        this.payment_status = payment_status;
    }

    public String getBank_account() {
        return bank_account;
    }

    public void setBank_account(String bank_account) {
        this.bank_account = bank_account;
    }

    public String getBank_sender() {
        return bank_sender;
    }

    public void setBank_sender(String bank_sender) {
        this.bank_sender = bank_sender;
    }

    public String getBank_number() {
        return bank_number;
    }

    public void setBank_number(String bank_number) {
        this.bank_number = bank_number;
    }

    public Date getBank_date() {
        return bank_date;
    }

    public void setBank_date(Date bank_date) {
        this.bank_date = bank_date;
    }

    public String getOrder_method_name() {
        return order_method_name;
    }

    public void setOrder_method_name(String order_method_name) {
        this.order_method_name = order_method_name;
    }

    public Date getPayment_date() {
        return payment_date;
    }

    public void setPayment_date(Date payment_date) {
        this.payment_date = payment_date;
    }

    public Integer getReceipt_yn() {
        return receipt_yn;
    }

    public void setReceipt_yn(Integer receipt_yn) {
        this.receipt_yn = receipt_yn;
    }

    public Integer getTotal_delivery_charge() {
        return total_delivery_charge;
    }

    public void setTotal_delivery_charge(Integer total_delivery_charge) {
        this.total_delivery_charge = total_delivery_charge;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Long getCreater() {
        return creater;
    }

    public void setCreater(Long creater) {
        this.creater = creater;
    }

    public Long getUpdater() {
        return updater;
    }

    public void setUpdater(Long updater) {
        this.updater = updater;
    }

    public Long getGoods_no() {
        return goods_no;
    }

    public void setGoods_no(Long goods_no) {
        this.goods_no = goods_no;
    }

    public String getGoods_main_img() {
        return goods_main_img;
    }

    public void setGoods_main_img(String goods_main_img) {
        this.goods_main_img = goods_main_img;
    }

    public boolean isCart_order() {
        return cart_order;
    }

    public void setCart_order(boolean cart_order) {
        this.cart_order = cart_order;
    }
}

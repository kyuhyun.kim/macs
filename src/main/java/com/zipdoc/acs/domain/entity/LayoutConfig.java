package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.define.PictureType;
import org.apache.commons.lang.StringUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LayoutConfig {

    private Integer category;
    private Integer config_type;
    private Integer service_target;
    private String config_name;
    private String display_name;
    private String config_title;
    private Integer file_location = 0;
    private String file_path;
    private String file_name;
    private Integer status;

    private String additional_1;
    private String additional_2;

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getConfig_type() {
        return config_type;
    }

    public void setConfig_type(Integer config_type) {
        this.config_type = config_type;
    }

    public Integer getService_target() {
        return service_target;
    }

    public void setService_target(Integer service_target) {
        this.service_target = service_target;
    }

    public String getConfig_name() {
        return config_name;
    }

    public void setConfig_name(String config_name) {
        this.config_name = config_name;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getDisplayOrConfig_name() {
        return StringUtils.isEmpty(display_name) ? config_name : display_name;
    }

    public String getConfig_title() {
        return config_title;
    }

    public void setConfig_title(String config_title) {
        this.config_title = config_title;
    }

    public Integer getFile_location() {
        return file_location;
    }

    public void setFile_location(Integer file_location) {
        this.file_location = file_location;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getImage_url() {
        if (StringUtils.isEmpty(this.file_name)) return null;
        String image_url = this.file_location == 0 ? Constants.STATIC_URL_C : Constants.STATIC_URL_Q;
        return image_url + getFile_path() + getFile_name();
    }

    public String getAdditional_1() {
        return additional_1;
    }

    public void setAdditional_1(String additional_1) {
        this.additional_1 = additional_1;
    }

    public String getAdditional_2() {
        return additional_2;
    }

    public void setAdditional_2(String additional_2) {
        this.additional_2 = additional_2;
    }
}

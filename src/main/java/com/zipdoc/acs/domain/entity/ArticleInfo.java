package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

/**
 * Created by ZIPDOC on 2016-06-21.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArticleInfo {
    private Long interior_tip;
    private Long event;
    private Long news;

    @JsonIgnore
    private Date interior_tip_create_date;
    @JsonIgnore
    private Date event_create_date;
    @JsonIgnore
    private Date news_create_date;

    public Long getEvent() {
        return event;
    }

    public void setEvent(Long event) {
        this.event = event;
    }

    public Date getEvent_create_date() {
        return event_create_date;
    }

    public void setEvent_create_date(Date event_create_date) {
        this.event_create_date = event_create_date;
        if(event_create_date != null){
            event = event_create_date.getTime();
        }
    }

    public Long getInterior_tip() {
        return interior_tip;
    }

    public void setInterior_tip(Long interior_tip) {
        this.interior_tip = interior_tip;
    }

    public Date getInterior_tip_create_date() {
        return interior_tip_create_date;
    }

    public void setInterior_tip_create_date(Date interior_tip_create_date) {
        this.interior_tip_create_date = interior_tip_create_date;

        if(interior_tip_create_date != null){
            interior_tip = interior_tip_create_date.getTime();
        }
    }

    public Long getNews() {
        return news;
    }

    public void setNews(Long news) {
        this.news = news;
    }

    public Date getNews_create_date() {
        return news_create_date;
    }

    public void setNews_create_date(Date news_create_date) {
        this.news_create_date = news_create_date;

        if(news_create_date != null){
            news = news_create_date.getTime();
        }
    }
}

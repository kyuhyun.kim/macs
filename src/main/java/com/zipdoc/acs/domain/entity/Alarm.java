package com.zipdoc.acs.domain.entity;


import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by KKH on 2018-11-07.
 */



@JsonInclude(JsonInclude.Include.NON_NULL)
public class Alarm {
   private AlarmInfo alarm_info;


    public AlarmInfo getAlarm_info() {
        return alarm_info;
    }

    public void setAlarm_info(AlarmInfo alarm_info) {
        this.alarm_info = alarm_info;
    }
}

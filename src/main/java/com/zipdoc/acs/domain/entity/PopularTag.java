package com.zipdoc.acs.domain.entity;

/**
 * 인기태그 정보
 */
public class PopularTag {

    private String tag;
    private String url;

    public PopularTag() {

    }

    public PopularTag(String tag, String url) {
        this.tag = tag;
        this.url = url;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

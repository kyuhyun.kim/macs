package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InteriorCategory {

	private @JsonProperty("category-id") String category_id;
	private @JsonProperty("description") String description;
	private @JsonProperty("image-url") String image_url;
	private @JsonProperty("etag") String etag;
	private @JsonProperty("item-count") int count;
	
	private @JsonIgnore String text_1;
	private @JsonIgnore String text_2;
	private @JsonIgnore String reg_date;
	private @JsonIgnore String mod_date;
	
	public String getCategory_id() {
		return category_id;
	}
	
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getImage_url() {
		return image_url;
	}
	
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	
	public String getText_1() {
		return text_1;
	}
	
	public void setText_1(String text_1) {
		this.text_1 = text_1;
	}
	
	public String getText_2() {
		return text_2;
	}
	
	public void setText_2(String text_2) {
		this.text_2 = text_2;
	}
	
	public String getReg_date() {
		return reg_date;
	}
	
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
	public String getMod_date() {
		return mod_date;
	}
	
	public void setMod_date(String mod_date) {
		this.mod_date = mod_date;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getEtag() {
		return etag;
	}

	public void setEtag(String etag) {
		this.etag = etag;
	}
}

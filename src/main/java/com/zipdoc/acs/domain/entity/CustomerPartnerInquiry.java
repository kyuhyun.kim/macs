package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang.StringUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class CustomerPartnerInquiry {

	private Long cno;

	private String partner_name;
	private String mobile_no;
	private String email;
	private String contents;

	private String region1;
	private String region2;

	private Integer agreement_marketing;

	private Long member_no;
	private String ip;

	public Long getCno() {
		return cno;
	}

	public void setCno(Long cno) {
		this.cno = cno;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getRegion1() {
		return region1;
	}

	public void setRegion1(String region1) {
		this.region1 = region1;
	}

	public String getRegion2() {
		return region2;
	}

	public void setRegion2(String region2) {
		this.region2 = region2;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getAgreement_marketing() {
		return agreement_marketing == null ? 0 : agreement_marketing;
	}

	public void setAgreement_marketing(Integer agreement_marketing) {
		this.agreement_marketing = agreement_marketing;
	}

	public Long getMember_no() {
		return member_no;
	}

	public void setMember_no(Long member_no) {
		this.member_no = member_no;
	}

	public boolean validation() {
		return StringUtils.isNotEmpty(partner_name) && StringUtils.isNotEmpty(mobile_no) && StringUtils.isNotEmpty(email)
				&& StringUtils.isNotEmpty(region1) && StringUtils.isNotEmpty(region2);
	}
}

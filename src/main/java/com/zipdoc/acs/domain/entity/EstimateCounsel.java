package com.zipdoc.acs.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zipdoc.acs.define.EstimateCounselStatus;

/**
 * 견적 상담 내역
 */
public class EstimateCounsel {

    private Long create_date;

    @JsonIgnore
    private int status;
    private String contents;
    private String creater_name;

    public Long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Long create_date) {
        this.create_date = create_date;
    }

    public String getStatus_name() {
        return EstimateCounselStatus.get(status).getName();
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getCreater_name() {
        return creater_name;
    }

    public void setCreater_name(String creater_name) {
        this.creater_name = creater_name;
    }
}

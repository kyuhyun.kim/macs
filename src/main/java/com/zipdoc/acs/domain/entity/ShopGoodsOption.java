package com.zipdoc.acs.domain.entity;

import java.util.Date;

/**
 *
 * 상품 옵션
 *
 * Created by dskim on 2017. 2. 3..
 */
public class ShopGoodsOption {


	//일련번호 -> BIGINT 로 정의해서
	private long sno;

	//상품번호 -> UNSIGNED 로 테이블 컬럼 필드 정의해서
	private long goods_no;

	//옵션코드
	private String option_code;

	//옵션명
	private String option_value;

	//옵션코드명칭
	private String option_code_name;

	//옵션 concat value
	private String option_concat_value;

	//옵션가격
	private Integer option_price;

	//옵션노출여부
	private Integer option_status;

	//옵션판매여부
	private Integer option_sell_status;
	
	//상품이미지
	private String goods_main_img;
	
	//상품이미지
	private String goods_name;

	//옵션순서
	private int option_seq;

	//재고량
	private int stock_cnt;

	//등록일
	private Date create_date;

	//수정일
	private Date update_date;

	//등록자
	private Long creater;

	//수정자
	private Long updater;

	//할인율
	private Long option_discount;

	//할인시작일
	private Date option_discount_start_date;

	//할인종료일
	private Date option_discount_end_date;

	//할인여부 1:할인안함 , 2:할인함
	private Integer option_discount_status;

	//할인금액
	private long option_discount_purchase;

	//할인가
	private int option_discount_price;


	public long getSno() {
		return sno;
	}

	public void setSno(long sno) {
		this.sno = sno;
	}

	public long getGoods_no() {
		return goods_no;
	}

	public void setGoods_no(long goods_no) {
		this.goods_no = goods_no;
	}

	public String getOption_code() {
		return option_code;
	}

	public void setOption_code(String option_code) {
		this.option_code = option_code;
	}

	public String getOption_value() {
		return option_value;
	}

	public void setOption_value(String option_value) {
		this.option_value = option_value;
	}

	public String getOption_code_name() {
		return option_code_name;
	}

	public void setOption_code_name(String option_code_name) {
		this.option_code_name = option_code_name;
	}

	public String getOption_concat_value() {
		return option_concat_value;
	}

	public void setOption_concat_value(String option_concat_value) {
		this.option_concat_value = option_concat_value;
	}

	public Integer getOption_price() {
		return option_price;
	}

	public void setOption_price(Integer option_price) {
		this.option_price = option_price;
	}

	public Integer getOption_status() {
		return option_status;
	}

	public void setOption_status(Integer option_status) {
		this.option_status = option_status;
	}

	public Integer getOption_sell_status() {
		return option_sell_status;
	}

	public void setOption_sell_status(Integer option_sell_status) {
		this.option_sell_status = option_sell_status;
	}

	public String getGoods_main_img() {
		return goods_main_img;
	}

	public void setGoods_main_img(String goods_main_img) {
		this.goods_main_img = goods_main_img;
	}

	public String getGoods_name() {
		return goods_name;
	}

	public void setGoods_name(String goods_name) {
		this.goods_name = goods_name;
	}

	public int getOption_seq() {
		return option_seq;
	}

	public void setOption_seq(int option_seq) {
		this.option_seq = option_seq;
	}

	public int getStock_cnt() {
		return stock_cnt;
	}

	public void setStock_cnt(int stock_cnt) {
		this.stock_cnt = stock_cnt;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}

	public Long getOption_discount() {
		return option_discount;
	}

	public void setOption_discount(Long option_discount) {
		this.option_discount = option_discount;
	}

	public Date getOption_discount_start_date() {
		return option_discount_start_date;
	}

	public void setOption_discount_start_date(Date option_discount_start_date) {
		this.option_discount_start_date = option_discount_start_date;
	}

	public Date getOption_discount_end_date() {
		return option_discount_end_date;
	}

	public void setOption_discount_end_date(Date option_discount_end_date) {
		this.option_discount_end_date = option_discount_end_date;
	}

	public Integer getOption_discount_status() {
		return option_discount_status;
	}

	public void setOption_discount_status(Integer option_discount_status) {
		this.option_discount_status = option_discount_status;
	}

	public long getOption_discount_purchase() {
		return option_discount_purchase;
	}

	public void setOption_discount_purchase(long option_discount_purchase) {
		this.option_discount_purchase = option_discount_purchase;
	}

	public int getOption_discount_price() {
		return option_discount_price;
	}

	public void setOption_discount_price(int option_discount_price) {
		this.option_discount_price = option_discount_price;
	}
}

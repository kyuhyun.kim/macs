package com.zipdoc.acs.domain.entity;

import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppVersion {

	private @JsonProperty("app_version") String app_version;
	private @JsonProperty("market_url") String download_url;
	private @JsonProperty("description") String description;
	
	private @JsonIgnore int version_id;
	private @JsonIgnore String os_type;
	private @JsonIgnore String 	app_market;
	private @JsonIgnore String lowest_version;
	private @JsonIgnore String reg_date;
	private @JsonIgnore String mod_date;
	
	public int getVersion_id() {
		return version_id;
	}
	
	public void setVersion_id(int version_id) {
		this.version_id = version_id;
	}
	
	public String getOs_type() {
		return os_type;
	}
	
	public void setOs_type(String os_type) {
		this.os_type = os_type;
	}

	public String getApp_market() {
		return app_market;
	}

	public void setApp_market(String app_market) {
		this.app_market = app_market;
	}

	public String getApp_version() {
		return app_version;
	}
	
	public void setApp_version(String app_version) {
		this.app_version = app_version;
	}
	
	public String getLowest_version() {
		return lowest_version;
	}
	
	public void setLowest_version(String lowest_version) {
		this.lowest_version = lowest_version;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDownload_url() {
		return download_url;
	}
	
	public void setDownload_url(String download_url) {
		this.download_url = download_url;
	}
	
	public String getReg_date() {
		return reg_date;
	}
	
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
	public String getMod_date() {
		return mod_date;
	}
	
	public void setMod_date(String mod_date) {
		this.mod_date = mod_date;
	}

    public boolean isForceUpdate(String version) {
        DefaultArtifactVersion clientVersion = new DefaultArtifactVersion(version);
        DefaultArtifactVersion minVersion = new DefaultArtifactVersion(this.lowest_version);
		if (clientVersion.compareTo(minVersion) < 0) return true;
        return false;
    }
}

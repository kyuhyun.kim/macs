package com.zipdoc.acs.domain.service;


import com.zipdoc.acs.model.ProductListRequest;
import org.springframework.dao.DataAccessException;

public interface KeywordSynonymService {

	String selectKeywordSynonym(String keyword) throws DataAccessException;

}

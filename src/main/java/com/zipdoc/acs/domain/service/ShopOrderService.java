package com.zipdoc.acs.domain.service;

import java.util.List;

import com.zipdoc.acs.domain.entity.ShopOrder;
import com.zipdoc.acs.domain.entity.ShopOrderInfo;
import com.zipdoc.acs.model.shop.ShopOrderListRequest;

/**
 * Created by dskim on 2017. 5. 11..
 */
public interface ShopOrderService {

    void insert(ShopOrder shopOrder) throws ServiceException;

    String getOrderNo() throws ServiceException;

	int selectListTotalCount(ShopOrderListRequest condition) throws ServiceException;

	List<ShopOrder> selectList(ShopOrderListRequest condition) throws ServiceException;

	ShopOrderInfo findByIdWithOrderGoods(String order_no) throws ServiceException;

	ShopOrder findById(String order_no) throws ServiceException;
}

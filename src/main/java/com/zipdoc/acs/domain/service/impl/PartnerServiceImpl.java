package com.zipdoc.acs.domain.service.impl;

import java.util.*;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.PartnerService;
import com.zipdoc.acs.model.*;
import com.zipdoc.acs.model.partners.RecomPartners;
import com.zipdoc.acs.persistence.dao.HomeLayoutDao;
import com.zipdoc.acs.persistence.dao.PartnerDao;

import com.zipdoc.acs.persistence.dao.ProductDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class PartnerServiceImpl implements PartnerService {
	
	@Autowired
	private PartnerDao partnerDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private HomeLayoutDao homeLayoutDao;

	@Override
	public AppVersion selectVersion(String os_type, String app_market) {
		
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("os_type", os_type);
		map.put("app_market", app_market);
		
		return partnerDao.selectVersion(map);
	}
		
	@Override
	public int deleteDeviceByDeviceId(String device_id) throws DataAccessException {
		return partnerDao.deleteDeviceByDeviceId(device_id);
	}
	
	@Override
	public Partner selectPartner(int partner_id) throws DataAccessException {
		Map<String, Object> map = new HashMap<>();
		map.put("partner_id", partner_id);
		return partnerDao.selectPartner(map);
	}
	
	@Override
	public PartnerProduct selectProduct(Integer product_code) throws DataAccessException {
		if (product_code == null) return null;
		return partnerDao.selectProduct(product_code);
	}	
	
	@Override
	public PartnerContractSummary summaryContract(int partner_id) throws DataAccessException {
		return partnerDao.summaryContract(partner_id);
	}	
	
	@Override
	public Estimate selectEstimate(long estimate_no) throws DataAccessException {
		return partnerDao.selectEstimate(estimate_no);
	}
	
	@Override
	public int getAssignCount(int partner_id) throws DataAccessException {
		return partnerDao.countAssignTotal(partner_id);
	}	
	
	@Override
	public int getAssignResidentialCount(int partner_id) throws DataAccessException {
		return partnerDao.countAssignResidential(partner_id);
	}	
	
	@Override
	public int getAssignCommercialCount(int partner_id) throws DataAccessException {
		return partnerDao.countAssignCommercial(partner_id);
	}		
	
	@Override
	public int getAssignFreeCount(int partner_id) throws DataAccessException {
		return partnerDao.countAssignFree(partner_id);
	}
	
	@Override
	public int getAssignAcceptCount(int partner_id) throws DataAccessException {
		return partnerDao.countAssignAcceptTotal(partner_id);
	}
	
	@Override
	public int getAssignRejectCount(int partner_id) throws DataAccessException {
		return partnerDao.countAssignRejectTotal(partner_id);
	}
	
	@Override
	public int getAssignNewCount(int partner_id) throws DataAccessException {
		return partnerDao.countAssignNew(partner_id);
	}

	@Override
	public int getAssignNewAcceptCount(int partner_id) throws DataAccessException {
		return partnerDao.countAssignNewAccept(partner_id);
	}

	@Override
	public int getAssignOngoingCount(int partner_id) throws DataAccessException {
		return partnerDao.countAssignOngoing(partner_id);
	}
	
	@Override
	public List<PartnerAssign> selectAssignNew(int partner_id) throws DataAccessException {
		return partnerDao.selectAssignNew(partner_id);
	}
	
	@Override
	public List<PartnerAssign> selectAssignOngoing(PartnerAssignSearchRequest searchCondition) throws DataAccessException {
		return partnerDao.selectAssignOngoing(searchCondition);
	}
	
	@Override
	public PartnerContract selectContractOngoing(int partner_id, long estimate_no) throws DataAccessException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("partner_id", partner_id);
		map.put("estimate_no", estimate_no);
		
		return partnerDao.selectContractOngoing(map);
	}	
	
	@Override
	public List<PartnerAssign> selectAssignCompletion(PartnerAssignSearchRequest searchCondition) throws DataAccessException {
		return partnerDao.selectAssignCompletion(searchCondition);
	}
	
	@Override
	public PartnerContract selectContractCompletion(int partner_id, long estimate_no) throws DataAccessException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("partner_id", partner_id);
		map.put("estimate_no", estimate_no);
		
		return partnerDao.selectContractCompletion(map);
	}
	
	@Override
	public List<PartnerAssign> selectAssignWithdraw(PartnerAssignSearchRequest searchCondition) throws DataAccessException {
		return partnerDao.selectAssignWithdraw(searchCondition);
	}

	@Override
	public PartnerAssign selectAssign(int partner_id, long estimate_no) throws DataAccessException {

		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("partner_id", partner_id);
		map.put("estimate_no", estimate_no);
		
		return partnerDao.selectAssign(map);
	}

	@Override
	public PartnerAssign selectAssignDetail(int partner_id, long estimate_no) throws DataAccessException {

		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("partner_id", partner_id);
		map.put("estimate_no", estimate_no);
		
		return partnerDao.selectAssignDetail(map);
	}
	
	public List<PartnerAssign> selectAssignList(int partner_id, int page, int limit) throws DataAccessException {
	
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		map.put("partner_id", partner_id);
		map.put("start", (page*limit));
		map.put("limit", limit);
		
		return partnerDao.selectAssignAll(map);
	}
	
	@Override
	public int acceptAssign(PartnerAssign assign) throws DataAccessException {
		return partnerDao.acceptAssign(assign);
	}

	@Override
	public int updateAssignWithCounsel(int partner_id, PartnerAssignCounsel counsel) throws DataAccessException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("partner_id", partner_id);
		map.put("estimate_no", counsel.getEstimate_no());
		
		AssignStatus status = AssignStatus.valueOf(counsel.getStatus()); 
		switch (status) {
		
			// 1단계 - 상담예약
			case SCHEDULE_COUNSELING:
				
			// 2단계 - 상담완료
			case COMPLETE_COUNSELING:
			{
				switch (AssignCounselType.valueOf(counsel.getCounsel_type())) {
				
					case VISIT:
					{
						map.put("status", counsel.getStatus());
						map.put("counsel_type", counsel.getCounsel_type());
						if (status == AssignStatus.SCHEDULE_COUNSELING) {
							map.put("schd_visit_date", new Date(counsel.getVisit_date()));
						} else {
							map.put("visit_date", new Date(counsel.getVisit_date()));
						}
						break;
					}
					
					case NO_VISIT:
					{
						map.put("status", counsel.getStatus());
						map.put("counsel_type", counsel.getCounsel_type());
						break;
					} 
					
					case ESTIMATE:
					{
						map.put("status", AssignStatus.COMPLETE_COUNSELING.value());
						map.put("counsel_type", AssignCounselType.NO_VISIT.value());
						//map.put("visit_date", new Date(counsel.getVisit_date()));
						break;
					} 
					
					default:
					{
						map.put("completed_flag", 1);
						map.put("stop_code", counsel.getStop_code());
						if (counsel.getStop_code() == AssignStopCode.ETC.value()) {
							if (counsel.getStop_reason() != null) { map.put("stop_reason", counsel.getStop_reason()); }
						}
						break;
					}
				}
				break;
			}
			
			case PUBLISH_ESTIMATE:
			{
				if (counsel.getFeedback_date() > 0) {
					map.put("feedback_date", new Date(counsel.getFeedback_date()));
				}
				if (counsel.getStop_code() > 0) {
					map.put("completed_flag", 1);
					map.put("stop_code", counsel.getStop_code());
					if (counsel.getStop_code() == AssignStopCode.ETC.value()) {
						if (counsel.getStop_reason() != null) { map.put("stop_reason", counsel.getStop_reason()); }
					}
				} else {
					map.put("status", counsel.getStatus());
					map.put("publish_date", new Date(System.currentTimeMillis()));
					map.put("estimate_price", counsel.getEstimate_price());
				}
				break;
			}
			
			case COMPLETE_CONTRACT:
			{
				map.put("completed_flag", 1);
				
				if (counsel.getStop_code() > 0) {
					map.put("stop_code", counsel.getStop_code());
					if (counsel.getStop_code() == AssignStopCode.ETC.value()) {
						if (counsel.getStop_reason() != null) { map.put("stop_reason", counsel.getStop_reason()); }
					}
				} else {
					map.put("status", counsel.getStatus());
				}
				break;
			}
			
			default: return 0;
		}
		
		return partnerDao.updateAssignByMap(map);
	}
	
	@Override
	public int restoreAssign(int partner_id, long estimate_no) throws DataAccessException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("partner_id", partner_id);
		map.put("estimate_no", estimate_no);
		
		return partnerDao.restoreAssign(map);
	}
	
	@Override
	public List<PartnerContract> selectContractList_estimateNo(long estimate_no) throws DataAccessException {
		return partnerDao.selectContractList_estimateNo(estimate_no);
	}
	
	@Override
	public PartnerContract selectContract(long contract_no) throws DataAccessException {
		return partnerDao.selectContract(contract_no);
	}
	
	@Override
	public PartnerContract selectContract_estimateNo(int partner_id, long estimate_no) throws DataAccessException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("partner_id", partner_id);
		map.put("estimate_no", estimate_no);
		
		return partnerDao.selectContract_estimateNo(map);
	}

	@Override
	public PartnerContract selectContract_Cancel(int partner_id, long estimate_no) throws DataAccessException {

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("partner_id", partner_id);
		map.put("estimate_no", estimate_no);

		return partnerDao.selectContract_Cancel(map);
	}
	
	@Override
	public long createContract(PartnerContract contract) throws DataAccessException {
		return partnerDao.createContract(contract);
	}
	
	@Override
	public int updateContract_agreementStatus(PartnerContract contract) throws DataAccessException {
		return partnerDao.updateContract_agreementStatus(contract);
	}
	
	@Override
	public int updateContract_app(PartnerContractModify modify) throws DataAccessException {
		return partnerDao.updateContract_app(modify);
	}
	
	@Override
	public int updateContract_start(long contract_no, long estimate_no, long datetime) throws DataAccessException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("contract_no", contract_no);
		map.put("start_date", datetime);

		//견적정보 상태 변경 처리
		map.put("estimate_no", estimate_no);
		map.put("status", 4);
		partnerDao.updateEstimateStatus(map);

		return partnerDao.updateContract_start(map);
	}
	
	@Override
	public int updateContract_stop(long contract_no, long estimate_no, long datetime) throws DataAccessException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("contract_no", contract_no);
		map.put("end_date", datetime);

		//견적정보를 완료 처리
		map.put("estimate_no", estimate_no);
		map.put("status", 8);
		map.put("completed_flag", 1);
		map.put("completed_code", 0);

		partnerDao.updateEstimateStatus(map);

		//계약정보를 완료 처리
		return partnerDao.updateContract_stop(map);
	}
	
	@Override
	public int restoreContract(long contract_no) throws DataAccessException {
		return partnerDao.restoreContract(contract_no);
	}
	
	@Override
	public int updateEstimate_contract(long estimate_no, int status) throws DataAccessException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("estimate_no", estimate_no);
		map.put("status", status);
		
		return partnerDao.updateEstimate_contract(map);
	}
	
	@Override
	public List<PartnerContract> selectUnpaidContract(int partner_id, int days) throws DataAccessException {
		
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		map.put("partner_id", partner_id);
		map.put("days", days);
		
		return partnerDao.selectUnpaidContract(map);
	}
	
	@Override
	public PartnerContractSummary summaryUnpaidContract(int partner_id, int days) throws DataAccessException {
	
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		map.put("partner_id", partner_id);
		map.put("days", days);
		
		return partnerDao.summaryUnpaidContract(map);
	}
	
	@Override
	public int createContractHistory(PartnerContractHistory history) throws DataAccessException {
		return partnerDao.createContractHistory(history);
	}
	
	@Override
	public int createContractHistoryFile(PartnerContractHistoryFile file) throws DataAccessException {
		return partnerDao.createContractHistoryFile(file);
	}
	
	@Override
	public List<PartnerContractHistory> selectContractHistoryByContractNo(long contract_no) throws DataAccessException {
		return partnerDao.selectContractHistoryByContractNo(contract_no);
	}
	
	@Override
	public List<PartnerContractHistoryFile> selectContractHistoryFile(long history_no) throws DataAccessException {
		return partnerDao.selectContractHistoryFile(history_no);
	}

	@Override
	public int createInquiry(PartnerInquiry inquiry) throws DataAccessException {
		return partnerDao.createInquiry(inquiry);
	}
	
	@Override
	public int createInquiryFile(PartnerInquiryFile file) throws DataAccessException {
		return partnerDao.createInquiryFile(file);
	}
	
	@Override
	public List<PartnerInquiry> selectInquiry(int partner_id, long estimate_no, int page, int limit) throws DataAccessException {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("partner_id", partner_id);
		map.put("estimate_no", estimate_no);
		map.put("start", (page*limit));
		map.put("limit", limit);
		
		return partnerDao.selectInquiry(map);
	}
	
	@Override
	public List<PartnerInquiryFile> selectInquiryFile(long cno) throws DataAccessException {
		return partnerDao.selectInquiryFile(cno);
	}

	@Override
	public List<PartnerAssign> selectAssignStopTargetList(long estimate_no, int partner_id) throws DataAccessException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("partner_id", partner_id);
		map.put("estimate_no", estimate_no);
		return partnerDao.selectAssignStopTargetList(map);
	}

	@Override
	public void updateAssignStop(PartnerAssign partnerAssign) throws DataAccessException {
		partnerDao.updateAssignStop(partnerAssign);
	}

	@Override
	public void updateContractFileInfo(PartnerContract contract) throws DataAccessException {
		partnerDao.updateContractFileInfo(contract);
	}

	@Override
	public void updateAssignMemo(PartnerAssign partnerAssign) throws DataAccessException {
		partnerDao.updateAssignMemo(partnerAssign);
	}

	@Override
	public void updateAssignRetryCall(PartnerAssign partnerAssign) throws DataAccessException {
		partnerDao.updateAssignRetryCall(partnerAssign);
	}

	@Override
	public void insertCallCounselStatus(CallEstimate callEstimate) throws DataAccessException {
		partnerDao.insertCallCounselStatus(callEstimate);
	}

	@Override
	public List<Long> selectCallEstimateList(Integer partner_id, Long estimate_no)
			throws DataAccessException {
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("partner_id", partner_id);
		condition.put("estimate_no", estimate_no);
		return partnerDao.selectCallEstimateList(condition);
	}


	@Override
	public List<PartnerAssignGeo> selectAssignWithdrawGeo(int partner_id) throws DataAccessException {
		return partnerDao.selectAssignWithdrawGeo(partner_id);
	}

	@Override
	public int selectListTotalCount(ListRequest searchCondition) throws DataAccessException {
		return partnerDao.selectListTotalCount(searchCondition);
	}

	@Override
	public List<PartnerFull> selectList(ListRequest searchCondition) throws DataAccessException {
		return partnerDao.selectList(searchCondition);
	}

	@Override
	public PartnerFull selectPartnerDetail(Integer partner_id) throws DataAccessException {
		return partnerDao.selectPartnerDetail(partner_id);
	}

	@Override
	public PartnerStatsResponse selectPartnerStatsByArea(ListRequest searchCondition) throws DataAccessException {

		// 평가점수
		PartnerStatsResponse result = partnerDao.selectPartnerEvaluationStats(searchCondition);

		// 계약현황
		PartnerStatsResponse temp = partnerDao.selectPartnerContractStats(searchCondition);
		result.setTotal_contract_count(temp.getTotal_contract_count());
		result.setTotal_contract_commission(temp.getTotal_contract_commission());
		result.setTotal_contract_price(temp.getTotal_contract_price());

		// 계약상품
		result.setProducts_rank(partnerDao.selectPartnerProductsRanks(searchCondition));

		return result;
	}

	@Override
	public PartnerBlackList selectBlackList(String mobile_no) throws DataAccessException {
		return partnerDao.selectBlackList(mobile_no);
	}

	@Override
	public List<Partner> selectBestPartner(Map<String, Object> map, int thumbnail_cnt) throws DataAccessException {

		// 캐쉬 기능을 사용하기 위해 HomeLayoutDao에서 호출한다.
		List<Partner> list = homeLayoutDao.selectBestPartner(map);
		if (list == null) return null;

		return setPartnerProductThumbnails(list, thumbnail_cnt, (String) map.get("category_code1"));
	}

	@Override
	public List<Partner> selectPartnerListRevision(Map<String, Object> condition) throws DataAccessException {

		List<Partner> list = partnerDao.selectPartnerListRevision(condition);
		if (list == null) return null;
		return setPartnerProductThumbnails(list, Integer.valueOf( condition.get("thumbnail_cnt").toString()), null);
	}

	@Override
	public List<Partner> selectPartnerList(PartnerListRequest condition) throws DataAccessException {

		List<Partner> list = partnerDao.selectPartnerList(condition);
		if (list == null) return null;
		return setPartnerProductThumbnails(list, condition.getThumbnail_cnt(), null);
	}

	@Override
	public List<Partner> selectQuarterRecoBestPartnerList(Map<String, Object> condition) throws DataAccessException {

		List<Partner> list = partnerDao.selectQuarterRecoBestPartnerList(condition);
		if (list == null) return null;

		return setPartnerProductThumbnails(list, 2, null);

	}

	@Override
	public List<Partner> selectMonthRecoBestPartnerList(Map<String, Object> condition) throws DataAccessException {

		List<Partner> list = partnerDao.selectMonthRecoBestPartnerList(condition);
		if (list == null) return null;

		return setPartnerProductThumbnails(list, 2, null);

	}

	@Override
	public List<Partner> selectMonthBestPartnerList(Map<String, Object> condition) throws DataAccessException {

		return partnerDao.selectMonthBestPartnerList(condition);
	}

	@Override
	public List<RecomPartners> selectQuarterBestPartner(Map<String, Object> condition) throws DataAccessException {

		return partnerDao.selectQuarterBestPartner(condition);
	}

	@Override
	public List<RecomPartners> selectMonthBestPartner(Map<String, Object> condition) throws DataAccessException {

		return partnerDao.selectMonthBestPartner(condition);
	}


	@Override
	public List<Partner> selectQuarterPartnerList(Map<String, Object> condition) throws DataAccessException {

		List<Partner> list = partnerDao.selectQuarterPartnerList(condition);
		if (list == null) return null;
		return setPartnerProductThumbnails(list, Integer.parseInt(condition.get("thumbnail_cnt").toString() ), null);

	}

	@Override
	public List<Partner> selectMonthPartnerList(Map<String, Object> condition) throws DataAccessException {

		List<Partner> list = partnerDao.selectMonthPartnerList(condition);
		if (list == null) return null;
		return setPartnerProductThumbnails(list, Integer.parseInt(condition.get("thumbnail_cnt").toString() ), null);

	}

	@Override
	public int selectQuarterPartnerListCount(Map<String, Object> condition) throws DataAccessException {

		return partnerDao.selectQuarterPartnerListCount(condition);
	}

	@Override
	public int selectMonthPartnerListCount(Map<String, Object> condition) throws DataAccessException {

		return partnerDao.selectMonthPartnerListCount(condition);
	}

	@Override
	public int selectReliefPartnerListCount() throws DataAccessException {

		return partnerDao.selectReliefPartnerListCount();
	}

	@Override
	public List<Partner> selectReliefPartner(Map<String, Object> condition) throws DataAccessException {

		return partnerDao.selectReliefPartner(condition);
	}


	@Override
	public int selectPartnerListCount(PartnerListRequest condition) throws DataAccessException {
		return partnerDao.selectPartnerListCount(condition);
	}


	@Override
	public int selectPartnerListCountRevision(Map<String, Object> condition) throws DataAccessException {
		return partnerDao.selectPartnerListCountRevision(condition);
	}

	@Override
	public List<Partner> setPartnerProductThumbnails(List<Partner> list, int gallery_cnt, String category_code1) {

		for (Partner partner : list) {

			// INTRO 에서 개행문자 제거
			if (StringUtils.isNotEmpty(partner.getIntro_msg())) {
				partner.setIntro_msg(partner.getIntro_msg().replaceAll("\r\n",""));
			}

			// 파트너사의 시공사례 N건 조회한다.
			ProductListRequest condition = new ProductListRequest();
			condition.setPage(0);
			condition.setLimit(gallery_cnt);
			if (StringUtils.isNotEmpty(category_code1)) {
				condition.setCategory_code1(category_code1);
			}
			condition.setPartner_id(partner.getPartner_id());

			List<Product> gallery = productDao.selectList(condition);
			if (gallery != null && gallery.size() > 0) {
				List<String> thumbnails = new ArrayList<>();
				for (Product product : gallery) {
					thumbnails.add(Constants.STATIC_URL + product.getFile_path() + PictureType.HDPI.getPrefix() + product.getFile_name());
				}
				partner.setGallery_thumbnails(thumbnails);
			}else{
				partner.setGallery_thumbnails(new ArrayList<String>());
			}
		}

		return list;
	}
}

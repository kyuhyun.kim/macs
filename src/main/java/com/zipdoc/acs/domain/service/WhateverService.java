package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.WhateverListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface WhateverService {
	public List<Whatever> selectList(WhateverListRequest searchCondition) throws DataAccessException;
	public int selectListTotalCount(WhateverListRequest searchCondition) throws DataAccessException;
	public Whatever selectById(long cno) throws DataAccessException;
	public void create(Whatever whatever) throws DataAccessException;
	public void delete(long cno) throws DataAccessException;
	public void updateIncreaseViewCnt(long cno) throws DataAccessException;
	public void deleteMulti(Whatever target) throws DataAccessException;
	public void deleteAllByMemberNo(Long member_no) throws DataAccessException;
}

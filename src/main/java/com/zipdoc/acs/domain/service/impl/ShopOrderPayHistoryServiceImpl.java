package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.ShopOrderPayHistory;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopOrderPayHistoryService;
import com.zipdoc.acs.persistence.dao.ShopOrderPayHistoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShopOrderPayHistoryServiceImpl implements ShopOrderPayHistoryService {
		
	@Autowired
	private ShopOrderPayHistoryDao shopOrderPayHistoryDao;
	
	@Override
	public void createOrderPayHistory(ShopOrderPayHistory shopOrderPayHistory) throws ServiceException {
		shopOrderPayHistoryDao.createOrderPayHistory(shopOrderPayHistory);
	}

}

package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.Item;
import com.zipdoc.acs.domain.entity.Partner;
import com.zipdoc.acs.domain.entity.Product;
import com.zipdoc.acs.domain.service.ProductService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.ItemListRequest;
import com.zipdoc.acs.model.ProductListRequest;
import com.zipdoc.acs.persistence.dao.CashedProductDao;
import com.zipdoc.acs.persistence.dao.PartnerDao;
import com.zipdoc.acs.persistence.dao.ProductDao;
import com.zipdoc.acs.utils.AcsConfig;
import com.zipdoc.acs.utils.FileInfo;
import com.zipdoc.acs.utils.FileUploadUtil;
import com.zipdoc.acs.utils.ImageResizeUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ZIPDOC on 2016-06-02.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private AcsConfig acsConfig;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private PartnerDao partnerDao;

    @Autowired
    private CashedProductDao cashedProductDao;


    @Override
    public List<Product> selectBeforeAfterList(ProductListRequest searchCondition) throws DataAccessException
    {
        if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
            return cashedProductDao.selectBeforeAfterList(searchCondition);
        else
            return productDao.selectBeforeAfterList(searchCondition);
    }

    @Override
    public  int selectListTotalBeforeAfterCount(ProductListRequest searchCondition) throws DataAccessException
    {
        if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
            return cashedProductDao.selectListTotalBeforeAfterCount(searchCondition);
        else
            return productDao.selectListTotalBeforeAfterCount(searchCondition);

    }

    @Override
    public int selectListTotalCount(ProductListRequest searchCondition) throws DataAccessException {
        return productDao.selectListTotalCount(searchCondition);
    }

    @Override
    public List<Product> selectList(ProductListRequest searchCondition) throws DataAccessException {
        return productDao.selectList(searchCondition);
    }

    @Override
    public int searchGalleryTotalCountRevision(ProductListRequest searchCondition) throws DataAccessException {
        return productDao.searchGalleryTotalCountRevision(searchCondition);
    }

    @Override
    public int searchGalleryTotalCountRevision(Map<String, Object> searchCondition ) throws DataAccessException {
        return productDao.searchGalleryTotalCountRevision(searchCondition);
    }

    @Override
    public int searchGalleryTotalCount(ProductListRequest searchCondition) throws DataAccessException {
        return productDao.searchGalleryTotalCount(searchCondition);
    }

    @Override
    public List<Product> searchGalleryList(ProductListRequest searchCondition) throws DataAccessException {
        return productDao.searchGalleryList(searchCondition);
    }



    @Override
    public List<Product> searchGalleryListRevision(Map<String, Object> searchCondition) throws DataAccessException {
        return productDao.searchGalleryListRevision(searchCondition);
    }

    @Override
    public List<Product> searchGalleryListRevision(ProductListRequest searchCondition) throws DataAccessException {
        return productDao.searchGalleryListRevision(searchCondition);
    }

    @Override
    public Product selectById(long pid, Long member_no) throws DataAccessException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("pid", pid);
        map.put("member_no", member_no);
        Product product = productDao.selectById(map);
        if (product != null) {
            product.setItems(productDao.selectItemListByPid(map));
        }
        return product;
    }

    @Override
    public int selectCommunityListTotalCount(ProductListRequest searchCondition) throws DataAccessException {
        return productDao.selectCommunityListTotalCount(searchCondition);
    }

    @Override
    public List<Product> selectCommunityList(ProductListRequest searchCondition) throws DataAccessException {
        return productDao.selectCommunityList(searchCondition);
    }

    @Override
    public Product selectCommunityById(long pid, Long member_no) throws DataAccessException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("pid", pid);
        map.put("member_no", member_no);
        Product product = productDao.selectCommunityById(map);
        if(product != null) {
            product.setItems(productDao.selectItemListByPid(map));
        }
        return product;
    }

    @Override
    public void updateProductStatus(Product product) throws DataAccessException {
        productDao.updateProductStatus(product);
    }

    @Override
    public void updateIncreaseHitCnt(long pid) throws DataAccessException {
        productDao.updateIncreaseHitCnt(pid);
    }

    @Override
    public void updateProductStatusAllByMemberNo(Long member_no) throws DataAccessException {
        productDao.updateProductStatusAllByMemberNo(member_no);
    }

    @Override
    public int selectItemListTotalCount(ItemListRequest searchCondition) throws DataAccessException {
        return productDao.selectItemListTotalCount(searchCondition);
    }

    @Override
    public List<Item> selectItemList(ItemListRequest searchCondition) throws DataAccessException {
        return productDao.selectItemList(searchCondition);
    }

    @Override
    public int searchGalleryPictureTotalCount(ItemListRequest searchCondition) throws DataAccessException {
        return productDao.searchGalleryPictureTotalCount(searchCondition);
    }

    @Override
    public int searchGalleryPictureTotalCountRevision(Map<String, Object> searchCondition) throws DataAccessException
    {
        return productDao.searchGalleryPictureTotalCountRevision(searchCondition);
    }

    @Override
    public int searchGalleryPictureTotalCountRevision(ItemListRequest searchCondition) throws DataAccessException {
        return productDao.searchGalleryPictureTotalCountRevision(searchCondition);
    }

    @Override
    public List<Item> searchGalleryPictureList(ItemListRequest searchCondition) throws DataAccessException {
        return productDao.searchGalleryPictureList(searchCondition);
    }

    @Override
    public List<Item> searchGalleryPictureListRevision(Map<String, Object> searchCondition) throws DataAccessException {
        return productDao.searchGalleryPictureListRevision(searchCondition);
    }

    @Override
    public List<Item> searchGalleryPictureListRevision(ItemListRequest searchCondition) throws DataAccessException {
        return productDao.searchGalleryPictureListRevision(searchCondition);
    }

    @Override
    public Partner selectPartner(int partner_id, int gallery_cnt) throws DataAccessException {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("partner_id", partner_id);
        map.put("detail", 1);

        Partner partner = partnerDao.selectPartner(map);
        if (partner != null) {
            // 파트너사의 시공사례 조회.
            ProductListRequest condtion = new ProductListRequest();
            condtion.setPage(0);
            condtion.setLimit(gallery_cnt);
            condtion.setPartner_id(partner_id);
            partner.setList(productDao.selectList(condtion));
        }

        return partner;
    }


    @Override
    public void insert(Product product) throws DataAccessException {
        productDao.insert(product);

        treatProductFile(product, null);

        //첨부된 사진이 없으면 리턴 처리함.
        if(product.getItems()==null) return;

        for(Item item:product.getItems()){
            item.setPid(product.getPid());
            productDao.insertItem(item);
            productDao.insertProductItem(item);
            treatItemFile(item, null);
        }
    }

    @Override
    public void update(Product product) throws DataAccessException {
        Product dbProduct = productDao.selectByPid(product.getPid());
        if(dbProduct==null) return;

        if(product.getUpdater().longValue() != dbProduct.getCreater().longValue()){
            throw new ServiceException(HttpStatus.SC_FORBIDDEN, "등록자가 아닙니다.");
        }

        productDao.update(product);

        //대표이미지 처리
        if(product.isPictureChanged()) {
            treatProductFile(product, dbProduct.getFile_name());
        }

        //등록된 사진이 없으면 리턴 처리함.
        if(product.getItems()==null) return;

        for(Item item:product.getItems()){
            if(item.isCreateCmd()) {
                productDao.insertItem(item);
                productDao.insertProductItem(item);
                treatItemFile(item, null);
            } else if(item.isUpdateCmd()) {
                productDao.updateItem(item);

                if(item.isPictureChanged()) {
                    Item dbItem = productDao.selectItem(item.getItem_id());
                    treatItemFile(item, dbItem.getFile_name());
                }
            } else if(item.isDeleteCmd()) {
                productDao.deleteProductItem(item);
                productDao.deleteItem(item);

                Item dbItem = productDao.selectItem(item.getItem_id());
                //파일 디렉토리를 삭제한다.
                if(dbItem != null && StringUtils.isNotEmpty(dbItem.getFile_path())) {
                    try {
                        String filePath = Constants.UPLOAD_STATIC_PATH + dbItem.getFile_path();
                        FileUtils.deleteDirectory(new File(filePath));
                    }catch (Exception e){}
                }
            }
        }
    }

    @Override
    public void updateIncreaseShareCnt(long pid) throws DataAccessException {
        productDao.updateIncreaseShareCnt(pid);
    }

    @Override
    public List<Product> selectSamePriceProductList(Map<String, Object> condition) throws DataAccessException {
        return productDao.selectSamePriceProductList(condition);
    }

    private void treatProductFile(Product entity, String oldFileName) throws ServiceException {
        try {
            String relativePath = Constants.RELATIVE_COMMUNITY_PATH + entity.getPid() + "/";
            String absolutePath = Constants.UPLOAD_STATIC_PATH + relativePath;

            entity.setFile_path(relativePath);

            //1.원본 파일을 저장한다.
            FileInfo fileInfo = FileUploadUtil.uploadFile(entity.getMultipartFile(), relativePath);
            entity.setFile_name(fileInfo.getFile().getName());
            entity.setFile_name_org(fileInfo.getOriginFilename());
            entity.setThumbnail(PictureType.MDPI.getPrefix() + fileInfo.getFile().getName());

            //2.썸네일 및 리사이징 파일 생성
            ImageResizeUtil.imageResizeForThurmbnails(absolutePath, entity.getFile_name(), absolutePath);

            File destDir = new File(absolutePath);
            FileUploadUtil.getInstance().uploadToS3(destDir, relativePath );

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "파일 변환 중 오류가 발생되었습니다.");
        }
        productDao.updateProductFileInfo(entity);

        //파일수정 시 이전 파일을 삭제한다.
        if(StringUtils.isNotEmpty(oldFileName)){
            FileUploadUtil.getInstance().deleteFilesBySuffix(entity.getFile_path(), oldFileName);
        }
    }

    private void treatItemFile(Item entity, String oldFileName){
        try {
            String relativePath = Constants.RELATIVE_ITEM_PATH + entity.getPid() + "/";
            String absolutePath = Constants.UPLOAD_STATIC_PATH + relativePath;

            entity.setFile_path(relativePath);

            //1.원본 파일을 저장한다.
            FileInfo fileInfo =
                    FileUploadUtil.uploadFile(entity.getMultipartFile(), relativePath);
            entity.setFile_name(fileInfo.getFile().getName());
            entity.setFile_name_org(fileInfo.getOriginFilename());
            entity.setFile_type(FileType.IMAGE.value());
            entity.setFile_size(fileInfo.getSize());
            entity.setThumbnail(PictureType.MDPI.getPrefix() + fileInfo.getFile().getName());

            //2.썸네일 및 리사이징 파일 생성
            ImageResizeUtil.imageResizeForThurmbnails(absolutePath, entity.getFile_name(), absolutePath);

            File destDir = new File(absolutePath);
            FileUploadUtil.getInstance().uploadToS3(destDir, relativePath );
            FileUploadUtil.getInstance().deleteFile(fileInfo.getFile());

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "파일 변환 중 오류가 발생되었습니다.");
        }
        entity.setTrans_status(FileTranStatus.COMPLETED.getCode());
        productDao.updateItemFileInfo(entity);

        //파일수정 시 이전 파일을 삭제한다.
        if(StringUtils.isNotEmpty(oldFileName)){
            FileUploadUtil.getInstance().deleteFilesBySuffix(entity.getFile_path(), oldFileName);
        }
    }

    @Override
    public Product selectConcept(long pid) throws DataAccessException {
        return productDao.selectConcept(pid);
    }
}

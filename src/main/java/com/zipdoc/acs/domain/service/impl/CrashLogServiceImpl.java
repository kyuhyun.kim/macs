package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.Product;
import com.zipdoc.acs.domain.entity.CrashLog;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.CrashLogService;
import com.zipdoc.acs.persistence.dao.ProductDao;
import com.zipdoc.acs.persistence.dao.CrashLogDao;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 이동규 on 2016-06-13.
 */
@Service
public class CrashLogServiceImpl implements CrashLogService {

    @Autowired
    private CrashLogDao crashLogDao;

    @Override
    public void createCrashLog(CrashLog crashLog) throws DataAccessException {
        crashLogDao.createCrashLog(crashLog);
    }
}

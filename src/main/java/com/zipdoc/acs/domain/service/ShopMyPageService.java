package com.zipdoc.acs.domain.service;

import java.util.List;
import java.util.Map;

import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.OrderSummary;
import com.zipdoc.acs.domain.entity.ShopOrderInfo;

public interface ShopMyPageService {

	public OrderSummary selectShopMyPageSummary(Member member) throws ServiceException;

	public List<ShopOrderInfo> findRecentList(Map<String, Object> condition) throws ServiceException;

}

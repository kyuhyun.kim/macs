package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.OrderSummary;
import com.zipdoc.acs.domain.entity.ShopOrderGoods;

import java.util.List;
import java.util.Map;

public interface ShopOrderGoodsService {
	List<ShopOrderGoods> findList(String order_no) throws ServiceException;
	void createOrderGoods(ShopOrderGoods shopOrderGoods) throws ServiceException;
	ShopOrderGoods findById(Long sno) throws ServiceException;
	void updatePurchasComplete(Map<String, Object> condition) throws ServiceException;
	OrderSummary orderSummary(Long member_no) throws ServiceException;
}

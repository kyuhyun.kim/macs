package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.ServiceInfo;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface CommonService {
	ServiceInfo selectServiceInfo() throws DataAccessException;
	ServiceInfo selectPartnerSerivceInfo() throws DataAccessException;
	InteriorProduct selectInteriorProduct(int product_code) throws DataAccessException;
	ServiceInfo selectCmsServiceInfo() throws DataAccessException;
	RecommendKeyword selectKeywordList(String keyword_type) throws DataAccessException;

	String selectCategoryName(String category_code) throws DataAccessException;


}



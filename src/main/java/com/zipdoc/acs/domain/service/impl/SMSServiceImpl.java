package com.zipdoc.acs.domain.service.impl;


import com.zipdoc.acs.define.SMSStatus;
import com.zipdoc.acs.domain.entity.CertificationTempKey;
import com.zipdoc.acs.domain.service.SMSService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.persistence.dao.SMSDao;
import com.zipdoc.acs.utils.SMSFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class SMSServiceImpl implements SMSService {

    private static final Logger log = LoggerFactory.getLogger(SMSServiceImpl.class);

    @Autowired
    private SMSDao smsDao;

    @Override
    public String tempKeyGenarator(String phone) throws ServiceException {

        //인증키 생성
        String tempKey = keyGenarator(phone);

        log.debug("암호화키 : "+tempKey);

        smsDao.deleteTempKey(phone);

        CertificationTempKey certificationTempKey = new CertificationTempKey();

        certificationTempKey.setTemp_key(tempKey);
        certificationTempKey.setPhone(phone);

        String resultCode = null;

        try {

            SMSFactory smsFactory = new SMSFactory(tempKey,phone);

            resultCode = smsFactory.Send();

            if(resultCode != null) {

                if(resultCode.equals(SMSStatus.SUCCESS.getCode())
                        || resultCode.equals(SMSStatus.RESERVED.getCode())
                        || resultCode.equals(SMSStatus.TEST_SUCCESS.getCode())) {

                    //sms로 발송한 신규 인증키를 db에 삽입
                    //System.out.println("sms 전송 완료");
                    smsDao.insertCertificationKey(certificationTempKey);
                }

            }


        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return resultCode;
    }

    /*
        success : DB인증키와 유저인증키 맞음
        fail : DB인증키와 유저인증키 틀림
        expiration : 인증키는 맞나 만료되었음
     */
    @Override
    public String isCorrectCertifiKey(String phone, String temp_key, String current_timestamp) throws ServiceException {

        String resultCode = null;

        //db에서 dbKey를 가져와 저장할 임시변수
        String dbKey = "";
        //암호화된 전화번호로 임시키 가져옴
        CertificationTempKey tempKeyInfo = smsDao.getTempKey(phone);

        if(tempKeyInfo != null) {
            dbKey = tempKeyInfo.getTemp_key();
        }

        //임시키와 인풋키 공백제거
        dbKey = dbKey.trim();
        temp_key = temp_key.trim();

        Date current_date = new Date(Long.parseLong(current_timestamp));
        Date expiration_date = tempKeyInfo.getExpiration_date();

        //인풋키와 임시키 비교
        if(temp_key.equals(dbKey)){

            //현재시간이 DB만료시간보다 이후이면 만료
            if(current_date.after(expiration_date)) {
                resultCode = "expiration";
            } else {
                resultCode = "success";
            }

            //디비에 누적된 임시키 삭제
            smsDao.deleteTempKey(phone);

        }
        else{

            if(current_date.after(expiration_date)) {
                //디비에 누적된 임시키 삭제
                smsDao.deleteTempKey(phone);
            } else {
                resultCode = "fail";
            }
        }

        return resultCode;
    }


    public String keyGenarator(String Number){
        String lastNumberString = null;
        String numberArray[] = Number.split("-");
        if(numberArray[2].charAt(0) == '0'){
            lastNumberString = "1"+numberArray[2].substring(1, numberArray[2].length());
        }else{
            lastNumberString = numberArray[2];
        }
        String last = Long.toString((Integer.parseInt(lastNumberString) * System.currentTimeMillis()));
        return last.substring(last.length()-6, last.length());
    }
}

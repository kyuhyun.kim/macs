package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.BenefitRequest;
import org.springframework.dao.DataAccessException;

public interface BenefitService {

	void insertBenefit(BenefitRequest request) throws DataAccessException;
}



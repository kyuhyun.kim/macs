package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.ContractSideHistory;
import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Created by dskim on 2017. 9. 22..
 */
public interface ContractSideHistoryService {

//    public int createContractSideHistory(ContractSideHistory contractSideHistory) throws DataAccessException;

    void createContractSideHistory(List<ContractSideHistory> contractSideHistoryList) throws DataAccessException;

}

package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.define.MemberType;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.persistence.dao.MemberDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MemberServiceImpl implements MemberService {
	
	@Autowired
	private MemberDao memberDao;
	
	public long findNextID() {
		return memberDao.findNextID();
	}

	public int createDevice(Member member) {
		return memberDao.createDevice(member);
	}

	/**
	 * 앱유형에 따라 사용자 정보를 조회한다.
	 * @param device_id
	 * @param app_type 앱유형 1 : 집닥앱, 2 : 파트너스앱
	 * @return
	 * @throws DataAccessException
     */
	@Override
	public Member selectDeviceByDeviceId(String device_id, int app_type) throws DataAccessException {
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("device_id", device_id);
		condition.put("app_type", app_type);
		return memberDao.selectDeviceByDeviceId(condition);
	}

	public Member selectMember(Long member_no, long zid) throws DataAccessException {
		if(member_no == null || member_no <= 0){ //비회원 가입자
			return memberDao.selectMemberByZid(zid);
		} else { //회원 가입자
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("member_no", member_no);
			map.put("zid", zid);
			return memberDao.selectMember(map);
		}
	}

	public Member selectMemberByZid(long zid) throws DataAccessException  {
		return memberDao.selectMemberByZid(zid);
	}

	@Override
	public Member selectMemberByMemberId(long member_id) throws DataAccessException {
		return memberDao.selectMemberByMemberId(member_id);
	}

	public Member selectMemberByForgotAccount(ForgotAccount account) throws DataAccessException  {
		return memberDao.selectMemberByForgotAccount(account);
	}
	
	public Member selectMemberByAccount(Account account) throws DataAccessException  {
		return memberDao.selectMemberByAccount(account);
	}
	
	public int updateDeviceUserLevel(Member member) throws DataAccessException  {
		return memberDao.updateDeviceUserLevel(member);
	}
	
	public boolean IsExistAccount(String account) throws DataAccessException  {
		return (memberDao.findAccount(account) == null) ? false : true;
	}
	
	public int updateToken(Member member) throws DataAccessException  {
		return memberDao.updateToken(member);
	}

	@Override
	public void treatMember(Member member) throws DataAccessException {

		// 회원정보를 생성한다.
		member.setGrade(MemberType.get(member.getMember_type()).getGrade());
		memberDao.createMember(member);

		// APP에서 회원가입을 요청한 경우 디바이스 정보를 업데이트한다.
		if (member.getZid() > 0) {
			memberDao.updateDeviceUserLevel(member);
			treatMemberDevice(member);
		}
	}

	@Override
	public void treatMemberDevice(Member member) throws DataAccessException {
		int memberDeviceCount = memberDao.selectMemberDeviceCount(member);
		if(memberDeviceCount == 0) { //멤버 디바이스 정보가 존재하지 않는 경우에만 수행
			memberDao.createMemberDevice(member);
		}
	}

	@Override
	public void updateMemberLoginInfo(Member member) throws DataAccessException {
		if(member == null || member.getMember_no() <= 0) return;
		memberDao.updateMemberLoginInfo(member);
	}

	@Override
	public void updateDevice(Member member) throws DataAccessException {
		memberDao.updateDevice(member);
	}

	@Override
	public MyPage selectMyPageSummary(Member member) throws DataAccessException {
		MyPage myPage = memberDao.selectMyPageSummary(member);
		myPage.setProfile(memberDao.selectProfile(member.getMember_no()));
		return myPage;
	}

	@Override
	public Profile selectProfile(long member_no) throws DataAccessException{
		return memberDao.selectProfile(member_no);
	}

	@Override
	public void updateProfile(Member member) throws DataAccessException{
		memberDao.updateProfile(member);
	}

	@Override
	public void updateNickname(Member member) throws DataAccessException{
		memberDao.updateNickname(member);
	}

	@Override
	public int selectCountUsingNicknameCount(String nickname) throws DataAccessException {
		return memberDao.selectCountUsingNicknameCount(nickname);
	}

	@Override
	public void updateDeviceAgree(Member member) throws DataAccessException {
		memberDao.updateDeviceAgree(member);
	}

	@Override
	public void updateDeviceAgreePrivacy(Member member) throws DataAccessException {
		memberDao.updateDeviceAgreePrivacy(member);
	}

	@Override
	public void deleteMember(Member member) throws DataAccessException {

		// T_DEVICE USER_LEVEL 변경. 회원(1) -> 자동가입고객(0)
		memberDao.updateDeviceUserLevel(member);

		// T_MEMBER_DEVICE 삭제
		memberDao.deleteMemberDevice(member);

		// T_MEMBER 삭제
		memberDao.deleteMember(member);
	}

	@Override
	public Member selectMemberByWebToken(String token) throws DataAccessException {
		return memberDao.selectMemberByWebToken(token);
	}

	@Override
	public ZipdocmanUser selectZipdocmanUser(long member_no) throws DataAccessException {
		ZipdocmanUser user = memberDao.selectZipdocmanUser(member_no);
		if (user != null) {
			user.setGroup_members(memberDao.selectZipdocmanGroup(user.getGroup_no()));
		}
		return user;
	}

	@Override
	public int updateMember(Account account) throws DataAccessException {
		return memberDao.updateMember(account);
	}

	@Override
	public void updateMemberAlarm(AlarmInfo alarmInfo)
	{
		memberDao.updateMemberAlarm(alarmInfo);
	}

	@Override
	public void updateDeviceAlarm(AlarmInfo alarmInfo)
	{
		memberDao.updateDeviceAlarm(alarmInfo);
	}

	@Override
	public AlarmInfo selectMemberAlarm(Member member)throws DataAccessException
	{
		return memberDao.selectMemberAlarm(member);
	}

	@Override
	public AlarmInfo selectDeviceAlarm(Member member)throws DataAccessException
	{
		return memberDao.selectDeviceAlarm(member);
	}

}

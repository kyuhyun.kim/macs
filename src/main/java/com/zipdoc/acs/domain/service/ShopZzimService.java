package com.zipdoc.acs.domain.service;

import java.util.List;

import com.zipdoc.acs.domain.entity.ShopZzim;
import com.zipdoc.acs.domain.entity.ShopZzimInfo;
import com.zipdoc.acs.model.shop.ShopZzimListRequest;

public interface ShopZzimService {
	public int selectShopZzimListTotalCount(ShopZzimListRequest condition) throws ServiceException;
	public List<ShopZzimInfo> selectShopZzimList(ShopZzimListRequest condition) throws ServiceException;
	public void createZzim(ShopZzim shopZzim) throws ServiceException;
	public void deleteZzim(ShopZzim shopZzim) throws ServiceException;
	public void deleteZzimAll(ShopZzim shopZzim) throws ServiceException;
}

package com.zipdoc.acs.domain.service.impl;

import java.util.List;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zipdoc.acs.domain.entity.ShopZzim;
import com.zipdoc.acs.domain.entity.ShopZzimInfo;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopZzimService;
import com.zipdoc.acs.model.shop.ShopZzimListRequest;
import com.zipdoc.acs.persistence.dao.ShopGoodsDao;
import com.zipdoc.acs.persistence.dao.ShopZzimDao;

@Service
public class ShopZzimServiceImpl implements ShopZzimService{

	@Autowired
	private ShopZzimDao shopZzimDao;
	
	@Autowired
	private ShopGoodsDao shopGoodsDao;
	
	@Override
	public int selectShopZzimListTotalCount(ShopZzimListRequest condition) throws ServiceException {
		return shopZzimDao.selectShopZzimListTotalCount(condition);
	}

	@Override
	public List<ShopZzimInfo> selectShopZzimList(ShopZzimListRequest condition) throws ServiceException {
		return shopZzimDao.selectShopZzimList(condition);
	}

	@Override
	public void createZzim(ShopZzim shopZzim) throws ServiceException {
		int count = shopZzimDao.selectShopZzimCount(shopZzim);
		
		if(count == 0){
			shopZzimDao.createZzim(shopZzim);
			shopGoodsDao.updateIncreaseZzimCnt(shopZzim.getGoods_no());
		}else{
			throw new ServiceException(HttpStatus.SC_CONFLICT, "이미 관심상품이 추가되었습니다."	);
		}
	}

	@Override
	public void deleteZzim(ShopZzim shopZzim) throws ServiceException {
		shopZzimDao.deleteZzim(shopZzim);
	}

	@Override
	public void deleteZzimAll(ShopZzim shopZzim) throws ServiceException {
		shopZzimDao.deleteZzimAll(shopZzim);
	}

}

package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.ArticleService;
import com.zipdoc.acs.model.ArticleListRequest;
import com.zipdoc.acs.model.ArticleListResponse;
import com.zipdoc.acs.persistence.dao.ArticleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ArticleServiceImpl implements ArticleService {
	
	@Autowired
	private ArticleDao articleDao;

	public ArticleListResponse selectArticleList(ArticleListRequest articleListRequest) throws DataAccessException {
		List<Article> list = null;
		if(articleListRequest.getArticle_type() == ArticleType.NOTICE){
			int total_count = articleDao.selectNoticeListTotalCount(articleListRequest);
			if(total_count>0) {
				list = articleDao.selectNoticeList(articleListRequest);
			}
			return new ArticleListResponse(total_count, list);
		} else if(articleListRequest.getArticle_type() == ArticleType.EVENT) {
			int total_count = articleDao.selectEventListTotalCount(articleListRequest);
			if(total_count>0) {
				list = articleDao.selectEventList(articleListRequest);
			}
			return new ArticleListResponse(total_count, list);

		} else if(articleListRequest.getArticle_type() == ArticleType.FAQ) {
			int total_count = articleDao.selectFaqListTotalCount(articleListRequest);
			if(total_count>0) {
				list = articleDao.selectFaqList(articleListRequest);
			}

			return new ArticleListResponse(total_count, list);
		} else if(articleListRequest.getArticle_type() == ArticleType.INTERIOR_TIP) {
			int total_count = articleDao.selectInteriorTipListTotalCount(articleListRequest);
			if(total_count>0) {
				list = articleDao.selectInteriorTipList(articleListRequest);
			}
			return new ArticleListResponse(total_count, list);

		} else if(articleListRequest.getArticle_type() == ArticleType.NEWS) {
			int total_count = articleDao.selectNewsListTotalCount(articleListRequest);
			if(total_count>0) {
				list = articleDao.selectNewsList(articleListRequest);
			}
			return new ArticleListResponse(total_count, list);

		} else if(articleListRequest.getArticle_type() == ArticleType.PARTNER_NOTICE) {
			int total_count = articleDao.selectPatnerNoticeListTotalCount(articleListRequest);
			if(total_count>0) {
				list = articleDao.selectPatnerNoticeList(articleListRequest);
			}
			return new ArticleListResponse(total_count, list);

		} else if(articleListRequest.getArticle_type() == ArticleType.ZIPDOCMAN) {
			int total_count = articleDao.selectZipdocmanListTotalCount(articleListRequest);
			if(total_count>0) {
				list = articleDao.selectZipdocmanList(articleListRequest);
			}
			return new ArticleListResponse(total_count, list);

		} else {
			return new ArticleListResponse(0, list);
		}
	}

	@Override
	public Article selectArticle(ArticleType articleType, int cno) throws DataAccessException {
		if(articleType == ArticleType.NOTICE){
			articleDao.updateNoticeViewCnt(cno);
			return articleDao.selectNotice(cno);
		} else if(articleType == ArticleType.EVENT) {
			articleDao.updateEventViewCnt(cno);
			return articleDao.selectEvent(cno);
		} else if(articleType == ArticleType.INTERIOR_TIP) {
			articleDao.updateInteriorTipViewCnt(cno);
			return articleDao.selectInteriorTip(cno);
		} else if(articleType == ArticleType.NEWS) {
			articleDao.updateNewsViewCnt(cno);
			return articleDao.selectNews(cno);
		} else if(articleType == ArticleType.PARTNER_NOTICE) {
			articleDao.updatePatnerNoticeViewCnt(cno);
			return articleDao.selectPatnerNotice(cno);
		} else if(articleType == ArticleType.ZIPDOCMAN) {
			articleDao.updateZipdocmanViewCnt(cno);
			return articleDao.selectZipdocman(cno);
		} else {
			return null;
		}
	}
}



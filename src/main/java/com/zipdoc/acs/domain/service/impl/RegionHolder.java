package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.AddressStep;
import com.zipdoc.acs.model.Region;
import com.zipdoc.acs.persistence.dao.CommonDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ZIPDOC on 2016-06-09.
 */
@Component
public class RegionHolder {
    private static final Logger logger = LoggerFactory.getLogger(RegionHolder.class);

    @Autowired
    private CommonDao commonDao;

    private boolean initialized = false;

    private List<Region> regions = new ArrayList<Region>();

    private long lastLoadTime = System.currentTimeMillis();

    private static int EXPIRED_DURATION = 5*60*1000; //5분

    private Thread reloader;

    public RegionHolder(){
    }

    @PostConstruct
    public void intialize(){
        reload();
        this.initialized = true;

        reloader = new Thread( new Runnable() {
            @Override
            public void run() {
                logger.debug("RegionHolder START!!");
                if(isNeedReloadRegions()){
                    logger.debug("RegionHolder reload Region START!!");
                    reload();
                    logger.debug("RegionHolder reload Region END!!");
                }
                try {
                    Thread.sleep(EXPIRED_DURATION);
                } catch (InterruptedException e) {}
                logger.debug("RegionHolder END!!");
            }
        });
        reloader.start();
    }

    @PreDestroy
    public void cleanUp(){
        try {
            reloader.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public List<Region> getRegions() {
        return regions;
    }

    private boolean isNeedReloadRegions(){
        if(regions.size() == 0) return true;
        if(System.currentTimeMillis()-lastLoadTime > EXPIRED_DURATION) return true;
        return false;
    }

    private void reload(){
        List<AddressStep> addressSteps = commonDao.selectAddressStepList();

        for(AddressStep address : addressSteps){

            Region region = new Region(address.getSido(), address.getSigungu());

            int index = regions.indexOf(region);

            if(index < 0){
                regions.add(region);
            } else {
                regions.get(index).addSubregion(address.getSigungu());
            }
        }
        lastLoadTime = System.currentTimeMillis();
    }
}

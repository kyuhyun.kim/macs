package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.BenefitRequest;
import com.zipdoc.acs.domain.service.BenefitService;
import com.zipdoc.acs.persistence.dao.BenefitDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class BenefitServiceImpl implements BenefitService {

	@Autowired
	private BenefitDao benefitDao;

	@Override
	public void insertBenefit(BenefitRequest request) throws DataAccessException {
		benefitDao.insertBenefit(request);
	}
}



package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.ShopOrder;
import com.zipdoc.acs.domain.entity.ShopOrderInfo;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopOrderService;
import com.zipdoc.acs.model.shop.ShopOrderListRequest;
import com.zipdoc.acs.persistence.dao.ShopOrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static com.zipdoc.acs.utils.DateUtil.DATE_FORMAT;

/**
 * 주문 서비스
 * Created by dskim on 2017. 5. 11..
 */
@Service
public class ShopOrderServiceImpl implements ShopOrderService {

    @Autowired
    private ShopOrderDao shopOrderDao;

    @Override
    public void insert(ShopOrder shopOrder) throws ServiceException {

        shopOrderDao.insert(shopOrder);

    }

    @Override
    public String getOrderNo() throws ServiceException {
        String codeValue = null;

        for(;;) {
            codeValue = orderNoGenerator();
            ShopOrder shopOrder = shopOrderDao.selectByOrderNo(codeValue);
            if(shopOrder == null) {
                break;
            }
        }

        return codeValue;
    }


    public String orderNoGenerator() {

        Date now = new Date();
        SimpleDateFormat vans = new SimpleDateFormat(DATE_FORMAT);

        String codeValue = vans.format(now);

        Random rnd = new Random();
        for (int i = 0; i < 2; i++) {
            codeValue += String.valueOf(rnd.nextInt(9));
        }

        //codeValue += "000";

        return codeValue;
    }

	@Override
	public int selectListTotalCount(ShopOrderListRequest condition) throws ServiceException {
		return shopOrderDao.selectListTotalCount(condition);
	}

	@Override
	public List<ShopOrder> selectList(ShopOrderListRequest condition) throws ServiceException {
		return shopOrderDao.selectList(condition);
	}

	@Override
	public ShopOrderInfo findByIdWithOrderGoods(String order_no) throws ServiceException {
		return shopOrderDao.findByIdWithOrderGoods(order_no);
	}

	@Override
	public ShopOrder findById(String order_no) throws ServiceException {
		return shopOrderDao.findById(order_no);
	}
}

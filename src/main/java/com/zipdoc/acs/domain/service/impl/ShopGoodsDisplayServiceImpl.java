package com.zipdoc.acs.domain.service.impl;


import com.zipdoc.acs.domain.entity.ShopGoods;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopGoodsDisplayService;
import com.zipdoc.acs.persistence.dao.ShopGoodsDisplayDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 *
 * 상품 진열 *
 * Created by dskim on 2017. 2. 22..
 */
@Service
public class ShopGoodsDisplayServiceImpl implements ShopGoodsDisplayService {

    @Autowired
    private ShopGoodsDisplayDao shopGoodsDisplayDao;

    @Override
    public List<ShopGoods> bestGoodsList() throws ServiceException {

        List<ShopGoods> shopGoodsList = shopGoodsDisplayDao.bestGoodsList();

        ImageUrlSet(shopGoodsList);

        return shopGoodsList;
    }

    //2017-03-07 신상품 추가
    @Override
    public List<ShopGoods> newGoodsList() throws ServiceException {

        List<ShopGoods> shopGoodsList = shopGoodsDisplayDao.newGoodsList();

        ImageUrlSet(shopGoodsList);

        return shopGoodsList;
    }

    //2017-3-20 패키지상품 추가
    @Override
    public List<ShopGoods> packageGoodsList() throws ServiceException {

        List<ShopGoods> shopGoodsList = shopGoodsDisplayDao.packageGoodsList();

        ImageUrlSet(shopGoodsList);

        return shopGoodsList;

    }


    public void ImageUrlSet(List<ShopGoods> shopGoodsList) {

        for(ShopGoods shopGoods:shopGoodsList){
            shopGoods.buildGoodsImageUrl();
        }

    }

}

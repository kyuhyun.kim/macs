package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.Like;
import com.zipdoc.acs.domain.service.LikeService;
import com.zipdoc.acs.persistence.dao.LikeDao;
import com.zipdoc.acs.persistence.dao.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 이동규 on 2017-03-08.
 */
@Service
public class LikeServiceImpl implements LikeService {

    @Autowired
    private LikeDao likeDao;

    @Autowired
    private ProductDao productDao;

    @Override
    public void create(Like like) throws DataAccessException {
        if(like.isLoginedUser()) {
            int count = likeDao.findCount(like);
            if(count==0) {
                likeDao.create(like);
            }
        }
        productDao.updateIncreaseLikeCnt(like.getPid());
    }

    @Override
    public void delete(Like like) throws DataAccessException {
        if(like.isLoginedUser()) {
            likeDao.delete(like);
        }
        productDao.updateDecreaseLikeCnt(like.getPid());
    }
}

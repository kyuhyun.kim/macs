package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.PartnerReply;
import com.zipdoc.acs.domain.entity.Product;
import com.zipdoc.acs.domain.entity.Zzim;
import com.zipdoc.acs.domain.service.PartnerReplyService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ZzimService;
import com.zipdoc.acs.model.PartnerReplyListRequest;
import com.zipdoc.acs.model.ZzimListRequest;
import com.zipdoc.acs.persistence.dao.PartnerReplyDao;
import com.zipdoc.acs.persistence.dao.ProductDao;
import com.zipdoc.acs.persistence.dao.ZzimDao;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 이동규 on 2016-06-13.
 */
@Service
public class PartnerReplyServiceImpl implements PartnerReplyService {

    @Autowired
    private PartnerReplyDao partnerReplyDao;

    @Override
    public int selectPartnerReplyListTotalCount(PartnerReplyListRequest searchCondition) throws DataAccessException {
        return partnerReplyDao.selectPartnerReplyListTotalCount(searchCondition);
    }

    @Override
    public List<PartnerReply> selectPartnerReplyList(PartnerReplyListRequest searchCondition) throws DataAccessException {
        return partnerReplyDao.selectPartnerReplyList(searchCondition);
    }

    @Override
    public void createPartnerReply(PartnerReply partnerReply) throws DataAccessException {
        partnerReplyDao.createPartnerReply(partnerReply);
    }


    @Override
    public void updatePartnerReply(PartnerReply partnerReply) throws DataAccessException {
        partnerReplyDao.updatePartnerReply(partnerReply);
    }

    @Override
    public void deletePartnerReply(PartnerReply partnerReply) throws DataAccessException {
        partnerReplyDao.deletePartnerReply(partnerReply);
    }
}

package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.define.AgentType;
import com.zipdoc.acs.define.CategoryCode;
import com.zipdoc.acs.define.VividCommentCategoryCode;
import com.zipdoc.acs.domain.entity.Customer;
import com.zipdoc.acs.domain.entity.CustomerPartnerInquiry;
import com.zipdoc.acs.domain.entity.CustomerReply;
import com.zipdoc.acs.domain.entity.VividComment;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface CustomerOpinionsService {

	List<VividComment> getVividComment(CategoryCode category, AgentType agentType, int startRow, int limit ) throws DataAccessException;

	List<VividComment> getVividComment(VividCommentCategoryCode vividCommentCategoryCode, AgentType agentType, int startRow, int limit ) throws DataAccessException;

	Integer getVividCommentCount(VividCommentCategoryCode vividCommentCategoryCode, AgentType agentType) throws DataAccessException;
}

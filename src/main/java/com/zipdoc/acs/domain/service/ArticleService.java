package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.domain.entity.Article;
import com.zipdoc.acs.model.ArticleListRequest;
import com.zipdoc.acs.model.ArticleListResponse;
import com.zipdoc.acs.model.ServiceInfo;
import org.springframework.dao.DataAccessException;

public interface ArticleService {

	public ArticleListResponse selectArticleList(ArticleListRequest req) throws DataAccessException;

	public Article selectArticle(ArticleType articleType, int cno) throws DataAccessException;


}



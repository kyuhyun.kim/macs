package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.Partner;

public interface AuthenticationHelper {
	/**
	 * 사용자 정보를 조회 후 유효하지 않은 경우 예외를 발생시키고, 정상인 경우 사용자 정보를 리턴한다.
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @return
	 * @throws ServiceException
     */
	Member findMember(Long member_no, Long zid, String zid_key) throws ServiceException;

	/**
	 * 사용자 정보를 조회 후 유효하지 않은 경우 예외를 발생시키고, 정상인 경우 사용자 정보를 리턴한다.
	 * needMember true인 경우에는 회원이 아닌경우 예외를 발생시킨다.
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @param needMember
	 * @return
	 * @throws ServiceException
     */
	Member findMember(Long member_no, Long zid, String zid_key, boolean needMember) throws ServiceException;


	/**
	 * 파트너사 정보를 조회 후 유효하지 않은 경우 예외를 발생시키고, 정상인 경우 파트너사 정보를 리턴한다.
	 * @param partner_id
	 * @return
	 * @throws ServiceException
     */
	Partner findPartner(Integer partner_id) throws ServiceException;

	/**
	 * 앱 또는 웹의 가입 정보 조회시 호출한다. 가입자인 경우에만 사용한다.
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @param web_token
	 * @return
	 * @throws ServiceException
	 */
	Member findMember(Long member_no, Long zid, String zid_key, String web_token) throws ServiceException;

	/**
	 * 앱 또는 웹의 가입 정보 조회시 호출한다. 웹인 경우 회원이 아닌 경우 오류 처리하며 앱의 경우 회원이 아닌 경우 디바이스 정보를 반환한다.
	 * @param member_no
	 * @param zid
	 * @param zid_key
	 * @param web_token
	 * @return
	 * @throws ServiceException
	 */
	Member findMemberOrDevice(Long member_no, Long zid, String zid_key, String web_token) throws ServiceException;
}

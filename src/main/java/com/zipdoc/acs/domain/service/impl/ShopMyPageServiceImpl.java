package com.zipdoc.acs.domain.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.OrderSummary;
import com.zipdoc.acs.domain.entity.ShopOrderInfo;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopMyPageService;
import com.zipdoc.acs.persistence.dao.ShopMyPageDao;

@Service
public class ShopMyPageServiceImpl implements ShopMyPageService{

	@Autowired
	private ShopMyPageDao shopMyPageDao;
	
	@Override
	public OrderSummary selectShopMyPageSummary(Member member) throws ServiceException {
		return shopMyPageDao.selectShopMyPageSummary(member);
	}

	@Override
	public List<ShopOrderInfo> findRecentList(Map<String, Object> condition) throws ServiceException {
		condition.put("startRow", 0);
		condition.put("endRow", 3);
		condition.put("itemSize", 3);
		return shopMyPageDao.findRecentList(condition);
	}

}

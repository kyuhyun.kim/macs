package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.Comment;
import com.zipdoc.acs.model.CommentListRequest;

import java.util.List;
import java.util.Map;

public interface CommentService {
	public List<Comment> selectList(CommentListRequest searchCondition) throws ServiceException;
	public int selectListTotalCount(CommentListRequest searchCondition) throws ServiceException;
	public Comment selectById(Comment comment) throws ServiceException;
	public void insert(Comment comment) throws ServiceException;
	public void update(Comment comment) throws ServiceException;
	public void updateDeleteFlag(Comment comment) throws ServiceException;

	public void delete(Comment comment) throws ServiceException;
}

package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.HomeScreenService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.HomeScreenLayout;
import com.zipdoc.acs.model.ListRequest;
import com.zipdoc.acs.model.MagazineListRequest;
import com.zipdoc.acs.model.ProductListRequest;
import com.zipdoc.acs.persistence.dao.*;
import com.zipdoc.acs.utils.AcsConfig;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class HomeScreenServiceImpl implements HomeScreenService {

	private static final Logger log = LoggerFactory.getLogger(HomeScreenServiceImpl.class);

	@Autowired
	private AcsConfig acsConfig;

	@Autowired
	private HomeLayoutDao homeLayoutDao;

	@Autowired
	private CashedHomeLayoutDao cashedHomeLayoutDao;

	@Autowired
	private CommonDao commonDao;

	@Autowired
	private CashedCommonDao cashedCommonDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private BoardDao boardDao;

	@Autowired
	private EstimateDao estimateDao;

	@Autowired
	private CashedEstimateDao cashedEstimateDao;

	@Override
	public HomeScreenLayout getCashedHomeScreenLayout(CategoryCode category, AgentType agentType) throws Exception {

		HomeScreenLayout layout = new HomeScreenLayout();

		// 주거/상업 검색조건 설정
		Map<String, Object> searchCondition = new HashMap<>();
		searchCondition.put("category", LayoutCategory.get(category).getCode());
		searchCondition.put("agent", agentType.getCode());

		// 홈메인화면 문구 조회
		List<LayoutConfig> configList = null;
		if (agentType == AgentType.WEB) {
			configList = cashedHomeLayoutDao.selectCashedHomeLayoutTextWeb(searchCondition);
		}
		else
		{
			 configList = homeLayoutDao.selectHomeLayoutText(searchCondition);
		}

		if (configList == null || configList.size() == 0) {
			log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 [TITLE] 미설정.");
			throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Layout-Title");
		}

		// 조회 편리를 위해 Map을 구성
		Map<String, LayoutConfig> layoutTitle = new HashMap<>();
		Map<String, String> configTags = new HashMap<>();
		for (LayoutConfig config : configList) {

			// 인기태그 정보
			if (config.getConfig_type() == LayoutConfigType.TAG.getCode()) {
				configTags.put(config.getConfig_name(), config.getImage_url());
			} else {
				layoutTitle.put(config.getConfig_name(), config);
			}
		}

		String temp;
		String [] array;

		// 주거/상업 공통

		// 인기태그 (웹 전용)
		if (agentType == AgentType.WEB) {
			searchCondition.put("layout", LayoutType.TAG.getCode());
			List<LayoutHome> homeList = cashedHomeLayoutDao.selectCashedHomeLayout(searchCondition);
			if (homeList == null || homeList.size() == 0) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 인기태그 미설정.");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Popular-Tags");
			}
			layout.setTags(getPopularTags(homeList, configTags));
		}

		// 파트너 정보
		LayoutConfig config = layoutTitle.get("파트너");
		if (config == null) {
			log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 파트너 설명문구 없음.");
			throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Partner's Title.");
		}
		layout.setPartners(new LayoutPartner());
		layout.getPartners().setTitle(config.getConfig_title());

		// 주거공간
		if (category == CategoryCode.RESIDENT) {

			// 배너 조회 (앱 전용)
			if (agentType == AgentType.APP) {
				if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
					layout.setBanners(cashedCommonDao.selectEventPromotionList(EventChannelType.APP_RESIDENT.getCode()));
				else
					layout.setBanners(commonDao.selectEventPromotionList(EventChannelType.APP_RESIDENT.getCode()));
			}
			// 배너 조회 (웹 전용)
			else {
				if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
					layout.setBanners(cashedCommonDao.selectEventPromotionList(EventChannelType.WEB_RESIDENT.getCode()));
				else
					layout.setBanners(commonDao.selectEventPromotionList(EventChannelType.WEB_RESIDENT.getCode()));
				if (layout.getBanners() != null) {
					for (EventPromotion banner : layout.getBanners()) {
						// 배너 유형중에 견적현황 배너가 포함된 경우
						if (banner.getLanding_type().equalsIgnoreCase("S")) {
							// 견적 현황 정보 설정
							EstimateSummary summary = estimateDao.selectEstimateSummary();
							summary.setDoing_count(null);
							layout.setEstimate(summary);
							break;
						}
					}
				}
			}

			// 주거공간 공간분류 조회
			searchCondition.put("layout", LayoutType.PARTIAL.getCode());
			List<LayoutHome> homeList = null;
			if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
				homeList = cashedHomeLayoutDao.selectCashedHomeLayout(searchCondition);
			else
				homeList = homeLayoutDao.selectHomeLayout(searchCondition);

			if (homeList == null || homeList.size() == 0) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 공간사진정보 미설정.");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Resident-Partial");
			}

			// 부분사진 태그 리스트 (FILTER_1 에 공간분류 목록이 포함됨)
			array = StringUtils.split(((temp = homeList.get(0).getAdditional_1()) == null ? "" : temp), " ,");
			if (array == null || array.length < 3) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 주거공간의 공간분류[RESIDENT_PARTIAL] 설정 오류. 3개이상 설정 필수");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Resident-Partial Tags");
			}

			// 부분사진 태그 설정
			layout.setPartials(getLayoutPartial(array, layoutTitle));

			// 앱인 경우 퀵필터 정보 설정
			if (agentType == AgentType.APP) {

				// 퀵 필터 설정 정보 조회
				searchCondition.put("layout", LayoutType.QUICK_FILTER.getCode());
				List<LayoutHome> filterList = null;

				if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
					filterList = cashedHomeLayoutDao.selectCashedHomeLayout(searchCondition);
				else
					filterList = homeLayoutDao.selectHomeLayout(searchCondition);

				if (filterList == null || filterList.size() < 2) {
					log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 퀵필터 정보 설정 오류. 2개이상 설정 필수.");
					throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Resident-Filter");
				}

				// 퀵 필터 설정
				layout.setFilters(getLayoutFilter(filterList, layoutTitle));
			}

			// 웹은 시공사례 및 견적현황 조회
			else {

				// 필터 시공사례 설정 정보 조회
				searchCondition.put("layout", LayoutType.GALLERY_FILTER.getCode());
				List<LayoutHome> galleryList = null;
				if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
					galleryList = cashedHomeLayoutDao.selectCashedHomeLayout(searchCondition);
				else
					galleryList = homeLayoutDao.selectHomeLayout(searchCondition);

				if (galleryList == null || galleryList.size() < 2) {
					log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 시공사례 정보 설정 오류. 2개이상 설정 필수.");
					throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Resident-Gallery");
				}

				// 시공사례 필터 설정
				layout.setFilter_gallery(getLayoutFilterGallery(galleryList, category, layoutTitle));

				// 견적신청 현황 조회

				if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
					layout.setEstimate_list(cashedEstimateDao.selectWebMainList());
				else
					layout.setEstimate_list(estimateDao.selectWebMainList());
			}
			return layout;
		}

		// 배너 조회 (앱 전용)
		if (agentType == AgentType.APP) {
			if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
				layout.setBanners(cashedCommonDao.selectEventPromotionList(EventChannelType.APP_COMMERCE.getCode()));
			else
				layout.setBanners(commonDao.selectEventPromotionList(EventChannelType.APP_COMMERCE.getCode()));
		}
		else {
			if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
				layout.setBanners(cashedCommonDao.selectEventPromotionList(EventChannelType.WEB_COMMERCE.getCode()));
			else
				layout.setBanners(commonDao.selectEventPromotionList(EventChannelType.WEB_COMMERCE.getCode()));
		}

		// 상업공간 시공사례 정보 조회
		searchCondition.put("layout", LayoutType.GALLERY.getCode());
		List<LayoutHome> galleryList = null;
		if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
			galleryList = homeLayoutDao.selectHomeLayout(searchCondition);
		else
			galleryList = cashedHomeLayoutDao.selectCashedHomeLayout(searchCondition);

		if (galleryList == null || galleryList.size() == 0) {
			log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 상업공간 시공사례 정보 미설정.");
			throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Commerce-Gallery");
		}

		// 시공사례 구성
		for (LayoutHome layoutHome : galleryList) {

			// 시공사례 표시 개수
			if (StringUtils.isEmpty(layoutHome.getAdditional_2())) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 상업공간의 시공사례항목[COMMERCE_GALLERY]의 사례 갯수 미설정.");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Commerce-Gallery's Count");
			}

			Integer gallery_cnt = 4;
			try {
				gallery_cnt = Integer.parseInt(layoutHome.getAdditional_2());
			} catch (Exception e) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 상업공간의 시공사례항목[COMMERCE_GALLERY]의 사례 갯수 설정 오류 ["+layoutHome.getAdditional_2()+"]");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Commerce-Gallery's Count");
			}

			// 시공사례 항목 리스트 (ADDITIONAL_1에 CATEGORY 포함됨)
			array = StringUtils.split(((temp = layoutHome.getAdditional_1()) == null ? "" : temp), " ,");
			if (array == null || array.length == 0) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 상업공간의 시공사례항목[COMMERCE_GALLERY]의 카테고리 미설정.");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Commerce-Gallery Category");
			}

			layout.setGallery(getCommerceLayoutGallery(array, layoutTitle, gallery_cnt));
		}

		// 필터 시공사례 설정 정보 조회
		searchCondition.put("layout", LayoutType.GALLERY_FILTER.getCode());
		List<LayoutHome> filter_gallery = null;
		if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
			filter_gallery = cashedHomeLayoutDao.selectCashedHomeLayout(searchCondition);
		else
			filter_gallery = homeLayoutDao.selectHomeLayout(searchCondition);

		// 시공사례 필터 설정
		if (filter_gallery != null && filter_gallery.size() > 0) {
			layout.setFilter_gallery(getLayoutFilterGallery(filter_gallery, category, layoutTitle));
		}

		return layout;
	}

	@Override
	public HomeScreenLayout getHomeScreenLayout(CategoryCode category, AgentType agentType) throws Exception {

		HomeScreenLayout layout = new HomeScreenLayout();

		// 주거/상업 검색조건 설정
		Map<String, Object> searchCondition = new HashMap<>();
		searchCondition.put("category", LayoutCategory.get(category).getCode());
		searchCondition.put("agent", agentType.getCode());

		// 홈메인화면 문구 조회
		List<LayoutConfig> configList = homeLayoutDao.selectHomeLayoutText(searchCondition);
		if (configList == null || configList.size() == 0) {
			log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 [TITLE] 미설정.");
			throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Layout-Title");
		}

		// 조회 편리를 위해 Map을 구성
		Map<String, LayoutConfig> layoutTitle = new HashMap<>();
        Map<String, String> configTags = new HashMap<>();
		for (LayoutConfig config : configList) {

			// 인기태그 정보
            if (config.getConfig_type() == LayoutConfigType.TAG.getCode()) {
            	configTags.put(config.getConfig_name(), config.getImage_url());
			} else {
				layoutTitle.put(config.getConfig_name(), config);
			}
		}

		String temp;
		String [] array;

		// 주거/상업 공통

		// 인기태그 (웹 전용)
		if (agentType == AgentType.WEB) {
			searchCondition.put("layout", LayoutType.TAG.getCode());
			List<LayoutHome> homeList = homeLayoutDao.selectHomeLayout(searchCondition);
			if (homeList == null || homeList.size() == 0) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 인기태그 미설정.");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Popular-Tags");
			}
			layout.setTags(getPopularTags(homeList, configTags));
		}

		// 파트너 정보
		LayoutConfig config = layoutTitle.get("파트너");
		if (config == null) {
			log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 파트너 설명문구 없음.");
			throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Partner's Title.");
		}
		layout.setPartners(new LayoutPartner());
		layout.getPartners().setTitle(config.getConfig_title());

		// 주거공간
		if (category == CategoryCode.RESIDENT) {

			// 배너 조회 (앱 전용)
			if (agentType == AgentType.APP) {
				layout.setBanners(commonDao.selectEventPromotionList(EventChannelType.APP_RESIDENT.getCode()));
			}
			// 배너 조회 (웹 전용)
			/*else {
				layout.setBanners(commonDao.selectEventPromotionList(EventChannelType.WEB_RESIDENT.getCode()));
				if (layout.getBanners() != null) {
					for (EventPromotion banner : layout.getBanners()) {
						// 배너 유형중에 견적현황 배너가 포함된 경우
						if (banner.getLanding_type().equalsIgnoreCase("S")) {
							// 견적 현황 정보 설정
							EstimateSummary summary = estimateDao.selectEstimateSummary();
							summary.setDoing_count(null);
							layout.setEstimate(summary);
							break;
						}
					}
				}
			}*/

			// 주거공간 공간분류 조회
			searchCondition.put("layout", LayoutType.PARTIAL.getCode());
			List<LayoutHome> homeList = homeLayoutDao.selectHomeLayout(searchCondition);
			if (homeList == null || homeList.size() == 0) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 공간사진정보 미설정.");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Resident-Partial");
			}

			// 부분사진 태그 리스트 (FILTER_1 에 공간분류 목록이 포함됨)
			array = StringUtils.split(((temp = homeList.get(0).getAdditional_1()) == null ? "" : temp), " ,");
			if (array == null || array.length < 3) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 주거공간의 공간분류[RESIDENT_PARTIAL] 설정 오류. 3개이상 설정 필수");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Resident-Partial Tags");
			}

			// 부분사진 태그 설정
			layout.setPartials(getLayoutPartial(array, layoutTitle));

			// 앱인 경우 퀵필터 정보 설정
			if (agentType == AgentType.APP) {

                // 퀵 필터 설정 정보 조회
                searchCondition.put("layout", LayoutType.QUICK_FILTER.getCode());
                List<LayoutHome> filterList = homeLayoutDao.selectHomeLayout(searchCondition);
                if (filterList == null || filterList.size() < 2) {
                    log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 퀵필터 정보 설정 오류. 2개이상 설정 필수.");
                    throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Resident-Filter");
                }

                // 퀵 필터 설정
                layout.setFilters(getLayoutFilter(filterList, layoutTitle));
            }

            // 웹은 시공사례 및 견적현황 조회
            else {

                // 필터 시공사례 설정 정보 조회
                searchCondition.put("layout", LayoutType.GALLERY_FILTER.getCode());
                List<LayoutHome> galleryList = homeLayoutDao.selectHomeLayout(searchCondition);
                if (galleryList == null || galleryList.size() < 2) {
                    log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 시공사례 정보 설정 오류. 2개이상 설정 필수.");
                    throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Resident-Gallery");
                }

                // 시공사례 필터 설정
                layout.setFilter_gallery(getLayoutFilterGallery(galleryList, category, layoutTitle));

                // 견적신청 현황 조회
				layout.setEstimate_list(estimateDao.selectWebMainList());
            }
			return layout;
		}

		// 배너 조회 (앱 전용)
		if (agentType == AgentType.APP) {
			layout.setBanners(commonDao.selectEventPromotionList(EventChannelType.APP_COMMERCE.getCode()));
		} else {
			layout.setBanners(commonDao.selectEventPromotionList(EventChannelType.WEB_COMMERCE.getCode()));
		}

		// 상업공간 시공사례 정보 조회
		searchCondition.put("layout", LayoutType.GALLERY.getCode());
		List<LayoutHome> galleryList = homeLayoutDao.selectHomeLayout(searchCondition);
		if (galleryList == null || galleryList.size() == 0) {
			log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 상업공간 시공사례 정보 미설정.");
			throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Commerce-Gallery");
		}

		// 시공사례 구성
		for (LayoutHome layoutHome : galleryList) {

			// 시공사례 표시 개수
			if (StringUtils.isEmpty(layoutHome.getAdditional_2())) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 상업공간의 시공사례항목[COMMERCE_GALLERY]의 사례 갯수 미설정.");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Commerce-Gallery's Count");
			}

			Integer gallery_cnt = 4;
			try {
				gallery_cnt = Integer.parseInt(layoutHome.getAdditional_2());
			} catch (Exception e) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 상업공간의 시공사례항목[COMMERCE_GALLERY]의 사례 갯수 설정 오류 ["+layoutHome.getAdditional_2()+"]");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Commerce-Gallery's Count");
			}

			// 시공사례 항목 리스트 (ADDITIONAL_1에 CATEGORY 포함됨)
			array = StringUtils.split(((temp = layoutHome.getAdditional_1()) == null ? "" : temp), " ,");
			if (array == null || array.length == 0) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면에 표시할 상업공간의 시공사례항목[COMMERCE_GALLERY]의 카테고리 미설정.");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Commerce-Gallery Category");
			}

			layout.setGallery(getCommerceLayoutGallery(array, layoutTitle, gallery_cnt));
		}

		// 필터 시공사례 설정 정보 조회
		searchCondition.put("layout", LayoutType.GALLERY_FILTER.getCode());
		List<LayoutHome> filter_gallery = homeLayoutDao.selectHomeLayout(searchCondition);

		// 시공사례 필터 설정
		if (filter_gallery != null && filter_gallery.size() > 0) {
			layout.setFilter_gallery(getLayoutFilterGallery(filter_gallery, category, layoutTitle));
		}

		return layout;
	}

	@Override
	public HomeScreenLayout getHomeScreenLayoutForPartners(AgentType agentType) throws Exception {

		HomeScreenLayout layout = new HomeScreenLayout();

		// 배너 조회 (웹 전용)
		if (agentType == AgentType.WEB) {
			layout.setBanners(commonDao.selectEventPromotionList(EventChannelType.WEB_PARTNERS.getCode()));
		}

		return layout;
	}

	@Override
	public HomeScreenLayout getHomeScreenLayoutForMagazine(AgentType agentType) throws Exception {

		HomeScreenLayout layout = new HomeScreenLayout();

		List<EventPromotion> banners = commonDao.selectEventPromotionList(EventChannelType.WEB_MAGAZINE.getCode());
		if (banners != null) {
			List<EventPromotion> u_banners = new ArrayList<>();
			List<EventPromotion> m_banners = new ArrayList<>();
			for (EventPromotion banner : banners) {
				if (banner.getPromotion_type().equals("B")) {
					u_banners.add(banner);
				} else if (banner.getPromotion_type().equals("M")) {
					m_banners.add(banner);
				}
			}
			// 상단 배너 조회 (웹 전용)
			if (u_banners.size() > 0) { layout.setBanners(u_banners); }

			// 중간 배너 조회 (웹 전용)
			if (m_banners.size() > 0) { layout.setMagazine_banners(m_banners); }
		}

		// 추천 매거진 표시명
		Map<String, Object> searchCondition = new HashMap<>();
		searchCondition.put("category", LayoutCategory.MAGAZINE.getCode());

		// 문구 조회
		List<LayoutConfig> configList = homeLayoutDao.selectHomeLayoutText(searchCondition);
		if (configList == null || configList.size() == 0) {
			log.error("[HOME-LAYOUT-MAGAZINE] 메인화면에 표시할 추천 매거진 제목 미설정.");
			throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "No Layout-Title");
		}
		layout.setBest_magazine_subject(configList.get(0).getConfig_title());

		// 추천 매거진 표시명
		MagazineListRequest searchMagazine = new MagazineListRequest();
		searchMagazine.setAgent(agentType.getCode());

		// 추천 매거진
		layout.setBest_magazines(boardDao.selectBestMagazine(searchMagazine));

        // 최신 매거진
		layout.setMagazines(boardDao.selectRecentMagazine(searchMagazine));

        // 뉴스 조회
		layout.setNews(boardDao.selectRecentNews(searchMagazine));

		// 인테리어 팁 조회
		layout.setTip_list(boardDao.selectRecentMagazineType(MagazineType.INTERIOR_GUIDE.getCode()));

		/* 집닥맨 현장 관리 */
		layout.setZipman_manage_list(boardDao.selectRecentMagazineType(MagazineType.ZIPDOCMAN.getCode()));

		/* 인테리어 트랜드 조회 */
		layout.setTrend_list(boardDao.selectRecentMagazineType(MagazineType.TREND.getCode()));

		return layout;
	}

	@Override
	public HomeScreenLayout getHomeScreenLayoutForBenefit(AgentType agentType) throws Exception {

		HomeScreenLayout layout = new HomeScreenLayout();

		// 배너 조회 (웹 전용)
		layout.setBanners(commonDao.selectEventPromotionList(EventChannelType.WEB_BENEFIT.getCode()));

		// 진행중인 최근 이벤트 3개 조회
		ListRequest searchCondition = new ListRequest();
		searchCondition.setAgent(agentType.getCode());
		searchCondition.setPageLimit(0, 3);

		layout.setEvents(boardDao.searchEventList(searchCondition));

		return layout;
	}

	private List<LayoutPartial> getLayoutPartial(String [] tags, Map<String,LayoutConfig> titleMap) throws ServiceException {

		List<LayoutPartial> partials = new ArrayList<>();

		// 메인 태그의 순서 조정. 랜덤
		int random_type = (int)(System.currentTimeMillis() % 6);
		switch (random_type) {
			case 1:
				partials.add(new LayoutPartial(tags[0]));
				partials.add(new LayoutPartial(tags[1]));
				partials.add(new LayoutPartial(tags[2]));
				break;
			case 2:
				partials.add(new LayoutPartial(tags[0]));
				partials.add(new LayoutPartial(tags[2]));
				partials.add(new LayoutPartial(tags[1]));
				break;
			case 3:
				partials.add(new LayoutPartial(tags[1]));
				partials.add(new LayoutPartial(tags[0]));
				partials.add(new LayoutPartial(tags[2]));
				break;
			case 4:
				partials.add(new LayoutPartial(tags[1]));
				partials.add(new LayoutPartial(tags[2]));
				partials.add(new LayoutPartial(tags[0]));
				break;
			case 5:
				partials.add(new LayoutPartial(tags[2]));
				partials.add(new LayoutPartial(tags[0]));
				partials.add(new LayoutPartial(tags[1]));
				break;
			default:
				partials.add(new LayoutPartial(tags[2]));
				partials.add(new LayoutPartial(tags[1]));
				partials.add(new LayoutPartial(tags[0]));
		}

		// 하위 태그 랜덤 1개 추가
		if (tags.length > 3) {
			Random r = new Random();
			partials.add(new LayoutPartial(tags[r.nextInt(tags.length - 3) + 3]));
		}

		// 표시 문구 설정
		for (LayoutPartial partial : partials) {
			LayoutConfig config = titleMap.get(partial.getTag());
			if (config == null) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면/주거공간의 태그["+partial.getTag()+"]의 표시문구 미설정");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Resident-Partial Text");
			}
			partial.setTitle(config.getConfig_title());
		}

		return partials;
	}

    private List<LayoutGallery> getLayoutFilterGallery(List<LayoutHome> layoutList, CategoryCode category, Map<String,LayoutConfig> titleMap) throws ServiceException {

        List<LayoutGallery> gallery = new ArrayList<>();

        for (LayoutHome layout : layoutList) {

            // 공간 정보 조회 (FILTER_1)
            LayoutConfig categoryConfig = titleMap.get(layout.getAdditional_1());
            if (categoryConfig == null) {
                log.error("[HOME-LAYOUT-SERVICE] 메인화면/"+LayoutCategory.get(layout.getCategory()).getName()+"의 ["+layout.getAdditional_1()+"]의 표시문구 미설정");
                throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Filter Text");
            }

            // 메인필터 정보 조회
            LayoutConfig mainFilterConfig = titleMap.get(layout.getAdditional_2());
            if (mainFilterConfig == null) {
                log.error("[HOME-LAYOUT-SERVICE] 메인화면/"+LayoutCategory.get(layout.getCategory()).getName()+"의 ["+layout.getAdditional_2()+"] 정보 미설정");
                throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Filter Text");
            }

            // 카데고리 설정
			LayoutGallery galleryItem = new LayoutGallery(categoryConfig.getAdditional_1());
            galleryItem.setCategory_name(categoryConfig.getDisplayOrConfig_name());

            // 필터 설정
			galleryItem.setFilter(layout.getAdditional_2());

			// 필터 구분자 설정
			galleryItem.setFilter_type(mainFilterConfig.getConfig_type());

            // 문구 설정
            galleryItem.setTitle(String.format(categoryConfig.getConfig_title(), layout.getAdditional_2()));

            // 검색옵션
			ProductListRequest condition = new ProductListRequest();

			// 주거/상업 여부
			condition.setCategory_code1(category.getCode());

			// 아파트/빌라 등
			List<String> category_2 = new ArrayList<>();
			category_2.add(categoryConfig.getAdditional_1());
			condition.setCategory_code2(category_2);

			// 필터 검색
			setFilterCondition(condition, mainFilterConfig);

			// 필터 구분이 평형인 경우 최소/최대값 설정
			if (galleryItem.getFilter_type() == LayoutConfigType.SPACE.getCode()) {
				galleryItem.setSpace_min(condition.getSpace_min());
				galleryItem.setSpace_max(condition.getSpace_max());
			}

			// 검색옵션 설정
			galleryItem.setSearchCondition(condition);

            gallery.add(galleryItem);
        }

        return gallery;
    }

	private List<LayoutGallery> getCommerceLayoutGallery(String [] tags, Map<String,LayoutConfig> titleMap, Integer gallery_cnt) throws ServiceException {

		List<LayoutGallery> gallery = new ArrayList<>();

		// 첫번째는 고정
		gallery.add(new LayoutGallery(tags[0]));

		if (tags.length > 1) {

			int random_count = tags.length - 1;
			random_count = random_count > 3 ? 3 : random_count; // 최대 3개까지만 지정

			int idx[] = new int[random_count];
			Random r = new Random();

			// 두번째부터는 랜덤 3개 설정
			for (int i = 0; i < random_count; i++) {
				idx[i] = r.nextInt(tags.length - 1) + 1; // 1~N중 하나 선택
				for (int j = 0; j < i; j++) {
					if (idx[i] == idx[j]) {
						i--;
					}
				}
			}

			// 랜덤숫자대로 태그 등록
			for (int i = 0; i < random_count; i++) {
				gallery.add(new LayoutGallery((tags[idx[i]])));
			}
		}

		// 표시 문구 설정
		for (LayoutGallery item : gallery) {
			LayoutConfig config = titleMap.get(item.getCategory());
			if (config == null) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면/상업공간의 태그["+item.getCategory()+"]의 표시문구 미설정");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Commerce-Gallery Text");
			}
			item.setCategory(config.getAdditional_1());
			item.setCategory_name(config.getConfig_name());
			item.setTitle(config.getConfig_title());

			ProductListRequest condition = new ProductListRequest();
			condition.setCategory_code1(CategoryCode.COMMERCE.getCode());

			List<String> category2 = new ArrayList<>();
			category2.add(item.getCategory());
			condition.setCategory_code2(category2);

			// 표시할 시공사례 수 설정
			condition.setLimit(gallery_cnt);

			// 시공사례 검색 옵션 설정
			item.setSearchCondition(condition);
		}

		return gallery;
	}

	private List<LayoutQuickFilter> getLayoutFilter(List<LayoutHome> filterList, Map<String,LayoutConfig> titleMap) throws ServiceException {

		// 퀵 필터 설정 정보 조회
		List<LayoutQuickFilter> filters = new ArrayList<>();

		List<LayoutHome> randomList = null;

		// 첫번째는 고정. 3개 이상인 경우 두번째 이후부터 랜덤 1개 선택
		if (filterList.size() > 2) {

			randomList = new ArrayList<>();
			randomList.add(filterList.get(0));

			Random r = new Random();
			randomList.add(filterList.get(r.nextInt(filterList.size()-1)+1));
		} else {
			randomList = filterList;
		}

		for (LayoutHome layout : randomList) {

            // 퀵필터 타이틀 조회
            LayoutConfig categoryConfig = titleMap.get(layout.getAdditional_1());
            if (categoryConfig == null) {
                log.error("[HOME-LAYOUT-SERVICE] 메인화면/"+LayoutCategory.get(layout.getCategory()).getName()+"의 ["+layout.getAdditional_1()+"] 표시문구 미설정");
                throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Filter Text");
            }
            // CATEGORY_1 설정
			LayoutQuickFilter filter = new LayoutQuickFilter(categoryConfig.getAdditional_1());
            filter.setCategory_name(categoryConfig.getConfig_name());

            // 퀵필터 - 메인필터 정보 조회
			LayoutConfig mainFilterConfig = titleMap.get(layout.getAdditional_2());
			if (mainFilterConfig == null) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면/"+LayoutCategory.get(layout.getCategory()).getName()+"의 ["+layout.getAdditional_2()+"] 정보 미설정");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Filter Text");
			}
			// 메인필터 설정
			filter.setFilter(mainFilterConfig.getDisplayOrConfig_name());

			// 메인필터의 구분자 설정
			filter.setFilter_type(mainFilterConfig.getConfig_type());

			// 메인필터 문구 설정
			filter.setTitle(String.format(categoryConfig.getConfig_title(), filter.getFilter()));

			// 퀵필터 항목 확인 (FILTER_3)
			String temp;
			String [] array = StringUtils.split(((temp = layout.getAdditional_3()) == null ? "" : temp), " ,");
			if (array == null || array.length == 0) {
				log.error("[HOME-LAYOUT-SERVICE] 메인화면/"+LayoutCategory.get(layout.getCategory()).getName()+"의 ["+filter.getCategory()+"]의 퀵필터 미설정");
				throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Quick-Filter");
			}

			List<QuickFilter> items = new ArrayList<>();

			// 퀵 필터 설정
			for (int i = 0, n = array.length; i < n; i++) {
				QuickFilter item = new QuickFilter();

				// 문구 및 URL 설정
				LayoutConfig itemConfig = titleMap.get(array[i]);
				if (itemConfig == null) {
					log.error("[HOME-LAYOUT-SERVICE] 메인화면/"+LayoutCategory.get(layout.getCategory()).getName()+"의 ["+array[i]+"] 표시문구 미설정");
					throw new ServiceException(HttpStatus.SERVICE_UNAVAILABLE.value(), "Unavailable Filter Text");
				}

				// 퀵필터 표시명
                item.setFilter(itemConfig.getDisplayOrConfig_name());
                item.setCategory(filter.getCategory());
                item.setMain_filter(filter.getFilter());

				// 퀵필터 문구 설정
				item.setTitle(itemConfig.getConfig_title());

				// 퀵필터 구분자 설정
				item.setFilter_type(itemConfig.getConfig_type());

				// 퀵필터 이미지 설정
				item.setThumbnail_url(itemConfig.getImage_url());

				// 시공사례 검색조건 설정
				ProductListRequest condition = new ProductListRequest();

				// 주거/상업 여부
				if (layout.getCategory() == LayoutCategory.RESIDENT.getCode()) {
					condition.setCategory_code1(CategoryCode.RESIDENT.getCode());
				} else {
					condition.setCategory_code1(CategoryCode.COMMERCE.getCode());
				}

				// 주거/상업에 따른 2차분류
				List<String> category2 = new ArrayList<>();
				category2.add(categoryConfig.getAdditional_1());
				condition.setCategory_code2(category2);

				// 메인필터 설정
				setFilterCondition(condition, mainFilterConfig);

				// 필터 구분이 평형인 경우 최소/최대값 설정
				if (filter.getFilter_type() == LayoutConfigType.SPACE.getCode()) {
					filter.setSpace_min(condition.getSpace_min());
					filter.setSpace_max(condition.getSpace_max());
				}

				// 퀵필터 설정
				setFilterCondition(condition, itemConfig);

				// 퀵필터 구분이 평형인 경우 최소/최대값 설정
				if (item.getFilter_type() == LayoutConfigType.SPACE.getCode()) {
					item.setSpace_min(condition.getSpace_min());
					item.setSpace_max(condition.getSpace_max());
				}

				// 시공사례 개수 조회
				item.setTotal_cnt(productDao.selectListTotalCount(condition));

				items.add(item);
			}
			filter.setList(items);
            filters.add(filter);
        }

        return filters;
	}

	private void setFilterCondition(ProductListRequest condition, LayoutConfig config) {

		// 평수 (메인필터)
		if (config.getConfig_type() == LayoutConfigType.SPACE.getCode()) {

			// 평수 최소값
			if (StringUtils.isNotEmpty(config.getAdditional_1())) {
				condition.setSpace_min(new Integer(config.getAdditional_1()));
			}

			// 평수 최대값
			if (StringUtils.isNotEmpty(config.getAdditional_2())) {
				condition.setSpace_max(new Integer(config.getAdditional_2()));
			}
		}

		// 색상 (메인필터)
		else if (config.getConfig_type() == LayoutConfigType.COLOR.getCode()) {
			List<String> color = new ArrayList<>();
			color.add(config.getDisplayOrConfig_name());
			condition.setColors(color);
		}

		// 스타일 (메인필터)
		else if (config.getConfig_type() == LayoutConfigType.STYLE.getCode()) {
			List<String> style = new ArrayList<>();
			style.add(config.getDisplayOrConfig_name());
			condition.setStyles(style);
		}
	}

	private List<PopularTag> getPopularTags(List<LayoutHome> layoutList, Map<String,String> urlMap) throws ServiceException {

		ArrayList<PopularTag> tags = new ArrayList<>();

		LayoutHome config = layoutList.get(0);
		ArrayList<String> filter1 =  new ArrayList<String>(Arrays.asList(StringUtils.split(config.getAdditional_1(), Constants.TAG_DELIMITER)));

		Collections.shuffle(filter1);

		for (String tag : filter1) {
			String url = urlMap.get(tag);
			if (url == null) continue;
			tags.add(new PopularTag(tag, url));
			if (tags.size() == 6) return tags;
		}
		return tags;
	}
}



package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ZzimService;
import com.zipdoc.acs.model.ZzimListRequest;
import com.zipdoc.acs.persistence.dao.ProductDao;
import com.zipdoc.acs.persistence.dao.ZzimDao;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 이동규 on 2016-06-13.
 */
@Service
public class ZzimServiceImpl implements ZzimService {

    @Autowired
    private ZzimDao zzimDao;

    @Autowired
    private ProductDao productDao;

    @Override
    public int selectZzimListTotalCount(ZzimListRequest searchCondition) throws DataAccessException {
        return zzimDao.selectZzimListTotalCount(searchCondition);
    }

    @Override
    public List<Product> selectZzimList(ZzimListRequest searchCondition) throws DataAccessException {
        return zzimDao.selectZzimList(searchCondition);
    }

    @Override
    public void createZzim(Zzim zzim) throws DataAccessException {
        int count = zzimDao.selectZzimCount(zzim);
        if (count == 0) {
            zzimDao.createZzim(zzim);
            productDao.updateIncreaseZzimCnt(zzim.getPid());
        } else {
            throw new ServiceException(HttpStatus.SC_CONFLICT, "이미 찜목록에 존재합니다.");
        }
    }

    @Override
    public void deleteZzim(Zzim zzim) throws DataAccessException {
        int row = zzimDao.deleteZzim(zzim);
        if(row == 1){
            productDao.updateDecreaseZzimCnt(zzim.getPid());
        }
    }

    @Override
    public void deleteZzimAll(Zzim zzim) throws DataAccessException {
        zzimDao.deleteZzimAll(zzim);
    }

    @Override
    public void deleteZzimMulti(Zzim zzim) throws DataAccessException {
        int row = zzimDao.deleteZzimMulti(zzim);
    }

    @Override
    public List<Long> selectZzimPidList(Long member_no, int product_type) throws DataAccessException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("member_no", member_no);
        map.put("product_type", product_type);
        return zzimDao.selectZzimPidList(map);
    }

    @Override
    public ZzimBook selectZzimBook(long book_no) throws DataAccessException {
        return zzimDao.selectZzimBook(book_no);
    }

    @Override
    public int selectZzimBookListTotalCount(ZzimListRequest searchCondition) throws DataAccessException {
        return zzimDao.selectZzimBookListTotalCount(searchCondition);
    }

    @Override
    public List<ZzimBook> selectZzimBookList(ZzimListRequest searchCondition) throws DataAccessException {
        return zzimDao.selectZzimBookList(searchCondition);
    }

    @Override
    public void createZzimBook(ZzimBook zzimBook) throws DataAccessException {
        zzimDao.createZzimBook(zzimBook);
    }

    @Override
    public void deleteZzimBook(ZzimBook zzimBook) throws DataAccessException {
        zzimDao.deleteZzimBook(zzimBook);
        zzimDao.deleteZzimByBookNo(zzimBook);
    }

    @Override
    public int selectZzimItemListTotalCount(ZzimListRequest searchCondition) throws DataAccessException {
        return zzimDao.selectZzimItemListTotalCount(searchCondition);
    }

    @Override
    public List<Item> selectZzimItemList(ZzimListRequest searchCondition) throws DataAccessException {
        return zzimDao.selectZzimItemList(searchCondition);
    }
}

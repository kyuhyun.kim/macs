package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.Customer;
import com.zipdoc.acs.domain.entity.CustomerPartnerInquiry;
import com.zipdoc.acs.domain.entity.CustomerReply;

import java.util.List;
import java.util.Map;

public interface CustomerService {

	// 1:1 문의하기 내역 조회
	int findListCount(Map<String, Object> condition) throws ServiceException;
	List<Customer> findList(Map<String, Object> condition) throws ServiceException;
	Customer findById(Map<String, Object> condition) throws ServiceException;

	// 1:1 문의하기 등록/수정/삭제
	void insert(Customer customer) throws ServiceException;
	int update(Customer customer) throws ServiceException;
	int delete(int cno) throws ServiceException;

	// 1:1 문의하기 댓글 조회
	List<CustomerReply> findReplyList(int cno) throws ServiceException;

	// 1:1 문의하기 댓글 등록/수정/삭제
	void insertReply(CustomerReply customerReply) throws ServiceException;
	int updateReply(CustomerReply customerReply) throws ServiceException;
	int deleteReply(int reply_no) throws ServiceException;

	// 파트너 문의 등록
	void insertPartnerInquiry(CustomerPartnerInquiry inquiry) throws ServiceException;
}

package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.Item;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.Partner;
import com.zipdoc.acs.domain.entity.Whatever;
import com.zipdoc.acs.domain.service.WhateverService;
import com.zipdoc.acs.model.ItemListRequest;
import com.zipdoc.acs.model.WhateverListRequest;
import com.zipdoc.acs.persistence.dao.CommentDao;
import com.zipdoc.acs.persistence.dao.WhateverDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 이동규 on 2017-03-08.
 */
@Service
public class WhateverServiceImpl implements WhateverService {

    @Autowired
    private WhateverDao whateverDao;

    @Override
    public List<Whatever> selectList(WhateverListRequest searchCondition) throws DataAccessException {
        return whateverDao.selectList(searchCondition);
    }

    @Override
    public int selectListTotalCount(WhateverListRequest searchCondition) throws DataAccessException {
        return whateverDao.selectListTotalCount(searchCondition);
    }

    @Override
    public Whatever selectById(long cno) throws DataAccessException {
        return whateverDao.selectById(cno);
    }

    @Override
    public void create(Whatever whatever) throws DataAccessException {
        whateverDao.create(whatever);
    }

    @Override
    public void delete(long cno) throws DataAccessException {
        whateverDao.deleteReply(cno);
        whateverDao.delete(cno);
    }

    @Override
    public void updateIncreaseViewCnt(long cno) throws DataAccessException {
        whateverDao.updateIncreaseViewCnt(cno);
    }

    @Override
    public void deleteMulti(Whatever target) throws DataAccessException {
        for(Long cno:target.getArticle_no_list()){
            delete(cno);
        }
    }

    @Override
    public void deleteAllByMemberNo(Long member_no) throws DataAccessException {
        WhateverListRequest searchCondition = new WhateverListRequest();
        searchCondition.setMember_no(member_no);
        searchCondition.setOwner(1);

        int totalCount = selectListTotalCount(searchCondition);
        searchCondition.setPageLimit(0, totalCount);

        List<Whatever> myList = selectList(searchCondition);

        for(Whatever whatever:myList){
            delete(whatever.getCno());
        }
    }
}

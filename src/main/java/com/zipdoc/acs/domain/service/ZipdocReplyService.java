package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.ZipdocReply;
import com.zipdoc.acs.model.ZipdocReplyListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface ZipdocReplyService {
	public List<ZipdocReply> selectList(ZipdocReplyListRequest searchCondition) throws DataAccessException;
	public int selectListTotalCount(ZipdocReplyListRequest searchCondition) throws DataAccessException;
	public ZipdocReply selectById(long cno) throws DataAccessException;
	public void create(ZipdocReply zipdocReply) throws DataAccessException;
	public void delete(long cno) throws DataAccessException;
	public void updateIncreaseViewCnt(long cno) throws DataAccessException;

	public void deleteMulti(ZipdocReply target) throws DataAccessException;
	public void deleteAllByMemberNo(Long member_no) throws DataAccessException;
}

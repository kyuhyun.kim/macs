package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.MemberDevice;

import java.util.List;

/**
 * Created by ZIPDOC on 2016-10-06.
 */
public interface SendMessageService {
    public List<MemberDevice> findListByPartnerId(int partner_id) throws ServiceException;
    public List<MemberDevice> findListByMobileNo(String phone_no) throws ServiceException;
    public List<MemberDevice> findListByMemberNo(long member_no) throws ServiceException;
    public List<MemberDevice> findListByMemberId(String member_id) throws ServiceException;
    public void updateExpiredToken(Long zid) throws ServiceException;

}

package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.MemberDevice;
import com.zipdoc.acs.domain.service.SendMessageService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.persistence.dao.SendMessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SendMessageServiceImpl implements SendMessageService {

	@Autowired
	private SendMessageDao sendMessageDao;

	@Override
	public List<MemberDevice> findListByPartnerId(int partner_id) throws ServiceException {
		return sendMessageDao.findListByPartnerId(partner_id);
	}

	@Override
	public List<MemberDevice> findListByMobileNo(String phone_no) throws ServiceException {
		return sendMessageDao.findListByMobileNo(phone_no);
	}

	@Override
	public List<MemberDevice> findListByMemberNo(long member_no) throws ServiceException {
		return sendMessageDao.findListByMemberNo(member_no);
	}

	@Override
	public List<MemberDevice> findListByMemberId(String member_id) throws ServiceException {
		return sendMessageDao.findListByMemberId(member_id);
	}

	@Override
	public void updateExpiredToken(Long zid) throws ServiceException {
		sendMessageDao.updateExpiredToken(zid);
	}
}

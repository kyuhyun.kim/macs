package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.define.EventChannelType;
import com.zipdoc.acs.define.FaqType;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.CommonService;
import com.zipdoc.acs.model.ServiceInfo;
import com.zipdoc.acs.persistence.dao.ArticleDao;
import com.zipdoc.acs.persistence.dao.CommonDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CommonServiceImpl implements CommonService {

	private static final Logger log = LoggerFactory.getLogger(CommonServiceImpl.class);
	
	@Autowired
	private CommonDao commonDao;

	@Autowired
	private ArticleDao articleDao;

	@Autowired
	private RegionHolder regionHolder;

	@Override
	public ServiceInfo selectServiceInfo() throws DataAccessException {

		ServiceInfo serviceInfo = new ServiceInfo();

		// 키워드정보
		List<RecommendKeyword> keywords = commonDao.selectRecommendKeywordList();
		serviceInfo.setKeyword_info(keywords);

		// 프로모션정보 (삭제)
//		List<EventPromotion> promotions = commonDao.selectEventPromotionList(EventChannelType.APP.getCode());
//		serviceInfo.setPromotion_info(promotions);

		// 지역정보
		serviceInfo.setRegion_info(regionHolder.getRegions());

		// 글정보
		serviceInfo.setArticle_info(articleDao.selectArticleCreateDateInfo());

		// 카테고리정보
		serviceInfo.setCategory_info(commonDao.selectCategoryList());

		//인테리어상품코드 정보
		serviceInfo.setInterior_product_info(commonDao.selectInteriorProductList());

		//공통코드 정보
		serviceInfo.setCode_info(commonDao.selectCodeList());

		// 색상 정보
		serviceInfo.setColor_info(commonDao.selectColorList());

		// FAQ 카테고리 정보
		FaqType [] list = FaqType.values();
		if (list != null) {
			List<CodeValue> faqList = new ArrayList<>();
			for (int i = 0; i < list.length; i++) {
				faqList.add(new CodeValue(list[i].getCode()+"", list[i].getName()));
			}
			serviceInfo.setFaq_category_info(faqList);
		}

		return serviceInfo;
	}

	@Override
	public ServiceInfo selectPartnerSerivceInfo() throws DataAccessException {
		ServiceInfo serviceInfo = new ServiceInfo();
		//카테고리정보
		serviceInfo.setCategory_info(commonDao.selectCategoryListForPartner());

		//인테리어상품코드 정보
		serviceInfo.setInterior_product_info(commonDao.selectInteriorProductList());

		//프로모션정보
		List<EventPromotion> promotions = commonDao.selectEventPromotionList(EventChannelType.PARTNER_APP.getCode());
		serviceInfo.setPromotion_info(promotions);

		return serviceInfo;
	}

	@Override
	public InteriorProduct selectInteriorProduct(int product_code) throws DataAccessException {
		return commonDao.selectInteriorProduct(product_code);
	}

	@Override
	public ServiceInfo selectCmsServiceInfo() throws DataAccessException {
		ServiceInfo serviceInfo = new ServiceInfo();

		//지역정보
		serviceInfo.setRegion_info(regionHolder.getRegions());

		//파트너스 계약상품 정보
		serviceInfo.setPartner_product_info(commonDao.selectPartnerProducts());

		return serviceInfo;
	}

	@Override
	public String selectCategoryName(String category_code) throws DataAccessException {
		return commonDao.selectCategoryName(category_code);
	}

	@Override
	public RecommendKeyword selectKeywordList(String keyword_type) throws DataAccessException {
		return commonDao.selectKeywordList(keyword_type);
	}
}



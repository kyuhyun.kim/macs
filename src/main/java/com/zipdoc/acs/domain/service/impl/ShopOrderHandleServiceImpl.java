package com.zipdoc.acs.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zipdoc.acs.domain.entity.ShopOrderHandle;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopOrderHandleService;
import com.zipdoc.acs.persistence.dao.ShopOrderGoodsDao;
import com.zipdoc.acs.persistence.dao.ShopOrderHandleDao;

@Service
public class ShopOrderHandleServiceImpl implements ShopOrderHandleService{

	@Autowired
	private ShopOrderHandleDao shopOrderHandleDao;
	
	@Autowired
	private ShopOrderGoodsDao shopOrderGoodsDao;
	
	@Override
	public void create(ShopOrderHandle shopOrderHandle) throws ServiceException {
		shopOrderHandleDao.create(shopOrderHandle);
		
		Long goods_sno = shopOrderHandle.getOrder_goods_sno();
		
		if(goods_sno >0 && goods_sno !=null){
			shopOrderGoodsDao.updateOrderStatus(goods_sno);
		}
	}

	@Override
	public ShopOrderHandle findById(long sno) throws ServiceException {
		return shopOrderHandleDao.findById(sno);
	}

	@Override
	public void update(ShopOrderHandle shopOrderHandle) throws ServiceException {
		shopOrderHandleDao.update(shopOrderHandle);
	}

}

package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.*;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface MemberService {

	long findNextID() throws DataAccessException;
	
	int createDevice(Member member) throws DataAccessException;

	Member selectDeviceByDeviceId(String device_id, int app_type) throws DataAccessException;
	
	Member selectMember(Long member_id, long zid) throws DataAccessException;

	Member selectMemberByZid(long zid) throws DataAccessException;

	Member selectMemberByWebToken(String token) throws DataAccessException;

	Member selectMemberByMemberId(long member_id) throws DataAccessException;

	Member selectMemberByForgotAccount(ForgotAccount account) throws DataAccessException;
	
	Member selectMemberByAccount(Account account) throws DataAccessException;
	
	int updateDeviceUserLevel(Member member) throws DataAccessException;
	
	boolean IsExistAccount(String account) throws DataAccessException;

	int updateToken(Member member) throws DataAccessException;

	void treatMember(Member member) throws DataAccessException;

	void treatMemberDevice(Member member) throws DataAccessException;

	void updateMemberLoginInfo(Member accountMember) throws DataAccessException;

	void updateDevice(Member member) throws DataAccessException;

	MyPage selectMyPageSummary(Member member) throws DataAccessException;

	Profile selectProfile(long member_no) throws DataAccessException;

	void updateProfile(Member member) throws DataAccessException;

	void updateNickname(Member member) throws DataAccessException;

	int selectCountUsingNicknameCount(String nickname) throws DataAccessException;

	void updateDeviceAgree(Member member) throws DataAccessException;

	void updateDeviceAgreePrivacy(Member member) throws DataAccessException;

	void deleteMember(Member member) throws DataAccessException;

	ZipdocmanUser selectZipdocmanUser(long member_no) throws DataAccessException;

	int updateMember(Account account) throws DataAccessException;

	void updateMemberAlarm(AlarmInfo alarmInfo) throws DataAccessException;

	AlarmInfo selectMemberAlarm(Member member)throws DataAccessException;

	AlarmInfo selectDeviceAlarm(Member member)throws DataAccessException;

	void updateDeviceAlarm(AlarmInfo alarmInfo) throws DataAccessException;
}

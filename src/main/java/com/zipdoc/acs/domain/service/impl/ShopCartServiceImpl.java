package com.zipdoc.acs.domain.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zipdoc.acs.domain.entity.ShopCart;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopCartService;
import com.zipdoc.acs.model.shop.ShopCartRequest;
import com.zipdoc.acs.persistence.dao.ShopCartDao;

@Service
public class ShopCartServiceImpl implements ShopCartService{

	@Autowired
	private ShopCartDao shopCartDao;
	
	@Override
	public List<ShopCart> findList(ShopCartRequest condition) throws ServiceException {
		return shopCartDao.findList(condition);
	}

	@Override
	public int findListCount(ShopCartRequest condition) throws ServiceException {
		return shopCartDao.findListCount(condition);
	}

	@Override
	public void create(ShopCart shopCart) throws ServiceException {
		shopCartDao.create(shopCart);
	}

	@Override
	public void delete(Map<String, Object> condition) throws ServiceException {
		shopCartDao.delete(condition);
	}

	@Override
	public ShopCart findById(Map<String, Object> condition) throws ServiceException {
		return shopCartDao.findById(condition);
	}

	@Override
	public void updateCnt(Map<String, Object> condition) throws ServiceException {
		shopCartDao.updateCnt(condition);
	}

	@Override
	public void deleteCartAll(Map<String, Object> condition) throws ServiceException {
		shopCartDao.deleteCartAll(condition);
	}

	@Override
	public void updateOption(Map<String, Object> condition) throws ServiceException {
		shopCartDao.updateOption(condition);
	}

	@Override
	public void deleteCartMulti(ShopCart cart) throws ServiceException {
		shopCartDao.deleteCartMulti(cart);
	}
}

package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.ShopOrderHandle;

public interface ShopOrderHandleService {

	void create(ShopOrderHandle shopOrderHandle) throws ServiceException;

	ShopOrderHandle findById(long sno) throws ServiceException;

	void update(ShopOrderHandle shopOrderHandle) throws ServiceException;

}

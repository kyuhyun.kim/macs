package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.ShopPartnerReq;

public interface ShopPartnerService {
	void create(ShopPartnerReq shopPartnerReq) throws ServiceException;
}

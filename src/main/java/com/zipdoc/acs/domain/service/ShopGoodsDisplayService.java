package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.ShopGoods;

import java.util.List;

/**
 * Created by dskim on 2017. 2. 22..
 */
public interface ShopGoodsDisplayService {


    List<ShopGoods> bestGoodsList() throws ServiceException;

    //2017-03-07 신상품
    List<ShopGoods> newGoodsList() throws ServiceException;


    //2017-03-20 패키지상품 상품타입이 시공인것만
    List<ShopGoods> packageGoodsList() throws ServiceException;
}

package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.ItemListRequest;
import com.zipdoc.acs.model.ProductListRequest;
import com.zipdoc.acs.model.ZzimListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface ZzimService {
	int selectZzimListTotalCount(ZzimListRequest searchCondition) throws DataAccessException;
	List<Product> selectZzimList(ZzimListRequest searchCondition) throws DataAccessException;
	void createZzim(Zzim zzim) throws DataAccessException;
	void deleteZzim(Zzim zzim) throws DataAccessException;
	void deleteZzimAll(Zzim zzim) throws DataAccessException;
	void deleteZzimMulti(Zzim zzim) throws DataAccessException;
	List<Long> selectZzimPidList(Long member_no, int product_type) throws DataAccessException;

	ZzimBook selectZzimBook(long book_no) throws DataAccessException;
	int selectZzimBookListTotalCount(ZzimListRequest searchCondition) throws DataAccessException;
	List<ZzimBook> selectZzimBookList(ZzimListRequest searchCondition) throws DataAccessException;
	void createZzimBook(ZzimBook zzimBook) throws DataAccessException;
	void deleteZzimBook(ZzimBook zzimBook) throws DataAccessException;

	int selectZzimItemListTotalCount(ZzimListRequest searchCondition) throws DataAccessException;
	List<Item> selectZzimItemList(ZzimListRequest searchCondition) throws DataAccessException;
}


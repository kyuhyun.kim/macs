package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.EstimateService;
import com.zipdoc.acs.model.*;
import com.zipdoc.acs.model.subs.ZID;
import com.zipdoc.acs.persistence.dao.ContractDao;
import com.zipdoc.acs.persistence.dao.EstimateDao;
import com.zipdoc.acs.persistence.dao.ProductDao;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.*;

import static javax.swing.UIManager.put;

@Service
public class EstimateServiceImpl implements EstimateService {

    @Autowired
    private EstimateDao estimateDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ContractDao contractDao;

    @Override
    public EstimateSummary selectEstimateSummary() throws DataAccessException {
        return estimateDao.selectEstimateSummary();
    }

    @Override
    public int createEstimate(Estimate estimate) {

        if (StringUtils.isEmpty(estimate.getSubject())) {
            estimate.setSubject("서울 아파트 인테리어");
        }

        if (StringUtils.isEmpty(estimate.getSpace())) {
            estimate.setSpace("00");
        }

        int count = estimateDao.createEstimate(estimate);

        //컨셉 등록
        if (estimate.getPid_list() != null && estimate.getPid_list().size() > 0) {
            for (Long pid : estimate.getPid_list()) {
                estimateDao.createEstimateConcept(new EstimateConcept(estimate.getEstimate_no(), pid));
            }
        }

        return count;
    }

    @Override
    public MyInteriorListResponse selectMyMoveEstimateList(EstimateListRequest searchCondition) throws DataAccessException {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("writer", searchCondition.getWriter());
        map.put("phone_no", searchCondition.getPhone_no());

        return selectMyInteriorEstimateList(map);
    }

    @Override
    public MyInteriorListResponse selectMyInteriorEstimateList(EstimateListRequest searchCondition) throws DataAccessException {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("username", searchCondition.getWriter());
        map.put("phone_no", searchCondition.getPhone_no());

        return selectMyInteriorEstimateList(map, null);
    }

    @Override
    public MyInteriorListResponse selectMyInteriorEstimateList(String writer, String phone_no) {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("writer", writer);
        map.put("phone_no", phone_no);

        return selectMyInteriorEstimateList(map, AgentType.WEB);
    }

    @Override
    public MyInteriorListResponse selectMyInteriorEstimateList(Long member_no, AgentType agentType) {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("member_no", member_no);
        map.put("agent", agentType.getCode());

        return selectMyInteriorEstimateList(map, agentType);
    }

    private MyInteriorListResponse selectMyInteriorEstimateList(Map<String, Object> map) {

        List<MyInterior> myInteriors = estimateDao.selectMyEstimateList(map);

        if (myInteriors == null || myInteriors.size() == 0) return null;

        for (MyInterior interior : myInteriors) {
            interior.setAssign_partner_list(estimateDao.selectAssignedPartnerList(interior.getEstimate_no()));
            interior.setContract_info(estimateDao.selectContract(interior.getEstimate_no()));
        }

        return new MyInteriorListResponse(myInteriors.size(), myInteriors);
    }


    private MyInteriorListResponse selectMyInteriorEstimateList(Map<String, Object> map, AgentType agentType) {

        List<MyInterior> myInteriors = estimateDao.selectMyEstimateList(map);

        if (myInteriors == null || myInteriors.size() == 0) return null;

        if (agentType != null && agentType == AgentType.APP) {
            for (MyInterior interior : myInteriors) {
                interior.setAssign_partner_list(estimateDao.selectAssignedPartnerList(interior.getEstimate_no()));
                interior.setContract_info(estimateDao.selectContract(interior.getEstimate_no()));

                // 공사완료인데 진행중 상태인 경우 계약의 상태를 확인한다.
                if (interior.getStatus() == 8 && interior.getCompleted_flag() == 0) {
                    if (interior.getContract_info()
                            != null && interior.getContract_info().getCompleted_flag() == 1) {
                        interior.setCompleted_flag(1);
                    }
                }
            }
        }

        return new MyInteriorListResponse(myInteriors.size(), myInteriors);
    }

    @Override
    public List<Product> selectEstimateConceptProductList(long estimate_no) throws DataAccessException {
        return estimateDao.selectEstimateConceptProductList(estimate_no);
    }

    @Override
    public int selectListTotalCount(ListRequest searchCondition) throws DataAccessException {
        return estimateDao.selectListTotalCount(searchCondition);
    }

    @Override
    public List<Estimate> selectEstimateList(ListRequest searchCondition) throws DataAccessException {
        return estimateDao.selectEstimateList(searchCondition);
    }

    @Override
    public List<EstimateCounsel> findCounselList(Long estimate_no) throws DataAccessException {
        return estimateDao.findCounselList(estimate_no);
    }

    @Override
    public PromotionCode selectPromotionCode(String promotion_code) throws DataAccessException {
        return estimateDao.selectPromotionCode(promotion_code);
    }

    @Override
    public MyEstimate selectMyInterior(EstimateListRequest searchCondition, long estimate_no) throws DataAccessException {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("username", searchCondition.getWriter());
        map.put("phone_no", searchCondition.getPhone_no());
        map.put("estimate_no", estimate_no);

        return selectMyInterior(map, estimate_no, null);
    }

    @Override
    public MyEstimate selectMyInterior(long member_no, long estimate_no, AgentType agentType) throws DataAccessException {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("member_no", member_no);
        map.put("estimate_no", estimate_no);

        return selectMyInterior(map, estimate_no, agentType);
    }

    private MyEstimate selectMyInterior(Map<String, Object> searchCondition, long estimate_no, AgentType agentType) throws DataAccessException {

        MyEstimate estimate = estimateDao.selectMyInterior(searchCondition);
        if (estimate == null) return null;

        // 수락한 파트너 조회
        estimate.setAssign_partners(estimateDao.selectAssignPartners(estimate_no));

        // 컨셉 시공사례 조회
        if (estimate.getConcept_pid() != null) {
            Product concept = productDao.selectConcept(estimate.getConcept_pid());
            if (concept != null) {
                concept.buildPictureUrl(PictureType.XXHDPI, PictureType.HDPI);
                estimate.setConcept_subject(concept.getSubject());
                estimate.setConcept_thumbnail_url(concept.getThumbnail_url());
            }
        }

        if (agentType != null && agentType == AgentType.APP) {
            // 상담진행단계 타임라인 구성
            if (estimate.getStatus() >= EstimateStatus.VISIT_COUNSEL.getCode()) {
                if (estimate.getAssign_partners() != null) {
                    for (MyEstimatePartner partner : estimate.getAssign_partners()) {

                        // 로고 URL 생성
                        partner.buildPictureUrl();

                        // 파트너사의 시공사례 2건 조회한다.
                        ProductListRequest condition = new ProductListRequest();
                        condition.setPage(0);
                        condition.setLimit(2);
                        condition.setPartner_id(partner.getPartner_id());

                        List<Product> gallery = productDao.selectList(condition);
                        if (gallery != null && gallery.size() > 0) {
                            List<String> thumbnails = new ArrayList<>();
                            for (Product product : gallery) {
                                product.buildPictureUrl(PictureType.XXHDPI, PictureType.HDPI);
                                thumbnails.add(product.getThumbnail_url());
                            }
                            partner.setGallery_thumbnails(thumbnails);
                        }

                        // 견적 수락일
                        if (partner.getAccept_date() != null && partner.getAccept_date() > 0) {
                            estimate.setVisit_timeline_item(new MyEstimateStep(partner.getAccept_date(), EstimateStepType.PARTNER_ACCEPT.getCode(), partner.getPartner_id().toString()));
                        }

                        // 통화 완료일
                        if (partner.getCall_complete_date() != null && partner.getCall_complete_date() > 0) {
                            estimate.setVisit_timeline_item(new MyEstimateStep(partner.getCall_complete_date(), EstimateStepType.PARTNER_CALL_COMPLETE.getCode(), partner.getPartner_id().toString()));
                        }

                        // 계약완료일
                        if (partner.getContract_date() != null && partner.getContract_date() > 0) {
                            estimate.setVisit_timeline_item(new MyEstimateStep(partner.getContract_date(), EstimateStepType.COMPLETE_CONTRACT.getCode(), partner.getPartner_id().toString()));
                        }

                        // 상담종료일 (계약완료 이전에 상담이 종료된 경우)
                        if (partner.getEstimate_close_date() > 0) {
                            estimate.setVisit_timeline_item(new MyEstimateStep(partner.getEstimate_close_date(), EstimateStepType.ESTIMATE_CLOSE.getCode(), partner.getPartner_id().toString()));
                        }
                    }
                }
            }

            // 공사진행단계 타임라인 구성
            if (estimate.getStatus() >= EstimateStatus.CONSTRUCT_START.getCode()) {

                // 공사 시작일
                if (estimate.getConstruct_start_date() != null && estimate.getConstruct_start_date() > 0) {
                    estimate.setConstruct_timeline_item(new MyEstimateStep(estimate.getConstruct_start_date(), EstimateStepType.CONSTRUCT_START.getCode()));
                }
            }

            // 공사완료단계 타임라인 구성
            if (estimate.getStatus() >= EstimateStatus.CONSTRUCT_COMPLETED.getCode()) {

                // 공사종료일
                if (estimate.getConstruct_end_date() != null && estimate.getConstruct_end_date() > 0) {
                    estimate.setWarranty_timeline_item(new MyEstimateStep(estimate.getConstruct_end_date(), EstimateStepType.CONSTRUCT_END.getCode()));
                }
            }
        }

        // 공사종료일 계산
        long construct_end_date = Long.MAX_VALUE;
        if (estimate.getConstruct_end_date() != null && estimate.getConstruct_end_date() > 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(estimate.getConstruct_end_date()));
            cal.add(Calendar.DATE, 1);
            construct_end_date = cal.getTimeInMillis();
        }

        // 계약이 성사되지 않은 경우 현장방문 및 계약금액 관련 처리를 진행하지 않음.
        if (estimate.getContract_no() == null) return estimate;

        if (agentType != null && agentType == AgentType.APP) {
            // 현장방문이력 조회
            List<SiteVisitHistory> siteVisitHistories = (List<SiteVisitHistory>) ObjectUtils.defaultIfNull(contractDao.selectVisitHistories(estimate.getContract_no()), Collections.EMPTY_LIST);
            if (siteVisitHistories.size() > 0) {
                estimate.setSite_histories(siteVisitHistories);
            }

            // 현장방문 이력 - 이벤트 등록
            for (SiteVisitHistory history : siteVisitHistories) {

                // 히스토리 등록일이 공사완료일 이전인 경우 공사진행단계에 이벤트를 등록한다.
                if (history.getCreate_date() < construct_end_date) {
                    estimate.setConstruct_timeline_item(new MyEstimateStep(history.getCreate_date(), EstimateStepType.VISIT_SCHEDULED.getCode(), history.getHistory_no().toString()));
                }

                // 공사완료일 이후인 경우 공사완료단계에 이벤트를 등록한다.
                else {
                    estimate.setWarranty_timeline_item(new MyEstimateStep(history.getCreate_date(), EstimateStepType.VISIT_SCHEDULED.getCode(), history.getHistory_no().toString()));
                }

                // 방문 완료 단계인 경우
                if (history.getVisit_status() == SiteVisitStatus.COMPLETE.getCode()) {

                    // 방문일이 공사완료일 이전인 경우 공사진행단계에 이벤트를 등록한다.
                    if (history.getVisiting_date() < construct_end_date) {
                        estimate.setConstruct_timeline_item(new MyEstimateStep(history.getVisiting_date(), EstimateStepType.VISIT_DONE.getCode(), history.getHistory_no().toString()));
                    }
                    // 방문일이 공사완료일 이후인 경우 공사완료단계에 이벤트를 등록한다.
                    else {
                        estimate.setWarranty_timeline_item(new MyEstimateStep(history.getVisiting_date(), EstimateStepType.VISIT_DONE.getCode(), history.getHistory_no().toString()));
                    }
                }
            }

            // 계약금액 추가/축소/취소 조회
            List<ContractPriceHistory> priceHistories = (List<ContractPriceHistory>) ObjectUtils.defaultIfNull(contractDao.selectContractPriceHistories(estimate.getContract_no()), Collections.EMPTY_LIST);

            // 계약금액 변경 이력 - 이벤트 등록
            for (ContractPriceHistory history : priceHistories) {

                int event_type = history.getContract_price_type() == ContractPriceHistoryType.ADD.getCode() ? EstimateStepType.CONTRACT_PRICE_ADD.getCode() :
                        (history.getContract_price_type() == ContractPriceHistoryType.CANCEL.getCode() ? EstimateStepType.CONTRACT_PRICE_CANCEL.getCode() : EstimateStepType.CONTRACT_PRICE_REDUCE.getCode());

                // 계약금액 변경일이 공사완료일 이전인 경우 공사진행단계에 이벤트를 등록한다.
                if (history.getCreate_date() < construct_end_date) {
                    estimate.setConstruct_timeline_item(new MyEstimateStep(history.getCreate_date(), event_type));
                }

                // 계약금액 변경일이 공사완료일 이후인 경우 공사완료단계에 이벤트를 등록한다.
                else {
                    estimate.setWarranty_timeline_item(new MyEstimateStep(history.getCreate_date(), event_type));
                }
            }
        }

        // 보증서 파일 정보 확인
        if (estimate.getContract_no() != null) {

            MyContract warranty = contractDao.selectWarranty(estimate.getContract_no());

            if (warranty != null && warranty.getEnd_date() != null && warranty.getWarranty_create_date() != null) {

                // 추가/취소/축소 금액 조회
                warranty.setPrice_histories(contractDao.selectContractPriceHistories(estimate.getContract_no()));

                // 계약금액 및 보증서 정보 생성
                warranty.calculate_contract_price();
                warranty.calculate_warranty_info();

                // 불필요 정보 삭제
                warranty.setContract_price(null);
                warranty.setPrice_histories(null);

                // 응답 설정
                estimate.setWarranty_info(warranty);
            }
        }

        return estimate;
    }

    @Override
    public void updateEstimateOwner(EstimateListRequest searchCondition) throws DataAccessException {
        estimateDao.updateEstimateOwner(searchCondition);
    }

    @Override
    public void updateEstimateMember(ZID zid) throws DataAccessException
    {
        Map<String, Object> map = new HashMap();
        map.put("member_no", zid.getAccount().getMember_no() );
        map.put("zid", zid.getZid());
        estimateDao.updateEstimateMember(map);
    }
}

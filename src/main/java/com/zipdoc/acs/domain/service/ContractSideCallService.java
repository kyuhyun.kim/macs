package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.ContractSideCall;

public interface ContractSideCallService {

    void createContractSideCall(ContractSideCall contractSideCall);

}

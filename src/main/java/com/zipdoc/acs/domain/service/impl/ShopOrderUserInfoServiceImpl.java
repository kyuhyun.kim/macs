package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.ShopOrderUserInfo;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopOrderUserInfoService;
import com.zipdoc.acs.persistence.dao.ShopOrderUserInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ShopOrderUserInfoServiceImpl implements ShopOrderUserInfoService{
	
	@Autowired
	private ShopOrderUserInfoDao ShopOrderUserInfoDao;
	
	@Override
	public void createOrderUserInfo(ShopOrderUserInfo shopOrderUserInfo) throws ServiceException {
		ShopOrderUserInfoDao.createOrderUserInfo(shopOrderUserInfo);
	}

	@Override
	public ShopOrderUserInfo findById(String order_no) throws ServiceException {
		return ShopOrderUserInfoDao.findById(order_no);
	}

	@Override
	public ShopOrderUserInfo findByNonMember(Map<String, Object> condition) throws ServiceException {
		return ShopOrderUserInfoDao.findByNonMember(condition);
	}

	@Override
	public ShopOrderUserInfo findByMemberNo(long member_no) throws ServiceException {
		return ShopOrderUserInfoDao.findByMemberNo(member_no);
	}
}

package com.zipdoc.acs.domain.service;

import org.springframework.dao.DataAccessException;

public class ServiceException extends DataAccessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6860644044806112728L;
	
	private int invokeResult;
	private String invokeResultMsg;
	
	public ServiceException(String invokeResultMsg){
		super("["+500+"]"+invokeResultMsg);
		this.invokeResult = 500;
		this.invokeResultMsg = invokeResultMsg;
	}
	
	public ServiceException(int invokeResult, String invokeResultMsg ){
		super("["+invokeResult+"]"+invokeResultMsg);
		this.invokeResult = invokeResult;
		this.invokeResultMsg = invokeResultMsg;
	}
	
	public ServiceException(int invokeResult, String invokeResultMsg, Throwable th ){
		super("["+invokeResult+"]"+invokeResultMsg, th);
		this.invokeResult = invokeResult;
		this.invokeResultMsg = invokeResultMsg;
	}

	public int getInvokeResult() {
		return invokeResult;
	}

	public void setInvokeResult(int invokeResult) {
		this.invokeResult = invokeResult;
	}

	public String getInvokeResultMsg() {
		return invokeResultMsg;
	}

	public void setInvokeResultMsg(String invokeResultMsg) {
		this.invokeResultMsg = invokeResultMsg;
	}
	
	
}

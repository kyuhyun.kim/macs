package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.service.ContractService;
import com.zipdoc.acs.persistence.dao.ContractDao;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ContractServiceImpl implements ContractService{
	
	@Autowired
	private ContractDao contractDao;


	@Override
	public List<ContractFile> selectContractFileList(long contract_no) throws DataAccessException {
		return contractDao.selectContractFileList(contract_no);
	}

	@Override
	public List<ContractFile> selectContractFileListAll(long contract_no) throws DataAccessException {
		return contractDao.selectContractFileListAll(contract_no);
	}

	@Override
	public void createContractFile(ContractFile contractFile) throws DataAccessException {
		contractDao.createContractFile(contractFile);
	}

	@Override
	public void deleteContractFile(long file_no) throws DataAccessException {
		contractDao.deleteContractFile(file_no);
	}

	@Override
	public ContractFile selectContractFile(long file_no) throws DataAccessException {
		return contractDao.selectContractFile(file_no);
	}

	@Override
	public void createContractHistoryFile(ContractFileHistory contractFileHistory) throws DataAccessException {
		contractDao.createContractHistoryFile(contractFileHistory);
	}

	@Override
	public int findListCountContractVisit(Map<String, Object> condition) throws DataAccessException {
		return contractDao.findListCountContractVisit(condition);
	}

	@Override
	public List<ContractVisit> findListContractVisit(Map<String, Object> condition) throws DataAccessException {
		return contractDao.findListContractVisit(condition);
	}

	@Override
	public int updateContractVisit(ContractVisit condition) throws DataAccessException {
		return contractDao.updateContractVisit(condition);
	}

	@Override
	public Contract findById(long contract_no) throws DataAccessException {
		return contractDao.findById(contract_no);
	}

	@Override
	public SiteVisitHistory selectVisitHistory(long history_no) throws DataAccessException {
		return contractDao.selectVisitHistory(history_no);
	}

	@Override
	public void createVisitHistory(SiteVisitHistory history) throws DataAccessException {
		contractDao.createVisitHistory(history);
	}

	@Override
	public void updateVisitHistory(SiteVisitHistory history) throws DataAccessException {
		contractDao.updateVisitHistory(history);
	}

	@Override
	public SiteVisitHistory selectLastSupervisionHistory(long contract_no) throws DataAccessException {
		return contractDao.selectLastSupervisionHistory(contract_no);
	}

	@Override
	public MyContract selectMyContract(long contract_no) throws DataAccessException {
		MyContract contract = contractDao.selectMyContract(contract_no);

		// 추가/취소/축소 금액 조회
		if (contract != null) {
			contract.setPrice_histories(contractDao.selectContractPriceHistories(contract_no));
			contract.calculate_contract_price();
		}

		// 계약문서 조회
		contract.setContract_files(contractDao.selectContractFileImageList(contract_no));

		return contract;
	}
}

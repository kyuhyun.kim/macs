package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.Event;
import com.zipdoc.acs.domain.entity.FAQ;
import com.zipdoc.acs.domain.entity.Magazine;
import com.zipdoc.acs.domain.entity.News;
import com.zipdoc.acs.domain.service.BoardService;
import com.zipdoc.acs.model.*;
import com.zipdoc.acs.persistence.dao.BoardDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class BoardServiceImpl implements BoardService {
	
	@Autowired
	private BoardDao boardDao;


	@Override
	public ListResponse<Magazine> searchMagazineListRevision(MagazineListRequest searchCondition) throws DataAccessException {

		ListResponse<Magazine> response = new ListResponse<>();

		Map<String, Object> objectMap = new HashMap<>();
		objectMap = searchCondition.getKeywordsMap();
		objectMap.put("keywords", searchCondition.getKeywords());
		objectMap.put("startRow", searchCondition.getStartRow());
		objectMap.put("limit", searchCondition.getLimit());

		response.setTotal_count(boardDao.searchMagazineTotalCountRevision(objectMap));
		response.setList(boardDao.searchMagazineListRevision(objectMap));
		return response;
	}


	@Override
	public ListResponse<Magazine> searchMagazineList(MagazineListRequest searchCondition) throws DataAccessException {

		ListResponse<Magazine> response = new ListResponse<>();

		response.setTotal_count(boardDao.searchMagazineTotalCount(searchCondition));
		response.setList(boardDao.searchMagazineList(searchCondition));
		return response;
	}

	@Override
	public Magazine selectMagazineDetail(int cno) throws DataAccessException {
		boardDao.updateViewMagazineCnt(cno);
		return boardDao.selectMagazineDetail(cno);
	}

	@Override
	public ListResponse<News> searchNewsList(MagazineListRequest searchCondition) throws DataAccessException {

		ListResponse<News> response = new ListResponse<>();

		response.setTotal_count(boardDao.searchNewsTotalCount(searchCondition));
		response.setList(boardDao.searchNewsList(searchCondition));
		return response;
	}

	@Override
	public News selectNewsDetail(int cno) throws DataAccessException {
		boardDao.updateViewMagazineCnt(cno);
		return boardDao.selectNewsDetail(cno);
	}

	@Override
	public ListResponse<Event> searchEventList(ListRequest searchCondition) throws DataAccessException {

		ListResponse<Event> response = new ListResponse<>();

		response.setTotal_count(boardDao.searchEventTotalCount(searchCondition));
		response.setList(boardDao.searchEventList(searchCondition));
		return response;
	}

	@Override
	public Event selectEventDetail(Map<String, Object> searchCondition) throws DataAccessException {
		boardDao.updateViewEventCnt((Integer)searchCondition.get("cno"));
		return boardDao.selectEventDetail(searchCondition);
	}

	@Override
	public ListResponse<FAQ> searchFAQList(FAQListRequest searchCondition) throws DataAccessException {

		ListResponse<FAQ> response = new ListResponse<>();

		response.setTotal_count(boardDao.searchFAQTotalCount(searchCondition));
		response.setList(boardDao.searchFAQList(searchCondition));
		return response;
	}
}



package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.ShopMileageHistory;

import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopMileageHistoryService;
import com.zipdoc.acs.persistence.dao.MemberDao;
import com.zipdoc.acs.persistence.dao.ShopMileageHistoryDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * Created by dskim on 2017. 3. 3..
 */
@Service
public class ShopMileageHistoryServiceImpl implements ShopMileageHistoryService {

    @Autowired
    private ShopMileageHistoryDao shopReservesHistoryDao;

    @Autowired
    private MemberDao memberDao;

    @Override
    public List<ShopMileageHistory> findList(Map<String, Object> condition, int currentPage, int itemSize) throws ServiceException {

        List<ShopMileageHistory> shopMileageHistoryList = shopReservesHistoryDao.findShopMileageHistoryList(condition);

        return  shopMileageHistoryList;
    }


    @Override
    public void insert(ShopMileageHistory shopReservesHistory) throws ServiceException {

        shopReservesHistoryDao.insertShopMileageHistory(shopReservesHistory);

        //유저테이블 적립금 update
        Member member = new Member();
        member.setMember_no(shopReservesHistory.getMember_no());
        member.setMileage(shopReservesHistory.getAfter_mileage());

        memberDao.updateMileage(member);


    }


    @Override
    public ShopMileageHistory findByCurrentShopMileageHistory(long member_no) throws ServiceException {
        return shopReservesHistoryDao.findByCurrentShopMileageHistory(member_no);
    }
}

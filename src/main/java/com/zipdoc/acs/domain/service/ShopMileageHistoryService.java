package com.zipdoc.acs.domain.service;


import com.zipdoc.acs.domain.entity.ShopMileageHistory;

import java.util.List;
import java.util.Map;

/**
 *
 * 적립금 내역 히스토리
 * Created by dskim on 2017. 3. 3..
 */
public interface ShopMileageHistoryService {


    List<ShopMileageHistory> findList(Map<String, Object> condition, int currentPage, int itemSize) throws ServiceException;

    ShopMileageHistory findByCurrentShopMileageHistory(long member_no) throws ServiceException;

    void insert(ShopMileageHistory shopMileageHistory) throws ServiceException;

}

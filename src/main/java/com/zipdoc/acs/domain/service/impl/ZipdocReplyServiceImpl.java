package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.ZipdocReply;
import com.zipdoc.acs.domain.service.ZipdocReplyService;
import com.zipdoc.acs.model.ZipdocReplyListRequest;
import com.zipdoc.acs.persistence.dao.ZipdocReplyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 이동규 on 2017-03-08.
 */
@Service
public class ZipdocReplyServiceImpl implements ZipdocReplyService {

    @Autowired
    private ZipdocReplyDao zipdocReplyDao;

    @Override
    public List<ZipdocReply> selectList(ZipdocReplyListRequest searchCondition) throws DataAccessException {
        return zipdocReplyDao.selectList(searchCondition);
    }

    @Override
    public int selectListTotalCount(ZipdocReplyListRequest searchCondition) throws DataAccessException {
        return zipdocReplyDao.selectListTotalCount(searchCondition);
    }

    @Override
    public ZipdocReply selectById(long cno) throws DataAccessException {
        return zipdocReplyDao.selectById(cno);
    }

    @Override
    public void create(ZipdocReply zipdocReply) throws DataAccessException {
        zipdocReplyDao.create(zipdocReply);
    }

    @Override
    public void delete(long cno) throws DataAccessException {
        zipdocReplyDao.deleteReply(cno);
        zipdocReplyDao.delete(cno);
    }

    @Override
    public void updateIncreaseViewCnt(long cno) throws DataAccessException {
        zipdocReplyDao.updateIncreaseViewCnt(cno);
    }

    @Override
    public void deleteMulti(ZipdocReply target) throws DataAccessException {
        for(Long cno:target.getArticle_no_list()){
            delete(cno);
        }
    }

    @Override
    public void deleteAllByMemberNo(Long member_no) throws DataAccessException {
        ZipdocReplyListRequest searchCondition = new ZipdocReplyListRequest();
        searchCondition.setMember_no(member_no);
        searchCondition.setOwner(1);

        int totalCount = selectListTotalCount(searchCondition);
        searchCondition.setPageLimit(0, totalCount);

        List<ZipdocReply> myList = selectList(searchCondition);

        for(ZipdocReply zipdocReply:myList){
            delete(zipdocReply.getCno());
        }
    }

}

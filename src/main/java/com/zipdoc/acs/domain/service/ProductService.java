package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.Item;
import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.Partner;
import com.zipdoc.acs.domain.entity.Product;
import com.zipdoc.acs.model.ItemListRequest;
import com.zipdoc.acs.model.ProductListRequest;
import com.zipdoc.acs.model.ZzimListRequest;
import org.springframework.dao.DataAccessException;


import java.util.List;
import java.util.Map;

public interface ProductService {
	int selectListTotalCount(ProductListRequest searchCondition) throws DataAccessException;
	List<Product> selectList(ProductListRequest searchCondition) throws DataAccessException;
	Product selectById(long pid, Long member_no) throws DataAccessException;
	int selectItemListTotalCount(ItemListRequest searchCondition) throws DataAccessException;
	List<Item> selectItemList(ItemListRequest searchCondition) throws DataAccessException;
	Partner selectPartner(int partner_id, int gallery_cnt) throws DataAccessException;

	int selectCommunityListTotalCount(ProductListRequest searchCondition) throws DataAccessException;
	List<Product> selectCommunityList(ProductListRequest searchCondition) throws DataAccessException;
	Product selectCommunityById(long pid, Long member_no) throws DataAccessException;

	void updateProductStatus(Product product) throws DataAccessException;

	void updateIncreaseHitCnt(long pid) throws DataAccessException;

	void updateProductStatusAllByMemberNo(Long member_no) throws DataAccessException;

	void insert(Product product) throws DataAccessException;
	void update(Product product) throws DataAccessException;

	void updateIncreaseShareCnt(long pid) throws DataAccessException;

    // 2018.10.02. KJB. 비슷한 가격의 시공사례
    List<Product> selectSamePriceProductList(Map<String,Object> condition) throws DataAccessException;

	int searchGalleryTotalCount(ProductListRequest searchCondition) throws DataAccessException;

	/* 검색 기능 개선 */
	int searchGalleryTotalCountRevision(ProductListRequest searchCondition) throws DataAccessException;

	int searchGalleryTotalCountRevision(Map<String, Object> searchCondition) throws DataAccessException;

	List<Product> searchGalleryList(ProductListRequest searchCondition) throws DataAccessException;

	List<Product> searchGalleryListRevision(ProductListRequest searchCondition) throws DataAccessException;

	List<Product> searchGalleryListRevision(Map<String, Object> searchCondition) throws DataAccessException;

    int searchGalleryPictureTotalCount(ItemListRequest searchCondition) throws DataAccessException;

	int searchGalleryPictureTotalCountRevision(ItemListRequest searchCondition) throws DataAccessException;

	int searchGalleryPictureTotalCountRevision(Map<String, Object> searchCondition) throws DataAccessException;

    List<Item> searchGalleryPictureList(ItemListRequest searchCondition) throws DataAccessException;

	List<Item> searchGalleryPictureListRevision(ItemListRequest searchCondition) throws DataAccessException;

	List<Item> searchGalleryPictureListRevision(Map<String, Object> searchCondition) throws DataAccessException;

	// 2018.09.03. KJB. 시공컨셉 조회
	Product selectConcept(long pid) throws DataAccessException;

	// 2019.02.26 비포애프터 총 갯수 조회
	int selectListTotalBeforeAfterCount(ProductListRequest searchCondition) throws DataAccessException;

	// 2019.02.26 비포애프터 메인 화면 리스트 조회
	List<Product> selectBeforeAfterList(ProductListRequest searchCondition) throws DataAccessException;


}

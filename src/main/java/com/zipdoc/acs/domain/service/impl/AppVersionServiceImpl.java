package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.AppVersion;
import com.zipdoc.acs.domain.service.AppVersionService;
import com.zipdoc.acs.persistence.dao.AppVersionDao;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class AppVersionServiceImpl implements AppVersionService{
	
	@Autowired
	private AppVersionDao appVersionDao;

	public AppVersion selectVersion(String os_type, String app_market) {
		Map<String, String> condition = new HashMap<String, String>();
		condition.put("os_type", os_type);
		condition.put("app_market", app_market);
		return appVersionDao.selectVersion(condition);
	}
}

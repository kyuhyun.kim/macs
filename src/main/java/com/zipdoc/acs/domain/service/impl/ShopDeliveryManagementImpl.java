package com.zipdoc.acs.domain.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zipdoc.acs.domain.entity.ShopDeliveryManagement;
import com.zipdoc.acs.domain.entity.ShopDeliveryManagementRequest;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopDeliveryManagementService;
import com.zipdoc.acs.persistence.dao.ShopDeliveryManagementDao;

@Service
public class ShopDeliveryManagementImpl implements ShopDeliveryManagementService{

	@Autowired
	private ShopDeliveryManagementDao shopDeliveryManagementDao;
	
	@Override
	public int selectDeliveryListTotalCount(ShopDeliveryManagementRequest shopDeliveryManagementRequest) {
		return shopDeliveryManagementDao.selectDeliveryListTotalCount(shopDeliveryManagementRequest);
	}

	@Override
	public List<ShopDeliveryManagement> selectDeliveryList(
			ShopDeliveryManagementRequest shopDeliveryManagementRequest) {
		return shopDeliveryManagementDao.selectDeliveryList(shopDeliveryManagementRequest);
	}

	@Override
	public ShopDeliveryManagement selectById(Map<String,Object> condition) throws ServiceException {
		return shopDeliveryManagementDao.selectById(condition);
	}

	@Override
	public void create(ShopDeliveryManagement shopDeliveryManagement) throws ServiceException {
		shopDeliveryManagementDao.create(shopDeliveryManagement);
		if(shopDeliveryManagement.getDefault_address_yn()!=null && shopDeliveryManagement.getDefault_address_yn() == 1){
			Map<String, Object> condition = new HashMap<>();
			condition.put("creater", shopDeliveryManagement.getCreater());
			condition.put("sno", shopDeliveryManagement.getSno());
			shopDeliveryManagementDao.updateDefaultAddress(condition);
		}
	}

	@Override
	public void delete(Map<String, Object> condition) throws ServiceException {
		shopDeliveryManagementDao.delete(condition);
	}

	@Override
	public void update(ShopDeliveryManagement shopDeliveryManagement) throws ServiceException {
		shopDeliveryManagementDao.update(shopDeliveryManagement);
		if(shopDeliveryManagement.getDefault_address_yn()!=null && shopDeliveryManagement.getDefault_address_yn() == 1){
			Map<String, Object> condition = new HashMap<>();
			condition.put("creater", shopDeliveryManagement.getCreater());
			condition.put("sno", shopDeliveryManagement.getSno());
			shopDeliveryManagementDao.updateDefaultAddress(condition);
		}
	}
}

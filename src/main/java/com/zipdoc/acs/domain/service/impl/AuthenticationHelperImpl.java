package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.Member;
import com.zipdoc.acs.domain.entity.Partner;
import com.zipdoc.acs.domain.service.AuthenticationHelper;
import com.zipdoc.acs.domain.service.MemberService;
import com.zipdoc.acs.domain.service.PartnerService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.PartnerType;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * Created by 이동규 on 2016-11-16.
 */
@Component
public class AuthenticationHelperImpl implements AuthenticationHelper {
    private static final Logger log = LoggerFactory.getLogger(AuthenticationHelperImpl.class);
    @Autowired
    private MemberService memberService;

    @Autowired
    private PartnerService partnerService;

    @Override
    public Member findMember(Long member_no, Long zid, String zid_key) throws ServiceException {
        return findMember(member_no, zid, zid_key, false);
    }

    @Override
    public Member findMember(Long member_no, Long zid, String zid_key, boolean needMember) throws ServiceException {

        // 가입자 조회
        Member member = memberService.selectMember(member_no, zid);

        // 가입자 정보가 없을 경우
        if (member == null) {
            log.error("회원번호["+member_no+"] 또는 디바이스["+zid+"]가 존재하지 않습니다.");
            throw new ServiceException(HttpStatus.FORBIDDEN.value(), "회원번호["+member_no+"] 또는 디바이스["+zid+"]가 존재하지 않습니다.");
        }

        //회원정보가 없는 경우
        if(needMember) {
            if (member_no == null || member_no == 0 || !member.isAccountNormal()) {
                log.error("로그인하지 않았거나 정상 회원이 아님[" + member_no + "]");
                throw new ServiceException(HttpStatus.FORBIDDEN.value(), "로그인하지 않았거나 정상 회원이 아님[" + member_no + "]");
            }
        }

        // 인증키 오류
        if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
            log.error("["+member.getMember_id()+"]의 ZID["+zid+"] 인증키가 일치하지 않습니다.");
            throw new ServiceException(HttpStatus.FORBIDDEN.value(), "회원의 인증키가 일치하지 않습니다.");
        }
        return member;
    }

    @Override
    public Partner findPartner(Integer partner_id) throws ServiceException {

        if(partner_id==null || partner_id < 0){
            throw new ServiceException(HttpStatus.UNAUTHORIZED.value(), "파트너번호["+partner_id+"]가 유효하지 않습니다.");
        }

        // 파트너 업체 정보 조회
        Partner partner = partnerService.selectPartner(partner_id);

        // 파트너 사 승인 여부
        if (partner == null || partner.getPartner_type() == PartnerType.UNAPPROVED.value()) {
            log.error("파트너번호 [" + partner_id+"]은 파트너 가입자가 아니거나 미승인 상태입니다.");
            throw new ServiceException(HttpStatus.UNAUTHORIZED.value(), " 파트너 가입자가 아니거나 미승인 상태입니다.");
        }
        return partner;
    }

    @Override
    public Member findMember(Long member_no, Long zid, String zid_key, String web_token) throws ServiceException {
        return findMemberOrDevice(member_no, zid, zid_key, web_token, true);
    }

    @Override
    public Member findMemberOrDevice(Long member_no, Long zid, String zid_key, String web_token) throws ServiceException {
        return findMemberOrDevice(member_no, zid, zid_key, web_token, false);
    }

    private Member findMemberOrDevice(Long member_no, Long zid, String zid_key, String web_token, boolean needMember) throws ServiceException {

        // 웹 가입자인 경우
        if (StringUtils.isNotEmpty(web_token)) {

            // 웹 가입자 조회
            Member member = memberService.selectMemberByWebToken(web_token);

            // 가입자 정보가 없을 경우
            if (member == null) {
                log.error("WEB-TOKEN[" + web_token + "]으로 등록된 가입자는 존재하지 않습니다.");
                throw new ServiceException(HttpStatus.FORBIDDEN.value(), "WEB-TOKEN[" + web_token + "]으로 등록된 가입자는 존재하지 않습니다.");
            }
            return member;
        }

        // 앱 가입자 검색
        if (zid == null || StringUtils.isEmpty(zid_key)) {
            log.error("가입 여부 확인을 위한 필수정보가 없습니다. [ZID / ZID-KEY / ZWS_TOKEN]");
            throw new ServiceException(HttpStatus.BAD_REQUEST.value(), "가입 여부 확인을 위한 필수정보가 없습니다. [ZID / ZID-KEY / ZWS_TOKEN]");
        }

        // 앱 가입자 검색
        if (needMember && member_no == null) {
            log.error("앱 가입 확인을 위한 필수정보가 없습니다. [MEMBER-NO]");
            throw new ServiceException(HttpStatus.BAD_REQUEST.value(), "앱 가입 확인을 위한 필수정보가 없습니다. [MEMBER-NO]");
        }

        // 가입자 조회
        Member member = memberService.selectMember(member_no, zid);

        // 가입자 정보가 없을 경우
        if (member == null) {
            log.error("회원["+member_no+"] 또는 디바이스["+zid+"]가 존재하지 않습니다.");
            throw new ServiceException(HttpStatus.FORBIDDEN.value(), "회원["+member_no+"] 또는 디바이스["+zid+"]가 존재하지 않습니다.");
        }

        // 인증키 오류
        if (member.getZid_key() == null || !member.getZid_key().equals(zid_key)) {
            log.error("ZID["+zid+"]의 인증키가 일치하지 않습니다.");
            throw new ServiceException(HttpStatus.FORBIDDEN.value(), "ZID["+zid+"]의 인증키가 일치하지 않습니다.");
        }
        return member;
    }
}

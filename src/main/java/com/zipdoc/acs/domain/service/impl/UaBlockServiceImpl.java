package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.controller.ContractHistoryController;
import com.zipdoc.acs.domain.entity.CrashLog;
import com.zipdoc.acs.domain.service.CrashLogService;
import com.zipdoc.acs.domain.service.UaBlockService;
import com.zipdoc.acs.persistence.dao.CrashLogDao;
import com.zipdoc.acs.persistence.dao.UaBlockDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 이동규 on 2016-06-13.
 */
@Service
public class UaBlockServiceImpl implements UaBlockService {

    private static final Logger log = LoggerFactory.getLogger(UaBlockServiceImpl.class);

    @Autowired
    private UaBlockDao uaBlockDao;

    @Override
    public boolean isContainKeyword(String user_agent) throws DataAccessException {

        List<String> uaBlockKeyword = uaBlockDao.getUaKeywordList();

        for (String keyword : uaBlockKeyword) {

            log.info("[CHECK-UA] user-agent="+user_agent+" block-keyword="+keyword);
            if (user_agent != null && user_agent.indexOf(keyword) != -1) {
                log.info("[CHECK-UA] BLOCK KEYWORD");
                return true;
            }
        }
        return false;
    }
}

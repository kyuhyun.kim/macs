package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.Event;
import com.zipdoc.acs.domain.entity.FAQ;
import com.zipdoc.acs.domain.entity.Magazine;
import com.zipdoc.acs.domain.entity.News;
import com.zipdoc.acs.model.FAQListRequest;
import com.zipdoc.acs.model.ListRequest;
import com.zipdoc.acs.model.ListResponse;
import com.zipdoc.acs.model.MagazineListRequest;
import org.springframework.dao.DataAccessException;

import java.util.Map;

public interface BoardService {


	ListResponse<Magazine> searchMagazineListRevision(MagazineListRequest searchCondition) throws DataAccessException;

	ListResponse<Magazine> searchMagazineList(MagazineListRequest searchCondition) throws DataAccessException;
	Magazine selectMagazineDetail(int cno) throws DataAccessException;

	ListResponse<News> searchNewsList(MagazineListRequest searchCondition) throws DataAccessException;
	News selectNewsDetail(int cno) throws DataAccessException;

	ListResponse<Event> searchEventList(ListRequest searchCondition) throws DataAccessException;
	Event selectEventDetail(Map<String, Object> searchCondition) throws DataAccessException;

	ListResponse<FAQ> searchFAQList(FAQListRequest searchCondition) throws DataAccessException;
}



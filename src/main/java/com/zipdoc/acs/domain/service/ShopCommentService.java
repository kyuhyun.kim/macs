package com.zipdoc.acs.domain.service;

import java.util.List;

import com.zipdoc.acs.domain.entity.ShopComment;
import com.zipdoc.acs.model.CommentListRequest;

public interface ShopCommentService {
	public List<ShopComment> selectList(CommentListRequest searchCondition) throws ServiceException;
	public int selectListTotalCount(CommentListRequest searchCondition) throws ServiceException;
	public ShopComment selectById(ShopComment comment) throws ServiceException;
	public void insert(ShopComment comment) throws ServiceException;
	public void update(ShopComment comment) throws ServiceException;
	public void updateDeleteFlag(ShopComment comment) throws ServiceException;
	public int findByCreater(ShopComment comment) throws ServiceException;
}

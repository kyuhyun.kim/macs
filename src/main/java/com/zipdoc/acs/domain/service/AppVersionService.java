package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.AppVersion;
import org.springframework.dao.DataAccessException;

public interface AppVersionService {
	public AppVersion selectVersion(String os_type, String appMarket) throws DataAccessException;
}

package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.CrashLog;
import org.springframework.dao.DataAccessException;

public interface CrashLogService {
	public void createCrashLog(CrashLog crashLog) throws DataAccessException;
}


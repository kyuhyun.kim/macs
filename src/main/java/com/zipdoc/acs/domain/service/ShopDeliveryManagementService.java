package com.zipdoc.acs.domain.service;

import java.util.List;
import java.util.Map;

import com.zipdoc.acs.domain.entity.ShopDeliveryManagement;
import com.zipdoc.acs.domain.entity.ShopDeliveryManagementRequest;

public interface ShopDeliveryManagementService {

	int selectDeliveryListTotalCount(ShopDeliveryManagementRequest shopDeliveryManagementRequest);

	List<ShopDeliveryManagement> selectDeliveryList(ShopDeliveryManagementRequest shopDeliveryManagementRequest);

	ShopDeliveryManagement selectById(Map<String, Object> condition) throws ServiceException;

	void create(ShopDeliveryManagement shopDeliveryManagement) throws ServiceException;

	void delete(Map<String, Object> condition) throws ServiceException;

	void update(ShopDeliveryManagement shopDeliveryManagement) throws ServiceException;
	
}

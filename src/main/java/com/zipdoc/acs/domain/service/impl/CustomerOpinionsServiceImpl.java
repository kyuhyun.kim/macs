package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.Article;
import com.zipdoc.acs.domain.entity.VividComment;
import com.zipdoc.acs.domain.service.ArticleService;
import com.zipdoc.acs.domain.service.CustomerOpinionsService;
import com.zipdoc.acs.model.ArticleListRequest;
import com.zipdoc.acs.model.ArticleListResponse;
import com.zipdoc.acs.persistence.dao.ArticleDao;
import com.zipdoc.acs.persistence.dao.CashedCustomerOpinionsDao;
import com.zipdoc.acs.persistence.dao.CustomerOpinionsDao;
import com.zipdoc.acs.utils.AcsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerOpinionsServiceImpl implements CustomerOpinionsService {
	
	@Autowired
	private CustomerOpinionsDao customerOpinionsDao;

	@Autowired
	private CashedCustomerOpinionsDao cashedCustomerOpinionsDao;

	@Autowired
	private AcsConfig acsConfig;


	public Integer getVividCommentCount(VividCommentCategoryCode vividCommentCategoryCode, AgentType agentType) throws DataAccessException
	{
		Map<String, Object> condition = new HashMap<>();
		if(vividCommentCategoryCode.getCode() != VividCommentCategoryCode.전체.getCode())
		condition.put("category_code1", vividCommentCategoryCode.getCode());

		return customerOpinionsDao.selectVividCommentCount(condition);
	}

	public List<VividComment> getVividComment(VividCommentCategoryCode vividCommentCategoryCode, AgentType agentType, int startRow, int limit ) throws DataAccessException
	{
		Map<String, Object> condition = new HashMap<>();
		condition.put("startRow", startRow);
		condition.put("limit", limit);
		if(vividCommentCategoryCode.getCode() != VividCommentCategoryCode.전체.getCode())
			condition.put("category_code1", vividCommentCategoryCode.getCode());

		List<VividComment> vividComments = customerOpinionsDao.selectVividComment(condition);
		SetAvgScore(vividComments);
		return vividComments;
	}


	public List<VividComment> getVividComment(CategoryCode category, AgentType agentType, int startRow, int limit ) throws DataAccessException
	{
		Map<String, Object> condition = new HashMap<>();
		condition.put("startRow", startRow);
		condition.put("limit", limit);
		if(category != null)
			condition.put("category_code1", category.getCode());

		List<VividComment> vividComments = null;
		if( EnableFlag.get( acsConfig.getPropertyToInteger("CASHED.ENABLE") ) == EnableFlag.ENABLE)
			vividComments = cashedCustomerOpinionsDao.selectVividComment(condition);
		else
			vividComments = customerOpinionsDao.selectVividComment(condition);

		SetAvgScore(vividComments);
		return vividComments;
	}

	public void SetAvgScore(List<VividComment> vividComments)
	{
		if(vividComments != null && vividComments.size() > 0)
		{
			for(VividComment vividComment: vividComments)
			{
				vividComment.setAvg_score();
			}
		}
	}
}



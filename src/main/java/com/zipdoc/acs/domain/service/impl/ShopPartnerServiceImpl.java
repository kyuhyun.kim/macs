package com.zipdoc.acs.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zipdoc.acs.domain.entity.ShopPartnerReq;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopPartnerService;
import com.zipdoc.acs.persistence.dao.ShopPartnerDao;

@Service
public class ShopPartnerServiceImpl implements ShopPartnerService{

	@Autowired
	private ShopPartnerDao shopPartnerDao;
	
	@Override
	public void create(ShopPartnerReq shopPartnerReq) throws ServiceException {
		shopPartnerDao.create(shopPartnerReq);
	}

}

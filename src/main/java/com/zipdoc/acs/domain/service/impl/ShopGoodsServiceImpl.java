package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.ShopGoods;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopGoodsService;
import com.zipdoc.acs.model.shop.ShopGoodsListRequest;
import com.zipdoc.acs.persistence.dao.ShopGoodsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by dskim on 2017. 5. 11..
 */
@Service
public class ShopGoodsServiceImpl implements ShopGoodsService {


    @Autowired
    private ShopGoodsDao shopGoodsDao;


    @Override
    public int selectListTotalCount(ShopGoodsListRequest searchCondition) throws ServiceException {
        return shopGoodsDao.selectListTotalCount(searchCondition);
    }

    @Override
    public List<ShopGoods> selectList(ShopGoodsListRequest searchCondition) throws ServiceException {
        return shopGoodsDao.selectList(searchCondition);
    }

    @Override
    public ShopGoods selectById(long goods_no) throws DataAccessException {
        return shopGoodsDao.selectByGoodsNo(goods_no);
    }


    @Override
    public void updateIncreaseOrderCnt(Map<String, Object> condition) throws ServiceException {
        shopGoodsDao.updateIncreaseOrderCnt(condition);
    }
}

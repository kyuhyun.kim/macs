package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.Customer;
import com.zipdoc.acs.domain.entity.CustomerPartnerInquiry;
import com.zipdoc.acs.domain.entity.CustomerReply;
import com.zipdoc.acs.domain.service.CustomerService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.persistence.dao.CustomerDao;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerDao customerDao;

	@Override
	public int findListCount(Map<String, Object> condition) throws ServiceException {
		return customerDao.findListCount(condition);
	}

	@Override
	public List<Customer> findList(Map<String, Object> condition) throws ServiceException {
		return (List<Customer>) ObjectUtils.defaultIfNull(customerDao.findList(condition), Collections.EMPTY_LIST);
	}

	@Override
	public Customer findById(Map<String, Object> condition) throws ServiceException {
		return customerDao.findById(condition);
	}

	@Override
	public void insert(Customer customer) throws ServiceException {
		customerDao.insert(customer);
	}

	@Override
	public int update(Customer customer) throws ServiceException {
		return customerDao.update(customer);
	}

	@Override
	public int delete(int cno) throws ServiceException {
		customerDao.deleteReplyAll(cno);
		return customerDao.delete(cno);
	}

	@Override
	public List<CustomerReply> findReplyList(int cno) throws ServiceException {
		return customerDao.findReplyList(cno);
	}

	@Override
	public void insertReply(CustomerReply customerReply) throws ServiceException {
		customerDao.insertReply(customerReply);
	}

	@Override
	public int deleteReply(int reply_no) throws ServiceException {
		return customerDao.deleteReply(reply_no);
	}

	@Override
	public int updateReply(CustomerReply customerReply) throws ServiceException {
		return customerDao.updateReply(customerReply);
	}

	@Override
	public void insertPartnerInquiry(CustomerPartnerInquiry inquiry) throws ServiceException {
		customerDao.insertPartnerInquiry(inquiry);
	}
}

package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.ShopOrderUserInfo;

import java.util.Map;

public interface ShopOrderUserInfoService {
	void createOrderUserInfo(ShopOrderUserInfo shopOrderUserInfo) throws ServiceException;

	ShopOrderUserInfo findById(String order_no) throws ServiceException;

	ShopOrderUserInfo findByNonMember(Map<String, Object> condition) throws ServiceException;

	//2017-03-31 최근주문한 유저주문내역
	ShopOrderUserInfo findByMemberNo(long member_no) throws ServiceException;
}

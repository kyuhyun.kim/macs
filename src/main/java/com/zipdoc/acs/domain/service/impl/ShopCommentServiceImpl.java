package com.zipdoc.acs.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zipdoc.acs.define.EnableFlag;
import com.zipdoc.acs.domain.entity.ShopComment;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopCommentService;
import com.zipdoc.acs.model.CommentListRequest;
import com.zipdoc.acs.persistence.dao.ShopCommentDao;

@Service
public class ShopCommentServiceImpl implements ShopCommentService{
	
	@Autowired
	private ShopCommentDao shopCommentDao;

	@Override
	public List<ShopComment> selectList(CommentListRequest searchCondition) throws ServiceException {
		return shopCommentDao.selectList(searchCondition);
	}

	@Override
	public int selectListTotalCount(CommentListRequest searchCondition) throws ServiceException {
		return shopCommentDao.selectListTotalCount(searchCondition);
	}

	@Override
	public ShopComment selectById(ShopComment comment) throws ServiceException {
		return shopCommentDao.selectById(comment);
	}

	@Override
	public void insert(ShopComment comment) throws ServiceException {
		int reply_no = shopCommentDao.selectNextReplyNo(comment);

		comment.setReply_no(reply_no);

		if(!comment.isRereply()) { //댓글
			comment.setParent_reply_no(reply_no);
			comment.setReply_flag(EnableFlag.DISABLE.getCode());
		} else { //대댓글
			comment.setReply_flag(EnableFlag.ENABLE.getCode());
		}

		shopCommentDao.insert(comment);
	}

	@Override
	public void update(ShopComment comment) throws ServiceException {
		shopCommentDao.update(comment);
	}

	@Override
	public void updateDeleteFlag(ShopComment comment) throws ServiceException {
		comment.setContents("삭제된 글입니다.");
		shopCommentDao.updateDeleteFlag(comment);
	}

	@Override
	public int findByCreater(ShopComment comment) throws ServiceException {
		return shopCommentDao.findByCreater(comment);
	}
}

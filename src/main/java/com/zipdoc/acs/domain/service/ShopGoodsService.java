package com.zipdoc.acs.domain.service;


import com.zipdoc.acs.domain.entity.ShopGoods;
import com.zipdoc.acs.model.shop.ShopGoodsListRequest;

import java.util.List;
import java.util.Map;


public interface ShopGoodsService {

	int selectListTotalCount(ShopGoodsListRequest searchCondition) throws ServiceException;

	List<ShopGoods> selectList(ShopGoodsListRequest searchCondition) throws ServiceException;

	ShopGoods selectById(long goods_no) throws ServiceException;

	//2017-03-28 주문수 업데이트
	void updateIncreaseOrderCnt(Map<String, Object> condition) throws ServiceException;
}

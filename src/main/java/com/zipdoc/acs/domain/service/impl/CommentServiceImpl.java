package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.define.EnableFlag;
import com.zipdoc.acs.domain.entity.Comment;
import com.zipdoc.acs.domain.service.CommentService;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.CommentListRequest;
import com.zipdoc.acs.persistence.dao.CommentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
	
	@Autowired
	private CommentDao commentDao;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Comment> selectList(CommentListRequest searchCondition) throws ServiceException {
		return commentDao.selectList(searchCondition);
	}

	@Override
	public int selectListTotalCount(CommentListRequest searchCondition) throws ServiceException {
		return commentDao.selectListTotalCount(searchCondition);
	}

	@Override
	public Comment selectById(Comment comment) throws ServiceException {
		return commentDao.selectById(comment);
	}

	@Override
	public void insert(Comment comment) throws ServiceException {
		int reply_no = commentDao.selectNextReplyNo(comment);

		comment.setReply_no(reply_no);

		if(!comment.isRereply()) { //댓글
			comment.setParent_reply_no(reply_no);
			comment.setReply_flag(EnableFlag.DISABLE.getCode());
		} else { //대댓글
			comment.setReply_flag(EnableFlag.ENABLE.getCode());
		}

		commentDao.insert(comment);
	}

	@Override
	public void update(Comment comment) throws ServiceException {
		commentDao.update(comment);
	}

	@Override
	public void updateDeleteFlag(Comment comment) throws ServiceException {
		comment.setContents("삭제된 글입니다.");
		commentDao.updateDeleteFlag(comment);
	}

	@Override
	public void delete(Comment comment) throws ServiceException {
		commentDao.delete(comment);
	}
}

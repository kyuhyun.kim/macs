package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.ContractSideCall;
import com.zipdoc.acs.domain.service.ContractSideCallService;
import com.zipdoc.acs.persistence.dao.ContractSideCallDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContractSideCallServiceImpl implements ContractSideCallService {

    @Autowired
    private ContractSideCallDao contractSideCallDao;

    @Override
    public void createContractSideCall(ContractSideCall contractSideCall) {

        if(contractSideCall != null) {
            contractSideCallDao.createContractSideCall(contractSideCall);
        }
    }
}

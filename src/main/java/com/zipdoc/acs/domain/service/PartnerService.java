package com.zipdoc.acs.domain.service;

import java.util.List;
import java.util.Map;

import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.*;

import com.zipdoc.acs.model.partners.RecomPartners;
import com.zipdoc.acs.model.partners.RelaxPartners;
import org.springframework.dao.DataAccessException;

public interface PartnerService {

	/**
	 * 앱 버전 조회
	 * @param os_type
	 * @param appMarket
	 * @return
	 * @throws DataAccessException
	 */
	public AppVersion selectVersion(String os_type, String appMarket) throws DataAccessException;
	
	/**
	 * 디바이스 정보 삭제
	 * @param device_id
	 * @return
	 * @throws DataAccessException
	 */
	public int deleteDeviceByDeviceId(String device_id) throws DataAccessException;
	
	/**
	 * 파트너 정보를 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public Partner selectPartner(int partner_id) throws DataAccessException;
	
	/**
	 * 파트너가 가입한 상품의 정보를 조회한다.
	 * @param product_code
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerProduct selectProduct(Integer product_code) throws DataAccessException;
	
	/**
	 * 파트너의 누적 계약 수 및 매출액을 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerContractSummary summaryContract(int partner_id) throws DataAccessException;
	
	/**
	 * 사용자가 등록한 견적신청 정보를 조회
	 * @param estimate_no
	 * @return
	 * @throws DataAccessException
	 */
	public Estimate selectEstimate(long estimate_no) throws DataAccessException;
	
	/**
	 * 월별 견적 할당 건수를 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public int getAssignCount(int partner_id) throws DataAccessException;
	
	/**
	 * 당월 할당된 견적 중 주거용을 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public int getAssignResidentialCount(int partner_id) throws DataAccessException;
	
	/**
	 * 당월 할당된 견적 중 상업용을 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public int getAssignCommercialCount(int partner_id) throws DataAccessException;
	
	/**
	 * 당월 할당된 견적 중 무료견적 건수를 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public int getAssignFreeCount(int partner_id) throws DataAccessException;
	
	/**
	 * 월별 수락 누적 건수를 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public int getAssignAcceptCount(int partner_id) throws DataAccessException;
	
	/**
	 * 월별 거절 누적 건수를 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public int getAssignRejectCount(int partner_id) throws DataAccessException;
	
	/**
	 * 신규 의뢰 DB 건수를 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public int getAssignNewCount(int partner_id) throws DataAccessException;

	/**
	 * 신규 수락 DB 건수를 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public int getAssignNewAcceptCount(int partner_id) throws DataAccessException;

	/**
	 * 진행중인 DB 건수를 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public int getAssignOngoingCount(int partner_id) throws DataAccessException;
	
	/**
	 * 신규 할당된 견적 정보를 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerAssign> selectAssignNew(int partner_id) throws DataAccessException;
	
	/**
	 * 진행중인 견적 할당 정보를 조회한다.
	 * @param searchCondition
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerAssign> selectAssignOngoing(PartnerAssignSearchRequest searchCondition) throws DataAccessException;
	
	/**
	 * 계약중인 정보를 진행중 내역 조회시 사용할 포맷으로 조회한다.
	 * @param partner_id
	 * @param estimate_no
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerContract selectContractOngoing(int partner_id, long estimate_no) throws DataAccessException;
	
	/**
	 * 시공완료된 견적 할당 정보를 조회한다.
	 * @param searchCondition
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerAssign> selectAssignCompletion(PartnerAssignSearchRequest searchCondition) throws DataAccessException;
	
	/**
	 * 계약중인 정보를 공사 완료 조회시 사용할 포맷으로 조회한다.
	 * @param partner_id
	 * @param estimate_no
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerContract selectContractCompletion(int partner_id, long estimate_no) throws DataAccessException;
	
	/**
	 * 진행 중 철회한 견적을 조회한다.
	 * @param searchCondition
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerAssign> selectAssignWithdraw(PartnerAssignSearchRequest searchCondition) throws DataAccessException;


	/**
	 * 진행 중 철회한 견적을 조회한다.
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerAssignGeo> selectAssignWithdrawGeo(int partner_id) throws DataAccessException;

	/**
	 * 지정한 견적 정보를 조회한다.
	 * @param partner_id
	 * @param estimate_no
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerAssign selectAssign(int partner_id, long estimate_no) throws DataAccessException;
	
	/**
	 * 지정한 견적 상세 정보를 조회한다.
	 * @param partner_id
	 * @param estimate_no
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerAssign selectAssignDetail(int partner_id, long estimate_no) throws DataAccessException;
	
	/**
	 * 파트너에게 당월 할당된 견적 리스트를 조회한다.
	 * @param partner_id
	 * @param page
	 * @param limit
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerAssign> selectAssignList(int partner_id, int page, int limit) throws DataAccessException;
	
	/**
	 * 신규 의뢰 DB를 수락/거절/회수 하는 경우
	 * @param assign
	 * @return
	 * @throws DataAccessException
	 */
	public int acceptAssign(PartnerAssign assign) throws DataAccessException;
	
	/**
	 * 사용자가 입력한 정보로 견적 할당 정보를 업데이트 한다.
	 * @param partner_id
	 * @param counsel
	 * @return
	 * @throws DataAccessException
	 */
	public int updateAssignWithCounsel(int partner_id, PartnerAssignCounsel counsel) throws DataAccessException;
	
	/**
	 * 준단된 견적을 다시 진행한다.
	 * @param estimate_no
	 * @return
	 * @throws DataAccessException
	 */
	public int restoreAssign(int partner_id, long estimate_no) throws DataAccessException;
	
	/**
	 * 견적 의뢰 ID로 계약 정보 조회
	 * @param estimate_no
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerContract> selectContractList_estimateNo(long estimate_no) throws DataAccessException;
	
	/**
	 * 계약 정보 조회
	 * @param contract_no
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerContract selectContract(long contract_no) throws DataAccessException;	
	
	/**
	 * 계약 정보 조회
	 * @param partner_id
	 * @param estimate_no
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerContract selectContract_estimateNo(int partner_id, long estimate_no) throws DataAccessException;

	/**
	 * 취소계약 조회
	 * @param partner_id
	 * @param estimate_no
	 * @return
	 * @throws DataAccessException
	 */
	PartnerContract selectContract_Cancel(int partner_id, long estimate_no) throws DataAccessException;
	
	/**
	 * 계약 완료 시 설문으로 수집한 정보를 이용하여 계약 정보를 생성한다.
	 * @param contract
	 * @return
	 * @throws DataAccessException
	 */
	public long createContract(PartnerContract contract) throws DataAccessException;
	
	/**
	 * 협의 상태를 변경한다.
	 * @param contract
	 * @return
	 * @throws DataAccessException
	 */
	public int updateContract_agreementStatus(PartnerContract contract) throws DataAccessException;
	
	/**
	 * 계약 정보를 변경한다.
	 * @param modify
	 * @return
	 * @throws DataAccessException
	 */
	public int updateContract_app(PartnerContractModify modify) throws DataAccessException;
	
	/**
	 * 계약 정보를 공사 시작 상태로 변경한다.
	 * @param contract_no
	 * @return
	 * @throws DataAccessException
	 */
	public int updateContract_start(long contract_no, long estimate_no, long datetime) throws DataAccessException;
	
	/**
	 * 계약 정보를 공사 완료 상태로 변경한다.
	 * @param contract_no
	 * @return
	 * @throws DataAccessException
	 */
	public int updateContract_stop(long contract_no, long estimate_no, long datetime) throws DataAccessException;
	
	/**
	 * 견적 재진행을 위해 중단 정보를 삭제한다.
	 * @param contract_no
	 * @return
	 * @throws DataAccessException
	 */
	public int restoreContract(long contract_no) throws DataAccessException;
	
	/**
	 * 계약이 완료된 경우 T_ESTIMATE 테이블의 상태를 변경한다.
	 * @param estimate_no
	 * @param status
	 * @return
	 * @throws DataAccessException
	 */
	public int updateEstimate_contract(long estimate_no, int status) throws DataAccessException;
	
	/**
	 * 미납 수수료 내역을 조회한다.
	 * @param partner_id
	 * @param days
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerContract> selectUnpaidContract(int partner_id, int days) throws DataAccessException;
	
	/**
	 * 미납 건수와 미납 총액을 조회한다.
	 * @param partner_id
	 * @param days
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerContractSummary summaryUnpaidContract(int partner_id, int days) throws DataAccessException;
	
	/**
	 * 협의확인서를 등록한다.
	 * @param history
	 * @return
	 * @throws DataAccessException
	 */
	public int createContractHistory(PartnerContractHistory history) throws DataAccessException;
	
	/**
	 * 협의확인서 사진을 등록한다.
	 * @param file
	 * @return
	 * @throws DataAccessException
	 */
	public int createContractHistoryFile(PartnerContractHistoryFile file) throws DataAccessException;
	
	/**
	 * 계약번호로 등록된 협의확인서 리스트를 조회한다.
	 * @param contract_no
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerContractHistory> selectContractHistoryByContractNo(long contract_no) throws DataAccessException;
	
	/**
	 * 협의확인서 등록시 업로드한 사진 정보를 조회한다.
	 * @param history_no
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerContractHistoryFile> selectContractHistoryFile(long history_no) throws DataAccessException;
	
	/**
	 * 문의할 내용을 등록한다.
	 * @param inquiry
	 * @return
	 * @throws DataAccessException
	 */
	public int createInquiry(PartnerInquiry inquiry) throws DataAccessException;
	
	/**
	 * 문의 내용에 포함된 사진 정보를 등록한다.
	 * @param file
	 * @return
	 * @throws DataAccessException
	 */
	public int createInquiryFile(PartnerInquiryFile file) throws DataAccessException;
	
	/**
	 * 견적별 등록한 문의 내역을 조회한다.
	 * @param partner_id
	 * @param estimate_no
	 * @param page
	 * @param limit
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerInquiry> selectInquiry(int partner_id, long estimate_no, int page, int limit) throws DataAccessException;
	
	/**
	 * 견적별 등록한 문의 중 사진 정보를 조회한다.
	 * @param cno
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerInquiryFile> selectInquiryFile(long cno) throws DataAccessException;

	/**
	 * 견적번호에 할당된 파트너사 정보를 조회한다.
	 * @param estimate_no
	 * @return
	 * @throws DataAccessException
     */
	public List<PartnerAssign> selectAssignStopTargetList(long estimate_no, int partner_id) throws DataAccessException;

	/**
	 * 할당된 파트너사의 진행상태를 종료처리한다.
	 * @param partnerAssign
	 * @throws DataAccessException
     */
	public void updateAssignStop(PartnerAssign partnerAssign) throws DataAccessException;

	/**
	 * 계약 시 첨부된 계약 파일 정보를 갱신한다.
	 * @param contract
	 * @throws DataAccessException
     */
	public void updateContractFileInfo(PartnerContract contract) throws DataAccessException;

	/**
	 * 할당된 견적의 메모 정보를 수정한다.
	 * @param partnerAssign
	 * @throws DataAccessException
     */
	public void updateAssignMemo(PartnerAssign partnerAssign) throws DataAccessException;

	/**
	 * 할당된 견적의 재통화여부를 수정한다.
	 * @param partnerAssign
	 * @throws DataAccessException
     */
	public void updateAssignRetryCall(PartnerAssign partnerAssign) throws DataAccessException;
	
	/**
	 * 할당된 견적의 1차 통화상담 여부를 저장한다.
	 * @throws DataAccessException
     */
	public void insertCallCounselStatus(CallEstimate callEstimate) throws DataAccessException;

	/**
	 * 할당된 견적의 1차 통화상담 리스트를 가져온다.
	 * @param partner_id
	 * @throws DataAccessException
     */
	public List<Long> selectCallEstimateList(Integer partner_id, Long estimate_no) throws DataAccessException;

	/**
	 * 파트너스 리스트 조회시 업체 총 개수 조회
	 * @param searchCondition
	 * @return
	 * @throws DataAccessException
	 */
	public int selectListTotalCount(ListRequest searchCondition) throws DataAccessException;

	/**
	 * 파트너스 리스트 조회
	 * @param searchCondition
	 * @return
	 * @throws DataAccessException
	 */
	public List<PartnerFull> selectList(ListRequest searchCondition) throws DataAccessException;

	/**
	 * 파트너스 상세 조회
	 * @param partner_id
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerFull selectPartnerDetail(Integer partner_id) throws DataAccessException;

	/**
	 * 지역(영업장주소, 사업자등록주소)에 따른 파트너스 통계 조회
	 * - 총 업체수
	 * - 매월 1일 ~ 현재까지의 계약 통계(건수, 금액)
	 * - 계약상품 랭크
	 * - 업체 평가 점수 비율
	 * @param searchCondition
	 * @return
	 * @throws DataAccessException
	 */
	public PartnerStatsResponse selectPartnerStatsByArea(ListRequest searchCondition) throws DataAccessException;

	/**
	 * 2018. 05.24. KJB. 블랙리스트로 관리중인 전화번호 여부검사
	 * @param mobile_no
	 * @return
	 * @throws DataAccessException
	 */
	PartnerBlackList selectBlackList(String mobile_no) throws DataAccessException;

	// 2018.08.21. KJB. 베스트 파트너 조회
	List<Partner> selectBestPartner(Map<String, Object> map, int thumbnail_cnt) throws DataAccessException;

	// 2018.08.21. KJB. 파트너 목록 조회
	int selectPartnerListCount(PartnerListRequest condition) throws DataAccessException;
	List<Partner> selectPartnerList(PartnerListRequest condition) throws DataAccessException;

	int selectPartnerListCountRevision(Map<String, Object> condition) throws DataAccessException;
	List<Partner> selectPartnerListRevision(Map<String, Object> condition) throws DataAccessException;

	// 2019.01.10 KKH 월별 베스트 파트너스 조회
	List<Partner> selectMonthBestPartnerList(Map<String, Object> condition) throws DataAccessException;

	List<Partner> selectMonthRecoBestPartnerList (Map<String, Object> condition) throws DataAccessException;

	List<Partner> selectQuarterRecoBestPartnerList (Map<String, Object> condition) throws DataAccessException;

	List<RecomPartners> selectMonthBestPartner(Map<String, Object> condition) throws DataAccessException;

	List<RecomPartners> selectQuarterBestPartner(Map<String, Object> condition) throws DataAccessException;

	List<Partner> selectMonthPartnerList(Map<String, Object> condition) throws DataAccessException;

	List<Partner> selectQuarterPartnerList(Map<String, Object> condition) throws DataAccessException;

	int selectQuarterPartnerListCount(Map<String, Object> condition) throws DataAccessException;

	int selectMonthPartnerListCount(Map<String, Object> condition) throws DataAccessException;

	int selectReliefPartnerListCount() throws DataAccessException;

	List<Partner> selectReliefPartner(Map<String, Object> condition) throws DataAccessException;

	List<Partner> setPartnerProductThumbnails(List<Partner> list, int gallery_cnt, String category_code1);


}

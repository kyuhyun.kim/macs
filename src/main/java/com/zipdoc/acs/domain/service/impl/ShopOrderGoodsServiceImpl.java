package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.define.*;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.domain.entity.ShopGoodsOption;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.domain.service.ShopOrderGoodsService;
import com.zipdoc.acs.persistence.dao.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ShopOrderGoodsServiceImpl implements ShopOrderGoodsService{

	@Autowired
	private ShopOrderGoodsDao shopOrderGoodsDao;
	
	@Autowired
	private ShopGoodsOptionDao shopGoodsOptionDao;
	
	@Autowired
	private ShopGoodsDao shopGoodsDao;
	
	@Autowired
	private ShopOrderDao shopOrderDao;
	
	@Autowired
	private ShopOrderHandleDao shopOrderHandleDao;
	
	@Autowired
	private ShopCartDao shopCartDao;
	
	@Override
	public void createOrderGoods(ShopOrderGoods shopOrderGoods) throws ServiceException {
		ShopOrder shopOrder = shopOrderDao.selectByOrderNo(shopOrderGoods.getOrder_no());
		//2017-04-21 무통장입금 또는 계좌이체 일경우 입금대기중으로 수정
		if(shopOrder.getOrder_method() == 2 || shopOrder.getOrder_method() == 4){
			shopOrderGoods.setOrder_status(OrderGoodsStatus.DEPOSIT_WAIT.getCode());
		}else {
			shopOrderGoods.setOrder_status(OrderGoodsStatus.DELIVERY_WAIT.getCode());
		}
		shopOrderGoodsDao.createOrderGoods(shopOrderGoods);
		
		List<ShopGoodsOption> list = shopGoodsOptionDao.findList(shopOrderGoods.getGoods_no());
		Map<String, Object> params = new HashMap<>();
		params.put("goods_no", shopOrderGoods.getGoods_no());

		//무통장,입금대기인경우에는 재고량 로직 안태우게 수정
		if(shopOrder.getOrder_method() != 2 && shopOrder.getOrder_method() != 4){
			int index = 0;
			int stock_sum = 0;
			//옵션상품 재고량 업데이트
			if(list.size() > 0 || list != null){
				for(ShopGoodsOption shopGoodsOption  : list){
					int now_stock = shopGoodsOption.getStock_cnt();
					if(shopOrderGoods.getGoods_option_sno() == list.get(index).getSno()){
						shopGoodsOption.setSno(list.get(index).getSno());
						if(now_stock > 0 && (now_stock - shopOrderGoods.getGoods_option_cnt()) >= 0){
							shopGoodsOption.setStock_cnt(now_stock - shopOrderGoods.getGoods_option_cnt());
							shopGoodsOptionDao.updateStockCnt(shopGoodsOption);
							stock_sum += shopOrderGoods.getGoods_option_cnt();
						}

						if(now_stock - shopOrderGoods.getGoods_option_cnt() == 0 || now_stock - shopOrderGoods.getGoods_option_cnt() < 0){
							//재고량이 0인 상품옵션일 때 모든 장바구니에서 해당 옵션삭제
							shopCartDao.deleteBecauseNoStock(list.get(index).getSno());
						}
					}
					index++;

					if(stock_sum > 0){
						params.put("stock_sum", stock_sum);
					}
//				shopGoodsDao.updateStockCnt(params);
				}
			}

			//단품상품 재고량 업데이트
			if(shopOrderGoods.getGoods_option_sno() == 0){
				ShopGoods shopGoods = shopGoodsDao.selectByGoodsNo(shopOrderGoods.getGoods_no());
				int now_stock = shopGoods.getUse_stock();
				if(now_stock > 0){
					stock_sum = shopOrderGoods.getGoods_option_cnt();
				}

				if(now_stock - shopOrderGoods.getGoods_option_cnt() == 0 || now_stock - shopOrderGoods.getGoods_option_cnt() < 0){
					shopCartDao.deleteBecauseSingleGoodsNoStock(shopOrderGoods.getGoods_no());
				}

				if(stock_sum > 0){
					params.put("stock_sum", stock_sum);
				}
			}

			shopGoodsDao.updateStockCnt(params);
		}


	}

	@Override
	public List<ShopOrderGoods> findList(String order_no) throws ServiceException {
		return shopOrderGoodsDao.findList(order_no);
	}

	@Override
	public ShopOrderGoods findById(Long sno) throws ServiceException {
		return shopOrderGoodsDao.findById(sno);
	}

	@Override
	public void updatePurchasComplete(Map<String,Object> condition) throws ServiceException {
		shopOrderGoodsDao.updatePurchaseComplete(condition);

		Long sno = (Long)condition.get("sno");
		Long member_no = (Long)condition.get("member_no");

		int status = (int)condition.get("status");


		if(status != 6){
			if(status == 5) {
				ShopOrderHandle shopOrderHandle = shopOrderHandleDao.findBySno(sno);
				shopOrderHandleDao.delete(shopOrderHandle.getSno());
				return;
			}
			ShopOrderGoods shopOrderGoods = shopOrderGoodsDao.findById(sno);
			ShopOrderHandle shopOrderHandle = new ShopOrderHandle();
			shopOrderHandle.setOrder_no(shopOrderGoods.getOrder_no());
			shopOrderHandle.setOrder_goods_sno(shopOrderGoods.getSno());
			shopOrderHandle.setRefund_price(shopOrderGoods.getGoods_price());
			shopOrderHandle.setHandle_complete_status(OrderHandleCompleteStatus.READY.getCode());
			//배송준비중 -> 주문취소 처리
			if(status == 8){
				shopOrderHandle.setHandle_mode(OrderHandleStatus.CANCLE.getCode());
			}
			
			if(member_no!=null){
				shopOrderHandle.setCreater(member_no);
			}else{
				shopOrderHandle.setCreater(Constants.NON_MEMBER);
			}
			
			shopOrderHandleDao.create(shopOrderHandle);
		}
	}

	@Override
	public OrderSummary orderSummary(Long member_no) throws ServiceException {
		return shopOrderGoodsDao.findSummary(member_no);
	}

}

package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.define.AgentType;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.EstimateListRequest;
import com.zipdoc.acs.model.ListRequest;
import com.zipdoc.acs.model.MyInteriorListResponse;
import com.zipdoc.acs.model.subs.ZID;
import org.springframework.dao.DataAccessException;
import java.util.List;

public interface EstimateService {
	EstimateSummary selectEstimateSummary() throws DataAccessException;
	int createEstimate(Estimate estimate) throws DataAccessException;

	// 마이인테리어 목록
	MyInteriorListResponse selectMyInteriorEstimateList(String writer, String phone_no) throws DataAccessException;
	MyInteriorListResponse selectMyInteriorEstimateList(Long member_no, AgentType agentType) throws DataAccessException;
	MyInteriorListResponse selectMyInteriorEstimateList(EstimateListRequest searchCondition) throws DataAccessException;
	MyInteriorListResponse selectMyMoveEstimateList(EstimateListRequest searchCondition) throws DataAccessException;



	List<Product> selectEstimateConceptProductList(long estimate_no) throws DataAccessException;

	int selectListTotalCount(ListRequest searchCondition) throws DataAccessException;

	List<Estimate> selectEstimateList(ListRequest searchCondition) throws DataAccessException;

	List<EstimateCounsel> findCounselList(Long estimate_no) throws DataAccessException;

	// 2018.08.03. KJB. 견적 프로모션 코드 조회
	PromotionCode selectPromotionCode(String promotion_code) throws DataAccessException;

	// 마이인테리어 상세조회
	MyEstimate selectMyInterior(long member_no, long estimate_no, AgentType agentType) throws DataAccessException;
	MyEstimate selectMyInterior(EstimateListRequest searchCondition, long estimate_no) throws DataAccessException;

	// 내 견적 등록
	void updateEstimateOwner(EstimateListRequest searchCondition) throws DataAccessException;

	// 로그인 시 회원 견적 전환
	void updateEstimateMember(ZID zid) throws DataAccessException;
}

package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.entity.ContractSideHistory;
import com.zipdoc.acs.domain.service.ContractSideHistoryService;
import com.zipdoc.acs.persistence.dao.ContractSideHistoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dskim on 2017. 9. 22..
 */
@Service
public class ContractSideHistoryServiceImpl implements ContractSideHistoryService {

    @Autowired
    private ContractSideHistoryDao contractSideHistoryDao;

//    @Override
//    public int createContractSideHistory(ContractSideHistory contractSideHistory) throws DataAccessException {
//
//        int count = contractSideHistoryDao.createContractSideHistory(contractSideHistory);
//
//        return count;
//    }


    @Override
    public void createContractSideHistory(List<ContractSideHistory> contractSideHistoryList) throws DataAccessException {

        if(contractSideHistoryList != null && contractSideHistoryList.size() > 0) {
            for(ContractSideHistory contractSideHistory : contractSideHistoryList) {
                contractSideHistoryDao.createContractSideHistory(contractSideHistory);
            }

        }


    }
}

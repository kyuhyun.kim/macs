package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.define.AgentType;
import com.zipdoc.acs.define.CategoryCode;
import com.zipdoc.acs.model.HomeScreenLayout;

public interface HomeScreenService {

    // 주거공간 및 상업공간에 따른 홈메인화면 표시 정보 조회 (캐쉬 처리)
    HomeScreenLayout getCashedHomeScreenLayout(CategoryCode category, AgentType agentType) throws Exception;

    // 주거공간 및 상업공간에 따른 홈메인화면 표시 정보 조회
    HomeScreenLayout getHomeScreenLayout(CategoryCode category, AgentType agentType) throws Exception;

    // 파트너스 홈메인화면 표시 정보 조회
    HomeScreenLayout getHomeScreenLayoutForPartners(AgentType agentType) throws Exception;

    // 매거진 홈메인화면 표시 정보 조회
    HomeScreenLayout getHomeScreenLayoutForMagazine(AgentType agentType) throws Exception;

    // 집닥혜택 홈메인화면 표시 정보 조회
    HomeScreenLayout getHomeScreenLayoutForBenefit(AgentType agentType) throws Exception;
}



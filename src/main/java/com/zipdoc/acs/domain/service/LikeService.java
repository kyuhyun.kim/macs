package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.Like;
import com.zipdoc.acs.domain.entity.Whatever;
import com.zipdoc.acs.model.WhateverListRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface LikeService {
	public void create(Like like) throws DataAccessException;
	public void delete(Like like) throws DataAccessException;
}

package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.CrashLog;
import org.springframework.dao.DataAccessException;

public interface UaBlockService {
	boolean isContainKeyword(String user_agent) throws DataAccessException;
}


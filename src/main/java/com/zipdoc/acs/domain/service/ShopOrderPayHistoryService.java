package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.ShopOrderPayHistory;

public interface ShopOrderPayHistoryService {
	void createOrderPayHistory(ShopOrderPayHistory shopOrderPayHistory) throws ServiceException;
}

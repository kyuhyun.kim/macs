package com.zipdoc.acs.domain.service;


/**
 * cafe24 sms 모듈
 */
public interface SMSService {


    //인증번호 생성
    String tempKeyGenarator(String phone) throws ServiceException;

    //인풋키와 임시키를 비교하여 일치하는지 반환
    String isCorrectCertifiKey(String phone,String temp_key,String current_timestamp) throws ServiceException;

}

package com.zipdoc.acs.domain.service.impl;

import com.zipdoc.acs.domain.service.KeywordSynonymService;
import com.zipdoc.acs.persistence.dao.KeywordSynonymDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 * Created by ZIPDOC on 2016-06-02.
 */
@Service
public class KeywordSynonymServiceImpl implements KeywordSynonymService {

    @Autowired
    private KeywordSynonymDao keywordSynonymDao;


    @Override
    public String selectKeywordSynonym(String keyword) throws DataAccessException
    {
        return keywordSynonymDao.selectKeywordSynonym(keyword);
    }

}

package com.zipdoc.acs.domain.service;

import com.zipdoc.acs.domain.entity.*;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface ContractService {
	public List<ContractFile> selectContractFileList(long contract_no) throws DataAccessException;
	List<ContractFile> selectContractFileListAll(long contract_no) throws DataAccessException;
	public void createContractFile(ContractFile contractFile) throws DataAccessException;
	public void deleteContractFile(long file_no) throws DataAccessException;
	public ContractFile selectContractFile(long file_no) throws DataAccessException;
	public void createContractHistoryFile(ContractFileHistory contractFileHistory) throws DataAccessException;

	// 2018.05.10. KJB. 집닥맨 앱 제공 API. - 계약 건수 조회
	int findListCountContractVisit(Map<String, Object> condition) throws DataAccessException;

	// 2018.05.10. KJB. 집닥맨 앱 제공 API. - 계약 조회
	List<ContractVisit> findListContractVisit(Map<String, Object> condition) throws DataAccessException;

	// 2018.05.10. KJB. 집닥맨 앱 제공 API. - 계약 업데이트
	int updateContractVisit(ContractVisit condition) throws DataAccessException;

	// 2018.06.05. KJB. 계약 조회
	Contract findById(long contract_no) throws DataAccessException;

	// 2018.08.03. KJB. 집닥맨 앱 제공 API. - 현장방문이력 관리 (조회,등록,업데이트)
	SiteVisitHistory selectVisitHistory(long history_no) throws DataAccessException;
	void createVisitHistory(SiteVisitHistory history) throws DataAccessException;
	void updateVisitHistory(SiteVisitHistory history) throws DataAccessException;
	SiteVisitHistory selectLastSupervisionHistory(long contract_no) throws DataAccessException;

	// 2018.09.03. KJB. 계약정보 조회
	MyContract selectMyContract(long contract_no) throws DataAccessException;
}

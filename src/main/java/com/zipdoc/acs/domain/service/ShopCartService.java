package com.zipdoc.acs.domain.service;

import java.util.List;
import java.util.Map;

import com.zipdoc.acs.domain.entity.ShopCart;
import com.zipdoc.acs.domain.service.ServiceException;
import com.zipdoc.acs.model.shop.ShopCartRequest;

public interface ShopCartService {
	public List<ShopCart> findList(ShopCartRequest cartRequest) throws ServiceException;

	public int findListCount(ShopCartRequest cartRequest) throws ServiceException;
	
	void create(ShopCart shopCart) throws ServiceException;

	void delete(Map<String, Object> condition) throws ServiceException;

	ShopCart findById(Map<String, Object> condition) throws ServiceException;

	void updateCnt(Map<String, Object> condition) throws ServiceException;

	void deleteCartAll(Map<String, Object> condition) throws ServiceException;

	void updateOption(Map<String, Object> condition) throws ServiceException;

	public void deleteCartMulti(ShopCart cart) throws ServiceException;

}

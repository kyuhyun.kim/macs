package com.zipdoc.acs.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

public class LogInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = LoggerFactory.getLogger(LogInterceptor.class);
    private final static String LINE = "\r\n";
    //private final static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    //private static String ID;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }

    @PostConstruct
    public void initialize() {
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	
		long startTime = System.currentTimeMillis();
		request.setAttribute("startTime", startTime);
		
        String message =
                		(LINE +
                        "--------------------------------------------------------------------------------" +
                        LINE +
                        toStringRequest(request) +
                        "================================================================================" +
                        LINE);

        log.debug(message);

        return super.preHandle(request, response, handler);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    	
        long startTime = (long) request.getAttribute("startTime");
        long endTime = System.currentTimeMillis();
        long executeTime = endTime - startTime;

        if (ex != null) {
        	log.error("error: ", ex);
        }
        String message =
                		(
                        "CODE=" + response.getStatus() + ". execute time: " + executeTime + "ms" +
                                LINE +
                                "--------------------------------------------------------------------------------" +
                                LINE +
                                toStringResponse(request, response) +
                                "================================================================================" +
                                LINE);

        log.debug(message);

        super.afterCompletion(request, response, handler, ex);
    }

    private String toStringRequest(HttpServletRequest request) {

        StringBuilder buffer = new StringBuilder();

        // method
        buffer.append(request.getMethod()).append(" ");

        // request-uri
        buffer.append(request.getRequestURI());

        // query string
        String queryString = request.getQueryString();
        if (queryString != null) {
            buffer.append("?").append(queryString);
        }
        buffer.append(" ").append(request.getProtocol()).append(LINE);

        // headers
        Enumeration<String> headers = request.getHeaderNames();
        String name;
        Enumeration<String> values;
        while (headers != null && headers.hasMoreElements()) {
            name = headers.nextElement();
            buffer.append(name).append(": ");
            values = request.getHeaders(name);
            while (values != null && values.hasMoreElements()) {
                buffer.append(values.nextElement());
                if (values.hasMoreElements()) {
                    buffer.append(",");
                }
            }
            buffer.append(LINE);
        }

        if (request.getContentLength() > 0) {
        	buffer.append(LINE + "(...)" + LINE);
        }

        return buffer.toString();
    }

    private String toStringResponse(HttpServletRequest request, HttpServletResponse response) {

        StringBuilder buffer = new StringBuilder();

        // status line
        buffer.append(request.getProtocol()).append(" ").append(response.getStatus()).append(" ").append(HttpStatus.valueOf(response.getStatus()).name()).append(LINE);

        // headers
        Collection<String> tmp = response.getHeaderNames();
        if (tmp != null) {
            Iterator<String> headers = tmp.iterator();
            String name;
            Iterator<String> values;
            while (headers.hasNext()) {
                name = headers.next();
                buffer.append(name).append(": ");
                if ((tmp = response.getHeaders(name)) != null) {
                    values = tmp.iterator();
                    while (values.hasNext()) {
                        buffer.append(values.next());
                        if (values.hasNext()) {
                            buffer.append(",");
                        }
                    }
                }
                buffer.append(LINE);
            }
        }

        // content
        String content = (String) request.getAttribute("content.response");
        if (content != null) {
            try {
                buffer.append(LINE);
                buffer.append(content);
                buffer.append(LINE);
            } catch (Exception e) {
                log.error("", e);
            }
        } else {

            String clen = response.getHeader("content-length");
            if (clen != null) {
                try {
                    int contentLength = Integer.parseInt(clen);
                    if (contentLength > 0) {
                        buffer.append(LINE + "(...)" + LINE);
                    }
                } catch (Exception ignored) {
                }
            }
        }

        return buffer.toString();
    }

    /**
     * 각 모듈 별로 HTTP Request 메시지에서 발신번호/착신번호 및 요청 구분자를 분석한다.
     * 스토리서비스의 경우에는 발신번호는 /user/{origination}에 포함되고 착신번호는 /destination/{destination}에 포함된다.
     * 요청 구분자는 user가 검색되는 경우에는 /user/{origination}/ 뒤에 포함되고
     * user가 없는 경우에는 /story/ 뒤에 포함된다.
     *
     * @param request 서블릿 요청 객체
     * @return Trace 정보
     */
//    private String[] getTraceInfo(HttpServletRequest request) {
//    	
//        String[] result = new String[3];
//
//        result[1] = request.getHeader("ZID");
//
//        if (request.getRequestURI().startsWith("/acs/join")) {
//            result[0] = "JOIN";
//        } else {
//        	result[0] = "UNKNOWN";
//        }
//
//        return result;
//    }
}

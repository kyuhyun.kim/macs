package com.zipdoc.acs.define;

public enum OrderGoodsStatus {
	DEPOSIT_WAIT(1,   	"입금대기중"),
	PAYMENT_COMPLETE(2, "결제완료"),
	DELIVERY_WAIT(3, "배송준비중"),
	DELIVERY_ING(4, "배송중"),
	DELIVERY_COMPLETE(5, "배송완료"),
	PURCHASE_COMPLETE(6, "구매확정"),
	REFUND(7, "반품/교환"),
	CANCLE(8, "주문취소")
	;

	private int code;
    private String name;
    OrderGoodsStatus(int code, String name){
        this.code = code;
        this.name = name;
    }  

	public int getCode() {
		return code;
	}
    public String getName(){
        return name;
    }
	public static OrderGoodsStatus get(int code){
		for(OrderGoodsStatus item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

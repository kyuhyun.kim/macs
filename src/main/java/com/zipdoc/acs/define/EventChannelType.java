package com.zipdoc.acs.define;

public enum EventChannelType {
	APP(1, "집닥앱"),
	PARTNER_APP(2, "파트너스앱"),
	WEB(3, "홈페이지"),
	APP_COMMERCE(4, "집닥앱/상업"),
	APP_RESIDENT(5, "집닥앱/주거"),
	WEB_RESIDENT(6, "웹/주거"),
	WEB_COMMERCE(7, "웹/상업"),
	WEB_PARTNERS(8, "웹/파트너스"),
	WEB_MAGAZINE(9, "웹/매거진"),
	WEB_BENEFIT(10, "웹/집닥혜택"),
	;

	private int code;
    private String name;
    EventChannelType(int code, String name) {
        this.code = code;
        this.name = name;
    }  

	public int getCode() {
		return code;
	}
    public String getName(){
        return name;
    }
	public static EventChannelType get(int code){
		for(EventChannelType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

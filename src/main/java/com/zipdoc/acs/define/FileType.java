package com.zipdoc.acs.define;

import org.apache.commons.lang.StringUtils;

public enum FileType {
	IMAGE ("00"),
	ETC   ("01"),
	MOVIE	("10"),
	;
	private final String value;

	private FileType(final String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}

	public static FileType find(String value) {
		for (final FileType type : values()) {
			if (StringUtils.equals(type.value, value)) return type;
		}
		return ETC;
	}
}

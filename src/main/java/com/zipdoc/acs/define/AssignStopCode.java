package com.zipdoc.acs.define;

public enum AssignStopCode {

	/**
	 * 미 중단 상태
	 */
	NO_CODE 				(0),
	
	/**
	 * 견적 금액이 맞지 않음
	 */
	PRICE				 	(1),
	
	/**
	 * 고객의 변심
	 */
	CUSTOMER			 	(2),
	
	/**
	 * 업체 사정
	 */
	PARTNER			 		(3),
	
	/**
	 * 컨셉이 맞지 않음
	 */
	CONCEPT			 		(3),	
	
	/**
	 * 기타 사유
	 */
	ETC			 			(999),	
	
	/**
	 * 이미 다른 파트너와 계약된 경우
	 */
	ALREADY_CONTRACT		(1000),
	;
	
	private final int value;
	
	private AssignStopCode(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static AssignStopCode valueOf(int value) {
        
		for (final AssignStopCode type : values()) {
			if (type.value == value) return type;
		}
		return NO_CODE;
	}
}

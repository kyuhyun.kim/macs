package com.zipdoc.acs.define;

public enum Cause {
	SUCCESS	 					(0),
	INVALID_PWD_USER			(1),
	INVALID_PWD_CMS				(2),
	INVALID_MOBILE_NO			(3),
	INVALID_PARAMETERS			(4),
	CONFLICT_MEMBER_ID			(5),
	UNKNOWN_ERROR				(999),
	;

	private final int value;

	Cause(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static Cause valueOf(int value) {
        
		for (final Cause type : values()) {
			if (type.value == value) return type;
		}
		return UNKNOWN_ERROR;
	}
}

package com.zipdoc.acs.define;

public enum AgreementStatus {
	READY(0,   			"협의예정"),
	REQ_AGREEMENT(1, 	"협의요청"),
	REQ_ADJUST(2,  	"정정요청"),
	COMPLETED(3,  		"협의완료"),
	;

	private int code;
	private String name;

	AgreementStatus(int code, String name){
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}
	public String getName(){
		return name;
	}
	public static AgreementStatus get(int code){
		for(AgreementStatus item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return READY;
	}
}

package com.zipdoc.acs.define;

import org.apache.commons.lang.StringUtils;

public enum VividCommentCategoryCode {
    전체("00",  "전체"),
    주거("10", "주거공간"),
    상업("20", "상업공간"),
    부분("30", "부분시공"),
    ;

    private String code;
    private String name;
    VividCommentCategoryCode(String code,  String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }

    public static VividCommentCategoryCode get(String code) {
        for (VividCommentCategoryCode item : VividCommentCategoryCode.values()) {
            if ( StringUtils.equals(code, item.getCode())) {
                return item;
            }
        }
        return null;
    }
}

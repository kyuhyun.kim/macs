package com.zipdoc.acs.define;

public enum MemberType {
	USER(1,   	"개인 회원", "USER"),
	PARTNER(2, "파트너 회원", "PARTNER"),
	;

	private int code;
	private String name;
	private String grade;

	MemberType(int code, String name, String grade){
		this.code = code;
		this.name = name;
		this.grade = grade;
	}

	public int getCode() {
		return code;
	}
	public String getName(){
		return name;
	}

	public String getGrade() {
		return grade;
	}

	public static MemberType get(int code){
		for(MemberType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return USER;
	}
}
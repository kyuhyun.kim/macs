package com.zipdoc.acs.define;

import org.apache.commons.lang.StringUtils;

public enum  SMSStatus {

    SUCCESS("success",   "즉시 전송 성공"),
    RESERVED("reserved", "예약 성공"),
    NUMBER_FORMAT_ERROR("3205", "번호형식 오류"),
    TEST_SUCCESS("Test Success!", "테스트 성공");


    private String code;
    private String name;
    SMSStatus(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }
    public String getName(){
        return name;
    }
    public static SMSStatus get(String code){
        for(SMSStatus item : values()){
            if(StringUtils.equals(code, item.getCode())){
                return item;
            }
        }
        return null;
    }
}

package com.zipdoc.acs.define;


public enum LivingType {
    LIVING(0, "선택 없음"),
    SCHEDULE(1, "사다리차 필요"),
    PRE_CONTRACT(2, "엘리베이터 없음/이용불가"),
    ALL(3, "사다리차 필요 & 엘리베이터 없음/이용불가"),
    ;

    private int code;
    private String name;

    LivingType(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static LivingType get(int code){
        for(LivingType item : values()){
            if(code == item.getCode()){
                return item;
            }
        }
        return null;
    }
}

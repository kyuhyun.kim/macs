package com.zipdoc.acs.define.pns;

import org.apache.commons.lang.StringUtils;

public enum MsgEnableType {

	NOK("0"),
	OK("1"),
	UNDEF("undef");

	private final String value;

	private MsgEnableType(final String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}

	public static MsgEnableType find(String value) {
		for (final MsgEnableType type : values()) {
			if (StringUtils.equals(type.value, value)) return type;
		}
		return UNDEF;
	}
}

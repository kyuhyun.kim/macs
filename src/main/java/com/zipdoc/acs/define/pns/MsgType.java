package com.zipdoc.acs.define.pns;

public enum MsgType {

	SMS(1,		"1"),
	LMS(2,		"2"),
	MMS(3,		"3");


	private int code;
	private String name;
	MsgType(int code, String name){
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}
	public String getName(){
		return name;
	}
	public static MsgType get(int code){
		for(MsgType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

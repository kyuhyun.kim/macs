package com.zipdoc.acs.define;

public enum PaymentStatus {
	READY(1,   	"미결제"),
	PAID(2, "결제완료"),
	CANCELLED(3, "결제취소"),
	FAILED(4, "결제실패")
	;

	private int code;
    private String name;
    PaymentStatus(int code, String name){
        this.code = code;
        this.name = name;
    }  

	public int getCode() {
		return code;
	}
    public String getName(){
        return name;
    }
	public static PaymentStatus get(int code){
		for(PaymentStatus item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

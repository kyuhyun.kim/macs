package com.zipdoc.acs.define;

/**
 * Created by moonji on 2017-11-08.
 */
public enum EstimateType {
    SIMPLE_TYPE(1, "간편"),
    DETAIL_TYPE(2, "상세")
    ;

    private int code;
    private String name;
    EstimateType(int code, String name){
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }
    public String getName(){
        return name;
    }
    public static EstimateType get(int code){
        for(EstimateType item : values()){
            if(code == item.getCode()){
                return item;
            }
        }
        return null;
    }
}

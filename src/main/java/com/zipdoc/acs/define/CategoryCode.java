package com.zipdoc.acs.define;

import org.apache.commons.lang.StringUtils;

public enum CategoryCode {
    RESIDENT("10", "00", "주거공간"),
    COMMERCE("20", "00", "상업공간"),
    ;

    private String code;
    private String type;
    private String name;
    CategoryCode(String code, String type, String name) {
        this.code = code;
        this.name = name;
        this.type = type;
    }

    public String getCode() {
        return code;
    }
    public String getType() {
        return type;
    }
    public String getName() {
        return name;
    }

    public static CategoryCode get(String code) {
        for (CategoryCode item : CategoryCode.values()) {
            if ( StringUtils.equals(code, item.getCode())) {
                return item;
            }
        }
        return null;
    }
}

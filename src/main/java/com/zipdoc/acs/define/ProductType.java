package com.zipdoc.acs.define;

public enum ProductType {
	GALLERY(0, "갤러리"),
	COMMUNITY(1, "커뮤니티")
	;

	private int code;
    private String name;
	ProductType(int code, String name){
        this.code = code;
        this.name = name;
    }  

	public int getCode() {
		return code;
	}
    public String getName(){
        return name;
    }
	public static ProductType get(int code){
		for(ProductType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

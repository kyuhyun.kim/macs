package com.zipdoc.acs.define;

import org.apache.commons.lang.StringUtils;

public enum ArticleType {
	NOTICE ("notice"),
	FAQ("faq"),
	NEWS("news"),
	INTERIOR_TIP("interior-tip"),
	EVENT("event"),
	PARTNER_NOTICE ("partner_notice"),
	ZIPDOCMAN ("zipdocman"),
	UNDEF("undef")
	;
	private final String value;

	private ArticleType(final String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}

	public static ArticleType find(String value) {
		for (final ArticleType type : values()) {
			if (StringUtils.equals(type.value, value)) return type;
		}
		return UNDEF;
	}
}

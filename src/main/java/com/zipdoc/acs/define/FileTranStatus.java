package com.zipdoc.acs.define;

public enum FileTranStatus {
	NOT_YET	(0),
	COMPLETED	(1),
	FAILED (9)
	;

	private int code;
	FileTranStatus(int code){
        this.code = code;
    }  

	public int getCode() {
		return code;
	}
	public static FileTranStatus get(int code){
		for(FileTranStatus item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return NOT_YET;
	}
}

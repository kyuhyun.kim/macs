package com.zipdoc.acs.define;

public enum LayoutCategory {

    ALL(0, "전체(주거/상업"),
    RESIDENT(1, "주거공간"),
    COMMERCE(2, "상업공간"),
    PARTNERS(3,	"파트너스"),
    MAGAZINE(4,	"매거진"),
    ;

    private int code;
    private String name;

    LayoutCategory(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }
    public String getName() {
        return name;
    }

    public static LayoutCategory get(int code) {
        for (LayoutCategory item : LayoutCategory.values()) {
            if (code == item.getCode()) {
                return item;
            }
        }
        return null;
    }

    public static LayoutCategory get(CategoryCode category) {

        switch (category) {
            case RESIDENT: return RESIDENT;
            case COMMERCE: return COMMERCE;
            default: return RESIDENT;
        }
    }
}

package com.zipdoc.acs.define.agreement;

public enum AgreementStatus {

	동의(0,				"0"),
	철회_거부(1,		"1"),
	ETC(2,				"2");


	private int code;
	private String name;
	AgreementStatus(int code, String name){
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}
	public String getName(){
		return name;
	}
	public static AgreementStatus get(int code){
		for(AgreementStatus item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

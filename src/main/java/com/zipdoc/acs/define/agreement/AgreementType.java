package com.zipdoc.acs.define.agreement;

public enum AgreementType {

	알림문자(0,			"0"),
	이벤트약관(1,		"1"),
	기타(2,				"2");


	private int code;
	private String name;
	AgreementType(int code, String name){
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}
	public String getName(){
		return name;
	}
	public static AgreementType get(int code){
		for(AgreementType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

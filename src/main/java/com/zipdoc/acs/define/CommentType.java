package com.zipdoc.acs.define;

public enum CommentType {
	GALLERY(0, 			"T_PRODUCT_REPLY",	1),
	COMMUNITY(1, 		"T_PRODUCT_REPLY",	1),
	WHATEVER(2, 		"T_WHATEVER_REPLY",	2),
	ZIPDOC_REPLY(3, 	"T_REPLY_REPLY",		1),
	INTERIOR_TIP(4, 	"T_INTERIOR_TIP_REPLY",	1),
	ZIPDOCMAN(5, 		"T_ZIPDOCMAN_REPLY",	1),
	SHOP_REVIEW(6, 		"T_SHOP_GOODS_REVIEW",	1),
	SHOP_QNA(7, 		"T_SHOP_GOODS_QNA",	1)
	;

	private int code;
    private String name;
	private int sort;
    CommentType(int code, String name, int sort){
        this.code = code;
        this.name = name;
		this.sort = sort;
    }  

	public int getCode() {
		return code;
	}
    public String getName(){
        return name;
    }
	public int getSort() {
		return sort;
	}
	public static CommentType get(int code){
		for(CommentType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

package com.zipdoc.acs.define;

public enum OrderMethodType {
	CREDIT_CARD(1, "신용카드"),
	NON_BANKBOOK(2, "무통장입금"),
	PHONE_PAYMENT(3, "핸드폰결제"),
	BANK_TRANSFER(4, "계좌이체");
	
	private int code;
	private String name;
	
	private OrderMethodType(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
	
	public static OrderMethodType get(int code){
		for(OrderMethodType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

package com.zipdoc.acs.define;

public enum PartnerStatus {
    READY(0, "가계약"),
    ING(1, "계약중"),
    PAUSE(2, "일시중지"),
    END(3, "계약종료")
    ;

    private int code;
    private String name;
    PartnerStatus(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}

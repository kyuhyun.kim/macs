package com.zipdoc.acs.define;

public enum PictureType {

	RECT	(240, 240, true),
	MDPI 	(360, 240, true),
	HDPI 	(540, 360, true),
    XHDPI	(720, 480, true),
    XXHDPI	(1080, 720, false),
	XXXHDPI (1440, 960, false)
	;

	private final int width;
	private final int height;
	private final String prefix;
	private final boolean crop;

	private PictureType(final int width, final int height, final boolean crop) {
		this.width = width;
		this.height = height;
		this.prefix = width + "x" + height + "_";
		this.crop = crop;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public String getPrefix() {
		return prefix;
	}

	public boolean isCrop() {
		return crop;
	}

}

package com.zipdoc.acs.define;

/**
 * Created by moonji on 2017-11-08.
 */
public enum EstimateStepType {

    PARTNER_ACCEPT(1),
    PARTNER_CALL_COMPLETE(2),
    ESTIMATE_CLOSE(3),
    COMPLETE_CONTRACT(4),
    CONSTRUCT_START(5),
    VISIT_SCHEDULED(6),
    VISIT_DONE(7),
    CONSTRUCT_END(8),
    CONTRACT_PRICE_ADD(10),
    CONTRACT_PRICE_REDUCE(11),
    CONTRACT_PRICE_CANCEL(12),
    ;

    private int code;

    EstimateStepType(Integer code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static EstimateStepType get(int code) {
        for (EstimateStepType item : EstimateStepType.values()) {
            if (code == item.getCode()) {
                return item;
            }
        }
        return null;
    }
}

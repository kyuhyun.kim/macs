package com.zipdoc.acs.define;


public enum InquireType {
	TYPE_ONE(1, "파트너스 관련 문의"),
	TYPE_TWO(2, "계약 진행 중 문의"),
	TYPE_THREE(3, "공사 진행 중 문의"),
	TYPE_FOUR(4, "공사 후 문의"),
	TYPE_ETC(5, "기타 문의");
	
	private int code;
	private String name;
	
	InquireType(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	
	public static InquireType get(int code){
		for(InquireType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

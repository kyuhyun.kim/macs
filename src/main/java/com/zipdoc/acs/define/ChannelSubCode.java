package com.zipdoc.acs.define;

/**
 * Created by 이동규 on 2016-09-12.
 */
public enum ChannelSubCode {
    WEB(1,				"웹"),
    APP_ANDROID(2,		"안드로이드"),
    HAPPY(3,			"해피콜"),
    SEARCH(4,			"검색"),
    SNS(5,				"SNS"),
    EX_AD(6,			"옥외광고"),
    INTRO(7,			"소개"),
    APP_IPHONE(8,		"아이폰"),
    BAND(100,			"밴드"),
    OLD_WORLD(101,		"중고나라"),
    CONTENTS_CLUSTER(102,		"컨텐츠클러스터"),
    CUBE(106,		    "마케팅큐브"),
    KAKAO(110,          "카카오"),
    OKCASHBAG(120,      "OKCASHBAG"),
    FACEBOOK(130,       "페북"),
    REPORTER(140,       "언론사네트워크"),
    KCA(160,        "KCA"),
    LABS1(170,        "케어랩스(1)"),
    INNO(180,       "이노아이티"),
    MIXNFIX(190,    "믹스앤픽스"),
    ETC(999,        "기타")
    ;


    private int code;
    private String name;
    ChannelSubCode(int code, String name){
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }
    public String getName(){
        return name;
    }
    public static ChannelSubCode get(int code){
        for(ChannelSubCode item : values()){
            if(code == item.getCode()){
                return item;
            }
        }
        return null;
    }
}

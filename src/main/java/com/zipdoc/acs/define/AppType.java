package com.zipdoc.acs.define;

public enum AppType {
	ZIPDOC	 		(0),
	PARTNER			(1),
	CACULATOR		(2),
	CMS				(3)
	;

	private final int value;

	private AppType(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static AppType valueOf(int value) {
        
		for (final AppType type : values()) {
			if (type.value == value) return type;
		}
		return ZIPDOC;
	}
}

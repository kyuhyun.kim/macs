package com.zipdoc.acs.define;

public enum SiteVisitType {
	SUPERVISION(1,	"감리"),
	PHOTO(2, 		"촬영"),
	COMPLAINT(3, 	"민원"),
	AS(4, 			"AS"),
	;

	private int code;
	private String name;
	SiteVisitType(int code, String name){
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}
	public String getName(){
		return name;
	}
	public static SiteVisitType get(int code){
		for(SiteVisitType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

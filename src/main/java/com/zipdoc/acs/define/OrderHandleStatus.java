package com.zipdoc.acs.define;

public enum OrderHandleStatus {
	CANCLE(1,"주문취소"),
	EXCHANGE(2, "교환"),
	RETURN(3, "반품"),
	REFUND(4, "환불"),
	ADMITTED(5, "불가")
	;
	
	
	private int code;
	private String name;
	
	private OrderHandleStatus(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static OrderHandleStatus get(int code){
		for(OrderHandleStatus item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
	
}

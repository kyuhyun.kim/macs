package com.zipdoc.acs.define;

/**
  * 상담 타입 정의
 */
public enum EstimateCounselStatus {
	NOT_COUNSEL(0, 			"견적(연락안됨)"),
	COMPLETED(1, 				"견적(상담완료)"),
	HAPPY_CALL_COMPLETED(2, 	"톡인입"),
	HAPPY_CALL_ENABLE(3, 		"해피콜(가망고객)"),
	HAPPY_CALL_NOT_CONTACT(4,	"해피콜(부재)"),
	//HAPPY_CALL_RETRY_CALL(5, 	"해피콜(재통화)"),

	// 2018.03.08. KJB. 해피콜(재배분)은 삭제하고 해피콜(70)으로 통합
	//HAPPY_CALL_ASSIGN(6, 		"해피콜(재배분)"),

	HAPPY_CALL_FINISHED(7, 	"해피콜(종료)"),
	HAPPY_CALL_ACCEPT_NOT_CONTACT(8,	"해피콜(수락/종료부재)"),
	PARTNER_FEEDBACK(9,		"업체피드백"),
	COMPLAIN(10, "민원접수"),
	COMPLAIN_ING(11, "민원진행중"),
	COMPLAIN_COMPLETED(12, "민원완료"),
	VISIT_RESERVATION(20, "공사기본안내"),
	GAURANTEE(21, "보증서"),
	RETRY(70, "해피콜"),
	RESERVATION(71, "통화예약"),

	// 2018.03.07. KJB. 텍스트 내용을 "통화이면"에서 "이면관리" 변경
	SIDE_CALL(80, "이면관리"),
    ASSASSIN(88, "암행"),

	LOAN(90, "대출상담"),
	ETC(99, "기타")
	;

	private int code;
	private String name;

	EstimateCounselStatus(int code, String name) {
		this.code = code;
		this.name = name;
	}
	
	public int getCode() {
		return code;
	}
	public String getName() {
		return name;
	}

	public static EstimateCounselStatus get(int code){
		for(EstimateCounselStatus item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return NOT_COUNSEL;
	}
}

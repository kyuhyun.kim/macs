package com.zipdoc.acs.define;

public enum AgentType {

	ALL(0),
	APP(1),
	WEB(2),
	;

	private final int code;

	AgentType(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public static AgentType get(int code) {
		for (AgentType item : AgentType.values()) {
			if (code == item.getCode()) {
				return item;
			}
		}
		return null;
	}
}

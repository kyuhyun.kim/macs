package com.zipdoc.acs.define;

import org.apache.commons.lang.StringUtils;

/**
 * Created by ZIPDOC on 2016-08-09.
 */
public enum OsType {
    ANDROID("android",   	"android"),
    IOS("ios", 			"ios")
    ;

    private String code;
    private String name;
    OsType(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }
    public String getName(){
        return name;
    }
    public static OsType get(String code){
        for(OsType item : values()){
            if(StringUtils.equals(code, item.getCode())){
                return item;
            }
        }
        return null;
    }
}

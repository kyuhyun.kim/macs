package com.zipdoc.acs.define;

public enum AssignCounselType {

	/**
	 * 상담 미 진행
	 */
	NO_COUNSEL 				(0),
	
	/**
	 * 방문 견적 상담 (DB 상태)
	 */
	VISIT				 	(1),
	
	/**
	 * 비방문/서면/전화 견적 상담 (DB 상태)
	 */
	NO_VISIT			 	(2),
	
	/**
	 * 견적 완료 (단말 연동 규격에서만 사용)
	 */
	ESTIMATE		 		(3),
	;
	
	private final int value;
	
	private AssignCounselType(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static AssignCounselType valueOf(int value) {
        
		for (final AssignCounselType type : values()) {
			if (type.value == value) return type;
		}
		return NO_COUNSEL;
	}
}

package com.zipdoc.acs.define;

public enum EnableFlag {
	DISABLE	(0),
	ENABLE	(1)
	;
	
	private int code;
	EnableFlag(int code){  
        this.code = code;
    }  

	public int getCode() {
		return code;
	}
	public static EnableFlag get(int code){
		for(EnableFlag item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return DISABLE;
	}
}

package com.zipdoc.acs.define;

public enum AssignStatus {

	/**
	 * 신규 할당
	 */
	ASSIGNED 				(0),
	
	/**
	 * 상담 예정
	 */
	SCHEDULE_COUNSELING 	(1),
	
	/**
	 * 상담 완료
	 */
	COMPLETE_COUNSELING 	(2),
	
	/**
	 * 견적 발송
	 */
	PUBLISH_ESTIMATE 		(3),
	
	/**
	 * 계약 완료
	 */
	COMPLETE_CONTRACT 		(4),

	/**
	 * 계약 취소
	 */
	CONTRACT_CANCEL			(9),
	;
	
	private final int value;
	
	private AssignStatus(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static AssignStatus valueOf(int value) {
        
		for (final AssignStatus type : values()) {
			if (type.value == value) return type;
		}
		return ASSIGNED;
	}
}

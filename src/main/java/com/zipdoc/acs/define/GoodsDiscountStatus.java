package com.zipdoc.acs.define;

/**
 * Created by dskim on 2017. 5. 11..
 */
public enum GoodsDiscountStatus {

    NOT_DISCOUNT(1, 	"할인안함"),
    DISCOUNT(2,   		"할인"),
    ;

    private int code;
    private String name;
    GoodsDiscountStatus(int code, String name){
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }
    public String getName(){
        return name;
    }
    public static GoodsDiscountStatus get(int code){
        for(GoodsDiscountStatus item : values()){
            if(code == item.getCode()){
                return item;
            }
        }
        return null;
    }
}

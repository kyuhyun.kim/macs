package com.zipdoc.acs.define;

public enum SpaceCode {
	CODE1(1,   	"10평 이하", 	0, 10),
	CODE2(2,   	"10~20평", 	10, 20),
	CODE3(3,   	"20~30평", 	20, 30),
	CODE4(4,   	"30~40평", 	30, 40),
	CODE5(5,   	"40~50평", 	40, 50),
	CODE6(6,   	"50~60평", 	50, 60),
	CODE7(7,   "60평 초과", 	60, 100000),
	;

	private int code;
    private String name;
	private int min;
	private int max;

	SpaceCode(int code, String name, int min, int max){
        this.code = code;
        this.name = name;
		this.min = min;
		this.max = max;
    }  

	public int getCode() {
		return code;
	}
    public String getName(){
        return name;
    }

	public int getMax() {
		return max;
	}

	public int getMin() {
		return min;
	}

	public static SpaceCode get(int code){
		for(SpaceCode item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

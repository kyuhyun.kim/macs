package com.zipdoc.acs.define;

public enum AssignAcceptType {

	/**
	 * 견적 할당 후 파트너의 수락/거절 대기 상태
	 */
	WAITING 	(0),
	
	/**
	 * 할당된 견적을 승인한 경우
	 */
	ACCEPT 		(1),
	
	/**
	 * 할당된 견적을 거절한 경우
	 */
	REJECT 		(2),
	
	/**
	 * 수락/거절 대기시간 만료로 자동 회수된 경우
	 */
	WITHDRAW 	(3),
	;
	
	private final int value;
	
	private AssignAcceptType(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static AssignAcceptType valueOf(int value) {
        
		for (final AssignAcceptType type : values()) {
			if (type.value == value) return type;
		}
		return WAITING;
	}
}

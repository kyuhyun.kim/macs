package com.zipdoc.acs.define;

public enum CustomerAgeGroup {
	AGE_20(2,   "20대"),
	AGE_30(3, 	"30대"),
	AGE_40(4,   "40대"),
	AGE_50(5,   "50대"),
	AGE_60(6, 	"60대이상"),
	;

	private int code;
    private String name;
	CustomerAgeGroup(int code, String name){
        this.code = code;
        this.name = name;
    }  

	public int getCode() {
		return code;
	}
    public String getName(){
        return name;
    }
	public static CustomerAgeGroup get(int code){
		for(CustomerAgeGroup item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

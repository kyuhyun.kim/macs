package com.zipdoc.acs.define;

public enum EstimateStatus {

	REGISTER(0, 			"접수완료"),
	CALL_COUNSEL(1, 		"업체배정 완료"),
	VISIT_COUNSEL(2, 		"상담/방문견적 완료"),
	CONTRACT(3, 			"계약완료"),
	CONSTRUCT_START(4, 		"이사진행"),
	CONSTRUCT_COMPLETED(8,	"이사완료"),
	CANCEL_CONTRACT(9,		"계약취소"),
	;

	private int code;
	private String name;

	EstimateStatus(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}
	public String getName() {
		return name;
	}

	public static EstimateStatus get(int code){
		for(EstimateStatus item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return REGISTER;
	}
}

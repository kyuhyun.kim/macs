package com.zipdoc.acs.define;

public enum ProcessingStateType {
	STATE_READY(0, "처리대기"),
	STATE_COMPLETE(1, "처리완료"),
	STATE_RETOUCH(2, "처리대기");
	
	private int code;
	private String name;
	
	ProcessingStateType(int code, String name) {
		this.code = code;
		this.name = name;
	}
	
	public int getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	
	public static ProcessingStateType get(int code){
		for(ProcessingStateType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return STATE_READY;
	}
}

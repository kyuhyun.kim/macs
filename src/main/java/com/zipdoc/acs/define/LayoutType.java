package com.zipdoc.acs.define;

public enum LayoutType {

    PARTIAL(0,          "공간사진"),
    QUICK_FILTER(1,     "퀵필터"),
    GALLERY(2,          "시공사례"),
    GALLERY_FILTER(3,   "시공사례(필터)"),
    TAG(4,              "인기태그"),
    ;

    private int code;
    private String name;

    LayoutType(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }
    public String getName() {
        return name;
    }

    public static LayoutType get(int code) {
        for (LayoutType item : LayoutType.values()) {
            if (code == item.getCode()) {
                return item;
            }
        }
        return null;
    }
}

package com.zipdoc.acs.define;

import com.zipdoc.acs.utils.Messages;
import com.zipdoc.fcm.Message;

/**
 * Created by ZIPDOC on 2016-06-02.
 */
public class Constants {

    public static final String TAG_DELIMITER = ",";

    public static final String STATIC_URL = Messages.getMessage("STATIC_URL", "http://www.zipdoc.co.kr/static/");
    public static final String SERVER_URL = Messages.getMessage("SERVER_URL", "http://www.zipcoc.co.kr/acs3/");
    public static final String ARTICLE_URL = SERVER_URL + Messages.getMessage("ARTICLE_URL");
    public static final String PRODDUCT_SHARE_URL = Messages.getMessage("PRODDUCT_SHARE_URL");
    public static final String COMMUNITY_SHARE_URL = Messages.getMessage("COMMUNITY_SHARE_URL");

    public static final String INTERIOR_PRODUCT_URL = SERVER_URL + Messages.getMessage("INTERIOR_PRODUCT_URL");

    public static final String AGREEMENT_URL = Messages.getMessage("AGREEMENT_URL", "http://www.zipdoc.co.kr/zws/html/agreement_info.jsp");
    public static final String PRIVACY_URL = Messages.getMessage("PRIVACY_URL", "http://www.zipdoc.co.kr/zws/html/private_info.jsp");
    public static final String ESTIMATE_PRIVACY_URL = Messages.getMessage("ESTIMATE_PRIVACY_URL", "http://www.zipdoc.co.kr/zws/html/estimate_register_private_info.jsp");
    public static final String THIRD_PARTY_URL = Messages.getMessage("THIRD_PARTY_URL", "http://www.zipdoc.co.kr/zws/partner/thirdParty.do");
    public static final String ESTIMATE_LIST_URL = Messages.getMessage("ESTIMATE_LIST_URL", "http://www.zipdoc.co.kr/zws/estimate/getList.do");
    public static final String IMAGE_SERVER_URL = Messages.getMessage("STATIC_URL", "http://www.zipdoc.co.kr/static/");
    public static final String AGREEMENT_GEO_URL = Messages.getMessage("AGREEMENT_GEO_URL","http://www.zipdoc.co.kr/zws/html/agreement_geo_info.jsp");
    public static final String MARKETING_URL = Messages.getMessage("MARKETING_URL", "http://www.zipdoc.co.kr/zws/html/event_zipdoc.jsp");

    public static final String ESTMIMATE_DEFAULT_SUBJECT = Messages.getMessage("ESTMIMATE_DEFAULT_SUBJECT", "서울 아파트 인테리어");
    public static final String ESTMIMATE_DEFAULT_SPACE = Messages.getMessage("ESTMIMATE_DEFAULT_SPACE", "00");

    public static final int DEFAULT_INTERIOR_PRODUCT_CODE = 1;

    public static final String STATIC_SRC_PATTERN = "\"/static/";
    public static final String STATIC_REPLACE_URL = "\"" + IMAGE_SERVER_URL;

    public static final String UPLOAD_STATIC_PATH = Messages.getMessage("UPLOAD_FILE.STATIC_PATH");
    public static final String RELATIVE_STATIC_PATH = Messages.getMessage("RELATIVE.STATIC_PATH");
    public static final String RELATIVE_SMARTEDITOR_PATH = Messages.getMessage("RELATIVE.SMARTEDITOR_PATH");
    public static final String RELATIVE_COMMUNITY_PATH = Messages.getMessage("RELATIVE.COMMUNITY_PATH");
    public static final String RELATIVE_ITEM_PATH = Messages.getMessage("RELATIVE.ITEM_PATH");
    public static final String RELATIVE_PROFILE_PATH = Messages.getMessage("RELATIVE.PROFILE_PATH");
    public static final String RELATIVE_ESTIMATE_PATH = Messages.getMessage("RELATIVE.ESTIMATE_PATH");
    public static final String RELATIVE_SHOP_PATH = Messages.getMessage("RELATIVE.SHOP_PATH");


    //쇼핑몰 비회원
    public static final Long NON_MEMBER = 0L;

    public static final Integer FILE_LOCATION = Integer.parseInt(Messages.getMessage("STATIC_URL_TYPE", "0"));
    public static final String STATIC_URL_C = Messages.getMessage("STATIC_URL_C", "http://cf.zipdoc.co.kr/static/");
    public static final String STATIC_URL_Q = Messages.getMessage("STATIC_URL_Q", "http://s3.ap-northeast-2.amazonaws.com/cf.zipdocdev.com/static/");
}

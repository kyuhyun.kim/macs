package com.zipdoc.acs.define;

import org.apache.commons.lang.StringUtils;

public enum MemberStatus {
	DISABLE(0, "미사용 계정입니다."),
	ENABLE(1, "정상 계정입니다."),
	DORMANT(2, "휴면 계정입니다."),
	BLOCK(8, "차단된 계정입니다."),
	WITHDRAWAL(9, "탈퇴된 계정입니다."),
	;

	private int code;
	private String name;

	MemberStatus(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static MemberStatus get(int code) {
		for (MemberStatus item : values()) {
			if (code == item.getCode()) {
				return item;
			}
		}
		return null;
	}
}

package com.zipdoc.acs.define;

public enum PartnerEvaluation {

    GRADE_NONE(0, "미평가"),
    GRADE_D(1, "D"),
    GRADE_C(2, "C"),
    GRADE_B(3, "B"),
    GRADE_A(4, "A"),
    GRADE_S(5, "S"),
    GRADE_F(6, "F")
    ;

    private final int value;
    private final String code;

    PartnerEvaluation(int value, String code) {
        this.value = value;
        this.code = code;
    }

    public int getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }
}

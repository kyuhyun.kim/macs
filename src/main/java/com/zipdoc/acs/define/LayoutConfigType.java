package com.zipdoc.acs.define;

public enum LayoutConfigType {

    QUICK_FILTER(0,	"퀵필터"),
    SPACE(1,		"평형"),
    COLOR(2,		"색상"),
    STYLE(3,		"스타일"),
    PARTIAL(4,		"부분공간"),
    GALLERY(5,		"시공사례"),
    TAG(6,			"인기태그"),
    ;

    private int code;
    private String name;

    LayoutConfigType(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }
    public String getName() {
        return name;
    }

    public static LayoutConfigType get(int code) {
        for (LayoutConfigType item : LayoutConfigType.values()) {
            if (code == item.getCode()) {
                return item;
            }
        }
        return null;
    }
}

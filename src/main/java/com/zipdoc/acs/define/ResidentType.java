package com.zipdoc.acs.define;

import org.apache.commons.lang.StringUtils;

public enum ResidentType {
	거주(1, 			"1",	"거주"),
	임대(2, 		"2",	"임대"),
	기타(9, 		"3",	"기타");;

	private int code;
    private String code_name;
	private String name;
    ResidentType(int code, String code_name, String name){
        this.code = code;
        this.code_name = code_name;
		this.name = name;
    }


	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getCode_name() {
		return code_name;
	}

	public void setCode_name(String code_name) {
		this.code_name = code_name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static String get(String code_name){
		for(ResidentType item : values()){
			if(StringUtils.equals(code_name , item.getCode_name())){
				return item.getName();
			}
		}
		return null;
	}

	public static ResidentType get(int code){
		for(ResidentType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

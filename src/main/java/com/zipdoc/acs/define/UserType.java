package com.zipdoc.acs.define;

public enum UserType {
	NO_MEMBER	 		(0),
	AUTO_REGI			(1),
	MEMBER				(2),
	OPERATOR			(3),
	;

	private final int value;

	private UserType(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static UserType valueOf(int value) {
        
		for (final UserType type : values()) {
			if (type.value == value) return type;
		}
		return NO_MEMBER;
	}
}

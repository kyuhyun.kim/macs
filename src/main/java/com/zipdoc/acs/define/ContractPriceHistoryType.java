package com.zipdoc.acs.define;

public enum ContractPriceHistoryType {
    ADD(1,      "추가"),
    CANCEL(2,   "취소"),
    REDUCE(3,   "축소")
    ;

    private int code;
    private String name;

    ContractPriceHistoryType(int code, String name){
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }
    public String getName(){
        return name;
    }
    public static ContractPriceHistoryType get(int code) {
        for(ContractPriceHistoryType item : values()){
            if(code == item.getCode()){
                return item;
            }
        }
        return null;
    }
}

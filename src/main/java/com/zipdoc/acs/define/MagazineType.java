package com.zipdoc.acs.define;

public enum MagazineType {

	ALL(0,				""),
	INTERIOR_GUIDE(1,	"인테리어 팁"),
	ZIPDOCMAN(2, 		"집닥맨 현장관리"),
	INTERVIEW(3, 		"고객 인터뷰"),
	NEWS(4,				"집닥 뉴스"),
	TREND(5,				"인테리어 트랜드")
	;

	private int code;
	private String name;
	MagazineType(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}
	public String getName(){
		return name;
	}
	public static MagazineType get(int code) {
		for(MagazineType item : values()) {
			if(code == item.getCode()) {
				return item;
			}
		}
		return null;
	}
}

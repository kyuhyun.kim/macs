package com.zipdoc.acs.define;

public enum SiteVisitStatus {
	SCHEDULE(0,	"예정"),
	COMPLETE(1,	"완료"),
	CANCEL(2, 	"취소"),
	NONE(9, 	"미대상"),
	;

	private int code;
	private String name;
	SiteVisitStatus(int code, String name){
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}
	public String getName(){
		return name;
	}
	public static SiteVisitStatus get(int code){
		for(SiteVisitStatus item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

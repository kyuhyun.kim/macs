package com.zipdoc.acs.define;

public enum Channel {
	WEB				(1),
	APP 			(2),
	CALL			(3),
	TALK			(4),
	EXTERNAL		(5)
	;

	private final int value;

	private Channel(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static Channel get(int value) {
        
		for (final Channel type : values()) {
			if (type.value == value) return type;
		}
		return WEB;
	}
}

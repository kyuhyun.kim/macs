package com.zipdoc.acs.define;


public enum FaqType {

	SERVICE(1, "서비스안내"),
	ESTIMATE(2, "견적"),
	COUNSEL(3, "상담"),
	CONTRACT(4, "계약"),
	CONSTRUCTION(5, "공사"),
	AFTER_SERVICE(6, "시공 후"),
	PARTNER(7, "파트너스"),
	;

	private int code;
	private String name;
	
	FaqType(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
	
	public static FaqType get(int code){
		for(FaqType item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return null;
	}
}

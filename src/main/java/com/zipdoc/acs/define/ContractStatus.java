package com.zipdoc.acs.define;

public enum ContractStatus {
	
	/**
	 * 계약 완료
	 */
	NO_STATUS				(0),
	COMPLETE_CONTRACT		(3),
	CONSTRUCT_START			(4),
	CONSTRUCT_END			(8),
	;

	private final int value;

	private ContractStatus(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static ContractStatus valueOf(int value) {
        
		for (final ContractStatus type : values()) {
			if (type.value == value) return type;
		}
		return NO_STATUS;
	}
}

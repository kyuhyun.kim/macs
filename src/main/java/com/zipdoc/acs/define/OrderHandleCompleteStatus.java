package com.zipdoc.acs.define;

public enum OrderHandleCompleteStatus {
	READY(1,   "처리대기"),
	COMPLETED(2, "처리완료"),
	RETOUCH(3, "재연락"),
	BAN(4, "불가");

	private int code;
    private String name;

    OrderHandleCompleteStatus(int code, String name){
        this.code = code;
        this.name = name;
    }

	public int getCode() {
		return code;
	}
    public String getName(){
        return name;
    }
	public static OrderHandleCompleteStatus get(int code){
		for(OrderHandleCompleteStatus item : values()){
			if(code == item.getCode()){
				return item;
			}
		}
		return READY;
	}

}

package com.zipdoc.acs.gcm;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

public class GCM {

	private static final Logger log = LoggerFactory.getLogger(GCM.class);

	private String gcm_key;

	public GCM(String gcm_key) {
		this.gcm_key = gcm_key;
	}

	public boolean sendGCM(String registraionID, String pushMessage) {

		// 키 설정
		Sender sender = new Sender(gcm_key);

		Message message = new Message.Builder().addData("msg", pushMessage).build();

		List<String> list = new ArrayList<>();
		list.add(registraionID);

		MulticastResult multiResult;

		try {

			log.debug("[GCM] [token="+registraionID+"] => "+pushMessage);

			multiResult = sender.send(message, list, 5);

			if (multiResult != null) {

				List<Result> results = multiResult.getResults();

				for (Result result : results) {
					log.debug("gcm result: "+result.getMessageId());
				}
			}

		} catch (Exception e) {
			log.error("#### gcm error.", e);
			return false;
		}
//		   else {
//		      String error = result.getErrorCodeName();   //에러 내용 받기
//		      if(Constants.ERROR_INTERNAL_SERVER_ERROR.equlas(error)) {
//
//		         //구글 푸시 서버 에러
//		      }

		return true;
	}
}

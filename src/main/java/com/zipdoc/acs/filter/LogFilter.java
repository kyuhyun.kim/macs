package com.zipdoc.acs.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogFilter extends OncePerRequestFilter{

    protected static final Logger logger = LoggerFactory.getLogger(LogFilter.class);
    //private AtomicLong id = new AtomicLong(1L);

    public LogFilter() {
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//        long requestId = this.id.incrementAndGet();
//        try {
//
//            request = new HttpRequestWrapper(request, requestId);
//            response = new HttpResponseWrapper(response, requestId);
//
//            filterChain.doFilter(request, response);
//
//            if(!logger.isDebugEnabled()) return;
//
//            httpRequestLog(request);
//            httpResponseLog(response);
//
//        } catch (Exception e) {
//            logger.error("", e);
//        }
    }

//    private void httpRequestLog(HttpServletRequest request){
//        HttpRequestWrapper wrequest = (HttpRequestWrapper)request;
//        Enumeration enumeration = request.getHeaderNames();
//        StringBuilder sb = new StringBuilder();
//        sb.append(String.format(
//                "\r\n============= HTTP %s REQUEST MESSAGE START ID[%d]======================\r\n",
//                request.getMethod(), wrequest.getId()));
//        sb.append("Request URI : " + request.getRequestURI() + "\r\n");
//        sb.append("Request URL : " + request.getRequestURL() + "\r\n");
//
//        while (enumeration != null && enumeration.hasMoreElements()){
//            String key = (String)enumeration.nextElement();
//            String value = request.getHeader(key);
//            sb.append(String.format("%s : %s\r\n", StringUtils.rightPad(key, 24),value));
//        }
//        String contentType = request.getContentType();
//
//
//        // 밑에 뭔가 이상한데..?
//        try {
//            if(wrequest.toByteArray().length < 400 && (contentType != null && contentType.startsWith("multi")))
//                sb.append("BODY : " + new String(wrequest.toByteArray(),
//                        request.getCharacterEncoding() != null ? request.getCharacterEncoding() : "UTF-8") + "\r\n");
//            else {
//                int size;
//                if(wrequest.toByteArray().length <= 400)
//                    size = wrequest.toByteArray().length;
//                else size = 400;
//                sb.append("BODY : " + new String(wrequest.toByteArray(),
//                        request.getCharacterEncoding() != null ? request.getCharacterEncoding() : "UTF-8").substring(0,size) + "\r\n");
//            }
//        } catch (Exception e){
//            logger.error("httpRequestLog : ", e);
//        }
//        sb.append("============= HTTP REQUEST MESSAGE END ===============\r\n");
//        logger.info(sb.toString());
//    }

//    private void httpResponseLog(HttpServletResponse response){
//        HttpResponseWrapper wresponse =  (HttpResponseWrapper) response;
//
//        StringBuilder sb = new StringBuilder();
//        sb.append(String.format("\r\n============= HTTP %d RESPONSE MESSAGE START ID [%d] ============\r\n",
//                response.getStatus(), wresponse.getId()));
//
//        String contentType = null;
//        if (((HttpResponseWrapper) response).getResponse() != null) {
//            ServletResponse sresponse = ((HttpResponseWrapper) response).getResponse();
//            if (sresponse != null && (sresponse.getContentType() != null && !sresponse.getContentType().isEmpty())) {
//                contentType = sresponse.getContentType();
//                sb.append(String.format("%s : %s\r\n", StringUtils.rightPad("Content-Type", 24), contentType));
//            }
//        }
//
//        try {
//            if(response.getOutputStream() != null && (contentType != null && contentType.startsWith("app"))) {
//                if(wresponse.toByteArray().length > 2048)
//                    sb.append("BODY : " + new String(wresponse.toByteArray(),
//                            response.getCharacterEncoding() != null ? response.getCharacterEncoding() : "UTF-8").substring(0,512) + "\r\n");
//                else {
//                    sb.append("BODY : " + new String(wresponse.toByteArray(),
//                            response.getCharacterEncoding() != null ? response.getCharacterEncoding() : "UTF-8") + "\r\n");
//                }
//            }
//        } catch (Exception e){
//            logger.error("httpResponseLog : ", e);
//        }
//
//        sb.append("============= HTTP RESPONSE MESSAGE END ========================\r\n");
//        logger.info(sb.toString());
//    }
}

package com.zipdoc.acs.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;



public class CorsFilter extends OncePerRequestFilter {



    @Override

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)

            throws ServletException, IOException {

        //"Access-Control-Allow-Origin", 'http://localhost:8080');
//        response.addHeader("Access-Control-Allow-Origin", "http://localhost:8090");
        response.addHeader("Access-Control-Allow-Origin", "*");
//        response.addHeader("Access-Control-Allow-Origin", "http://aws.zipdocdev.com");

        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())); {

            // CORS "pre-flight" request

            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");

            //response.addHeader("Access-Control-Allow-Headers", "Authorization");
            response.addHeader("Access-Control-Allow-Headers", "Content-Type,Accept");
            response.addHeader("Access-Control-Max-Age", "1728000");

        }

        filterChain.doFilter(request, response);

    }



}
package com.zipdoc.acs.model;

public enum SubscriberType {

	UNKNOWN		(-1),
	
	/**
	 * 계정 미등록 개인 고객
	 */
	AUTO 		(0),
	
	/**
	 * 계정 등록 개인 고객
	 */
	PERSONAL 	(1),
	
	/**
	 * 업체 고객
	 */
	CORPORATE 	(2),

    /**
     * 관리자
     */
    ADMIN		(3);
	

	
	private final int value;
	
	private SubscriberType(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static SubscriberType valueOf(int value) {
        
		for (final SubscriberType type : values()) {
			if (type.value == value) return type;
		}
		return UNKNOWN;
	}
}

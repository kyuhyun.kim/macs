package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.define.MagazineType;

/**
 * Created by ZIPDOC on 2016-06-02.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class MagazineListRequest extends ListRequest {

    private int magazine_type;
    private Integer cno;

    @JsonIgnore
    private Integer ex_cno;

    public Integer getEx_cno() {
        return ex_cno;
    }

    public void setEx_cno(Integer ex_cno) {
        this.ex_cno = ex_cno;
    }

    public int getMagazine_type() {
        return magazine_type;
    }

    public void setMagazine_type(int magazine_type) {
        this.magazine_type = magazine_type;
    }

    public Integer getCno() {
        return cno;
    }

    public void setCno(Integer cno) {
        this.cno = cno;
    }

    @Override
    public String toString() {
        return "MagazineListRequest{" +
                "magazine_type='" + magazine_type + '\'' +
                '}';
    }
}

package com.zipdoc.acs.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.PartnerContractHistory;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerHistoryResponse {
	
	private Long contract_no;
	
	private String partner_name;
	
	private String customer;
	
	private String category_code1;
	
	private String category_code2;
	
	private Integer agreement_status;
	
	private List<PartnerContractHistory> history;

	public Long getContract_no() {
		return contract_no;
	}

	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getCategory_code1() {
		return category_code1;
	}

	public void setCategory_code1(String category_code1) {
		this.category_code1 = category_code1;
	}

	public String getCategory_code2() {
		return category_code2;
	}

	public void setCategory_code2(String category_code2) {
		this.category_code2 = category_code2;
	}

	public Integer getAgreement_status() {
		return agreement_status;
	}

	public void setAgreement_status(Integer agreement_status) {
		this.agreement_status = agreement_status;
	}

	public List<PartnerContractHistory> getHistory() {
		return history;
	}

	public void setHistory(List<PartnerContractHistory> history) {
		this.history = history;
	}
}

package com.zipdoc.acs.model;

/**
 * 파트너 견적 진행현황 상세 설정
 * @author 김종부
 *
 */
public class PartnerAssignCounsel {
	
	/**
	 * 사용자가 의뢰한 견적 ID
	 */
	private long estimate_no;
	
	/**
		계약 진행 상태. 1단계부터 4단계의 상담 과정 시 각 계약의 상태를 확인하기 위해 사용함.
		1 : 상담예정
		2 : 상담완료
		3 : 견적발송
		4 : 계약완료
	 */
	private int status;
	
	/**
		상담 형태. status가 상담예정(1), 상담완료(2) 이면서 중단 사유가 없을 경우 
		1 : 방문 견적 상담
		2 : 비방문/서면/전화 견적 상담
		3: 견적 상담 완료 (이 값을 선택한 경우 견적발송(3) 단계로 설문 진행)
	 */
	private int counsel_type;
	
	/**
		각 단계에서 계약완료(4) 까지 진행하지 못하게 된 사유
		1 : 견적 금액이 맞지 않음
		2 : 고객의 변심
		3 : 업체 사정
		4 : 컨셉이 맞지 않음
		999 : 기타
	 */
	private int stop_code;
	
	/**
		계약 중단 기타 사유 (stop_code가 999일 경우에만 사용함. 200글자 이내)
	 */	
	private String stop_reason;
	
	/**
		방문예정일 / 방문 완료일
		계약 진행 상태가 상담예정(1), 상담완료(2) 이면서 상담형태가 방문 견적 상담(1)인 경우에만 사용함.
	 */
	private long visit_date;
	
	/**
		견적 발송에 대한 고객 답변 예정일
		계약 진행 상태가 계약완료(3) 이면서 견적 답변을 받기로 한 경우에 사용함
	 */
	private long feedback_date;
	
	/**
		계약 예상 금액
	 */
	private long estimate_price;
	
	/**
	 * 최종 계약 금액
	 */
	private long contract_price;
	
	/**
		계약일
		계약 진행 상태가 계약완료(4) 상태인 경우에만 사용함
	 */
	private long contract_date;
	
	/**
		공사 시작 예정일
		계약 진행 상태가 계약완료(4) 상태인 경우에만 사용함
	 */
	private long start_date;
	
	/**
		공사 종료 예정일
		계약 진행 상태가 계약완료(4) 상태인 경우에만 사용함
	 */
	private long end_date;
	
	/**
		표준 계약서 발급 여부
		0 : 표준계약서 발급안함
		1 : 표준계약서 발급요청
	 */
	private int std_contract_status;

	public long getEstimate_no() {
		return estimate_no;
	}

	public void setEstimate_no(long estimate_no) {
		this.estimate_no = estimate_no;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getCounsel_type() {
		return counsel_type;
	}

	public void setCounsel_type(int counsel_type) {
		this.counsel_type = counsel_type;
	}

	public int getStop_code() {
		return stop_code;
	}

	public void setStop_code(int stop_code) {
		this.stop_code = stop_code;
	}

	public String getStop_reason() {
		return stop_reason;
	}

	public void setStop_reason(String stop_reason) {
		this.stop_reason = stop_reason;
	}

	public long getVisit_date() {
		return visit_date;
	}

	public void setVisit_date(long visit_date) {
		this.visit_date = visit_date;
	}

	public long getFeedback_date() {
		return feedback_date;
	}

	public void setFeedback_date(long feedback_date) {
		this.feedback_date = feedback_date;
	}

	public long getEstimate_price() {
		return estimate_price;
	}

	public void setEstimate_price(long estimate_price) {
		this.estimate_price = estimate_price;
	}

	public long getContract_date() {
		return contract_date;
	}

	public void setContract_date(long contract_date) {
		this.contract_date = contract_date;
	}

	public long getStart_date() {
		return start_date;
	}

	public void setStart_date(long start_date) {
		this.start_date = start_date;
	}

	public long getEnd_date() {
		return end_date;
	}

	public void setEnd_date(long end_date) {
		this.end_date = end_date;
	}

	public int getStd_contract_status() {
		return std_contract_status;
	}

	public void setStd_contract_status(int std_contract_status) {
		this.std_contract_status = std_contract_status;
	}

	public long getContract_price() {
		return contract_price;
	}

	public void setContract_price(long contract_price) {
		this.contract_price = contract_price;
	}
}

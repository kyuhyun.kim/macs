package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ServiceInfo {
	private Map<String, String> url_info = new HashMap<String, String>();
	private List<RecommendKeyword> keyword_info;
	private List<EventPromotion> promotion_info;
	private List<Region> region_info;
	private ArticleInfo article_info;
	private List<Category> category_info;
	private List<InteriorProduct> interior_product_info;
	private List<Code> code_info;
	private List<String> color_info;

	private String person_agree;
	private String person_agree_privacy;

	private String agree_url;

	//2017.12.13
	private List<PartnerProduct> partner_product_info;

	private List<CodeValue> faq_category_info;

	public List<RecommendKeyword> getKeyword_info() {
		return keyword_info;
	}

	public void setKeyword_info(List<RecommendKeyword> keyword_info) {
		this.keyword_info = keyword_info;
	}

	public List<EventPromotion> getPromotion_info() {
		return promotion_info;
	}

	public void setPromotion_info(List<EventPromotion> promotion_info) {
		this.promotion_info = promotion_info;
	}

	public List<Region> getRegion_info() {
		return region_info;
	}

	public void setRegion_info(List<Region> region_info) {
		this.region_info = region_info;
	}

	public ArticleInfo getArticle_info() {
		return article_info;
	}

	public void setArticle_info(ArticleInfo article_info) {
		this.article_info = article_info;
	}

	public List<Category> getCategory_info() {
		return category_info;
	}

	public void setCategory_info(List<Category> category_info) {
		this.category_info = category_info;
	}

	public List<InteriorProduct> getInterior_product_info() {
		return interior_product_info;
	}

	public void setInterior_product_info(List<InteriorProduct> interior_product_info) {
		this.interior_product_info = interior_product_info;
	}

	public Map<String, String> getUrl_info() {
		return url_info;
	}

	public void setUrl_info(Map<String, String> url_info) {
		this.url_info = url_info;
	}

	public void putUrl_info(String key, String value){
		this.url_info.put(key, value);
	}

	public void buildPictureUrl(){
	}

	public void setCode_info(List<Code> code_info) {
		this.code_info = code_info;
	}

	public List<Code> getCode_info() {
		return code_info;
	}

	public String getPerson_agree() {
		return person_agree;
	}

	public void setPerson_agree(String person_agree) {
		this.person_agree = person_agree;
	}

	public String getAgree_url() {
		return agree_url;
	}

	public void setAgree_url(String agree_url) {
		this.agree_url = agree_url;
	}

	public String getPerson_agree_privacy() {
		return person_agree_privacy;
	}

	public void setPerson_agree_privacy(String person_agree_privacy) {
		this.person_agree_privacy = person_agree_privacy;
	}

	public List<PartnerProduct> getPartner_product_info() {
		return partner_product_info;
	}

	public void setPartner_product_info(List<PartnerProduct> partner_product_info) {
		this.partner_product_info = partner_product_info;
	}

	public List<String> getColor_info() {
		return color_info;
	}

	public void setColor_info(List<String> color_info) {
		this.color_info = color_info;
	}

    public List<CodeValue> getFaq_category_info() {
        return faq_category_info;
    }

    public void setFaq_category_info(List<CodeValue> faq_category_info) {
        this.faq_category_info = faq_category_info;
    }
}

package com.zipdoc.acs.model;

/**
 * Created by ZIPDOC on 2016-06-13.
 */
public class ZzimListRequest extends ListRequest {

    private long book_no;
    private Integer gallery_limit = 9;
    private Integer picture_limit = 16;

    public ZzimListRequest(){
    }

    public ZzimListRequest(long member_no, int page, int limit){
        super.setMember_no(member_no);
        setPageLimit(page, limit);
    }

    public ZzimListRequest(long member_no, int page, int limit, long book_no){
        super.setMember_no(member_no);
        this.book_no = book_no;
        setPageLimit(page, limit);
    }

    public long getBook_no() {
        return book_no;
    }

    public void setBook_no(long book_no) {
        this.book_no = book_no;
    }

    public Integer getGallery_limit() {
        return gallery_limit;
    }

    public void setGallery_limit(Integer gallery_limit) {
        this.gallery_limit = gallery_limit;
    }

    public Integer getPicture_limit() {
        return picture_limit;
    }

    public void setPicture_limit(Integer picture_limit) {
        this.picture_limit = picture_limit;
    }
}

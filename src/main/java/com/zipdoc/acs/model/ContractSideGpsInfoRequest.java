package com.zipdoc.acs.model;

import com.zipdoc.acs.utils.AES128Util;
import com.zipdoc.acs.utils.Messages;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

public class ContractSideGpsInfoRequest {


    //위도
    private String latitude;

    //경도
    private String longitude;

    private Long staying_time;


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Long getStaying_time() {
        return staying_time;
    }

    public void setStaying_time(long staying_time) {
        this.staying_time = staying_time;
    }


    public void buildAES128Encrypt() throws Exception {

        String key = Messages.getMessage("AES128.KEY");

        if (StringUtils.isNotEmpty(getLatitude())) {

            String aes128Lat = AES128Util.Encrypt(getLatitude(), key);
            setLatitude(aes128Lat);
        }

        if (StringUtils.isNotEmpty(getLongitude())) {
            String aes128Lon = AES128Util.Encrypt(getLongitude(), key);
            setLongitude(aes128Lon);
        }

    }
}

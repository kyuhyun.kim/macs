package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.PartnerContract;

import java.util.List;

/**
 * 파트너 비용 관리
 * @author 김종부
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerAssignSearchRequest extends ListRequest{
	private Integer partner_id;
	private String keyword;
	private Integer status = -1;
	private Integer retry_call = -1;

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getRetry_call() {
		return retry_call;
	}

	public void setRetry_call(Integer retry_call) {
		this.retry_call = retry_call;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}

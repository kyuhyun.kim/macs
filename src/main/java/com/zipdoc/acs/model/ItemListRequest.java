package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZIPDOC on 2016-06-02.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ItemListRequest extends ListRequest {

    private Long item_id;
    private String category_code1;
    private List<String> exclude_tags = new ArrayList<String>();
    private List<String> tags = new ArrayList<String>();
    private String BnA;
    private boolean beforeflag;
    private String searchKeyword;

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public boolean isBeforeflag() {
        return beforeflag;
    }

    public void setBeforeflag(boolean beforeflag) {
        this.beforeflag = beforeflag;
    }

    public String getBnA() {
        return BnA;
    }

    public void setBnA(String bnA) {
        BnA = bnA;
    }

    public Long getItem_id() {
        return item_id;
    }

    public void setItem_id(Long item_id) {
        this.item_id = item_id;
    }

    public String getCategory_code1() {
        return category_code1;
    }

    public void setCategory_code1(String category_code1) {
        this.category_code1 = category_code1;
    }

    public List<String> getExclude_tags() {
        return exclude_tags;
    }

    public void setExclude_tags(List<String> exclude_tags) {
        this.exclude_tags = exclude_tags;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "ItemListRequest{" +
                "tags=" + tags +
                '}';
    }
}

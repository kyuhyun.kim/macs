package com.zipdoc.acs.model;

public enum PartnerType {

	/**
	 * 미승인 파트너
	 */
	UNAPPROVED 		(0),
	
	/**
	 * 일반 파트너
	 */
	NORMAL 	(1),
	
	/**
	 * 베스트 파트너
	 */
	BEST 	(2),
	
	;
	
	private final int value;
	
	private PartnerType(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static PartnerType valueOf(int value) {
        
		for (final PartnerType type : values()) {
			if (type.value == value) return type;
		}
		return UNAPPROVED;
	}
}

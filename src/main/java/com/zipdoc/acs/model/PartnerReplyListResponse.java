package com.zipdoc.acs.model;

import com.zipdoc.acs.domain.entity.PartnerReply;

import java.util.List;

public class PartnerReplyListResponse {
	private int total_count;
    private List<PartnerReply> list;

	public PartnerReplyListResponse(){

	}

	public PartnerReplyListResponse(int total_count, List<PartnerReply> list) {
		this.total_count = total_count;
		this.list = list;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<PartnerReply> getList() {
		return list;
	}

	public void setList(List<PartnerReply> list) {
		this.list = list;
	}

	public void buildPictureUrl(){
		if(list != null && list.size() > 0){
			for(PartnerReply reply:list){
				reply.buildPictureUrl();
			}
		}
	}
}

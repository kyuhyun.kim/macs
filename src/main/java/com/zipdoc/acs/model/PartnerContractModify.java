package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * 파트너 계약 정보 수정 (공사금액/계약일/공사시작예정일/공사완료예정일
 * @author 김종부
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerContractModify {
	
	private Long contract_no;
	private Long contract_price;
	private Long contract_date;
	private Long start_date;
	private Long expect_end_date;
	private Integer commission_rate = 5;
	private Integer commission;
	private Integer vat_tax;
	private Integer amount_sum;
	private List<Long> delete_files;
	
	public Long getContract_no() {
		return contract_no;
	}
	
	public void setContract_no(Long contract_no) {
		this.contract_no = contract_no;
	}
	
	public Long getContract_price() {
		return contract_price;
	}
	
	public void setContract_price(Long contract_price) {
		this.contract_price = contract_price;
	}
	
	public Long getContract_date() {
		return contract_date;
	}
	
	public void setContract_date(Long contract_date) {
		this.contract_date = contract_date;
	}
	
	public Long getStart_date() {
		return start_date;
	}
	
	public void setStart_date(Long start_date) {
		this.start_date = start_date;
	}
	
	public Long getExpect_end_date() {
		return expect_end_date;
	}
	
	public void setExpect_end_date(Long expect_end_date) {
		this.expect_end_date = expect_end_date;
	}

	public Integer getAmount_sum() {
		return amount_sum;
	}

	public void setAmount_sum(Integer amount_sum) {
		this.amount_sum = amount_sum;
	}

	public Integer getCommission() {
		return commission;
	}

	public void setCommission(Integer commission) {
		this.commission = commission;
	}

	public int getCommission_rate() {
		return commission_rate;
	}

	public void setCommission_rate(int commission_rate) {
		this.commission_rate = commission_rate;
	}

	public Integer getVat_tax() {
		return vat_tax;
	}

	public void setVat_tax(Integer vat_tax) {
		this.vat_tax = vat_tax;
	}

	public List<Long> getDelete_files() {
		return delete_files;
	}

	public void setDelete_files(List<Long> delete_files) {
		this.delete_files = delete_files;
	}

	//수수료 및 부가세 계산
	public void calculateCommition() {
		if(this.commission_rate==null) commission_rate = 5;
		if(this.contract_price==null || contract_price <= 0) return;

		this.commission = (int)Math.round((contract_price * commission_rate)/100.0);
		this.vat_tax = (int)Math.round((Math.round((contract_price * commission_rate)/100.0) * 0.1));
		this.amount_sum = (int)Math.round(((contract_price * commission_rate)/100.0) + (Math.round((contract_price * commission_rate)/100.0) * 0.1));
	}

}

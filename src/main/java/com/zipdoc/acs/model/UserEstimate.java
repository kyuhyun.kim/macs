package com.zipdoc.acs.model;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 사용자 신청 견적 요청 정보
 * @author 김종부
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserEstimate {
	
	private static String ESTIMATE_APP_CATEGORY_1		= "주거용 견적문의";
	private static String ESTIMATE_APP_CATEGORY_2		= "상업용 견적문의";
	
	private static String ESTIMATE_WEB_CATEGORY_1		= "주거공간";
	private static String ESTIMATE_WEB_CATEGORY_2		= "상업공간";

	// 견적 공간 구분 (주거용 / 상업용) 
	private @JsonProperty(value="category") String category;

	// 고객 이름
	private @JsonProperty("writer") String writer;
	
	// 고객 전화번호
	private @JsonProperty("phone-no") String phone_no;
	
	// 시공 주소
	private @JsonProperty("address") String address;
	
	// 이메일
	private @JsonProperty("e-mail") String email;
	
	// 견적 비밀번호
	private @JsonProperty("password") String password;
	
	// 견적 비밀번호
	private @JsonProperty("subject") String subject;
	
	// 시공 대상 (아파트, 빌라, 주책, 오피스텔 등)
	private @JsonProperty("space") String space;
	
	// 시공 예산
	private @JsonProperty("budget") String budget;
	
	// 시공 면적
	private @JsonProperty("area") String area;
	
	// 공사 시작일
	private @JsonProperty("start-date") long start_date;
	
	// 입주/영업 시작일
	private @JsonProperty("end-date") long end_date;
	
	// 방문 희망일
	private @JsonProperty("estimate-date") long estimate_date;
	
	// 거주 여부
	private @JsonProperty("is-live") boolean isLive;
	
	// 공사 내용
	private @JsonProperty("comment") String comment;
	
	// 방문 희망일
	private @JsonProperty("image-urls") String [] image_urls;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		
		if (StringUtils.isEmpty(category)) {
			this.category = ESTIMATE_WEB_CATEGORY_1;
		} 
		
		// 주거용 견적문의 => 주거공간
		else if (category.equals(ESTIMATE_APP_CATEGORY_1)){
			this.category = ESTIMATE_WEB_CATEGORY_1;
		} 
		
		// 상업용 견적문의 => 상업공간
		else if (category.equals(ESTIMATE_APP_CATEGORY_2)){
			this.category = ESTIMATE_WEB_CATEGORY_2;
		}

		// 미정의 => 방문견적접수
		else {
			this.category = ESTIMATE_WEB_CATEGORY_1;
		}
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public long getStart_date() {
		return start_date;
	}

	public void setStart_date(long start_date) {
		this.start_date = start_date;
	}

	public long getEnd_date() {
		return end_date;
	}

	public void setEnd_date(long end_date) {
		this.end_date = end_date;
	}

	public long getEstimate_date() {
		return estimate_date;
	}

	public void setEstimate_date(long estimate_date) {
		this.estimate_date = estimate_date;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String[] getImage_urls() {
		return image_urls;
	}

	public void setImage_urls(String[] image_urls) {
		this.image_urls = image_urls;
	}

	public boolean isLive() {
		return isLive;
	}

	public void setLive(boolean isLive) {
		this.isLive = isLive;
	}
}

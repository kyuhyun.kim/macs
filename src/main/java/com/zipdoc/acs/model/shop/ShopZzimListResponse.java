package com.zipdoc.acs.model.shop;

import java.util.List;

import com.zipdoc.acs.domain.entity.ShopZzimInfo;

public class ShopZzimListResponse {
	private int total_count;
	private List<ShopZzimInfo> list;
	
	public ShopZzimListResponse(){
		
	}

	public ShopZzimListResponse(int total_count, List<ShopZzimInfo> list) {
		this.total_count = total_count;
		this.list = list;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<ShopZzimInfo> getList() {
		return list;
	}

	public void setList(List<ShopZzimInfo> list) {
		this.list = list;
	}
}

package com.zipdoc.acs.model.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.model.ListRequest;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ShopOrderListRequest extends ListRequest{

	public ShopOrderListRequest() {
	}

	public ShopOrderListRequest(int page, int limit) {
		setPageLimit(page, limit);
	}

	@Override
	public String toString() {
		return "ShopOrderListRequest [member_no=" + super.getMember_no() + "]";
	}
}

package com.zipdoc.acs.model.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.ShopOrderInfo;
import com.zipdoc.acs.domain.entity.ShopOrderUserInfo;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ShopOrderDetailResponse {
	private ShopOrderUserInfo shopOrderUserInfo = new ShopOrderUserInfo();
	private ShopOrderInfo shopOrderInfo = new ShopOrderInfo();
	private String order_name;
	
	public ShopOrderDetailResponse(ShopOrderUserInfo shopOrderUserInfo, ShopOrderInfo shopOrderInfo) {
		this.shopOrderUserInfo = shopOrderUserInfo;
		this.shopOrderInfo = shopOrderInfo;
	}
	
	public ShopOrderDetailResponse(ShopOrderUserInfo shopOrderUserInfo, ShopOrderInfo shopOrderInfo, String order_name){
		this.shopOrderUserInfo = shopOrderUserInfo;
		this.shopOrderInfo = shopOrderInfo;
		this.order_name = order_name;
	}

	public String getOrder_name() {
		return order_name;
	}

	public void setOrder_name(String order_name) {
		this.order_name = order_name;
	}

	public ShopOrderUserInfo getShopOrderUserInfo() {
		return shopOrderUserInfo;
	}

	public void setShopOrderUserInfo(ShopOrderUserInfo shopOrderUserInfo) {
		this.shopOrderUserInfo = shopOrderUserInfo;
	}

	public ShopOrderInfo getShopOrderInfo() {
		return shopOrderInfo;
	}

	public void setShopOrderInfo(ShopOrderInfo shopOrderInfo) {
		this.shopOrderInfo = shopOrderInfo;
	}
}

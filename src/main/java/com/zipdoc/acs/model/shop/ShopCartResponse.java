package com.zipdoc.acs.model.shop;

import java.util.List;

import com.zipdoc.acs.domain.entity.CartPrice;
import com.zipdoc.acs.domain.entity.ShopCart;

public class ShopCartResponse {
	private int total_count;
	private List<ShopCart> list;
	private CartPrice cartPrice = new CartPrice();

	public ShopCartResponse() {
	}

	public ShopCartResponse(int total_count, List<ShopCart> list, CartPrice cartPrice) {
		super();
		this.total_count = total_count;
		this.list = list;
		this.cartPrice = cartPrice;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<ShopCart> getList() {
		return list;
	}

	public void setList(List<ShopCart> list) {
		this.list = list;
	}

	public CartPrice getCartPrice() {
		return cartPrice;
	}

	public void setCartPrice(CartPrice cartPrice) {
		this.cartPrice = cartPrice;
	}
}

package com.zipdoc.acs.model.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.model.ListRequest;

/**
 * 상품 리스트
 * Created by dskim on 2017. 5. 11..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ShopGoodsListRequest extends ListRequest {


    //상품타입
    private String goods_type;

    //카테고리 코드(1depth)
    private String category_code;

    //카테고리 코드(2depth)
    private String category_code2;

    //카테고리 코드(3depth)
    private String category_code3;

    //정렬조건 (판매순:0,최신순:1,인기순:2,가격순:3)
    private Integer sort = 0;

    //검색어
    private String keyword;

    public String getGoods_type() {
        return goods_type;
    }

    public void setGoods_type(String goods_type) {
        this.goods_type = goods_type;
    }

    public String getCategory_code() {
        return category_code;
    }

    public void setCategory_code(String category_code) {
        this.category_code = category_code;
    }

    public String getCategory_code2() {
        return category_code2;
    }

    public void setCategory_code2(String category_code2) {
        this.category_code2 = category_code2;
    }

    public String getCategory_code3() {
        return category_code3;
    }

    public void setCategory_code3(String category_code3) {
        this.category_code3 = category_code3;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String getKeyword() {
        return keyword;
    }

    @Override
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}

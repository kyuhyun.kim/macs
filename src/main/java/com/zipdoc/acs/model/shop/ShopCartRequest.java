package com.zipdoc.acs.model.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.CartPrice;
import com.zipdoc.acs.model.ListRequest;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ShopCartRequest extends ListRequest{
	private boolean goods_single_type = false;
	private CartPrice cartPrice = new CartPrice();
	private Long member_no;
	private String nonmember_no;
	
	public ShopCartRequest() {
	}
	
	public ShopCartRequest(int page, int limit){
		setPageLimit(page, limit);
	}
	
	public boolean isGoods_single_type() {
		return goods_single_type;
	}
	public void setGoods_single_type(boolean goods_single_type) {
		this.goods_single_type = goods_single_type;
	}
	public CartPrice getCartPrice() {
		return cartPrice;
	}
	public void setCartPrice(CartPrice cartPrice) {
		this.cartPrice = cartPrice;
	}

	public Long getMember_no() {
		return member_no;
	}

	public void setMember_no(Long member_no) {
		this.member_no = member_no;
	}

	public String getNonmember_no() {
		return nonmember_no;
	}

	public void setNonmember_no(String nonmember_no) {
		this.nonmember_no = nonmember_no;
	}
}

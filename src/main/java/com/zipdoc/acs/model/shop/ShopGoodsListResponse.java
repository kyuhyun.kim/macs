package com.zipdoc.acs.model.shop;

import com.zipdoc.acs.domain.entity.ShopGoods;

import java.util.List;

/**
 * 상품 리스트
 * Created by dskim on 2017. 5. 11..
 */
public class ShopGoodsListResponse {

    private int total_count;
    private List<ShopGoods> list;


    public ShopGoodsListResponse(int total_count, List<ShopGoods> list) {
        this.total_count = total_count;
        this.list = list;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public List<ShopGoods> getList() {
        return list;
    }

    public void setList(List<ShopGoods> list) {
        this.list = list;
    }
}

package com.zipdoc.acs.model.shop;


import com.zipdoc.acs.define.ShopGoodsOption;
import com.zipdoc.acs.domain.entity.*;

import java.util.List;

/**
 * Created by dskim on 2017. 5. 12..
 */
public class ShopOrderRequest {

    private long goods_no;

    private ShopGoods shopGoods = new ShopGoods();
    private ShopOrderUserInfo shopOrderUserInfo = new ShopOrderUserInfo();
    private ShopOrder shopOrder = new ShopOrder();
    private List<ShopOrderGoods> shopOrderGoods;
    private ShopGoodsOption shopGoodsOption = new ShopGoodsOption();
    private ShopOrderPayHistory shopOrderPayHistory = new ShopOrderPayHistory();
    private List<ShopCart> shopCart;
    private boolean order_type = false;

    private String category_code;
    private String category_code2;
    private String category_code3;
    private Integer goods_type;
    private boolean best = false;

    //2017-03-27 단품일경우
    private boolean goods_single_type = false;

    //2017-03-31 적립금 파라미터 추가
    private Integer reserves;


    public long getGoods_no() {
        return goods_no;
    }

    public void setGoods_no(long goods_no) {
        this.goods_no = goods_no;
    }

    public ShopGoods getShopGoods() {
        return shopGoods;
    }

    public void setShopGoods(ShopGoods shopGoods) {
        this.shopGoods = shopGoods;
    }

    public ShopOrderUserInfo getShopOrderUserInfo() {
        return shopOrderUserInfo;
    }

    public void setShopOrderUserInfo(ShopOrderUserInfo shopOrderUserInfo) {
        this.shopOrderUserInfo = shopOrderUserInfo;
    }

    public ShopOrder getShopOrder() {
        return shopOrder;
    }

    public void setShopOrder(ShopOrder shopOrder) {
        this.shopOrder = shopOrder;
    }

    public List<ShopOrderGoods> getShopOrderGoods() {
        return shopOrderGoods;
    }

    public void setShopOrderGoods(List<ShopOrderGoods> shopOrderGoods) {
        this.shopOrderGoods = shopOrderGoods;
    }

    public ShopGoodsOption getShopGoodsOption() {
        return shopGoodsOption;
    }

    public void setShopGoodsOption(ShopGoodsOption shopGoodsOption) {
        this.shopGoodsOption = shopGoodsOption;
    }

    public ShopOrderPayHistory getShopOrderPayHistory() {
        return shopOrderPayHistory;
    }

    public void setShopOrderPayHistory(ShopOrderPayHistory shopOrderPayHistory) {
        this.shopOrderPayHistory = shopOrderPayHistory;
    }

    public List<ShopCart> getShopCart() {
        return shopCart;
    }

    public void setShopCart(List<ShopCart> shopCart) {
        this.shopCart = shopCart;
    }

    public boolean isOrder_type() {
        return order_type;
    }

    public void setOrder_type(boolean order_type) {
        this.order_type = order_type;
    }

    public String getCategory_code() {
        return category_code;
    }

    public void setCategory_code(String category_code) {
        this.category_code = category_code;
    }

    public String getCategory_code2() {
        return category_code2;
    }

    public void setCategory_code2(String category_code2) {
        this.category_code2 = category_code2;
    }

    public String getCategory_code3() {
        return category_code3;
    }

    public void setCategory_code3(String category_code3) {
        this.category_code3 = category_code3;
    }

    public Integer getGoods_type() {
        return goods_type;
    }

    public void setGoods_type(Integer goods_type) {
        this.goods_type = goods_type;
    }

    public boolean isBest() {
        return best;
    }

    public void setBest(boolean best) {
        this.best = best;
    }

    public boolean isGoods_single_type() {
        return goods_single_type;
    }

    public void setGoods_single_type(boolean goods_single_type) {
        this.goods_single_type = goods_single_type;
    }

    public Integer getReserves() {
        return reserves;
    }

    public void setReserves(Integer reserves) {
        this.reserves = reserves;
    }
}

package com.zipdoc.acs.model.shop;


import com.zipdoc.acs.domain.entity.ShopGoods;

import java.util.List;

/**
 * Created by dskim on 2017. 5. 15..
 */
public class ShopMainResponse {

    //베스트 상품
    List<ShopGoods> bestGoodsList;

    //신상품
    List<ShopGoods> newGoodsList;

    //패키지상품
    List<ShopGoods> packageGoodsList;

    public List<ShopGoods> getBestGoodsList() {
        return bestGoodsList;
    }

    public void setBestGoodsList(List<ShopGoods> bestGoodsList) {
        this.bestGoodsList = bestGoodsList;
    }

    public List<ShopGoods> getNewGoodsList() {
        return newGoodsList;
    }

    public void setNewGoodsList(List<ShopGoods> newGoodsList) {
        this.newGoodsList = newGoodsList;
    }

    public List<ShopGoods> getPackageGoodsList() {
        return packageGoodsList;
    }

    public void setPackageGoodsList(List<ShopGoods> packageGoodsList) {
        this.packageGoodsList = packageGoodsList;
    }
}

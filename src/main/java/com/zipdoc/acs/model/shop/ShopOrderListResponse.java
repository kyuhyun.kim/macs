package com.zipdoc.acs.model.shop;

import java.util.List;

import com.zipdoc.acs.domain.entity.ShopOrder;

public class ShopOrderListResponse{
	private int total_count;
	private List<ShopOrder> list;
	
	public ShopOrderListResponse() {
	}

	public ShopOrderListResponse(int total_count, List<ShopOrder> list) {
		this.total_count = total_count;
		this.list = list;
	}
	
	public int getTotal_count() {
		return total_count;
	}
	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}
	public List<ShopOrder> getList() {
		return list;
	}
	public void setList(List<ShopOrder> list) {
		this.list = list;
	}
}

package com.zipdoc.acs.model.shop;

import java.util.List;

import com.zipdoc.acs.domain.entity.ShopDeliveryManagement;

public class ShopDeliveryListResponse {
	private int total_count;
	private List<ShopDeliveryManagement> list;
	
	public ShopDeliveryListResponse() {
	}

	public ShopDeliveryListResponse(int total_count, List<ShopDeliveryManagement> list) {
		this.total_count = total_count;
		this.list = list;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<ShopDeliveryManagement> getList() {
		return list;
	}

	public void setList(List<ShopDeliveryManagement> list) {
		this.list = list;
	}
}

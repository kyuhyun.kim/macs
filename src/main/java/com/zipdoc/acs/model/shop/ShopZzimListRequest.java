package com.zipdoc.acs.model.shop;

import com.zipdoc.acs.model.ListRequest;

public class ShopZzimListRequest extends ListRequest{

	public ShopZzimListRequest() {
	}
	
	public ShopZzimListRequest(long member_no, int page, int limit){
		super.setMember_no(member_no);
		setPageLimit(page, limit);
	}
}

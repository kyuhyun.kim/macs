package com.zipdoc.acs.model;

import com.zipdoc.acs.domain.entity.MyInterior;

import java.util.List;

public class MyInteriorListResponse {
	private int total_count;
    private List<MyInterior> list;

	public MyInteriorListResponse(){

	}

	public MyInteriorListResponse(int total_count, List<MyInterior> list) {
		this.list = list;
		this.total_count = total_count;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<MyInterior> getList() {
		return list;
	}

	public void setList(List<MyInterior> list) {
		this.list = list;
	}
}

package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.Partner;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerFull extends Partner {

    //2017.12.13. hansol
    private Integer assign_status;
    private Integer kind;
    private Integer evaluation_point;
    private Integer evaluation_point1;
    private Integer evaluation_point2;
    private Integer evaluation_point3;
    private Integer evaluation_point4;
    private Integer partner_manager_no;
    private String partner_manager;
    private String address_detail;
    private String phone_no;
    private String main_product;
    private String required_region1;
    private String required_region2;
    private String required_region3;
    private String partner_memo;
    private String main_construction;
    private String avoid_construction;
    private String homepage;
    private String ceo_name;
    private String biz_no;
    private Integer pause_count;
    private Long pause_start_date;
    private Long pause_end_date;
    private String post_no;
    private Long assign_stop_date;
    private Integer assign_stop_month_fee;
    private Integer assign_stop_doc;
    private Integer assign_stop_commition;
    private Integer assign_stop_admin;
    private Integer month_fee_method;
    private Long join_date;
    private String company_name;
    private Long company_established_date;
    private String real_post_no;
    private String real_address;
    private String real_address_detail;
    private String tax_bill_email;
    private String manager_name;
    private String manager_phone_no;
    private Integer design_enabled;
    private Integer license_flag;
    private Long create_date;
    private Long update_date;
    private String creater;
    private String updater;
    private String eval_updater;
    private Long contract_start_date;
    private Long contract_end_date;
    private Integer reserved_stop_status;
    private Integer branch_no;
    private Integer main_branch_status;
    private Integer biz_period;

    public Integer getAssign_status() {
        return assign_status;
    }

    public Integer getKind() {
        return kind;
    }

    public Integer getEvaluation_point() {
        return evaluation_point;
    }

    public Integer getEvaluation_point1() {
        return evaluation_point1;
    }

    public Integer getEvaluation_point2() {
        return evaluation_point2;
    }

    public Integer getEvaluation_point3() {
        return evaluation_point3;
    }

    public Integer getEvaluation_point4() {
        return evaluation_point4;
    }

    public Integer getPartner_manager_no() {
        return partner_manager_no;
    }

    public String getPartner_manager() {
        return partner_manager;
    }

    public String getAddress_detail() {
        return address_detail;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public String getMain_product() {
        return main_product;
    }

    public String getRequired_region1() {
        return required_region1;
    }

    public String getRequired_region2() {
        return required_region2;
    }

    public String getRequired_region3() {
        return required_region3;
    }

    public String getPartner_memo() {
        return partner_memo;
    }

    public String getMain_construction() {
        return main_construction;
    }

    public String getAvoid_construction() {
        return avoid_construction;
    }

    public String getHomepage() {
        return homepage;
    }

    public String getCeo_name() {
        return ceo_name;
    }

    public String getBiz_no() {
        return biz_no;
    }

    public Integer getPause_count() {
        return pause_count;
    }

    public Long getPause_start_date() {
        return pause_start_date;
    }

    public Long getPause_end_date() {
        return pause_end_date;
    }

    public String getPost_no() {
        return post_no;
    }

    public Long getAssign_stop_date() {
        return assign_stop_date;
    }

    public Integer getAssign_stop_month_fee() {
        return assign_stop_month_fee;
    }

    public Integer getAssign_stop_doc() {
        return assign_stop_doc;
    }

    public Integer getAssign_stop_commition() {
        return assign_stop_commition;
    }

    public Integer getAssign_stop_admin() {
        return assign_stop_admin;
    }

    public Integer getMonth_fee_method() {
        return month_fee_method;
    }

    public Long getJoin_date() {
        return join_date;
    }

    public String getCompany_name() {
        return company_name;
    }

    public Long getCompany_established_date() {
        return company_established_date;
    }

    public String getReal_post_no() {
        return real_post_no;
    }

    public String getReal_address() {
        return real_address;
    }

    public String getReal_address_detail() {
        return real_address_detail;
    }

    public String getTax_bill_email() {
        return tax_bill_email;
    }

    public String getManager_name() {
        return manager_name;
    }

    public String getManager_phone_no() {
        return manager_phone_no;
    }

    public Integer getDesign_enabled() {
        return design_enabled;
    }

    public Integer getLicense_flag() {
        return license_flag;
    }

    public Long getCreate_date() {
        return create_date;
    }

    public Long getUpdate_date() {
        return update_date;
    }

    public String getCreater() {
        return creater;
    }

    public String getUpdater() {
        return updater;
    }

    public String getEval_updater() {
        return eval_updater;
    }

    public Long getContract_start_date() {
        return contract_start_date;
    }

    public Long getContract_end_date() {
        return contract_end_date;
    }

    public Integer getReserved_stop_status() {
        return reserved_stop_status;
    }

    public Integer getBranch_no() {
        return branch_no;
    }

    public Integer getMain_branch_status() {
        return main_branch_status;
    }

    public void setAssign_status(Integer assign_status) {
        this.assign_status = assign_status;
    }

    public void setKind(Integer kind) {
        this.kind = kind;
    }

    public void setEvaluation_point(Integer evaluation_point) {
        this.evaluation_point = evaluation_point;
    }

    public void setEvaluation_point1(Integer evaluation_point1) {
        this.evaluation_point1 = evaluation_point1;
    }

    public void setEvaluation_point2(Integer evaluation_point2) {
        this.evaluation_point2 = evaluation_point2;
    }

    public void setEvaluation_point3(Integer evaluation_point3) {
        this.evaluation_point3 = evaluation_point3;
    }

    public void setEvaluation_point4(Integer evaluation_point4) {
        this.evaluation_point4 = evaluation_point4;
    }

    public void setPartner_manager_no(Integer partner_manager_no) {
        this.partner_manager_no = partner_manager_no;
    }

    public void setPartner_manager(String partner_manager) {
        this.partner_manager = partner_manager;
    }

    public void setAddress_detail(String address_detail) {
        this.address_detail = address_detail;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public void setMain_product(String main_product) {
        this.main_product = main_product;
    }

    public void setRequired_region1(String required_region1) {
        this.required_region1 = required_region1;
    }

    public void setRequired_region2(String required_region2) {
        this.required_region2 = required_region2;
    }

    public void setRequired_region3(String required_region3) {
        this.required_region3 = required_region3;
    }

    public void setPartner_memo(String partner_memo) {
        this.partner_memo = partner_memo;
    }

    public void setMain_construction(String main_construction) {
        this.main_construction = main_construction;
    }

    public void setAvoid_construction(String avoid_construction) {
        this.avoid_construction = avoid_construction;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public void setCeo_name(String ceo_name) {
        this.ceo_name = ceo_name;
    }

    public void setBiz_no(String biz_no) {
        this.biz_no = biz_no;
    }

    public void setPause_count(Integer pause_count) {
        this.pause_count = pause_count;
    }

    public void setPause_start_date(Long pause_start_date) {
        this.pause_start_date = pause_start_date;
    }

    public void setPause_end_date(Long pause_end_date) {
        this.pause_end_date = pause_end_date;
    }

    public void setPost_no(String post_no) {
        this.post_no = post_no;
    }

    public void setAssign_stop_date(Long assign_stop_date) {
        this.assign_stop_date = assign_stop_date;
    }

    public void setAssign_stop_month_fee(Integer assign_stop_month_fee) {
        this.assign_stop_month_fee = assign_stop_month_fee;
    }

    public void setAssign_stop_doc(Integer assign_stop_doc) {
        this.assign_stop_doc = assign_stop_doc;
    }

    public void setAssign_stop_commition(Integer assign_stop_commition) {
        this.assign_stop_commition = assign_stop_commition;
    }

    public void setAssign_stop_admin(Integer assign_stop_admin) {
        this.assign_stop_admin = assign_stop_admin;
    }

    public void setMonth_fee_method(Integer month_fee_method) {
        this.month_fee_method = month_fee_method;
    }

    public void setJoin_date(Long join_date) {
        this.join_date = join_date;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public void setCompany_established_date(Long company_established_date) {
        this.company_established_date = company_established_date;
    }

    public void setReal_post_no(String real_post_no) {
        this.real_post_no = real_post_no;
    }

    public void setReal_address(String real_address) {
        this.real_address = real_address;
    }

    public void setReal_address_detail(String real_address_detail) {
        this.real_address_detail = real_address_detail;
    }

    public void setTax_bill_email(String tax_bill_email) {
        this.tax_bill_email = tax_bill_email;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public void setManager_phone_no(String manager_phone_no) {
        this.manager_phone_no = manager_phone_no;
    }

    public void setDesign_enabled(Integer design_enabled) {
        this.design_enabled = design_enabled;
    }

    public void setLicense_flag(Integer license_flag) {
        this.license_flag = license_flag;
    }

    public void setCreate_date(Long create_date) {
        this.create_date = create_date;
    }

    public void setUpdate_date(Long update_date) {
        this.update_date = update_date;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public void setEval_updater(String eval_updater) {
        this.eval_updater = eval_updater;
    }

    public void setContract_start_date(Long contract_start_date) {
        this.contract_start_date = contract_start_date;
    }

    public void setContract_end_date(Long contract_end_date) {
        this.contract_end_date = contract_end_date;
    }

    public void setReserved_stop_status(Integer reserved_stop_status) {
        this.reserved_stop_status = reserved_stop_status;
    }

    public void setBranch_no(Integer branch_no) {
        this.branch_no = branch_no;
    }

    public void setMain_branch_status(Integer main_branch_status) {
        this.main_branch_status = main_branch_status;
    }

    public Integer getBiz_period() {
        return biz_period;
    }

    public void setBiz_period(Integer biz_period) {
        this.biz_period = biz_period;
    }
}

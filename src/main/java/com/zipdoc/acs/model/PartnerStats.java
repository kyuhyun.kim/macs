package com.zipdoc.acs.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.PartnerAssign;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerStats {
	
	// 파트너 업체명
	private String partner_name;
	
	// 사용중인 서비스 상품명
	private String product_name;
	
	// 누적 계약 건수
	private Long total_contracts;
	
	// 누적 매출액
	private Long total_sales;
	
	// 통계 집계 시작일
	private Long stats_start_date;
	
	// 통계 집계 완료일
	private Long stats_end_date;
	
	// 가입 상품의 기본 제공 견적 건수
	private Integer service_cnt;
	
	// 이번달 받은 견적 건수
	private Integer assign_cnt;
	
	// 이번달 주거 견적 건수
	private Integer residential_cnt;
	
	// 이번달 상업 견적 건수
	private Integer commercial_cnt;

	// 이번달 무료 건적 건수
	private Integer free_cnt;
	
	// 견적 제공 내역
    private List<PartnerAssign> list;

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public Long getTotal_contracts() {
		return total_contracts;
	}

	public void setTotal_contracts(Long total_contracts) {
		this.total_contracts = total_contracts;
	}

	public Long getTotal_sales() {
		return total_sales;
	}

	public void setTotal_sales(Long total_sales) {
		this.total_sales = total_sales;
	}

	public Long getStats_start_date() {
		return stats_start_date;
	}

	public void setStats_start_date(Long stats_start_date) {
		this.stats_start_date = stats_start_date;
	}

	public Long getStats_end_date() {
		return stats_end_date;
	}

	public void setStats_end_date(Long stats_end_date) {
		this.stats_end_date = stats_end_date;
	}

	public Integer getService_cnt() {
		return service_cnt;
	}

	public void setService_cnt(Integer service_cnt) {
		this.service_cnt = service_cnt;
	}

	public Integer getAssign_cnt() {
		return assign_cnt;
	}

	public void setAssign_cnt(Integer assign_cnt) {
		this.assign_cnt = assign_cnt;
	}

	public Integer getResidential_cnt() {
		return residential_cnt;
	}

	public void setResidential_cnt(Integer residential_cnt) {
		this.residential_cnt = residential_cnt;
	}

	public Integer getCommercial_cnt() {
		return commercial_cnt;
	}

	public void setCommercial_cnt(Integer commercial_cnt) {
		this.commercial_cnt = commercial_cnt;
	}

	public List<PartnerAssign> getList() {
		return list;
	}

	public void setList(List<PartnerAssign> list) {
		this.list = list;
	}

	public Integer getFree_cnt() {
		return free_cnt;
	}

	public void setFree_cnt(Integer free_cnt) {
		this.free_cnt = free_cnt;
	}
}

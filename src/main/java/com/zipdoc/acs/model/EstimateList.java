package com.zipdoc.acs.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.domain.entity.Estimate;

public class EstimateList {

	private int total_count;
    private List<Estimate> list;

    public EstimateList(int total_count, List<Estimate> list) {
        this.total_count = total_count;
        this.list = list;
    }

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<Estimate> getList() {
		return list;
	}

	public void setList(List<Estimate> list) {
		this.list = list;
	}
}

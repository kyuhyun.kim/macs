package com.zipdoc.acs.model;

public class PartnerProductStats {

    private int product_code;
    private String product_name;
    private int count;

    public int getProduct_code() {
        return product_code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public int getCount() {
        return count;
    }

    public void setProduct_code(int product_code) {
        this.product_code = product_code;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public void setCount(int count) {
        this.count = count;
    }
}

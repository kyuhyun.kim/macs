package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.Cause;
import com.zipdoc.acs.domain.entity.Item;
import com.zipdoc.acs.domain.entity.Product;

import java.util.List;

/**
 * 성공 및 실패원인만 포함하는 응답 메시지 전문
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class CauseResponse {

    private int cause;
    private String description;

    public CauseResponse() {
    }

    public CauseResponse(int cause) {
        this.cause = cause;
    }

    public CauseResponse(Cause cause) {
        this.cause = cause.value();
    }

    public CauseResponse(int cause, String description) {
        this.cause = cause;
        this.description = description;
    }

    public CauseResponse(Cause cause, String description) {
        this.cause = cause.value();
        this.description = description;
    }

    public int getCause() {
        return cause;
    }

    public void setCause(int cause) {
        this.cause = cause;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 이동규 on 2016-06-09.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Region {

    private String region;
    private List<String> subregions = new ArrayList<String>();

    public Region(){

    }

    public Region(String region, String subregion){
        this.region = region;
        addSubregion(subregion);
    }

    public void addSubregion(String subregion){
        this.subregions.add(subregion);
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public List<String> getSubregions() {
        return subregions;
    }

    public void setSubregions(List<String> subregions) {
        this.subregions = subregions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Region)) return false;

        Region region1 = (Region) o;

        if (!getRegion().equals(region1.getRegion())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = getRegion().hashCode();
        return result;
    }
}

package com.zipdoc.acs.model;

import java.util.List;

public class PartnerStatsResponse {

    // 해당지역의 파트너스 총개수
    private int total_count;

    // 평가점수
    private int evaluation_none;
    private int evaluation_d;
    private int evaluation_c;
    private int evaluation_b;
    private int evaluation_a;
    private int evaluation_s;
    private int evaluation_f;

    // 계약통계
    private int total_contract_count;
    private long total_contract_commission;
    private long total_contract_price;

    // 계약상품 통계
    private List<PartnerProductStats> products_rank;

    public int getTotal_count() {
        return total_count;
    }

    public int getEvaluation_none() {
        return evaluation_none;
    }

    public int getEvaluation_d() {
        return evaluation_d;
    }

    public int getEvaluation_c() {
        return evaluation_c;
    }

    public int getEvaluation_b() {
        return evaluation_b;
    }

    public int getEvaluation_a() {
        return evaluation_a;
    }

    public int getEvaluation_s() {
        return evaluation_s;
    }

    public int getEvaluation_f() {
        return evaluation_f;
    }

    public int getTotal_contract_count() {
        return total_contract_count;
    }

    public long getTotal_contract_commission() {
        return total_contract_commission;
    }

    public List<PartnerProductStats> getProducts_rank() {
        return products_rank;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public void setEvaluation_none(int evaluation_none) {
        this.evaluation_none = evaluation_none;
    }

    public void setEvaluation_d(int evaluation_d) {
        this.evaluation_d = evaluation_d;
    }

    public void setEvaluation_c(int evaluation_c) {
        this.evaluation_c = evaluation_c;
    }

    public void setEvaluation_b(int evaluation_b) {
        this.evaluation_b = evaluation_b;
    }

    public void setEvaluation_a(int evaluation_a) {
        this.evaluation_a = evaluation_a;
    }

    public void setEvaluation_s(int evaluation_s) {
        this.evaluation_s = evaluation_s;
    }

    public void setEvaluation_f(int evaluation_f) {
        this.evaluation_f = evaluation_f;
    }

    public void setTotal_contract_count(int total_contract_count) {
        this.total_contract_count = total_contract_count;
    }

    public void setTotal_contract_commission(long total_contract_commission) {
        this.total_contract_commission = total_contract_commission;
    }

    public long getTotal_contract_price() {
        return total_contract_price;
    }

    public void setTotal_contract_price(long total_contract_price) {
        this.total_contract_price = total_contract_price;
    }

    public void setProducts_rank(List<PartnerProductStats> products_rank) {
        this.products_rank = products_rank;
    }
}

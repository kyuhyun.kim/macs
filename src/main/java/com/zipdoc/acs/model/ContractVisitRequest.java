package com.zipdoc.acs.model;

/**
 * 현장방문 계약 조회
 * @author 김종부
 *
 */
public class ContractVisitRequest {
	
	/**
		현장방문일 기준 검색 시작일
	 */
	private Long visit_start_date;
	
	/**
		현장방문일 기준 검색 종료일
	 */
	private Long visit_end_date;

	/**
	 * 검색 옵션
	 */
	private String search_condition;

	/**
	 * 검색 내용
	 */
	private String search_value;


	public Long getVisit_start_date() {
		return visit_start_date;
	}

	public void setVisit_start_date(Long visit_start_date) {
		this.visit_start_date = visit_start_date;
	}

	public Long getVisit_end_date() {
		return visit_end_date;
	}

	public void setVisit_end_date(Long visit_end_date) {
		this.visit_end_date = visit_end_date;
	}

	public String getSearch_condition() {
		return search_condition;
	}

	public void setSearch_condition(String search_condition) {
		this.search_condition = search_condition;
	}

	public String getSearch_value() {
		return search_value;
	}

	public void setSearch_value(String search_value) {
		this.search_value = search_value;
	}
}

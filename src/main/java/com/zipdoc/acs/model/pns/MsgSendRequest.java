package com.zipdoc.acs.model.pns;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.pns.MsgType;
import com.zipdoc.acs.domain.entity.*;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MsgSendRequest {


    private Long member_no;
    private List<String> dstuser;
    private String msgtype;
    private String subject;
    private String text;
    private Long send_date;
    private List<MmsContentsFile> mmsContentsFiles;

    private String callback;
    private Long estimate_no;
    private Long contract_no;
    private String contents_type;
    private String data_type;
    private String etc;

    public MsgSendRequest()
    {

    }

    public MsgSendRequest(Estimate estimate, NotificationPhrase notificationPhrase )
    {

        if(notificationPhrase.getPhrase().getBytes().length >= 140)
            this.msgtype= MsgType.MMS.getName();
        else
            this.msgtype= MsgType.SMS.getName();
        this.estimate_no = estimate.getEstimate_no();
        if(this.dstuser == null)
            this.dstuser = new ArrayList<>();

        this.dstuser.add(estimate.getPhone_no());
        this.subject = notificationPhrase.getSubject();
        this.text =  notificationPhrase.getPhrase().replace("#{username}", estimate.getWriter());
        this.contents_type = notificationPhrase.getPhrase_cd();
        this.data_type = "ESTIMATE";

        try {
            this.etc = InetAddress.getLocalHost().getHostName();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public MsgSendRequest(MyContract contract, SatisfactionRequest satisfactionRequest, NotificationPhrase notificationPhrase)
    {

        if(notificationPhrase.getPhrase().getBytes().length >= 140)
            this.msgtype= MsgType.MMS.getName();
        else
            this.msgtype= MsgType.SMS.getName();

        if(this.dstuser == null)
            this.dstuser = new ArrayList<>();

        this.dstuser.add(contract.getPhone_no());
        this.subject = notificationPhrase.getSubject();
        this.text =  notificationPhrase.getPhrase().replace("#{username}", contract.getContracter());
        this.text =  this.text.replace("#{url}", satisfactionRequest.getShort_url());
        this.contents_type = notificationPhrase.getPhrase_cd();
        this.data_type = "CONTRACT";
        this.contract_no = contract.getContract_no();
        this.member_no = satisfactionRequest.getMember_no();

        try {
            this.etc = InetAddress.getLocalHost().getHostName();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public String getEtc() {
        return etc;
    }

    public void setEtc(String etc) {
        this.etc = etc;
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public List<String> getDstuser() {
        return dstuser;
    }

    public void setDstuser(List<String> dstuser) {
        this.dstuser = dstuser;
    }

    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getSend_date() {
        return send_date;
    }

    public void setSend_date(Long send_date) {
        this.send_date = send_date;
    }

    public List<MmsContentsFile> getMmsContentsFiles() {
        return mmsContentsFiles;
    }

    public void setMmsContentsFiles(List<MmsContentsFile> mmsContentsFiles) {
        this.mmsContentsFiles = mmsContentsFiles;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public Long getEstimate_no() {
        return estimate_no;
    }

    public void setEstimate_no(Long estimate_no) {
        this.estimate_no = estimate_no;
    }

    public Long getContract_no() {
        return contract_no;
    }

    public void setContract_no(Long contract_no) {
        this.contract_no = contract_no;
    }

    public String getContents_type() {
        return contents_type;
    }

    public void setContents_type(String contents_type) {
        this.contents_type = contents_type;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }
}

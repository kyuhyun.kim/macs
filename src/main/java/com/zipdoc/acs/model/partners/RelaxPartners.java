package com.zipdoc.acs.model.partners;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.Partner;

import java.util.List;

/**
 * Created by zipdoc on 2019-01-16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RelaxPartners {

    private Integer total_count;
    private List<Partner> relax_list;


    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public List<Partner> getRelax_list() {
        return relax_list;
    }

    public void setRelax_list(List<Partner> relax_list) {
        this.relax_list = relax_list;
    }
}

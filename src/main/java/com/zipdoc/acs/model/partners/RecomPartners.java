package com.zipdoc.acs.model.partners;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.Partner;

import java.util.List;

/**
 * Created by zipdoc on 2019-01-16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecomPartners {

    private String month;
    private Integer total_count;
    private String best_year;
    private Integer best_quarter;

    // 월간 추천 파트너스
    private List<Partner> list;

    public String getBest_year() {
        return best_year;
    }

    public void setBest_year(String best_year) {
        this.best_year = best_year;
    }

    public Integer getBest_quarter() {
        return best_quarter;
    }

    public void setBest_quarter(Integer best_quarter) {
        this.best_quarter = best_quarter;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public List<Partner> getList() {
        return list;
    }

    public void setList(List<Partner> list) {
        this.list = list;
    }
}

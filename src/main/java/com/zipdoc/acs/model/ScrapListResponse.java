package com.zipdoc.acs.model;

import com.zipdoc.acs.domain.entity.Item;
import com.zipdoc.acs.domain.entity.Product;

import java.util.List;

/**
 * 스크랩 목록 (시공사례, 시공사진)
 */
public class ScrapListResponse {

    private Long book_no;
    private String book_name;
    private Integer total_count;
    private Integer gallery_total_count;
    private Integer picture_total_count;

    private List<Product> gallery_list;
    private List<Item> picture_list;

    public Long getBook_no() {
        return book_no;
    }

    public void setBook_no(Long book_no) {
        this.book_no = book_no;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public Integer getGallery_total_count() {
        return gallery_total_count;
    }

    public void setGallery_total_count(Integer gallery_total_count) {
        this.gallery_total_count = gallery_total_count;
    }

    public Integer getPicture_total_count() {
        return picture_total_count;
    }

    public void setPicture_total_count(Integer picture_total_count) {
        this.picture_total_count = picture_total_count;
    }

    public List<Product> getGallery_list() {
        return gallery_list;
    }

    public void setGallery_list(List<Product> gallery_list) {
        this.gallery_list = gallery_list;
    }

    public List<Item> getPicture_list() {
        return picture_list;
    }

    public void setPicture_list(List<Item> picture_list) {
        this.picture_list = picture_list;
    }
}

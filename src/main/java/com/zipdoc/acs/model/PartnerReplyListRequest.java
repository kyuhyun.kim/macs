package com.zipdoc.acs.model;

/**
 * Created by ZIPDOC on 2016-06-13.
 */
public class PartnerReplyListRequest extends ListRequest {
    private Long member_no;
    private Integer partner_id;

    public PartnerReplyListRequest(){
    }

    public PartnerReplyListRequest(int page, int limit){
        setPageLimit(page, limit);
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public Integer getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(Integer partner_id) {
        this.partner_id = partner_id;
    }
}

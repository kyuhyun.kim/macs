package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.Map;

/**
 * Created by ZIPDOC on 2016-06-02.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ListRequest {
    private int page = 0;
    private int limit = 20;
    private String keyword;
    private List<String> keywords;
    private Integer random;
    private Long member_no;
    private Integer agent;
    private String searchKeyword;

    private Map<String, Object> keywordsMap;

    public Map<String, Object> getKeywordsMap() {
        return keywordsMap;
    }

    public void setKeywordsMap(Map<String, Object> keywordsMap) {
        this.keywordsMap = keywordsMap;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getStartRow() {
        return this.page * limit;
    }

    public void setPageLimit(int page, int limit) {
        this.page = page;
        this.limit = limit;
    }

    public void setPageLimit(int page, int limit, String keyword) {
        this.page = page;
        this.limit = limit;
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getRandom() {
        return random;
    }

    public void setRandom(Integer random) {
        this.random = random;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public Integer getAgent() {
        return agent;
    }

    public void setAgent(Integer agent) {
        this.agent = agent;
    }

    @Override
    public String toString() {
        return "ListRequest{" +
                "page=" + page +
                ", limit=" + limit +
                ", keyword='" + keyword + '\'' +
                '}';
    }
}

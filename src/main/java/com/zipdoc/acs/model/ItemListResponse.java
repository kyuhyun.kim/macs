package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.Item;
import com.zipdoc.acs.domain.entity.Product;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ItemListResponse {
	private int total_count;
    private List<Item> list;

	public ItemListResponse(){

	}

	public ItemListResponse(int total_count, List<Item> list) {
		this.list = list;
		this.total_count = total_count;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<Item> getList() {
		return list;
	}

	public void setList(List<Item> list) {
		this.list = list;
	}
}

package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * PC기준으로 기본값이 설정되어 있음
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class SearchListRequest extends ListRequest {

    private Integer resident_gallery_limit = 9;
    private Integer resident_picture_limit = 16;
    private Integer commerce_gallery_limit = 15;
    private Integer partners_limit = 9;
    private Integer magazine_limit = 9;
    private String category_code1;

    public Integer getResident_gallery_limit() {
        return resident_gallery_limit;
    }

    public void setResident_gallery_limit(Integer resident_gallery_limit) {
        this.resident_gallery_limit = resident_gallery_limit;
    }

    public Integer getResident_picture_limit() {
        return resident_picture_limit;
    }

    public void setResident_picture_limit(Integer resident_picture_limit) {
        this.resident_picture_limit = resident_picture_limit;
    }

    public Integer getCommerce_gallery_limit() {
        return commerce_gallery_limit;
    }

    public void setCommerce_gallery_limit(Integer commerce_gallery_limit) {
        this.commerce_gallery_limit = commerce_gallery_limit;
    }

    public Integer getPartners_limit() {
        return partners_limit;
    }

    public void setPartners_limit(Integer partners_limit) {
        this.partners_limit = partners_limit;
    }

    public Integer getMagazine_limit() {
        return magazine_limit;
    }

    public void setMagazine_limit(Integer magazine_limit) {
        this.magazine_limit = magazine_limit;
    }

    public String getCategory_code1() {
        return category_code1;
    }

    public void setCategory_code1(String category_code1) {
        this.category_code1 = category_code1;
    }
}

package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.ProductType;

import java.util.List;

/**
 * Created by ZIPDOC on 2016-06-02.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ProductListRequest extends ListRequest {
    private String category_code1;
    private List<String> category_code2;
    private Integer space_min;
    private Integer space_max;
    private String region1;
    private String region2;
    private Integer price_min;
    private Integer price_max;
    private List<String> styles;
    private List<String> colors;
    private String tag;
    private Integer partner_id;
    private Integer sort = 0;
    private Integer product_type = ProductType.GALLERY.getCode();
    private int owner = 0;
    private String BnA;
    private boolean beforeflag;
    private String searchKeyword;

    @JsonIgnore Long member_no;

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public boolean isBeforeflag() {
        return beforeflag;
    }

    public void setBeforeflag(boolean beforeflag) {
        this.beforeflag = beforeflag;
    }

    public String getBnA() {
        return BnA;
    }

    public void setBnA(String bnA) {
        BnA = bnA;
    }

    public String getCategory_code1() {
        return category_code1;
    }

    public void setCategory_code1(String category_code1) {
        this.category_code1 = category_code1;
    }

    public List<String> getCategory_code2() {
        return category_code2;
    }

    public void setCategory_code2(List<String> category_code2) {
        this.category_code2 = category_code2;
    }

    public Integer getSpace_min() {
        return space_min;
    }

    public void setSpace_min(Integer space_min) {
        this.space_min = space_min;
    }

    public Integer getSpace_max() {
        return space_max;
    }

    public void setSpace_max(Integer space_max) {
        this.space_max = space_max;
    }

    public String getRegion1() {
        return region1;
    }

    public void setRegion1(String region1) {
        this.region1 = region1;
    }

    public String getRegion2() {
        return region2;
    }

    public void setRegion2(String region2) {
        this.region2 = region2;
    }

    public Integer getPrice_min() {
        return price_min;
    }

    public void setPrice_min(Integer price_min) {
        this.price_min = price_min;
    }

    public Integer getPrice_max() {
        return price_max;
    }

    public void setPrice_max(Integer price_max) {
        this.price_max = price_max;
    }

    public List<String> getStyles() {
        return styles;
    }

    public void setStyles(List<String> styles) {
        this.styles = styles;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(Integer partner_id) {
        this.partner_id = partner_id;
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getProduct_type() {
        return product_type;
    }

    public void setProduct_type(Integer product_type) {
        this.product_type = product_type;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    @Override
    public String toString() {
        return "ProductListRequest{" +
                "category_code1='" + category_code1 + '\'' +
                ", category_code2=" + category_code2 +
                ", space_min=" + space_min +
                ", space_max=" + space_max +
                ", region1='" + region1 + '\'' +
                ", region2='" + region2 + '\'' +
                ", price_min=" + price_min +
                ", price_max=" + price_max +
                ", styles=" + styles +
                ", colors=" + colors +
                ", tag='" + tag + '\'' +
                ", partner_id=" + partner_id +
                ", keyword='" + getKeyword() + '\'' +
                '}';
    }
}

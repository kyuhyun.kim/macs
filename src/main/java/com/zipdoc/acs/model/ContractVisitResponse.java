package com.zipdoc.acs.model;

import com.zipdoc.acs.domain.entity.Contract;
import com.zipdoc.acs.domain.entity.ContractVisit;
import com.zipdoc.acs.domain.entity.Item;

import java.util.List;

public class ContractVisitResponse {
	private int total_count;
    private List<ContractVisit> list;

	public ContractVisitResponse() {

	}

	public ContractVisitResponse(int total_count, List<ContractVisit> list) {
		this.list = list;
		this.total_count = total_count;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<ContractVisit> getList() {
		return list;
	}

	public void setList(List<ContractVisit> list) {
		this.list = list;
	}
}

package com.zipdoc.acs.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.PartnerContract;

/**
 * 파트너 비용 관리
 * @author 김종부
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerCharge {

	// 서비스 비용.
	private int cost;

	// 부가세
	private int vat;

	// 월 회비 합계 (서비스 비용 + 부가세)
	private int month_fee;

	// 미납 건수
	private int unpaid_count;

	// 미납 총액
	private long unpaid_sum;

	// 계약일로부터 미납 기간
	private int expiration_days;

	// 미납 내역 리스트
	private List<PartnerContract> unpaid;

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getVat() {
		return vat;
	}

	public void setVat(int vat) {
		this.vat = vat;
	}

	public int getMonth_fee() {
		return month_fee;
	}

	public void setMonth_fee(int month_fee) {
		this.month_fee = month_fee;
	}

	public int getUnpaid_count() {
		return unpaid_count;
	}

	public void setUnpaid_count(int unpaid_count) {
		this.unpaid_count = unpaid_count;
	}

	public long getUnpaid_sum() {
		return unpaid_sum;
	}

	public void setUnpaid_sum(long unpaid_sum) {
		this.unpaid_sum = unpaid_sum;
	}

	public List<PartnerContract> getUnpaid() {
		return unpaid;
	}

	public void setUnpaid(List<PartnerContract> unpaid) {
		this.unpaid = unpaid;
	}

	public int getExpiration_days() {
		return expiration_days;
	}

	public void setExpiration_days(int expiration_days) {
		this.expiration_days = expiration_days;
	}
}

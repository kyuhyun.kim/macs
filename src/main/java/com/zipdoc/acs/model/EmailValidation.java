package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class EmailValidation {

    private String member_id;
	private Boolean emailExist;

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public Boolean getEmailExist() {
		return emailExist;
	}

	public void setEmailExist(Boolean emailExist) {
		this.emailExist = emailExist;
	}
}

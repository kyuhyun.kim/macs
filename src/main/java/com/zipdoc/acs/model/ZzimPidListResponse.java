package com.zipdoc.acs.model;

import com.zipdoc.acs.domain.entity.Item;

import java.util.List;

public class ZzimPidListResponse {
	private int total_count;
    private List<Long> list;

	public ZzimPidListResponse(){

	}

	public ZzimPidListResponse(int total_count, List<Long> list) {
		this.list = list;
		this.total_count = total_count;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<Long> getList() {
		return list;
	}

	public void setList(List<Long> list) {
		this.list = list;
	}
}

package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.*;
import com.zipdoc.acs.model.partners.RecomPartners;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class HomeScreenLayout {

	// 배너 영역
	private List<EventPromotion> banners;

	// 견적현황
	private EstimateSummary estimate;

	// 인기태그
	private List<PopularTag> tags;

	// 부분사진
	private List<LayoutPartial> partials;

	// 시공사례. 평형/컬러/스타일 중 하나의 필터가 포함된 시공사례
	private List<LayoutGallery>  filter_gallery;

	// 시공사례.
	private List<LayoutGallery>  gallery;

	// 퀵필터
	private List<LayoutQuickFilter> filters;

	// 파트너스
	private LayoutPartner partners;

	// 추천 매거진 - 표시명
	private String best_magazine_subject;

	// 추천 매거진
	private List<Magazine> best_magazines;

	// 최신 매거진
	private List<Magazine> magazines;

	// 주거 우수 파트너스
	private List<Partner> resident_best_partners;

	// 상업 우수 파트너스
	private List<Partner> commerce_best_partners;

	/*// 월간 추천 파트너스
	private List<Partner> month_best_partners;
*/
	// 월간 추천 파트너스
	private List<RecomPartners> month_best_partners;

	// 매거진 - 중간 배너
	private List<EventPromotion> magazine_banners;

	// 매거진 - 뉴스
	private List<News> news;

	// 주거공간 - 최신 견적신청 현황
	private List<Estimate> estimate_list;

	// 집닥 이벤트
	private List<Event> events;

	// 고객 생생 후기
	private List<VividComment> vividcomments;

	// 인테리어 팁
	private List<Magazine> tip_list;

	// 집닥맨 현장 관리
	private List<Magazine> zipman_manage_list;

	// 인테리어 트랜드 조회
	private List<Magazine> trend_list;

	// 비포앤에프터 조회
	private LayoutGallery  before_after_photos;


	public LayoutGallery getBefore_after_photos() {
		return before_after_photos;
	}

	public void setBefore_after_photos(LayoutGallery before_after_photos) {
		this.before_after_photos = before_after_photos;
	}

	public List<Magazine> getTrend_list() {
		return trend_list;
	}

	public void setTrend_list(List<Magazine> trend_list) {
		this.trend_list = trend_list;
	}

	public List<Magazine> getZipman_manage_list() {
		return zipman_manage_list;
	}

	public void setZipman_manage_list(List<Magazine> zipman_manage_list) {
		this.zipman_manage_list = zipman_manage_list;
	}

	public List<Magazine> getTip_list() {
		return tip_list;
	}

	public void setTip_list(List<Magazine> tip_list) {
		this.tip_list = tip_list;
	}

	public List<VividComment> getVividcomments() {
		return vividcomments;
	}

	public void setVividcomments(List<VividComment> vividcomments) {
		this.vividcomments = vividcomments;
	}

	public List<RecomPartners> getMonth_best_partners() {
		return month_best_partners;
	}

	public void setMonth_best_partners(List<RecomPartners> month_best_partners) {
		this.month_best_partners = month_best_partners;
	}

	public List<EventPromotion> getBanners() {
		return banners;
	}

	public void setBanners(List<EventPromotion> banners) {
		this.banners = banners;
	}

	public EstimateSummary getEstimate() {
		return estimate;
	}

	public void setEstimate(EstimateSummary estimate) {
		this.estimate = estimate;
	}

	public List<PopularTag> getTags() {
		return tags;
	}

	public void setTags(List<PopularTag> tags) {
		this.tags = tags;
	}

	public List<LayoutPartial> getPartials() {
		return partials;
	}

	public void setPartials(List<LayoutPartial> partials) {
		this.partials = partials;
	}

	public LayoutPartner getPartners() {
		return partners;
	}

	public void setPartners(LayoutPartner partners) {
		this.partners = partners;
	}

	public List<LayoutGallery> getFilter_gallery() {
		return filter_gallery;
	}

	public void setFilter_gallery(List<LayoutGallery> filter_gallery) {
		this.filter_gallery = filter_gallery;
	}

	public List<LayoutGallery> getGallery() {
		return gallery;
	}

	public void setGallery(List<LayoutGallery> gallery) {
	    if (gallery == null) return;
	    if (this.gallery == null) {
            this.gallery = gallery;
        } else {
	        for (LayoutGallery item : gallery) {
	            this.gallery.add(item);
            }
        }
	}

	public List<LayoutQuickFilter> getFilters() {
		return filters;
	}

	public void setFilters(List<LayoutQuickFilter> filters) {
		this.filters = filters;
	}

	public List<Partner> getResident_best_partners() {
		return resident_best_partners;
	}

	public void setResident_best_partners(List<Partner> resident_best_partners) {
		this.resident_best_partners = resident_best_partners;
	}

	public List<Partner> getCommerce_best_partners() {
		return commerce_best_partners;
	}

	public void setCommerce_best_partners(List<Partner> commerce_best_partners) {
		this.commerce_best_partners = commerce_best_partners;
	}

	public List<EventPromotion> getMagazine_banners() {
		return magazine_banners;
	}

	public void setMagazine_banners(List<EventPromotion> magazine_banners) {
		this.magazine_banners = magazine_banners;
	}

	public String getBest_magazine_subject() {
		return best_magazine_subject;
	}

	public void setBest_magazine_subject(String best_magazine_subject) {
		this.best_magazine_subject = best_magazine_subject;
	}

	public List<Magazine> getBest_magazines() {
		return best_magazines;
	}

	public void setBest_magazines(List<Magazine> best_magazines) {
		this.best_magazines = best_magazines;
	}

	public List<Magazine> getMagazines() {
		return magazines;
	}

	public void setMagazines(List<Magazine> magazines) {
		this.magazines = magazines;
	}

	public List<News> getNews() {
		return news;
	}

	public void setNews(List<News> news) {
		this.news = news;
	}

	public List<Estimate> getEstimate_list() {
		return estimate_list;
	}

	public void setEstimate_list(List<Estimate> estimate_list) {
		this.estimate_list = estimate_list;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}
}

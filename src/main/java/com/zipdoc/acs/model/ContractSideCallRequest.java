package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.ContractSideCall;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ContractSideCallRequest {

    private long estimate_no;
    private int partner_id;
    private String partner_phone;
    private String writer_phone;
    private int outgoing_flag;

    public long getEstimate_no() {
        return estimate_no;
    }

    public int getPartner_id() {
        return partner_id;
    }

    public String getPartner_phone() {
        return partner_phone;
    }

    public String getWriter_phone() {
        return writer_phone;
    }

    public int getOutgoing_flag() {
        return outgoing_flag;
    }

    public void setEstimate_no(long estimate_no) {
        this.estimate_no = estimate_no;
    }

    public void setPartner_id(int partner_id) {
        this.partner_id = partner_id;
    }

    public void setPartner_phone(String partner_phone) {
        this.partner_phone = partner_phone;
    }

    public void setWriter_phone(String writer_phone) {
        this.writer_phone = writer_phone;
    }

    public void setOutgoing_flag(int outgoing_flag) {
        this.outgoing_flag = outgoing_flag;
    }

    public ContractSideCall build(long zid) {

        ContractSideCall result = new ContractSideCall();
        result.setZid(zid);
        result.setEstimate_no(this.estimate_no);
        result.setPartner_id(this.partner_id);
        result.setPartner_phone(this.partner_phone);
        result.setWriter_phone(this.writer_phone);
        result.setOutgoing_flag(this.outgoing_flag);
        return result;
    }
}

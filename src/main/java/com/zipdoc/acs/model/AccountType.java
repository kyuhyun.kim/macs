package com.zipdoc.acs.model;

public enum AccountType {
	NO_ACCOUNT 		(0),
	ACCOUNT_ZIPDOC		(1),
	ACCOUNT_FACEBOOK	(2),
	ACCOUNT_KAKAO		(3),
	ACCOUNT_NAVER		(4),
	ACCOUNT_GOOGLE		(5)
	;
	
	private final int value;
	
	private AccountType(final int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
	
	public static AccountType valueOf(int value) {
        
		for (final AccountType type : values()) {
			if (type.value == value) return type;
		}
		return NO_ACCOUNT;
	}
}

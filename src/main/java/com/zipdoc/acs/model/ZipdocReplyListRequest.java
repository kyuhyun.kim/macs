package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by ZIPDOC on 2016-06-02.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ZipdocReplyListRequest extends ListRequest {
    private String category;
    private Integer owner = 0;
    private Integer sort = 2;

    @JsonIgnore Long member_no;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "ZipdocReplyListRequest{" +
                "category='" + category + '\'' +
                ", keyword='" + getKeyword() + '\'' +
                ", owner=" + owner +
                ", sort=" + sort +
                ", member_no=" + member_no +
                '}';
    }
}

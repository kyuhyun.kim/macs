package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerListRequest extends ListRequest {


    private Integer best_flag;
    private Integer thumbnail_cnt;
    private Integer month_best;
    private Integer month_best_page;
    private Integer month_best_limit;
    private String branch_no;
    private String searchKeyword;

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getBranch_no() {
        return branch_no;
    }

    public void setBranch_no(String branch_no) {
        this.branch_no = branch_no;
    }

    public Integer getMonth_best_page() {
        return month_best_page;
    }

    public void setMonth_best_page(Integer month_best_page) {
        this.month_best_page = month_best_page;
    }

    public Integer getMonth_best_limit() {
        return month_best_limit;
    }

    public void setMonth_best_limit(Integer month_best_limit) {
        this.month_best_limit = month_best_limit;
    }

    public Integer getMonth_best() {
        return month_best;
    }

    public void setMonth_best(Integer month_best) {
        this.month_best = month_best;
    }

    public Integer getBest_flag() {
        return best_flag;
    }

    public void setBest_flag(Integer best_flag) {
        this.best_flag = best_flag;
    }

    public Integer getThumbnail_cnt() {
        return thumbnail_cnt;
    }

    public void setThumbnail_cnt(Integer thumbnail_cnt) {
        this.thumbnail_cnt = thumbnail_cnt;
    }

    @Override
    public String toString() {
        return "PartnerListRequest{" +
                "best_flag=" + best_flag +
                ", thumbnail_cnt=" + thumbnail_cnt +
                '}';
    }
}

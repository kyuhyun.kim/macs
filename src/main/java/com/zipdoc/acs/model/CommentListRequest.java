package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.CommentType;
import org.springframework.util.StringUtils;

/**
 * Created by 이동규 on 2017-03-09.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class CommentListRequest extends ListRequest {
    private Integer comment_type;
    @JsonIgnore
    private long cno;
    private Integer owner = 0;
    private Integer sort = 1;
    @JsonIgnore Long member_no;
    @JsonIgnore String table_name;

    public Integer getComment_type() {
        return comment_type;
    }

    public void setComment_type(Integer comment_type) {
        this.comment_type = comment_type;
        if(this.comment_type != null){
            CommentType commentType = CommentType.get(comment_type);
            if(commentType!=null){
                this.table_name = commentType.getName();
                this.sort = commentType.getSort();
            }
        }
    }

    public Long getMember_no() {
        return member_no;
    }

    public void setMember_no(Long member_no) {
        this.member_no = member_no;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public long getCno() {
        return cno;
    }

    public void setCno(long cno) {
        this.cno = cno;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public boolean validation(){
        if(comment_type == null || StringUtils.isEmpty(table_name) || cno <= 0) return false;
        return true;
    }

    @Override
    public String toString() {
        return "CommentListRequest{" +
                "cno=" + cno +
                ", comment_type=" + comment_type +
                ", owner=" + owner +
                ", sort=" + sort +
                ", member_no=" + member_no +
                ", table_name='" + table_name + '\'' +
                '}';
    }
}

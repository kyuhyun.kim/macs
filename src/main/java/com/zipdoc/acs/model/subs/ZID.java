package com.zipdoc.acs.model.subs;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.domain.entity.Account;
import com.zipdoc.acs.domain.entity.ZipdocmanUser;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ZID {

	private @JsonProperty("zid") long zid;
	private @JsonProperty("zid_key") String zid_key;
	private @JsonProperty("account_info") Account account;
	private ZipdocmanUser zipdocman_account;
	
	public ZID() {}
	
	public ZID(long zid, String zid_key) {
		this.zid = zid;
		this.zid_key = zid_key;
	}
	
	public long getZid() {
		return zid;
	}
	
	public void setZid(long zid) {
		this.zid = zid;
	}
	
	public String getZid_key() {
		return zid_key;
	}
	
	public void setZid_key(String zid_key) {
		this.zid_key = zid_key;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public ZipdocmanUser getZipdocman_account() {
		return zipdocman_account;
	}

	public void setZipdocman_account(ZipdocmanUser zipdocman_account) {
		this.zipdocman_account = zipdocman_account;
	}
}

package com.zipdoc.acs.model.subs;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.domain.entity.AlarmInfo;
import com.zipdoc.acs.domain.entity.AppVersion;
import com.zipdoc.acs.domain.entity.Member;

public class AuthRes {
	
	private @JsonProperty("account_info") Member member;
	private @JsonProperty("app_update") AppVersion version;
	private @JsonProperty("alarm_info") AlarmInfo alarm_info;
	
	private Map<String, String> url_info;
	
	public AuthRes() {}
	
	public AuthRes(AppVersion version, Member member) {
		this.version = version;
		this.member = member;
	}

	public AuthRes(AppVersion version, Member member, AlarmInfo alarmInfo) {
		this.version = version;
		this.member = member;
		this.alarm_info = alarmInfo;
	}

	public AlarmInfo getAlarm_info() {
		return alarm_info;
	}

	public void setAlarm_info(AlarmInfo alarm_info) {
		this.alarm_info = alarm_info;
	}

	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		this.member = member;
	}
	
	public AppVersion getVersion() {
		return version;
	}
	
	public void setVersion(AppVersion version) {
		this.version = version;
	}

	public Map<String, String> getUrl_info() {
		return url_info;
	}
	
	public void setUrl_info(Map<String, String> url_info) {
		this.url_info = url_info;
	}
}

package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.ArticleType;
import org.apache.commons.lang.StringUtils;

/**
 * Created by ZIPDOC on 2016-06-02.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ArticleListRequest extends ListRequest {
    private ArticleType article_type;

    public ArticleListRequest(){
    }

    public ArticleListRequest(ArticleType article_type, int page, int limit){
        setPageLimit(page, limit);
        this.article_type = article_type;
    }

    public ArticleType getArticle_type() {
        return article_type;
    }

    public void setArticle_type(ArticleType article_type) {
        this.article_type = article_type;
    }

    @Override
    public String toString() {
        return "ArticleListRequest{" +
                "article_type='" + article_type + '\'' +
                '}';
    }
}

package com.zipdoc.acs.model;

import java.util.List;

import com.zipdoc.acs.domain.entity.OrderSummary;
import com.zipdoc.acs.domain.entity.ShopOrderInfo;
import com.zipdoc.acs.domain.entity.ShopOrderSummaryInfo;

public class ShopMyPageResponse {
	private OrderSummary shopMySummary = new OrderSummary();
	private List<ShopOrderInfo> list;
	
	public ShopMyPageResponse(OrderSummary shopMySummary, List<ShopOrderInfo> list) {
		super();
		this.shopMySummary = shopMySummary;
		this.list = list;
	}

	public OrderSummary getShopMySummary() {
		return shopMySummary;
	}
	public void setShopMySummary(OrderSummary shopMySummary) {
		this.shopMySummary = shopMySummary;
	}
	public List<ShopOrderInfo> getList() {
		return list;
	}
	public void setList(List<ShopOrderInfo> list) {
		this.list = list;
	}
}

package com.zipdoc.acs.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.domain.entity.InteriorCategory;

public class CategoryList {
	
	private @JsonProperty("etag") String etag;
    private @JsonProperty("category") List<InteriorCategory> category;

	public List<InteriorCategory> getCategory() {
		return category;
	}
	
	public void setCategory(List<InteriorCategory> category) {
		this.category = category;
	}

	public String getEtag() {
		return etag;
	}

	public void setEtag(String etag) {
		this.etag = etag;
	}
}

package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zipdoc.acs.domain.entity.Estimate;
import org.apache.commons.lang.StringUtils;


import java.util.Arrays;

/**
 * 사용자 신청 견적 요청 정보
 * @author 이동규
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ExternalEstimate extends Estimate{
	// 견적 공간 구분 (주거용 / 상업용)
	private String category_code;

	public String getCategory_code() {
		return category_code;
	}

	public void setCategory_code(String category_code) {
		this.category_code = category_code;
		setCategory_code1(category_code);
	}
}

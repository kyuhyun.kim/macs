package com.zipdoc.acs.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.CallEstimate;
import com.zipdoc.acs.domain.entity.PartnerAssign;
import com.zipdoc.acs.domain.entity.PartnerAssignGeo;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PartnerAssignResponse {
	
	// 파트너 다음 결제일까지 남은 일수
	private Integer remaining;
	
	// 이번달 누적 수락 건수
	private Integer total_accept;
	
	// 이번달 누적 거절 건수
	private Integer total_reject;
	
	// 신규 할당 DB 건수
	private Integer assign_count;
	
	// 신규 수락 DB 건수
	private Integer ongoing_count;
	
	// 할당된 견적의 자동 회수 시간
	private Integer assign_expiration_hour;

	// 조회된 DB 리스트
    private List<PartnerAssign> list;
    
    private List<CallEstimate> call_list;

	private List<PartnerAssignGeo> geo_list;

	private String aes128_key;

	public List<CallEstimate> getCall_list() {
		return call_list;
	}

	public void setCall_list(List<CallEstimate> call_list) {
		this.call_list = call_list;
	}

	public Integer getRemaining() {
		return remaining;
	}

	public void setRemaining(Integer remaining) {
		this.remaining = remaining;
	}

	public Integer getTotal_accept() {
		return total_accept;
	}

	public void setTotal_accept(Integer total_accept) {
		this.total_accept = total_accept;
	}

	public Integer getTotal_reject() {
		return total_reject;
	}

	public void setTotal_reject(Integer total_reject) {
		this.total_reject = total_reject;
	}

	public Integer getAssign_count() {
		return assign_count;
	}

	public void setAssign_count(Integer assign_count) {
		this.assign_count = assign_count;
	}

	public Integer getOngoing_count() {
		return ongoing_count;
	}

	public void setOngoing_count(Integer ongoing_count) {
		this.ongoing_count = ongoing_count;
	}

	public List<PartnerAssign> getList() {
		return list;
	}

	public void setList(List<PartnerAssign> list) {
		this.list = list;
	}

	public Integer getAssign_expiration_hour() {
		return assign_expiration_hour;
	}

	public void setAssign_expiration_hour(Integer assign_expiration_hour) {
		this.assign_expiration_hour = assign_expiration_hour;
	}

	public List<PartnerAssignGeo> getGeo_list() {
		return geo_list;
	}

	public void setGeo_list(List<PartnerAssignGeo> geo_list) {
		this.geo_list = geo_list;
	}

	public String getAes128_key() {
		return aes128_key;
	}

	public void setAes128_key(String aes128_key) {
		this.aes128_key = aes128_key;
	}
}

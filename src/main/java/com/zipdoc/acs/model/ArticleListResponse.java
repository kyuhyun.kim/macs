package com.zipdoc.acs.model;

import com.zipdoc.acs.define.ArticleType;
import com.zipdoc.acs.domain.entity.Article;
import com.zipdoc.acs.domain.entity.Product;

import java.util.List;

public class ArticleListResponse {
	private int total_count;
    private List<Article> list;

	public ArticleListResponse(){

	}

	public ArticleListResponse(int total_count, List<Article> list) {
		this.total_count = total_count;
		this.list = list;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<Article> getList() {
		return list;
	}

	public void setList(List<Article> list) {
		this.list = list;
	}

	public void buildUrl(ArticleType articleType){
		if(list == null || list.size() == 0) return;
		for(Article obj:list){
			obj.buildUrl(articleType);
		}
	}
}

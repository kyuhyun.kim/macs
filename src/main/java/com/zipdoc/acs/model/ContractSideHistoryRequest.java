package com.zipdoc.acs.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.ContractSideHistory;

/**
 * Created by dskim on 2017. 9. 22..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ContractSideHistoryRequest {

    //zid
    private long zid;

    //견적번호
    private long estimate_no;

    //파트너 번호
    private int partner_id;


    private List<ContractSideGpsInfoRequest> contractSideGpsInfoRequestList = new ArrayList<>();

    public long getZid() {
        return zid;
    }

    public void setZid(long zid) {
        this.zid = zid;
    }

    public long getEstimate_no() {
        return estimate_no;
    }

    public void setEstimate_no(long estimate_no) {
        this.estimate_no = estimate_no;
    }

    public int getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(int partner_id) {
        this.partner_id = partner_id;
    }

    public List<ContractSideGpsInfoRequest> getContractSideGpsInfoRequestList() {
        return contractSideGpsInfoRequestList;
    }

    public void setContractSideGpsInfoRequestList(List<ContractSideGpsInfoRequest> contractSideGpsInfoRequestList) {
        this.contractSideGpsInfoRequestList = contractSideGpsInfoRequestList;
    }

    public List<ContractSideHistory> buildEstimate(long zid) throws Exception{

        List<ContractSideHistory> contractSideHistoryList = new ArrayList<>();

        for(ContractSideGpsInfoRequest contractSideGpsInfoRequest : contractSideGpsInfoRequestList) {

            ContractSideHistory contractSideHistory = new ContractSideHistory();
            contractSideHistory.setZid(zid);
            contractSideHistory.setEstimate_no(this.getEstimate_no());
            contractSideHistory.setPartner_id(this.getPartner_id());
            contractSideHistory.setLatitude(contractSideGpsInfoRequest.getLatitude());
            contractSideHistory.setLongitude(contractSideGpsInfoRequest.getLongitude());

            //현재
            long millisStart = Calendar.getInstance().getTimeInMillis();

            Long after = contractSideGpsInfoRequest.getStaying_time();

            if(after != null) {
                long mills = millisStart - after;

                Date staying_date = new Date(mills);

                contractSideHistory.setStaying_time(staying_date);

            } else {
                contractSideHistory.setStaying_time(new Date(millisStart));
            }

            contractSideHistoryList.add(contractSideHistory);

        }
        return contractSideHistoryList;
    }



}

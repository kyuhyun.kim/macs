package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.domain.entity.Product;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ListResponse<T> {
	private int total_count;
    private List<T> list;

	private List<T> best_list;

	public ListResponse(){

	}

	public ListResponse(int total_count, List<T> list) {
		this.total_count = total_count;
		this.list = list;
	}

	public ListResponse(int total_count, List<T> list, List<T> best_list) {
		this.total_count = total_count;
		this.list = list;
		this.best_list = best_list;
	}

	public List<T> getBest_list() {
		return best_list;
	}

	public void setBest_list(List<T> best_list) {
		this.best_list = best_list;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
}

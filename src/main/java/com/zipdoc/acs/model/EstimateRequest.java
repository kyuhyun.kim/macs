package com.zipdoc.acs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zipdoc.acs.define.AgentType;
import com.zipdoc.acs.define.Constants;
import com.zipdoc.acs.domain.entity.Estimate;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by ZIPDOC on 2016-06-10.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class EstimateRequest {
    private long estimate_no;
    private String subject;
    private String category_code1;
    private String category_code2;
    private String category_name2;
    private String writer;
    private String phone_no;
    private String address;
    private String address_detail;
    private String dest_address;
    private String dest_address_detail;
    private String email;
    private String budget;
    private String space;
    private String password;
    private Long schd_start_date;
    private Long schd_visit_date;
    private String comment;
    private Integer interior_product_code = 1;
    private Integer estimate_app_type;
    private Integer estimate_type;
    private String adid;

    // 2018. 08.01. KJB. 프로모션 코드
    private String promotion_code;

    private Long concept_pid;
    private List<Long> pid_list;

    //2018.1.2. 장한솔 (마케팅수신동의 추가)
    private Integer agreement_marketing;

    private Integer living_type;
    private Integer channel_sub_code;

    // 2018.11.14. KJB. 견적 계산기 JSON 정보
    private String construction_json_data;
    private String construction_str;

    //2018.12.19 referer 추가
    private String referer;

    /*2019.02.22 purpose 추가*/
    private Integer purpose;

    /*2019.02.26 방 갯수 추가*/
    private String struct_room_cnt;

    /*2019.02.26 욕실 갯수 추가*/
    private String bath_cnt;

    /*2019.02.22 resident_type 추가*/
    private String resident_type;

    /*2019.02.22 resident 추가*/
    private String resident;

    /* 이사 관련 추가 */
    private String floor;
    private String dest_floor;
    private Integer dest_living_type;
    private Integer load_keep;


    public String getDest_address() {
        return dest_address;
    }

    public void setDest_address(String dest_address) {
        this.dest_address = dest_address;
    }

    public String getDest_address_detail() {
        return dest_address_detail;
    }

    public void setDest_address_detail(String dest_address_detail) {
        this.dest_address_detail = dest_address_detail;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getDest_floor() {
        return dest_floor;
    }

    public void setDest_floor(String dest_floor) {
        this.dest_floor = dest_floor;
    }

    public Integer getDest_living_type() {
        return dest_living_type;
    }

    public void setDest_living_type(Integer dest_living_type) {
        this.dest_living_type = dest_living_type;
    }

    public Integer getLoad_keep() {
        return load_keep;
    }

    public void setLoad_keep(Integer load_keep) {
        this.load_keep = load_keep;
    }

    public String getResident() {
        return resident;
    }

    public void setResident(String resident) {
        this.resident = resident;
    }

    public String getResident_type() {
        return resident_type;
    }

    public void setResident_type(String resident_type) {
        this.resident_type = resident_type;
    }

    public String getStruct_room_cnt() {
        return struct_room_cnt;
    }

    public void setStruct_room_cnt(String struct_room_cnt) {
        this.struct_room_cnt = struct_room_cnt;
    }

    public String getBath_cnt() {
        return bath_cnt;
    }

    public void setBath_cnt(String bath_cnt) {
        this.bath_cnt = bath_cnt;
    }

    public Integer getPurpose() {
        return purpose;
    }

    public void setPurpose(Integer purpose) {
        this.purpose = purpose;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getCategory_code1() {
        return category_code1;
    }

    public void setCategory_code1(String category_code1) {
        this.category_code1 = category_code1;
    }

    public String getCategory_code2() {
        return category_code2;
    }

    public void setCategory_code2(String category_code2) {
        this.category_code2 = category_code2;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getEstimate_no() {
        return estimate_no;
    }

    public void setEstimate_no(long estimate_no) {
        this.estimate_no = estimate_no;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public Long getSchd_start_date() {
        return schd_start_date;
    }

    public void setSchd_start_date(Long schd_start_date) {
        this.schd_start_date = schd_start_date;
    }

    public Long getSchd_visit_date() {
        return schd_visit_date;
    }

    public void setSchd_visit_date(Long schd_visit_date) {
        this.schd_visit_date = schd_visit_date;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public Integer getInterior_product_code() {
        return interior_product_code;
    }

    public void setInterior_product_code(Integer interior_product_code) {
        this.interior_product_code = interior_product_code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress_detail() {
        return address_detail;
    }

    public void setAddress_detail(String address_detail) {
        this.address_detail = address_detail;
    }

    public List<Long> getPid_list() {
        return pid_list;
    }

    public void setPid_list(List<Long> pid_list) {
        this.pid_list = pid_list;
    }

    public Integer getEstimate_app_type() {
        return estimate_app_type;
    }

    public void setEstimate_app_type(Integer estimate_app_type) {
        this.estimate_app_type = estimate_app_type;
    }

    public Integer getEstimate_type() {
        return estimate_type;
    }

    public void setEstimate_type(Integer estimate_type) {
        this.estimate_type = estimate_type;
    }

    public Integer getAgreement_marketing() {
        return agreement_marketing == null ? 0 : agreement_marketing;
    }

    public void setAgreement_marketing(Integer agreement_marketing) {
        this.agreement_marketing = agreement_marketing;
    }

    public String getAdid() {
        return adid;
    }

    public void setAdid(String adid) {
        this.adid = adid;
    }

    public String getPromotion_code() {
        return promotion_code;
    }

    public void setPromotion_code(String promotion_code) {
        this.promotion_code = promotion_code;
    }

    public String getCategory_name2() {
        return category_name2;
    }

    public void setCategory_name2(String category_name2) {
        this.category_name2 = category_name2;
    }

    public Integer getLiving_type() {
        return living_type;
    }

    public void setLiving_type(Integer living_type) {
        this.living_type = living_type;
    }

    public Integer getChannel_sub_code() {
        return channel_sub_code;
    }

    public void setChannel_sub_code(Integer channel_sub_code) {
        this.channel_sub_code = channel_sub_code;
    }

    public Long getConcept_pid() {
        return concept_pid;
    }

    public void setConcept_pid(Long concept_pid) {
        this.concept_pid = concept_pid;
    }

    public String getConstruction_json_data() {
        return construction_json_data;
    }

    public void setConstruction_json_data(String construction_json_data) {
        this.construction_json_data = construction_json_data;
    }

    public String getConstruction_str() {
        return construction_str;
    }

    public void setConstruction_str(String construction_str) {
        this.construction_str = construction_str;
    }

    public String validate(AgentType agentType) {

        // 공통 필수 정보. 간편/상세여부, 작성사, 전화번호, 주소
        /*if (estimate_type == null || StringUtils.isEmpty(writer) || StringUtils.isEmpty(phone_no) || StringUtils.isEmpty(address)) return "[estimate_type|writer|phone_no|address]";*/

        if(this.estimate_app_type != null) {
            if (this.estimate_app_type == 2 && this.estimate_type == null)
                this.estimate_type = 1;
        }
        if (estimate_type == null || StringUtils.isEmpty(writer) || StringUtils.isEmpty(phone_no) ) return "[estimate_type|writer|phone_no|]";

        // 앱 필수
        if (agentType == AgentType.APP) {
            return null;
        }

        // 웹 필수
        if (StringUtils.isEmpty(category_code1) ) return "[category_code1]";

        return null;
    }

    public Estimate buildEstimate(){

        Estimate estimate = new Estimate();

        // 견적명 설정
        if (StringUtils.isNotEmpty(address)) {
            estimate.setSubject(address + (StringUtils.isNotEmpty(category_name2) ? (" "+category_name2) : "") + " 이사 견적");
        } else {
            estimate.setSubject("이사 견적");
        }
        estimate.setEstimate_type(estimate_type);
        estimate.setCategory_code1(category_code1);
        estimate.setCategory_code2(category_code2);
        estimate.setWriter(writer);
        estimate.setPhone_no(StringUtils.replace(phone_no, "-", ""));
        estimate.setAddress(address);
        estimate.setAddress_detail(address_detail);
        estimate.setDest_address(dest_address);
        estimate.setDest_address_detail(dest_address_detail);
        estimate.setFloor(floor);
        estimate.setDest_floor(dest_floor);
        estimate.setLiving_type(living_type);
        estimate.setDest_living_type(dest_living_type);
        estimate.setLoad_keep(load_keep);
        estimate.setSpace(space);

        if (StringUtils.isNotEmpty(address)) {
            String [] token = StringUtils.split(address, " ");
            if (token.length >= 2) {
                estimate.setRegion1(token[0]);
                estimate.setRegion2(token[1]);
            }
        }



        if (schd_start_date != null && schd_start_date > 0){
            Date tempDate;
            try {
                tempDate = new Date(schd_start_date);
            } catch (Exception ignore) {
                tempDate = new Date();
            }
            estimate.setSchd_start_date(tempDate);
        }

        if (schd_visit_date != null && schd_visit_date > 0){
            Date tempDate;
            try {
                tempDate = new Date(schd_visit_date);
            } catch (Exception ignore) {
                tempDate = new Date();
            }
            estimate.setSchd_visit_date(tempDate);
        }

        estimate.setComment(comment);

        if(this.agreement_marketing == null)
            this.agreement_marketing = 0;

        estimate.setAgreement_marketing(this.agreement_marketing);
        estimate.setPromotion_code(this.promotion_code);


        return estimate;
    }

    @Override
    public String toString() {
        return "EstimateRequest{" +
                "budget='" + budget + '\'' +
                ", estimate_no=" + estimate_no +
                ", subject='" + subject + '\'' +
                ", category_code1='" + category_code1 + '\'' +
                ", category_code2='" + category_code2 + '\'' +
                ", writer='" + writer + '\'' +
                ", phone_no='" + phone_no + '\'' +
                ", address='" + address + '\'' +
                ", address_detail='" + address_detail + '\'' +
                ", email='" + email + '\'' +
                ", space='" + space + '\'' +
                ", password='" + password + '\'' +
                ", schd_start_date=" + schd_start_date +
                ", schd_visit_date=" + schd_visit_date +
                ", comment='" + comment + '\'' +
                ", interior_product_code='" + interior_product_code + '\'' +
                ", estimate_app_type='" + estimate_app_type + '\'' +
                ", estimate_type='" + estimate_type + '\'' +
                (adid == null ? "" : ", adid='" + adid + '\'') +
                '}';
    }
}
